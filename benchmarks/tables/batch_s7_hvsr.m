clear; close all
cd '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables'
%  Seccion 7
ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFP_xs0_zs0_npplo6_DWN_20210122T023314.mat';
va = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables/va_7.xlsx';

%% cargar funciones de Green
load(ruta)
hv = readtable(va);

% Las unidades del analisis son 
% [m] distancias
% [m/s] velocidades
% [ton/m3] o [kg/m3] densidad de masa
% [s] tiempo

% Las unidades de los resultados son
% [mm] si densidad en [ton/m3]
% [m] si densidad en [kg/m3]


%% grafica H/V
disp('hvsr')
ImGiiXX = imag(uw);
df      = para.fmax/(para.nf/2);
Fq = para.jvec*df;
% para el receptor 1
% h1 = figure;
for ir = 1
    figure;set(gcf,'name','H/V'); hold on
%     HV = sqrt((-2*squeeze(ImGiiXX(para.jvec+1,ir,1,1)))./...
%               squeeze(ImGiiXX(para.jvec+1,ir,3,3)));
HVt = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,1,1))...
           -squeeze(ImGiiXX(para.jvec+1,ir,2,2)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,3,3)));
   loglog(Fq,abs(HVt),'k-','DisplayName','Modelo','LineWidth',2)
end
loglog(hv.t,hv.P1,'k--','DisplayName','P1','LineWidth',2)
loglog(hv.t,hv.P2,'k-.','DisplayName','P2','LineWidth',2)
loglog(hv.t,hv.P3,'k:','DisplayName','P3','LineWidth',2)
loglog(hv.t,hv.P4,'k--','DisplayName','P4','LineWidth',1)

xlim([0.1 10])
ylim([0.1 10]) 
grid on
legend
title('H/V Sección 7')
xlabel('Frecuencia (Hertz)')
ylabel('Amplitud de H/V')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
set(gca,'FontName','arial')
set(gca,'FontSize',11)
set(gca,'GridLineStyle','-')
set(gca,'GridAlpha',0.55)
set(gca,'MinorGridLineStyle','-')
set(gca,'MinorGridAlpha',0.25)
set(0,'DefaultFigureWindowStyle','normal')
p = [31   205   404   454];
set(gcf,'Position',p)
%% regresar
cd(para.here)
