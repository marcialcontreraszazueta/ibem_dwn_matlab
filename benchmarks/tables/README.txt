Figuras para el reporte.
Para cada SECCION de análisis,
1: Naucalpan
2: Tlalnepantla
3: Acolman
4: Ecatepec
5: Yuridia
6: Chapultepec
7: Texcoco
8: Necaxa

Se revisan simultaneamente 8 CASOS de carga agrupados en dos tipos de excitacion:
Para hincado de columnas con,
0: 4m de avance
1: 6m de avance
2: 10m de avance
3: 16m de avance
4: 22m de avance
5: 29m de avance
Para hincado de tablestacas,
6: Vibración sostenida a 21Hz
7: Encendido o apagado de vibrador

Para cada sección y grupo de cargas se revisan 4 condiciones de servicio:
Dano: Generación de daño estructural, medido en mm/s
Deterioro: Generación de deterioro acelerado, medido en mm/s
AcelConfort: Confort en persosas medido en aceleración promedio en 1 segundo, en m/s2
DosisConfort: Confort en personas considerando la vibración acumulada durante un día, o una noche, medido en m/s1.75

Esto resulta en 8 figuras para cada sección = 2 grupos * 4 condiciones de servicio.
