function batch_s6_all_v6_parahvsr
% FunciOn de Green en medio estratificado
% Rio de los Remedios. 

% Seccion 6 Lago Chapultepec

% matlab -nojvm -nodesktop -r batch_s6_all_v6 sin amortiguamiento

para.descripcion = "TablestacadoRioRemdios Seccion 6, con canal, sin amortiguamiento";

% Sismogramas sinteticos de los tres componentes de desplazamiento en 
% una linea de receptores dada una fuerza puntual en el origen
% para este ejercicio unidades en [m, m/s, ton/m^3], resultados en [mm]
clear; close all
cd '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables'
%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^11; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
para.here=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funciones de transferencia
%% Materiales
para.dim = 1; % 2D
para.nmed = 2; % NUmero de medios
% Propiedades del medio 1 (fondo)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 8500;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.37524;
para.reg(med).sub(i).alpha    = 968;
para.reg(med).sub(i).bet      = 347.18;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 3.4; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.29578;
para.reg(med).sub(i).alpha    = 550;
para.reg(med).sub(i).bet      = 192.21;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 10.9; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.1591;
para.reg(med).sub(i).alpha    = 768;
para.reg(med).sub(i).bet      = 175.43;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 9.5; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.30485;
para.reg(med).sub(i).alpha    = 1282;
para.reg(med).sub(i).bet      = 498.164;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 4.2; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.20764;
para.reg(med).sub(i).alpha    = 1300;
para.reg(med).sub(i).bet      = 237.93;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 10.7; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.28833;
para.reg(med).sub(i).alpha    = 1500;
para.reg(med).sub(i).bet      = 244.38;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 10.6; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.06333;
para.reg(med).sub(i).alpha    = 1575;
para.reg(med).sub(i).bet      = 700;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 0; %espesor


para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% inclusion hueco
med = 2;
para.tipoMed(med) = 1;   % Homogeneo
para.reg(med).rho = 0; % <<< con rho = 0 indicamos que es un hueco
para.reg(med).alpha = 2000;
para.reg(med).bet = 1000;
para.reg(med).qd= 100;

for i=1:para.nmed % LamE
  para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
  para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
  para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
  para.reg(i).tipoatts=1; % Q
end

clear med i
%% GeometrIA
 %de acuerdo a:
%              2D ={'No boundaries',...    2
%                   'Semi Espacio',...     3
%                   '2 contornos ',...     4
%                   'placa ilimitada',...  5
%                   'semi-placa L',...     6
%                   'semi-placa R'};      %7
%              3D ={'No boundaries',...
%                   'From STL file(s)'}; % 3D general
med = 1; 
para.geo(med) = 3; % un semiespacio estratificado
for c = 1:2
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 1; % no boundaries
para.cont(med,c).xa = -3;
para.cont(med,c).a = 6;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 0.25;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;
end

med = 2;  

para.geo(med) = 4; % dos contornos
c = 1; % de arriba
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 9; % desde hardcodeGeom9.m
para.cont(med,c).xa = 0;
para.cont(med,c).a = 0.3;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 1;
para.cont(med,c).h = 1;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;

c = 2; % de abajo
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 10; % desde hardcodeGeom10.m, luego borrarlo
para.cont(med,c).xa = 0;
para.cont(med,c).a = 0.3;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 0.25;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;

clear med
%% Fuente

para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales
para.pol = 2; %{'SH','PSV'}

n = 3; % las 15 priemras para cargas, las ultimas 3 para hvsr
para.ninc = n; % cantida de fuentes verticales
para.tipo_onda=1; % no se usa

% linea de fuentes verticales
% para.fuente(1:n)=2; % FuerzaPuntual
% para.tipo_onda(1:n)=2; % P, S, R
% para.xs(1:n)=   0.0;
% para.ys(1:n)=   0.0;
% para.zs(1:n)=   cat(2,2.0:2.0:30.0,[0,0,0]);
% para.xzs(1:n)=  1;
% para.gam(1:n)= 180; % direccion +z
% para.phi(1:n)=   0;

% fuerzas para hvsr
i=1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
% ninc
para.ninc = n;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% receptor para hvsr
para.rec.xr = 0.0;
para.rec.yr = 0.0;
para.rec.zr = 0.0;
para.rec.nrecx= 1; % cantidad de receptores

% linea de receptores de 2.5 a 300 cada 2.5
nr = 120-9;
r = linspace(25,300,nr).';
th = 0.0*pi/180;
para.rec.xr= [para.rec.xr;r*cos(th)];
para.rec.yr= [para.rec.yr;r*sin(th)];
para.rec.zr= [para.rec.zr;zeros(nr,1)];
para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores

% malla plano xz
% nrx = 120;
% nrz = 31;
% linx = linspace(2.5,300,nrx);
% linz = linspace(0,75,nrz);
% [Z,X]=meshgrid(linz,linx);
% xx = reshape(X,nrx*nrz,1);
% zz = reshape(Z,nrx*nrz,1);
% nr2 = nrx*nrz;
% para.rec.xr= [para.rec.xr;xx];
% para.rec.yr= [para.rec.yr;xx*0];
% para.rec.zr= [para.rec.zr;zz];
% para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores

dispwithname(para.rec.nrecx)
%% Analisis
para.npplo	 = 18;
para.fmax	 = 30.0;
para.nf      = 2^12;
para.jini    = 0;  % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = floor(para.nf/2); %0; % hasta (nf/2+1)=(nf/2)*df


para.fac_omei_DWN = 2;

para.tmax = (para.zeropad)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = 1*para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);       disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

para.jvec = para.jini:para.jfin;
%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
%dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
%  return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
%load handel; sound(y,Fs)    % sonido triunfal
save([para.nomrep '/last.mat'],'R','para');

%quit

% disp(squeeze(R.uw(101,1,1:3,1:3)))

% uw(:,:,:,:) 
%    | | | '-- ux, uy, uz
%    | | '---- indice de incidencia
%    | '------ indice de receptor
%    '-------- indice de frecuencia

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 

cd(para.here)
end
