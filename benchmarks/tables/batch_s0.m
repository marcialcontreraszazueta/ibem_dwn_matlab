function batch_s0
% FunciOn de Green en medio estratificado
% canon semicircular

% matlab -nojvm -nodesktop -r batch_s0

para.descripcion = "canon semicircular";

% Sismogramas sinteticos de los tres componentes de desplazamiento en 
% una linea de receptores dada una fuerza puntual en el origen
% para este ejercicio unidades en [m, m/s, ton/m^3], resultados en [mm]
clear; close all
cd '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables'
%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^11; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
para.here=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funciones de transferencia
%% Materiales
para.dim = 1; % 2D
para.nmed = 2; % NUmero de medios
% Propiedades del medio 1
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 200;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1;
para.reg(med).sub(i).alpha    = 2;
para.reg(med).sub(i).bet      = 1;
para.reg(med).sub(i).qd       = 1000;
% para.reg(med).sub(i).qbili.qfcorner = 20;
% para.reg(med).sub(i).qbili.b = -37.2;
% para.reg(med).sub(i).qbili.m = 2.61;
para.reg(med).sub(i).tipoatts = 1; % Q bilineal
para.reg(med).sub(i).h        = 0; %espesor


para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
clear med i

% inclusion hueco
med = 2;
para.tipoMed(med) = 1;   % Homogeneo
para.reg(med).rho = 0; % <<< con rho = 0 indicamos que es un hueco
para.reg(med).alpha = 2;
para.reg(med).bet = 1;
para.reg(med).qd= 1000;

for i=1:para.nmed % LamE
  para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
  para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
  para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
  para.reg(i).tipoatts=1; % Q
end

clear med i
%% GeometrIA
 %de acuerdo a:
%              2D ={'No boundaries',...    2
%                   'Semi Espacio',...     3
%                   '2 contornos ',...     4
%                   'placa ilimitada',...  5
%                   'semi-placa L',...     6
%                   'semi-placa R'};      %7
%              3D ={'No boundaries',...
%                   'From STL file(s)'}; % 3D general
med = 1; 
para.geo(med) = 3; % un semiespacio estratificado
for c = 1:2
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 1; % no boundaries
para.cont(med,c).xa = -3;
para.cont(med,c).a = 6;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 0.25;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;
end

med = 2;  

para.geo(med) = 4; % dos contornos
c = 1; % de arriba
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 7;
para.cont(med,c).xa = -1;
para.cont(med,c).a = 1;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 1;
para.cont(med,c).h = 0.0;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;
para.cont(med,c).za = 0;

c = 2; % de abajo
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 5; % desde hardcodeGeom9.m
para.cont(med,c).xa = -1;
para.cont(med,c).a = 1;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 1;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;
para.cont(med,c).za = 0;

clear med
%% Fuente

para.fuente=1; % 1 Ondas Planas  2 Fuentes Puntuales
para.pol = 2; %{'SH','PSV'}

n = 8; % las 15 priemras para cargas, las ultimas 3 para hvsr
para.ninc = n; % cantida de fuentes verticales
para.tipo_onda=1; % no se usa

% linea de fuentes verticales
para.fuente(1:n)=1; % OP
para.tipo_onda(1:4)=1; % P, S, R
para.tipo_onda(5:8)=2; % P, S, R
para.xs(1:n)=   0.0;
para.ys(1:n)=   0.0;
para.zs(1:n)=   0.0;
para.xzs(1:n)=  1;
para.gam(1:n)=  [0,-15,-30,-60,0,-15,-30,-60]; % direccion +z
para.phi(1:n)=  0;

% ninc
para.ninc = n;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;


% linea de receptores de 2.5 a 300 cada 2.5
nr = 101;
th = linspace(0,pi,51);
para.rec.xr= linspace(-2,2,nr).';
para.rec.yr= zeros(nr,1);
para.rec.zr= zeros(nr,1);
para.rec.xr(26:76)=cos(th).';
para.rec.zr(26:76)=sin(th).';
para.rec.medi=ones(nr,1);
para.rec.nrecx= nr;

% figure;plot(para.rec.xr,para.rec.zr)

dispwithname(para.rec.nrecx)
%% Analisis
para.npplo	 = 18;
para.fmax	 = 0.25;
para.nf      = 2^12;
para.jini    = floor(para.nf/2);  % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = floor(para.nf/2); %0; % hasta (nf/2+1)=(nf/2)*df

para.loadme = false;

para.fac_omei_DWN = 1;

para.tmax = (para.zeropad)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = 1*para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);       disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

para.jvec = para.jini:para.jfin;
%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
%dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
%  return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
%load handel; sound(y,Fs)    % sonido triunfal
% save([para.nomrep '/last.mat'],'R','para');

% el resultado con 18 pplo y Q=15 hasta 20Hz, luego m=2.61
% /home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/2D_PSV_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo18_DWN_20201211T105807.mat

%quit

% disp(squeeze(R.uw(101,1,1:3,1:3)))

% uw(:,:,:,:) 
%    | | | '-- ux, uy, uz
%    | | '---- indice de incidencia
%    | '------ indice de receptor
%    '-------- indice de frecuencia

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 

% R = RESULT;

for iin=1:1
figure;plot(abs(R.uw(end,:,iin,1)),'k-');hold on;plot(abs(R.uw(end,:,iin,2)),'k--')
end
%figure;plot(abs(R.uw(end,:,iin,1)),'k-');hold on;plot(abs(R.uw(end,:,iin,2)),'k--')



cd(para.here)
%%

end