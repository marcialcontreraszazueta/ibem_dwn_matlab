function batch00
% FunciOn de Green en medio estratificado
% Rio de los Remedios. 

% Seccion 3
% paso 1, verificar fuente

% Sismogramas sinteticos de los tres componentes de desplazamiento en 
% una linea de receptores dada una fuerza puntual en el origen
% para este ejercicio unidades en [m, m/s, Ton/m^3], resultados en [mm]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^11; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
para.here=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funciones de transferencia

%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios

% Propiedades del medio 1
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 8500;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.74351;
para.reg(med).sub(i).alpha    = 823.10;
para.reg(med).sub(i).bet      = 57.94;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 3.0; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 1.66533;
para.reg(med).sub(i).alpha    = 1010.03;
para.reg(med).sub(i).bet      = 54.28;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 8.0; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.00653;
para.reg(med).sub(i).alpha    = 1036.04;
para.reg(med).sub(i).bet      = 174.89;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 11.0; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.10365;
para.reg(med).sub(i).alpha    = 1278.73;
para.reg(med).sub(i).bet      = 260.84;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 7.6; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.18822;
para.reg(med).sub(i).alpha    = 1316.16;
para.reg(med).sub(i).bet      = 216.10;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 7.6; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.47945;
para.reg(med).sub(i).alpha    = 1503.99;
para.reg(med).sub(i).bet      = 593.09;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
clear med i
%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med
%% Fuente

para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales

n = 15;
para.ninc = n; % cantida de fuentes verticales
para.tipo_onda=1; % no se usa

% linea de fuentes verticales
para.fuente(1:n)=2; % FuerzaPuntual
para.tipo_onda(1:n)=2; % P, S, R
para.xs(1:n)=   0.0;
para.ys(1:n)=   0.0;
para.zs(1:n)=   2.0:2.0:30.0;
para.xzs(1:n)=  1;
para.gam(1:n)= 180; % direccion +z
para.phi(1:n)=   0;

%% ninc
para.ninc = n;

%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% linea de receptores
nr = 100;
r = linspace(5,305,nr).'; th = 0.0*pi/180;
para.rec.xr= r*cos(th);
para.rec.yr= r*sin(th);
para.rec.zr= zeros(nr,1);
  
%   para.rec.medi = [ones(11,1);2*ones(39,1);ones(11,1);ones(11,1);2*ones(39,1);ones(11,1);];
para.rec.nrecx= nr; % cantidad de receptores
  
%% Analisis
para.npplo	 = 6;
para.fmax	 = 30.0;
para.nf      = 2^12;
para.jini    = 0;  % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = floor(para.nf/2); %0; % hasta (nf/2+1)=(nf/2)*df


para.fac_omei_DWN = 2;

para.tmax = (para.zeropad)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = 1*para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);       disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

para.jvec = para.jini:para.jfin;

%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
dibujo_estratifi(para,[],0);
BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
%load handel; sound(y,Fs)    % sonido triunfal
save([para.nomrep '/last.mat'],'R','para');
%quit

uw = R.uw; sw = R.sw;
% disp(squeeze(R.uw(101,1,1:3,1:3)))

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 
return
%% ver respuesta en frecuencia
figure; hold on
df = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
plot(para.jvec*df, real(uw2(:,30,1,3)),'r-')
plot(para.jvec*df, imag(uw2(:,30,1,3)),'b-')
plot(para.jvec*df, abs(uw2(:,30,1,3)),'k-')

%% funcion de amplitud
% para.pulso.tipo=3; %Ricker periodo caracter?stico tp
% para.pulso.a = 0.03;   % tp
% para.pulso.b = 1;    % ts
% para.pulso.c = 0;  % corrimiento

para.pulso.tipo=5; % gaussiano rise time
para.pulso.a = 0.1;   % rise time 
para.pulso.b = 1;   % retraso

para.pulso.tipo=2.5; %bandpass Gaussiano
para.pulso.a = 27.5;   % media Hz
para.pulso.b = 1;    % ts
para.pulso.c = 0.5;  % desviacion estandar Hz

% para.pulso.tipo=4; % plano + tapper gaussiano
% para.pulso.a = 2;   % rise time
% para.pulso.b = 10;    % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);

%% Derivar para obtener velocidades
% w_vec   = Fq.'*(2*pi);
% uwv=zeros(size(uw));
% for ii2=1:size(uw,2)
%   for ii3=1:size(uw,3)
%     for ii4=1:size(uw,4);
% uwv(:,ii2,ii3,ii4) = w_vec.*uw(:,ii2,ii3,ii4); 
%     end
%   end
% end
% % grafica
% 
% iinc = 1;
% recep = 30;
% 
% figure(9800);
% tx = [num2str(recep),':(',num2str(para.rec.xr(recep)),' , ',...
%   num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
% set(gcf,'name',tx)
% arnam{1} = ['u_',tx]; arnam{2} = ['v_',tx]; arnam{3} = ['w_',tx];
% axes; hold on
% for icomp = 1:3
%   semilogy(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',arnam{icomp});
%   %plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
%   %plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
%   %plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
%   
%   semilogy(Fq,abs(uwv(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',['V' arnam{icomp}]);
%   %plot(Fq,abs(uwv(:,recep,iinc,icomp)),'r.','DisplayName',['V' arnam{icomp}]);
% end
% title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
% 
% disp('done')
% uw = uwv;

%% inversion
[utc,~]   = inversion_w(uw,sw,para);

%% sabana


% resultados en  x
namf={'x','y','z'};
tran = find(tps>0); % sec
ran = 1:para.rec.nrecx;
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));

for iinc = 1:15
    for icomp = 3
        tx = ['U',namf{icomp},'| F',iinc,':(',num2str([para.xs(iinc) para.ys(iinc) para.zs(iinc)]),')'];
        figure; hold on; set(gcf,'name',tx)
        % sca = 4.3955e+09;%
%         sca = 20 * para.rec.dxr / max(max(utc(tran,ran,iinc,icomp)));
        sca = 2e+13;
        for ir = ran
            plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-');
            %plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
            ylabel('x (m)')
            xlabel('tiempo')
            ylim([0 para.rec.xr(ran(end))]) % receptores
        end
        xlim([0,15])
    end
end
%% regresar
cd(para.here)
end