% Combinaciones de carga para proyecto de tablestacado

% superposición de campos de ondas para cada
% uw(:,:,:,:)
%    | | | '-- ux, uy, uz
%    | | '---- indice de incidencia
%    | '------ indice de receptor
%    '-------- indice de frecuencia
        
%% config
clear
set(0,'DefaultFigureWindowStyle' , 'normal')
pausarcadafig = true;
for seccion = 3
% correr_configuraciones = 1:2; % velcidad instantanea de particula
% correr_configuraciones = 3:4; % aceleracion de particula rms
% correr_configuraciones = 5:6; % dosis de vibracion
for correr_configuraciones = [[1,2],[3,4],[5,6]]
% for correr_configuraciones = [[6]]
close all
cd '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables'
clearvars -except seccion correr_configuraciones pausarcadafig
% configuraciones disponibles, cada una produce una grafica por cada grupo
% de umbrales

config = {
    {0:5,'v','n','mm',1000},... % columna, velocidad, no peso, mm/s
    {6:7,'v','n','mm',1000},... % tablest, velocidad, no peso, mm/s
    {0:5,'a','n','m',1},...     % columna, acel,      no peso, m/s2
    {6:7,'a','n','m',1},...     % tablest, acel,      no peso, m/s2
    {0:5,'a','bd','m',1},...     % columna, dosis,     peso h/b,m/s1.2
    {6:7,'a','bd','m',1}};       % tablest, dosis,     peso h/b,m/s1.2


% radios de afectacion en casa leidos en las imagenes.
% .caso(5) es para las figuras de columnas
% .caso(7) es para las figuras de tablestacas
% .dano Figura dano estructural (Siskind)
% .deter Figura deterioro acelerado (Alemania/casas)
% .confA Figura límite de confort vs Amax particula (ISO 2631-2)
% .confDosis Figura límite de confort vs Dosis de vibracion (Casas/dia)
rCasa1 = 40; % m hasta la primer casa

ranC.Sec(1).ready = true;
ranC.Sec(1).caso(5).dano=40;
ranC.Sec(1).caso(5).deter=143;
ranC.Sec(1).caso(5).confA=160.2;
ranC.Sec(1).caso(5).confDosis=78.27;
ranC.Sec(1).caso(7).dano=40;
ranC.Sec(1).caso(7).deter=140.8;
ranC.Sec(1).caso(7).confA=189.3;
ranC.Sec(1).caso(7).confDosis=91.36;

ranC.Sec(2).ready = false;
ranC.Sec(2).caso(5).dano=40;
ranC.Sec(2).caso(5).deter=40;
ranC.Sec(2).caso(5).confA=40;
ranC.Sec(2).caso(5).confDosis=40;
ranC.Sec(2).caso(7).dano=40;
ranC.Sec(2).caso(7).deter=40;
ranC.Sec(2).caso(7).confA=40;
ranC.Sec(2).caso(7).confDosis=40;

ranC.Sec(3).ready = true;
ranC.Sec(3).caso(5).dano=40;
ranC.Sec(3).caso(5).deter=110.3;
ranC.Sec(3).caso(5).confA=127.7;
ranC.Sec(3).caso(5).confDosis=66.16;
ranC.Sec(3).caso(7).dano=40;
ranC.Sec(3).caso(7).deter=112.2;
ranC.Sec(3).caso(7).confA=147.1;
ranC.Sec(3).caso(7).confDosis=67.61;

ranC.Sec(4).ready = true;
ranC.Sec(4).caso(5).dano=40;
ranC.Sec(4).caso(5).deter=135;
ranC.Sec(4).caso(5).confA=141.3;
ranC.Sec(4).caso(5).confDosis=69.55;
ranC.Sec(4).caso(7).dano=40;
ranC.Sec(4).caso(7).deter=110.3;
ranC.Sec(4).caso(7).confA=162.1;
ranC.Sec(4).caso(7).confDosis=64.7;

ranC.Sec(5).ready = true;
ranC.Sec(5).caso(5).dano=40;
ranC.Sec(5).caso(5).deter=91;
ranC.Sec(5).caso(5).confA=96.2;
ranC.Sec(5).caso(5).confDosis=52.58;
ranC.Sec(5).caso(7).dano=40;
ranC.Sec(5).caso(7).deter=91.84;
ranC.Sec(5).caso(7).confA=134.5;
ranC.Sec(5).caso(7).confDosis=72.46;

ranC.Sec(6).ready = true;
ranC.Sec(6).caso(5).dano=40;
ranC.Sec(6).caso(5).deter=45.32;
ranC.Sec(6).caso(5).confA=57.5;
ranC.Sec(6).caso(5).confDosis=40;
ranC.Sec(6).caso(7).dano=40;
ranC.Sec(6).caso(7).deter=75.46;
ranC.Sec(6).caso(7).confA=160.2;
ranC.Sec(6).caso(7).confDosis=55;

ranC.Sec(7).ready = true;
ranC.Sec(7).caso(5).dano=40;
ranC.Sec(7).caso(5).deter=85.54;
ranC.Sec(7).caso(5).confA=94.26;
ranC.Sec(7).caso(5).confDosis=54.52;
ranC.Sec(7).caso(7).dano=40;
ranC.Sec(7).caso(7).deter=88.45;
ranC.Sec(7).caso(7).confA=118;
ranC.Sec(7).caso(7).confDosis=55;

ranC.Sec(8).ready = true;
ranC.Sec(8).caso(5).dano=40;
ranC.Sec(8).caso(5).deter=83.7;
ranC.Sec(8).caso(5).confA=93.58;
ranC.Sec(8).caso(5).confDosis=52.7;
ranC.Sec(8).caso(7).dano=40;
ranC.Sec(8).caso(7).deter=89.27;
ranC.Sec(8).caso(7).confA=116.8;
ranC.Sec(8).caso(7).confDosis=57;
% Calcular resultados para casos de carga

switch seccion
    case 1
        %  Seccion 1  naucalpan
        % %version sin amortiguamiento
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201008T230415.ma';
        % version con amortiguamiento bilineal 0,28;9,28;30,80
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201104T074331.mat';
        puntos = {{35,20.34969572},{35,23.91674416},{118,4.501704704},{175,2.893277668},{136,4.835855691}}; % mm/s
    case 2
        %  Seccion 2   tlalnepantla
        % % version sin amortiguamiento
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201009T005156.mat';
        % % version Q=80
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201102T060038.mat';
    case 3
        %  Seccion 3  acolman
        % %version sin amortiguamiento
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201009T021127.mat';
        % % version Q=80
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201102T070448.mat';
        % Q 3.8
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210525T155653.mat';
        % Q 5.7
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210701T194140.mat';
        % Q 12
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210701T212847.mat';
        % Q 16
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210702T133347.mat';
        % Q 28
        % ruta ='/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210702T114410.mat';
        % Q bili 28
        % ruta='/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210702T170307.mat';
        % Q bili 12
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210703T000334.mat';
        % Q bili 12 m 0.65
        %ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210704T132827.mat';
        % Q bili 16 m -0.52
        % ruta='/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210704T153621.mat';
        % Q bili 18 m -0.68
        %ruta='/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210704T185556.mat';
        % Q bili 23 m -0.88
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210704T202945.mat';
        puntosPilote = {{ 20.87 , 13.133403062820435 },{ 20.87 , 9.64475513 },{ 20.87 , 6.84012711 },{ 135.7 , 0.6223355233669281 }}; % mm/s
    case 4
        %  Seccion 4   ecatepec
        % %version Q=80
        % ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201102T080420.mat';
        % %version con amortiguamiento bilineal
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201130T141105.mat';
        puntos = {{ 85.8 , 4.525887489318848 },{ 20 , 7.021567344665527 },{166,5.7510333}}; % mm/s
    case 5
        %  Seccion 5   yuridia
        % Q = 15 hazta 20, luego m=2.16
        %ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201211T105431.mat';
        %ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201215T065503.mat';
        % con secciones corregidas
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210122T001528.mat';
        puntos =  {{ 60,12.850130442529917},{ 147,0.6095619755797088},{ 244,2.9332569101825356}}; % mm/s
    case 6
        %  Seccion 6   chapultepec
        % Q = 15 hazta 20, luego m=2.16
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20201211T131144.mat';
        puntos = {{ 51,2.679285672941 },{ 173,1.175728936355 },{ 286,0.357740848547 }}; % mm/s
    case 7
        %  Seccion 7   texcoco
        % Q = 15 hata 20, luego 2.16
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210122T025927.mat';
        puntos = {{ 47.5 , 14.320637591982999 },{ 158 , 0.102663926078 }}; % mm/s
    case 8
        %  Seccion 8   necaxa
        %ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210122T040932.mat';
        ruta = '/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs2_npplo6_DWN_20210525T131939.mat';
        puntos = {{47.5, 4.7402056}, {93.5, 1.5565219}, {170.5, 0.172163}}; % mm/s
end

cd ../../ibem_matlab/

% cargar funciones de Green
load(ruta)
% Las unidades del analisis son
% [m] distancias
% [m/s] velocidades
% [ton/m3] o [kg/m3] densidad de masa
% [s] tiempo

% [mm] si densidad en [ton/m3]
% [m] si densidad en [kg/m3]


% se calculó con ton/m3, de modo que uw es en mm.
uw = uw / 1000; % [m] <<-- para aplicar las fuerza en Newtons !!! no quitar


for igraf = correr_configuraciones%   igraf=2
    close all
    casos = config{igraf}{1}; % hincado o tablestaca
    tipo = config{igraf}{2}; % d desplazamiento, v velocidad, a aceleraicion
    % si 'a' se elige si se debe dar peso al espectro:
    subtipo = config{igraf}{3}; % peso de acuerdo a sensibildiad
    % dar peso a aceleracion. 'n': no, b: vertical, d: horizontal
    unidad1 = config{igraf}{4}; % unidad en la grafica
    escala_de_graf = config{igraf}{5}; % 1000 para pasar de m a mm
    
    if tipo == 'v'
        grupos = 1:2;
    elseif tipo == 'a' && contains('n',subtipo)
        grupos = 3;
    else % 'a', 'bd'
        grupos = 4;
    end
    
    VDVcaso = cell(1,8);
    for caso = casos %   caso = 6
        
        dispwithname(caso)
        
        % Funcion de amplitud
        if caso<=5
            caso_grupo_nom = 'Hincado de pilote';
            disp(["Caso",num2str(caso)," Gaussiano con risetime"])
            para.pulso.tipo=5; % gaussiano rise time
            para.pulso.a = 0.1;   % rise time
            para.pulso.b = 1.0;   % retraso
            npulsos = 10; % si se agregan mas se 
            deltapulsos = 1.327; % el propuesto se compruebó en campo
            
            [Fq,cspectre,tps,~] = showPulso(para,false,true,[npulsos,deltapulsos]);
            
            if caso==1
                plotcspectre(para,cspectre,true)
            end
        elseif caso == 6
            caso_grupo_nom = 'hincado de tablestaca';
            disp(["Caso",num2str(caso)," Bandpass Gaussiano"])
            para.pulso.tipo=2.5; %bandpass Gaussiano
            para.pulso.a = 21.0;   % media Hz
            para.pulso.b = 15;    % ts
            para.pulso.c = 0.05;  % desviacion estandar Hz
            [Fq,cspectre,tps,~] = showPulso(para,false,true);
        elseif caso == 7
            caso_grupo_nom = 'hincado de tablestaca';
            disp(["Caso",num2str(caso)," Bandpass Gaussiano"])
            para.pulso.tipo=2.5; %bandpass Gaussiano
            para.pulso.a = 8.0;   % media Hz
            para.pulso.b = 1;    % ts
            para.pulso.c = 0.5;  % desviacion estandar Hz
            [Fq,cspectre,tps,~] = showPulso(para,false,true);
        end
        
        
        % p = [1 1 1141 440];
        % set(gcf,'Position',p)
        % xlim([0 10])
        
        % Derivar para obtener velocidades
        w_vec   = 1j * Fq.'*(2*pi);
        w_vec2  = w_vec.^2;
        uwv=zeros(size(uw));
        for ii2=1:size(uw,2)
            for ii3=1:size(uw,3)
                for ii4=1:size(uw,4)
                    uwv(:,ii2,ii3,ii4) = w_vec.*uw(:,ii2,ii3,ii4);
                end
            end
        end
        
        % Derivar para obtener aceleraciones
        uwa=zeros(size(uw));
        for ii2=1:size(uw,2)
            for ii3=1:size(uw,3)
                for ii4=1:size(uw,4)
                    uwa(:,ii2,ii3,ii4) = w_vec2.*uw(:,ii2,ii3,ii4);
                end
            end
        end
        
        if tipo == 'd'
            UUU = uw;
            disp('desplazamiento')
            unidades = unidad1;
            unidadtxt = 'Desplazamiento máximo';
        end
        if tipo == 'v'
            UUU = uwv;
            disp('velocidad')
            unidades = [unidad1 '/s'];
            unidadtxt = 'Velocidad máxima';
        end
        if tipo == 'a'
            UUU = uwa;
            disp('aceleracion')
            unidades = [unidad1 '/s2'];
            unidadtxt = 'Aceleración máxima';
            if contains('bd',subtipo)
                disp('dosis')
                unidades = [unidad1 '/s^{1.75}'];
                unidadtxt = 'Dosis diaria de vibración';
            end
        end
        
        % %% grafica
        %
        % iinc = 1;
        % recep = 30;
        %
        % figure(9800);
        % tx = ['(',num2str(para.rec.xr(recep)),' , ',...
        %   num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
        % set(gcf,'name',tx)
        % arnam{1} = ['_x ',tx]; arnam{2} = ['_y ',tx]; arnam{3} = ['_z ',tx];
        % axes; hold on
        % for icomp = 3
        %   plot(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',['D', arnam{icomp}]);
        %   %plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
        %   %plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
        %   %plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
        %
        %   plot(Fq,abs(uwv(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',['V', arnam{icomp}]);
        %   %plot(Fq,abs(uwv(:,recep,iinc,icomp)),'r.','DisplayName',['V' arnam{icomp}]);
        %   plot(Fq,abs(uwa(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',['A', arnam{icomp}]);
        % end
        % grid on
        % set(gca, 'YScale', 'log')
        % title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
        % legend()
        % disp('done')

        disp('superposicion para generar casos de cargas')
        bb = size(UUU);
        uwSup = zeros(bb(1),bb(2),1,bb(4));
        switch caso
            case 0 % caso 0
                pun = 3650.5 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,2,:);
                casoTx = ' 4m de avance';
            case 1 % caso 1
                pun = 3509.5 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,3,:);
                casoTx = ' 6m de avance';
            case 2
                pun = 3738.5 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,5,:);
                casoTx = ' 10m de avance';
            case 3
                pun = 3604.5 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,8,:);
                casoTx = ' 16m de avance';
            case 4
                pun = 3834 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,11,:);
                casoTx = ' 22m de avance';
            case 5
                pun = 4111.5 * 1000; % [N] fuerza de punta (en el ultimo)
                uwSup = pun*UUU(:,:,15,:);
                casoTx = ' 29m de avance';
            case 6
                pwer = 458; % kW potencia
                frc  = 21; % Hz vibracion estacionaria del vibrador
                dur  = 10; % segundos para enterrar
                prof = 7; % m enterrados
                energi = pwer/frc; % kJ
                nciclos = dur*frc; %ciclos de carga aplicados
                desp_por_ciclo = prof/nciclos; % m
                fza = energi/desp_por_ciclo; % N energia por ciclo
                tra = fza*1000/2.5; % [N] fuerza a repartir entre entre 7 metros
                for ii =1:3 % carga a cada 2 metros
                    uwSup = uwSup + tra*UUU(:,:,ii,:);
                end
                casoTx = ' Sostenido a 21Hz';
            case 7
                pwer = 458; % kW potencia
                frc  = 8; % Hz vibracion estacionaria del vibrador
                dur  = 10; % segundos para enterrar
                prof = 7; % m enterrados
                energi = pwer/frc; % kJ
                desp_por_ciclo = 0.0333; % m <<<<<<- el desplazamiento con vibracion a 21 Hz
                fza = energi/desp_por_ciclo; % N energia por ciclo
                tra = fza*1000/2.5; % [N] fuerza a repartir entre 7 metros
                for ii = 1:3 % carga a cada 2 metros
                    uwSup = uwSup + tra*UUU(:,:,ii,:);
                end
                casoTx = ' Enciende/Apaga vibrador';
        end
        
%         %% Espectro
%         icomp = 3;
%         dispwithname(icomp)
%         
%         xrS = [11, 21, 41, 81, 112]; % indices de receptores (cada 50m)
%         color = {'#000000','#000000','#77AC30','#77AC30','#7E2F8E','#7E2F8E','#000000','#000000','#77AC30','#77AC30','#7E2F8E','#7E2F8E'};
%         grueso = [3,3,3,2,2,2,1,1,1,0.5,0.5,0.5];
%         estilo = {'-',':','-',':','-',':','-',':','-',':','-',':','-',':','-',':','-'};
%         namf={'x','y','z'};
%         figure(49227); clf; hold on
%         for xri=1:size(xrS,2)
%             cosiente = abs(uwSup(:,xrS(xri),1,icomp));
%             elcolor = sscanf(color{xri}(2:end),'%2x%2x%2x',[1 3])/255;
%             plot(Fq,cosiente , ...
%             'DisplayName',['|V_',namf{icomp},'(',num2str(para.rec.xr(xrS(xri))),'m)|'], ...
%             'Color',elcolor, ...
%             'LineStyle',estilo{xri}, ...
%             'LineWidth',grueso(xri));
%         end
%         legend()
%         set(datacursormode(49227), 'UpdateFcn', @tooltipfunc1)
%         
%         title(['Seccion ',num2str(seccion),' Caso ', num2str(caso),...
%             '. Espectro de amplitud de velocidad vertical '])
%         xlabel('frecuencia [Hz]');ylabel('amplitud')
%         set(gca,'XMinorTick','on')
%         grid on
%         p = [5 45 1252 347];
%         set(49227,'Position',p)
%         
%         xlim([0, 20])
%         %%
        
        % Sintenticos        
        if tipo == 'a' && contains('bd',subtipo)
            if para.zeropad < para.nf; para.zeropad = para.nf; end
            utc = zeros(para.zeropad,size(uwSup,2),1,size(uwSup,4));
            % vertical
            utc(:,:,1,end) = BS6841_W(uwSup(:,:,1,end),para,cspectre,'b');
            % horizontal
            utc(:,:,1,1) = BS6841_W(uwSup(:,:,1,1),para,cspectre,'d');
        else
            [utc,~] = inversion_w(uwSup,sw,para,cspectre);
        end
        
        if tipo == 'a'
            if contains('bd',subtipo)
                % dosis
                % Se obtiene la dosis para 10 golpes, y se multiplica por 100,
                % para tener la dosis de un hincado completo, la cual se
                % acepta puede ocurrir cada hora.
                %
                % Para vibración, consideramos que el vibrado de 30 segundos
                % puede ocurrir cada media hora.
                dt = tps(2);
                if caso<=5
                    TT = 36; % muestra en segundos
                    Ttramo = 196; % cada 196 seg. se avanzan 4 m.
                else
                    TT = 30; % muestra en segundos
                    Ttramo = TT; %
                end
                nnn = int32(TT/dt); % segundos
                VDV = BS6841_VDV(utc(1:nnn,:,:,:)*escala_de_graf,dt);
                
                % los guardamos por cada caso de carga, para luego sumarlos
                VDVcaso(caso+1) = {VDV * (Ttramo/TT)^0.25}; % en un hincado cada caso
            else
                % rms. for 1 sec running average
                t0 = find(tps==0.5);
                tf = find(tps==(tps(end)-0.5));
                d1sec = (t0-1)*2;
                utcM = utc.*0;
                for icom = 1:size(utc,4)
                    for ir = 1:size(utc,2)
                        oututc = rmsDecimate(utc(:,ir,1,icom),tps(2),d1sec);
                        utcM(:,ir,1,icom) = oututc;
%                         utcM(:,ir,1,icom) = smooth(utc(:,ir,1,icom),d1sec,'lowess');
                    end
                end
                utc = utcM;
            end
        end
        
%         %% Ver uno
%         ir = 5
%         icomp = 2
%         
%         iinc = caso;
%         namf={'x','y','z'};
%         tran = find(tps>0); % sec
%         tx = ['U',namf{icomp},' caso ',num2str(iinc),' x:(',num2str([para.rec.xr(ir) para.rec.yr(ir) para.rec.zr(ir)]),')'];
%         fh = figure; set(fh,'name',tx); hold on
%         plot(tps(tran),utc(tran,ir,1,icomp),'b-');
% %         plot(tps(tran),utcM(tran,ir,1,icomp),'r-');
%         ylabel(['V',namf{icomp},' (m/s)'])
%         xlabel('Tiempo (s)')
%         title(tx)
%         xlim([0,30])
%         %% ylim([-0.12,0.12])
%         %% Sabanas
%         namf={'x','y','z'};
%         tran = find(tps>0); % sec
%         ran = 1:para.rec.nrecx;
%         para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
%         for iinc = 1 %1:15
%             for icomp = 1:size(utc,4)
%                 tx = ['V',namf{icomp},' caso ',num2str(iinc)];
%                 %tx = ['U',namf{icomp},'| Fz','(',num2str([para.xs(iinc) para.ys(iinc) para.zs(iinc)]),')'];
%                 fh = figure; hold on; set(fh,'name',tx)
%                 %sca = 20 * para.rec.dxr / max(max(utc(tran,ran,iinc,icomp)));
%                 sca = 1e-4; % para V
% %                 sca = 1e01; % para a
%                 for ir = ran
%                     plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-');
%                     %plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
%                     ylabel('x (m)')
%                     xlabel('Tiempo (s)')
%                     title(tx)
%                     margen = 5*para.rec.xr(1);
%                     ylim([-margen para.rec.xr(ran(end))+margen]) % receptores
%                 end
%                 xlim([0,30])
%                 
%                 set(gca,'FontName','arial')
%                 set(gca,'FontSize',10)
%                 set(0,'DefaultFigureWindowStyle','normal')
%                 p = [1262 612 589 359];
%                 %p = [1262 612 359 589];
%                 set(fh,'Position',p)
%                 saveas(fh,[para.here,'/',tx,'.png'])
%             end
%         end
        %%
        
        % Envolvente de absolutos de velocidad total
        if not(contains('bd',subtipo))
            tran = find(tps<15); % sec
            ran = 1:para.rec.nrecx;
            bb = size(utc);
            Env = zeros(size(para.rec.xr,1),1);
            for ir = ran
                Env(ir) = escala_de_graf * max((utc(tran,ir,1,1).^2+(utc(tran,ir,1,end).^2).^0.5),[],1); % en unidades unidad1
            end
            

            % Una grafica por cada tipo de incidencia (columnas y tablestacas) y por
            % cada grupo de umbrales (dano, deterioro, confort por amax, confor por dosis)
            
            tiplin=["-k","-k",'--k',':k','-.k','-k','-k','-b'];
            grslin=[2.5,2,2,2,2,1,2,2];
            
            for ifig = grupos
                handfigura = 92740+ifig;
                figure(handfigura);hold on
                plot(para.rec.xr(2:end),Env(2:end),tiplin(caso+1),'LineWidth',grslin(caso+1),'DisplayName',['Caso ',num2str(caso), casoTx])
                title('')
            end
        end
    end % cerrar el loop de casos
    
    % suma de dosis en cada caso de carga
    if contains('bd',subtipo)
        VDV1hincado = VDVcaso{casos(1)+1}*0;
        VDV1hincado10 = VDV1hincado; % 10m o mas
        for caso = casos
            if tipo == 'a' && caso > 1
                VDV1hincado10 = VDV1hincado10 + VDVcaso{caso+1}.^4;
            end
            VDV1hincado=VDV1hincado + VDVcaso{caso+1}.^4;
        end
        % la dosis por un hincado completo
        VDV1hincado10=VDV1hincado10.^0.25; 
        VDV1hincado=VDV1hincado.^0.25; 
        
        VDV1H10 = VDV1hincado10*0;
        VDV1H = VDV1hincado*0;
        if caso == 5 % columnas
            % una columna por hora
            VDV1H10 = VDV1hincado10;
            VDV1H = VDV1hincado;
        end
        if caso == 7 % tablestacas
            % 3 tablestacas por hora
            VDV1H = VDV1hincado * 3^0.25;
        end
        
        VDV16H10 = VDV1H10 * 16^0.25; % en 16 horas del dia
        VDV08H10 = VDV1H10 * 8^0.25; % en 8 horas de la noche
        VDV16H = VDV1H * 16^0.25; % en 16 horas del dia
        VDV08H = VDV1H * 8^0.25; % en 8 horas de la noche
        
        % graficar
        tiplin=["--k","--k",'-k','-k','-b','-b'];
        for ifig = grupos
            handfigura = 92740+ifig;
            figure(handfigura);hold on
%             plot(para.rec.xr,VDV1H(1,:,1,1),tiplin(1),'LineWidth',1,'DisplayName','Dosis Horizontal 1 hora')
            plot(para.rec.xr,VDV16H(1,:,1,1),tiplin(3),'LineWidth',1,'DisplayName','Dosis Horizontal Dia')
            plot(para.rec.xr,VDV08H(1,:,1,1),tiplin(5),'LineWidth',1,'DisplayName','Dosis Horizontal Noche')
            
            plot(para.rec.xr,VDV16H10(1,:,1,1),tiplin(3),'LineWidth',3,'DisplayName','Dosis Horizontal Dia h>10m')
            plot(para.rec.xr,VDV08H10(1,:,1,1),tiplin(5),'LineWidth',3,'DisplayName','Dosis Horizontal Noche h>10m')
            title('')
        end
        for ifig = grupos
            handfigura = 92740+ifig;
            figure(handfigura);hold on
%             plot(para.rec.xr,VDV1H(1,:,1,3),tiplin(2),'LineWidth',2,'DisplayName','Dosis Vertical 1 hora')
            plot(para.rec.xr,VDV16H(1,:,1,end),tiplin(4),'LineWidth',2,'DisplayName','Dosis Vertical Dia')
            plot(para.rec.xr,VDV08H(1,:,1,end),tiplin(6),'LineWidth',2,'DisplayName','Dosis Vertical Noche')
            
            plot(para.rec.xr,VDV16H10(1,:,1,end),tiplin(4),'LineWidth',3,'DisplayName','Dosis Vertical Dia h>10m')
            plot(para.rec.xr,VDV08H10(1,:,1,end),tiplin(6),'LineWidth',3,'DisplayName','Dosis Vertical Noche h>10m')
            title('')
        end
    end
    %% Trazar umbrales y decorar la grafica
    
    limit = {{... % umbrales para dano mm/s
        {5,'Dano','Vibración contra umbrales de probable daño',ranC.Sec(seccion).caso(caso).dano},...
        {56,'Siskind | 10% prob. fisuras en yeso'},...
        {127,'Siskind | 50% prob. fisuras en yeso'},...
        {76,'Siskind | 10% prob. daño elementos estructurales'},...
        {152,'Siskind | 50% prob. daño elementos estructurales'},...
        {2.5, 'Nivel de ruido ambiental natural'}},...
        {... % umbrales para deterioro acelerado  mm/s
        {6,'Deterioro','Vibración contra valores límite para deterioro acelerado',ranC.Sec(seccion).caso(caso).deter},...
        {5,['\bf','Reino Unido | vibración transitoria',newline,...
            'Alemania | ','casas','\rm']},...
        {12, 'Suiza | edificios industriales, torres, túneles'},...
        {8, 'Suiza | edificios con losa de concreto'},...
        {3, 'Suiza y Alemania | edificios históricos'},...
        {20, 'Alemania y Reino Unido | edificios industriales'},...
        {2.5, 'Nivel de ruido ambiental natural'}},...
        {... % umbrales para confort por acel. max  m/s2
        {5,'AcelConfort','Vibración contra valores límite de confort',ranC.Sec(seccion).caso(caso).confA},...
        {0.49,'NTC-Met2017 | Tolerancia a vib. transitoria'},...
        {0.20,['Argentina | Fatiga en personas',newline,...
                'NTC-Met2017 | Molestia personas en movimiento']},...
        {0.05,['\bf','ISO 2631-2 | Viviendas, vibración transitoria',newline,...
               'NTC-Met2017 | Molestia personas en reposo','\rm',newline,newline,...
               'ISO 2631-2 | Viviendas, vibración continua en el día']},...
        {0.045,''},...
        {0.015,'ISO 2631-2 | Viviendas, vibración continua en la noche'}},...
        {... % umbrales para confort por dosis
        {4,'DosisConfort','Dosis de vibración contra valores límite de confort',ranC.Sec(seccion).caso(caso).confDosis},...
        {0.8,'Casas | Noche'},...
        {1.6,['\bf','Casas | Dia','\rm']},...
        {3.2,'Oficinas | Dia'},...
        {6.4,'Talleres | Dia'},...
        }};
    
    for ifig = grupos
        handfigura = 92740+ifig;
        han = figure(handfigura);
        prefijo = '';
        if not(ranC.Sec(seccion).ready)
             prefijo = '\bf PRELIMINAR \rm ';
        end
        title({[prefijo,'Sección ',num2str(seccion)],...
            caso_grupo_nom,...
            limit{ifig}{1}{3}})
        xlabel('Distancia desde frente de trabajo [m]')
        ylabel([unidadtxt,' de partícula en ', unidades])
        
        ca=gca; hold on
        ca.set('OuterPosition',[0,0,0.7,1])
        
        li = ca.XLim;
        for kk = 2:limit{ifig}{1}{1}+1
            hh =  plot(li,[limit{ifig}{kk}{1},limit{ifig}{kk}{1}],'DisplayName',limit{ifig}{kk}{2});
            set(get(get(hh,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            text(310,limit{ifig}{kk}{1},limit{ifig}{kk}{2})
        end
        
        % % Puntos de vibracion medida con hincado de pilotes
        if tipo == 'v' && caso <= 5
            if exist('puntosPilote','var') == 1
            for kk = 1:size(puntosPilote,2)
                hh = plot(puntosPilote{kk}{1},puntosPilote{kk}{2},'*r','DisplayName','Mediciones de campo');
                if kk>1
                    set(get(get(hh,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                end
            end    
            end
        end
        % % Puntos de vibracion medida con tablestacado
        if tipo == 'v' && caso >= 6
            if exist('puntos','var') == 1
            for kk = 1:size(puntos,2)
                hh = plot(puntos{kk}{1},puntos{kk}{2},'*r','DisplayName','Mediciones de campo');
                if kk>1
                    set(get(get(hh,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                end
            end
            end
        end
        if tipo == 'v'
            y0=0.9189; yf=168.1860;
            y0=0.5
            ypatch = [y0,y0,yf,yf];
        elseif tipo == 'a' && contains('n',subtipo)
            y0=0.001; yf=0.7;
            ypatch = [y0,y0,yf,yf];
        else
            y0=0.01; yf=20;
            ypatch = [y0,y0,yf,yf];
        end
        ylim([y0,yf])
        % sombreado de primera y ultima casa
        if caso==5 || caso==7
            h_patch_casas = patch([rCasa1,limit{ifig}{1}{4},limit{ifig}{1}{4},rCasa1],...
                ypatch,'yellow',...
                'DisplayName','Rango afectación en casas',...
                'EdgeColor','none','FaceAlpha',0.4);
            uistack(h_patch_casas,'bottom')
            hh = plot([rCasa1,rCasa1],[y0,yf],'r--','DisplayName','Primera casa');
            uistack(hh,'bottom')
        end
        legend
        set(gca, 'YScale', 'log')
        set(gca, 'XGrid', 'on')
        p = [1 1 1141 874];
        set(handfigura,'Position',p)
        name = ['/home/marcialcz/Documents/DOC/DOC/ibem_dwn_matlab/benchmarks/tables/fig_Sec',...
            num2str(seccion),'_',num2str(igraf),'_C',erase(num2str(casos),' '),'_',limit{ifig}{1}{2}];
%         savefig(han,[name,'.fig']) % <<--- activar cuando ranC ya tiene
                                    %         los valores correctos
        saveas(han,[name,'.png'])
        if pausarcadafig == true
            pause; % <--- se agrego para permitir registrar en ranC el valor de la interseccion.
        end
    end
    %
end % igraf
end % correr_configuraciones
end % seccion
close all