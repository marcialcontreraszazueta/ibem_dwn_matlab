function batchGenerarEjercicio000
% FunciOn de Green en semiespacio.

X  = [ 0.7575,    0.4692,    0.4226];
Xi = [ 0.4351,    0.7777,    0.4228];
% En m=1

V = [-0.7905,   -0.4530,   -0.4122];

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

% med = 1;
% % para.tipoMed(med) = 1; % HomogEneo / Espacio completo
% para.reg(med).rho = 1000;
% para.reg(med).alpha = 2000;
% para.reg(med).bet = 10000;
% para.reg(med).qd= 10000;
% 
% for i=1:para.nmed % LamE
%   para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
%   para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
%   para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
%   para.reg(i).tipoatts=1; % Q
% end

med = 1;
para.tipoMed(med) = 2;   % Estratificado
% Propiedades submedio 1 del medio 1
para.reg(med).nsubmed = 1;
 i = 1;
para.reg(med).sub(i).rho      = 1;
para.reg(med).sub(i).alpha    = 1.73205;
para.reg(med).sub(i).bet      = 1;
para.reg(med).sub(i).qd       = 10000;
para.reg(med).sub(i).tipoatts = 1; % Q

for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
  para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
end
para.DWNxl = 1000; % periodicidad de la fuente ( L )

clear med i
%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med
%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales
n=1;
para.ninc = n*3; % cantida de fuentes * 3 direcciones
para.tipo_onda=1; % no se usa

para.xs= repmat(Xi(1),3,1);
para.ys= repmat(Xi(2),3,1);
para.zs= repmat(Xi(3),3,1);
% direcci?n +x
para.gam(1:n) = 90;
para.phi(1:n) = 0;
% direcci?n +y
para.gam(n+1:2*n) = 90;
para.phi(n+1:2*n) = 90;
% direcci?n +z
para.gam(2*n+1:3*n) = 180;
para.phi(2*n+1:3*n) = 0;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;
para.rec.nrecx= 1; % cantidad de receptores

para.rec.xr= X(1);
para.rec.yr= X(2);
para.rec.zr= X(3);
%% Analisis
para.npplo	 = 10;
para.fmax	   = 0.43301;
para.nf      = 200;
para.jini    = 100;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
% para.tmaxinteres = min(20,para.tmax);
para.tmaxinteres = para.tmax;
%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 1;
%contrainte
para.sortie.sxx =1;
para.sortie.syy =1;
para.sortie.szz =1;
para.sortie.sxy =1;
para.sortie.sxz =1;
para.sortie.syz =1;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

% ver dibujo:
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);

% disp(squeeze(R.uw(101,1,1:3,1:3)))

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 

%% tracciones y desplazamientos
T=zeros(3,3);
%Ti1
j=1;
sw = squeeze(R.sw(101,1,j,:));
T(1,j) = sw(1)*V(1) + sw(4)*V(2) + sw(5)*V(3); % xx xy xz
T(2,j) = sw(4)*V(1) + sw(2)*V(2) + sw(6)*V(3); % xy yy yz
T(3,j) = sw(5)*V(1) + sw(6)*V(2) + sw(3)*V(3); % xz yz zz


%Ti2
j=2;
sw = squeeze(R.sw(101,1,j,:));
T(1,j) = sw(1)*V(1) + sw(4)*V(2) + sw(5)*V(3); % xx xy xz
T(2,j) = sw(4)*V(1) + sw(2)*V(2) + sw(6)*V(3); % xy yy yz
T(3,j) = sw(5)*V(1) + sw(6)*V(2) + sw(3)*V(3); % xz yz zz


%Ti3
j=3;
sw = squeeze(R.sw(101,1,j,:));
T(1,j) = sw(1)*V(1) + sw(4)*V(2) + sw(5)*V(3); % xx xy xz
T(2,j) = sw(4)*V(1) + sw(2)*V(2) + sw(6)*V(3); % xy yy yz
T(3,j) = sw(5)*V(1) + sw(6)*V(2) + sw(3)*V(3); % xz yz zz

disp('Tracciones')
disp(T)

disp('Desp')
disp(squeeze(R.uw(101,1,:,:)));
end