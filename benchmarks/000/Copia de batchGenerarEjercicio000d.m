function batchGenerarEjercicio000d
% FunciOn de Green para comparar con resulados de DG por Josue

% Sismogramas sinteticos de los tres componentes de desplazamiento en 
% el receptor X dada una fuerza en Xi
% Una capa sobre semiespacio

% fuente
Xi = [0,    0,    1];

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 1000;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.2;
para.reg(med).sub(i).alpha    = 2;
para.reg(med).sub(i).bet      = 1;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 2; %espesor

i = 2;
para.reg(med).sub(i).rho      = 2.2;
para.reg(med).sub(i).alpha    = 4;
para.reg(med).sub(i).bet      = 2;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
  para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
end
clear med i
%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med
%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales

% todo el tensor
n=1;
para.ninc = n*3; % cantida de fuentes * 3 direcciones
para.tipo_onda=1; % no se usa

para.xs= repmat(Xi(1),3,1);
para.ys= repmat(Xi(2),3,1);
para.zs= repmat(Xi(3),3,1);
% direcci?n +x
para.gam(1:n) = 90;
para.phi(1:n) = 0;
% direcci?n +y
para.gam(n+1:2*n) = 90;
para.phi(n+1:2*n) = 90;
% direcci?n +z
para.gam(2*n+1:3*n) = 180;
para.phi(2*n+1:3*n) = 0;

% una sola fuerza:
% n=1;
% para.ninc = n; % cantida de fuentes
% para.tipo_onda=1; % no se usa
% 
% para.xs= Xi(1);
% para.ys= Xi(2);
% para.zs= Xi(3);
% % direcci?n +z
% para.gam(1) = 180;
% para.phi(1) = 0;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% para.rec.nrecx= 1; % cantidad de receptores
% nr = 1;
% para.rec.xr= X(1);
% para.rec.yr= X(2);
% para.rec.zr= X(3);

 % linea de receptores:
  nr = 23;
  para.rec.xr= linspace(-11,11,nr).';
  para.rec.yr= ones(nr,1)*2;
  para.rec.zr= zeros(nr,1);
%   para.rec.medi = [ones(11,1);2*ones(39,1);ones(11,1);ones(11,1);2*ones(39,1);ones(11,1);];
  para.rec.nrecx= nr; % cantidad de receptores
  
%% Analisis
para.npplo	 = 6;
para.fmax	   = 8;
para.nf      = 2048;
para.jini    = 0;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
load handel; sound(y,Fs)    % sonido triunfal

uw = R.uw; sw = R.sw;
% disp(squeeze(R.uw(101,1,1:3,1:3)))

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 

%% funcion de amplitud
% para.pulso.tipo=3; %Ricker periodo caracter?stico tp
% para.pulso.a = 3;   % tp
% para.pulso.b = 6;    % ts
% para.pulso.c = -10;  % corrimiento

para.pulso.tipo=5; % gaussiano rise time
para.pulso.a = 0.5;   % rise time 
para.pulso.b = 1;   % retraso

% para.pulso.tipo=4; % plano + tapper gaussiano
% para.pulso.a = 2;   % rise time
% para.pulso.b = 10;    % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);
%
 % inversion
[utc,~]   = inversion_w(uw,sw,para);

% un receptor
iinc = 1;
recep = 1;
tran = 1:16000;

figure(9800);
tx = [num2str(recep),':(',num2str(para.rec.xr(recep)),' , ',...
  num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
set(gcf,'name',tx)
arnam{1} = ['u_',tx]; arnam{2} = ['v_',tx]; arnam{3} = ['w_',tx];
subplot(2,1,1); hold on
for icomp = 1:3
  plot(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',arnam{icomp});
  plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
  plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
  plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
end
title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
subplot(2,1,2); hold on
for icomp = 1:3
  plot(tps(tran),utc(tran,recep,iinc,icomp),'k-','DisplayName',arnam{icomp});
end
title('sismogramas sinteticos');xlabel('tiempo [segundos]');ylabel('amplitud')
xlim([0 15])
% %% sabana
% % resultados en  x
% ran = 2:31;
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
% 
% for icomp = 1:3
% figure(9800+icomp); clf; axes; hold on
% sca = 5 * para.rec.dxr / max(max(utc(tran,ran,iinc,icomp)));
% for ir = ran
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-');
% %   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
% end
% end

% %% sabana
% % resultados en  y
% ran = 62:122;
% para.rec.dyr = para.rec.yr(ran(2))-para.rec.yr(ran(1));
% 
% for icomp = 3
% figure(9800+icomp); clf; axes; hold on
% sca = 10 * para.rec.dyr / max(max(utc(tran,ran,iinc,icomp)));
% for ir = ran
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.yr(ir),'k-');
% end
% end
end