function batchGenerarEjercicio000e
% FunciOn de Green en medio estratificado

% Sismogramas sinteticos de los tres componentes de desplazamiento en 
% una linea de receptores dada una fuerza puntual en el origen
% para este ejercicio unidades en [m, m/s, ton/m^3]

% fuente en el origen
Xi = [0,0,25];

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^11; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 1000;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.0;
para.reg(med).sub(i).alpha    = 3.0;
para.reg(med).sub(i).bet      = 1.0;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 20; %espesor

i = i+1;
para.reg(med).sub(i).rho      = 2.5;
para.reg(med).sub(i).alpha    = 6.0;
para.reg(med).sub(i).bet      = 3.0;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
clear med i
%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med
%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales

% todo el tensor
n=1;
para.ninc = n*3; % cantida de fuentes * 3 direcciones
para.tipo_onda=1; % no se usa

para.xs= repmat(Xi(1),3,1);
para.ys= repmat(Xi(2),3,1);
para.zs= repmat(Xi(3),3,1);
% direcci?n +x
para.gam(1:n) = 90;
para.phi(1:n) = 0;
% direcci?n +y
para.gam(n+1:2*n) = 90;
para.phi(n+1:2*n) = 90;
% direcci?n +z
para.gam(2*n+1:3*n) = 180;
para.phi(2*n+1:3*n) = 0;

% una sola fuerza:
% n=1;
% para.ninc = n; % cantida de fuentes
% para.tipo_onda=1; % no se usa
% 
% para.xs= Xi(1);
% para.ys= Xi(2);
% para.zs= Xi(3);
% % direcci?n +z
% para.gam(1) = 180;
% para.phi(1) = 0;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% receptor donde esta la fuerza
% nr = 1;
% para.rec.xr= Xi(1);
% para.rec.yr= Xi(2);
% para.rec.zr= Xi(3);

 % linea de receptores:
  nr = 46;
  r = linspace(0,450,nr).'; th = 30*pi/180;
  para.rec.xr= r*cos(th);
  para.rec.yr= r*sin(th);
  para.rec.zr= zeros(nr,1); 
  
%   para.rec.medi = [ones(11,1);2*ones(39,1);ones(11,1);ones(11,1);2*ones(39,1);ones(11,1);];
  para.rec.nrecx= nr; % cantidad de receptores
  
%% Analisis
para.npplo	 = 6;
para.fmax	   = 2.0;
para.nf      = 512;
para.jini    = 0;  % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = 256; %0; % hasta (nf/2+1)=(nf/2)*df


para.fac_omei_DWN = 2;

para.tmax = (para.zeropad)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = 1*para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

% % para espaciar las frecuencias como logaritmo
% para.jvec(1) = para.jini;
% jj = 2;
% for j=para.jini:para.jfin
%   if j*df >= 1.03*para.jvec(jj-1)*df % <-- empirico
%     para.jvec(jj) = j;
%     jj = jj+1;
%   end
% end
% disp(['Se van a calcular ',num2str(jj-1),' frecuencias'])
% disp('--')
% disp(para.jvec)
% disp('--')


%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
dibujo_estratifi(para,[],0);
BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
%load handel; sound(y,Fs)    % sonido triunfal
%quit

uw = R.uw; sw = R.sw;
% disp(squeeze(R.uw(101,1,1:3,1:3)))

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 

% %% grafica H/V
% ImGiiXX = imag(uw);
% df      = para.fmax/(para.nf/2);
% Fq = para.jvec*df;
% % para cada receptor
% h1 = figure;
% for ir = 1
% %   iinc = (5+ir*3-2):(5+ir*3); 
% 
% figure(h1);
% % HV = sqrt((-2*squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1)))./...
% %               squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
% HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,1,1))...
%            -squeeze(ImGiiXX(para.jvec+1,ir,2,2)))./...
%             squeeze(ImGiiXX(para.jvec+1,ir,3,3)));
% loglog(Fq,abs(HV),'k-','DisplayName',...
%   [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
% hold on
% 
% end
% %xlim([0.1 2])
% % ylim([0.6 20]) % caso A
% %ylim([0.2 5]) % caso B
% grid on
% xlabel('Frecuencia (Hertz)')
% ylabel('Amplitud de H/V')
% set(gca,'FontName','arial')
% set(gca,'FontSize',20)
% set(gca,'GridLineStyle','-')
% set(gca,'GridAlpha',0.55)
% set(gca,'MinorGridLineStyle','-')
% set(gca,'MinorGridAlpha',0.25)
% disp('done')
% set(0,'DefaultFigureWindowStyle','normal')
% p = [31   205   404   454];
% set(gcf,'Position',p)
% 
% return

%% funcion de amplitud
para.pulso.tipo=3; %Ricker periodo caracter?stico tp
para.pulso.a = 2;   % tp
para.pulso.b = 3;    % ts
para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=5; % gaussiano rise time
% para.pulso.a = 0.5;   % rise time 
% para.pulso.b = 1;   % retraso

% para.pulso.tipo=4; % plano + tapper gaussiano
% para.pulso.a = 2;   % rise time
% para.pulso.b = 10;    % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);

%% Derivar para obtener velocidades
% w_vec   = Fq.'*(2*pi);
% uwv=zeros(size(uw));
% for ii2=1:size(uw,2)
%   for ii3=1:size(uw,3)
%     for ii4=1:size(uw,4);
% uwv(:,ii2,ii3,ii4) = w_vec.*uw(:,ii2,ii3,ii4); 
%     end
%   end
% end
% % grafica
% 
% iinc = 1;
% recep = 30;
% 
% figure(9800);
% tx = [num2str(recep),':(',num2str(para.rec.xr(recep)),' , ',...
%   num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
% set(gcf,'name',tx)
% arnam{1} = ['u_',tx]; arnam{2} = ['v_',tx]; arnam{3} = ['w_',tx];
% axes; hold on
% for icomp = 1:3
%   semilogy(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',arnam{icomp});
%   %plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
%   %plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
%   %plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
%   
%   semilogy(Fq,abs(uwv(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',['V' arnam{icomp}]);
%   %plot(Fq,abs(uwv(:,recep,iinc,icomp)),'r.','DisplayName',['V' arnam{icomp}]);
% end
% title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
% 
% disp('done')
% uw = uwv;

%% inversion
[utc,~]   = inversion_w(uw,sw,para);
disp('done')

%% sabana


% resultados en  x
iinc = 3; % fuerza vertical
namf={'x','y','z'};
tran = 1:find(tps>120,1); % sec
ran = 1:para.rec.nrecx;
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));

for icomp = 3 %    1:3
tx = ['U',namf{icomp},'| F',namf{iinc},':(',num2str([para.xs(iinc) para.ys(iinc) para.zs(iinc)]),')'];
figure; hold on; set(gcf,'name',tx)
sca = 4.3955e+09;%10 * para.rec.dxr / max(max(utc(tran,ran,iinc,icomp)));
for ir = ran
  plot(tps(tran) ,...- para.rec.xr(ir)/6000, ... % Reducir con una velociad de 6 km/s
    utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-');
%    plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
end
ylabel('x (m)')
xlabel('tiempo')
ylim([0 para.rec.xr(ran(end))]) % receptores
% xlim([0 10]) % tiempo
% view(90,-90) % t en vetical, x en horizontal
end

end