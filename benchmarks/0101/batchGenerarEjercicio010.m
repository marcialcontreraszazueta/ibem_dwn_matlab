function batchGenerarEjercicio010
% Valle PLATO, DWN y DWN con estratos de Cruz-Atienza 2016
% Incidencia fuerza puntual vertical en varios puntos, para H/V
% [Kilometro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=0; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios
%% Propiedades del medio 1 (semiespacio)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 100;     % periodicidad de la fuente 

i = 1; %5 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.2;
para.reg(med).sub(i).alpha    = 2.700;
para.reg(med).sub(i).bet      = 1.560;
para.reg(med).sub(i).qd       = 156;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 1.420; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

warning('a fuerza sin amortiguamiento')
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).qd = 10000; 
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
%% Propiedades del medio 2 (valle)
med = 2;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 100;     % periodicidad de la fuente ( L ) >= 200

i = 1; %1 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2;
% para.reg(med).sub(i).alpha    = 0.800;
% para.reg(med).sub(i).bet      = 0.050;
% para.reg(med).sub(i).qd       = 15;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.030; %espesor
%  
% i = i+1; %2 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2;
% para.reg(med).sub(i).alpha    = 1.200;
% para.reg(med).sub(i).bet      = 0.100;
% para.reg(med).sub(i).qd       = 30;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.020; %espesor
% 
% i = i+1; %3 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.05;
para.reg(med).sub(i).alpha    = 2.000;
para.reg(med).sub(i).bet      = 0.400;
para.reg(med).sub(i).qd       = 40;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.250; %espesor caso A
para.reg(med).sub(i).h        = 0.300; %espesor caso B

i = i+1; %4 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.05;
para.reg(med).sub(i).alpha    = 2.500;
para.reg(med).sub(i).bet      = 0.800;
para.reg(med).sub(i).qd       = 80;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.250; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

warning('a fuerza sin amortiguamiento')
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).qd = 10000; 
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

clear med i
%% GeometrIA
% nota: Las normales hacia afuera

para.geo = 4;

% --------------------------------------------------------
med=1;
para.cont(med,1).NumPieces = 0;
med = 2;  
para.cont(med,1).piece=cell(1); 
i = 1;
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/gauCurvSway.stl']; %+z
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=1;
para.cont(med,1).piece{i,1}.ColorIndex=1; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       region el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuentes

% 1_OndasPlanas  2_FuerzaPuntuales
i=0;
%% onda plana P vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=1; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=   0;
para.phi(i)=   0;
%% onda plana S dir x vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=  0;
para.phi(i)=  0;
%% onda plana S dir y vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=3; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)= 0; 
para.phi(i)= 0;
%% Fuerza vertical en (-3,0,0)
i=i+1;   
para.fuente(i)=2; % FuerzaPuntual
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   -3.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% Fuerza vertical en (0,3,0)
i=i+1;   
para.fuente(i)=2; % FuerzaPuntual
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   3.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% Las siguientes son para HVSR, vienen en grupitos de 3
%% grupo (a) x= -0.75, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.75;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (b) x= -0.60, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.60;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (c) x= -0.45, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.45;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (d) x= -0.30, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.30;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (e) x= -0.15, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.15;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (f) x=  0.00, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (g) x=  0.15, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.15;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (h) x=  0.30, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.30;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (i) x=  0.45, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.45;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (j) x=  0.60, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.60;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (k) x=  0.75, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.75;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (l) x=  0.00, y=0.15
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.15;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (m) x=  0.00, y=0.30
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.30;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (n) x=  0.00, y=0.45
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.45;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (o) x=  0.00, y=0.60
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.60;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (p) x=  0.00, y=0.75
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.75;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% ninc
para.ninc = i;
%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  
  para.rec.resatboundary=0;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  
  % receptores para el analisis de HVSR:
  para.rec.xr= [(-0.75:0.15:0.75).';zeros(5,1)];
  para.rec.yr= [zeros(11,1);(0.15:0.15:0.75).'];
  para.rec.zr= zeros(16,1);
  para.rec.nrecx=length(para.rec.xr);
   
%   % receptores para el analisis de HVSR vs func transf:
%   para.rec.xr= [(-0.75:0.15:0.75).';zeros(5,1);1;0];
%   para.rec.yr= [zeros(11,1);(0.15:0.15:0.75).';0;1];
%   para.rec.zr= zeros(16+2,1);
%   para.rec.nrecx=length(para.rec.xr);
  
%   % dos lineas de receptores:
%   nr = 202;
%   para.rec.xr= [linspace(-1.1,1.1,nr/2).' ; zeros(nr/2,1)];
%   para.rec.yr= [zeros(nr/2,1) ; linspace(-1.1,1.1,nr/2).'];
%   para.rec.zr= zeros(nr,1);
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores

%   receptores de referencia:
%   nr = 3;
%   para.rec.xr= [1;0;-1];
%   para.rec.yr= [0;1;0];
%   para.rec.zr= zeros(3,1);
%   para.rec.nrecx= length(para.rec.xr);

%   % malla plano xy
%   nrx = 40; nry = 40;
%   linx = linspace(-0.9,0.9,nrx);
%   liny = linspace(-0.9,0.9,nry);
%   [X,Y]=meshgrid(linx,liny);
%   xx = reshape(X,nrx*nry,1);
%   yy = reshape(Y,nrx*nry,1);
%   nr3 = nrx*nry;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;yy];
%   para.rec.zr= [para.rec.zr;xx*0];
%   para.rec.nrecx= para.rec.nrecx + nr3; % cantidad de receptores
%   nr = para.rec.nrecx;

  % malla plano xz
%   nrx = 9;  nrz = 12;
%   linx = linspace(-0.7,0.8,nrx);
%   linz = linspace(0,0.06,nrz);
%   [X,Z]=meshgrid(linx,linz);
%   xx = reshape(X,nrx*nrz,1);
%   zz = reshape(Z,nrx*nrz,1);
%   nr2 = nrx*nrz;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;xx*0];
%   para.rec.zr= [para.rec.zr;zz];
%   para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores
%   disp(para.rec.nrecx)
%% Analisis
para.loadme = false; %cargar los PHI ya calculados
para.com = false; % usar programa compilado para Aij/B 

para.npplo	 = 6;
para.fmax	   = 2;
para.nf      = 200;
para.jini    = 5; % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = 74; %0; % hasta (nf/2+1)=(nf/2)*df

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

% para espaciar las frecuencias como logaritmo
% para.jvec(1) = para.jini;
% jj = 2;
% for j=para.jini:para.jfin
%   if j*df >= 1.1111*para.jvec(jj-1)*df % <-- empirico
%     para.jvec(jj) = j;
%     jj = jj+1;
%   end
% end
% disp(['Se van a calcular ',num2str(jj-1),' frecuencias'])
para.jvec(1)=24;
para.jvec(2)=25;
disp(para.jvec)
% return
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')
%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
size(uw);

% load handel; sound(y,Fs)    % sonido triunfal
exit
%% cargar anteriores

% solo 16 puntos para HV
% modNam = '(A)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/16recep/modA/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170213T042846.mat');

% dos crecuencias extra para el modelo B: 24 y 25
% load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/16recep/modBextra/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170401T225446.mat');
uwextra = uw; clear uw
% modNam = '(B)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/16recep/modB/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170401T180929.mat');
% juntar:
uw(24+1,:,:,:) = uwextra(24+1,:,:,:);
uw(25+1,:,:,:) = uwextra(25+1,:,:,:);
jv = [para.jvec(1:12),24,25,para.jvec(13:end)];
para.jvec = jv;
clear uwextra
% malla de receptores
% modNam = '(A)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/enMallaEnSuperficie/modA/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170321T200303.mat');
% modNam = '(B)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/enMallaEnSuperficie/modB/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170322T143724.mat')

% pozos virtuales 3 + 9*30
% modNam = '(A)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/prof/modA/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170329T204544.mat');
% modNam = '(B)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/prof/modB/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170330T142219.mat');

% pozos virtuales 3 + 9*12
% modNam = '(A)';load('/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/prof/modA/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170331T020418.mat');
df      = para.fmax/(para.nf/2);
% 
%% grafica H/V
ImGiiXX = imag(uw);
df      = para.fmax/(para.nf/2);
Fq = para.jvec*df;
% para cada receptor
h1 = figure; clc
for ir = 1:16
%   iinc = (5+ir*3-2):(5+ir*3); 

figure(h1);
% HV = sqrt((-2*squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1)))./...
%               squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-2,1))...
           -squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-1,2)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-0,3)));
loglog(Fq,abs(HV),'k-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
hold on

end
xlim([0.1 2])
% ylim([0.6 20]) % caso A
ylim([0.2 5]) % caso B
grid on
xlabel('Frecuencia (Hertz)')
ylabel('Amplitud')
set(gca,'FontName','times')
set(gca,'FontSize',20)
set(gca,'GridLineStyle',':')
set(gca,'MinorGridAlpha',0.75)
disp('done')
set(0,'DefaultFigureWindowStyle','normal')
p = [31   205   404   454];
set(gcf,'Position',p)
%% graficar H/V overkill
ImGiiXX = imag(uw);
Fq = para.jvec*df;
% para cada receptor
% figure;set(gcf,'name','H/V');
h1 = figure; clc
h2 = figure; clc; 
for ir = 1:16
  iinc = (5+ir*3-2):(5+ir*3); 
  disp('---')
  disp([para.rec.xr(ir), para.rec.yr(ir), para.rec.zr(ir)])
  disp([para.xs(iinc).',para.ys(iinc).',para.zs(iinc).'])
  disp(para.gam(iinc))
  disp('---')
  
figure(h1);
% HV = sqrt((-2*squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1)))./...
%               squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-2,1))...
           -squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-1,2)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-0,3)));
loglog(Fq,abs(HV),'k-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
hold on

HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-1,2))...
           -squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-1,2)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-0,3)));
loglog(Fq,abs(HV),'r-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])


HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-2,1))...
           -squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-2,1)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,5+ir*3-0,3)));
loglog(Fq,abs(HV),'b-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])

xlim([0.1 5])
ylim([0.6 50])

figure(h2); hold on
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-2,1),'r','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-1,1),'r--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-0,1),'r--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-2,2),'g--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-1,2),'g','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-0,2),'g--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-2,3),'b--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-1,3),'b--')
plot(Fq,ImGiiXX(para.jvec+1,ir,5+ir*3-0,3),'b','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
end
grid on
xlabel('Frecuencia (Hertz)')
ylabel('Amplitud')
set(gca,'FontName','times')
set(gca,'FontSize',20)
set(gca,'GridLineStyle',':')
set(gca,'MinorGridAlpha',0.75)
disp('done')

p = [31   205   404   454];
set(gcf,'Position',p)
%% Func transf puntos del HV
% los receptores 17 y 18 estan fuera del valle y se usan como referencia
iinc = 3;
icomp = 1;


namic = {'u','v','w'};
namii = {'P','Sx','Sy','Fz(3,0)','Fz(0,3)'};
figure; set(gcf,'Name',['18. Cocientes componente ',namic{icomp},', incidencia ',namii{iinc}]);
Fq = para.jvec*df;
for ir = 4:16+4
%   ft = uw(para.jvec+1,ir,iinc,icomp) / uw(para.jvec+1,17,iinc,icomp);
  ft = uw(para.jvec+1,ir,iinc,icomp) / uw(para.jvec+1,18,iinc,icomp);
  semilogx(Fq,abs(ft),'k-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
xlim([0.1 2])
hold on
end
grid on
p = [0.1300    0.1491    0.7750    0.7759];
set(gca,'Position',p)
set(gca,'FontName','times')
set(gca,'FontSize',20)
xlabel('Frecuencia (Hertz)')
ylabel('Amplitud')
%% malla de funciones de transferencia
iinc = 1;
iref = 1;

doU = false;
doAz = false;
doHg = true;
doHV = false;

% 3 receptores de referencia + 
nrx = 40; nry = 40;
linx = linspace(-0.9,0.9,nrx);
liny = linspace(-0.9,0.9,nry);
[X,Y]=meshgrid(linx,liny);
ranrec = (1:40*40)+3;
% mamaiinc(1) = max(max(max(max(abs(uw(para.jvec+1,ranrec,iinc,1)) ./ abs(uw(para.jvec+1,ranrec,iinc,1))))))*2;
% mamaiinc(2) = max(max(max(max(abs(uw(para.jvec+1,ranrec,iinc,2)) ./ abs(uw(para.jvec+1,ranrec,iinc,2))))))*2;
% mamaiinc(3) = max(max(max(max(abs(uw(para.jvec+1,ranrec,iinc,3)) ./ abs(uw(para.jvec+1,ranrec,iinc,3))))))*2;

namic = {'u','v','w'};
namii = {'P','Sx','Sy','Fz(3,0)','Fz(0,3)'}; 
if iinc < 6
  nana = namii{iinc};
else
  nana = ['(',num2str(para.rec.xr(iinc),3),',',num2str(para.rec.yr(iinc),3),')'];
end
% nanaref = ['(',num2str(para.rec.xr(iref),3),',',num2str(para.rec.yr(iref),3),')'];
df      = para.fmax/(para.nf/2);

colormap bone
cm = colormap;
cm = cm(end:-1:1,:);
close(gcf);

% for ij = %[14,16,18,20,23,26]%para.jvec caso A
% for ij = [23,26,29,33,37,42,47,53,59,66]%para.jvec caso B
for ij = 16
if  doU
for icomp = 1:3
% disp(['F = ',num2str(ij*df),' Hertz'])

% Fq = para.jvec*df;
ft = abs(uw(ij+1,ranrec,iinc,icomp) / mean(uw(ij+1,iref,iinc,icomp)));
ftM = reshape(ft,nrx,nry);

% compresion de imagen
% p1 = 0.01;
% ftM = log(1 + exp(p1)*abs(ftM)) / log(1 + exp(p1));
% ftM = ftM / max(max(ftM));

% interpolar en el espacio
nnn = nrx*2;
for ismoo = 1 % vueltas
linx = linspace(-0.9,0.9,nnn);
liny = linspace(-0.9,0.9,nnn);
[Xq,Yq]=meshgrid(linx,liny);
ftM = interp2(X,Y,ftM,Xq,Yq,'cubic');
Xi = Xq; Yi = Yq;
nnn = nnn*2;
end

figure; 
set(gcf,'Name',...
  ['Cocientes componente ',namic{icomp},...
  ', incidencia ',nana, ...
  num2str(ij*df),' Hertz']);
h=surf(Xi,Yi,ftM); set(h,'EdgeColor','none','FaceColor','flat')
% [C,h]=contour(Xi,Yi,ftM); clabel(C,h)
view([0 90]);xlabel('x'),ylabel('y');axis tight;axis square
%nam = ['| ',namic{icomp},'(0,0) / ',namic{icomp},nanaref,' | = ',num2str(ft(783),4)];
nam = ['| ',namic{icomp},'/',namic{icomp},'_g|   ',num2str(ij*df,4),' Hz'];
title(nam)
nam = [nana,'_j',num2str(ij),'_',namic{icomp}];
hold on;plot3(0,0,100,'k.')

mxx = max(max(ft));
if mxx > 40
% saturar escala de amplitud
% mx = 0.5*max(max(ft)); disp(mx)
caxis([0 40])
end

% caxis([-mamaiinc(icomp) mamaiinc(icomp)])
colorbar

% colormap bone
% cm = colormap;
% cm = cm(end:-1:1,:);
colormap(cm)

set(gca,'FontName','times')
set(gca,'FontSize',22)
% print('-dpng','-r300',['../out/fig8_',nam,'B.png']);% resolcion
saveas(gcf,['../out/fig8_',nam,modNam,'.png'])
close(gcf);
end
end

if doAz
% azimut
az = atan2(real(uw(ij+1,ranrec,iinc,2)),real(uw(ij+1,ranrec,iinc,1)));
ftM = reshape(az,nrx,nry);
% interpolar en el espacio
% nnn = nrx*2;
% for ismoo = 1 % vueltas
% linx = linspace(-0.9,0.9,nnn);
% liny = linspace(-0.9,0.9,nnn);
% [Xq,Yq]=meshgrid(linx,liny);
% ftM = interp2(X,Y,ftM,Xq,Yq,'cubic');
% Xi = Xq; Yi = Yq;
% nnn = nnn*2;
% end
figure; 
set(gcf,'Name',...
  ['Azimut ',...
  ', incidencia ',nana, ...
  num2str(ij*df),' Hertz']);
h=surf(X,Y,ftM); set(h,'EdgeColor','none','FaceColor','flat')
% [C,h]=contour(Xi,Yi,ftM); clabel(C,h)
view([0 90]);xlabel('x'),ylabel('y');axis tight;axis square
%nam = ['| ',namic{icomp},'(0,0) / ',namic{icomp},nanaref,' | = ',num2str(ft(783),4)];
nam = [' azimut ',num2str(ij*df,4),' Hz'];
title(nam)
nam = [nana,'_j',num2str(ij)];
hold on;plot3(0,0,100,'k.')
caxis([-pi pi])
c =colorbar;
c.Location = 'south';
colormap(cm)
set(gca,'FontName','times')
set(gca,'FontSize',22)
% print('-dpng','-r300',['../out/fig8_',nam,'B.png']);% resolcion
%saveas(gcf,['../out/fig8_',nam,'_1az_',modNam,'.png'])
%close(gcf);
end

if doHg
% H/H_g
HH = sqrt(abs(uw(ij+1,ranrec,iinc,2)).^2+abs(uw(ij+1,ranrec,iinc,1)).^2);
HHref = sqrt(abs(mean(uw(ij+1,iref,iinc,2))).^2+abs(mean(uw(ij+1,iref,iinc,1))).^2);
HHg = HH./HHref;
ftM = reshape(HHg,nrx,nry);
% interpolar en el espacio
nnn = nrx*2;
for ismoo = 1 % vueltas
linx = linspace(-0.9,0.9,nnn);
liny = linspace(-0.9,0.9,nnn);
[Xq,Yq]=meshgrid(linx,liny);
ftM = interp2(X,Y,ftM,Xq,Yq,'cubic');
Xi = Xq; Yi = Yq;
nnn = nnn*2;
end
figure; 
set(gcf,'Name',...
  ['H/H_g ',...
  ', incidencia ',nana, ...
  num2str(ij*df),' Hertz']);
h=surf(Xi,Yi,ftM); set(h,'EdgeColor','none','FaceColor','flat')
% [C,h]=contour(Xi,Yi,ftM); clabel(C,h)
view([0 90]);xlabel('x'),ylabel('y');axis tight;axis square
%nam = ['| ',namic{icomp},'(0,0) / ',namic{icomp},nanaref,' | = ',num2str(ft(783),4)];
nam = ['| H/H_g|   ',num2str(ij*df,4),' Hz'];
title(nam)
nam = [nana,'_j',num2str(ij)];
hold on;plot3(0,0,100,'k.')
% caxis([-0.2 35])
caxis([-0.2 1.8])
c =colorbar;
% c.Location = 'south';
colormap(cm)
set(gca,'FontName','times')
set(gca,'FontSize',22)
% print('-dpng','-r300',['../out/fig8_',nam,'B.png']);% resolcion
saveas(gcf,['../out/fig8_',nam,'_2HHg_',modNam,'.png'])
close(gcf);
end

if doHV
% H/V
HH = sqrt(abs(uw(ij+1,ranrec,iinc,2)).^2+abs(uw(ij+1,ranrec,iinc,1)).^2);
VV = sqrt(2)*abs(uw(ij+1,ranrec,iinc,3)); 
HV = HH./VV;
ftM = reshape(HV,nrx,nry);
% interpolar en el espacio
nnn = nrx*3;
for ismoo = 1 % vueltas
linx = linspace(-0.9,0.9,nnn);
liny = linspace(-0.9,0.9,nnn);
[Xq,Yq]=meshgrid(linx,liny);
ftM = interp2(X,Y,ftM,Xq,Yq,'cubic');
Xi = Xq; Yi = Yq;
nnn = nnn*2;
end
figure; 
set(gcf,'Name',...
  ['H/V ',...
  ', incidencia ',nana, ...
  num2str(ij*df),' Hertz']);
h=surf(Xi,Yi,ftM); set(h,'EdgeColor','none','FaceColor','flat')
% [C,h]=contour(Xi,Yi,ftM); clabel(C,h)
view([0 90]);xlabel('x'),ylabel('y');axis tight;axis square
%nam = ['| ',namic{icomp},'(0,0) / ',namic{icomp},nanaref,' | = ',num2str(ft(783),4)];
nam = [' HVSR   ',num2str(ij*df,4),' Hz'];
title(nam)
nam = [nana,'_j',num2str(ij)];
hold on;plot3(0,0,100,'k.')
mxx = max(max(HV));
if mxx > 5
% saturar escala de amplitud
caxis([0 5])
end
% caxis([0 4])
c =colorbar;
% c.Location = 'south';
colormap(cm)
set(gca,'FontName','times')
set(gca,'FontSize',22)
% print('-dpng','-r300',['../out/fig8_',nam,'B.png']);% resolcion
saveas(gcf,['../out/fig8_',nam,'_3HV_',modNam,'.png'])
close(gcf);
end

end
%% grafica H/V sismos
Fq = para.jvec*df;
h1 = figure; clc
uwa = abs(uw);
sq2 = sqrt(2);
% iran = 1; %medio mal
% iran = 2:3; %muy mal
% iran = 4:5; % decente
% iran = 6:53; % tooodas las fuentes del hvsr campos difusos
% iran = [6:14,21:23,30:44,48:53]; % sin los que fallan
% iran = 4:53;
todosiran = 1:53;
for ir = 1:16 
  figure(h1);
  iran = todosiran;
  iran((5+ir*3-2):(5+ir*3))=[];
  HV = sqrt(squeeze(mean(uwa(para.jvec+1,ir,iran,1),3)).^2 ...
           +squeeze(mean(uwa(para.jvec+1,ir,iran,2),3)).^2)./...
       (sq2*squeeze(mean(uwa(para.jvec+1,ir,iran,3),3)));   
  loglog(Fq,HV,'k-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
  hold on
end
%
xlim([0.1 2])
ylim([0.6 20])
grid on
xlabel('Frecuencia (Hertz)')
ylabel('Amplitud')
set(gca,'FontName','times')
set(gca,'FontSize',20)
set(gca,'GridLineStyle',':')
set(gca,'MinorGridAlpha',0.75)
disp('done')
set(0,'DefaultFigureWindowStyle','normal')
p = [31   205   404   454];
set(gcf,'Position',p)
%% dispersion en malla de receptores
% definir el arreglo:
% iinc = 1; %P, Sx, Sy, FzX, FzY
iincNam = {'P,','Sx,', 'Sy,', 'FzX,', 'FzY,'};
iincNam = {'Avg'};

rlim = zeros(12,4);

% caso
t = 0;
t= t+1; rlim(t,:) = [-1.0  -0.5  -0.25 0.25]; str{t}= '(a)';%1
t= t+1; rlim(t,:) = [-0.75 -0.25 -0.25 0.25]; str{t}= '(b)';%2
t= t+1; rlim(t,:) = [-0.5   0    -0.25 0.25]; str{t}= '(c)';%3
t= t+1; rlim(t,:) = [-0.25  0.25 -0.25 0.25]; str{t}= '(d)';%4
t= t+1; rlim(t,:) = [0      0.5  -0.25 0.25]; str{t}= '(e)';%5
t= t+1; rlim(t,:) = [0.25   0.75 -0.25 0.25]; str{t}= '(f)';%6
t= t+1; rlim(t,:) = [0.5    1.0  -0.25 0.25]; str{t}= '(g)';%7
t= t+1; rlim(t,:) = [-0.25  0.25  0    0.5 ]; str{t}= '(h)';%8
t= t+1; rlim(t,:) = [-0.25  0.25  0.25 0.75]; str{t}= '(i)';%9
t= t+1; rlim(t,:) = [-0.25  0.25  0.5  1.0 ]; str{t}= '(j)';%10
t= t+1; rlim(t,:) = [-1.0   1.0  -0.25 0.25]; str{t}= '(k)';
t= t+1; rlim(t,:) = [-0.25  0.25 -1.00 1.00]; str{t}= '(l)';
disp('ready')
% relacion de dispersion
Fq = para.jvec*df;
% for caso = 1:10
if true
for iinc = 1%1%3%1:5

caso = 5;  casini = caso;
% nomb = [iincNam{iinc},str{caso},' [',num2str(rlim(caso,1)),'<x<',num2str(rlim(caso,2)),'] [',num2str(rlim(caso,3)),'<y<',num2str(rlim(caso,4)),']'];
ran1 = find(para.rec.xr >= rlim(caso,1) & para.rec.xr <= rlim(caso,2) & ...
           para.rec.yr >= rlim(caso,3) & para.rec.yr <= rlim(caso,4) & para.rec.medio == 2).';
% ran2=ran1; 
caso = 11;  casfin = caso;
ran2 = find(para.rec.xr >= rlim(caso,1) & para.rec.xr <= rlim(caso,2) & ...
           para.rec.yr >= rlim(caso,3) & para.rec.yr <= rlim(caso,4) & para.rec.medio == 2).';        
nomb = [str{casini} str{casfin}];

nnn = length(ran1)*length(ran2);
VecFq=zeros(1,nnn);
VecVL=zeros(1,nnn);
% VecAd=zeros(1,nnn);
jj = 0;
for i1 = ran1
  for i2 = ran2
    % verticales, Rayleigh
%     X = uw(para.jvec+1,i1,iinc,3); % cada inciencia por separado
%     Y = uw(para.jvec+1,i2,iinc,3); 
    
    % para tomar el prmedio de varias incidencias
    X = mean(squeeze(uw(para.jvec+1,i1,1:5,3)),2); 
    Y = mean(squeeze(uw(para.jvec+1,i2,1:5,3)),2); 
%     X = sum(squeeze(uw(para.jvec+1,i1,1:53,3)),2); % con las fuentes de adentro
%     Y = sum(squeeze(uw(para.jvec+1,i2,1:53,3)),2);
    
%     Th = atan2((para.rec.yr(i1)-para.rec.yr(i2)),(para.rec.xr(i1)-para.rec.xr(i2)))+pi/2;
%     X = mean(squeeze(uw(para.jvec+1,i1,iinc,1)*cos(Th)+uw(para.jvec+1,i1,iinc,2)*sin(Th)),2); % tangential Love
%     Y = mean(squeeze(uw(para.jvec+1,i2,iinc,1)*cos(Th)+uw(para.jvec+1,i2,iinc,2)*sin(Th)),2);
%     
    % distancia entre receptores
    d = sqrt((para.rec.xr(i2)-para.rec.xr(i1))^2 + ...
             (para.rec.yr(i2)-para.rec.yr(i1))^2 + ...
             (para.rec.zr(i2)-para.rec.zr(i1))^2); 
    
    % Libro de Kramer cap 6. pag 204
    % cross spectrum
    XY = X.' .* Y';
    % phase difference
    ph = atan2(imag(XY),real(XY));
    % phase velocity
    vl = (2*pi*Fq*d)./ph;
    % coherencia
%     vl = sqrt(real(XY).^2 + imag(XY).^2).^2./((X.'.*X').*(Y.'.*Y'));
%     vl = sqrt(real(XY).^2 + imag(XY).^2);
    
    
isok = find(~isnan(vl));

% acumular resultaado:
VecVL((jj+1):(jj)+length(vl(isok))) = vl(isok);
VecFq((jj+1):(jj)+length(vl(isok))) = Fq(isok);
    jj = jj+length(vl);
  end
end
disp('haciendo grafica')
%
% grafica colores
% rejilla contando cauntos son de cada grupo
velVec = linspace(0,2,1000);
M = zeros(length(velVec),length(Fq)); % vl
for ii = 1:size(M,2)
  VetosX = VecVL((VecFq == Fq(ii)));
  for jj = 1:size(M,1)-1
    estosZ = find(VetosX((velVec(jj) <= VetosX) & (velVec(jj+1) > VetosX)));
    M(jj,ii) = M(jj,ii) + length(estosZ);
  end
end

% compresion de amplitudes
p1 = 0.1;
Mc = log(1 + exp(p1)*abs(M)) / log(1 + exp(p1));
%M = Mc / max(max(Mc));
M = Mc / 10;


figure; 
h=surf(Fq,velVec,M); set(h,'EdgeColor','none'); view(0,90)
% h=contour(Fq,velVec,M);
% h=contourf(Fq,velVec,M,'EdgeColor','none'); 
%title(['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'])
title(nomb)
xlim([0.1 1.5]);ylim([velVec(1) velVec(end)])
set(gca,'XScale','log')
set(gca,'XTickMode','manual');
set(gca,'XTick',[0.1,0.3,0.5,0.9,1.5]);
set(gca,'XTickLabel',{'0.1';'0.3';'0.5';'0.9';'1,5'});

p = [3   309   400   380];
% p(1)=p(1) + iXran*100;
set(0,'DefaultFigureWindowStyle','normal')
set(gcf,'Position',p);
set(gca,'FontName','times')
set(gca,'FontSize',20)
xlabel('Frecuencia (Hertz)')
ylabel('Velocidad de fase (Km/s)')
colorbar
% caxis([0 1.5])
% set(gca,'Box','off')

set(gcf,'PaperPositionMode','auto') 
% print(['../out/fig8_vl',modNam,iincNam{iinc},str{caso}],'-dpng')
print(['../out/fig8_vl',modNam,iincNam{iinc},str{casini},str{casfin}],'-dpng')
close(gcf);


% figure; 
% h=surf(Fq,amdVec,Mad); set(h,'EdgeColor','none'); view(0,90)
% title(nomb)
% xlim([0.1 1.5]); 
% ylim([0 1])
% set(gca,'XScale','log')
% set(gca,'XTickMode','manual');
% set(gca,'XTick',[0.1,0.3,0.5,0.7,0.9,1.2,1.5]);
% set(gca,'XTickLabel',{'0.1';'0.3';'0.5';'0.7';'0.9';'1.2';'1,5'});
% 
% p = [3   309   400   380];
% % p(1)=p(1) + iXran*100;
% set(0,'DefaultFigureWindowStyle','normal')
% set(gcf,'Position',p);
% set(gca,'FontName','times')
% set(gca,'FontSize',20)
% xlabel('Frecuencia (Hertz)')
% ylabel('|Sxy|')
% colorbar
% % caxis([0 1])
% set(gca,'Box','off')
% 
% set(gcf,'PaperPositionMode','auto') 
% print(['../out/fig8_cro',modNam,iincNam{iinc},str{caso}],'-dpng')
% close(gcf);

end
end
%% pozos virtuales

iinc = 2;

mmmm = max(max(max(max(real(uw(para.jvec+1,4:end,iinc,1))))));

% sca = 0.25/mmmm; paso = 12;
sca = 1/mmmm; paso = 30;

ran = 3+1:3+paso;
open '/Users/marshall/Documents/DOC/unit9/012HVimGii/resultados/prof/base.fig'
hold on



% letrerito
% mod A
% text(-0.89,3,0.32,num2str(mmmm,3))
% plot3([-0.9 -0.80],[3 3],[0.35 0.35],'k-','LineWidth',5)
% plot3([-0.9 -0.85],[3 3],[0.37 0.37],'k-','LineWidth',5)
% plot3([-0.81 -0.8],[3 3],[0.37 0.37],'k-','LineWidth',5)
% plot3([-0.895 -0.80],[3 3],[0.38 0.38],'k-','LineWidth',1)
% mod B
% text(-0.89,3,0.07,num2str(mmmm,3))
% plot3([-0.9 -0.80],[3 3],[0.075 0.075],'k-','LineWidth',5)
% plot3([-0.9 -0.85],[3 3],[0.078 0.078],'k-','LineWidth',5)
% plot3([-0.81 -0.8],[3 3],[0.078 0.078],'k-','LineWidth',5)
% plot3([-0.895 -0.80],[3 3],[0.08 0.08],'k-','LineWidth',1)
% zlim([0 0.1])

%
ind=1;
for jj = para.jvec
  disp([ind, jj, jj*df])
  ind=ind+1;
end
%
for ix = 1:9
%   figure; title(num2str(ix)); hold on
  zvec = para.rec.zr(ran);
  xpozo = para.rec.xr(ran);
% curvas color negro
for jj = para.jvec(1:14)
  u = xpozo + real(uw(jj+1,ran,iinc,1)).'*sca;
%   v = real(uw(jj+1,ran,iinc,2));
  plot3(u,u.*0+2.9,zvec,'k-','DisplayName',num2str(jj),'LineWidth',0.5)
%   plot(v,zvec,'b-','DisplayName',num2str(jj))
end
col={'y-','c-','g-','b-','m-','r-'};ind=1;
for jj = para.jvec([15,16,18,19,20,22])
  u = xpozo + real(uw(jj+1,ran,iinc,1)).'*sca;
%   v = real(uw(jj+1,ran,iinc,2));
  plot3(u,u.*0+3,zvec,col{ind},'DisplayName',num2str(jj),'LineWidth',2)
%   plot(v,zvec,'b-','DisplayName',num2str(jj))
  ind=ind+1;
end 
set(gca,'YDir','reverse');
ran = ran + paso;
end
% raya blanca
if modNam == '(A)'
% mod A
plot3([-0.9 0.9],[2.9 2.9],[0.03 0.03],'w','LineWidth',2)
plot3([-0.9 0.9],[2.9 2.9],[0.05 0.05],'w','LineWidth',2)
plot3([-0.2 0.6],[2.9 2.9],[0.3 0.3],'w','LineWidth',2)
% zlim([0 0.06])
if paso == 12
  zlim([0 0.06])
  xlim([-1 1])
  set(gcf,'Position',[822   442   510   109])
else
  zlim([0.06 0.5])
  xlim([-0.9 1])
  set(gcf,'Position',[823   247   510   121])
end
else
% mod B
plot3([-0.2 0.6],[2.9 2.9],[0.3 0.3],'w','LineWidth',2)
end
end