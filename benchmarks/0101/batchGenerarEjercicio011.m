function batchGenerarEjercicio011
% Valle PLATO, DWN y DWN sin estratos
% Incidencia fuerza puntual vertical en varios puntos, para H/V
% [Kilometro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=0; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios

%% Propiedades del medio 1 (semiespacio)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 100;     % periodicidad de la fuente 

i = 1; %5 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.2;
para.reg(med).sub(i).alpha    = 2.700;
para.reg(med).sub(i).bet      = 1.560;
para.reg(med).sub(i).qd       = 156;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

warning('a fuerza sin amortiguamiento')
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).qd = 10000; 
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

%% Propiedades del medio 2 (valle)
med = 2;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 100;     % periodicidad de la fuente ( L ) >= 200

i = 1; %1 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.05;
para.reg(med).sub(i).alpha    = 2.133;
para.reg(med).sub(i).bet      = 0.552;
para.reg(med).sub(i).qd       = 56.5;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

warning('a fuerza sin amortiguamiento')
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).qd = 10000; 
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

clear med i
%% GeometrIA
% nota: Las normales hacia afuera

para.geo = 4;

% --------------------------------------------------------
med=1;
para.cont(med,1).NumPieces = 0;
med = 2;  
para.cont(med,1).piece=cell(1); 
i = 1;
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/gauCurvSway.stl']; %+z
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=1;
para.cont(med,1).piece{i,1}.ColorIndex=1; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       region el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuentes

% 1_OndasPlanas  2_FuerzaPuntuales
i=0;

%% onda plana P vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=1; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=   0;
para.phi(i)=   0;
%% onda plana S dir x vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=  0;
para.phi(i)=  0;
%% onda plana S dir y vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=3; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)= 0; 
para.phi(i)= 0;
%% Fuerza vertical en (-3,0,0)
i=i+1;   
para.fuente(i)=2; % FuerzaPuntual
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   -3.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% Fuerza vertical en (0,3,0)
i=i+1;   
para.fuente(i)=2; % FuerzaPuntual
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   3.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% Las siguientes son para HVSR, vienen en grupitos de 3
%% grupo (a) x= -0.75, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.75;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (b) x= -0.60, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.60;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (c) x= -0.45, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.45;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (d) x= -0.30, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.30;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (e) x= -0.15, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)= -0.15;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (f) x=  0.00, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (g) x=  0.15, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.15;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (h) x=  0.30, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.30;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (i) x=  0.45, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.45;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (j) x=  0.60, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.60;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (k) x=  0.75, y=0
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.75;
para.ys(i:i+2)=   0.0;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (l) x=  0.00, y=0.15
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.15;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (m) x=  0.00, y=0.30
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.30;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (n) x=  0.00, y=0.45
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.45;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (o) x=  0.00, y=0.60
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.60;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;
%% grupo (p) x=  0.00, y=0.75
i=i+1;   
para.fuente(i:i+2)=2; % FuerzaPuntual
para.tipo_onda(i:i+2)=2; % P, S, R
para.xs(i:i+2)=   0.0;
para.ys(i:i+2)=   0.75;
para.zs(i:i+2)=   0.0;
para.xzs(i:i+2)=  2;
para.gam(i)= 90; % direccion +x
para.phi(i)=  0;
i=i+1;   
para.gam(i)= 90; % direccion +y
para.phi(i)= 90;
i=i+1;   
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;

%% ninc
para.ninc = i;

%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  
  para.rec.resatboundary=0;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  
  % receptores para el analisis de HVSR:
  para.rec.xr= [(-0.75:0.15:0.75).';zeros(5,1)];
  para.rec.yr= [zeros(11,1);(0.15:0.15:0.75).'];
  para.rec.zr= zeros(16,1);
  para.rec.nrecx=16;
   
%   % dos lineas de receptores:
%   nr = 202;
%   para.rec.xr= [linspace(-1.1,1.1,nr/2).' ; zeros(nr/2,1)];
%   para.rec.yr= [zeros(nr/2,1) ; linspace(-1.1,1.1,nr/2).'];
%   para.rec.zr= zeros(nr,1);
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
%   % malla plano xz
%   nrx = 70;  nrz = 11;
%   linx = linspace(-0.9,0.9,nrx);
%   linz = linspace(0,0.05,nrz);
%   [X,Z]=meshgrid(linx,linz);
%   xx = reshape(X,nrx*nrz,1);
%   zz = reshape(Z,nrx*nrz,1);
%   nr2 = nrx*nrz;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;xx*0];
%   para.rec.zr= [para.rec.zr;zz];
%   para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores
%   disp(para.rec.nrecx)
  
%   % malla plano xy
%   nrx = 40; nry = 20;
%   linx = linspace(-0.6,0.6,nrx);
%   liny = linspace(0,0.6,nry);
%   [X,Y]=meshgrid(linx,liny);
%   xx = reshape(X,nrx*nry,1);
%   yy = reshape(Y,nrx*nry,1);
%   nr3 = nrx*nry;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;yy];
%   para.rec.zr= [para.rec.zr;xx*0];
%   para.rec.nrecx= para.rec.nrecx + nr3; % cantidad de receptores
  
%% Analisis
para.loadme = false; %cargar los PHI ya calculados
para.com = false; % usar Aij/B compilado

para.npplo	 = 6;
para.fmax	   = 3;
para.nf      = 300;
para.jini    = 5; % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = floor(para.nf/2); %0; % hasta (nf/2+1)=(nf/2)*df

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

% para espaciar las frecuencias como logaritmo
para.jvec(1) = para.jini;
jj = 2;
for j=para.jini:para.jfin
  if j*df >= 1.1111*para.jvec(jj-1)*df % <-- empirico
    para.jvec(jj) = j;
    jj = jj+1;
  end
end
disp(['Se van a calcular ',num2str(jj-1),' frecuencias'])
disp(para.jvec)

%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
size(uw);
disp('done')
% load handel; sound(y,Fs)    % sonido triunfal
exit

%% cargar resultados
load('../out/TexHomo/gen3D_OPOPOPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFPFP_xs0_zs0_npplo6_DWN_20170213T062451.mat');
df = para.fmax/(para.nf/2);  disp(['df = ',num2str(df)])
%% graficar
ImGiiXX = imag(uw);
Fq = para.jvec*df;
% para cada receptor
% figure;set(gcf,'name','H/V');
for ir = 1:16
% HV = sqrt((-2*squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1)))./...
%               squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
HV = sqrt(2*(-squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1)))./...
            squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
% HV = sqrt((-squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-2,1))...
%            -squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-1,2)))./...
%             squeeze(ImGiiXX(para.jvec+1,ir,5+3*ir-0,3)));
loglog(Fq,abs(HV),'r-','DisplayName',...
  [num2str(ir),' (',num2str(para.rec.xr(ir)),',',num2str(para.rec.yr(ir)),')'])
xlim([0.1 5])
% ylim([0.6 50])
hold on
end
grid on
disp('done')
end