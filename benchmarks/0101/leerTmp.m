%% leer datos
% clear
jtas = [5:81, 90, 95, 100]; %las siguiente frecuencia
para.fmax	   = 3;
para.nf      = 400;
nrecep = 10;
X = zeros(10,2); X(1:7,1) = -0.75:0.25:0.75; X(8:10,2) = 0.25:0.25:0.75;
df      = para.fmax/(para.nf/2);  disp(['df = ',num2str(df)]); para.df = df;
allda = cell(size(jtas,2),1);
count = 1;
cd '/Users/marshall/Documents/DOC/ibem_dwn_matlab/out'
for j = jtas
  if j < 10
    allda{count}=load(['tmp_00',num2str(j),'tmp.mat'],'uw');
  else
    if j < 100
      allda{count}=load(['tmp_0',num2str(j),'tmp.mat'],'uw');
    else
      allda{count}=load(['tmp_',num2str(j),'tmp.mat'],'uw');
    end
  end
  fj    = j*df;
  allda{count}.fj = fj;
  count = count+1;
end
cd '/Users/marshall/Documents/DOC/ibem_dwn_matlab/ibem_matlab'
Fq = jtas*df; Fq = Fq.';
clear count fj j jtas
%% para cada receptor, graficar H/V

  rvals = linspace(0,1,nrecep);
  bval = 0.2;
  gval = 0.8;
for ir = 1:nrecep
  fx = ir*3-2;
  fy = ir*3-1;
  fz = ir*3;
  nj = size(allda,1);
  ImG11=zeros(nj,1);
  ImG22=zeros(nj,1);
  ImG33=zeros(nj,1);
  for j = 1:nj
    ImG11(j) = (imag(allda{j}.uw(1,ir,fx,1)));
    ImG22(j) = (imag(allda{j}.uw(1,ir,fy,2)));
    ImG33(j) = (imag(allda{j}.uw(1,ir,fz,3)));
  end
  figure(6868);hold on; set(gcf,'name','ImGii(x,x)')
  plot(Fq,squeeze( ImG11),'r-','DisplayName',['ImG11ir_' num2str(ir)]);
  plot(Fq,squeeze( ImG22),'b-','DisplayName',['ImG22ir_' num2str(ir)]);
  plot(Fq,squeeze( ImG33),'k-','DisplayName',['ImG33ir_' num2str(ir)]);
  
  
  HV = sqrt((ImG11+ImG22)./ImG33);
  figure(5757); set(gcf,'name','H/V')
  loglog(Fq,abs(HV),'Color', [rvals(ir),bval,gval],...
    'DisplayName',['ir_' num2str(ir) '_(' num2str(X(ir,1)) ',' num2str(X(ir,2)) ')'])
  hold on;
  xlim([0.1 5])
  ylim([0.6 50])
  disp('done')
  grid on
end