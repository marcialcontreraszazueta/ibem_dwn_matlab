function batchGenerarEjercicio001
% Continuidad entre dos semiespacios
% en una frecuencia muy baja
% (func. analitica o DWN)

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios
% Propiedades del medio 1
med = 1;

% para.tipoMed(med) = 1; % HomogEneo / Espacio completo
% para.reg(med).rho = 1;
% para.reg(med).alpha = 1.8708;
% para.reg(med).bet = 1;
% para.reg(med).qd= 1000;

para.tipoMed(med) = 2;   % Estratificado
% Propiedades submedio 'i' del medio 'med'
 i = 1;
para.reg(med).sub(i).rho      = 1;
para.reg(med).sub(i).alpha    = 1.8708;%4;%1.73205;%4;
para.reg(med).sub(i).bet      = 1;%2.3094;
para.reg(med).sub(i).qd       = 10000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

%  i = 2;
% para.reg(med).sub(i).rho      = 1;
% para.reg(med).sub(i).alpha    = 1.8708;%4;%1.73205;%4;
% para.reg(med).sub(i).bet      = 1;%2.3094;
% para.reg(med).sub(i).qd       = 10000;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
  
para.DWNxl = 2000; % periodicidad de la fuente ( L )

med = 2;
para.tipoMed(med) = 1; % HomogEneo / Espacio completo
para.reg(med).rho = 1;
para.reg(med).alpha = 1.8708;
para.reg(med).bet = 1;
para.reg(med).qd= 10000;
for i=1:para.nmed % LamE
  if para.tipoMed(i) == 1
  para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
  para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
  para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
  para.reg(i).tipoatts=1; % Q
  end
end
clear med i
%% GeometrIA
para.geo = 4;

med = 1;  
para.cont(med,1).piece=cell(1); 
i = 1;
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/001_plano3n.stl'];
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=2;
para.cont(med,1).piece{i,1}.ColorIndex=1; % Azul
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

med = 2;
para.cont(med,1).NumPieces = 0;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       regi?n el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuente
para.fuente=1; % 1 Ondas Planas  2 Fuentes Puntuales
para.ninc     =1;
para.tipo_onda=1; % P, S, R
para.xs(1)= 0;%-1;
para.ys(1)= 0;%-1;
para.zs(1)= 0;% 1;
para.gam(1)= 0;
para.phi(1)= 0;
para.xzs(1) = 1;
%% Receptores
para.rec.resatboundary=0;
para.recpos =4;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;

  para.rec.nrecx = 6;
  para.rec.xr=[   0;   0.5;    0;     0;   0.5;     0];
  para.rec.yr=[   0;     0;  0.5;     0;     0;   0.5];
  para.rec.zr=[ 0.01; 0.01; 0.01; -0.01; -0.01; -0.01];
  para.rec.medi = [ones(3,1);2*ones(3,1)];
%% Analisis
para.npplo	 = 10;
para.fmax	   = 0.046771;
para.nf      = 200;
para.jini    = 100;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

% ver dibujo:
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);

disp([squeeze(R.uw(101,1,1,:)),squeeze(R.uw(101,2,1,:)),squeeze(R.uw(101,3,1,:))])
disp([squeeze(R.uw(101,4,1,:)),squeeze(R.uw(101,5,1,:)),squeeze(R.uw(101,6,1,:))])
end