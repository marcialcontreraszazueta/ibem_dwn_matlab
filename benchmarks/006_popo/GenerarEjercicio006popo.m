function GenerarEjercicio006popo
% MontaNa de forma del Popo y el Izta
%
% matlab -nojvm -nodesktop GenerarEjercicio006popo 
%
% Incidencia vertical onda S
% Solucion en 0.3,0.4,0.5,0.6,0.7 Hz
% Ejercicio con inclusion homogenea sobre el semiespacio

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd; pwd
para.nomrep=pwd;
cd ../../ibem_matlab;
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios
% Propiedades del medio 1
med = 1;

para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 200;     % periodicidad de la fuente ( L ) >= 200

% Propiedades submedio 'i' del medio 'med'
 i = 1;
para.reg(med).sub(i).rho      = 2.5; % ton/m3
para.reg(med).sub(i).alpha    = 2.5; % km/s
para.reg(med).sub(i).bet      = 1.0;
para.reg(med).sub(i).qd       = 10000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
% para.DWNxl = 2000; % periodicidad de la fuente ( L )


med = 2;
para.tipoMed(med) = 1; % HomogEneo / Espacio completo
para.reg(med).rho = 2.5;
para.reg(med).alpha = 2.5;
para.reg(med).bet = 1.0;
para.reg(med).qd= 10000;
for i=1:para.nmed % LamE
  if para.tipoMed(i) == 1
  para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
  para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
  para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
  para.reg(i).tipoatts=1; % Q
  end
end
clear med i
%% GeometrIA
% nota: Las normales hacia afuera

para.geo = 4;

med = 1;  
para.cont(med,1).piece=cell(1); 
i = 1;
% para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/topografia_tapa_trim250.stl'];
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/topografia_tapa_trim850_radio10.stl'];
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=2;
para.cont(med,1).piece{i,1}.ColorIndex=1; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

med = 2;
para.cont(med,1).piece=cell(1); 
i = 1;
% para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/topografia_montana_trim250.stl'];
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/topografia_montana_trim850_radio10.stl'];
para.cont(med,1).piece{i,1}.kind=1; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=0;
para.cont(med,1).piece{i,1}.ColorIndex=3; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       regi?n el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuente
i=0;
% onda plana S dir x vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=  0;
para.phi(i)=  0;
% onda plana S dir y vertical
i=i+1;   
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)= 0; 
para.phi(i)= 90;
% Fuerza vertical en (-3,0,0)
i=i+1;   
para.fuente(i)=2; % FuerzaPuntual
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   -60.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
para.gam(i)= 180; % direccion +z
para.phi(i)=   0;

para.ninc = i;

%% Receptores
para.rec.resatboundary=0;

% indat = importdata([para.nomrep '/Recep_trim750.txt']);
indat = importdata([para.nomrep '/Recep_trim850.txt']);
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;
para.rec.nrecx= size(indat,1); % cantidad de receptores
para.rec.xr= indat(:,1);
para.rec.yr= indat(:,2);
para.rec.zr= indat(:,3);
para.rec.medi = 2*ones(para.rec.nrecx,1);
para.rec.medi(1:6)=1;
para.rec.medi(14:19)=1;
para.rec.medi(30:32)=1;
para.rec.medi(43:45)=1;
%% Analisis
para.npplo	 = 5;
para.fmax	 = 0.3;
para.nf      = 2;
para.jini    = 1;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

% ver dibujo:
BatchScript([para.nomrep '/parameters.mat'],true); 
return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);
return
% Desplazamientos en seis puntos
% disp([squeeze(R.uw(101,41,1,:)),squeeze(R.uw(101,42,1,:)),squeeze(R.uw(101,43,1,:))])
% disp([squeeze(R.uw(101,44,1,:)),squeeze(R.uw(101,45,1,:)),squeeze(R.uw(101,46,1,:))])

% Grafica
%open([para.nomrep '/img.fig']); hold on;
%nrecep = para.rec.nrecx;
%nr = nrecep;
%xax = linspace(0,2.0,nr); j=100;
%plot(xax,squeeze(abs(R.uw(j+1,1:nr,1,1))),'r-','DisplayName','|Ux|');
% plot(xax,squeeze(abs(R.uw(j+1,1:nr,1,2))),'r-')
%plot(xax,squeeze(abs(R.uw(j+1,1:nr,1,3))),'r-','DisplayName','|Uz|')
end