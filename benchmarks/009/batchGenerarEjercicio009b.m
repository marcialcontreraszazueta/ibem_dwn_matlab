function batchGenerarEjercicio009b
% Valle PLATO, DWN y DWN   a) con estratos
% Incidencia onda P, SV, para gamma 0 y 30
% SoluciOn en eta = 2,4,6,8

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios

% Propiedades del medio 1 (semiespacio)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 1000;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.0;
para.reg(med).sub(i).alpha    = 8.3267;
para.reg(med).sub(i).bet      = 4;
para.reg(med).sub(i).qd       = 5000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% Propiedades del medio 2 (valle)
med = 2;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 1000;     % periodicidad de la fuente ( L ) >= 200

%a)
i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 0.8;
para.reg(med).sub(i).alpha    = 2.91;
para.reg(med).sub(i).bet      = 1.2;
para.reg(med).sub(i).qd       = 300;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

clear med i
%% GeometrIA
% nota: Las normales hacia afuera

para.geo = 4;

% --------------------------------------------------------
med=1;
para.cont(med,1).NumPieces = 0;
med = 2;  
para.cont(med,1).piece=cell(1); 
i = 1;
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/gauflat0_1.stl']; %+z
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=1;
para.cont(med,1).piece{i,1}.ColorIndex=1; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       region el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuente
para.fuente=1; % 1 Todas Ondas Planas  2 Todas Fuentes Puntuales
i = 0;

% i = i+1;
% para.tipo_onda(i)=1; % 1P, 2S
% para.xs(i)= 0;
% para.ys(i)= 0;
% para.zs(i)= 0;
% para.gam(i)= 0; % angulo desde la vertical
% para.phi(i)= 0;   % azimuth
% para.xzs(i) = 1;  % medio de la fuente
% 
% i = i+1;
% para.tipo_onda(i)=1; % 1P, 2S
% para.xs(i)= 0;
% para.ys(i)= 0;
% para.zs(i)= 0;
% para.gam(i)= 30; % angulo desde la vertical
% para.phi(i)= 0;   % azimuth
% para.xzs(i) = 1;  % medio de la fuente
% 
% i = i+1;
% para.tipo_onda(i)=2; % 1P, 2S
% para.xs(i)= 0;
% para.ys(i)= 0;
% para.zs(i)= 0;
% para.gam(i)= 0; % angulo desde la vertical
% para.phi(i)= 0;   % azimuth
% para.xzs(i) = 1;  % medio de la fuente

i = i+1;
para.tipo_onda(i)=2; % 1P, 2S
para.xs(i)= 0;
para.ys(i)= 0;
para.zs(i)= 0;
para.gam(i)= 30; % angulo desde la vertical
para.phi(i)= 0;   % azimuth
para.xzs(i) = 1;  % medio de la fuente

para.ninc=i;
%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  
  para.rec.resatboundary=0;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  
%   % dos lineas de receptores densas:
%   nr = 122;
%   para.rec.xr= [linspace(-1.5,1.5,nr/2).' ; zeros(nr/2,1)];
%   para.rec.yr= [zeros(nr/2,1) ; linspace(-1.5,1.5,nr/2).'];
%   para.rec.zr= zeros(nr,1);
%   para.rec.nrecx= nr; % cantidad de receptores
  
  % dos lineas cortitas pero tupidas
  nr = 1024;
  para.rec.xr= [linspace(-0.75,0.75,nr/2).' ; zeros(nr/2,1)];
  para.rec.yr= [zeros(nr/2,1) ; linspace(-0.75,0.75,nr/2).'];
  para.rec.zr= zeros(nr,1);
  para.rec.nrecx= nr; % cantidad de receptores
  
  
%   % malla plano xz
%   nrx = 70;  nrz = 11;
%   linx = linspace(-0.9,0.9,nrx);
%   linz = linspace(0,0.05,nrz);
%   [X,Z]=meshgrid(linx,linz);
%   xx = reshape(X,nrx*nrz,1);
%   zz = reshape(Z,nrx*nrz,1);
%   nr2 = nrx*nrz;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;xx*0];
%   para.rec.zr= [para.rec.zr;zz];
%   para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores
%   disp(para.rec.nrecx)
  
%   % malla plano xy
%   nrx = 50; nry = 25;
%   linx = linspace(-0.7,0.7,nrx);
%   liny = linspace(0,0.7,nry);
%   [X,Y]=meshgrid(linx,liny);
%   xx = reshape(X,nrx*nry,1);
%   yy = reshape(Y,nrx*nry,1);
%   nr3 = nrx*nry;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;yy];
%   para.rec.zr= [para.rec.zr;xx*0];
%   para.rec.nrecx= para.rec.nrecx + nr3; % cantidad de receptores
  
%% Analisis
para.npplo	 = 6;

% para.fmax	   = 0.5; % eta = 2
% para.fmax	   = 1; % eta = 4
% para.fmax	   = 1.5; % eta = 6
para.fmax	   = 2; % eta = 8

para.nf      = 8;
para.jini    = 2;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;

%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);

%% Graficar 
% 
% % resultados de IBEM-DWN
load('../out/gen3D_OP_xs0_zs0_npplo6_DWN_20161210T210117.mat')
R.uw = uw;
nr = 1024;

xax = linspace(para.rec.xr(1),para.rec.xr(nr/2),nr/2);
yax = linspace(para.rec.yr(nr/2+1),para.rec.yr(end),nr/2);

dxx = xax(2)-xax(1); dkx = 1/(dxx*nr/2); kxax = linspace(-dkx*(nr/2-1),dkx*(nr/2),nr/2);
dxy = yax(2)-yax(1); dky = 1/(dxy*nr/2); kyax = linspace(-dky*(nr/2-1),dky*(nr/2),nr/2);

for iinc = 1:para.ninc
figure(30+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' x/a'])
figure(40+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' y/a'])
figure(130+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' x/a'])
figure(140+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' y/a'])
for j=2:4

% x/a
figure(30+iinc);hold on
ran = 1:nr/2;

subplot(3,1,1); hold on
plot(xax,squeeze(abs(R.uw(j+1,1:nr/2,iinc,1))),'k--','DisplayName',['u_xa_HOMOeta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(xax,squeeze(abs(R.uw(j+1,1:nr/2,iinc,2))),'k--','DisplayName',['v_xa_HOMOeta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(xax,squeeze(abs(R.uw(j+1,1:nr/2,iinc,3))),'k--','DisplayName',['w_xa_HOMOeta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_z | ')

% xlim([-0.75 0.75])
%fk
figure(130+iinc);hold on
subplot(3,1,1); hold on
fk = fftshift(R.uw(j+1,ran,iinc,1)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['u_kx_HOMOeta ' num2str(j*2)])
xlabel('kx');ylabel('| U_x(x,\omega) | ')
subplot(3,1,2); hold on
fk = fftshift(R.uw(j+1,ran,iinc,2)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['v_kx_HOMOeta ' num2str(j*2)])
xlabel('kx');ylabel('| U_y(x,\omega) | ')
subplot(3,1,3); hold on
fk = fftshift(R.uw(j+1,ran,iinc,3)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['w_kx_HOMOeta ' num2str(j*2)])
xlabel('kx');ylabel('| U_z(x,\omega) | ')


% y/a
figure(40+iinc);hold on
ran = nr/2+1:nr;

subplot(3,1,1); hold on
plot(yax,squeeze(abs(R.uw(j+1,nr/2+1:end,iinc,1))),'k--','DisplayName',['u_ya_HOMOeta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(yax,squeeze(abs(R.uw(j+1,nr/2+1:end,iinc,2))),'k--','DisplayName',['v_ya_HOMOeta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(yax,squeeze(abs(R.uw(j+1,nr/2+1:end,iinc,3))),'k--','DisplayName',['w_ya_HOMOeta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_z | ')

% xlim([-0.75 0.75])

%fk
figure(140+iinc);hold on
subplot(3,1,1); hold on
fk = fftshift(R.uw(j+1,ran,iinc,1)*dxy); %forward
plot(kyax,squeeze(abs(fk)),'k-','DisplayName',['u_ky_HOMOeta ' num2str(j*2)])
xlabel('ky');ylabel('| U_x(x,\omega) | ')
subplot(3,1,2); hold on
fk = fftshift(R.uw(j+1,ran,iinc,2)*dxy); %forward
plot(kyax,squeeze(abs(fk)),'k-','DisplayName',['v_ky_HOMOeta ' num2str(j*2)])
xlabel('ky');ylabel('| U_y(x,\omega) | ')
subplot(3,1,3); hold on
fk = fftshift(R.uw(j+1,ran,iinc,3)*dxy); %forward
plot(kyax,squeeze(abs(fk)),'k-','DisplayName',['w_ky_HOMOeta ' num2str(j*2)])
xlabel('ky');ylabel('| U_z(x,\omega) | ')

end
end
% 
% % load handel; sound(y,Fs)    % sonido triunfal
end
