function batchGenerarEjercicio030
% FunciOn de Green en semiespacio 2D. fotograma
% [metro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;
para.rafraichi = 1;
para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 1; % 2D 
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 20000;     % periodicidad de la fuente ( L ) >= 200

i=0;
% i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo SS1
% para.reg(med).sub(i).rho      = 1.2;
% para.reg(med).sub(i).alpha    = 400;
% para.reg(med).sub(i).bet      = 70;
% para.reg(med).sub(i).qd       = 100;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 40; %espesor

i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo MA
para.reg(med).sub(i).rho      = 1.1;
para.reg(med).sub(i).alpha    = 255.69;
para.reg(med).sub(i).bet      = 34;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 20; %espesor
i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo MA
para.reg(med).sub(i).rho      = 1.5;
para.reg(med).sub(i).alpha    = 594.1;
para.reg(med).sub(i).bet      = 79;
para.reg(med).sub(i).qd       = 500;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 20; %espesor

i = i+1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.5;
para.reg(med).sub(i).alpha    = 2000;
para.reg(med).sub(i).bet      = 1000;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.nsubmed = i; % cantidad de estratos (en 2D, DWN solo en medio 1)
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

L = para.reg(med).xl;
disp(['L = ',num2str(L),' en medio ',num2str(med)])
minB = para.reg(med).sub(1).bet;
maxA = para.reg(med).sub(i).alpha;
disp(['Arribo de espurias a la fuente: (', ...
  num2str(L/maxA),' a ',num2str(L/minB),') sec'])
clear L minB maxB
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% med = 2;
% para.tipoMed(med) = 1; % HomogEneo / Espacio completo
% para.reg(med).rho = 2.5;
% para.reg(med).alpha = 2000;
% para.reg(med).bet = 1000;
% para.reg(med).qd= 10000;
% 
% for i=1:para.nmed % LamE
%   para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
%   para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
%   para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
%   para.reg(i).tipoatts=1; % Q
% end

clear med i
%% GeometrIA
para.geo = 2; %de acuerdo a:
%              2D ={'No boundaries',...    2
%                   'Semi Espacio',...     3
%                   '2 contornos ',...     4
%                   'placa ilimitada',...  5
%                   'semi-placa L',...     6
%                   'semi-placa R'};      %7
%              3D ={'No boundaries',...
%                   'From STL file(s)'}; % 3D general
                
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med
%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales
para.pol = 2; %{'SH','PSV'}

% n=1;
% para.ninc = n*2; % cantida de fuentes * 3 direcciones
% para.tipo_onda=1; % no se usa
% 
% para.xs= repmat(Xi(1),2,1);
% para.ys= repmat(Xi(2),2,1);
% para.zs= repmat(Xi(3),2,1);
% % direccion +x
% para.gam(1:n) = 90;
% para.phi(1:n) = 0;
% % direccion +z
% para.gam(n+1:2*n) = 180;
% para.phi(n+1:2*n) = 0;

para.ninc = 1; % una fuente
para.tipo_onda=1; % no se usa

Xi = [0, 0, 0]; % fuente x2

para.xs = Xi(1);
para.ys = Xi(2);
para.zs = Xi(3);
% direccion +x
para.gam = 90;
para.phi = 0;
%% Receptores
para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];

para.rec.resatboundary=0;
para.chgrec = 0;
  
% para.recpos=3; %receptores en posicion libre
% para.chgrec = 0;
% nr = 1;
% para.rec.nrecx= nr; % cantidad de receptores
% para.rec.xr= X(1);
% para.rec.yr= X(2);
% para.rec.zr= X(3);

% lineas de receptores:
  para.recpos=3; %receptores en posicion libre
  nr = 2048;
  para.rec.xr= linspace(1000,2999,nr).';
  para.rec.yr= zeros(nr,1);
  para.rec.zr= zeros(nr,1);
  para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores

% malla plano xz
% para.recpos = 2;
% para.rec.nrecx = 61;
% para.rec.nrecy = 1;
% para.rec.nrecz = 31;
% para.rec.xri = -10;
% para.rec.yri = 0;
% para.rec.zri = 0;
% para.rec.dxr = 1/3;
% para.rec.dyr = 0;
% para.rec.dzr = 1/3;
%% Analisis
para.fac_omei_DWN = 0.1; % 1

para.npplo	 = 10;
para.fmax	   = 2;

para.nf      = 600;
para.jini    = 0; % (1)=casi 0, (2)=df, (3)=2*df, ...
para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ',num2str(para.tmaxinteres),'sec de ',num2str(para.tmax),'sec'])
df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')
%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;
para.sortie.Ut  = 1;  % todos

para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
bol = 0;
para.sortie.sxx =bol;
para.sortie.syy =bol;
para.sortie.szz =bol;
para.sortie.sxy =bol;
para.sortie.sxz =bol;
para.sortie.syz =bol; clear bol
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')
%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
sw = R.sw;
size(uw);
% return;
% load handel; sound(y,Fs)    % sonido triunfal
% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 
%% load anterior
% load('../out/2D_PSV_FP_xs0_zs0_npplo10_DWN_20170228T164403.mat')
% df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;
%% funcion de amplitud
% para.pulso.tipo=3; %Ricker periodo caracter?stico tp
% para.pulso.a = 2;1.5;%0.3;    % tp
% para.pulso.b = 2;    % ts
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo =5; % gaussiano rise time
% para.pulso.a = 1;   % rise time 
% para.pulso.b = 2;   % retraso
% para.pulso.c = 0;  % corrimiento

para.pulso.tipo=4; % plano + tapper gaussiano
para.pulso.a = 2.5;   % rise time
para.pulso.b = 5;    % retraso
para.pulso.c = 1;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);
% inversion
[utc,stc]   = inversion_w(uw,sw,para);
%% sabana desplazamientos linea
bn = 3000;
% resultados en  x
ran = 1:2:nr; % en linea de receptores
tran = 1:para.zeropad; %floor(50/dt);%para.zeropad; % menor que zeropad
nam{1} = 'u';
nam{2} = 'w';
Hh=zeros(4,1); ii=1;
for iinc = 1
  sca = 0;
  for icomp = 1:2
    sca = max(sca,(para.rec.xr(ran(2))-para.rec.xr(ran(1))) / max(max(utc(tran,ran,iinc,icomp))));
  end
  sca = sca*2;
  for icomp = 1:2
    figure(bn+(iinc-1)*100+icomp); clf; axes; hold on
    set(gcf,'name',nam{icomp})
    hg = hggroup;
    for ir = ran
      plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-',...
        'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
      %   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
    end
    % ylim([-11 11]); xlim([0 40])
    Hh(ii) = hg; ii=ii+1;
  end
end
% delete(Hh)
disp('trazados')
%% odograma
bn = 5000;
% resultados en  x
ran = 1:4:nr; % en linea de receptores
tran = 1:floor(50/dt);%para.zeropad; % menor que zeropad
tran = 1:10:para.zeropad; % menor que zeropad
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));

Hh=zeros(4,1); ii=1;
for iinc = 1
figure(bn+(iinc-1)*100+icomp);   hold on   %   ; clf; axes; hold on
set(gcf,'name','odograma uw')
sca = 1 * (para.rec.xr(2)-para.rec.xr(1)) / max(max(max(utc(tran,ran,iinc,1:2))));
del = 2;(para.rec.xr(2)-para.rec.xr(1));
hg = hggroup; 
for ir = ran
  quiver(tps(tran),ir,utc(tran,ir,iinc,1).'*sca,...
    -utc(tran,ir,iinc,2).'*sca,del,'k-','ShowArrowHead','off');
  
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir),'k-',...
%     'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
end
% ylim([-11 11]); xlim([0 40])
Hh(ii) = hg; ii=ii+1;
end
%% fotogramas
% para.cont1 = para.cont;
% 
% RESULT.utc = utc;
% RESULT.stc = stc;
% para.film.filmStyle = 5; %color,grid,grid+shadow,elemMecEnPared,color+quiver}
% para.film.filmeMecElem = 1;
% para.film.fps = 50;
% para.film.BoundaryWarpRange = '1:end';
% para.film.filmeRange = 1:2:ceil(para.zeropad/3);
% para.film.strmecelemlist= {'Ux'  'Uz'  'sxx'  'szz'  'sxz'};
% set(0,'DefaultFigureWindowStyle','normal')
% filmoscopio2(para,RESULT,1);

end