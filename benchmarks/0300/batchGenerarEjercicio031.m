function batchGenerarEjercicio031
% Borde del valle, modelo SS o MA
% [metro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;
para.rafraichi = 1;
para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia
%% Materiales
para.dim = 1; % 2D 
para.nmed = 2; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 20000;     % periodicidad de la fuente ( L ) >= 200

i=0;
% i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo SS1
% para.reg(med).sub(i).rho      = 1.2;
% para.reg(med).sub(i).alpha    = 400;
% para.reg(med).sub(i).bet      = 70;
% para.reg(med).sub(i).qd       = 100;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 40; %espesor

i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo MA
para.reg(med).sub(i).rho      = 1.1;
para.reg(med).sub(i).alpha    = 255.69;
para.reg(med).sub(i).bet      = 34;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 20; %espesor
i = i+1; % Propiedades submedio 'i' del medio 'med' %% Modelo MA
para.reg(med).sub(i).rho      = 1.5;
para.reg(med).sub(i).alpha    = 594.1;
para.reg(med).sub(i).bet      = 79;
para.reg(med).sub(i).qd       = 500;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 20; %espesor

i = i+1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.5;
para.reg(med).sub(i).alpha    = 2000;
para.reg(med).sub(i).bet      = 1000;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.nsubmed = i; % cantidad de estratos (en 2D, DWN solo en medio 1)
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

L = para.reg(med).xl;
disp(['L = ',num2str(L),' en medio ',num2str(med)])
minB = para.reg(med).sub(1).bet;
maxA = para.reg(med).sub(i).alpha;
disp(['Arribo de espurias a la fuente: (', ...
  num2str(L/maxA),' a ',num2str(L/minB),') sec'])
clear L minB maxB
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

med = 2;
para.tipoMed(med) = 1; % HomogEneo / Espacio completo
para.reg(med).rho = 2.5;
para.reg(med).alpha = 2000;
para.reg(med).bet = 1000;
para.reg(med).qd= 100;

for i=1:para.nmed % LamE
  para.reg(i).lambda	= para.reg(i).rho*(para.reg(i).alpha^2-2*para.reg(i).bet^2);
  para.reg(i).mu      = para.reg(i).rho*para.reg(i).bet^2;
  para.reg(i).nu	= para.reg(i).lambda/(2*(para.reg(i).lambda + para.reg(i).mu));
  para.reg(i).tipoatts=1; % Q
end

clear med i
%% GeometrIA
 %de acuerdo a:
%              2D ={'No boundaries',...    2
%                   'Semi Espacio',...     3
%                   '2 contornos ',...     4
%                   'placa ilimitada',...  5
%                   'semi-placa L',...     6
%                   'semi-placa R'};      %7
%              3D ={'No boundaries',...
%                   'From STL file(s)'}; % 3D general
                
med = 1; 
para.geo(med) = 2; 
for c = 1:2
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 1; % no boundaries
para.cont(med,c).xa = -3;
para.cont(med,c).a = 6;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 0.25;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;
end

med = 2; 
para.geo(med) = 4; % dos contornos
c = 1; % de arriba
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 9; % desde hardcodeGeom9.m
para.cont(med,c).xa = 0;
para.cont(med,c).a = 0.3;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 1;
para.cont(med,c).h = 1;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;

c = 2; % de abajo
para.cont(med,c).NumPieces = 0;
para.cont(med,c).piece=cell(1); 
para.cont(med,c).geom = 10; % desde hardcodeGeom10.m, luego borrarlo
para.cont(med,c).xa = 0;
para.cont(med,c).a = 0.3;
para.cont(med,c).th = 0;
para.cont(med,c).ba = 0.25;
para.cont(med,c).h = 0.25;
para.cont(med,c).ruggeo = 1;
para.cont(med,c).rba = 0.25;
para.cont(med,c).rh = 0.25;

clear med
%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales
para.pol = 2; %{'SH','PSV'}

% n=1;
% para.ninc = n*2; % cantida de fuentes * 3 direcciones
% para.tipo_onda=1; % no se usa
% 
% para.xs= repmat(Xi(1),2,1);
% para.ys= repmat(Xi(2),2,1);
% para.zs= repmat(Xi(3),2,1);
% % direccion +x
% para.gam(1:n) = 90;
% para.phi(1:n) = 0;
% % direccion +z
% para.gam(n+1:2*n) = 180;
% para.phi(n+1:2*n) = 0;

para.ninc = 1; % una fuente
para.tipo_onda=1; % no se usa

Xi = [0, 0, 0]; % fuente x2

para.xs = Xi(1);
para.ys = Xi(2);
para.zs = Xi(3);
% direccion +x
para.gam = 90;
para.phi = 0;
%% Receptores
para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];

para.rec.resatboundary=0;
para.chgrec = 0;
  
% para.recpos=3; %receptores en posicion libre
% para.chgrec = 0;
% nr = 1;
% para.rec.nrecx= nr; % cantidad de receptores
% para.rec.xr= X(1);
% para.rec.yr= X(2);
% para.rec.zr= X(3);

% lineas de receptores:
%   para.recpos=3; %receptores en posicion libre
%   nr = 2048;
%   para.rec.xr= linspace(1000,2999,nr).';
%   para.rec.yr= zeros(nr,1);
%   para.rec.zr= zeros(nr,1);
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores

% malla plano xz
para.recpos = 2;
para.rec.nrecx = 333;
para.rec.nrecy = 1;
para.rec.nrecz = 16;
para.rec.xri = 2000;
para.rec.yri = 0;
para.rec.zri = 0;
para.rec.dxr = 3;
para.rec.dyr = 0;
para.rec.dzr = 3;
para.rafraichi=0; % <- para que identifique en que medio esta cada receptor (se tarda)
%% Analisis
para.fac_omei_DWN = 0.1; % 1
para.loadme = false;

para.npplo	 = 10;
para.fmax	   = 2;

para.nf      = 600;
para.jini    = 0; % (1)=casi 0, (2)=df, (3)=2*df, ...
para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ',num2str(para.tmaxinteres),'sec de ',num2str(para.tmax),'sec'])
df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')
%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;
para.sortie.Ut  = 1;  % todos

para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
bol = 0;
para.sortie.sxx =bol;
para.sortie.syy =bol;
para.sortie.szz =bol;
para.sortie.sxy =bol;
para.sortie.sxz =bol;
para.sortie.syz =bol; clear bol
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')
%% ver dibujo:
 dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
return
%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
sw = R.sw;
size(uw);

exit

% return;
% load handel; sound(y,Fs)    % sonido triunfal
% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 
%% load anterior
% caso SS corregido
%load('/Users/marshall/Documents/DOC/unit9/013BordeV/outModeloSS/2D_PSV_FP_xs0_zs0_npplo10_DWN_20170316T170755.mat');

% campo libre modelo M
%load('/Users/marshall/Documents/DOC/ibem_dwn_matlab/out/2D_PSV_FP_xs0_zs0_npplo10_DWN_20170318T015231.mat');
%uw0 = uw;

% caso M corregido
%load('/Users/marshall/Documents/DOC/unit9/013BordeV/outModeloMA/2D_PSV_FP_xs0_zs0_npplo10_DWN_20170315T170330.mat');

% df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;

% uw = uw-uw0;

% para fotogramas:
load('/Users/marshall/Documents/DOC/unit9/013BordeV/mov/2D_PSV_FP_xs0_zs0_npplo10_DWN_20170326T191205.mat');
df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;
%% Dencidad espectral en tramos de x
nf      = para.nf;           %disp(['nf = ',num2str(nf)])
nfN     = nf/2+1;
df      = para.fmax/(nf/2);  %disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       %disp(['Fmx= ',num2str(Fq(end))])
icomp = 2;

ran = 1:nr;
dxx = para.rec.xr(ran(2))-para.rec.xr(ran(1)); 
for iXran = 0:15; % 0:7
ran =  1:128;
ran = ran+128*iXran; % 0 a 7
figure;
acupsdx = zeros(nfN,length(ran)); ijj = 1;
for ir = ran
  psdx = (1/(2*pi*nfN)) * abs(uw(:,ir,1,icomp)).^2;
  acupsdx(:,ijj) = psdx; ijj=ijj+1;
  plot(Fq,10*log10(psdx),'Color',[0.7 0.7 0.7],'LineWidth',0.1);hold on;
%   semilogy(Fq,(uw(:,ir,1,icomp).' .* (uw(:,ir,1,icomp))'),'k-','LineWidth',0.3);hold on;
end
medpsdx = mean(acupsdx,2);
sigma = std(acupsdx,0,2);
plot(Fq,10*log10(medpsdx),'r-','LineWidth',2)
plot(Fq,real(10*log10(medpsdx-sigma)),'k--','LineWidth',1)
plot(Fq,10*log10(medpsdx+sigma),'k--','LineWidth',1)

nam = ['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'];
title(nam)
xlabel('Frecuencia (Hertz)'); ylabel('dB/Hz');ylim([-210 -100])
set(gca,'FontName','times')
set(gca,'FontSize',20)
% savefig(['../out/fig7_psd',nam,'_u.fig'])
saveas(gcf,['../out/fig7_psd',nam,'_w.png'])
close(gcf);
end
%% dispersion relation between two stations
for iXran = 0:7; % 0:7
ran =  1:1:256;
ran = ran+256*iXran; % 0 a 7
% ran = nr-512:nr;
disp(['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'])
VecX=zeros(1,length(ran)*length(ran));
VecZ=zeros(1,length(ran)*length(ran));
% VecZg=zeros(1,length(ran)*length(ran));
jj = 0;

nf      = para.nf;           %disp(['nf = ',num2str(nf)])
df      = para.fmax/(nf/2);  %disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       %disp(['Fmx= ',num2str(Fq(end))])

for i1 = ran
  for i2 = ran
% i1 = 1; i2 = 1024;
% X = uw(:,i1,1,1); % horizontales
% Y = uw(:,i2,1,1); 

X = uw(:,i1,1,2); % verticales
Y = uw(:,i2,1,2); 

% distancia entre receptores
d = para.rec.xr(i2)-para.rec.xr(i1);

% Libro de Kramer cap 6.
% cross spectrum
XY = X.' .* Y';
% phase difference
ph = atan2(imag(XY),real(XY));
% phase velocity
vl = (2*pi*Fq*d)./ph;

isok = find(~isnan(vl) & vl>0);

% acumular resultaado:
VecZ((jj+1):(jj)+length(vl(isok))) = vl(isok);
VecX((jj+1):(jj)+length(vl(isok))) = Fq(isok);
jj = jj+length(vl);
  end
end
disp('haciendo grafica')

% grafica bn
% h = scatter(VecX,VecZ,'k.');hold on;ylim([0 1000])

% grafica colores
% rejilla
velVec = 0:5:1000;
M = zeros(length(velVec),length(Fq));
for ii = 1:size(M,2)
  estosX = find(VecX == Fq(ii));
  VetosX = VecZ(estosX);
  for jj = 1:size(M,1)-1
    estosZ = find(VetosX((velVec(jj) <= VetosX) & (velVec(jj+1) > VetosX)));
    M(jj,ii) = M(jj,ii) + length(estosZ);
  end
end
% M(find(M==0))=nan;
%
% compresion
p1 = 0.1;
Mc = log(1 + exp(p1)*abs(M)) / log(1 + exp(p1));
Mc = Mc / max(max(Mc));
      
figure; 
h=surf(Fq,velVec,Mc); 
title(['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'])
set(h,'EdgeColor','none')
view(0,90)
xlim([0.35 2])
xlabel('Frecuencia (Hertz)');ylabel('Velocidad de fase (m/s)')

disp('termino la grafica')
p = [3   309   347   380];
p(1)=p(1) + iXran*100;
set(gcf,'Position',p);
end
%% f-k
nf      = para.nf;           %disp(['nf = ',num2str(nf)])
df      = para.fmax/(nf/2);  %disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       %disp(['Fmx= ',num2str(Fq(end))])

for iXran = 0%0:7; % 0:7
ran =  1:256*8;
ran = ran+256*iXran; % 0 a 7
% ran = nr-512:nr;
disp(['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'])
dxx = para.rec.xr(ran(2))-para.rec.xr(ran(1)); 
nk = length(ran);
dkx = 2*pi/(dxx*nk/2);
kVec = linspace(-nk/2-1,nk/2,nk)*dkx;
M = zeros(nk,length(Fq));

for jj = 1:nf/2+1
% X = uw(jj,ran,1,1); % horizontales
X = uw(jj,ran,1,2); % verticales

fk = ifftshift(X*dxx); 
M(:,jj) = abs(fk);
% M(:,jj) = X;
end
% compresion
p1 = 1;
Mc = log(1 + exp(p1)*abs(M)) / log(1 + exp(p1));
Mc = Mc / max(max(Mc));

figure; 
h=surf(Fq,kVec,Mc); 
title(['x=[',num2str(para.rec.xr(ran(1)),4),',',num2str(para.rec.xr(ran(end)),4),']'])
set(h,'EdgeColor','none')
view(0,90)
xlim([0 2])
xlabel('Frecuencia (Hertz)');ylabel('N?mero de onda horizontal (1/m)')

end
%% funcion de amplitud
df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;

% para.pulso.tipo=3; %Ricker periodo caracter?stico tp
% para.pulso.a = 1.5;%0.3;    % tp
% para.pulso.b = 6;    % ts
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo =5; % gaussiano rise time
% para.pulso.a = 1.0;   % rise time 
% para.pulso.b = 2;   % retraso
% para.pulso.c = 0;  % corrimiento

para.pulso.tipo=4; % plano + tapper gaussiano
para.pulso.a = 2.5;   % rise time
para.pulso.b = 5;    % retraso
para.pulso.c = 1;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);
% inversion
[utc,stc]   = inversion_w(uw,sw,para);
%% S transform
para.zeropad = 2^12; % para que no tarde demasiado
nf      = para.nf;           disp(['nf = ',num2str(nf)])
nfN     = nf/2+1; 
df      = para.fmax/(nf/2);     disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       disp(['Fmx= ',num2str(Fq(end))])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
tps = (0:para.zeropad-1)*dt; disp(['tmx= ',num2str(tps(end))])
[utcS,~]   = inversion_w(uw,sw,para);
%%
ir = 1025+102*8+3;
ir = 1025+102*7+3;
disp(num2str(para.rec.xr(ir),4))

tran = 1:para.zeropad;
iinc = 1;
icomp = 1;
m = mean(utcS(tran,ir,iinc,icomp));
X = (utcS(tran,ir,iinc,icomp)-m).';
ST=stran(X);
N = para.zeropad;
df = 1/dt/N;
fmax = df*N/2;
FqS = linspace(0,fmax,N/2+1);

figure; 
h = surf(tps,FqS,abs(ST));
set(h,'EdgeColor','none')
view(0,90)
ylabel('Frecuencia (Hertz)')
xlabel('Tiempo (segundos)')
ylim([0 para.fmax])

p = [69         400        1186         230];
set(gcf,'Position',p);
set(gca,'FontName','times')
set(gca,'FontSize',20)
title(['x=',num2str(para.rec.xr(ir),4),'m'])
% savefig(['../out/fig7_Strans',num2str(ir),'_u.fig'])
% saveas(gcf,['../out/fig7_Strans',num2str(ir),'_u.png'])
% close(gcf);

%% encimar sintentico.
ir = 1025+102*1;
disp(num2str(para.rec.xr(ir),4))

tran = 1:para.zeropad;
iinc = 1;
icomp = 1;
m = mean(utcS(tran,ir,iinc,icomp));
X = (utcS(tran,ir,iinc,icomp)-m).';

figure; 
set(gcf,'Name',['x=',num2str(para.rec.xr(ir),4),'m'])
plot(tps,X,'y-','LineWidth',2)
set(gca,'Color','b')
% set(gca,'YAxisLocation','left');
p = [69         400        1186         230];
set(gcf,'Position',p);
set(gca,'FontName','times')
set(gca,'FontSize',20)
title(['x=',num2str(para.rec.xr(ir),4),'m'])
xlabel('tiempo')
ylabel('')
%% sabana desplazamientos linea
bn = 3000;
% resultados en  x
ran = 1000:10:1750;%nr; % en linea de receptores
tran = 1:para.zeropad; %floor(50/dt);%para.zeropad; % menor que zeropad
nam{1} = 'u';
nam{2} = 'w';
Hh=zeros(4,1); ii=1;
for iinc = 1
  sca = 0;
  for icomp = 1:2
    sca = max(sca,(para.rec.xr(ran(2))-para.rec.xr(ran(1))) / max(max(utc(tran,ran,iinc,icomp))));
  end
  sca = sca*1;
  for icomp = 1
%     figure(bn+(iinc-1)*100+icomp); clf; axes; hold on
    figure(icomp); hold on;
    set(gcf,'name',nam{icomp})
    hg = hggroup;
    for ir = ran
      m = mean(utc(tran,ir,iinc,icomp));
      plot(tps(tran),(utc(tran,ir,iinc,icomp)-m)*sca+para.rec.xr(ir),'k-',...
        'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
      %   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
    end
    % ylim([-11 11]); xlim([0 40])
    Hh(ii) = hg; ii=ii+1;
%     ylim([990 3010])
%     set(gcf,'Position',pp);
  end
end
% delete(Hh)
disp('trazados')
%% odograma
bn = 5000;
% resultados en  x
ran = 1:100:nr; % en linea de receptores
% ran = 1000:50:1750;
tran = 1:5:para.zeropad; %1:floor(100/dt);%para.zeropad; % menor que zeropad
% tran = 1:10:para.zeropad; % menor que zeropad
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));

Hh=zeros(4,1); ii=1;
for iinc = 1
figure(bn+(iinc-1)*10); clf; axes; hold on
set(gcf,'name','odograma uw')
sca = 1 * (para.rec.xr(2)-para.rec.xr(1)) / max(max(max(utc(tran,ran,iinc,1:2))));
% del = (para.rec.xr(2)-para.rec.xr(1));
% del = 20*sca;
del = 2; j=1;
hg = hggroup; 
mxr = 0;
for ir = ran
u = (utc(tran,ir,iinc,1)-mean(utc(tran,ir,iinc,1))) * sca;
w = (utc(tran,ir,iinc,2)-mean(utc(tran,ir,iinc,2))) * sca;
r = (u.^2 + w.^2).^0.5; 
mxr = max(mxr,max(r));
end

for ir = ran
  u = (utc(tran,ir,iinc,1)-mean(utc(tran,ir,iinc,1))) * sca;
  w = (utc(tran,ir,iinc,2)-mean(utc(tran,ir,iinc,2))) * sca*50;
  r = (u.^2 + w.^2).^0.5; 
  quiver(tps(tran),para.rec.xr(ir), u.', + w.',...
    1 * (max(r)/mxr),...
    'k-','ShowArrowHead','off','LineWidth',0.2);
%   px = tps(tran) + utc(tran,ir,iinc,1).'*sca;
%   pz = ir        + utc(tran,ir,iinc,2).'*sca;
%   plot(px,pz,'r')
j=j+1;
end
% ylim([-11 11]); xlim([0 40])
Hh(ii) = hg; ii=ii+1;
end

%% fotogramas
para.cont1 = para.cont;

% remover la media de los sismogramas
iinc = 1;
utcm = utc;
for ir = size(utc,2)
  for icom = 1:2
    for ite = 1:3
    utcm(:,ir,iinc,icom) = detrend(utcm(:,ir,iinc,icom));
    m100 = mean(utcm(1:300,ir,iinc,icom));
    utcm(:,ir,iinc,icom) = utcm(:,ir,iinc,icom) - m100;
    end
  end
end
% %
% for ir = size(utcm,2)
%   for icom = 1:2
%     utcm(:,ir,iinc,icom) = utcm(:,ir,iinc,icom).^9;
%     m100 = mean(utcm(1:100,ir,iinc,icom));
%     utcm(:,ir,iinc,icom) = utcm(:,ir,iinc,icom) - m100;
%   end
% end
utcS = utcm;
disp('done')
%% hacer video
clear RESULT
RESULT.utc = utcm;
RESULT.stc = stc;
para.film.filmStyle = 5; %color,grid,grid+shadow,elemMecEnPared,color+quiver}
para.film.filmeMecElem = 1;
para.film.fps = 50;
para.film.BoundaryWarpRange = '1:end';
para.film.filmeRange = 1:5:ceil(para.zeropad);
para.film.strmecelemlist= {'Ux'  'Uz'  'sxx'  'szz'  'sxz'};
set(0,'DefaultFigureWindowStyle','normal')
filmoscopio2(para,RESULT,1);

end