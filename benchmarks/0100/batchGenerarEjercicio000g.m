function batchGenerarEjercicio000g
% como 000f pero sin los dos primeros estratos
% Im(Gii(x,x)) en la superficie. Usa estratigrafia de Cruz-Atienza et al. 
% "Long Duration of Ground Motion in the Paradigmatic Valley of Mexico"
% Nature Scientific Reports, 2016. 
% En m/s e incluyendo estratos planos hasta el semiespacio

X  = [0,0,0];
X2 = [[0;0],[0;0],[0.555;0.6]];
Xi = [0,0,0];

%% Preambulo 
set(0,'DefaultFigureWindowStyle','docked')
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia

%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 1000;     % periodicidad de la fuente ( L ) >= 200

i = 1; %1 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2;
% para.reg(med).sub(i).alpha    = 0.800;
% para.reg(med).sub(i).bet      = 0.050;
% para.reg(med).sub(i).qd       = 15;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.030; %espesor
% %  
% i = i+1; %2 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2;
% para.reg(med).sub(i).alpha    = 1.200;
% para.reg(med).sub(i).bet      = 0.100;
% para.reg(med).sub(i).qd       = 30;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.020; %espesor
% 
% i = i+1; %3 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.05;
para.reg(med).sub(i).alpha    = 2.000;
para.reg(med).sub(i).bet      = 0.400;
para.reg(med).sub(i).qd       = 40;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.300;%0.250; %espesor

i = i+1; %4 Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.05;
para.reg(med).sub(i).alpha    = 2.500;
para.reg(med).sub(i).bet      = 0.800;
para.reg(med).sub(i).qd       = 80;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0.250; %espesor
para.reg(med).sub(i).h        = 0.150; %espesor % al fondo del valle 

i = i+1; %5 Propiedades submedio 'i' del medio 'med' % medio exterior
para.reg(med).sub(i).rho      = 2.2;
para.reg(med).sub(i).alpha    = 2.700;
para.reg(med).sub(i).bet      = 1.560;
para.reg(med).sub(i).qd       = 156;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 1.420; %espesor

% i = i+1; %6 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2.53;
% para.reg(med).sub(i).alpha    = 5.510;
% para.reg(med).sub(i).bet      = 3.180;
% para.reg(med).sub(i).qd       = 318;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 2.340; %espesor

% i = i+1; %7 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2.69;
% para.reg(med).sub(i).alpha    = 6.000;
% para.reg(med).sub(i).bet      = 3.460;
% para.reg(med).sub(i).qd       = 346;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 10.970; %espesor

% i = i+1; %8 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 2.91;
% para.reg(med).sub(i).alpha    = 6.680;
% para.reg(med).sub(i).bet      = 3.860;
% para.reg(med).sub(i).qd       = 386;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 27.620; %espesor

% i = i+1; %9 Propiedades submedio 'i' del medio 'med'
% para.reg(med).sub(i).rho      = 3.43;
% para.reg(med).sub(i).alpha    = 8.310;
% para.reg(med).sub(i).bet      = 4.800;
% para.reg(med).sub(i).qd       = 480;
% para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

warning('a fuerza sin amortiguamiento')
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).qd = 10000; 
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
clear med i

%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med

%% Fuente

i=1;
para.fuente(i:i+2)=2; % 1 Ondas Planas  2 Fuentes Puntuales

% todo el tensor
n=1;
para.ninc = n*3; % cantida de fuentes * 3 direcciones
para.tipo_onda(i:i+2)=1; % no se usa

para.xs(i:i+2)= repmat(Xi(1),3,1);
para.ys(i:i+2)= repmat(Xi(2),3,1);
para.zs(i:i+2)= repmat(Xi(3),3,1);
para.zxs(i:i+2) = 1;
% direcci?n +x
para.gam(1:n) = 90;
para.phi(1:n) = 0;
% direcci?n +y
para.gam(n+1:2*n) = 90;
para.phi(n+1:2*n) = 90;
% direcci?n +z
para.gam(2*n+1:3*n) = 180;
para.phi(2*n+1:3*n) = 0;

i=4;   
para.ninc = para.ninc +1;
para.fuente(i)=1; % onda plana
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=   0.0;
para.ys(i)=   0.0;
para.zs(i)=   0.0;
para.xzs(i)=  1;
% de la normal :
para.gam(i)=  0;
para.phi(i)=  0;
%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% para.rec.nrecx= 1; % cantidad de receptores

para.rec.xr= [X(1);X2(:,1)];
para.rec.yr= [X(2);X2(:,2)];
para.rec.zr= [X(3);X2(:,3)];

para.rec.nrecx= length(para.rec.xr); % cantidad de receptores
nr = para.rec.nrecx;  
%% Analisis
para.npplo	 = 6;
para.fmax	   = 1.5;%3;
para.nf      = 700;%1500;
para.jini    = 5;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

% dibujo_estratifi(para,0,0);

% BatchScript([para.nomrep '/parameters.mat'],true); return
%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
% load handel; sound(y,Fs)    % sonido triunfal

uw = R.uw; 
% disp(squeeze(R.uw(101,1,1:3,1:3)))
%% cargar
% caso sin los estratos 1 y 2. L = 1000 para HV y func de trasf, onda S
% load('/Users/marshall/Documents/DOC/ibem_dwn_matlab/out/gen3D_FPFPFPOP_xs0_zs0_npplo6_DWN_20170324T124859.mat')
%%

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 
% 
nf      = para.nf;           disp(['nf = ',num2str(nf)])
df      = para.fmax/(nf/2);     disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       disp(['Fmx= ',num2str(Fq(end))])
ImGiiXX = imag(uw);
disp('got it')

%% figura (a) Im(G11) Im(G33)
figure;hold on; set(gcf,'name','ImGii(x,x)')
plot(Fq,squeeze(-2*ImGiiXX(:,1,1,1)),'r-.','DisplayName','ImG11');
% plot(Fq,squeeze(ImGiiXX(:,1,2,2)),'b--','DisplayName','ImG22');
plot(Fq,squeeze(2*ImGiiXX(:,1,3,3)),'r-.','DisplayName','ImG33');

%% figura (b) H/V
% I = imread('~/Desktop/figHVMalishfondo.png');
% image(I)

figure; set(gcf,'name','H/V')
HV = sqrt((-2*squeeze(ImGiiXX(:,1,1,1)))./squeeze(ImGiiXX(:,1,3,3)));

loglog(Fq,abs(HV),'r-')
% xlim([0.01 100])
% ylim([0.01 100])
xlim([0.1 5])
ylim([0.6 50])
hold on
grid on
disp('done')
%% transf
iinc = 4;
ftu = uw(:,1,iinc,1)./uw(:,2,iinc,1);
ftv = uw(:,1,iinc,2)./uw(:,2,iinc,2);
ftw = uw(:,1,iinc,3)./uw(:,2,iinc,3);
figure;
axes; set(gca,'Color','k');hold on
plot(Fq,abs(ftu),'r');
plot(Fq,abs(ftv),'g');
plot(Fq,abs(ftw),'b');
end