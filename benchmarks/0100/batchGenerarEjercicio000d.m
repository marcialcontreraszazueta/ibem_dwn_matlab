function batchGenerarEjercicio000d
% Im(Gii(x,x)) en la superficie. Reproducir grafica de Sanchez-Sesma et al.
% en "A theory for microtremor H/V ..." en GJI 2011

X  = [0,0,0];
Xi = [0,0,0];

%% Preambulo 
set(0,'DefaultFigureWindowStyle','docked')
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo funcione de transferencia

%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 1; % NUmero de medios
% Propiedades del medio 1

med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 10000;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.2;
para.reg(med).sub(i).alpha    = 400;
para.reg(med).sub(i).bet      = 70;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 40; %espesor
 
i = i+1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 2.5;
para.reg(med).sub(i).alpha    = 2000;
para.reg(med).sub(i).bet      = 1000;
para.reg(med).sub(i).qd       = 10000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end
clear med i

%% GeometrIA
para.geo = 4;
med = 1;  
para.cont(med,1).NumPieces = 0;
para.cont(med,1).piece=cell(1); 
clear med

%% Fuente
para.fuente=2; % 1 Ondas Planas  2 Fuentes Puntuales

% todo el tensor
n=1;
para.ninc = n*3; % cantida de fuentes * 3 direcciones
para.tipo_onda=1; % no se usa

para.xs= repmat(Xi(1),3,1);
para.ys= repmat(Xi(2),3,1);
para.zs= repmat(Xi(3),3,1);
% direcci?n +x
para.gam(1:n) = 90;
para.phi(1:n) = 0;
% direcci?n +y
para.gam(n+1:2*n) = 90;
para.phi(n+1:2*n) = 90;
% direcci?n +z
para.gam(2*n+1:3*n) = 180;
para.phi(2*n+1:3*n) = 0;

%% Receptores
para.rec.resatboundary=0;
para.recpos=3; %receptores en posicion libre
para.chgrec = 0;

% para.rec.nrecx= 1; % cantidad de receptores
nr = 1;
para.rec.xr= X(1);
para.rec.yr= X(2);
para.rec.zr= X(3);

para.rec.nrecx= nr; % cantidad de receptores
  
%% Analisis
para.npplo	 = 6;
para.fmax	   = 5;
para.nf      = 900;
para.jini    = 0;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

%% Variables de salida
%deplacements
para.GraficarCadaDiscretizacion = false;
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
% load handel; sound(y,Fs)    % sonido triunfal

uw = R.uw; 
% disp(squeeze(R.uw(101,1,1:3,1:3)))

% los tensores est?n en
%         R.sw(:,:,:,:)                1   2   3   4   5   6
%              | | | '--- componente: sxx syy szz sxy sxz syz
%              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz] 
%              | '------- receptor: 1:nFault
%              '--------- datos en frecuenica positiva 1:(para.nf/2+1) 
% 
nf      = para.nf;           disp(['nf = ',num2str(nf)])
df      = para.fmax/(nf/2);     disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       disp(['Fmx= ',num2str(Fq(end))])
ImGiiXX = imag(uw);
disp('got it')

%% figura (a) Im(G11) Im(G33)
figure;hold on; set(gcf,'name','ImGii(x,x)')
plot(Fq,squeeze(-2*ImGiiXX(:,1,1,1)),'r-.','DisplayName','ImG11');
% plot(Fq,squeeze(ImGiiXX(:,1,2,2)),'b--','DisplayName','ImG22');
plot(Fq,squeeze(2*ImGiiXX(:,1,3,3)),'r-.','DisplayName','ImG33');

%% figura (b) H/V

figure; set(gcf,'name','H/V')
HV = sqrt((-2*squeeze(ImGiiXX(:,1,1,1)))./squeeze(ImGiiXX(:,1,3,3)));

loglog(Fq,abs(HV),'r-.')
xlim([0.1 5])
ylim([0.6 50])
disp('done')
end