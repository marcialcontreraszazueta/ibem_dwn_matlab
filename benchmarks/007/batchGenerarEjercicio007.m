function batchGenerarEjercicio007
% Valle irregular, DWN y DWN
% Incidencia vertical onda P
% SoluciOn en eta_q = 0.5

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 4; % 3D geometria irregular
para.nmed = 2; % NUmero de medios

% Propiedades del medio 1 (semiespacio)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 200;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 1.0;
para.reg(med).sub(i).alpha    = 3.46;
para.reg(med).sub(i).bet      = 2;
para.reg(med).sub(i).qd       = 5000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% Propiedades del medio 2 (valle)
med = 2;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 200;     % periodicidad de la fuente ( L ) >= 200

i = 1; % Propiedades submedio 'i' del medio 'med'
para.reg(med).sub(i).rho      = 0.8;
para.reg(med).sub(i).alpha    = 2.08;
para.reg(med).sub(i).bet      = 1;
para.reg(med).sub(i).qd       = 100;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 0.0; %espesor

para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio
for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% para.DWNxl = 1000; % periodicidad de la fuente ( L )
clear med i
%% GeometrIA
% nota: Las normales hacia afuera

para.geo = 4;

% --------------------------------------------------------
med=1;
para.cont(med,1).NumPieces = 0;
med = 2;  
para.cont(med,1).piece=cell(1); 
i = 1;
para.cont(med,1).piece{i,1}.fileName = [para.nomrep '/Cuerno_21_A.stl']; %+z
para.cont(med,1).piece{i,1}.kind=2; % 1 Free surface, 2 Continuity, 3 Auxiliar
para.cont(med,1).piece{i,1}.continuosTo=1;
para.cont(med,1).piece{i,1}.ColorIndex=1; % 1b,2r,3g,4y,5p,6c,7w,8k
para.cont(med,1).piece{i,1}.isalist = 0;
[para.cont(med,1).piece{i,1}.geoFileData,flag] = ...
 previewSTL(0,para.cont(med,1).piece{i});
if flag == 0 % cuando no se pudo cargar
  disp (para.cont(med,1).piece{i,1}.fileName)
  error ('No se pudo cargar')
end
para.cont(med,1).NumPieces = i;

% apilar   
  %     FV sOlo se usa en inclusiontest3G.m 
  %             para identificar la 
  %       region el medio de los sensores
for med=1:para.nmed 
  if para.cont(med,1).NumPieces>0
  para.cont(med,1).FV.vertices = [];
  para.cont(med,1).FV.faces = [];
  para.cont(med,1).FV.facenormals = [];
  for ip = 1:para.cont(med,1).NumPieces
      if size(para.cont(med,1).piece,1) >= ip
    if isfield(para.cont(med,1).piece{ip}.geoFileData,'V')
      % si el anterior es una frontera auxiliar .kind=3 y 
      % es continuosTo = ip entonces no hacer este ip
      if ip>1 
        if (para.cont(med,1).piece{ip-1}.kind == 3) && ...
          (para.cont(med,1).piece{ip-1}.continuosTo == ip)
          continue
        end
      end
      
      para.cont(med,1).FV.vertices =    [para.cont(med,1).FV.vertices;    para.cont(med,1).piece{ip}.geoFileData.V];
      para.cont(med,1).FV.faces =       [para.cont(med,1).FV.faces;       para.cont(med,1).piece{ip}.geoFileData.F];
      para.cont(med,1).FV.facenormals = [para.cont(med,1).FV.facenormals; para.cont(med,1).piece{ip}.geoFileData.N];
    end
      end
  end
  end
end
clear med i ip
%% Fuente
para.fuente=1; % 1 Ondas Planas  2 Fuentes Puntuales
para.ninc     =1;
para.tipo_onda=1; % P, S, R
para.xs(1)= 0;
para.ys(1)= 0;
para.zs(1)= 0;
para.gam(1)= 30;
para.phi(1)= 0;
para.xzs(1) = 1;
%% Receptores
para.rec.resatboundary=0;
para.recpos =4; % ../ins/Recep.txt'

% Receptores Croissant
a = 4;
d = 0.075745;
v = linspace(0,47,48)*d-1.82;
res = zeros(48*2,3);
for i=1:48
  res(i,1) = v(i)*a;
  res(i+48,2) = v(i)*a;
end
dlmwrite([para.nomrep '/Recep.txt'],res,' ')

  indat = importdata([para.nomrep '/Recep.txt']);
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  para.rec.nrecx= size(indat,1); % cantidad de receptores
  para.rec.xr= indat(:,1);
  para.rec.yr= indat(:,2);
  para.rec.zr= indat(:,3);
  clear indat

%% Analisis
para.npplo	 = 6;
para.fmax	   = 0.25; % eta = 1
% para.fmax	   = 0.5; % eta = 2
para.nf      = 200;
para.jini    = 100;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =0;
para.sortie.syy =0;
para.sortie.szz =0;
para.sortie.sxy =0;
para.sortie.sxz =0;
para.sortie.syz =0;
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

% %% ver dibujo:
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);

%% Cargar resultados de comparacion
arch = '../benchmarks/007/OndaP2.txt';
nrecep = 48*2;
nfrec = 1024+1;

% leer datos
fileID = fopen(arch,'r');
vfrec = zeros(nfrec,1);
Uu = zeros(nfrec,nrecep);
Uv = zeros(nfrec,nrecep);
Uw = zeros(nfrec,nrecep);
for j=1:nfrec
a = fscanf(fileID,'%f %d',2); vfrec(j)=a(1); 
% disp(['[' num2str(j) ']= ' num2str(vfrec(j)) ' Hertz'])
au = fscanf(fileID,'%f',2*nrecep);
Uu(j,:) = au(1:2:2*nrecep)+au(2:2:2*nrecep)*1i;
av = fscanf(fileID,'%f',2*nrecep);
Uv(j,:) = av(1:2:2*nrecep)+av(2:2:2*nrecep)*1i;
aw = fscanf(fileID,'%f',2*nrecep);
Uw(j,:) = aw(1:2:2*nrecep)+aw(2:2:2*nrecep)*1i; 
end
fclose(fileID);
disp(['Done reading file ' arch])

%% Graficar 

% resultados de comparacion
jh = 128+1; % para 0.25Hz
% jh = 256+1; % para 0.5Hz
disp([num2str(vfrec(jh)) ' eta'])

% resultados de IBEM-DWN
j=100;

% x/a
figure(1);hold on
xax = linspace(para.rec.xr(1),para.rec.xr(48),48);

subplot(3,1,1); hold on
plot(xax,squeeze(abs(Uu(jh,1:48))),'ko','DisplayName','|Ux| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,1:48,1,1))),'k-','DisplayName','|Ux| IBEM-DWN')
xlabel('x/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(xax,squeeze(abs(Uv(jh,1:48))),'ko','DisplayName','|Uy| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,1:48,1,2))),'k-','DisplayName','|Uy| IBEM-DWN')
xlabel('x/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(xax,squeeze(abs(Uw(jh,1:48))),'ko','DisplayName','|Uz| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,1:48,1,3))),'k-','DisplayName','|Uz| IBEM-DWN')
xlabel('x/a');ylabel('| U_z | ')

% y/a
figure(2);hold on
xax = linspace(para.rec.yr(49),para.rec.yr(96),48);

subplot(3,1,1); hold on
plot(xax,squeeze(abs(Uu(jh,49:96))),'ko','DisplayName','|Ux| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,49:96,1,1))),'k-','DisplayName','|Ux| IBEM-DWN')
xlabel('y/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(xax,squeeze(abs(Uv(jh,49:96))),'ko','DisplayName','|Uy| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,49:96,1,2))),'k-','DisplayName','|Uy| IBEM-DWN')
xlabel('y/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(xax,squeeze(abs(Uw(jh,49:96))),'ko','DisplayName','|Uz| IBEM')
plot(xax,squeeze(abs(R.uw(j+1,49:96,1,3))),'k-','DisplayName','|Uz| IBEM-DWN')
xlabel('y/a');ylabel('| U_z | ')

% load handel; sound(y,Fs)    % sonido triunfal
end
