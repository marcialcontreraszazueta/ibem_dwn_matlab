function batchGenerarEjercicio025
% Tunel con recubrimiento, 2D, IBEM-DWN y WFE 
% [Metro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 1; % 2D
para.nmed = 2; % NUmero de medios

% Propiedades del medio 1 (semiespacio)
med = 1;
para.tipoMed(med) = 2;   % Estratificado
para.reg(med).xl = 6000;     % periodicidad de la fuente, caso R
% para.reg(med).xl = 3000;     % para P y S 

% Propiedades submedio 'i' del medio 'med'
caso = 0;
disp(['***** CASO **** [',num2str(caso),']'])
switch caso
  case 0
%% GEO 0   Semiespacio homog?neo de Lutita.
i = 1;  % Semiespacio de Lutita
para.reg(med).sub(i).rho      = 2.0;
para.reg(med).sub(i).alpha    = 300;
para.reg(med).sub(i).bet      = 150;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor
  case 1
%% GEO A Un estrato sobre semiespacio: Estratos 1 y 2 con el material del estrato 3.
i = 1; %3 Gravas y boleos
para.reg(med).sub(i).rho      = 1.8;
para.reg(med).sub(i).alpha    = 33.5;
para.reg(med).sub(i).bet      = 17.9;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 8.2+2.3+9.5; %espesor

i = i+1; %4 Lutita
para.reg(med).sub(i).rho      = 2.0;
para.reg(med).sub(i).alpha    = 300;
para.reg(med).sub(i).bet      = 150;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor
  case 2
%% GEO B Dos estratos sobre semiespacio: Despreciar el estrato 2 y sustituirlo con material del estrato 1.
i = 1; %1 Limo carbonatado
para.reg(med).sub(i).rho      = 1.9;
para.reg(med).sub(i).alpha    = 32.6;
para.reg(med).sub(i).bet      = 17.4;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 8.2+2.3; %espesor

i = i+1; %3 Gravas y boleos
para.reg(med).sub(i).rho      = 1.8;
para.reg(med).sub(i).alpha    = 33.5;
para.reg(med).sub(i).bet      = 17.9;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 9.5; %espesor

i = i+1; %4 Lutita
para.reg(med).sub(i).rho      = 2.0;
para.reg(med).sub(i).alpha    = 300;
para.reg(med).sub(i).bet      = 150;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor
  case 3
%% GEO C Tres estratos sobre semiespacio: Tal como en la Tabla
i = 1; %1 Limo carbonatado
para.reg(med).sub(i).rho      = 1.9;
para.reg(med).sub(i).alpha    = 32.6;
para.reg(med).sub(i).bet      = 17.4;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 8.2; %espesor
 
i = i+1; %2 Conglomerado
para.reg(med).sub(i).rho      = 2.1;
para.reg(med).sub(i).alpha    = 98;
para.reg(med).sub(i).bet      = 52.4;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 2.3; %espesor

i = i+1; %3 Gravas y boleos
para.reg(med).sub(i).rho      = 1.8;
para.reg(med).sub(i).alpha    = 33.5;
para.reg(med).sub(i).bet      = 17.9;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
para.reg(med).sub(i).h        = 9.5; %espesor

i = i+1; %4 Lutita
para.reg(med).sub(i).rho      = 2.0;
para.reg(med).sub(i).alpha    = 300;
para.reg(med).sub(i).bet      = 150;
para.reg(med).sub(i).qd       = 1000;
para.reg(med).sub(i).tipoatts = 1; % Q
% para.reg(med).sub(i).h        = 0; %espesor
end
%% resto de materiales 
para.reg(med).nsubmed = i;
para.reg(med).sub(i).h        = 0; %ultimo estrato=semi espacio

for i=1:para.reg(med).nsubmed
  para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
  para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
  para.reg(med).sub(i).nu	      = para.reg(med).sub(i).lambda/(2*(para.reg(med).sub(i).lambda + para.reg(med).sub(i).mu));
end

% Propiedades del medio 2 (recubrimiento del t?nel)
med = 2; %Concreto
para.tipoMed(med) = 1.5;   % 1_Homogeneo 1.5_WFE 2_DWN
warning('aguas el material')
para.reg(med).rho = 2.4;
para.reg(med).alpha = 5500;
para.reg(med).bet = 2250;
para.reg(med).qd= 1000;
para.reg(med).tipoatts = 1;

para.reg(med).lambda   = para.reg(med).rho*(para.reg(med).alpha^2-2*para.reg(med).bet^2);
para.reg(med).mu       = para.reg(med).rho*para.reg(med).bet^2;
para.reg(med).nu	     = para.reg(med).lambda/(2*(para.reg(med).lambda + para.reg(med).mu));

clear med
%% GeometrIA
% nota: Las normales hacia afuera
para.pol=2; % P-SV

% Tipos de fronteras:
% 2 sin fronteras
% 3 semiespacio
% 4 dos contornos 
% 5 placa ilimitada
% 6 semi-placa L
% 7 semi-placa R
para.geo(1,1) = 2; % medio 1 sin fronteras
para.geo(1,2) = 4; % medio 2 dos contornos

% Descripcion de los contornos del medio 2
med = 2;
para.cont(med,1).xa = -5;
para.cont(med,1).a = 5;
para.cont(med,1).th = 0;
para.cont(med,1).za = 10;
para.cont(med,1).geom = 5;
para.cont(med,1).ba = 5;
para.cont(med,1).h = -5;
para.cont(med,1).ruggeo = 1;

para.cont(med,2).xa = -5;
para.cont(med,2).a = 5;
para.cont(med,2).th = 0;
para.cont(med,2).za = 10;
para.cont(med,2).geom = 5;
para.cont(med,2).ba = 5;
para.cont(med,2).h = 5;
para.cont(med,2).ruggeo = 1;

% del tunel:
para.cont(med,3).a = 5-0.6; % radio interior
para.cont(med,3).xa = 0;
para.cont(med,3).za = 10;

clear med
%% Fuentes solitas
% para.fuente=1;   % 1OndasPlanas  2FuentesPuntuales
% i=0;
% i=i+1;
% para.tipo_onda(i)=1; % P,c S, R
% para.xs(i)=  0.0;
% para.ys(i)=  0.0;
% para.zs(i)=  0.0; 
% para.gam(i)= 180;   % direccion +z
% para.phi(i)=   0;   %
% para.xzs(i) =  1; % medio al que pertenece la fuente

% para.fuente=1;   % 1OndasPlanas  2FuentesPuntuales
% i=0;
% i=i+1;
% para.tipo_onda(i)=2; % P, S, R
% para.xs(i)=  0.0;
% para.ys(i)=  0.0;
% para.zs(i)=  0.0;
% para.gam(i)= 180; % direccion +z
% para.phi(i)= 0;
% para.xzs(i) = 1;

para.fuente=2;   % 1OndasPlanas  2FuentesPuntuales
i=0;
i=i+1;
para.tipo_onda(i)=3; % P, S, R
para.xs(i)=  -60.0;
para.ys(i)=  0.0;
para.zs(i)=  0.0;
para.gam(i)= 180; % direccion +z
para.phi(i)= 0;
para.xzs(i) = 1;

para.ninc=i;
% cheat sheet:
%          +x    +y    +z
% gamma    90    90    180
% phi       0    90      0
%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  para.rafraichi = 1;
  
  para.rec.resatboundary=0;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  
  % circulo de receptores
  nr = 501;
  th = linspace(0,2*pi,nr);
  para.rec.xr= round(para.cont(2,3).xa+ (4.7)*cos(th).',5);
  para.rec.yr= zeros(nr,1);
  para.rec.zr= round(para.cont(2,3).za+ (4.7)*sin(th).',5);
  para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
%   nr = 150;
%   th = linspace(0,2*pi,nr);
%   para.rec.xr= [para.rec.xr;round(para.cont(2,3).xa+ (4.45)*cos(th).',5)];
%   para.rec.yr= [para.rec.yr;zeros(nr,1)];
%   para.rec.zr= [para.rec.zr;round(para.cont(2,3).za+ (4.45)*sin(th).',5)];
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
  
%   % lineas de receptores:
%   nr = 101;
%   para.rec.xr= linspace(-10,10,nr).';
%   para.rec.yr= zeros(nr,1);
%   para.rec.zr= zeros(nr,1);%ones(nr,1) * 5.3;
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
%   % malla plano xz
%   nrx = 70;  nrz = 11;
%   linx = linspace(-0.9,0.9,nrx);
%   linz = linspace(0,0.05,nrz);
%   [X,Z]=meshgrid(linx,linz);
%   xx = reshape(X,nrx*nrz,1);
%   zz = reshape(Z,nrx*nrz,1);
%   nr2 = nrx*nrz;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;xx*0];
%   para.rec.zr= [para.rec.zr;zz];
%   para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores
%   disp(para.rec.nrecx)
  
%   % malla plano xy
%   nrx = 40; nry = 20;
%   linx = linspace(-0.6,0.6,nrx);
%   liny = linspace(0,0.6,nry);
%   [X,Y]=meshgrid(linx,liny);
%   xx = reshape(X,nrx*nry,1);
%   yy = reshape(Y,nrx*nry,1);
%   nr3 = nrx*nry;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;yy];
%   para.rec.zr= [para.rec.zr;xx*0];
%   para.rec.nrecx= para.rec.nrecx + nr3; % cantidad de receptores
%% Analisis

disp('Frecuencias para caso inciencia onda S')
% para onda S: 
para.npplo	 = 40;
para.fmax	   = 2.5;
para.nf      = 180;
para.jini    = 0; % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df
% con diametro = 10, y lambda/diam = 21,18,15,12,9,6
% f = 150 / (lam/d) / d  / df:
para.jvec = [26,30,36,45,60,90];
% nterm para WFE  ni = 67; nfi = 82;

% disp('Frecuencias para caso inciencia onda P')
% %para onda P:
% para.npplo	 = 40;
% para.fmax	   = 5;
% para.nf      = 140;
% para.jini    = 0; % (1)=0, (2)=df, (3)=2*df, ...
% para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df
% % con diametro = 10, y lambda/diam = 21,18,15,12,9,6
% % f = 150 / (lam/d) / d  / df:
% para.jvec = [20,23,28,35,47,70];


para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =1;
para.sortie.syy =1;
para.sortie.szz =1;
para.sortie.sxy =1;
para.sortie.sxz =1;
para.sortie.syz =1;
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')
%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,~] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
sw = R.sw;
size(uw);

disp('terminado')
exit
% load handel; sound(y,Fs)    % sonido triunfal

%% cargar
clear

% referencia:
% incidencia P:
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaP/tun0/2D_PSV_OP_xs0_zs0_npplo40_DWN_20170310T130949.mat')

% incidencia S:
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaS/tun0/2D_PSV_OP_xs0_zs0_npplo30_DWN_20170310T111749.mat')

% incidencia R:
% load('../out/incidenciaR/tun0/2D_PSV_OP_xs0_zs0_npplo30_DWN_20170310T155502.mat') %semiespacio analitica
% load('../out/incidenciaR/tun0/2D_PSV_FP_xs-60_zs0_npplo30_DWN_20170310T190128.mat') % fz (-60,0) L 4000 <----
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaR/tun0/2D_PSV_FP_xs-60_zs0_npplo40_DWN_20170405T210218.mat'); % L = 6000

uw0 = uw;
sw0 = sw;
% clear sw uw
% P
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaP/tunA/2D_PSV_OP_xs0_zs0_npplo40_DWN_20170310T131624.mat'); co='r-'; tx='(a)';uw0 = uw;
% uw0 = uw;
% sw0 = sw;
% clear sw uw
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaP/tunB/2D_PSV_OP_xs0_zs0_npplo40_DWN_20170310T135656.mat'); co='b-'; tx='(b)';
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaP/tunC/2D_PSV_OP_xs0_zs0_npplo40_DWN_20170310T150246.mat'); co='k-'; tx='(c)';

% S
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaS/tunA/2D_PSV_OP_xs0_zs0_npplo30_DWN_20170310T113851.mat'); co='r-'; tx='(a)';
% uw0 = uw;
% sw0 = sw;
% clear sw uw
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaS/tunB/2D_PSV_OP_xs0_zs0_npplo30_DWN_20170310T121038.mat'); co='b-'; tx='(b)';
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaS/tunC/2D_PSV_OP_xs0_zs0_npplo30_DWN_20170310T123359.mat'); co='k-'; tx='(c)';

% R
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaR/tunA/2D_PSV_FP_xs-60_zs0_npplo30_DWN_20170310T201128.mat'); co='r-'; tx='(a)';
% uw0 = uw;
% sw0 = sw;
% clear sw uw
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaR/tunB/2D_PSV_FP_xs-60_zs0_npplo30_DWN_20170310T211551.mat'); co='b-'; tx='(b)';
% load('/Users/marshall/Documents/DOC/unit9/011Tunel/incidenciaR/tunC/2D_PSV_FP_xs-60_zs0_npplo30_DWN_20170310T215053.mat'); co='k-'; tx='(c)';

% 
df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;
disp('cargado')
%% sabana desp esf circ polares, EN FRECUENCIA
df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;
bn = 4000;
lam = para.reg(1).sub(end).lambda;
% resultados en  x
% iinc = 1;
ran = 1:nr;
nrec = length(ran);
jvec = para.jvec;
ndat = length(jvec);

med=2;
xij     = para.cont(med,3).xa-para.rec.xr;
zij     = para.cont(med,3).za-para.rec.zr;
threcF = atan2(zij,xij);

Spol = zeros(ndat,nrec,3,1); % ( Srr, Stt, Srt ) x dir
Upol = zeros(ndat,nrec,2,1); % ( Ur , Ut ) x dir
j = 1;
for ijvec = jvec
for ir = 1:nrec
  an = threcF(ir); % <-- angulo en coordenadas de la fuente.
  m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
  m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
  for dir=1
%   Upol(j,ir,1,dir) = m2(1,:) * [uw(ijvec+1,ir,dir,1);uw(ijvec+1,ir,dir,2)]; %ur
%   Upol(j,ir,2,dir) = m2(2,:) * [uw(ijvec+1,ir,dir,1);uw(ijvec+1,ir,dir,2)]; %ut
    a(1) = m2(1,:) * [uw(ijvec+1,ir,dir,1);uw(ijvec+1,ir,dir,2)]; %ur
    a(2) = m2(2,:) * [uw(ijvec+1,ir,dir,1);uw(ijvec+1,ir,dir,2)]; %ut
    a0(1) = m2(1,:) * [uw0(ijvec+1,ir,dir,1);uw0(ijvec+1,ir,dir,2)]; %ur
    a0(2) = m2(2,:) * [uw0(ijvec+1,ir,dir,1);uw0(ijvec+1,ir,dir,2)]; %ut
    Upol(j,ir,1,dir) = a(1)/a0(1);
    Upol(j,ir,2,dir) = a(2)/a0(2);
%     Upol(j,ir,1,dir) = a0(1);
%     Upol(j,ir,2,dir) = a0(2);
%     Upol(j,ir,1,dir) = a(1);
%     Upol(j,ir,2,dir) = a(2);
    
  a = m2*[[sw(ijvec+1,ir,dir,1) sw(ijvec+1,ir,dir,3)];...
          [sw(ijvec+1,ir,dir,3) sw(ijvec+1,ir,dir,2)]]*m1;
  
  a0 = m2*[[sw0(ijvec+1,ir,dir,1) sw0(ijvec+1,ir,dir,3)];...
          [sw0(ijvec+1,ir,dir,3) sw0(ijvec+1,ir,dir,2)]]*m1;
  
  Spol(j,ir,1,dir) = a(1,1)/a0(1,1);
  Spol(j,ir,2,dir) = a(2,2)/a0(2,2); % stt
  Spol(j,ir,3,dir) = a(1,2)/a0(1,2);
  
%   Spol(j,ir,1,dir) = a(1,1);
%   Spol(j,ir,2,dir) = a(2,2); % stt
%   Spol(j,ir,3,dir) = a(1,2);
  
%   Spol(j,ir,1,dir) = a0(1,1);
%   Spol(j,ir,2,dir) = a0(2,2); % stt
%   Spol(j,ir,3,dir) = a0(1,2);
  end
end

% smooth over 'ir' only 'stt'
dir = 1;
var = Spol(j,:,2,dir);
varyy = smooth(var,11);
Spol(j,:,2,dir) = varyy;

j=j+1;
end
disp('en polares')
%% graficar polares todas las frec en una grafica
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma rr';
nam{2} = '\sigma \theta\theta';
nam{3} = '\sigma rt';

% naminc{1} = ' P ';
naminc{1} = ' S ';
% naminc{1} = ' R ';

namLd = [21,18,15,12,9,6];
forma = {'k-','k--','r-','r--','b-','b--'};
grues = [0.5,1,0.5,1,0.5,0.5].*2;

for iinc = 1 % un caso a la vez
figure(bn+1000+(iinc-1)*100); %hold on; %clf; axes; hold on
set(gcf,'name',[nam{2},naminc{iinc}])
t = linspace(0,2*pi,nr); %h=polar(0,8);set(h,'Visible','off');hold on
for j = ndat:-1:1
sca = 1;%5 * 1 /  max(max(max(max(Spol(j,:,2,iinc)))));
% if j ==1 ; sca = 10; end
% if j==6; sca = 0.1; end
h=polar(t,abs(Spol(j,:,2,iinc)*sca),forma{j});
set(h,'LineWidth',grues(j),'DisplayName',['\lambda/d=',num2str(namLd(j))]);

hold on
% plot(5*cos(t),5*sin(t),'Color',[0.7 0.7 0.7]);
end
end

%% graficar polares todas las frec en una grafica DESPLAZAMIENTO
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'Ur';
nam{2} = 'U\theta';

naminc{1} = ' P ';
% naminc{1} = ' S ';
naminc{1} = ' R ';

namLd = [21,18,15,12,9,6];
forma = {'k-','k--','r-','r--','b-','b--'};
grues = [0.5,1,0.5,1,0.5,0.5].*2;
maxico = [1.5,1.5];
for icomp = 1:2
for iinc = 1 % un caso a la vez
figure(bn+1000*icomp+(iinc-1)*100); %hold on; %clf; axes; hold on
set(gcf,'name',[nam{icomp},naminc{iinc}])
t = linspace(0,2*pi,nr); h=polar(0,maxico(icomp));set(h,'Visible','off');hold on
for j = ndat:-1:1
sca = 1;%5 * 1 /  max(max(max(max(Spol(j,:,2,iinc)))));
if j==3; sca = 0.1; continue; end
h=polar(t,abs(Upol(j,:,icomp,iinc)*sca),forma{j});
set(h,'LineWidth',grues(j),'DisplayName',['\lambda/d=',num2str(namLd(j))]);

hold on
% plot(5*cos(t),5*sin(t),'Color',[0.7 0.7 0.7]);
end
end
end

%% graficar polares acumular por modelos
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma rr';
nam{2} = '\sigma \theta\theta';
nam{3} = '\sigma rt';

% naminc{1} = ' P ';
% naminc{1} = ' S ';
naminc{1} = ' R ';

namLd = [21,18,15,12,9,6];
% maxplot = [2,1.5,30,2,1.5,1]; % P
% maxplot = [0.6,0.5,0.6,1.0,1.2,15]; % S
maxplot = [3,8,4,5,15,5]; % R % llega hasta 37
for iinc = 1 % un caso a la vez
for j = 1:ndat
figure(bn+1000+(iinc-1)*100+j); %hold on; %clf; axes; hold on
t = linspace(0,2*pi,nr); h=polar(0,maxplot(j));set(h,'Visible','off');hold on

% caso S, quitar alrededor de pi/2 y 3pi/2
% killRan = [116:136,366:386];
% t(killRan)=NaN;

% caso R, quitar alrededor de pi/3 y pi
% killRan = [85-22:85+22,252-22:252+22]; % 30 grados alrededor de
% t(killRan)=NaN;

set(gcf,'name',[nam{2},naminc{iinc},'\lambda/d',num2str(namLd(j))])
sca = 1;%5 * 1 /  max(max(max(max(Spol(j,:,2,iinc)))));

h=polar(t,abs(Spol(j,:,2,iinc)*sca),co);
set(h,'LineWidth',2,'DisplayName',tx);

hold on
% plot(5*cos(t),5*sin(t),'Color',[0.7 0.7 0.7]);
end
end

%% graficar polares acumular por modelos DESPLAZAMIENTOS
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'Ur';
nam{2} = 'U\theta';

% naminc{1} = ' P ';
% naminc{1} = ' S ';
naminc{1} = ' R ';

namLd = [21,18,15,12,9,6];
% maxplot = [2.6,3,2.75,1.5,1.75,3.5]; % P
% maxplot = [2.6,2,0.6,3.5,3.5,4.5]; % S
maxplot = [1.5,2,0.6,2.5,3.5,4.5]; % R
for icomp = 1 % solo Ur
for iinc = 1 % un caso a la vez
for j = 1:ndat
if j==3;continue;end
figure(bn+1000*icomp+(iinc-1)*100+j); %hold on; %clf; axes; hold on
t = linspace(0,2*pi,nr); h=polar(0,maxplot(j));set(h,'Visible','off');hold on

% caso S, quitar alrededor de pi/2 y 3pi/2
% killRan = [116:136,366:386];
% t(killRan)=NaN;

% caso R, quitar alrededor de pi/3 y pi
% killRan = [85-22:85+22,252-22:252+22]; % 30 grados alrededor de
% t(killRan)=NaN;

set(gcf,'name',[nam{2},naminc{iinc},'\lambda/d',num2str(namLd(j))])
sca = 1;%5 * 1 /  max(max(max(max(Spol(j,:,2,iinc)))));

h=polar(t,abs(Upol(j,:,icomp,iinc)*sca),co);
set(h,'LineWidth',2,'DisplayName',tx);

hold on
% plot(5*cos(t),5*sin(t),'Color',[0.7 0.7 0.7]);
end
end
end
%% rayitas amarillas
maxplot = [0.6,0.5,0.6,1.0,1.2,15]; % S
killRan = [116:136,366:386];
t = linspace(0,2*pi,nr);
for iinc = 1 % un caso a la vez
for j = 1:ndat
figure(bn+1000+(iinc-1)*100+j); hold on;
polar([t(116) t(116)],[0 maxplot(j)],'y-')
polar([t(136) t(136)],[0 maxplot(j)],'y-')
polar([t(366) t(366)],[0 maxplot(j)],'y-')
polar([t(386) t(386)],[0 maxplot(j)],'y-')
end
end

end