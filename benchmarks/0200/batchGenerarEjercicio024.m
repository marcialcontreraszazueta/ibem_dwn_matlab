function batchGenerarEjercicio024
% Cavidad circular en semiespacio, 2D, WFE, fotogramas
% Para verificar con Ejercicio021
% [Metro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;
para.rafraichi = 1;
para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 1; % 2D
para.nmed = 1; % NUmero de medios

% Propiedades del medio 1 (espacio completo)
med = 1;
para.tipoMed(med) = 1.5;   % 1_Homogeneo 1.5_WFE 2_DWN
warning('aguas el material')
para.reg(med).rho = 2.0;
para.reg(med).alpha = 5.00;
para.reg(med).bet = 2.50;
para.reg(med).qd= 1000;
para.reg(med).tipoatts = 1;

para.reg(med).lambda   = para.reg(med).rho*(para.reg(med).alpha^2-2*para.reg(med).bet^2);
para.reg(med).mu       = para.reg(med).rho*para.reg(med).bet^2;
para.reg(med).nu	     = para.reg(med).lambda/(2*(para.reg(med).lambda + para.reg(med).mu));
para.reg(med).nsubmed  = 0;

clear med
%% GeometrIA
% nota: Las normales hacia afuera
para.pol=2; % SH, P-SV

% Tipos de fronteras:
% 2 sin fronteras
% 3 semiespacio
% 4 dos contornos 
% 5 placa ilimitada
% 6 semi-placa L
% 7 semi-placa R
para.geo(1,1) = 2; %  espacio completo

% Descripcion de los contornos del medio
med = 1;
para.cont(med,1).xa = -4; % no se usa
para.cont(med,1).a = 4;
para.cont(med,1).th = 0;
para.cont(med,1).za = 0;
para.cont(med,1).geom = 1;
para.cont(med,1).ba = 4;
para.cont(med,1).h = -4;
para.cont(med,1).ruggeo = 1;

para.cont(med,2).xa = -4;
para.cont(med,2).a = 4;
para.cont(med,2).th = 0;
para.cont(med,2).za = 0;
para.cont(med,2).geom = 1;
para.cont(med,2).ba = 4;
para.cont(med,2).h = 4;
para.cont(med,2).ruggeo = 1;

% del tunel:
para.cont(med,3).a =  4; % radio interior
para.cont(med,3).xa = 0;
para.cont(med,3).za = 0;

% 2D geometry cheatsheet  [.geom = ]
% 1   Gaussiana suave
% 2   Parabola
% 3   Triangulo
% 4   Coseno
% 5   Elipse  (circulo)
% 6   Elipse asimetrica
% 7   Trapecio
% 8   Base en los bordes
% 9   Arbitrario
clear med
%% Fuentes solitas

i=0;

i=i+1;
para.fuente(i)=2;   % 1_OndasPlanas  2_FuentesPuntuales
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=  4.0;
para.ys(i)=  0.0;
para.zs(i)=  3.0; 
para.gam(i)=  90;   % direccion +x
para.phi(i)=   0;   %
para.xzs(i) =  1; % medio al que pertenece la fuente
% 
i=i+1;
para.fuente(i)=2;   % 1_OndasPlanas  2_FuentesPuntuales
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)=  4.0;
para.ys(i)=  0.0;
para.zs(i)=  3.0; 
para.gam(i)= 180;   % direccion +z
para.phi(i)=   0;   %
para.xzs(i) =  1; % medio al que pertenece la fuente

para.ninc=i;
% cheat sheet:
%          +x    +y    +z
% gamma    90    90    180
% phi       0    90      0
%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  para.rafraichi = 1;
  
  para.rec.resatboundary=0;
  para.chgrec = 0;
  
  % circulo de receptores
  nr = 61;
  para.recpos=3; %receptores en posicion libre
  th = linspace(0,2*pi,nr);
  para.rec.xr= round(para.cont(1,3).xa+ (4.0)*cos(th).',5);
  para.rec.yr= zeros(nr,1);
  para.rec.zr= round(para.cont(1,3).za+ (4.0)*sin(th).',5);
  para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
  % lineas de receptores:
%   para.recpos=3; %receptores en posicion libre
%   nr = 101;
%   para.rec.xr= ones(nr,1) * (5);
%   para.rec.yr= zeros(nr,1);
%   para.rec.zr= linspace(-10,10,nr).';
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
  % malla plano xz
%   para.recpos = 2;
%   para.rec.nrecx = 60;
%   para.rec.nrecy = 1;
%   para.rec.nrecz = 60;
%   para.rec.xri = -10;
%   para.rec.yri = 0;
%   para.rec.zri = -10;
%   para.rec.dxr = 0.3389;
%   para.rec.dyr = 0;
%   para.rec.dzr = 0.3389;
  
  para.rafraichi = 1;
%% Analisis
para.npplo	 = 10;
para.fmax	   = 9;

para.nf      = 400;
para.jini    = 0; % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df
% 
% 1+1;
% para.jini = 100;
% para.jfin = 100;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')
%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =1;
para.sortie.syy =1;
para.sortie.szz =1;
para.sortie.sxy =1;
para.sortie.sxz =1;
para.sortie.syz =1;
%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')
%% ver dibujo:
% dibujo_estratifi(para,[],0);
% BatchScript([para.nomrep '/parameters.mat'],true); 
% return
%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
sw = R.sw;
size(uw);
% disp('saliendo')
% exit
%return;
% load handel; sound(y,Fs)    % sonido triunfal
%% load anterior
% pelicula:
% load('../out/vid1/2D_PSV_FPFP_xs-5_zs0_npplo10_WFE_20170308T054829.mat')
% df      = para.fmax/(para.nf/2);  nr   = para.rec.nrecx;
%% funcion de amplitud
para.pulso.tipo=3; %Ricker periodo caracter?stico tp
para.pulso.a = 0.3;%0.5;%0.3;    % tp
para.pulso.b = 1;    % ts
para.pulso.c = 0;  % corrimiento

% para.pulso.tipo =5; % gaussiano rise time
% para.pulso.a = 0.35;   % rise time 
% para.pulso.b = 0.5;   % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=4; % plano + tapper gaussiano
% para.pulso.a = 2;   % rise time
% para.pulso.b = 10;    % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , false);
%% inversion
[utc,stc]   = inversion_w(uw,sw,para);

bn = 4000;
%% otros
% %% sabana de desplazamientos en frecuencia
% bn = 4000;  
% iinc = 1;   
% ran = 1:nr; 
% % para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
% nam{1} = 'u';
% nam{2} = 'w';
% for icomp = 1:2
% figure(bn+icomp); hold on;% clf; axes; hold on
% set(gcf,'name',nam{icomp})
% sca = 5e3;
% for ir = ran
%   plot(Fq,real(uw(:,ir,iinc,icomp).*cspectre.')*sca+para.rec.zr(ir),'r-');
%   plot(Fq,imag(uw(:,ir,iinc,icomp).*cspectre.')*sca+para.rec.zr(ir),'b-');
%   plot(Fq,abs(uw(:,ir,iinc,icomp).*cspectre.')*sca+para.rec.zr(ir),'k-');
% end
% end
% %% sabana desplazamientos circ
% bn = 4000;
% % resultados en  x
% % iinc = 1;
% ran = 1:nr;%51; % en linea de receptores
% tran = 1:para.zeropad; % menor que zeropad
% % para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
% nam{1} = 'u';
% nam{2} = 'w';
% Hh=zeros(4,1); ii=1;
% for iinc = 1:2
% for icomp = 1:2
% figure(bn+(iinc-1)*100+icomp);   hold on;  %clf; axes; hold on
% set(gcf,'name',[nam{icomp},num2str(iinc)])
% sca = 2e8;%
% % sca = 3 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(utc(tran,ran,iinc,icomp)));
% hg = hggroup; 
% disp(max(max(utc(tran,ran,iinc,icomp))))
% for ir = ran
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+0.1*(ir-1),'r-',...
%     'LineWidth',2,'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
% %   plot(tps(tran),utc(tran,ir,iinc,icomp),'r-');
% %   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
% end
% ylim([-1 7]);xlim([1 7]); ylabel('\theta / \pi'); xlabel('tiempo (segundos)')
% Hh(ii) = hg; ii=ii+1;
% end
% end
% % delete(Hh)
% %% sabana esfuerzos circ cartesianas
% bn = 4000;
% lam = para.reg(1).lambda;
% % resultados en  x
% % iinc = 1;
% ran = 1:nr;
% tran = 1:para.zeropad; % menor que zeropad
% % para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
% nam{1} = '\sigma xx';
% nam{2} = '\sigma zz';
% nam{3} = '\sigma xz';
% Hh=zeros(4,1); ii=1;
% for iinc = 2
% for icomp = 1:3
% figure(bn+1000+(iinc-1)*100+icomp); hold on; %clf; axes; hold on
% set(gcf,'name',[nam{icomp},num2str(iinc)])
% sca = 1e8/lam;%5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(stc(tran,ran,iinc,icomp)));
% hg = hggroup; 
% for ir = ran
%   plot(tps(tran),stc(tran,ir,iinc,icomp)*sca+1*(ir-1),'k-',...
%     'LineWidth',1,'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
% %   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
% end
% ylim([-1 ir]);xlim([0 10])
% Hh(ii) = hg; ii=ii+1;
% end
% end
%% sabana desp esf circ polares
bn = 4000;
lam = para.reg(1).lambda;
% resultados en  x
% iinc = 1;
ran = 1:nr;
nrec = length(ran);
tran = 1:5000;%para.zeropad; % menor que zeropad
ndat = length(tran);

med=1;
xij     = para.cont(med,3).xa-para.rec.xr;
zij     = para.cont(med,3).za-para.rec.zr;
threcF = atan2(zij,xij);

Spol = zeros(ndat,nrec,3,2); % ( Srr, Stt, Srt ) x dir
Upol = zeros(ndat,nrec,2,2); % ( Ur , Ut ) x dir
for it = tran
for ir = 1:nrec
  an = threcF(ir); % <-- angulo en coordenadas de la fuente.
  m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
  m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
  for dir=1:2
  Upol(it,ir,1,dir) = m2(1,:) * [utc(it,ir,dir,1);utc(it,ir,dir,2)]; %ur
  Upol(it,ir,2,dir) = m2(2,:) * [utc(it,ir,dir,1);utc(it,ir,dir,2)]; %ut
  a = m2*[[stc(it,ir,dir,1) stc(it,ir,dir,3)];...
          [stc(it,ir,dir,3) stc(it,ir,dir,2)]]*m1;
  Spol(it,ir,1,dir) = a(1,1);
  Spol(it,ir,2,dir) = a(2,2);
  Spol(it,ir,3,dir) = a(1,2);
  end
end
end
% graficar polares
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma rr';
nam{2} = '\sigma tt';
nam{3} = '\sigma rt';
Hh=zeros(4,1); ii=1;
for iinc = 1:2 
for icomp = 1:2:3
figure(bn+1000+(iinc-1)*100+icomp); hold on; %clf; axes; hold on
set(gcf,'name',[nam{icomp},num2str(iinc)])
sca = 1e8/lam;%5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(stc(tran,ran,iinc,icomp)));
hg = hggroup; 
for ir = ran
  plot(tps(tran),Spol(tran,ir,icomp,iinc)*sca+1*(ir-1),'b-',...
    'LineWidth',2,'Parent',hg,'DisplayName','wfe','HandleVisibility','on');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
ylim([-1 ir]);xlim([0 10])
Hh(ii) = hg; ii=ii+1;
end
end
%% sabana desplazamientos linea
bn = 4000;
% resultados en  x
iinc = 1;
ran = 1:nr;%51; % en linea de receptores
% ran = 23:30:30*30; % en malla
tran = 1:para.zeropad; % menor que zeropad
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'u';
nam{2} = 'w';
Hh=zeros(4,1); ii=1;
for iinc = 1:2
for icomp = 1:2
figure(bn+(iinc-1)*100+icomp);   hold on   %   ; clf; axes; hold on
set(gcf,'name',nam{icomp})
sca = 1e8;%
sca = 4 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(utc(tran,ran,iinc,icomp)));
hg = hggroup; 
for ir = ran
  plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-',...
    'Parent',hg,'DisplayName','wfe','HandleVisibility','on','LineWidth',1);
%   plot(tps(tran),utc(tran,ir,iinc,icomp),'r-');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
ylim([-11 11]);xlim([2.5 8]);xlabel('tiempo (segundos)');ylabel('z')
Hh(ii) = hg; ii=ii+1;
end
end
% delete(Hh)
%% 2
% resultados en  x
iinc = 1;
ran = 1:nr; % en linea de receptores
% ran = 23:30:30*30; % en malla
tran = 1:para.zeropad; % menor que zeropad
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'u';
nam{2} = 'w';
for icomp = 1:2
figure(bn+10+icomp); clf; axes; hold on;  
set(gcf,'name',nam{icomp})
sca = 5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(utc(tran,ran,iinc,icomp)));
for ir = ran
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-');
  plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),para.rec.zr(ir));
end
end
%% sabana esfuerzos
lam = para.reg(1).lambda;
% resultados en  x
iinc = 1;
ran = 1:nr;
tran = 1:para.zeropad; % menor que zeropad
% para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma xx';
nam{2} = '\sigma zz';
nam{3} = '\sigma xz';
for icomp = 1:3
figure(bn+100+icomp); clf; axes; hold on
set(gcf,'name',nam{icomp})
sca = 1e8/lam;%5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(stc(tran,ran,iinc,icomp)));
for ir = ran
  plot(tps(tran),stc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
ylim([-11 11]);xlim([2 10])
end
%% fotogramas tiempo:
para.cont1 = para.cont;

RESULT.utc = utc;
RESULT.stc = stc;
para.film.filmStyle = 5; %color,grid,grid+shadow,elemMecEnPared,color+quiver}
para.film.filmeMecElem = 1;
para.film.fps = 50;
para.film.BoundaryWarpRange = '1:end';
para.film.filmeRange = 440:2:6700;
para.film.strmecelemlist= {'Ux'  'Uz'  'sxx'  'szz'  'sxz'};
set(0,'DefaultFigureWindowStyle','normal')
% filmoscopio2(para,RESULT,1); %fx
filmoscopio2(para,RESULT,2); %fz
end