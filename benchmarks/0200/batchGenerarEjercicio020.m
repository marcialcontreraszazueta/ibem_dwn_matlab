function batchGenerarEjercicio020
% Incidencia de fuerza puntual en espacio completo, 2D, analitico
% Para complementar con Ejercicio021
% [Metro, gramo, segundo]

%% Preambulo 
para.meth_PS=0; %calculo normal =0, calculo con equipartition =1
para.aLoMejorSiChecarLasCurvasDeDispersion = false;
para.zeropad=2^14; %Numero de muestras en sismograma
para.siDesktop=false;

para.nomcarpeta=pwd;
para.nomrep=pwd;
cd ../../ibem_matlab
para.spct=1; % 0 Hacer sismogramas, 1 solo respuesta en frecuencia
%% Materiales
para.dim = 1; % 2D
para.nmed = 1; % NUmero de medios

% Propiedades del medio 1 (espacio completo)
med = 1;
para.tipoMed(med) = 1;   % 1_Homogeneo 1.5_WFE 2_DWN
warning('aguas el material')
para.reg(med).rho = 2.0;
para.reg(med).alpha = 5.00;
para.reg(med).bet = 2.50;
para.reg(med).qd= 1000;
para.reg(med).tipoatts = 1;

para.reg(med).lambda   = para.reg(med).rho*(para.reg(med).alpha^2-2*para.reg(med).bet^2);
para.reg(med).mu       = para.reg(med).rho*para.reg(med).bet^2;
para.reg(med).nu	     = para.reg(med).lambda/(2*(para.reg(med).lambda + para.reg(med).mu));
para.reg(med).nsubmed  = 0;

clear med
%% GeometrIA
% nota: Las normales hacia afuera
para.pol=2; % SH, P-SV

% Tipos de fronteras:
% 2 sin fronteras
% 3 semiespacio
% 4 dos contornos 
% 5 placa ilimitada
% 6 semi-placa L
% 7 semi-placa R
para.geo(1,1) = 2; % medio 1 espacio completo

% Descripcion de los contornos del medio
med = 1;
para.cont(med,1).xa = -4; % no se usa
para.cont(med,1).a = 4;
para.cont(med,1).th = 0;
para.cont(med,1).za = 0;
para.cont(med,1).geom = 1;
para.cont(med,1).ba = 4;
para.cont(med,1).h = -4;
para.cont(med,1).ruggeo = 1;

para.cont(med,2).xa = -4;
para.cont(med,2).a = 4;
para.cont(med,2).th = 0;
para.cont(med,2).za = 0;
para.cont(med,2).geom = 1;
para.cont(med,2).ba = 4;
para.cont(med,2).h = 4;
para.cont(med,2).ruggeo = 1;


% 2D geometry cheatsheet  [.geom = ]
% 1   Gaussiana suave
% 2   Parabola
% 3   Triangulo
% 4   Coseno
% 5   Elipse  (circulo)
% 6   Elipse asimetrica
% 7   Trapecio
% 8   Base en los bordes
% 9   Arbitrario
clear med
%% Fuentes solitas

i=0;

i=i+1;
para.fuente(i)=2;   % 1_OndasPlanas  2_FuentesPuntuales
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)= -5.0;
para.ys(i)=  0.0;
para.zs(i)=  0.0; 
para.gam(i)=  90;   % direccion +x
para.phi(i)=   0;   %
para.xzs(i) =  1; % medio al que pertenece la fuente
% 
i=i+1;
para.fuente(i)=2;   % 1_OndasPlanas  2_FuentesPuntuales
para.tipo_onda(i)=2; % P, SV, --, R
para.xs(i)= -5.0;
para.ys(i)=  0.0;
para.zs(i)=  0.0; 
para.gam(i)= 180;   % direccion +z
para.phi(i)=   0;   %
para.xzs(i) =  1; % medio al que pertenece la fuente

para.ninc=i;
% cheat sheet:
%          +x    +y    +z
% gamma    90    90    180
% phi       0    90      0
%% Receptores
  para.rec.nrecx = 0; para.rec.xr=[]; para.rec.yr=[]; para.rec.zr=[];
  
  para.rec.resatboundary=0;
  para.recpos=3; %receptores en posicion libre
  para.chgrec = 0;
  
%   % circulo de receptores
%   nr = 300;
%   th = linspace(0,2*pi,nr);
%   para.rec.xr= round(para.cont(2,3).xa+ (4)*cos(th).',5);
%   para.rec.yr= zeros(nr,1);
%   para.rec.zr= round(para.cont(2,3).za+ (4)*sin(th).',5);
%   para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
  % lineas de receptores:
  nr = 101;
  para.rec.xr= ones(nr,1) * 5;
  para.rec.yr= zeros(nr,1);
  para.rec.zr= linspace(-10,10,nr).';
  para.rec.nrecx= para.rec.nrecx + nr; % cantidad de receptores
  
%   % malla plano xz
%   nrx = 70;  nrz = 11;
%   linx = linspace(-0.9,0.9,nrx);
%   linz = linspace(0,0.05,nrz);
%   [X,Z]=meshgrid(linx,linz);
%   xx = reshape(X,nrx*nrz,1);
%   zz = reshape(Z,nrx*nrz,1);
%   nr2 = nrx*nrz;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;xx*0];
%   para.rec.zr= [para.rec.zr;zz];
%   para.rec.nrecx= para.rec.nrecx + nr2; % cantidad de receptores
%   disp(para.rec.nrecx)
  
%   % malla plano xy
%   nrx = 40; nry = 20;
%   linx = linspace(-0.6,0.6,nrx);
%   liny = linspace(0,0.6,nry);
%   [X,Y]=meshgrid(linx,liny);
%   xx = reshape(X,nrx*nry,1);
%   yy = reshape(Y,nrx*nry,1);
%   nr3 = nrx*nry;
%   para.rec.xr= [para.rec.xr;xx];
%   para.rec.yr= [para.rec.yr;yy];
%   para.rec.zr= [para.rec.zr;xx*0];
%   para.rec.nrecx= para.rec.nrecx + nr3; % cantidad de receptores
  
%% Analisis
para.npplo	 = 10;
para.fmax	   = 9;

para.nf      = 600;
para.jini    = 0; % (1)=0, (2)=df, (3)=2*df, ...
para.jfin    = para.nf/2; % hasta (nf/2+1)=(nf/2)*df
% 
% 1+1;
% para.jini = 10;
% para.jfin = 10;

para.tmax = (para.zeropad-1)/(para.fmax/(para.nf/2)*para.zeropad);
para.tmaxinteres = para.tmax;
disp(['tmax = ', num2str(para.tmaxinteres), 's de ',num2str(para.tmax)])

df      = para.fmax/(para.nf/2);     disp(['df = ',num2str(df)])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
disp(['fini = ',num2str(para.jini * df),' ... @(',...
  num2str(para.jfin - para.jini),' x ',num2str(df),') ... ',...
  num2str(para.jfin * df),' Hertz'])
disp('---')

%% Variables de salida
% Graficar resultados parciales:
para.GraficarCadaDiscretizacion = false;
%deplacements
para.sortie.Ux	= 1;
para.sortie.Uy  = 1;
para.sortie.Uz  = 1;

para.sortie.Ut  = 1;
para.sortie.UPh = 0;
para.sortie.USh = 0;
para.sortie.UIh = 0;
para.sortie.UPt = 0;
para.sortie.USt = 0;
%contrainte
para.sortie.sxx =1;
para.sortie.syy =1;
para.sortie.szz =1;
para.sortie.sxy =1;
para.sortie.sxz =1;
para.sortie.syz =1;

%% Guardar
save([para.nomrep '/parameters.mat'],'para');
disp('saved PARA successfuly')

%% ver dibujo:
% dibujo_estratifi(para,[],0);
BatchScript([para.nomrep '/parameters.mat'],true); 
% return

%% Ejecutar
[R,para] = BatchScript([para.nomrep '/parameters.mat']);
uw = R.uw;
sw = R.sw;
size(uw);

% load handel; sound(y,Fs)    % sonido triunfal

%% load anterior
% load('/Users/marshall/Documents/DOC/ibem_dwn_matlab/out/2D_PSV_FPFP_xs-5_zs0_npplo10_Hom_20170220T001333.mat')
% df      = para.fmax/(para.nf/2); 

%% frecuencias un receptor
nf      = para.nf;
df      = para.fmax/(nf/2);
Fq      = (0:nf/2)*df;
iinc = 1;
ran = 1;
j = 1:nf/2+1; % <-- las frecuencias
nam{1} = 'u';
nam{2} = 'w';
figure(777);
hold on;
for icomp = 1:2
tx = [nam{icomp} ' at ' num2str(ran) ', iinc ' num2str(iinc)];
plot(Fq(j),imag(uw(j,ran,iinc,icomp)),'b-','Visible','off');
plot(Fq(j),real(uw(j,ran,iinc,icomp)),'r-','Visible','off');
plot(Fq(j), abs(uw(j,ran,iinc,icomp)),'k-','DisplayName',tx);
end
nam{1} = '\sigma xx';
nam{2} = '\sigma zz';
nam{3} = '\sigma xz';
for icomp = 1:3
tx = [nam{icomp} ' at ' num2str(ran) ', iinc ' num2str(iinc)];
plot(Fq(j),imag(sw(j,ran,iinc,icomp)),'b-','Visible','off');
plot(Fq(j),real(sw(j,ran,iinc,icomp)),'r-','Visible','off');
plot(Fq(j), abs(sw(j,ran,iinc,icomp)),'k-','DisplayName',tx);
end
%% sabana desplazamientos una frecuencia
iinc = 1;
ran = 1:101;
j = 51+1; % <-- la frecuencia
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'u';
nam{2} = 'w';
for icomp = 1:2
figure(9810+icomp); 
% clf; axes; 
tx = [nam{icomp} ' at ' num2str(j*df) 'Hz'];
hold on; set(gcf,'name',tx)
plot(para.rec.zr(ran),imag(uw(j,ran,iinc,icomp)),'b-');
plot(para.rec.zr(ran),real(uw(j,ran,iinc,icomp)),'r-');
plot(para.rec.zr(ran), abs(uw(j,ran,iinc,icomp)),'k-','DisplayName',tx);
end

%% sabana esfuerzos una frecuencia
iinc = 1;
ran = 1:101;
j = 150+1; % <-- la frecuencia
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma xx';
nam{2} = '\sigma zz';
nam{3} = '\sigma xz';
for icomp = 1:3
figure(9910+icomp); 
% clf; axes; 
hold on; set(gcf,'name',[nam{icomp} ' at ' num2str(j*df) 'Hz'])
plot(para.rec.zr(ran),imag(sw(j,ran,iinc,icomp)),'b-');
plot(para.rec.zr(ran),real(sw(j,ran,iinc,icomp)),'r-');
plot(para.rec.zr(ran), abs(sw(j,ran,iinc,icomp)),'k-');
end
% return
%% funcion de amplitud
para.pulso.tipo=3; %Ricker periodo caracter?stico tp
para.pulso.a = 0.3;   % tp
para.pulso.b = 1;    % ts
para.pulso.c = 0;  % corrimiento

% para.pulso.tipo =5; % gaussiano rise time
% para.pulso.a = 0.35;   % rise time 
% para.pulso.b = 0.5;   % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=4; % plano + tapper gaussiano
% para.pulso.a = 2;   % rise time
% para.pulso.b = 10;    % retraso
% para.pulso.c = 0;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

% para.pulso.tipo=7; % plano
% para.pulso.b = 10;    % retraso

[Fq,cspectre,tps,~] = showPulso( para , true);
%
 % inversion
[utc,stc]   = inversion_w(uw,sw,para);

% %% un receptor
% iinc = 1;
% recep = 102;
% tran = 1:8000;
% 
% figure(9800);
% tx = [num2str(recep),':(',num2str(para.rec.xr(recep)),' , ',...
%   num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
% set(gcf,'name',tx)
% arnam{1} = ['u_',tx]; arnam{3} = ['w_',tx];
% subplot(2,1,1); hold on
% for icomp = 1:2
%   plot(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',arnam{icomp});
%   plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
%   plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
%   plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
% end
% title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
% subplot(2,1,2); hold on
% for icomp = 1:3
%   plot(tps(tran),utc(tran,recep,iinc,icomp),'k-','DisplayName',arnam{icomp});
% end
% title('sismogramas sinteticos');xlabel('tiempo [segundos]');ylabel('amplitud')
% xlim([0 15])

%% sabana desplazamientos
% resultados en  x
iinc = 1;
ran = 1:101;
tran = 1:10000; % menor que zeropad
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'u';
nam{2} = 'w';
for icomp = 1:2
figure(9800+icomp); clf; axes; hold on
set(gcf,'name',nam{icomp})
sca = 5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(utc(tran,ran,iinc,icomp)));
for ir = ran
  plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
end
%% 2
bn = 5000;
% resultados en  x
iinc = 1;
ran = 1:5:101;
tran = 1:10000; % menor que zeropad
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = 'u';
nam{2} = 'w';
for icomp = 1:2
figure(bn+icomp); clf; axes; hold on
set(gcf,'name',nam{icomp})
sca = 5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(utc(tran,ran,iinc,icomp)));
for ir = ran
%   plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-');
  plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
end
%% sabana esfuerzos
% resultados en  x
iinc = 1;
ran = 1:101;
tran = 1:10000; % menor que zeropad
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
nam{1} = '\sigma xx';
nam{2} = '\sigma zz';
nam{3} = '\sigma xz';
for icomp = 1:3
figure(9900+icomp); clf; axes; hold on
set(gcf,'name',nam{icomp})
sca = 5 * (para.rec.zr(2)-para.rec.zr(1)) / max(max(stc(tran,ran,iinc,icomp)));
for ir = ran
  plot(tps(tran),stc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir),'k-');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.zr(ir));
end
end
end