NPPLO = 8
-------------------------------------------------------------------------------
#  # ,   h     alpha        beta         rho  , qmod   Q , lambda          mu         nu
1    |          6.0         3.4         2.7|  1  10000 | 3.445e+01      3.121e+01  0.262
----------------------------------------------------------------------------------------
F(1) at m=1
F(2) at m=1
F(3) at m=1
F(4) at m=1
F(5) at m=1
El domino de cada receptor fue indicado en el archivo de datos
-Receptores--------------------------------------------------------------------
  1.000         0.459         0.623        -0.001         1.000
  2.000         0.367         0.531        -0.007         1.000
  3.000         0.271         0.406        -0.014         1.000
  4.000         0.234         0.317        -0.030         1.000
  5.000         0.187         0.228        -0.018         1.000
  6.000         0.157         0.172        -0.010         1.000
  7.000         0.111         0.102        -0.015         1.000
  8.000         0.081         0.069        -0.016         1.000
  9.000         0.036         0.015        -0.013         1.000
 10.000         0.020        -0.040        -0.014         1.000
 11.000         0.016        -0.092        -0.016         1.000
 12.000         0.017        -0.141        -0.019         1.000
 13.000         0.186         0.192        -0.035         1.000
 14.000         0.045        -0.180        -0.065         1.000
 15.000         0.120        -0.233        -0.187         1.000
 16.000         0.435         0.560        -0.022         1.000
 17.000         0.341         0.449        -0.034         1.000
 18.000         0.269         0.352        -0.047         1.000
 19.000         0.234         0.263        -0.053         1.000
 20.000         0.393         0.412        -0.070         1.000
 21.000         0.479         0.430        -0.072         1.000
 22.000         0.495         0.496        -0.052         1.000
 23.000         0.514         0.628        -0.001         1.000
 24.000         0.537         0.580        -0.019         1.000
 25.000         0.541         0.511        -0.049         1.000
 26.000         0.584         0.581        -0.017         1.000
 27.000         0.607         0.617         0.001         1.000
 28.000         0.654         0.630        -0.001         1.000
 29.000         0.677         0.580        -0.016         1.000
 30.000         0.607         0.539        -0.039         1.000
 31.000         0.654         0.526        -0.037         1.000
 32.000         0.588         0.490        -0.057         1.000
 33.000         0.594         0.371        -0.085         1.000
 34.000         0.617         0.432        -0.065         1.000
 35.000         0.660         0.459        -0.052         1.000
 36.000         0.525         0.371        -0.093         1.000
 37.000         0.393         0.329        -0.090         1.000
 38.000         0.474         0.313        -0.106         1.000
 39.000         0.339         0.312        -0.088         1.000
 40.000         0.258         0.245        -0.080         1.000
 41.000         0.328         0.259        -0.098         1.000
 42.000         0.233         0.192        -0.075         1.000
 43.000         0.257         0.176        -0.111         1.000
 44.000         0.328         0.175        -0.134         1.000
 45.000         0.374         0.205        -0.116         1.000
 46.000         0.462         0.205        -0.134         1.000
 47.000         0.509         0.260        -0.126         1.000
 48.000         0.157         0.120        -0.045         1.000
 49.000         0.186         0.089        -0.080         1.000
 50.000         0.233         0.123        -0.108         1.000
 51.000         0.256         0.088        -0.149         1.000
 52.000         0.083         0.033        -0.041         1.000
 53.000         0.117         0.018        -0.083         1.000
 54.000         0.045        -0.018        -0.034         1.000
 55.000         0.035        -0.075        -0.037         1.000
 56.000         0.080        -0.035        -0.067         1.000
 57.000         0.105        -0.018        -0.088         1.000
 58.000         0.070        -0.070        -0.072         1.000
 59.000         0.032        -0.123        -0.046         1.000
 60.000         0.065        -0.141        -0.090         1.000
 61.000         0.087        -0.088        -0.095         1.000
 62.000         0.086        -0.123        -0.103         1.000
 63.000         0.133        -0.035        -0.118         1.000
 64.000         0.125        -0.070        -0.121         1.000
 65.000         0.078        -0.180        -0.111         1.000
 66.000         0.109        -0.180        -0.136         1.000
 67.000         0.121        -0.198        -0.167         1.000
 68.000         0.118        -0.141        -0.134         1.000
 69.000         0.150        -0.088        -0.146         1.000
 70.000         0.145        -0.123        -0.152         1.000
 71.000         0.162         0.018        -0.108         1.000
 72.000         0.187         0.036        -0.100         1.000
 73.000         0.224         0.017        -0.123         1.000
 74.000         0.246         0.035        -0.144         1.000
 75.000         0.168        -0.018        -0.129         1.000
 76.000         0.193        -0.035        -0.149         1.000
 77.000         0.224        -0.017        -0.152         1.000
 78.000         0.183        -0.070        -0.164         1.000
 79.000         0.162        -0.176        -0.195         1.000
 80.000         0.176        -0.141        -0.182         1.000
 81.000         0.198        -0.087        -0.185         1.000
 82.000         0.196        -0.123        -0.198         1.000
 83.000         0.247        -0.038        -0.175         1.000
 84.000         0.278         0.018        -0.177         1.000
 85.000         0.279        -0.021        -0.180         1.000
 86.000         0.231        -0.073        -0.192         1.000
 87.000         0.229        -0.130        -0.210         1.000
 88.000         0.255        -0.098        -0.205         1.000
 89.000         0.160        -0.229        -0.218         1.000
 90.000         0.181        -0.194        -0.215         1.000
 91.000         0.212        -0.194        -0.218         1.000
 92.000         0.238        -0.165        -0.215         1.000
 93.000         0.184        -0.255        -0.221         1.000
 94.000         0.222        -0.268        -0.218         1.000
 95.000         0.252        -0.259        -0.218         1.000
 96.000         0.228        -0.241        -0.218         1.000
 97.000         0.261        -0.200        -0.218         1.000
 98.000         0.272        -0.290        -0.213         1.000
 99.000         0.301        -0.295        -0.205         1.000
100.000         0.322        -0.286        -0.208         1.000
101.000         0.301        -0.222        -0.215         1.000
102.000         0.303         0.108        -0.185         1.000
103.000         0.350         0.142        -0.167         1.000
104.000         0.411         0.124        -0.164         1.000
105.000         0.476         0.158        -0.149         1.000
106.000         0.311         0.055        -0.192         1.000
107.000         0.411         0.077        -0.182         1.000
108.000         0.359         0.042        -0.192         1.000
109.000         0.315        -0.038        -0.198         1.000
110.000         0.362        -0.013        -0.195         1.000
111.000         0.306        -0.098        -0.205         1.000
112.000         0.332        -0.074        -0.203         1.000
113.000         0.325        -0.187        -0.213         1.000
114.000         0.327        -0.130        -0.210         1.000
115.000         0.531         0.141        -0.157         1.000
116.000         0.457         0.040        -0.169         1.000
117.000         0.511         0.070        -0.159         1.000
118.000         0.399        -0.030        -0.192         1.000
119.000         0.442        -0.013        -0.177         1.000
120.000         0.378        -0.070        -0.198         1.000
121.000         0.397        -0.106        -0.192         1.000
122.000         0.372        -0.141        -0.203         1.000
123.000         0.370        -0.198        -0.205         1.000
124.000         0.442        -0.104        -0.185         1.000
125.000         0.465        -0.051        -0.175         1.000
126.000         0.544         0.016        -0.159         1.000
127.000         0.513        -0.052        -0.167         1.000
128.000         0.462        -0.157        -0.187         1.000
129.000         0.537        -0.116        -0.175         1.000
130.000         0.509        -0.168        -0.185         1.000
131.000         0.415        -0.215        -0.198         1.000
132.000         0.370        -0.303        -0.198         1.000
133.000         0.416        -0.268        -0.198         1.000
134.000         0.463        -0.282        -0.190         1.000
135.000         0.518        -0.264        -0.190         1.000
136.000         0.541        -0.222        -0.187         1.000
137.000         0.605        -0.240        -0.182         1.000
138.000         0.619        -0.207        -0.177         1.000
139.000         0.584        -0.138        -0.172         1.000
140.000         0.689        -0.217        -0.172         1.000
141.000         0.630        -0.114        -0.167         1.000
142.000         0.700        -0.169        -0.170         1.000
143.000         0.842        -0.163        -0.165         1.000
144.000         0.770        -0.150        -0.167         1.000
145.000         0.982        -0.113        -0.146         1.000
146.000         1.120        -0.087        -0.126         1.000
147.000         1.231        -0.054        -0.111         1.000
148.000         1.332        -0.001        -0.090         1.000
149.000         1.440         0.041        -0.072         1.000
150.000         1.487         0.054        -0.065         1.000
151.000         1.579         0.066        -0.057         1.000
152.000         0.908        -0.107        -0.157         1.000
153.000         1.050        -0.069        -0.134         1.000
154.000         1.174        -0.044        -0.116         1.000
155.000         1.274        -0.001        -0.098         1.000
156.000         1.385         0.053        -0.075         1.000
157.000         1.523         0.083        -0.057         1.000
158.000         0.770        -0.072        -0.162         1.000
159.000         0.677        -0.054        -0.159         1.000
160.000         0.860        -0.049        -0.154         1.000
161.000         0.883         0.011        -0.141         1.000
162.000         0.638         0.021        -0.157         1.000
163.000         0.705         0.093        -0.141         1.000
164.000         0.659         0.161        -0.141         1.000
165.000         0.586         0.179        -0.154         1.000
166.000         0.814         0.081        -0.131         1.000
167.000         1.046        -0.002        -0.123         1.000
168.000         0.976         0.031        -0.126         1.000
169.000         1.116         0.033        -0.111         1.000
170.000         0.861         0.137        -0.108         1.000
171.000         0.982         0.102        -0.108         1.000
172.000         0.958         0.201        -0.080         1.000
173.000         0.720         0.214        -0.118         1.000
174.000         0.861         0.208        -0.088         1.000
175.000         0.814         0.235        -0.090         1.000
176.000         0.935         0.137        -0.100         1.000
177.000         0.934         0.242        -0.072         1.000
178.000         1.046         0.131        -0.106         1.000
179.000         1.174         0.008        -0.111         1.000
180.000         1.111         0.095        -0.111         1.000
181.000         1.455         0.190        -0.057         1.000
182.000         1.220         0.053        -0.098         1.000
183.000         1.180         0.106        -0.103         1.000
184.000         1.046         0.190        -0.098         1.000
185.000         1.005         0.225        -0.080         1.000
186.000         0.863         0.314        -0.070         1.000
187.000         0.723         0.293        -0.098         1.000
188.000         0.579         0.251        -0.139         1.000
189.000         0.613         0.303        -0.111         1.000
190.000         0.683         0.328        -0.090         1.000
191.000         0.816         0.352        -0.065         1.000
192.000         0.957         0.316        -0.062         1.000
193.000         0.934         0.361        -0.057         1.000
194.000         1.027         0.295        -0.067         1.000
195.000         1.004         0.345        -0.057         1.000
196.000         1.097         0.226        -0.095         1.000
197.000         1.114         0.296        -0.072         1.000
198.000         1.086         0.331        -0.062         1.000
199.000         1.207         0.158        -0.093         1.000
200.000         1.166         0.211        -0.093         1.000
201.000         1.242         0.263        -0.080         1.000
202.000         1.208         0.316        -0.072         1.000
203.000         1.523         0.155        -0.052         1.000
204.000         1.275         0.123        -0.080         1.000
205.000         1.314         0.158        -0.075         1.000
206.000         1.377         0.123        -0.070         1.000
207.000         1.322         0.245        -0.075         1.000
208.000         1.263         0.053        -0.090         1.000
209.000         1.683         0.113        -0.044         1.000
210.000         1.774         0.157        -0.029         1.000
211.000         1.844         0.229        -0.009         1.000
212.000         1.914         0.325         0.004         1.000
213.000         1.923         0.481        -0.001         1.000
214.000         1.902         0.626         0.002         1.000
215.000         1.825         0.762         0.004         1.000
216.000         1.756         0.880         0.001         1.000
217.000         1.681         0.974         0.006         1.000
218.000         1.625         1.027         0.001         1.000
219.000         0.093        -1.030        -0.028         1.000
220.000         0.112        -0.798        -0.057         1.000
221.000        -0.053        -0.617        -0.051         1.000
222.000        -0.070        -0.726        -0.034         1.000
223.000         1.561         1.110         0.001         1.000
224.000         1.513         1.145        -0.022         1.000
225.000         1.423         1.150        -0.019         1.000
226.000         1.378         1.133         0.002         1.000
227.000         1.313         1.068         0.007         1.000
228.000         1.290         0.999        -0.009         1.000
229.000         1.240         0.913        -0.013         1.000
230.000         1.149         0.844        -0.011         1.000
231.000         1.033         0.754         0.006         1.000
232.000         0.942         0.704         0.011         1.000
233.000         0.895         0.695         0.008         1.000
234.000         0.817         0.658         0.013         1.000
235.000         0.747         0.637         0.005         1.000
236.000         0.794         0.606         0.007         1.000
237.000         0.872         0.640         0.008         1.000
238.000         0.895         0.572        -0.006         1.000
239.000         0.724         0.576        -0.011         1.000
240.000         0.747         0.485        -0.031         1.000
241.000         0.706         0.423        -0.052         1.000
242.000         0.817         0.541        -0.009         1.000
243.000         0.794         0.481        -0.031         1.000
244.000         0.864         0.525        -0.017         1.000
245.000         0.840         0.428        -0.047         1.000
246.000         0.887         0.465        -0.039         1.000
247.000         0.986         0.669        -0.006         1.000
248.000         1.025         0.702        -0.012         1.000
249.000         0.952         0.556        -0.026         1.000
250.000         0.996         0.598        -0.029         1.000
251.000         0.944         0.477        -0.042         1.000
252.000         0.967         0.442        -0.049         1.000
253.000         1.014         0.433        -0.049         1.000
254.000         1.059         0.563        -0.049         1.000
255.000         1.061         0.475        -0.054         1.000
256.000         1.110         0.422        -0.057         1.000
257.000         1.180         0.480        -0.065         1.000
258.000         1.101         0.754        -0.014         1.000
259.000         1.146         0.792        -0.024         1.000
260.000         1.093         0.702        -0.031         1.000
261.000         1.139         0.665        -0.039         1.000
262.000         1.119         0.595        -0.049         1.000
263.000         1.191         0.564        -0.062         1.000
264.000         1.216         0.879        -0.031         1.000
265.000         1.240         0.862        -0.060         1.000
266.000         1.211         0.810        -0.057         1.000
267.000         1.245         0.774        -0.065         1.000
268.000         1.221         0.700        -0.052         1.000
269.000         1.277         0.665        -0.067         1.000
270.000         1.267         0.599        -0.067         1.000
271.000         1.337         0.567        -0.072         1.000
272.000         1.231         0.444        -0.072         1.000
273.000         1.254         0.369        -0.075         1.000
274.000         1.312         0.496        -0.075         1.000
275.000         1.359         0.457        -0.070         1.000
276.000         1.401         0.491        -0.060         1.000
277.000         1.335         0.385        -0.080         1.000
278.000         1.395         0.350        -0.067         1.000
279.000         1.387         0.281        -0.067         1.000
280.000         1.457         0.261        -0.054         1.000
281.000         1.287         0.946        -0.029         1.000
282.000         1.323         0.930        -0.060         1.000
283.000         1.301         0.880        -0.072         1.000
284.000         1.344         0.845        -0.088         1.000
285.000         1.317         0.792        -0.083         1.000
286.000         1.375         0.739        -0.077         1.000
287.000         1.359         0.686        -0.075         1.000
288.000         1.443         0.618        -0.067         1.000
289.000         1.430         0.565        -0.065         1.000
290.000         1.447         0.493        -0.060         1.000
291.000         1.494         0.459        -0.049         1.000
292.000         1.465         0.386        -0.049         1.000
293.000         1.511         0.331        -0.042         1.000
294.000         1.313         1.030        -0.012         1.000
295.000         1.347         1.068        -0.009         1.000
296.000         1.370         1.086        -0.008         1.000
297.000         1.347         1.030        -0.027         1.000
298.000         1.370         1.019        -0.052         1.000
299.000         1.403         1.068        -0.026         1.000
300.000         1.403         1.040        -0.052         1.000
301.000         1.367         0.966        -0.072         1.000
302.000         1.432         1.103        -0.034         1.000
303.000         1.469         1.109        -0.044         1.000
304.000         1.490         1.056        -0.044         1.000
305.000         1.453         1.022        -0.060         1.000
306.000         1.483         0.969        -0.065         1.000
307.000         1.430         0.934        -0.077         1.000
308.000         1.420         0.827        -0.085         1.000
309.000         1.461         0.862        -0.075         1.000
310.000         1.443         0.757        -0.077         1.000
311.000         1.497         0.722        -0.062         1.000
312.000         1.514         0.636        -0.054         1.000
313.000         1.467         1.186        -0.011         1.000
314.000         1.527         1.062        -0.039         1.000
315.000         1.560         1.044        -0.022         1.000
316.000         1.581         1.021        -0.022         1.000
317.000         1.638         0.991        -0.009         1.000
318.000         1.572         0.968        -0.042         1.000
319.000         1.614         0.950        -0.029         1.000
320.000         1.544         0.933        -0.052         1.000
321.000         1.556         0.862        -0.054         1.000
322.000         1.526         0.827        -0.065         1.000
323.000         1.540         0.757        -0.060         1.000
324.000         1.700         0.915        -0.006         1.000
325.000         1.651         0.898        -0.026         1.000
326.000         1.622         0.845        -0.042         1.000
327.000         1.758         0.810        -0.006         1.000
328.000         1.672         0.792        -0.029         1.000
329.000         1.621         0.739        -0.037         1.000
330.000         1.818         0.691        -0.001         1.000
331.000         1.862         0.569        -0.003         1.000
332.000         1.760         0.581        -0.013         1.000
333.000         1.738         0.651        -0.013         1.000
334.000         1.659         0.669        -0.026         1.000
335.000         1.884         0.422        -0.003         1.000
336.000         1.799         0.440        -0.008         1.000
337.000         1.736         0.510        -0.016         1.000
338.000         1.649         0.513        -0.031         1.000
339.000         1.594         0.601        -0.042         1.000
340.000         1.806         0.362        -0.013         1.000
341.000         1.860         0.300        -0.004         1.000
342.000         1.744         0.337        -0.021         1.000
343.000         1.798         0.220        -0.024         1.000
344.000         1.751         0.265        -0.026         1.000
345.000         1.728         0.164        -0.034         1.000
346.000         1.625         0.113        -0.049         1.000
347.000         1.600         0.180        -0.044         1.000
348.000         1.704         0.227        -0.031         1.000
349.000         1.634         0.243        -0.037         1.000
350.000         1.587         0.324        -0.034         1.000
351.000         1.650         0.380        -0.024         1.000
352.000         1.603         0.460        -0.031         1.000
353.000         0.594        -0.378        -0.166         1.000
354.000         0.649        -0.565        -0.127         1.000
355.000         0.697        -0.440        -0.143         1.000
356.000         0.848        -0.377        -0.135         1.000
357.000         0.607        -0.299        -0.179         1.000
358.000         0.887        -0.263        -0.148         1.000
359.000         0.776        -0.246        -0.164         1.000
360.000         1.120        -0.500        -0.059         1.000
361.000         0.951        -0.459        -0.102         1.000
362.000         0.949        -0.736        -0.036         1.000
363.000         1.228        -0.366        -0.060         1.000
364.000         1.184        -0.194        -0.103         1.000
365.000         1.031        -0.193        -0.136         1.000
366.000         0.666        -0.281        -0.174         1.000
367.000         1.413        -0.377        -0.017         1.000
368.000         1.842        -0.125        -0.014         1.000
369.000         1.318        -0.089        -0.088         1.000
370.000         1.537        -0.199        -0.035         1.000
371.000         1.699        -0.236        -0.014         1.000
372.000         1.145        -0.664        -0.016         1.000
373.000         1.486        -0.083        -0.063         1.000
374.000         1.566        -0.011        -0.062         1.000
375.000         1.761        -0.015        -0.034         1.000
376.000         1.831         0.092        -0.014         1.000
377.000         0.339        -0.463        -0.141         1.000
378.000         0.329        -0.651        -0.107         1.000
379.000         0.459        -0.379        -0.176         1.000
380.000         0.071        -0.533        -0.096         1.000
381.000         0.485        -0.692        -0.116         1.000
382.000         0.593        -0.885        -0.054         1.000
383.000         0.672        -0.985        -0.016         1.000
384.000         0.456        -0.899        -0.066         1.000
385.000         0.280        -1.090        -0.028         1.000
386.000         0.878        -0.879        -0.016         1.000
387.000         0.707        -0.677        -0.094         1.000
388.000         0.858        -0.635        -0.076         1.000
389.000         1.800        -0.429         0.001         1.000
390.000         0.020        -1.416         0.002         1.000
391.000         1.244        -1.704         0.002         1.000
392.000         1.392        -1.852         0.002         1.000
393.000         0.800        -1.704         0.002         1.000
394.000         0.948        -1.852         0.002         1.000
395.000         0.361        -1.680         0.002         1.000
396.000         0.509        -1.828         0.002         1.000
397.000        -0.044        -1.598         0.002         1.000
398.000         0.105        -1.764         0.002         1.000
399.000         1.693        -0.708         0.002         1.000
400.000         1.837        -0.963         0.002         1.000
401.000        -0.464        -1.219         0.002         1.000
402.000        -0.351        -1.367         0.002         1.000
403.000        -0.089        -1.106         0.001         1.000
404.000        -0.193        -0.908        -0.005         1.000
405.000         0.246        -1.314         0.001         1.000
406.000         1.244        -1.259         0.002         1.000
407.000         1.392        -1.407         0.002         1.000
408.000         1.688        -1.259         0.002         1.000
409.000         1.837        -1.407         0.002         1.000
410.000        -0.128        -1.268         0.002         1.000
411.000         1.468        -0.693         0.004         1.000
412.000        -0.896        -0.736         0.026         1.000
413.000        -0.723        -0.875         0.002         1.000
414.000        -0.471        -0.774        -0.005         1.000
415.000        -0.344        -0.922        -0.005         1.000
416.000         1.307        -0.847         0.004         1.000
417.000         2.133        -0.815         0.002         1.000
418.000         2.281        -0.963         0.002         1.000
419.000         1.003        -0.969         0.001         1.000
420.000        -0.980        -0.314         0.108         1.000
421.000        -0.792        -0.487         0.050         1.000
422.000        -0.512        -0.370         0.026         1.000
423.000        -0.364        -0.519        -0.005         1.000
424.000         2.204        -0.370         0.002         1.000
425.000         2.281        -0.519         0.002         1.000
426.000         1.010        -1.219         0.002         1.000
427.000        -1.020         0.115         0.176         1.000
428.000        -0.848        -0.049         0.142         1.000
429.000        -0.534         0.074         0.108         1.000
430.000        -0.386        -0.074         0.050         1.000
431.000         2.204        -0.153         0.002         1.000
432.000         2.355         0.035         0.002         1.000
433.000         2.207        -0.044         0.002         1.000
434.000         0.862        -1.367         0.002         1.000
435.000        -1.000         0.564         0.176         1.000
436.000        -0.841         0.377         0.176         1.000
437.000        -0.508         0.499         0.176         1.000
438.000        -0.373         0.360         0.142         1.000
439.000        -0.077         0.509         0.108         1.000
440.000         0.024         0.412         0.074         1.000
441.000         2.281         0.585         0.002         1.000
442.000         2.355         0.503         0.002         1.000
443.000         0.649        -1.224         0.001         1.000
444.000        -0.874         1.010         0.176         1.000
445.000        -0.768         0.807         0.176         1.000
446.000        -0.448         0.906         0.176         1.000
447.000        -0.347         0.785         0.176         1.000
448.000        -0.064         0.943         0.176         1.000
449.000         0.072         0.805         0.142         1.000
450.000         0.355         0.963         0.108         1.000
451.000         0.479         0.861         0.074         1.000
452.000         2.059         0.839         0.002         1.000
453.000         2.281         0.963         0.002         1.000
454.000         2.207         0.823         0.002         1.000
455.000         0.438        -1.412         0.002         1.000
456.000        -0.702         1.289         0.176         1.000
457.000        -0.432         1.385         0.176         1.000
458.000        -0.283         1.204         0.176         1.000
459.000        -0.047         1.390         0.176         1.000
460.000         0.072         1.249         0.176         1.000
461.000         0.355         1.407         0.176         1.000
462.000         0.503         1.259         0.142         1.000
463.000         0.725         1.259         0.108         1.000
464.000         0.948         1.407         0.108         1.000
465.000         0.874         1.259         0.108         1.000
466.000         1.170         1.290         0.062         1.000
467.000         1.392         1.469         0.050         1.000
468.000         1.318         1.352         0.038         1.000
469.000         1.688         1.469         0.026         1.000
470.000         1.837         1.321         0.002         1.000
471.000         2.099         1.407         0.002         1.000
472.000         2.247         1.259         0.002         1.000
473.000         1.237        -1.010         0.002         1.000
474.000        -0.351         1.724         0.176         1.000
475.000        -0.124         1.889         0.176         1.000
476.000         0.032         1.736         0.176         1.000
477.000         0.271         1.931         0.176         1.000
478.000         0.475         1.743         0.176         1.000
479.000         0.771         1.931         0.176         1.000
480.000         0.948         1.743         0.142         1.000
481.000         1.244         1.899         0.108         1.000
482.000         1.393         1.711         0.074         1.000
483.000         1.698         1.839         0.074         1.000
484.000         1.846         1.684         0.050         1.000
485.000         2.108         1.684         0.026         1.000
486.000         2.014        -0.387         0.002         1.000
487.000         2.015        -0.170         0.002         1.000
488.000         1.953         0.197         0.006         1.000
489.000        -0.158        -0.349        -0.000         1.000
490.000        -0.716        -1.171         0.002         1.000
491.000        -0.311        -1.599         0.002         1.000
492.000         1.688        -1.704         0.002         1.000
493.000         2.133        -1.259         0.002         1.000
494.000         0.991         1.049         0.064         1.000
495.000         0.898         0.863         0.036         1.000
496.000         0.886         0.987         0.055         1.000
497.000         0.769         1.049         0.064         1.000
498.000         0.821         0.784         0.024         1.000
499.000         0.671         0.947         0.064         1.000
500.000         0.710         0.806         0.043         1.000
501.000         0.637         0.707         0.033         1.000
502.000         0.543         0.707         0.033         1.000
503.000         0.473         0.723         0.042         1.000
504.000         0.378         0.716         0.063         1.000
505.000         1.908         0.821         0.007         1.000
506.000         1.963         0.654         0.004         1.000
507.000         0.364         0.656         0.033         1.000
508.000         0.296         0.587         0.035         1.000
509.000         0.198         0.498         0.055         1.000
510.000         0.200         0.399         0.033         1.000
511.000         0.174         0.327         0.022         1.000
512.000         0.151         0.275         0.026         1.000
513.000         0.072         0.250         0.044         1.000
514.000         0.056         0.148         0.025         1.000
515.000         0.079         0.183         0.025         1.000
516.000        -0.054         0.254         0.062         1.000
517.000        -0.088         0.138         0.037         1.000
518.000        -0.119         0.111         0.038         1.000
519.000         0.014         0.099         0.020         1.000
520.000         1.963         0.777         0.004         1.000
521.000         1.930         0.957         0.004         1.000
522.000         1.846         0.969         0.009         1.000
523.000         1.815         1.093         0.006         1.000
524.000         1.690         1.075         0.011         1.000
525.000         1.624         1.159         0.011         1.000
526.000         1.600         1.239         0.006         1.000
527.000         1.463         1.234         0.018         1.000
528.000         1.417         1.201         0.018         1.000
529.000         1.221         1.151         0.050         1.000
530.000         1.350         1.170         0.017         1.000
531.000         1.299         1.150         0.029         1.000
532.000         1.286         1.100         0.021         1.000
533.000         1.208         1.101         0.043         1.000
534.000         1.236         0.997         0.014         1.000
535.000         1.206         0.943         0.009         1.000
536.000         1.112         0.860         0.014         1.000
537.000         1.145         0.926         0.016         1.000
538.000         1.049         0.836         0.021         1.000
539.000         1.016         0.785         0.021         1.000
540.000         0.964         0.767         0.021         1.000
541.000         0.997         0.853         0.028         1.000
542.000         1.083         0.936         0.031         1.000
543.000         1.089         1.008         0.048         1.000
544.000         1.181         1.052         0.038         1.000
545.000         0.036        -0.435        -0.121         1.000
546.000        -0.021        -0.426        -0.098         1.000
547.000         0.085        -0.280        -0.164         1.000
548.000         0.011        -0.253        -0.057         1.000
549.000        -0.052        -0.320        -0.024         1.000
550.000        -0.076        -0.442        -0.042         1.000
551.000        -0.143        -0.535        -0.020         1.000
552.000        -0.042        -0.067         0.021         1.000
553.000        -0.082         0.023         0.028         1.000
554.000        -0.161         0.097         0.052         1.000
555.000        -0.122        -0.141         0.021         1.000
556.000        -0.201        -0.014         0.040         1.000
557.000         0.107        -0.348        -0.181         1.000
558.000         0.176        -0.327        -0.195         1.000
559.000         0.243        -0.345        -0.185         1.000
560.000         0.203        -0.433        -0.142         1.000
561.000         0.026        -0.322        -0.119         1.000
562.000        -0.121        -0.603        -0.036         1.000
563.000         0.003        -0.519        -0.081         1.000
564.000         2.058         0.592         0.002         1.000
565.000         2.132         0.486         0.002         1.000
566.000         2.059         0.251         0.002         1.000
567.000         2.133         0.162         0.002         1.000
568.000         2.281         0.315         0.002         1.000
569.000         2.355         0.257         0.002         1.000
-------------------------------------------------------------------------------
omega_i = 0
tmax de interes = 99.9939
  100 frecuencias @0.01 Hz
Starting parallel pool (parpool) using the 'local' profile ... connected to 4 workers.
[100/100  1.000 Hz] 0.024119
Resultados uw sw para (en frecuencia) guardados en archivo
/Users/marshall/Documents/DOC/ibem_dwn_matlab/out/gen3D_OP_xs0_zs0_npplo8_Hom_20181211T135122.mat
