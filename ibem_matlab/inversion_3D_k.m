function [ut,st]=inversion_3D_k(para,phi_fv,gaussian,DWN)
%% inversion_3D_k
coord = para.coord;
%%%%%%%%%%%%%
% unpacking %
%%%%%%%%%%%%%

% Num de inclusiones y de ecuaciones
ninc    = para.ninc;
nbeq    = coord.nbeq;


%rango de las incidencias
rangoIINC = 1:ninc;


% coordenadas de los receptores
xr      = para.rec.xr;
yr      = para.rec.yr;
zr      = para.rec.zr;
nrec    = para.rec.nrec;

% coordenadas de la fuente
xs      = para.xs;
ys      = para.ys;
zs      = para.zs;

if para.fuente==1 % onda plana
  kxk     = para.kxk;
  kyk     = para.kyk;
  polOP   = para.tipo_onda;
else              % fuente puntual
  fij     = para.fij;
end


% variables de salida
sal     = para.sortie;
ns      =(sal.Ux + sal.Uy + sal.Uz);
ut      = zeros(nrec,ninc,ns);
% utonlydiff  = zeros(nrec,ninc,ns);
nss     = sal.sxx + sal.syy + sal.szz+sal.sxy+sal.sxz+sal.syz;
st      = zeros(nrec,ninc,nss);

% calculo de las soluciones a partir de los coeficientes
if para.dim == 3
nm = para.nmed0;
else
nm = para.nmed;
end
for m=1:nm
  %el receptor esta en el medio m
  irec        = para.rec.m(m).ind;
  nrecm       = length(irec);
  mmat        = m;%para.subm1(m);
  if sum(irec) == 0; continue; end
  if nrecm~=0
    coordr.x	= xr(irec).';
    coordr.y	= yr(irec).';
    coordr.z	= zr(irec).';
    coordr.nbpt = nrecm;
    
%     %% testme
%     figure(654750234);clf;hold on;set(gca,'Zdir','reverse'); 
%     axis equal;quiver3(coordr.x,coordr.y,coordr.z,U0(1,:,1),U0(2,:,1),U0(3,:,1));view(3);grid on;    
%         
    % campo incidente
    U0=zeros(3  ,nrecm,ninc);S0=zeros(nss,nrecm,ninc);
    for iinc=rangoIINC
      salu=logical(ones(nrecm,1)*(para.sortie.Ut));
      sals=logical(ones(nrecm,1)*max([para.sortie.sxx,para.sortie.syy,para.sortie.szz, ...
        para.sortie.sxy,para.sortie.sxz,para.sortie.syz]));
      
      if para.xzs(iinc)==m %fuente en m    m==1%
        if para.tipoMed(m)==2 % Medio Estratificado (DWN)
          %calculo ya hecho anteriormente en vector_fuente_3D
          %se recupera el valor
          U0(:,:,iinc)      = DWN{m}.U0(:,:,iinc);%(3  ,para.rec.m(1).nr,para.ninc)
          S0(:,:,iinc)      = DWN{m}.S0(:,:,iinc);%(nss,para.rec.m(1).nr,para.ninc)
        else % Medio homog?neo
          Ci  = para.reg(mmat).Ci;
          ksi = para.reg(mmat).ksi;
          kpi = para.reg(mmat).kpi;
          kri = para.reg(mmat).kri;
      
          if para.fuente==1 %OP
%           Ci  = real(para.reg(mmat).Ci);
%           ksi = real(para.reg(mmat).ksi);
%           kpi = real(para.reg(mmat).kpi);
%           kri = real(para.reg(mmat).kri);
            if para.fuenteimagen==1
              [U0(:,:,iinc),~] = campo_ref_OPHeI_3D_SE(xs(iinc),ys(iinc),zs(iinc),coordr,kpi,ksi,kri,Ci,polOP(iinc),kxk(iinc),kyk(iinc));
              U0(isnan(U0)) = 0;
            else
              [U0(:,:,iinc),~] = campo_ref_OPHeI_3D(xs(iinc),ys(iinc),zs(iinc),coordr,kpi,ksi,kri,Ci,polOP(iinc),kxk(iinc),kyk(iinc));
            end
          elseif para.fuente==2 %FP
            [U0(:,:,iinc),~,S0(:,:,iinc)] = campo_ref_FP3D(xs(iinc),ys(iinc),zs(iinc),coordr,kpi,ksi,Ci,fij(iinc,:),salu,sals,para);
          end
        end
      end
    end
    if para.tipoMed(m)==2 % Medio Estratificado (DWN)
      %adjuntar campo difractado a campo incidente DWN
%       dA      = coord.dA;
      phi     = coord.phi;
      
      %posicion de las fv que hay que tomar en cuenta
      j       = 1:coord.nbpt;
      jphi    = 1:coord.nbeq;
      
      %indice (logic y natural) de los puntos de colocacion perteneciendo a m
      jjx     = coord.indm(m).ind;
%       ii      = j(jjx);
      
      %indice (logic y natural) de los phi que hay que contemplar (columnas
      %de la matriz)
      jjphi   = false(coord.nbeq,1);
      jjphi(phi(j(jjx),m)) = true(1);
      jj      = jphi(jjphi);
      
%       dAj = dA(ii);
      dAj = 1; % Ya estA en la funcion de Green
      %integracion de las contribuciones
      Udiff=zeros(3,ninc);
      Sdiff=zeros(nss,ninc);
      for i=1:nrecm
        j=1;
        if sal.Ut==1
          if sal.Ux==1
            for iinc = rangoIINC
              Udiff(1,iinc) = sum(dAj.*(...
                DWN{m}.Udiff(1,jj       ,i).*phi_fv(jj       ,iinc).'+...
                DWN{m}.Udiff(1,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
                DWN{m}.Udiff(1,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
            end
            ut(irec(i),:,j) = squeeze(U0(1,i,:)).'+Udiff(1,:);
%     utonlydiff(irec(i),:,j) = Udiff(1,:);
            j=j+1;
          end
          if sal.Uy==1
            for iinc = rangoIINC
              Udiff(2,iinc)         = sum(dAj.*(...
                DWN{m}.Udiff(2,jj       ,i).*phi_fv(jj       ,iinc).'+...
                DWN{m}.Udiff(2,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
                DWN{m}.Udiff(2,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
            end
            ut(irec(i),:,j) = squeeze(U0(2,i,:)).'+Udiff(2,:);
%     utonlydiff(irec(i),:,j) = Udiff(2,:);
            j=j+1;
          end
          if sal.Uz==1
            for iinc = rangoIINC
              Udiff(3,iinc)         = sum(dAj.*(...
                DWN{m}.Udiff(3,jj       ,i).*phi_fv(jj       ,iinc).'+...
                DWN{m}.Udiff(3,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
                DWN{m}.Udiff(3,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
            end
            ut(irec(i),:,j) = squeeze(U0(3,i,:)).'+Udiff(3,:);
%     utonlydiff(irec(i),:,j) = Udiff(3,:);
          end
        end
        
        %campo total esfuerzos
        j=1;
        if sal.sxx==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.syy==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.szz==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.sxy==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.sxz==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.syz==1
          for iinc = rangoIINC
            Sdiff(j,iinc)         = sum(dAj.*(...
              DWN{m}.Sdiff(j,jj       ,i).*phi_fv(jj       ,iinc).'+...
              DWN{m}.Sdiff(j,jj+  nbeq,i).*phi_fv(jj+  nbeq,iinc).'+...
              DWN{m}.Sdiff(j,jj+2*nbeq,i).*phi_fv(jj+2*nbeq,iinc).'));
          end
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
        end
      end
    else %Medio homogEneo
      Ci  = para.reg(mmat).Ci;
      ksi = para.reg(mmat).ksi;
      kpi = para.reg(mmat).kpi;
      %kri = para.reg(mmat).kri;
      for i=1:nrecm
        %campo difractado
        [Udiff,Sdiff]  = int_fv_Gij_3D(phi_fv,coord,para,m,xr(irec(i)),yr(irec(i)),zr(irec(i)),gaussian,kpi,ksi,Ci);
        
        %campo total desplazamientos
        j=1;
        if sal.Ut==1
          if sal.Ux==1
            ut(irec(i),:,j) = squeeze(U0(1,i,:)).'+Udiff(j,:);
            j=j+1;
          end
          if sal.Uy==1
            ut(irec(i),:,j) = squeeze(U0(2,i,:)).'+Udiff(j,:);
            j=j+1;
          end
          if sal.Uz==1
            ut(irec(i),:,j) = squeeze(U0(3,i,:)).'+Udiff(j,:);
          end
        end
        
        %campo total esfuerzos
        j=1;
        if sal.sxx==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.syy==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.szz==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.sxy==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.sxz==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
          j=j+1;
        end
        if sal.syz==1
          st(irec(i),:,j) = squeeze(S0(j,i,:)).'+Sdiff(j,:);
        end
      end
    end
  end
end
% return
% %% Test me: Todo
% uw(1,:,:,:) = uw(2,:,:,:);%ut;
% figure;
% subplot(1,2,1);
% graficarunafrecuencia(para,real(uw(1,:,:,:)),3)
% subplot(1,2,2);
% graficarunafrecuencia(para,imag(uw(1,:,:,:)),3)
% 
% %% El puro campo difractado:
% uw(1,:,:,:) = utonlydiff;
% graficarunafrecuencia(para,real(uw(1,:,:,:)),3)
% 
% %% El puro campo incidente:
% uw(1,:,:,:) = ut - utonlydiff;
% graficarunafrecuencia(para,real(uw(1,:,:,:)),3)

% %% test me
% figure(1); hold on
% 
% m = 1;
% thisXi = 1:318;
% this_X = 5;
% 
% tam = 1/abs(max(max(max(DWN{m}.Udiff))));
% % cols = pT.m(1,:) == im;
% rens = para.rec.m(m).ind;
% 
% pTXi(3,:) = coord.z;
% pTXi(2,:) = coord.y;
% pTXi(1,:) = coord.x;
% % pTdA = pT.dA(:,cols);
% pcX(3,:) = para.rec.zr(rens);
% pcX(2,:) = para.rec.yr(rens);
% pcX(1,:) = para.rec.xr(rens);
% 
% plot3(pcX(1,this_X),pcX(2,this_X),pcX(3,this_X),'r^');
% for iixs = thisXi % Indice global de la fuente
%   for ir = this_X % Indice global del receptor
% %     plot3(pcX(1,ir),pcX(2,ir),pcX(3,ir),'r^');
% %     plot3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),'r*');
% %     
%     cophi = para.coord.phi(iixs,m); % indice del phi correspondiente a la fuente
%     
%     % fx
%     phh = cophi;
%     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
%       real(DWN{m}.Udiff(1,phh,ir)),real(DWN{m}.Udiff(2,phh,ir)),real(DWN{m}.Udiff(3,phh,ir)),tam,'r-');
% %     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
% %       imag(DWN{m}.Udiff(1,phh,ir)),imag(DWN{m}.Udiff(2,phh,ir)),imag(DWN{m}.Udiff(3,phh,ir)),tam,'r--');
%     
%     % fy
%     phh = cophi+nbeq;
%     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
%       real(DWN{m}.Udiff(1,phh,ir)),real(DWN{m}.Udiff(2,phh,ir)),real(DWN{m}.Udiff(3,phh,ir)),tam,'g-');
% %     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
% %       imag(DWN{m}.Udiff(1,phh,ir)),imag(DWN{m}.Udiff(2,phh,ir)),imag(DWN{m}.Udiff(3,phh,ir)),tam,'g--');
%     
%     % fz
%     phh = cophi+2*nbeq;
%     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
%       real(DWN{m}.Udiff(1,phh,ir)),real(DWN{m}.Udiff(2,phh,ir)),real(DWN{m}.Udiff(3,phh,ir)),tam,'b-');
% %     quiver3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),...
% %       imag(DWN{m}.Udiff(1,phh,ir)),imag(DWN{m}.Udiff(2,phh,ir)),imag(DWN{m}.Udiff(3,phh,ir)),tam,'b--');
%     
%   end
% end
end