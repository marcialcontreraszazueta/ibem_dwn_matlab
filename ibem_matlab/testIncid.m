function testIncid
r0 = 2;
n = [0,1]; n = n./sqrt(n(1)^2 + n(2)^2);
xr = -0.5; zr = 2;
rho = 2;
Calf = 4;
Cbet = 2;
ome = 0.25*2*pi;

% campo incidente
[psi_a,psi_b]=fGrMagOndas(r0,n,xr,zr,rho,ome,Calf,Cbet);

disp(psi_a)
disp(psi_b)

% rij = sqrt((xr-r0)^2 + (zr-0)^2);
% g(1,1) = (xr-r0)/rij;
% g(2,1) = (zr-0 )/rij;
% C(6,6) = rho*Cbet^2;
% C(1,1) = rho*Calf^2;
% n = 1;
% G=Gij_PSV(ome/Cbet,ome/Calf,rij,g,C,n)
end

function [psi_a,psi_b]=fGrMagOndas(r0,n,xr,zr,rho,ome,Calf,Cbet)
% fGrMagOndas Magnitud de las ondas dada una fuerza puntual
NNmax = 200;

[theta,r] = cart2pol(xr,zr);% del receptor
[ThetaF,~] = cart2pol(n(1),n(2)); %de la fuente

kalfa = ome/Calf;
kbeta = ome/Cbet;

ar0 = kalfa*r0;
br0 = kbeta*r0;
ar = kalfa*r;
br = kbeta*r;

siA = zeros(NNmax,1);
siB = siA;
% n = 0
psi_a = (cos(theta - ThetaF) * besselj(0,ar0) * besselh(1,ar));
psi_b = (sin(theta - ThetaF) * besselj(0,br0) * besselh(1,br));

for n=1:NNmax
  % n positivo
  siA(n) = ((-1)^n * cos((n+1)*theta - ThetaF) * besselj(n,ar0) * besselh(n+1,ar)) + ...
    ((-1)^(-n) * cos((-n+1)*theta - ThetaF) * besselj(-n,ar0) * besselh(-n+1,ar));
  % n negativo
  siB(n) = ((-1)^n * sin((n+1)*theta - ThetaF) * besselj(n,br0) * besselh(n+1,br)) + ...
    ((-1)^(-n) * sin((-n+1)*theta - ThetaF) * besselj(-n,br0) * besselh(-n+1,br));
  if ~isnan(siA(n)) && ~isnan(siB(n))
    psi_a = psi_a + siA(n);
    psi_b = psi_b + siB(n);
  else
    break
  end
end
psi_a = psi_a * (-1i/(4*rho*ome*Calf));
psi_b = psi_b * (-1i/(4*rho*ome*Cbet));

figure;clf;hold on
plot(0, real(cos(theta - ThetaF) * besselj(0,ar0) * besselh(1,ar)),'r-');
plot((1:NNmax),real(siA),'r-');
plot(0, imag(cos(theta - ThetaF) * besselj(0,ar0) * besselh(1,ar)),'b-');
plot((1:NNmax),imag(siA),'b-');
end