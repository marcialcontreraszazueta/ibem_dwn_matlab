function [FsourcePSV,FsourceSH,ics,htop] = VecfuentDWN(Xiz,DWN)
% Vector de fuente (sin fase horizontal) para una fuerza puntual en medio
% estratificado. 
ncapas = DWN.ncapas;
kr = DWN.kr;
nkr = length(kr);
FsourcePSV  = complex(zeros(4*(ncapas-1)+2,nkr,2)); % Ultimo Indice para Fx y Fz
FsourceSH   = complex(zeros(2*(ncapas-1)+1,nkr));

% Estrato de la fuerza
%identification des couches dans lesquelles se trouvent les forces et
%les coordonnees z auxquels doivent etre evaluees les conditions aux
%interface
ics = 1; % indice de la capa de la fuente
htop= 0; % profundidad de la interfaz de arriba
hbot= 0; % profundidad de la interfaz de abajo
hc 	= DWN.h(1);
hbot= hbot+hc;
while Xiz>hc && ics<ncapas
    ics	= ics+1;
    htop= hc;
    hc 	= hc+DWN.h(ics);
    hbot= hc;
end
% Profundidad de las interfaces alrededor de la fuente
tam=1;if ics~=ncapas; tam = 2;end
coordr = struct('z',zeros(1,tam),'x',zeros(1,tam),'y',zeros(1,tam));
coordr.z(1) = htop;
if ics~=ncapas
    coordr.z(2)=hbot;
end
coordr.x    = 0*coordr.z;
coordr.y    = 0*coordr.z;

% Profundidad de las fuentes
coordfi = struct('xs',zeros(1,1),'ys',zeros(1,1),'zs',zeros(1,1));
coordfi.xs	= 0;
coordfi.ys	= 0;
coordfi.zs	= Xiz;

% Lo siguiente es Gij3D-POLAR, FJSS 2012
% pero sin los coeficientes de fase horizontal ni la suma en K, MP(2015)
%  prep:
% paratmp.reg(1).sub = DWN.sub;

%calculo del vector fuente
%el numero 2 al final de los argumentos de la funcion es para indicar
%que no se hace la suma, ni tampoco se multiplica por los coeficientes
%respectivos a las fuerzas horizontal y verticales, y que se restringe
%el calculo para solamente la coordenadas polares


% lo siguiente se prediminciona para que el compilador no chiste
nrec        = length(coordr.x);
nxs       	= 1;
% unusedGc = complex(zeros(3,3,nrec,nxs)); %#ignore 
Gp       = complex(zeros(3,3,nrec,nxs)); %#ok
Spz      = complex(zeros(3,3,nrec,nxs)); %#ok
Spx      = complex(zeros(3,3,nrec,nxs)); %#ok

%calculo del vector fuente
%el numero 2 al final de los argumentos de la funcion es para indicar
%que no se hace la suma, ni tampoco se multiplica por los coeficientes
%respectivos a las fuerzas horizontal y verticales, y que se restringe
%el calculo para solamente la coordenadas polares
[~,Gp,Spz,Spx]=Gij_3D_DWN_polar(DWN.sub,DWN,coordr,coordfi,ics,2);
    %3 eme indice de Spx/Spz ou Gp correspond au recepteur (1 : interface du
    %haut;2 : interface du bas)
    %4 eme source
    %5 eme kr
    
%signe oppose a celui de la matrice A utile a l'arrangement des termes
%incident et diffracte
mic	=-(-1)^ics;

%indice de los terminos en el vector fuente
iszz0 = 0;iszr0 = 0;iuz0 = 0;iur0 = 0;
iszzh = 0;iszrh = 0;iuzh = 0;iurh = 0;
if ics==1
    iszz0=1;%SIGMAzz EN 0
    iszr0=2;%SIGMAzr EN 0
    if ncapas>1
        iszzh=3;%SIGMAzz EN h(ics)
        iszrh=4;%SIGMAzr EN h(ics)
        iuzh =5;%Uz EN h(ics)
        iurh =6;%Ur EN h(ics)
    end
elseif ics>1
    iszz0=(ics-2)*4+2+1;%SIGMAzz EN 0
    iszr0=(ics-2)*4+2+2;%SIGMAzr EN 0
    iuz0 =(ics-2)*4+2+3;%Uz EN 0
    iur0 =(ics-2)*4+2+4;%Ur EN 0
    if ics<=(ncapas-1)
        iszzh=(ics-2)*4+2+5;%SIGMAzz EN h(ics)
        iszrh=(ics-2)*4+2+6;%SIGMAzr EN h(ics)
        iuzh =(ics-2)*4+2+7;%Uz EN h(ics)
        iurh =(ics-2)*4+2+8;%Ur EN h(ics)
    end
end

%%%%%%
% fx %
%%%%%%
    FsourcePSV(iszz0,:,1)    = Spx(3,3,1,1,:);      %sigmazz en 0
    FsourcePSV(iszr0,:,1)    = Spx(3,1,1,1,:);      %sigmazr en 0
    if ics>1
        FsourcePSV(iuz0,:,1) = Gp(3,1,1,1,:);   	%Uz en 0
        FsourcePSV(iur0,:,1) = Gp(1,1,1,1,:);   	%Ur en 0
    end
    if ics<=(ncapas-1)
        FsourcePSV(iszzh,:,1)= Spx(3,3,2,1,:);      %sigmazz en h
        FsourcePSV(iszrh,:,1)= Spx(3,1,2,1,:);      %sigmazr en h
        FsourcePSV(iuzh,:,1) = Gp(3,1,2,1,:);       %Uz en h
        FsourcePSV(iurh,:,1) = Gp(1,1,2,1,:);       %Ur en h
    end
    FsourcePSV(:,:,1)= FsourcePSV(:,:,1)*mic;
    
    FsourceSH(iszr0/2,:)    = Spx(2,2,1,1,:);   	%sigmazr en 0 %falsos indices
    if ics>1
        FsourceSH(iur0/2,:) = Gp(2,2,1,1,:);   	%Ur en 0 %falsos indices
    end
    if ics<=(ncapas-1)
        FsourceSH(iszrh/2,:)= Spx(2,2,2,1,:);     %sigmazr en h %falsos indices
        FsourceSH(iurh/2,:) = Gp(2,2,2,1,:);      %Ur en h %falsos indices
    end
    FsourceSH= FsourceSH*mic;
%%%%%%
% fz %
%%%%%%
    FsourcePSV(iszz0,:,2)    = Spz(3,3,1,1,:);	%sigmazz en 0
    FsourcePSV(iszr0,:,2)    = Spz(3,1,1,1,:);	%sigmazr en 0
    if ics>1
        FsourcePSV(iuz0,:,2) = Gp(3,3,1,1,:);   	%Uz en 0
        FsourcePSV(iur0,:,2) = Gp(1,3,1,1,:);   	%Ur en 0
    end
    if ics<=(ncapas-1)
        FsourcePSV(iszzh,:,2)= Spz(3,3,2,1,:); %sigmazz en h
        FsourcePSV(iszrh,:,2)= Spz(3,1,2,1,:); %sigmazr en h
        FsourcePSV(iuzh,:,2) = Gp(3,3,2,1,:); 	%Uz en h
        FsourcePSV(iurh,:,2) = Gp(1,3,2,1,:); 	%Ur en h
    end
    FsourcePSV(:,:,2)= FsourcePSV(:,:,2)*mic;
end