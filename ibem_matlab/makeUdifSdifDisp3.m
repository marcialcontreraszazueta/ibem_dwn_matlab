function [Udiff,Sdiff] = makeUdifSdifDisp3...%(para,DWNorig,pT,im,j,Xiz)
  (nbeq,coordphi,CubWts,pcX,DWNorig,pT,im,j,Xiz,fracVert)
% Parecido a makeUdifSdiff pero solo para una profundidad de la fuente. Xiz

% factores de calibraciOn:
fracDist = 1/60;   % Agrupar distancias horizontales
fracCercano = 1/200;% Integracion Gaussiana de campo difractado

ngau = size(CubWts,1);

% sal = para.sortie;
% doU = (sal.Ux + sal.Uy + sal.Uz) >= 1;
% doS = (sal.sxx + sal.syy + sal.szz + sal.sxy + sal.sxz + sal.syz) >= 1;

DWN = DWNorig;
Udiff  = complex(zeros(DWNorig.Udiffsize));
Sdiff = 0;
%Sdiff  = zeros(DWNorig.Sdiffsize);
% fuentes y receptores en el medio im
cols = pT.m(1,:) == im;       if isempty(cols); return;end

pTXi = pT.Xi(:,cols);
pTdA = pT.dA(:,cols);
pTVn = pT.Vn(:,cols);
pTcubPts = pT.CubPts(:,:,cols);
nrens = size(pcX,2);
ncols = sum(cols);

% Reconocer los receptores en [rens] a la misma profundidad
[X_z_uni,uni_ref,zXref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
  pcX(3,:),pcX(4,:),DWN.ncapas,fracVert);
leX_z_uni = length(X_z_uni);

% Gij = complex(zeros(3,3,1,1));
GijP= complex(zeros(3,3,1));
facU = complex(zeros( 5,DWN.nkr));

% % test me
%  if im == 1
%    irrrr= 1; 
%    figure(2654321);hold on; plot3(pcX(1,irrrr),pcX(2,irrrr),pcX(3,irrrr),'r*'); set(gca,'Zdir','reverse'); axis equal
%  else
%    irrrr = 0;
%  end
 
% ------------------ [para el campo intermedio, lejano] -------------------
% Armar vector de fuente                                                  |
% ics = 1; %#ok para que no chiste el compilador                            |
% FsourcePSV  = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok          |
% FsourceSH   = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok            |
[FsourcePSV,FsourceSH,ics,~] = VecfuentDWN(Xiz,DWN);%                     |
% Amplitud de las ondas planas que construyen el campo difractado por la  |
% estratificaciOn:                                                        |
% SolutionPSV = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok          |
% SolutionSH  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok            |
[SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...   |
  DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);%                               |

% Para cada profundidad Unica de los receptores:
for iX_z_uni = 1:leX_z_uni % Para cada profundiad de receptores
  % indice de los receptores a esta profundidad:
  res_at_iX_z = find(zXref==iX_z_uni); %e.g.: pcX(3,res_at_iX_z) son todos iguales
  icr = pcX(4,uni_ref(iX_z_uni)); % estrato del receptor
  
  ksi=complex(zeros(1,1)); kpi=complex(zeros(1,1)); Ci=complex(zeros(1,1)); %#ok
  MecElemU = complex(zeros(3,3)); %#ok
  %   MecElemS = complex(zeros(3,3,3)); %#ok
  
  [ksi,kpi,Ci] = theSub(DWN.sub,icr(1,1));
  longonda = (2*pi)/real(ksi);
  
  % profundidad relativa a la interface de la capa del receptor:
  zricr = X_z_uni(iX_z_uni) -  DWN.z(icr);
  
  % matriz distancia horizontal, cosenos directores y acimut entre
  %    res_at_iX_z(:)j(:) cantidad de receptores a una profundidad
  %    nj                 cantidad de fuentes a una profundidad
  % la matriz tiene el tamaNo original y los indices son globales
  MdistXY = zeros(nrens,ncols) .* NaN;
%   ir=0;iixs=0;Xfs=0.0;Yfs=0.0;r=0.0;c=0.0;s=0.0;th=0.0; %#ok
  for Iiixs = 1:length(j.') % Matriz con las distancias r
    iixs = j(Iiixs);
    for Iir = 1:length(res_at_iX_z.')
      ir = res_at_iX_z(Iir);
      Xfs = pcX(1,ir)-pTXi(1,iixs);
      Yfs = pcX(2,ir)-pTXi(2,iixs);
      r = sqrt(Xfs^2 + Yfs^2);
      MdistXY(ir,iixs) = r;
    end
  end
  
  if true % DWN y directo --- Fuente y receptor CERCA:
    %     if doU
    [Mi] = MdistXY(:,:) <= longonda*fracCercano;         % cualquier Z, meh
    % IntegraciOn gaussiana para fuente en segmento triangular en un
    % medio estratificado (desplazamientos)
    [irxs,ixss] = find(Mi); %los pares ir,iixs de Mi
    for iii = 1:length(irxs)
      ir   = irxs(iii); % Indice global del receptor
      iixs = ixss(iii); % Indice global de la fuente
      
%       if ir == irrrr % testme
%       figure(2654321);hold on;plot3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),'bo');
%       end
      
      pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
      rij = sum((pcXaux) .* (pcXaux))^0.5;  %   distancia en el espacio
      g = pcXaux./rij;
      dr = sqrt(pTdA(iixs)/pi); % radio del cIrculo de Area dA(ic)
      Vn = pTVn(1:3,iixs);
      % ---- < ahorita usa la version 'lenta' que aplica la fuerza por cada
      % punto de la cuadratura > ----- Falta depurar la version rApida:
      %             1)Fuerza en el receptor y desplazamientos en puntos de
      %             integraciOn. 2) Reciprocidad y suma con pesos y Area.
      
      % version lenta:
      Gij = Gij_3DDWN_smallGENLENTO(pcX(1:4,ir),rij,g,dr,Vn,...
        pTcubPts(1:3,1:ngau,iixs),CubWts,icr,ics,...
        DWN);  %func de Green [(u v w) x (fx fy fz)]
      GijP(1:3,1:3,1) = squeeze(Gij(1:3,1:3,1,1));
      cophi = coordphi(iixs);% indice del phi correspondiente a la fuente
      % Multiplicar por Area del segmento. Ya es [(u v w) x (fx fy fz)]
      Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) = squeeze(GijP).* ...
        pTdA(iixs);
      %             end
    end
    
    % Los que restan (lejos) como fuente puntual en medio estratificado
    MdistXY(Mi) = NaN;
    %     end
  end % cerca
  
  % DWN y directo --- Fuente y receptor LEJOS:
%   MdisUni = zeros(nrens,ncols); disN = zeros(nrens*ncols,1); %#ok
  [MdisUni,~,disN] = psedoUniqueM(MdistXY(:,:),longonda*fracDist,false);
  if ~isempty(disN) 
    % tErminos de propagaciOn vertical a las ondas planas que
    % constituyen el campo difractado por la estratificaciOn
    % en el estrato del receptor [fac].
    % DWN.kz(1,kr,estrato) = nu    para todos los kr en ese estrato
    % DWN.kz(2,kr,estrato) = gamma   ''
    %   if doU % desplazamientos
    facU(:,:) = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
      SolutionPSV,SolutionSH,...
      DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,1,DWN.nkr);
    %   end
    %   if doS % esfuerzos
    %     facS = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
    %       SolutionPSV,SolutionSH,...
    %       DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,-1,DWN.nkr);
    %   end
    
    nkss = DWN.nkr;
    Mi=zeros(nrens,ncols).*NaN;iDist=0.0; %#ok
    J0 = complex(zeros(1,nkss)); J1 = complex(zeros(1,nkss));%#ok
    dJ1= complex(zeros(1,nkss));d2J1= complex(zeros(1,nkss));%#ok
    dJ0= complex(zeros(1,nkss));d2J0= complex(zeros(1,nkss));%#ok
    % Para cada grupo de distancia horizontal
    for IiDist = 1:length(disN.')%iDist = disN.'
      iDist = disN(IiDist);
      %       Mi=zeros(nrens,ncols).*NaN; %#ok
      [Mi] = MdisUni == iDist;
      r = mean(mean(MdistXY(Mi))); % distancia (en el plano) promedio
      
      %     for iiik = 1:nkss
      %       % funcion en matlab truducida de fortran:
      %       [J0(iiik),J1(iiik)] = BessJ01(DWN.kr(iiik)*r);
      %     end
      J0      = besselj(0,DWN.kr*r);
      J1      = besselj(1,DWN.kr*r);
      dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
      d2J1    =-DWN.kr/r.*J0+1.*(2/r^2-DWN.kr.^2);
      dJ0     =-DWN.kr.*J1;
      d2J0    =-DWN.kr.*dJ1;
      
      % Agregar terminos que solo dependen del radio horizontal
      % tracciones
      %     if doU % desplazamientos
      MecRadialU = campodiffporestratRadialTransv(1,...
        r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
        ksi,kpi,facU(:,:),DWN.DK);
      %     end
      %     if doS
      %       MecRadialS = campodiffporestratRadialTransv(-1,...
      %         r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
      %         ksi,kpi,facS(:,:,iX_z_uni),DWN.DK);
      %     end
      
      % usar psedoUniqueM con los azimuths es tardado, mejor di una vez.
      
      [irxs,ixss] = find(Mi); %los pares ir,iixs con radio 'r'
      for iii = 1:length(irxs)
        ir   = irxs(iii); % Indice global del receptor
        iixs = ixss(iii); % Indice global de la fuente
        
%         if ir == irrrr % test me
%           figure(2654321);hold on;plot3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),'ro');
%         end
        
        cophi = coordphi(iixs); % indice del phi correspondiente a la fuente
        
        r = MdistXY(ir,iixs); %2D
        pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
        if r<1e-6
          c = 1/sqrt(2);
          s = 1/sqrt(2);
        else
          c = (pcXaux(1))/r;
          s = (pcXaux(2))/r;
        end
        
        % --- tracs ---- copiar de calcBlockDWNTGdisp
        
        % --- desplazamientos campo difractado ----
        MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6));
        % MecElemU viene como [(fx fy fz) x (u v w)]
        GijP = squeeze(Gij_fromMecElem(MecElemU,false));
        
        % func de Green por incidencia directa: [(fx fy fz) x (u v w)]
        if icr == ics
          rij = sum((pcXaux) .* (pcXaux))^0.5; %3D
          g = pcXaux./rij;
          GijP = GijP + squeeze(Gij_3Done(ksi,kpi,rij,g,Ci));
        end
        
        % permute: [(u v w) x (fx fy fz)]
        GijP(1:3,1:3,1,1) = GijP(1:3,1:3,1,1).';

        Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) = GijP.* ...
          pTdA(iixs);
        
      end % iii
    end %iDist
  end % lejos
end % % iXiz_uni: Profundidad Unica de los receptores
end

function [ksi,kpi,Ci] = theSub(sub,icr)
ksi 	= sub(icr).ksi;
kpi 	= sub(icr).kpi;
Ci    = sub(icr).Ci;
end