function output_txt = myfunction(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

pos = get(event_obj,'Position');
disp(pos)
event_obj
nam = get(event_obj,'DisplayName');
disp(nam)
output_txt = {[nam],...
    [num2str(pos(2),4),'mm/s']};

