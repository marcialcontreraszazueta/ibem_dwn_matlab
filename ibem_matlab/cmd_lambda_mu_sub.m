thissubmed     =get(bouton.submed,'value');

alpha   = para.reg(med).sub(thissubmed).alpha;
bet     = para.reg(med).sub(thissubmed).bet;
rho     = para.reg(med).sub(thissubmed).rho;

lambda  = rho*(alpha^2-2*bet^2);
mu      = rho*bet^2;
nu      = lambda/(2*(lambda+mu));

para.reg(med).sub(thissubmed).lambda	= lambda;
para.reg(med).sub(thissubmed).mu    	= mu;
para.reg(med).sub(thissubmed).nu    	= nu;

set(bouton.lambda   ,'string',num2str(lambda));
set(bouton.mu       ,'string',num2str(mu));
set(bouton.nu       ,'string',num2str(nu));