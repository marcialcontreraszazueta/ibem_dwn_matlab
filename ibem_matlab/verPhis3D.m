%% Graficar phis
function verPhis3D(para,phi_fv)
warning('warning de depuracion. se ejecuta comando verphi para incidencia 1')
iinc = 1;
nbeq    = para.coord.nbeq;
sca  =max(max(abs(phi_fv)))*0.002;
  
figure(2987462);clf;
for m=1:para.nmed
%indice (logic y natural) de los puntos de colocacion perteneciendo a m
jj = para.coord.phi(:,m); 
jj(jj==0)=[];

  pxO=phi_fv(jj+0*nbeq,iinc).';
  pyO=phi_fv(jj+1*nbeq,iinc).';
  pzO=phi_fv(jj+2*nbeq,iinc).';
  
  figure(2987462);hold on
  px = real(pxO); py=real(pyO); pz=real(pzO);
  plot([px.';py.';pz.'],'r-')
  mamama = max([px py pz]);
  mimimi = min([px py pz]);
  px = px/mamama*sca;
  py = py/mamama*sca;
  pz = pz/mamama*sca;
  
  pxI = imag(pxO); pyI=imag(pyO); pzI=imag(pzO);
  plot([pxI.';pyI.';pzI.'],'b-')
  mamamaI = max([pxI pyI pzI]);
  mimimiI = min([pxI pyI pzI]);
  pxI = pxI/mamamaI*sca;
  pyI = pyI/mamamaI*sca;
  pzI = pzI/mamamaI*sca;
  disp([m mamama + 1i*mamamaI mimimi + 1i*mimimiI])
  
  H=figure;hold on;set(gca,'Zdir','reverse'); 
  axis equal;view(3);grid on; set(gca,'Projection','perspective')
  title(['medio ' num2str(m)])
  x = para.coord.x;
  y = para.coord.y;
  z = para.coord.z;
  ii = para.coord.Xim(:,m) ~= 0;
  quiver3(x(ii),y(ii),z(ii),px,py,pz,'r')
  quiver3(x(ii),y(ii),z(ii),pxI,pyI,pzI,'b')
  savefig(H,['phi_m',num2str(m),'.fig'],'compact');
end
savefig(2987462,'allphi.fig','compact');
end
%%