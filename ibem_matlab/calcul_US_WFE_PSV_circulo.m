function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo...
  (para,xr0,zr0,salu,sals,coordf,WFE)
% calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo. 
% Deformacion plana en un espacio completo 2D con una cavidad circular.
% Metodo de expansion de funciones de onda de Mow y Pao
% Incidencia de fuente puntual.
% Util para:
% Obtener las funciones de Green en puntos de colocacion y receptores
% dada la fuente coordf (una columna de la matriz) en el ejercicio del
% tunel.
% parecido a Perez Ruiz y SS en 2008.

rcondMin = 1e-14;
%cantidad maxima de terminos en el campo difractado
ni = 67; nfi = 82; % para ondas S
% ni = 18; nfi = 21; % para ondas P
nterm = ceil(ni+ (nfi-ni)*para.j/(para.nf/2)); 

nxs = 1; % una fuente
ixs = 1;
U2        = zeros(2,sum(salu),nxs);
S2        = zeros(2,2,sum(sals),nxs);
U1        = zeros(2,sum(salu),nxs);
S1        = zeros(2,2,sum(sals),nxs);

m = WFE.m; % medio donde se coloca el tunel
a = para.cont(m,3).a; % radio del tunel

% - propiedades del material
mmat = m;%mmat= para.subm1(m);
ksi  = para.reg(mmat).ksi;  % numeros de onda
kpi  = para.reg(mmat).kpi;
ome = 2*pi*para.fj;
if isfield(WFE,'omegac')
  ome = WFE.omegac;
end
rho = para.reg(mmat).rho;  % densidad de masa
Ca = para.reg(mmat).alpha; % velocidades de propagacion
Cb = para.reg(mmat).bet;
Ci  = para.reg(mmat).Ci;
mu = rho*Cb^2;%Ci(6,6);                 % Lame 1 (G de cortante)
% nu = para.reg(mmat).nu;    % Poisson
% lambda = (2*mu*nu)/(1-2*nu);  % Lame 2

% - posicion relativa al centro del tunel
% -- fuente
xs = coordf.x - para.cont(m,3).xa;
zs = coordf.z - para.cont(m,3).za;
r0 = (xs.^2 + zs.^2).^.5;
% -- receptores
xr = round(xr0 - para.cont(m,3).xa,5);
zr = round(zr0 - para.cont(m,3).za,5);
rrec = (xr.^2 + zr.^2).^.5;
enE = rrec > 0.99*a;
nrec = length(xr);

% no hacer los receptores casi encima de la fuente         [integrar]    ?
% xij     = xr0-coordf.x;
% zij     = zr0-coordf.z;
% rij     = sqrt(xij.^2+zij.^2);
% jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% jj0 = coordf.i;

%% - sistema de referencia local
% -- angulos respecto al eje x', la fuente en (xF,0)
t1 = atan2(zs,xs);
% -- direccion de las fuentes
THx = - t1;

% -skip
skip=false(2,1);

% -- posicion de los receptores en locales
threc = atan2(zr,xr);% en globales   x ,z
ang_a = threc + THx; % en locales    x',z'

%% testme dibujo:
% figure(54312); 
% if ~strcmp(get(gcf,'name'),'coordenadas globales')
%   % figura nueva
% hold on; t=linspace(0,2*pi);
% plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% plot(xs,zs,'r*'); % fuerza
% quiver(xs,zs,1,0,'r');
% quiver(xs,zs,0,1,'r');
% plot(xr,zr,'bo'); plot(xr(enE),zr(enE),'b.'); % receptores
% axis equal;grid on;xlabel('x');ylabel('z');%set(gca,'YDir','reverse')
% set(gcf,'name','coordenadas globales')
% clear t
% end

%% CAMPO DIFRACTADO  ------------------------------------------------------
if true 
% recN = ones(1,nrec)*nterm;

RES = complex(zeros(5,2,nrec));
%           | `-- Fx, Fz
%           `-- ur,ut,srr,stt,srt
% - Calcular coeficientes de la expansion
RE = complex(zeros(5,1,1));
% suponemos que r0 >= a

A = complex(zeros(2,2));
B = complex(zeros(2,2));
EPS = 1;
for n = 0:nterm
% -- coeficientes del campo difractado en la pared de la cavidad
r = a;           % (k,q,n,r,tipo,todos)
E_d = ondasRadiales(ksi,kpi,n,r,4,false); % H
A(1,:) = [E_d.e11, E_d.e12]; % srr
A(2,:) = [E_d.e41, E_d.e42]; % srt  

if sum(sum(isinf(A))) > 0 || sum(sum(isnan(A))) > 0
  break
end

% -- coeficientes de la incidencia
E_i = ondasRadiales(ksi,kpi,n,r,1,false); % J
s = (-1)^n;

% con coseno
CphiC = -1i/4/rho/ome/Ca * EPS/2 * (besselh(1+n,2,kpi*r0) + s*besselh(1-n,2,kpi*r0));
CpsiC = -1i/4/rho/ome/Cb * EPS/2 * (besselh(1+n,2,ksi*r0) + s*besselh(1-n,2,ksi*r0));
% con seno
CphiS = -1i/4/rho/ome/Ca * EPS/2 * (besselh(1+n,2,kpi*r0) - s*besselh(1-n,2,kpi*r0));
CpsiS = -1i/4/rho/ome/Cb * EPS/2 * (besselh(1+n,2,ksi*r0) - s*besselh(1-n,2,ksi*r0));

% -- campo incidente en el sistema local x'
% Fx'
B(1,1) = -( -CphiC*E_i.e11 + CpsiS*E_i.e12 ); % -srr
B(2,1) = -( -CphiC*E_i.e41 + CpsiS*E_i.e42 ); % -srt
% Fz'
B(1,2) = -(  CphiS*E_i.e11 - CpsiC*E_i.e12 ); % -srr
B(2,2) = -( -CphiS*E_i.e41 + CpsiC*E_i.e42 ); % -srt * -1

V = complex(zeros(2,2));
for dir = 1:2
if rcond(A) < rcondMin || sum(sum(isnan(A))) > 0 || sum(isnan(B(:,dir))) > 0
%   disp(n)
%   warning('recond')
%   skip(dir)=true;  
%   continue
%   V = complex(zeros(2,1));
  V(:,dir) = zeros(2,1);
else
  V(:,dir)=A\B(:,dir); %coef. ondas P; ondas S
end
end

for ir = 1:nrec
  if ~enE(ir)
    RES(1:5,1:2,ir) = NaN; % en el aire
    continue
  end
%   if n > recN(ir); continue; end
  r = rrec(ir);
  r2 = r^2;
  th = ang_a(ir);
  E_d = ondasRadiales(ksi,kpi,n,r,4,true); % H
  %E_i = ondasRadiales(ksi,kpi,n,r,1,true); % J
  for dir =1:2
    if skip(dir);  continue;  end
    switch dir % ok para fuerzas sobre eje x'
      case 1
        gC=cos(n*th);
        gS=sin(n*th);
      case 2
        gC=sin(n*th);
        gS=cos(n*th);
    end
    %ur
    RE(1) = + 1/r*(...
      V(1,dir)*E_d.e71 + V(2,dir)*E_d.e72)*gC;
    
    %ut
    RE(2) = + 1/r*(...
      V(1,dir)*E_d.e81 + V(2,dir)*E_d.e82)*gS;
    
    %srr
    RE(3) = + 2*mu/r2*(...
      V(1,dir)*E_d.e11 + V(2,dir)*E_d.e12)*gC;
    
    %stt
    RE(4) = + 2*mu/r2*(...
      V(1,dir)*E_d.e21 + V(2,dir)*E_d.e22)*gC;
    
    %srt
    RE(5) = + 2*mu/r2*(...
      V(1,dir)*E_d.e41 + V(2,dir)*E_d.e42)*gS; 
    
    if sum(isnan(RE))==0
      RES(:,dir,ir) = RES(:,dir,ir) + RE;
    end
  end % dir
end% ir

EPS = 2;
end %n

%% polares a rectangulares y de sistema local a global
% Se tienen desplazamientos y esfuerzos en coordenadas polares
% cambio a coordenadas rectangulares en el sistema x,y (global)

for ir = 1:nrec
  an = threc(ir);
  m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
  m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
  
  % Fx'
  dir = 1;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U1(1,ir,ixs) = a(1); % ux'
  U1(2,ir,ixs) = a(2); % uz'
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S1(1,1,ir,ixs) = a(1,1); % sxx'
  S1(2,2,ir,ixs) = a(2,2); % szz'
  S1(1,2,ir,ixs) = a(1,2); % sxz'
  % Fz'
  dir = 2;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U2(1,ir,ixs) = a(1); % ux'
  U2(2,ir,ixs) = a(2); % uz'
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S2(1,1,ir,ixs) = a(1,1); % sxx'
  S2(2,2,ir,ixs) = a(2,2); % szz'
  S2(1,2,ir,ixs) = a(1,2); % sxz'
  
  % combinar resultados en el sistema x' para obtenerlos en el sistema x
  % desplazamientos:
  auxX(1) = U1(1,ir,ixs)*cos(t1) + U2(1,ir,ixs)*cos(t1 + pi/2);
  auxX(2) = U1(2,ir,ixs)*cos(t1) + U2(2,ir,ixs)*cos(t1 + pi/2);
  
  auxZ(1) = U1(1,ir,ixs)*sin(t1) + U2(1,ir,ixs)*sin(t1 + pi/2);
  auxZ(2) = U1(2,ir,ixs)*sin(t1) + U2(2,ir,ixs)*sin(t1 + pi/2);
  
  U1(1,ir,ixs) = auxX(1); % Ux  Fx
  U1(2,ir,ixs) = auxX(2); % Uz  Fx
  U2(1,ir,ixs) = auxZ(1); % Ux  Fz
  U2(2,ir,ixs) = auxZ(2); % Uz  Fz
  
  % esfuerzos:
  auxX(1) = S1(1,1,ir,ixs)*cos(t1) + S2(1,1,ir,ixs)*cos(t1 + pi/2);
  auxX(2) = S1(2,2,ir,ixs)*cos(t1) + S2(2,2,ir,ixs)*cos(t1 + pi/2);
  auxX(3) = S1(1,2,ir,ixs)*cos(t1) + S2(1,2,ir,ixs)*cos(t1 + pi/2);
  
  auxZ(1) = S1(1,1,ir,ixs)*sin(t1) + S2(1,1,ir,ixs)*sin(t1 + pi/2);
  auxZ(2) = S1(2,2,ir,ixs)*sin(t1) + S2(2,2,ir,ixs)*sin(t1 + pi/2);
  auxZ(3) = S1(1,2,ir,ixs)*sin(t1) + S2(1,2,ir,ixs)*sin(t1 + pi/2);
  
  S1(1,1,ir,ixs) = auxX(1); % sxx Fx
  S1(2,2,ir,ixs) = auxX(2); % szz Fx
  S1(1,2,ir,ixs) = auxX(3); % sxz Fx
  
  S2(1,1,ir,ixs) = auxZ(1); % sxx Fz
  S2(2,2,ir,ixs) = auxZ(2); % szz Fz
  S2(1,2,ir,ixs) = auxZ(3); % sxz Fz
end %ir
end

%% CAMPO INCIDNETE FUNCION ANALITICA  -------------------------------------
if true 
ksi  = para.reg(mmat).ksi;  % numeros de onda
kpi  = para.reg(mmat).kpi;
for ir = 1:nrec
  if ~enE(ir)
    RES(1:5,1:2,ir) = NaN; % en el aire
    continue
  end
  
  xij     = xr0(ir)-coordf.x;
  zij     = zr0(ir)-coordf.z;
  rij     = sqrt(xij.^2+zij.^2);
  g(1,1)  = xij./rij;
  g(2,1)  = zij./rij;
  
  if salu(ir)==1
    if isfield(coordf,'vnx')
      dr = coordf.dr;
      if rij==0
        gn(1)=-coordf.vnz;
        gn(2)= coordf.vnx;
        Gij0 = Greenex_PSV(ksi,kpi,gn,Ci,dr);
      elseif rij<=1*para.npplo*dr
        Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,ksi,kpi,para.gaussian,Ci);
      else
        Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
      end
    else
      Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
    end
    if ~skip(2)
    U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
    U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
    end
    if ~skip(1)
    U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
    U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
    end
  end
  
  if sals(ir)==1
    if isfield(coordf,'vnx')
      dr = coordf.dr;
      if rij<=1*para.npplo*dr && rij~=0
        [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),ksi,kpi,Ci,[1 1],para.gaussian);
      elseif rij~=0
        [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
      end
    else
      [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
    end
    
    if rij~=0
      if ~skip(1)
      %Fx
      S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
      S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
      S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
      end
      if ~skip(2)
      %Fz
      S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
      S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
      S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
      end
    end
  end
end
end

%% testme grafica 
% % desplazamientos y esfuerzos totales en polares
% recran = 1:nrec; nam ={'ur','ut','srr/\lambda','stt\lambda','srt\lambda'};
% Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% for ir = 1:nrec
%   an = threcF(ir)-THx; % <-- angulo en coordenadas de la fuente.
%   m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
%   m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
%   % Fx
%   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
%   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
%   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
%           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,1) = a(1,1);
%   Spol(ir,2,1) = a(2,2);
%   Spol(ir,3,1) = a(1,2);
%   % Fz
%   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
%   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
%   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
%           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,2) = a(1,1);
%   Spol(ir,2,2) = a(2,2);
%   Spol(ir,3,2) = a(1,2);
% end
% 
% col ={'k-','b-','r-'};
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:2
%   mx(ico) = max(squeeze(real(Upol(recran,ico,1))));
%   my(ico) = max(squeeze(real(Upol(recran,ico,2))));
% end
% for ico = 1:3
%   mx(ico+2) = max(squeeze(real(Spol(recran,ico,1))))/abs(lambda);
%   my(ico+2) = max(squeeze(real(Spol(recran,ico,2))))/abs(lambda);
% end
% 
% sca=50;
% set(0,'DefaultFigureWindowStyle','normal')
% 
% % para FX
% r = para.cont(m,3).a; % radio del tunel
% figure(50001); h1=polar(0,r+sca*max(mx),'y.'); hold on;set(h1,'visible','off')
% % tunel:
% t = linspace(0,2*pi); h=polar(t,r+t.*0); set(h,'color',[0.3,0.3,0.3],'LineWidth',3)
% for ico = 1:2 % U
% h=polar(threc(recran).',r+sca*squeeze(real(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% end
% for ico = 1:3 %   Srr, Stt, Srt
% h = polar(threc(recran).',r+sca/abs(lambda)*squeeze(real(Spol(recran,ico,1))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','on');
% end
% 
% % para Fz
% r = para.cont(m,3).a; % radio del tunel
% figure(50002); h1=polar(0,r+sca*max(my),'y.'); hold on;set(h1,'visible','off')
% % tunel:
% t = linspace(0,2*pi); h=polar(t,r+t.*0); set(h,'color',[0.3,0.3,0.3],'LineWidth',3)
% for ico = 1:2 % U
% h=polar(threc(recran).',r+sca*squeeze(real(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% end
% for ico = 1:3 %   Srr, Stt, Srt
% h = polar(threc(recran).',r+sca/abs(lambda)*squeeze(real(Spol(recran,ico,2))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','on');
% end
end

%%         -------------------- funciones ---------------------
function F = ondasRadiales(k,q,n,r,tipo,todos)
 % coeficientes a la Mow y Pao 1971. 
 % Tambien en Cahngping, Yi, et al. Dynamic reponse of a circu...
% con la opcion cos(n th) para srr y sin (n th) para srt
F = [];
r2 = r^2;
k2 = k^2;
q2 = q^2;
n2 = n^2;
qr = q*r;
kr = k*r;

switch tipo
  case 1
    % J
    Zq =besselj(n  ,qr);
    Z1q=besselj(n-1,qr);
    Zk =besselj(n  ,kr);
    Z1k=besselj(n-1,kr);
  case 3
    % H^(1)
    K = 1;
    Zq =besselh(n  ,K,qr);
    Z1q=besselh(n-1,K,qr);
    Zk =besselh(n  ,K,kr);
    Z1k=besselh(n-1,K,kr); 
  case 4
    % H^(2)
    K = 2;
    Zq =besselh(n  ,K,qr);
    Z1q=besselh(n-1,K,qr);
    Zk =besselh(n  ,K,kr);
    Z1k=besselh(n-1,K,kr); 
end

% los de P
F.e11 = (n2+n - k2*r2/2)*Zq - qr*Z1q;     % e11  srr  phi cos F(13)
F.e41 = -n*(-(n+1)*Zq + qr*Z1q);          % e41  srt  phi cos F(7)
if todos 
F.e21 = -(n2+n + k2*r2/2 -q2*r2)*Zq + qr*Z1q; % e21  stt  phi cos 3,5,9?
F.e71 = qr*Z1q - n*Zq;                   % e71  ur   phi cos F(1)
F.e81 = -n*Zq;                           % e81  ut   phi sin F(11)
end

% los de S
F.e12  = n*(-(n+1)*Zk + kr*Z1k);         % e12  srr  psi cos  F(8)
F.e42  = -(n2+n - k2*r2/2)*Zk + kr*Z1k;  % e42  srt  psi sin y cos F(6)
if todos
F.e22 = n*((n+1)*Zk - kr*Z1k);           % e22  stt  psi cos 4?
F.e72 = n*Zk;                            % e72  ur   psi cos F(10)
F.e82 = -(kr*Z1k - n*Zk);                % e82  ut   psi sin F(2)
end
end

% function HHH = transBessH2(wn,u,v,w,a,x,n)
% % transBessH2 funciones de besselH2 con traslacion v 
% % Usa forma degenerada de teorema de Graf (alfa y Xi == 0)
% % siempre y cuando  |v| < |u| 
% HHH=complex(zeros(3,1));
% K = 2;
% u = u*wn;
% v = v*wn;
% 
% eps = 1; if n>0 ; eps = 2; end
% s = (-1)^n;
% bj = besselj(n,v);
% 
% % H^(2)_0
% fcirc = cos( n*a);
% 
% HHH(1) = eps/2 * (besselh(0+n,K,u) + s*besselh(0-n,K,u))*fcirc * bj;
% 
% if abs(cos(1*x)) >= cos(pi/4); fc = @cos; s2=1; else fc = @sin; s2=-1; end
% fcirc=fc( n*a);
% fx=fc(1*x);
% 
% % H^(2)_1
% HHH(2) = eps/2 * (besselh(1+n,K,u) + s*s2*besselh(1-n,K,u))*fcirc * bj;
% HHH(2) = HHH(2)/fx;
% 
% % H^(2)_2
% HHH(3) = -HHH(1) + 2/wn/w*HHH(2);
% 
% % if abs(cos(2*x)) >= cos(pi/4); fc = @cos; else fc = @sin; end
% % fp=fc( n*a);
% % fn=fc(-n*a);
% % HHH(3) = eps/2 * (besselh(2+n,K,u)*fp + s*besselh(2-n,K,u)*fn) * bj
% % HHH(3) = HHH(3)/fc(2*x);
% end