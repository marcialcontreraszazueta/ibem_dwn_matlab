function paratmp=attenuation(para,fj)
%compute wave vector of L, T and Rayleigh waves and their attenuation
%  
paratmp = para;

for m=1:para.nmed
    if para.tipoMed(m) == 2
        nsubmed = para.reg(m).nsubmed;
    else
        nsubmed = 1;
    end
    
    for ms=1:nsubmed
        if para.tipoMed(m) == 2
            paraM   = para.reg(m).sub(ms);
        else
            paraM   = para.reg(m);
        end
        
        C   = paraM.C;
        rho = paraM.rho;
        q   = paraM.qd;
        if paraM.tipoatts==0 || paraM.tipoatts==3
            Ci  = C;
            vL  = sqrt(C(1,1)/rho);
            vT  = sqrt(C(6,6)/rho);
        elseif paraM.tipoatts==1
            %Q
            Ci  = C/(1-1i/(2*q))^2;
            vL  = sqrt(Ci(1,1)/rho);
            vT  = sqrt(Ci(6,6)/rho);
        elseif paraM.tipoatts==1.1
            %Q variable con la frecuencia (bilineal)
            q0 = paraM.qd; % q cte hasta qfcorner
            fcorner = paraM.qbili.qfcorner;
            % para f>fcorner q = b + m f
            q_b = paraM.qbili.b; 
            q_m = paraM.qbili.m;
            q = testqbili(real(fj),q0,fcorner,q_m,q_b);
            Ci  = C/(1-1i/(2*q))^2;
            vL  = sqrt(Ci(1,1)/rho);
            vT  = sqrt(Ci(6,6)/rho);
        elseif paraM.tipoatts==2
            % Kelvin-Voigt-Model
            if length(fj)==1
                Ci  = C.*(1+1i*2*pi*fj*q);
                vL  = sqrt(Ci(1,1)/rho);
                vT  = sqrt(Ci(6,6)/rho);
            else
                Ci=zeros(6,6,length(fj));
                Ci(1,1,:)  = C(1,1).*(1+1i*2*pi*fj*q);
                Ci(2,2,:)  = C(2,2).*(1+1i*2*pi*fj*q);
                Ci(6,6,:)  = C(6,6).*(1+1i*2*pi*fj*q);
                vL  = squeeze(sqrt(Ci(1,1,:)/rho)).';
                vT  = squeeze(sqrt(Ci(6,6,:)/rho)).';
            end
        elseif paraM.tipoatts==4
          % Azimi 
          Ci  = C;
          vL  = sqrt(C(1,1)/rho);
          vT  = sqrt(C(6,6)/rho);
          % see Kennett, The seismic wavefield, vol1 pag 148-150
          qp = q;
          qs = q*4/3*(vT/vL)^2;
          
          vL = vL*(1+1/pi/qp*log(fj))-1i/2/qp;
          vT = vT*(1+1/pi/qs*log(fj))-1i/2/qs;
          
          Ci(1,1) = rho*vL^2;
          Ci(6,6) = rho*vT^2;
        end
        
        if para.tipoMed(m) == 2
            paratmp.reg(m).sub(ms).ksi = 2*pi*fj./vT;
            paratmp.reg(m).sub(ms).kpi = 2*pi*fj./vL;
            paratmp.reg(m).sub(ms).Ci  = Ci;
        else
            paratmp.reg(m).ksi = 2*pi*fj./vT;
            paratmp.reg(m).kpi = 2*pi*fj./vL;
            paratmp.reg(m).Ci  = Ci;    
        end
        
        if C(1,1)~=0
            x0              = Ci(6,6)/Ci(1,1);
            tmp             = roots([-1,8,8*(2*x0-3),16*(-x0+1)]);
            vR              = vT*sqrt(tmp(abs(tmp)<1));
            paratmp.reg(m).kri = 2*pi*fj/vR;
        end
    end
end