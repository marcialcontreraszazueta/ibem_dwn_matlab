function z = GaussianaSS1983(x,y)
a = 1;
h = 0.5;
xi = 1/a*sqrt(x^2+y^2);
if 0 <= xi && xi <= 1
  z = -h * (1 -3*xi^2 +2*xi^3);
else
  z=0;
end
end