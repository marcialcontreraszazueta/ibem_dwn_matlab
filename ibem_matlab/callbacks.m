function callbacks(arg)
para = evalin('base','para');
bouton = evalin('base','bouton');
info = evalin('base','info');
boutonsal = evalin('base','boutonsal');
dim = get(bouton.dim,'value');
med = get(bouton.med,'value');
submed = get(bouton.submed,'value');
switch arg
  case 'dim'
    cmd_fixboutonGeo;para.dim=get(bouton.dim,'value');cmd_dim;
  case 'rho'
    para.reg(med).rho=str2double(get(bouton.rho,'string'));cmd_lambda_mu;
  case 'alpha'
    para.reg(med).alpha=str2double(get(bouton.alpha,'string'));cmd_lambda_mu;
  case 'bet'
    para.reg(med).bet=str2double(get(bouton.bet,'string'));
    disp(med)
    cmd_lambda_mu;
  case 'lstatts'
    para.reg(med).tipoatts=get(bouton.lstatts,'value');cmd_att;
  case 'Q'
    para.reg(med).qd =str2double(get(bouton.Q  ,'string'));
  case 'subh'
    %         1
    para.reg(med).sub(submed).h=str2double(get(bouton.subh,'string'));
  case 'gfThisPieceContinuosTo'
    val = str2double(get(bouton.gfThisPieceContinuosTo,'string'));
    if(val>para.nmed); 
      warning(' The value should be <= nmed '); 
    else
      para.cont(med,1).piece{info.ThisPiece}.continuosTo = val; 
    end
    clear val; 
  case 'gfThisPieceColor'
    para.cont(med,1).piece{info.ThisPiece}.ColorIndex = ...
      get(bouton.gfThisPieceColor,'value');
  case 'geo'
    if para.dim == 4
    para.geo(med)=get(bouton.geo,'value') + 2;  % <---- ojo  
    else
    para.geo(med)=get(bouton.geo,'value') + 1;  % <---- ojo
    end
    para.chggeo=1; %Si   o cambian los estratos
    cmd_med;
  case 'tipoMed'
    para.tipoMed(med)=get(bouton.tipoMed,'value');
    para.chggeo=1;
    cmd_med;
  case 'xa'
    para.cont(med,1).xa = str2double(get(bouton.xa,'string'));
    para.cont(med,2).xa = para.cont(med,1).xa;
  case 'za'
    para.cont(med,1).za = str2double(get(bouton.za,'string'));
    para.cont(med,2).za = para.cont(med,1).za;
  case 'a'
    para.cont(med,1).a = str2double(get(bouton.a,'string'));
    para.cont(med,2).a = para.cont(med,1).a;
  case 'th'
    para.cont(med,1).th = str2double(get(bouton.th,'string'));
    para.cont(med,2).th = para.cont(med,1).th;
  otherwise
    disp(arg)
    error('callback no implementado')
end

assignin('base','para',para);
assignin('base','bouton',bouton);
assignin('base','info',info);
assignin('base','boutonsal',boutonsal);
end