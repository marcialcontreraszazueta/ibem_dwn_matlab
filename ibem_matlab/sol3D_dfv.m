function [phi_fv,DWN,pT]=sol3D_dfv(para,DWN,j)
% sol3D_dfv calcula la densidad de fuerza (phi) en las fronteras
% para construir el campo difractado/refractado.

%  #### ########  ######## ##     ##         ########  ##      ## ##    ##
%   ##  ##     ## ##       ###   ###         ##     ## ##  ##  ## ###   ##
%   ##  ##     ## ##       #### ####         ##     ## ##  ##  ## ####  ##
%   ##  ########  ######   ## ### ## ####### ##     ## ##  ##  ## ## ## ##
%   ##  ##     ## ##       ##     ##         ##     ## ##  ##  ## ##  ####
%   ##  ##     ## ##       ##     ##         ##     ## ##  ##  ## ##   ###
%  #### ########  ######## ##     ##         ########   ###  ###  ##    ##
%    version:   geometria general 3D, paralelo, compilable, reciprocidad
%               agrupado por z, rXY, (azimuth), cuadratura, truncable
%
%         UNAM.  Marcial Alberto Contreras Zazueta, 2016
%    basado en trabajos (1991 a 2015) de F.J. SAnchez Sesma y M. Perton

if sum(para.tipoMed == 2)>0 %Por lo menos un medio es estratificado
  DWN = DWNprepararMatriceseIndices(para,DWN);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% vector de terminos fuente (incidencia) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[B,DWN]	= vector_fuente3D(para,DWN);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% matriz de condiciones de frontera en puntos de colocaciOn %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[A,pT]=Aij_3D_Block_Recip(para,DWN,para.GraficarCadaDiscretizacion);
save(['../out/PHI','_00',num2str(j),'Amatrix.mat'],'A','B','pT');
phi_fv=A\B;                                %   disp('obtained phi')
clear B
if isdistributed(phi_fv)
  phi_fv = gather(phi_fv);
end

if para.GraficarCadaDiscretizacion
  verPhis3D(para,phi_fv);
end

% Se podria usar la funciones de green en A para construir la soluciOn en
% los puntos de colocacion 'gratis'
clear A;

% Ahora resolver las funciones de Green entre puntos de colocaciOn y
% receptores:  Udiff (Sdiff falta)
[DWN] = makeUdifSdifDisp(para,pT,DWN); % solo desplazamientos, faltan esf.
end