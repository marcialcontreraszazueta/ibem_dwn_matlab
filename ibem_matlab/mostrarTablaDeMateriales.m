function mostrarTablaDeMateriales(para)
disp(['NPPLO = ' num2str(para.npplo)])
format compact
disp('-------------------------------------------------------------------------------')
for med = 1:para.nmed
  if med > 1; anTmed = floor(para.tipoMed(med-1)); else anTmed = 0; end
  if floor(para.tipoMed(med)) == 1 % homo
    if med ==1 || (med>1 && anTmed ==1)
    disp('#  # ,   h     alpha        beta         rho  , qmod   Q , lambda          mu         nu')
    end
    m = para.reg(med);
    nt= [num2str(med) '    |          ' num2str([m.alpha m.bet m.rho],'%12.1f') '|  '...
         num2str([m.tipoatts m.qd]) ' | ' num2str([m.lambda m.mu],'%15.3e') '  ' num2str(m.nu,'%15.3f')];
      disp(nt)
  else % capas
    if med ==1 || (med>1 && anTmed ==2)
    disp('#  # ,   h     alpha        beta         rho  , qmod   Q , lambda          mu         nu')
    end
    for i = 1:para.reg(med).nsubmed
      m = para.reg(med).sub(i);
      nt= [num2str([med i]) ' | ' num2str([m.h m.alpha m.bet m.rho],'%12.1f') '|  '...
         num2str([m.tipoatts m.qd]) ' | ' num2str([m.lambda m.mu],'%15.3e') '  ' num2str(m.nu,'%15.3f')];
      disp(nt)
    end
  end
end
disp('----------------------------------------------------------------------------------------')
format
end