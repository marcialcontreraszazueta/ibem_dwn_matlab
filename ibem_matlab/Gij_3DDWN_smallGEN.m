function GIJ = Gij_3DDWN_smallGEN(xr,... %receptor coord y estrato
  CubPts,CubWts,icr,... % coordenadas y pesos de la cubatura y estrato
  DWN)
% Una fuente distribuida un receptor.
% El receptor cerca de la fuente en un medio estratificado.
% La fuente distribuida en un segmento triangular.
%func de Green [(u v w) x (fx fy fz)]
GIJ = complex(zeros(3,3,1,1));
Gij_iga = complex(zeros(3,3,1,1));
% Procedimiento:
% 1)Fuerza en el receptor y desplazamientos en puntos de
% integración. 2) Reciprocidad y suma con peso de cuadratura.

% Armar vector de fuente en medio estratificado (en el receptor real):
[FsourcePSV,FsourceSH,~,~] = VecfuentDWN(xr(3),DWN);

% Amplitud de las ondas planas que construyen el campo difractado por la
% estratificaciOn:
[SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
  DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);

% Para cada receptor (punto de integraciOn)
ngau = length(CubWts);
%icr : estrato del receptor (punto de int.) .. del centroide del triang.
ksi 	= DWN.sub(icr).ksi;
kpi 	= DWN.sub(icr).kpi;
Ci    = DWN.sub(icr).Ci;
for iga = 1:ngau
  % profundidad relativa a la interface de la capa del receptor:
  zricr = CubPts(3,iga) -  DWN.z(icr);
        
  % tErminos de propagaciOn vertical a las ondas planas que
  % constituyen el campo difractado por la estratificaciOn
  % en el estrato del receptor [fac].
  facU = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
    SolutionPSV,SolutionSH,...
    DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,1,DWN.nkr);
  
  % fase horizontal
  Xfs = CubPts(1,iga)-xr(1); % receptor - fuente
  Yfs = CubPts(2,iga)-xr(2);
  r = sqrt(Xfs^2 + Yfs^2); % distancia
  J0      = besselj(0,DWN.kr*r);
  J1      = besselj(1,DWN.kr*r);
  dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
  d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
  dJ0     =-DWN.kr.*J1;
  d2J0    =-DWN.kr.*dJ1;
  if r<1e-6
    c = 1/sqrt(2);
    s = 1/sqrt(2);
  else
    c = Xfs/r;
    s = Yfs/r;
  end
  
  % cAlculo del campo difractado por la estratificaciOn
  % (3x3). Primer Indice fx,fy,fz. Segundo u,v,w
  MecElemU = campodiffporestrat2(1,...
    r,J0,J1,dJ1,d2J1,dJ0,d2J0,c,s,...
    ksi,kpi,Ci(6,6),facU,DWN.DK);
  
  %func de Green [(fx fy fz) x (u v w)] por aplicar fuerza en 
  % receptor y registrar en punto de integraciOn:
  Gij_iga(:,:,1,1) = Gij_fromMecElem(MecElemU,false);
  
  % Teorema de reciprocidad, Gij(x,xi) = Gji(xi,x),
  % multiplicar por peso de integraciOn y acumular en GIJ
  
  GIJ = GIJ + Gij_iga.*CubWts(iga); %[(u v w) x (fx fy fz)]
end % iga
end