function para = cargardelista(para,J)
needsToUpdate = false;
for m = 1:para.nmed % para cada medio (s�lo inclusiones)
  for p = 1:para.cont(m,1).NumPieces % cada pieza
    if (size(para.cont(m,1).piece{p}.fileName,2)>1) % se carg� algo v�lido
      if size(para.cont(m,1).piece{p}.geoFileData,2)>0 % y hay datos
        kind = para.cont(m,1).piece{p}.kind;
        if kind ~= 3 % si no es una frontera auxiliar
          if para.cont(m,1).piece{p}.isalist
            % revisar si es necesario usar la etapa siguiente:
            idat = para.cont(m,1).piece{p}.stagelist(J+1);
            
              needsToUpdate = true;
%               disp([ '  [m' num2str(m) ' p' num2str(p) ...
%                 ']: Loading boundary stage for ' num2str(jini) ...
%                 '% to ' num2str(jfin) '%'])
              auxcont.isalist = true;
              auxcont.stage(1).fileName = para.cont(m,1).piece{p}.stage(idat).fileName;
              if isfield(para.cont(m,1).piece{p},'geoFileData')
                para.cont(m,1).piece{p}.geoFileData = [];
              end
              if isfield(para.cont(m,1).piece{p},'subdibData')
                para.cont(m,1).piece{p}.subdibData = [];
              end
              [para.cont(m,1).piece{p}.geoFileData,flag] = ...
                justloadSTL(auxcont);
              clear auxcont
%               para.updatebecauseitwasjustloadedfromalist =true;
              if flag == 0 % cuando no se pudo cargar
                disp ('No se pudo cargar')
                disp (para.cont(m,1).piece{p}.fileName)
                disp ('check list')
                stop
              end
%             end
          end % si no es una lista, se queda con lo que ya tiene
        end
      end
    end
  end
end
if(needsToUpdate)
  para = getConnectivityStruct(para);
end
end