load('fortest3.mat')
% addAttachedFiles(gcp,'fortest.mat')
%%
nbeq = para.coord.nbeq;
CubWts = para.cubature(1:12,3);

  fracVert=10^6;
  for im = 1
    if para.tipoMed(im) == 2 % si este medio es estratificado
      cols = pT.m(1,:) == im;       if isempty(cols); continue;end
      coordphi = para.coord.phi(:,im); % indice del phi correspondiente a la fuente
      pTXi = pT.Xi(:,cols);
      pTXi = findlayer(pTXi,DWN{im}.ncapas,DWN{im}.z);
      % Reconocer las fuentes a profundiades iguales:
      [Xiz_uni,Xiz_uniIndex,zrref] = psedoUnique(DWN{im}.sub,...
        DWN{im}.z,DWN{im}.h,pTXi(3,:),pTXi(4,:),DWN{im}.ncapas,fracVert);
      
      rens = para.rec.m(im).ind;    if isempty(rens); return;end
      pcX = zeros(4,length(rens));
      pcX(3,:) = para.rec.zr(rens);
      pcX(2,:) = para.rec.yr(rens);
      pcX(1,:) = para.rec.xr(rens);
      pcX  = findlayer(pcX ,DWN{im}.ncapas,DWN{im}.z);

            
      % repartir las fuentes
      leXiz_uni = length(Xiz_uni);
      thisDWNim = DWN{im};
      
%       % se reparten los procesos en matlab
%         Udiffc = cell(leXiz_uni,1);
%         % Sdiffc = cell(leXiz_uni,1);
%         disp(['Udifrepartido m' num2str(im)])
%         parfor iii = 1:leXiz_uni
%           j = find(zrref==iii);
%           Xiz = Xiz_uni(iii);
%           [Udiffc{iii},~] = makeUdifSdifDisp(nbeq,coordphi,CubWts,pcX,...
%             thisDWNim,pT,im,j,Xiz);
%         end
%         DWN{im}.Udiff  = zeros(DWN{im}.Udiffsize);
%         % DWN{im}.Sdiff  = zeros(DWN{im}.Sdiffsize);
%         for ic = 1:leXiz_uni % juntarlos
%           DWN{im}.Udiff=DWN{im}.Udiff+Udiffc{ic};
%           % DWN{im}.Sdiff=DWN{im}.Sdiff+Sdiffc{ic};
%         end
%         
%         
      disp('Udif repartido matlab')
        
        [compUdiff1,~] = makeUdifSdifDisp2(nbeq,coordphi,CubWts,pcX,...
            thisDWNim,pT,im,leXiz_uni,zrref,Xiz_uni);
       
%       disp('Udif repartido compilado')
%         [compUdiff2,~] = makeUdifSdifDisp2_mex(nbeq,coordphi,CubWts,pcX,...
%             thisDWNim,pT,im,leXiz_uni,zrref,Xiz_uni);
          
        disp('done')
        
%        disp(abs(sum(sum(sum(sum(compUdiff1 - compUdiff2))))));
    end
  end

 % [DWN] = makeUdifSdiff(para,DWN,pT);
 % disp(abs(sum(sum(sum(sum(DWN{im}.Udiff - compUdiff))))));
