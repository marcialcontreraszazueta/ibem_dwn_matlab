function [utc] = BS6841_W(uw,para,espe,key)
% Pesos para evaluar la exposicion a la vibracion en el cuerpo. BS 6841 (1987) equivale a iso2631

f1=0.4;f2=100;Q1=0.71;
switch key
    case 'b'
        f3=16;f4=16;f5=2.5;f6=4;Q2=0.55;Q3=0.90;Q4=0.95;K=0.4;
    case 'c'
        f3=8;f4=8;f5=NaN;f6=NaN;Q2=0.63;Q3=NaN;Q4=NaN;K=1.0;
    case 'd'
        f3=2;f4=2;f5=NaN;f6=NaN;Q2=0.63;Q3=NaN;Q4=NaN;K=1.0;
    case 'e'
        f3=1;f4=1;f5=NaN;f6=NaN;Q2=0.63;Q3=NaN;Q4=NaN;K=1.0;
    case 'f'
        f1=0.08;f2=0.63;Q1=0.71;
        f3=NaN;f4=0.250;f5=0.0625;f6=0.10;Q2=0.86;Q3=0.8;Q4=0.8;K=0.4;
    case 'g'
        f1=0.8;f2=100;Q1=0.71;
        f3=1.5;f4=5.3;f5=NaN;f6=NaN;Q2=0.63;Q3=NaN;Q4=NaN;K=0.42;
    otherwise
        error('key invalido')
end


% utc : sismograma sintetico con espectro pesado por percepcion humana
if para.zeropad < para.nf; para.zeropad = para.nf; end

nf      = para.nf;       disp(['nf = ',num2str(nf)])
nfN     = nf/2+1;
para.df = para.fmax/(nf/2);     %paso en frecuencia
df      = para.fmax/nfN; disp(['df = ',num2str(df)])

% vector de frecuencias
Fq      = (0:nf/2)*df;   disp(['Fmx= ',num2str(Fq(end))]) 

% vector de frecuencia circular compleja  s = 2pi j f  
% (operador de Laplace)
s = 1j*Fq*2*pi;

s2 = s.^2;
% band-limiting filters
Hb = (s2*4*pi^2*f2^2)./((s2+(2*pi*f1)/Q1*s+4*pi^2*f1^2).*(s2+(2*pi*f2)/Q1*s+4*pi^2*f2^2));

% funcion de transferenica del filtro
a1 = (s+2*pi*f3)./(s2+(2*pi*f4)/Q2*s+4*pi^2*f4^2);
a2 = (2*pi*K*f4^2)/f3;

if contains('cdeg',key)
    Hw = a1.*a2;
end
if contains('bf',key)
    a3 = (s2+(2*pi*f5)/Q3*s+4*pi^2*f5^2)./(s2+(2*pi*f6)/Q4*s+4*pi^2*f6^2);
    a4 = f6^2/f5^2;
    Hw = a1.*a3*a2*a4;
end

% trazamos el modulo para validar
% figure;
% plot(Fq,abs(Hb),'-k','DisplayName','|Hb|');hold on;
% plot(Fq,abs(Hw),'-b','DisplayName','|Hw|')
% plot(Fq,abs(Hw.*Hb),'-r','DisplayName','|HbHw|')
% legend

% zeropad = para.zeropad;
% tps     = 0:(1/(df*2*(nfN+zeropad))*(2*(nfN+zeropad)/(2*(nfN+zeropad)-2))):1/df;
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
tps = (0:para.zeropad-1)*dt; disp(['tmx= ',num2str(tps(end))])

nuw     = size(uw);
% funcion de amplitud
if isempty(espe)
    cspectre = correction_spectre(para,nfN,df);
else
    cspectre = espe;
end

% aplicar filtro y pesos
cspectre = cspectre.*Hb.*Hw;

% wrap de las dimensiones
if ~isempty(uw)
    ntot=1;
    nnuw=length(nuw);
    for i=2:length(nuw)
        ntot=ntot*nuw(i);
    end
    uw      = reshape(uw,nuw(1),ntot);
    utc     = zeros(para.zeropad,ntot);
    for i=1:ntot
        tmp     = uw(1:nfN,i).';
        tmp     = tmp.*cspectre;
        
        % crepa:
        vec = zeros(1,para.zeropad);
        vec(1:nfN) = tmp(1:nfN);
        vec(end-nfN+3:end)=conj(tmp(nfN-1:-1:2));
        % escala:
        dt_nopad = 1/df/(nf-1); % dt sin zeropading
        sca = dt_nopad*para.zeropad/nf/nf;
        utc(:,i)= real(sca*ifft(vec)).*exp(para.DWNomei*tps); % inversa y frec imag
    end
    
    utc   = reshape(utc,[para.zeropad,nuw(2:nnuw)]);
else
    utc = [];
end



end
