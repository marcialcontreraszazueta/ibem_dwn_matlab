function MecElem = campodiffporestratAngular(tipo,U,...
  ct,st,Ci66)
% campodiffporestratAngular Agrega la fase angular para cada receptor
 
if tipo == 1
MecElem = complex(zeros(3,3));
else
MecElem = complex(zeros(3,3,3));
end
if tipo == 1 % Desplazamientos
%   MecElem = complex(zeros(3,3)); % Primer �ndice [fx,fy,fz], segundo el componente
  % fx %
  Urx = U.Urx;
  Utx = U.Utx;
  Uzx = U.Uzx;
  %UXW_fx(1,ir,ixs) 
  MecElem(1,1) = (Urx*ct^2-Utx*st^2);          %Uxx
%   UXW_fx(2,ir,ixs) 
  MecElem(1,2) = (Urx+Utx)*ct*st;              %Uyx
%   UXW_fx(3,ir,ixs) 
  MecElem(1,3) = Uzx*ct;                       %Uzx
  
  
%   UXW_fy(1,ir,ixs)
  MecElem(2,1) = (Urx+Utx)*ct*st;              %Uxy=Uyx, G12=G21
%   UXW_fy(2,ir,ixs) 
  MecElem(2,2) = (Urx*st^2-Utx*ct^2);          %Uyy=Uxx, th= -(pi/2-th), ct->st,st>-ct
%   UXW_fy(3,ir,ixs) 
  MecElem(2,3) =  Uzx*st;
  
  % fz %
  Urz = U.Urz;
  Uzz = U.Uzz;
%   UXW_fz(1,ir,ixs) 
  MecElem(3,1) = Urz*ct;                   %UxzW
%   UXW_fz(2,ir,ixs) 
  MecElem(3,2) = Urz*st;                   %UyzW
%   UXW_fz(3,ir,ixs) 
  MecElem(3,3) = Uzz;                      %UzzW
end
if tipo == -1 % Esfuerzos
%   MecElem = complex(zeros(3,3,3));% Primer �ndice [fx,fy,fz], segundo y tercero tensor
  % fx *
  
  Szrx = U.Szrx;
  Sztx = U.Sztx;
  Szzx = U.Szzx;
  Srrx = U.Srrx;
  Sttx = U.Sttx;
  Srtx = U.Srtx;
  
  % Sp=[Srrx Srtx Srzx; Strx Sttx Stzx; Szrx Sztx Szzx];
  Sp=[ct*Srrx -st*Srtx ct*Szrx; -st*Srtx ct*Sttx -st*Sztx; ct*Szrx -st*Sztx ct*Szzx];
  T =[ ct    -st    0  ;   st    ct    0  ;   0     0     1  ];
  Sc= T*Sp*(T.');
  %Sc=[SxxxW SxyxW SxzxW; SyxxW SyyxW SyzxW; SzxxW SzyxW SzzxW];
%   SXW_fx(:,:,ir,ixs) 
  MecElem(1,:,:) = Ci66*Sc;
%   for i=1:3
%     for j=1:3
%       MecElem(1,i,j) = Ci66(1,1)*Sc(i,j);
%     end
%   end
  
  % theta de la fuerza solamente: th= -(pi/2-th), ct->st,st>-ct
  Sp=[st*Srrx ct*Srtx st*Szrx; ct*Srtx st*Sttx ct*Sztx; st*Szrx ct*Sztx st*Szzx];
  T =[ ct    -st    0  ;   st    ct    0  ;   0     0     1  ];
  Sc= T*Sp*(T.');
%   SXW_fy(:,:,ir,ixs) 
  MecElem(2,:,:) = Ci66*Sc;
%   for i=1:3
%     for j=1:3
%       MecElem(2,i,j) = Ci66(1,1)*Sc(i,j);
%     end
%   end
  
  % fz %
  Szrz = U.Szrz;
  Sztz = U.Sztz;
  Szzz = U.Szzz;
  Srrz = U.Srrz;
  Sttz = U.Sttz;
  Srtz = U.Srtz;
  
  %Sp=[Srrz Srtz Srzz; Strz Sttz Stzz; Szrz Sztz Szzz];
  Sp=[Srrz Srtz Szrz; Srtz Sttz Sztz; Szrz Sztz Szzz];
  T =[ ct   -st   0 ;   st   ct   0 ;   0    0    1 ];
  Sc= T*Sp*T.'; % a coordendas cartesianas
  
%   SXW_fz(:,:,ir,ixs) 
  MecElem(3,:,:) = Ci66*Sc;
%   for i=1:3
%     for j=1:3
%       MecElem(3,i,j) = Ci66(1,1)*Sc(i,j);
%     end
%   end
end
end