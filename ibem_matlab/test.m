load('fortest.mat')
for  ib = 1:nBLO
  % BLOCKS se convierte en una variable rebanada (sliced)
  if BLOCKS{ib}.isrelatedTo ~= -1
    %[BLOCKS{ib},~] = Fill_TG_block(BLOCKS{ib},pc,pT,mat,CubWts);
    BB = BLOCKS{ib};
    rens = BB.ri   : BB.rf;
    cols = BB.ci   : BB.cf;
    pcX = pc.X(:,rens);
    pcVn = pc.Vn(:,rens);
    pcXim = pc.Xim(:,rens);
    m = BB.m; % medio en el que se resuelve G o T
    im0 = BB.im0; % medio con el que estA en contacto esta frontera
    pTXi = pT.Xi(:,cols);
    pTdA = pT.dA(:,cols);
    pTcubPts = pT.CubPts(:,:,cols);
    if BB.tipo == 'T'
  tipo = -1;
elseif BB.tipo == 'G'
  tipo = 1;
    end
pcX = findlayer(pcX,BB.DWN.ncapas,BB.DWN.z);
  pTXi = findlayer(pTXi,BB.DWN.ncapas,BB.DWN.z);
  
  % un solo procesador
  [Abdirecto,~] = calcBlockDWN(tipo,...
  length(rens),pcX,pcVn,pcXim,m,im0,...
  length(cols),pTXi,pTdA,pTcubPts,...
  BB.isSelfReciproc,BB.isrelatedTo,...
  CubWts,BB.DWN); 

% Calculo hasta 12 procesadores de matlab
[Abdisp1,~] = calcBlockDWNdisp(tipo,...
  length(rens),pcX,pcVn,pcXim,m,im0,...
  length(cols),pTXi,pTdA,pTcubPts,...
  BB.isSelfReciproc,BB.isrelatedTo,...
  CubWts,BB.DWN); 

Abdisp2 = calcBlockDWNdisp2(tipo,...
  length(rens),pcX,pcVn,pcXim,m,im0,...
  length(cols),pTXi,pTdA,pTcubPts,...
  BB.isSelfReciproc,BB.isrelatedTo,...
  CubWts,BB.DWN); 

if ~(exist('calcBlockDWNdisp2_mex.mexa64','file')==3)
  disp('os x')
  Ab = Abdirecto;
else
Ab = calcBlockDWNdisp2_mex(tipo,...
  length(rens),pcX,pcVn,pcXim,m,im0,...
  length(cols),pTXi,pTdA,pTcubPts,...
  BB.isSelfReciproc,BB.isrelatedTo,...
  CubWts,BB.DWN);
end
disp('A directo - ')
disp('disp disp2 disp2_mex')
disp([abs(sum(sum(sum(sum(Abdirecto - Abdisp1))))) ...
      abs(sum(sum(sum(sum(Abdirecto - Abdisp2))))) ...
      abs(sum(sum(sum(sum(Abdirecto - Ab)))))]);
  end
end
disp('done')
%%