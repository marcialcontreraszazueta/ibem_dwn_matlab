function [J0,J1] = BessJ01(V)
% BessJ01 funciones de Bessel de primera especia orden 0 y 1
% Basado en funcion BESJC programada por SAnchez-Sesma en fortran 70 con
% algoritmo de Abramowitz y Stegun.  Marcial Contreras Zazueta 2016

% Se traduce a matlab para poder compilar en C dentro de un programa en
% Matlab. Se intentO utlizar la version compilada como mex function pero
% Matlabcoder no la admite.

%      V=ARGUMENTO COMPLEJO
NMAX = 1;%      NMAX=ORDEN MAXIMO
J0 = complex(zeros(1,1));
J1 = complex(zeros(1,1));
D = 10;  %      D=CIFRAS SIGNIFICATIVAS REQUERIDAS
%      U(N)=FUNCIONES DE BESSEL DE PRIMERA ESPECIE JN(V), N=0,1
U  = complex(zeros(1,3));
RR = complex(zeros(1,2));

EPS=0.5*10.0^(-D);
NMA=NMAX+1;
X=real(V);
Y=imag(V);
%      I=(0.0,1.0)
if (X <= 0 && Y == 0) %|| (NMAX < 0 || NMAX > 100) %porque NMAX esta dado
  if (X ==0)
    J0 = complex(1);
    J1 = complex(0);
    return
  end
  disp('RE(V)<0 & IM(V)==0')
  disp('ORDEN O ARGUMENTO INVALIDO')
  return
end
UI = complex(zeros(1,2)); % flag 10
LL = Y >= 0;
Y = abs(Y);
Z = complex(X,Y);
R0 = abs(Z);
% R02 = R0*R0;
SUM = exp(-1i*Z);
D1=2.3026*D+1.3863;

S1=0.0;
if (NMAX > 0) % IF(NMAX.EQ.0)GO TO 50
  X=0.5*D1/NMAX;
  S1 = S1fromX(X); %flag 20
end
R1=S1*NMAX;

if (Y-D1)<0
  X=0.73576*(D1-Y)/R0;
  S1 = S1fromX(X);
  S1=1.3591*R0*S1;
else %>= 0
  S1=1.3591*R0;
end

if(R1-S1)<=0 % flag 61
  % hacer flag 62
  NU=1.0+S1; % flag 62
%   GO TO 70 ok
else % > 0
  % hacer flag 63
  NU=1.0+R1; % flag 63
end

again70=true;
while again70
  N=0; % flag 70
  L=1.0;
  C=complex(1);
  again80 = true;
  while again80
    N=N+1;% flag 80
    L=N*L/(N+1);
    C=-C*1i;
    if (N < NU)
      again80 = true;
    else
      again80 = false;
    end
  end %IF(N.LT.NU)GO TO 80
  R=complex(0);
  S=complex(0);
  again81 = true;
  while again81
    R=1.0/(2.0*N/Z-R); % flag 81
    L=(N+1)*L/N;
    LAMDA=2.0*N*L*C;
    C=C*1i;
    S=R*(LAMDA+S);
    if (N <= NMAX)
      RR(N) = R;
    end
    N=N-1;
    if (N >= 1)
      again81 = true;
    else
      again81 = false;
    end
  end
  U(1)=SUM/(1.0+S);
  if (NMAX ~= 0) %IF(NMAX.EQ.0)GO TO 90
    for K=1:NMAX % loop 82
      U(K+1) = U(K)*RR(K); % flag 82
    end
  end
  if (~LL) %flag 90   IF(LL)GO TO 92
    for K = 1:NMA % loop 91
      U(K) = conj(U(K)); % flag 91
    end
  end
  % flag 92
  
  again70 = false;
  for K=1:NMA % loop 100
    if (abs(U(K) - UI(K))/abs(U(K)) > EPS)
      % goto 101
      NU=NU+5; %flag 101
      for KKK=1:NMA %loop 102
        UI(KKK) = U(KKK); %flag 102
      end
      again70 = true; %GO TO 70
      break %100
    end
  end%100
end %again70

J0 = U(1);
J1 = U(2);
end

function S1 = S1fromX(X)
if (X <= 10.0) %flag 20        IF(X.GT.10.0)GO TO 30
    P=X*5.7941D-5 -1.76148D-3;
    P=X*P+2.08645D-2;
    P=X*P-1.29013D-1;
    P=X*P+8.57770D-1;
    S1=X*P+1.01250;
    %       GO TO 40
else
    Q=log(X)-0.775;  % flag 30
    P=(0.775-log(Q))/(1.0+Q);
    P=1.0/(1.0+P);
    S1=X*P/Q;
end
end