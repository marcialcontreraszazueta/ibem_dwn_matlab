para.film.filmeRange = eval(get(bouton.bfilmeRangeBt,'string'));
set(info.filmeRangeTime,'string','');
if exist('RESULT','var')
  nf      = para.nf;
  nfN     = nf/2+1; %Nyquist
  df      = para.fmax/nfN;     %paso en frecuencia
  dt = 1/(df*para.zeropad);
  tps = (0:para.zeropad-1)*dt;
  
  if para.film.filmeRange(end) > length(tps)
    para.film.filmeRange(para.film.filmeRange(:) > length(tps)) = [];
  end
  u = RESULT.utc(para.film.filmeRange,1,1,1);
  str = [num2str(tps(para.film.filmeRange(1)),2) ' : ' ...
         num2str(dt,2) '[' num2str(size(u,1)) '] : ' ...
         num2str(tps(para.film.filmeRange(end)),2)];
  set(info.filmeRangeTime,'string',str);
  clear u nf df nfN zerospad dt tps str
else
  nf      = para.nf;
  nfN     = nf/2+1; %Nyquist
  df      = para.fmax/nfN;     %paso en frecuencia
  dt = 1/(df*para.zeropad);
  tps = (0:para.zeropad-1)*dt;
  
  if para.film.filmeRange(end) > length(tps)
    para.film.filmeRange(para.film.filmeRange(:) > length(tps)) = [];
  end
  str = [num2str(tps(para.film.filmeRange(1)),2) ' : ' ...
         num2str(dt,2) ' : ' ...
         num2str(tps(para.film.filmeRange(end)),2)];
  set(info.filmeRangeTime,'string',str);
  clear nf df nfN zerospad dt tps str
end