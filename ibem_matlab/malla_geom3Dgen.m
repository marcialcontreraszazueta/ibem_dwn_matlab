function para = malla_geom3Dgen(para,fj)
% Refinar superficies para cumplir la nUmero de puntos por longitud de onda
% Asume que la frecuencia va aumentando.
% Refina sOlo los triAngulos cuya Area es tal que un cIrculo de la misma Area
% y rAdio 'r' es mayor que la 1/2 lambda/npplo. Donde lambda es la longitud
% de onda mAs pequeNa en esa frontera.
%
% SubdivisiOn: Los triAngulos se parten en dos desde la mitad del lado mAs
% largo al vErtice opuesto. Se modi

%ya estamos en el ciclo de frecuencias
fj      = real(fj);
npplo   = para.npplo;
%%%%%%%%%%%%%%%%%
%   subdividir  %
%%%%%%%%%%%%%%%%%
maxSteps = 20;

for m = 1:para.nmed % para cada medio (sOlo inclusiones)
  for p = 1:para.cont(m,1).NumPieces % cada pieza
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      % longitud de onda mAs pequeAa (entre ambas caras) de cada elemento:
      lambda  = para.cont(m,1).piece{p}.subdibData.minVel/fj;
      lambdaObjetivo = lambda/npplo; % serIa  2r  de un cIrculo
      %
      %       .
      %     .    .
      %   .        .          .      Una longitud de onda
      %              .     .
      %                 .
      %     ___________________
      %     /\    /\    /\    /
      %    /  \  /  \  /  \  /       6 segmentos de integraciOn
      %   /    \/    \/    \/
      %   ?????????????????? 
      radioObjetivo = lambdaObjetivo / 2; % como si fueran cIrculos
      para.cont(m,1).piece{p}.subdibData.radioObjetivo = radioObjetivo;
      %               disp(['lambda Objetivo ',num2str(lambdaObjetivo)])
      %               disp(['radio  Objetivo ',num2str(radioObjetivo)])
      %               disp(['maxRadio ',num2str(max(para.cont(m,1).piece{p}.subdibData.radios))])
      % indices de los triAngulos que se deben subdividir:
      iStep = 0;
      for splits = [2 1]%[2 1.5 1]
        deNuevo = true;
        while deNuevo
          iStep = iStep + 1;
          l = find(para.cont(m,1).piece{p}.subdibData.radios>radioObjetivo*splits);
          l = reshape(l,1,length(l));
          %         disp(['at ' num2str(fj) ' on p',num2str(p),' splitting ' num2str(length(l))]);
          if ~isempty(l)
            NewTriangs = [];
            for t = l % cada triAngulo grandote se parte en cuatro o dos
              someNewTriags = splitThisTriangle(para.cont(m,1).piece{p}.subdibData,t,splits);
              someNewTriags.radioObjetivo = radioObjetivo(t).*ones(1,2*splits);
              %             twoNewTriangs = splitThisTriangle(para.cont(m,1).piece{p}.subdibData,t,splits);
              %             twoNewTriangs.radioObjetivo = [radioObjetivo(t) radioObjetivo(t)];
              
              NewTriangs = apilar(NewTriangs,someNewTriags);
            end
            % borrar los triAngulos l
            para.cont(m,1).piece{p}.subdibData = borrar(l,para.cont(m,1).piece{p}.subdibData);
            
            % agregar los nuevos triAngulos
            para.cont(m,1).piece{p}.subdibData = apilar(NewTriangs,para.cont(m,1).piece{p}.subdibData);
            
            radioObjetivo = para.cont(m,1).piece{p}.subdibData.radioObjetivo;
            %           disp(['m',num2str(m),' p',num2str(p),' newT_',num2str(length(NewTriangs.centers))])
          else
            deNuevo = false;
          end
          if iStep > maxSteps; disp('max subdiv steps reached'); deNuevo = false; end
        end % subdividir deNuevo
        %       disp(['m',num2str(m),' p',num2str(p),' nColocPts:_', ...
        %         num2str( size(para.cont(m,1).piece{p}.subdibData.centers,2))]); %cant. triangs
        %
        %
        %               figure(343); hold on
        %               sdD = para.cont(m,1).piece{p}.subdibData;
        %               for t = 1:size(sdD.triangles,3)
        %                 plot3(sdD.triangles(1,[1 2 3 1],t),...
        %                   sdD.triangles(2,[1 2 3 1],t),...
        %                   sdD.triangles(3,[1 2 3 1],t),'k-','LineWidth',0.5)
        %                 plot3(sdD.centers(1,t),...
        %                       sdD.centers(2,t),...
        %                       sdD.centers(3,t),'k.')
        %               end
        %               clear sdD
        
      end %modo
    end
  end
end


%% subdividir MAS donde la superficie no es suave
if false
  for m = 1:para.nmed % para cada medio (sOlo inclusiones)
    
    % seleccionar todos los triangulos de las superficies en este medio
    c = [];  r = []; n = []; ip=[];
    for p = 1:para.cont(m,1).NumPieces % cada pieza
      kind = para.cont(m,1).piece{p}.kind;
      if kind ~= 3 % si no es una frontera auxiliar
        % centro de cada triangulo en esta pieza
        c = [c para.cont(m,1).piece{p}.subdibData.centers];
        r = [r; para.cont(m,1).piece{p}.subdibData.radios];
        n = [n para.cont(m,1).piece{p}.subdibData.N.'];
        ip = [ip; p*ones(size(para.cont(m,1).piece{p}.subdibData.radios))];
      end
    end
    
    % identificar cuales triangulos estAn
    %  a) muy cerca de otro
    %  b) mas o menos cerca pero con orientacion muy distinta
    l = false(size(r));
    for i=1:length(r)
      % comparando distancias
      d = ((c(1,:) - c(1,i)).^2 + ...
        (c(2,:) - c(2,i)).^2 + ...
        (c(3,:) - c(3,i)).^2 ).^0.5 < (2*r(i));
      d(i) = false;
      d(l) = false;
      % de estas, las que tienen normales con orientancion muy distinta
      for dd = find(d)
        if abs(dot(n(:,dd),n(:,i)))>0.866 && ...% cos(angulo * pi/180)
            (((c(1,dd) - c(1,i))^2 + ...
            (c(2,dd) - c(2,i))^2 + ...
            (c(3,dd) - c(3,i))^2 )^0.5 > r(i))
          d(dd) = false;
        end
      end
      if sum(d) > 0
        l(d) = true;
      end
      %     %testme
      %     if sum(d) > 0
      %       figure(1)
      %        hold on
      % %        plotCircle3D(c(:,i).',[1 0 0],2*r(i),1);
      % %        plotCircle3D(c(:,i).',[0 1 0],2*r(i),1);
      % %        plotCircle3D(c(:,i).',[0 0 1],2*r(i),1);
      %        plot3(c(1,i),c(2,i),c(3,i),'rx')
      % %        plot3(c(1,d),c(2,d),c(3,d),'k*')
      %     end
    end
    
    % subdividir los triangulos que cumplen dichas condiciones
    splits = 1;
    l = find(l).';
    if sum(l) > 0
      for p = 1:para.cont(m,1).NumPieces % cada pieza
        kind = para.cont(m,1).piece{p}.kind;
        if kind ~= 3 % si no es una frontera auxiliar
          NewTriangs = [];
          % recortar l
          all = false(size(ip));
          all(l)= true;
          all(ip ~= p) = false;
          lthisp = find(all).';
          if p>1
            ppp = p-1;
            while ppp>0
              lthisp = lthisp - sum(ip == ppp);
              ppp = ppp-1;
            end
          end
          
          
          for t = lthisp % cada triAngulo grandote se parte en cuatro o dos
            someNewTriags = splitThisTriangle(para.cont(m,1).piece{p}.subdibData,t,splits);
            someNewTriags.radioObjetivo = radioObjetivo(t).*ones(1,2*splits);
            %             twoNewTriangs = splitThisTriangle(para.cont(m,1).piece{p}.subdibData,t,splits);
            %             twoNewTriangs.radioObjetivo = [radioObjetivo(t) radioObjetivo(t)];
            
            NewTriangs = apilar(NewTriangs,someNewTriags);
          end
          
          
          % borrar los triAngulos l
          para.cont(m,1).piece{p}.subdibData = borrar(lthisp,para.cont(m,1).piece{p}.subdibData);
          
          % agregar los nuevos triAngulos
          para.cont(m,1).piece{p}.subdibData = apilar(NewTriangs,para.cont(m,1).piece{p}.subdibData);
          
          radioObjetivo = para.cont(m,1).piece{p}.subdibData.radioObjetivo;
          %           disp(['m',num2str(m),' p',num2str(p),' newT_',num2str(length(NewTriangs.centers))])
        end
      end
    end
  end
end
%%

% Recorrer los centros lejos del lado mAs largo
if false % Este es util cuando la malla se hace con triangulacion Delaunay
  for m = 1:para.nmed % para cada medio (sOlo inclusiones)
  for p = 1:para.cont(m,1).NumPieces % cada pieza
%       figure; hold on; axis equal
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      sdD = para.cont(m,1).piece{p}.subdibData;
      for t = 1:size(sdD.centers,2)
%         plot3(sdD.triangles(1,[1 2 3 1],t),...
%           sdD.triangles(2,[1 2 3 1],t),...
%           sdD.triangles(3,[1 2 3 1],t),'k-','LineWidth',0.5)
%         plot3(sdD.centers(1,t),...
%           sdD.centers(2,t),...
%           sdD.centers(3,t),'k.')
%         h = plotCircle3D([sdD.centers(1,t),sdD.centers(2,t),sdD.centers(3,t)],...
%           [sdD.N(t,1),sdD.N(t,2),sdD.N(t,3)],sdD.radios(t),1);
%         set(h,'color',[0,0.5,0])
        % Encontrar lado mAs largo, su centro y el Indice de vErtice opuesto
        l = zeros(3,6);
        l(1,1:6) = lado(sdD.triangles(:,2,t),sdD.triangles(:,3,t),2,3);
        l(2,1:6) = lado(sdD.triangles(:,1,t),sdD.triangles(:,3,t),1,3);
        l(3,1:6) = lado(sdD.triangles(:,1,t),sdD.triangles(:,2,t),1,2);
        lM = find(l == max(l(:,1)),1); %indice del lado mAs grande y del vertice opeusto
        
        % normal y distancia entre centro de lado mAs largo y vErtice opuesto
        dl = sqrt(sum(( squeeze(sdD.triangles(:,lM,t)) - l(lM,2:4).' ) .^2));
        nl = ( squeeze(sdD.triangles(:,lM,t)) - l(lM,2:4).' ) / dl;
        
        % Recorrer Centro lejos del lado mAs largo
        rd = l(lM,1)^0.5 / dl / 120;%72; %<-- empirico
        sdD.centers(1,t) = sdD.centers(1,t) + nl(1)*rd;
        sdD.centers(2,t) = sdD.centers(2,t) + nl(2)*rd;
        sdD.centers(3,t) = sdD.centers(3,t) + nl(3)*rd;
%         plot3(sdD.centers(1,t),...
%           sdD.centers(2,t),...
%           sdD.centers(3,t),'r.')
%         h = plotCircle3D([sdD.centers(1,t),sdD.centers(2,t),sdD.centers(3,t)],...
%           [sdD.N(t,1),sdD.N(t,2),sdD.N(t,3)],sdD.radios(t),1);
%         set(h,'color',[0.5,0,0])
      end
    end
  end
  end
end

% Puntos de cubatura
for m = 1:para.nmed % para cada medio (sOlo inclusiones)
  for p = 1:para.cont(m,1).NumPieces % cada pieza
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      [para.cont(m,1).piece{p}.subdibData.trianglesCubaturePoints]...
        = XYZ_FromBarycentric_Triang(...
        para.cont(m,1).piece{p}.subdibData.triangles,...
        para.cubature);
      %       [para.cont(m,1).piece{p}.subdibData.trianglesCubaturePointsex]...
      %         = XYZ_FromBarycentric_Triang(...
      %         para.cont(m,1).piece{p}.subdibData.triangles,...
      %         para.cubatureex);
      
      %               %% Test me:
      %               warning('imprimiendo puntos de cubatura')
      %               figure(343); hold on
      %               sdD = para.cont(m,1).piece{p}.subdibData;
%                     for t = 1:size(sdD.triangles,3)
%                       plot3(sdD.triangles(1,[1 2 3 1],t),...
%                         sdD.triangles(2,[1 2 3 1],t),...
%                         sdD.triangles(3,[1 2 3 1],t),'k-','LineWidth',0.5)
%                       plot3(sdD.centers(1,t),...
%                             sdD.centers(2,t),...
%                             sdD.centers(3,t),'k.')
      %                 plot3(sdD.trianglesCubaturePoints(1,:,t),...
      %                       sdD.trianglesCubaturePoints(2,:,t),...
      %                       sdD.trianglesCubaturePoints(3,:,t),'r.')
      %               end
      %               clear sdD
      %               %%
    end
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% variables para ensamblar dominios
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbpt0 = 0; % cantidad de puntos de colocaciOn
% medio(para.nmed).ind =[];
for m=1:para.nmed;
  for p = 1:para.cont(m,1).NumPieces
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      n = size(para.cont(m,1).piece{p}.subdibData.centers,2); %cant. triangs
      nbpt0 = nbpt0 + n;
    end
  end
end

% Exportar geometria para verificar con programa en fortran
if false 
  clear tx
  tx = sprintf('%s \n',[num2str(nbpt0), ' elementos en total']);
  for m=1:para.nmed;
  for p = 1:para.cont(m,1).NumPieces
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      sdD = para.cont(m,1).piece{p}.subdibData;
      tx = sprintf('%s \n',[tx,'pieza ', num2str(p), ': ', num2str(size(sdD.centers,2)), ' elementos']);
      for t = 1:size(sdD.centers,2)
        tx = sprintf('%s %f %f %f %f %f %f %f\n',tx,...
           sdD.centers(1,t),sdD.centers(2,t),sdD.centers(3,t),...
           sdD.N(t,1),sdD.N(t,2),sdD.N(t,3),sdD.radios(t));
      end
    end
  end
  end
  cd ..
  cd out
  fileID = fopen('GeomForFortran.txt','w');
  fprintf(fileID,'%s',tx);
  fclose(fileID);
  cd ..
  cd ibem_matlab
  disp('saved ../out/GeomForFortran.txt')
  error('fin')
end

%% inicio
n1          = para.nmed;
coord       = struct( ...
  'x'     ,zeros(1,nbpt0),	'y'     ,zeros(1,nbpt0) , 'z'   ,zeros(1,nbpt0), ...
  'vnx'   ,zeros(1,nbpt0),	'vny'   ,zeros(1,nbpt0) , 'vnz' ,zeros(1,nbpt0), ...
  'drxz'  ,zeros(1,nbpt0),  'cv'    ,zeros(nbpt0,1) , 'dA'  ,zeros(1,nbpt0),...
  'Xim' 	,zeros(nbpt0,n1), 'phi'  	,zeros(nbpt0,n1), ...
  'CubPts',zeros(3,para.gaussian.ngau,nbpt0)           , ...
  'fl'  	,zeros(nbpt0,n1), 'indm'  ,[]             , ...
  'ju', [], 'nbeq', [], 'nfl', [],...
  'm',zeros(1,nbpt0),'p',zeros(1,nbpt0),...
  'mint',zeros(1,nbpt0),'mext',zeros(1,nbpt0));%, ...
%   'x0',zeros(1,nbpt0) ,'th',zeros(1,nbpt0), 'nbptc',zeros(1,nbpt0));
coord.nbpt  = nbpt0;
%% x,y,z,vnx,vny,vnz,cv,Xim,phi,fl;  (no r)
jjF = 0; jp  = 0;    %initializacion del indice de los phi
for m=1:para.nmed;
  for p = 1:para.cont(m,1).NumPieces
    kind = para.cont(m,1).piece{p}.kind;
    if kind ~= 3 % si no es una frontera auxiliar
      n = size(para.cont(m,1).piece{p}.subdibData.centers,2); %cant. triangs
      jjI = jjF + 1;
      jjF = jjF + n;
      jj = jjI:jjF; %indices de putnos de colocaciOn actuales
      
      %indices para recordar de donde saliO
      coord.m(jj) = m;
      coord.p(jj) = p;
      % lo demAs:
      coord.x(jj) = para.cont(m,1).piece{p}.subdibData.centers(1,:);
      coord.y(jj) = para.cont(m,1).piece{p}.subdibData.centers(2,:);
      coord.z(jj) = para.cont(m,1).piece{p}.subdibData.centers(3,:);
      %     coord.x0(jj) =
      coord.vnx(jj) = para.cont(m,1).piece{p}.subdibData.N(:,1);
      coord.vny(jj) = para.cont(m,1).piece{p}.subdibData.N(:,2);
      coord.vnz(jj) = para.cont(m,1).piece{p}.subdibData.N(:,3);
      coord.cv(jj) = para.cont(m,1).piece{p}.subdibData.cv;
      
      % puntos de integraciOn Gaussiana
      coord.CubPts  (:,:,jj) = para.cont(m,1).piece{p}.subdibData.trianglesCubaturePoints  (:,:,:);
      %       coord.CubPtsex(:,:,jj) = para.cont(m,1).piece{p}.subdibData.trianglesCubaturePointsex(:,:,:);
      % de la inclusiOn:
      mext = para.cont(m,1).piece{p}.continuosTo;
      coord.mext(jj) = mext; %medio exterior
      mint = m;
      coord.mint(jj) = mint;%medio interior
      %       mext	= para.cont1(m-1).vec.mv; coord.mext(jj) = mext; %medio exterior
      %       mint	= para.cont1(m-1).m;      coord.mint(jj) = mint;%medio interior
      c     = coord.cv(jj);     %1 contorno de arriba; 2 contorno de abajo
      
      %indexacion (ver si hay un punto o dos), medios en contacto, y normal
      if mext==0 || para.reg(mext).rho==0 %2015-01-16
        %superficie libre y hueco afuera
        coord.Xim(jj,mint)  = c;	%ademas de saber que el punto Xi pertenece al medio m, se conoce su posicion arriba/abajo en el contorno
        coord.phi(jj,mint)  = jp+(1:n); %posicion de los phi en el vector X del sistema: AX=B
        jp                  = jp+n;     % una columna de phi por cada medio,
        txtaux = 'sup. libre';
        % un renglOn por cada triAngulo
        % el valor es un Indice en el
        % sistema del ibem. Los puntos de
        % freontera tienen valores en sOlo
        % una columna (sOlo un medio),
        % mientras que los puntos de
        % continuidad aparecen en dos
        % columnas (dos medios).
      elseif mint==0 || para.reg(m).rho==0
        %para tomar en cuenta los huecos
        % continuidad de tracciones:
        coord.Xim(jj,mext)  = (c==2)+2*(c==1);%inversion de la posicion
        coord.phi(jj,mext)  = jp+(1:n);
        jp                  = jp+n;
      else %continuidad
        % de tracciones:
        coord.Xim(jj,mext)  = (c==2)+2*(c==1);%inversion de la posicion
        coord.phi(jj,mext)  = jp+(1:n);
        jp                  = jp+n;
        if kind == 2 % si es una frontera de continuidad
          % de desplazamientos:
          coord.Xim(jj,mint)	= c;
          coord.phi(jj,mint) 	= jp+(1:n);
          jp                  = jp+n;
          txtaux = 'continuidad';
        else
          mext=0;
          para.cont1(m-1).vec.mv = 0;
          coord.mext(jj) = 0;
        end
      end
      
      disp(['m',num2str(m),' p',num2str(p),'] ',num2str(length(jj)),' ',txtaux])
      
      if mint~=0
        %         medio(mint).ind	= [medio(mint).ind;[jj(1) jj(end)]];%principio y fin
        if para.reg(mint).bet==0 && para.reg(mint).rho~=0
          %indicacion de presencia de fluido
          coord.fl(jj,mint)	= 1;
        end
      end
      if mext~=0
        %         medio(mext).ind	= [medio(mext).ind;[jj(1) jj(end)]];%principio y fin
        if  para.reg(mext).bet==0 && para.reg(mext).rho~=0
          %indicacion de presencia de fluido
          coord.fl(jj,mext)    = 1;
        end
      end
      
      %se censa los puntos que pertenecen al medio m o mas bien al sub medio mi
      for mi=1:size(coord.Xim,2)
        coord.indm(mi).ind=(coord.Xim(:,mi)~=0);
      end
      
      % en Axisimetrico:
      % ".drxy" longitud del arco que describe la geometrIa axisimEtrica
      % ".drxz" distancia entre centros de segmentos discretiaciOn 2D
      % ".dA"   Area del segmento 'escudo'.
      
      % en geometrIa general:
      %         Se da el Area del triAngulo
      coord.dA(jj) = para.cont(m,1).piece{p}.subdibData.areas(:);
      
      %         Radio de un cIrculo de la misma Area que el triAngulo:
      coord.drxz(jj) = sqrt(coord.dA(jj)/pi);
      
      
      %       disp(['m' num2str(m) ' p' num2str(p) ': n' num2str(n) ...
      %             '[' num2str(jjI) ';' num2str(jjF) ']'])
    end
  end
end

%se censa los puntos que no tienen frontera libre de manera a aplicar la
%continuidad de los desplazamientos
coord.ju        = cumsum(sum(coord.Xim~=0,2)==2);
coord.nbeq      = coord.ju(coord.nbpt)+coord.nbpt;
coord.nfl       = sum(sum(coord.fl)); % frontera con fluido

para.coord = coord;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Graficar discretizaciOn %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if para.GraficarCadaDiscretizacion %|| para.updatebecauseitwasjustloadedfromalist
  figure(19283734);cla
  dibujo_conf_geo(para,gca)
  hold on
  if para.dim == 3
    % Son escudos pero graficamos discos
    for i_elem = 1:coord.nbpt
      plotCircle3D([coord.x(i_elem) coord.y(i_elem) coord.z(i_elem)],...
        [coord.vnx(i_elem) coord.vny(i_elem) coord.vnz(i_elem)],...
        sqrt(coord.dA(i_elem)/pi),2);
    end
  elseif para.dim == 4
    %% Graficar la discretizaciOn actual
    col = {'b','r','g','y','m','c','none','k'};
    for m=1:para.nmed
      for p = 1:para.cont(m,1).NumPieces % para cada pieza del contorno
        kind = para.cont(m,1).piece{p}.kind;
        if kind ~= 3 % si no es una frontera auxiliar
          if size(para.cont(m,1).piece{p}.subdibData,2)>0 % y hay datos
            % pintar todos los triAngulos subdivididos
            sdD = para.cont(m,1).piece{p}.subdibData;
            icol = para.cont(m,1).piece{p}.ColorIndex;
            hg = hggroup;
            for t = 1:size(sdD.triangles,3)
              plot3(sdD.triangles(1,[1 2 3 1],t),...
                sdD.triangles(2,[1 2 3 1],t),...
                sdD.triangles(3,[1 2 3 1],t),[col{icol} '-'],...
                'LineWidth',1.0,'parent',hg)
            end
            
            % normales
%             quiver3(sdD.centers(1,:),sdD.centers(2,:),sdD.centers(3,:),...
%               (sdD.N(:,1)).',(sdD.N(:,2)).',(sdD.N(:,3)).',...
%               1,'k-');
            
            % cv
            ths = sdD.cv == 1;
            h = plot3(sdD.centers(1,ths),sdD.centers(2,ths),sdD.centers(3,ths),...
              'r+');
            set(h,'DisplayName','cv=1')
            ths = sdD.cv == 2;
            h= plot3(sdD.centers(1,ths),sdD.centers(2,ths),sdD.centers(3,ths),...
              'bx');
            set(h,'DisplayName','cv=2')
          end
        end
      end
    end
    
  end
  drawnow
end
%   % Para entender el multiibem
%   figure(11122345);clf;cla
%   dibujo_conf_geo(para,gca)
%   ts = coord.cv ==1;
%   plot3(coord.x(ts),coord.y(ts),coord.z(ts),'r+');
%   ts = coord.cv ==2;
%   plot3(coord.x(ts),coord.y(ts),coord.z(ts),'bx');
%   
%   ylim([0 0.5])
%   quiver3(coord.x,coord.y,coord.z,coord.vnx,coord.vny,coord.vnz,0.5,'k');
%   for iii = 1:length(coord.x)
%     if coord.y(iii) < 0 || coord.y(iii)>0.5
%       continue
%     end
%     text(coord.x(iii),coord.y(iii),coord.z(iii),cellstr([num2str(coord.Xim(iii,1)),',',num2str(coord.Xim(iii,2))]));
%   end
%   hold off
%   
%   set(gca,'Zdir','reverse'); 
%   grid on; set(gca,'Projection','perspective')
%   axis equal; ylim([0 0.5])

end

function out = apilar(a,b)
if isempty(a); out=b; return;end
out = a;
ni = size(a.centers,2)+1;
nf = ni-1 + size(b.centers,2);
l = ni:nf;
if ~isempty(l)
  out.triangles(:,:,l) = b.triangles;
  out.centers(:,l) = b.centers;
  out.areas(l) = b.areas;
  out.N(l,:) = b.N;
  out.cv(l) = b.cv;
  out.radios(l) = b.radios;
  out.radioObjetivo(l) = b.radioObjetivo;
end
end

function out = borrar(l,a)
out = a;
if ~isempty(l)
  out.triangles(:,:,l) = [];
  out.centers(:,l) = [];
  out.areas(l) = [];
  out.N(l,:) = [];
  out.cv(l) = [];
  out.radios(l) = [];
  out.radioObjetivo(l) = [];
end
end

function out = lado(a,b,l1,l2)
out = zeros(1,6);
out(1) = ( (a(1)-b(1))^2 + ...
           (a(2)-b(2))^2 + ...
           (a(3)-b(3))^2 ); %tamaNo de la cara al cuadrado
 % punto medio:
 out(2) = b(1) + (a(1)-b(1))/2;
 out(3) = b(2) + (a(2)-b(2))/2;
 out(4) = b(3) + (a(3)-b(3))/2;
 out(5) = l1;
 out(6) = l2;
end