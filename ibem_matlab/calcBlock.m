function [Ab] = calcBlock(tipo,...
  nrens,pcX,pcVn,pcXim,m,im0,...
  ncols,pTXi,pTdA,pTcubPts,...
  isSelfReciproc,isrelatedTo,...
  Ci,ksi,kpi,CubWts)
% calcBlock llena un bloque de la matriz de condiciones de frontera cuando
%           el material es homogEneo. Se aprovecha la reciprocidad de las
%           funciones de Green cuando existe. 
%           IntegraciOn gaussiana en triAngulos.

% fracCercano = 1/12;% Bola donde hacer cuadratura
% longonda = (2*pi)/real(ksi);
% BEALF = real(kpi)/real(ksi);

ngau = length(CubWts);
Ab  = zeros(3,3,nrens,ncols);
if isSelfReciproc || isrelatedTo > 0
    doRe = true;
else
    doRe = false;
end
if doRe;  AbR = zeros(3,3,ncols,nrens); end

jren = 1:nrens;

% PARA CADA FUENTE, ic, EN LOS RECEPTORES rens
for ic = 1:ncols 
  % 1) indices naturales completos
  if isSelfReciproc
    rens = ic:nrens;   % La mitad debajo de la diagonal y la diagonal
  else
    rens = 1:nrens;    % Se calcula toda la matriz porque no hay recIprocos
  end
  % 2) rij y cosenos directores
  pcXaux = pcX;
  for idi=1:3
    pcXaux(idi,:)=pcXaux(idi,:)-pTXi(idi,ic); % : x(todos) - xi(ic)
  end
  rij = (sum(pcXaux .* pcXaux)).^0.5;
  g = zeros(3,nrens);
  for idi=1:3
    g(idi,:) = pcXaux(idi,:)./rij;
  end
  
%   %% testme
%   figure;hold on;quiver3(zeros(1,nrens),zeros(1,nrens),zeros(1,nrens),...
%     rij.*g(1,:),rij.*g(2,:),rij.*g(3,:))
%   set(gca,'Zdir','reverse'); axis equal

  % 3)  Los Indices lOgicos: 
  %  Puntos singulares:
  xeqXi = jren(rij==0); % x=xi;
  
  %  Campo lejano:
  lrens = false(nrens,1); lrens(rens) = true;
%   lrens(rij<=fracCercano*longonda) = false; %<--- cursed line, beware.
  lrens(xeqXi) = false;

  % 4) Los Indices lOgicos: campo cercano y singularidades
  lrensGau=false(nrens,1);
  if isSelfReciproc
    lrensGau(ic:end) = ~lrens(ic:end); % cercano (sOlo lo que falta)
  else
    lrensGau = ~lrens; % campo cercano
  end
%   lrensGau(xeqXi)=false;
  
  % 5) Calcular elementos de la matrIz
  if tipo == -1 % Tracciones ==============================================
    lrensGau(xeqXi)=false;
    % 5.1) Tracciones campo lejano
      if doRe
        [Ab(1:3,1:3,lrens,ic),AbR(1:3,1:3,ic,lrens)] = ...
          Tij_3D(ksi,kpi,rij(lrens),g(:,lrens),0,pcVn(:,lrens),pcVn(:,ic));
      else
        Ab(1:3,1:3,lrens,ic) = ...
          Tij_3D(ksi,kpi,rij(lrens),g(:,lrens),0,pcVn(:,lrens));
      end
    
    %) 5.2 Tracciones campo cercano
    %  Para el campo cercano se considera la fuerza distribuida sobre
    %  el elemento triangular.
    if (sum(lrensGau) > 0) % Tracciones de campo cercano
      for niG = jren(lrensGau)
        Ab(1:3,1:3,niG,ic) = Tij_3D_r_smallGENf(...
          pcX(1:3,niG),pcVn(1:3,niG),...
          pTcubPts(1:3,1:ngau,ic),CubWts(1:ngau),...
          ksi,kpi,0);
      end
      if doRe
        for niG = jren(lrensGau)% Tracciones de campo cercano
          if niG == ic
            continue
          end
          AbR(1:3,1:3,ic,niG) = Tij_3D_r_smallGENf(...
            pcX(1:3,ic),pcVn(1:3,ic),...
            pTcubPts(1:3,1:ngau,niG),CubWts(1:ngau),...
            ksi,kpi,0);
        end
      end
    end
        
    % 5.3) multiplicar por el Area de la fuente:
    Ab(1:3,1:3,:,ic) = Ab(1:3,1:3,:,ic) .* pTdA(ic);
    if doRe
      for idi=1:3
        for idi2=1:3
          AbR(idi,idi2,ic,:) = squeeze(AbR(idi,idi2,ic,:)) .* pTdA(:);
        end
      end
    end
    
    % 5.4) Caso cuando rij=0 es +-1/2 para traccion
    %     lrens = false(nrens,1); lrens(rij==0) = true; jrens=jren(lrens);
    for jrensXeqXi =xeqXi
      % en este punto, hay discontinuidad de las tractiones ti-ti'=phi
      % el signo1 es igual a +1 (resp. -1) si la normal apunta hacia el exterior
      % (resp. a interior) del medio a que pertenece el phi del punto de
      % colocacion en la posicion singular rij=0
      % como la convencion es que las normales esten siempre dirigidas hacia
      % abajo, hay que conocer si el phi pertenece a un contorno arriba o
      % abajo. Esta informacion esta en Xim
%       signo1= (coord.Xim(i,m)==1) - (coord.Xim(i,m)==2);
      signo1= (pcXim(m,jrensXeqXi)==1) - (pcXim(m,jrensXeqXi)==2);
      Ab(:,:,jrensXeqXi,ic) = signo1*0.5*eye(3);
    end
  end % tracciones
  
  if tipo ==  1 % Desplazamientos =========================================
    % 5.4) Desplazamientos de campo lejano:
      Ab(1:3,1:3,lrens,ic) = ...
        Gij_3D(ksi,kpi,rij(lrens),g(:,lrens),Ci,sum(lrens));
    if doRe %RecIprocos de campo lejano
      AbR(1,1,ic,lrens) = squeeze(Ab(1,1,lrens,ic));
      AbR(2,1,ic,lrens) = squeeze(Ab(1,2,lrens,ic));
      AbR(3,1,ic,lrens) = squeeze(Ab(1,3,lrens,ic));
      
      AbR(1,2,ic,lrens) = squeeze(Ab(2,1,lrens,ic));
      AbR(2,2,ic,lrens) = squeeze(Ab(2,2,lrens,ic));
      AbR(3,2,ic,lrens) = squeeze(Ab(2,3,lrens,ic));
      
      AbR(1,3,ic,lrens) = squeeze(Ab(3,1,lrens,ic));
      AbR(2,3,ic,lrens) = squeeze(Ab(3,2,lrens,ic));
      AbR(3,3,ic,lrens) = squeeze(Ab(3,3,lrens,ic));
    end
    
    % 5.5) Desplazamientos de campo cercano:
    if (sum(lrensGau) > 0)
      for niG = jren(lrensGau)
        Ab(1:3,1:3,niG,ic) = Gij_3D_r_smallGENf(...
          pcX(1:3,niG),...
          pTcubPts(1:3,1:ngau,ic),CubWts(1:ngau),...
          ksi,kpi,Ci);
      end
      if doRe
        for niG = jren(lrensGau)
          if niG == ic
            continue
          end
          AbR(1:3,1:3,ic,niG) = Gij_3D_r_smallGENf(...
            pcX(1:3,ic),...
            pTcubPts(1:3,1:ngau,niG),CubWts(1:ngau),...
            ksi,kpi,Ci);
        end
      end
    end
    
%     for jrensXeqXi =xeqXi % rij=0
%       Ab(:,:,jrensXeqXi,ic) = ...
%         Greenex_3D(ksi,sqrt(pTdA(jrensXeqXi)/pi),BEALF,pcVn(1:3,ic),Ci(6,6),0,0);
%     end
    
    % 5.6) Multiplicar por el Area de la fuente:
    Ab(1:3,1:3,:,ic) = Ab(1:3,1:3,:,ic) .* pTdA(ic);
    if doRe
      for idi=1:3
        for idi2=1:3
          AbR(idi,idi2,ic,:) = squeeze(AbR(idi,idi2,ic,:)) .* pTdA(:);
        end
      end
    end
  end % desplazamiento
end % ic = 1:ncols

% 6) Si el bloque es autorecIproco juntar Ab y AbR
if doRe
  Ab = Ab + AbR;
%   AbR = 0;
end

% signo de acuerdo al Indice del medio
% en un punto de colocacion, hay siempre 2 medios en contacto,
% aunque a veces este medio es el vacio
% las ecuaciones de continuidad de traccion o de desplazamiento
% involucran siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
% y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
% se reorganiza entonces como :
% u_diff(m)-u_diff(m1)=u_inc(m1)-u_inc(m)
% los signos se fijan deacuerdo con el indice del medio,
% + si el indice es el mas pequeno de los medios en contacto
% - en lo contrario
for iii = 1:nrens
  Ab(:,:,iii,:) = Ab(:,:,iii,:) * ((m==im0(iii)) - (m~=im0(iii)));
end

% return
% %% testme
% figure;surf(real(squeeze(Ab(1,1,:,:))),'EdgeColor','none');title('1 1');view(2);set(gca,'Ydir','reverse');%zlim([0.00001 0.5])
% figure;surf(real(squeeze(Ab(1,2,:,:))),'EdgeColor','none');title('1 2');view(2);set(gca,'Ydir','reverse');%zlim([0.00001 0.5])
% figure;surf(real(squeeze(Ab(1,3,:,:))),'EdgeColor','none');title('1 3');view(2);set(gca,'Ydir','reverse');%zlim([0.00001 0.5])
end