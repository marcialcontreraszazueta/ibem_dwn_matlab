function graficarunafrecuencia(para,uwi,filmStyle)
%% graficar una frecuencia
% lee para y uw
figure;
ifrec = 1;
iinc = 1;
% sca = 0.1;
% m3max = max(max(max(max(uwi(ifrec,:,iinc,:)))))/sca;
u =uwi(ifrec,:,iinc,1);%/m3max;
v =uwi(ifrec,:,iinc,2);%/m3max;
w =uwi(ifrec,:,iinc,3);%/m3max;

nrecx=para.rec.nrecx; % inicializar variables
nrecy=para.rec.nrecy;
nrecz=para.rec.nrecz;
if filmStyle ~= 5
  xr      = para.rec.xri+para.rec.dxr*(0:(nrecx-1));
  yr      = para.rec.yri+para.rec.dyr*(0:(nrecy-1));
  zr      = para.rec.zri+para.rec.dzr*(0:(nrecz-1));
  [MX,MY,MZ] = meshgrid(xr,yr,zr);
  MXi=MX;MYi=MY;MZi=MZ;C=MX.*0;
  if (abs(filmStyle) ~= 4); MXi=MX;MYi=MY;MZi=MZ;C=MX.*0;end
  if (abs(filmStyle) == 4); [~,nes]=size(u);x=zeros(1,nes);y=x;z=x;mag=x;n=zeros(3,nes);end
else
  xr = para.rec.xr;
  yr = para.rec.yr;
  zr = para.rec.zr;
end
r = (u.^2+v.^2+w.^2).^0.5;
maxr = max(max(r));
mx = 1.0*mean(maxr);

it = 1;
for iz=1:nrecz
  for iy=1:nrecy
    for ix=1:nrecx
      ies = ix+(iy-1)*nrecx+(iz-1)*nrecx*nrecy;
      if abs(filmStyle) == 1
        C(iy,ix,iz)  = -r(it,ies);
      elseif (abs(filmStyle) == 2 || abs(filmStyle) == 3)
        % mesh
%         if (max(abs(sqrt(u(it,ies)^2+v(it,ies)^2+w(it,ies)^2)))==0)
%           MZi(iy,ix,iz)= nan; %No traza el grid
% %           C(iy,ix,iz)  = nan;
%         else
          MXi(iy,ix,iz)=MXi(iy,ix,iz)+u(it,ies);
          MYi(iy,ix,iz)=MYi(iy,ix,iz)+v(it,ies);
          MZi(iy,ix,iz)=MZi(iy,ix,iz)+w(it,ies);
          C(iy,ix,iz)  = -r(it,ies);
          %                 MZi(ix,iz)=MZi(ix,iz)-r(it,ies);
%         end
      elseif abs(filmStyle) == 4
        % quiver
        x(1,ies) = xr(ix);
        y(1,ies) = yr(iy);
        z(1,ies) = zr(iz);
        n(1,ies) = u(it,ies);
        n(2,ies) = v(it,ies);
        n(3,ies) = w(it,ies);
        mag(ies) = comprimir(magnitud(n(1:3,ies))*4,mx);
        %               mag(ies) = magnitud(n(1:3,ies));
      elseif abs(filmStyle) == 5
        % quiver
        n(1,ies) = u(it,ies);
        n(2,ies) = v(it,ies);
        n(3,ies) = w(it,ies);
      end
    end
  end
end     

set(gcf,'Color',[1 1 1]);
% set(fig,'WindowStyle','normal');
% set(fig,'Position', [0, 0, 800, 800]);
set(gcf,'DoubleBuffer','on'); set(gcf,'Renderer','zbuffer')
% cmd_iflist_loadthelast; %Cargar la ultima subdivicion de elementos
% paratmp = cargardelista(para,J);
if (filmStyle > 0)
dibujo_conf_geo(para,gca);
xl = xlim; yl = ylim; zl = zlim;
end
hold on

colormap(gray);
cm = colormap.^0.7;
%   cm = colormap;
colormap(cm);
caxis([-real(mx) real(mx)])

if abs(filmStyle) == 1
  tmph = surf(squeeze(MX),squeeze(MY),squeeze(MZ),squeeze(C),'FaceColor','interp',...
    'FaceLighting','phong','AmbientStrength',.9,'DiffuseStrength',.8,...
    'SpecularStrength',.9,'SpecularExponent',25,...
    'BackFaceLighting','unlit');%,...
  %           'EdgeColor','none','LineStyle','none');
  %         caxis([-0.1*maxr 0.005*maxr]);
  alpha(tmph,0.6);
  set(tmph,'FaceAlpha',0.4);
elseif abs(filmStyle) == 2
  tmph = mesh(squeeze(MXi),squeeze(MYi),squeeze(MZi),squeeze(C));
  %         caxis([-40 1]); %todo blanco
  set(tmph,'FaceAlpha',0)
elseif abs(filmStyle) == 3
  tmph = surf(squeeze(MXi),squeeze(MYi),squeeze(MZi),squeeze(C),'FaceColor','interp',...
    'FaceLighting','phong','AmbientStrength',.9,'DiffuseStrength',.8,...
    'SpecularStrength',.9,'SpecularExponent',25,...
    'BackFaceLighting','unlit');%,...
  %           'EdgeColor','none','LineStyle','none');
  %         caxis([-0.1*maxr 0.005*maxr]);
  alpha(tmph,0.6);
  set(tmph,'FaceAlpha',0.4);
  zl(1) = min(min(min(MZi)),min(zlim)); zl(2) = max(max(max(MZi)),max(zlim));
  zlim(zl);
elseif abs(filmStyle) == 4
    iv = mag>0.6*mean(mag,'omitnan');
    quiver3(x(iv),y(iv),z(iv),n(1,iv),n(2,iv),n(3,iv),1.0*max(mag),'k');
elseif abs(filmStyle) == 5
    quiver3(xr,yr,zr,real(n(1,:).'),real(n(2,:).'),real(n(3,:).'),'r',...
      'ShowArrowHead','off','LineWidth',1.5);
    quiver3(xr,yr,zr,imag(n(1,:).'),imag(n(2,:).'),imag(n(3,:).'),'b',...
      'ShowArrowHead','off','LineWidth',1.5);
end





if (filmStyle < 0)
  xl = [min(min(MXi)) max(max(MXi))]; 
  yl = [min(min(MYi)) max(max(MYi))];
  zl = [min(min(MZi)) max(max(MZi))];
% if sum(xr) == 0; xl = [-0.01 0.01]; end
% if sum(yr) == 0; yl = [-0.01 0.01]; end
% if sum(zr) == 0; zl = [-0.01 0.01]; end
end
xl(1) = min(xl(1),min(xr)); xl(2) = max(xl(2),max(xr));
yl(1) = min(yl(1),min(yr)); yl(2) = max(yl(2),max(yr));
zl(1) = min(zl(1),min(zr)); zl(2) = max(zl(2),max(zr));
xlim('manual');ylim('manual');zlim('manual')
xlim(xl); ylim(yl); zlim(zl); %garantizar que se vean todos los receptores


end

function [s] = comprimir(s,mx)
p = 60; %8
s =  log((1. + exp(p)*abs(s))) / (log(exp(p)+1.));
%disp(s)
s = s / mx;
end

function [r] = magnitud(s)
r = sqrt(sum((s(1:3)).^2));
end