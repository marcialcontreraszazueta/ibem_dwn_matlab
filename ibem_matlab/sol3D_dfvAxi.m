function [phi_fv,DWN,para]=sol3D_dfvAxi(para,fj,DWN)
% calcula la densidad de fuerzas virtuales (dfv)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% discretizacion de los contornos %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
coord   = malla_geom(para,fj);
coord   = malla_geom_axi(coord,para);
%el dr varia en funcion de la velocidad y el dA en funcion de v^2
nbeq    = coord.nbeq;
nbpt    = coord.nbpt;
ju      = coord.ju;
nbeq2   = 3*nbeq;       %hay ecuaciones para phix, phiy y phiz
disp(['ifreq = ',num2str(para.j),', system size: ',num2str(nbeq2),'^2'])

if para.geo(1)==3
    %%%%%%%%%%%%
    % IBEM-DWN %
    %%%%%%%%%%%%
    % en esta parte, se calcula la matriz del DWN, una primera vez con paso
    % constante, otra con paso que permite mejor discretizar los polos.
    % Csc: acceleracion de los calculos del DWN para rellenar la matriz del
    % IBEM.
    % Y tambien se busca los puntos en los cuales hay que calcular G^DWN,
    % tratando conjuntamente todos los receptores que tienen una misma z,
    
    %se propone a la primera iteracion un vector de la componente
    %horizontal de kr con paso constante lo que corresponde a
    %xmax = para.DX*para.DWNnbptkx/2;
    %los puntos en polar estan en el centro del segmento de longitud DK
    nk          = para.DWNnbptkx;
    DK      	= para.DWNkmax(1)/nk;%kmax posicion central del ultimo
    %xmax doit etre > vmax*tmax=alpha/dfe
    kr          = (0.5+(0:nk))*DK;
    DWN.kr      = kr;
    DWN.dkr     = kr*0+DK;
    DWN.k2      = kr;%copia para rebuildk2, sino inutil
    DWN.omegac  = 2*pi*fj;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % calculo DWN - parte 1 %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    
    % calculo de la matriz del DWN
    DWN         = calcul_A_DWN_3D_polar_Ncapas_HS(para,DWN);
%     DWN         = rebuildk2(DWN);
%     DWN.kr      = DWN.k2;
%     DWN.dkr     = DWN.dk2;
%     DWN.dkr(1)  = DWN.kr(2)-DWN.dkr(2)/2;
%     DWN         = calcul_A_DWN_3D_polar_Ncapas_HS(para,DWN);
    % inversion de las matrices
    for i=1:length(DWN.kr)
        DWN.A_DWN(:,:,i)=inv(DWN.A_DWN(:,:,i));
        DWN.B_DWN(:,:,i)=inv(DWN.B_DWN(:,:,i));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % identificacion de los puntos en los cuales se tiene que calcular DWN -parte 2 %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% para el calculo de la matriz del IBEM:
    %   posicion puntos de colocacion: receptores y fuentes (virtuales)
    
    indpt               = 1:coord.nbpt;         %indice de todos los puntos
    indi                = coord.indm(1,1).ind;  %logical de los puntos perteneciendo a m=1
    indir               = indpt(indi);          %indice de estos puntos
    
    xrv                 = coord.x(indi);
    yrv                 = coord.y(indi);
    zrv                 = coord.z(indi);
    nxrv                = length(xrv);
    vn(1,1:nxrv)        = coord.vnx(indi);
    vn(2,1:nxrv)        = coord.vny(indi);
    vn(3,1:nxrv)        = coord.vnz(indi);
    salu                = ones(nxrv,1);
    sals                = ones(nxrv,1);
    
    %reduccion du numero de profundidades de las funtes virtuales, moviendo
    %los puntos en z de manera a agrupar puntos cercanos (<lambda/10)
    [zrfv,izrfv,zrref]	= pseudo_unique(zrv,para);
    % zrfv=zrv.';zrref=zrv;izrfv=(1:length(zrv)).';
    coord.z(indi)       = zrref; %actualizacion de la posicion modificada de los puntos de colocacion
    
    %%% se incluye de una ves los receptores reales para el calculo de los
    %   campos difractados.
    %   El campo difractado es indepediente de las
    %   amplitudes de las fuerzas en cada punto de colocacion.
    %   Eso se toma en cuenta  hasta tomar en cuenta los phi cf inversion_3D_k
    xrr                 = para.rec.xr(para.rec.m(1).ind).';
    yrr                 = para.rec.yr(para.rec.m(1).ind).';
    zrr                 = para.rec.zr(para.rec.m(1).ind).';
    nrr                 = para.rec.m(1).nr;
    
    [zrfr,izr0,~]       = pseudo_unique(zrr,para);
    %         zrr=zrr.';izr0=(1:length(zrr)).';
    
    % inicialisacion del vector de los diffractados en los receptores
    % reales y salidas
    sal                 = para.sortie;
    ns                  =(sal.Ux + sal.Uy + sal.Uz)*sal.Ut;
    nss                 = sal.sxx + sal.syy + sal.szz + sal.sxy + sal.sxz + sal.syz;
    DWN.Udiff           = zeros(ns ,nbeq2,para.rec.m(1).nr);
    DWN.Sdiff           = zeros(nss,nbeq2,para.rec.m(1).nr);
    salur               = logical(ones(nrr,1)*sal.Ut);
    salsr               = logical(ones(nrr,1)*(nss>0));
    
    %concatenacion de los puntos virtuales y reales y concatenacion de las salidas
    xr                  = [xrv,xrr];
    yr                  = [yrv,yrr];
    zr                  = [zrv,zrr];
    zr0                 = [zrfv;zrfr];
    izr0                = [izrfv;izr0+length(zrfv)];
    salu                = [salu;salur];
    sals                = [sals;salsr];
    
    nzr0                = length(zr0);
    zricrall            = zeros(1,nzr0);
    icrall              = zeros(1,nzr0);
    for ir=1:nzr0
        icrv     = 1;
        zricrv   = zr0(ir); % profundidad relativa a la interface de la capa del receptor
        while zricrv>para.reg(1).sub(icrv).h && icrv<para.nsubmed
            zricrv   = zricrv-para.reg(1).sub(icrv).h;
            icrv     = icrv+1;
        end
        zricrall(ir) = zricrv;
        icrall(ir)   = icrv;
    end
    
    %reduccion du numero de profundidades de los receptores
    DWN.xr              = xr;
    DWN.yr              = yr;
    DWN.zr              = zr;
    DWN.zr0             = zr0;
    DWN.izr0            = izr0;
    DWN.zricrall     	= zricrall;
    DWN.icrall        	= icrall;
    DWN.salu            = salu;
    DWN.sals            = sals;
    DWN.nxrv            = nxrv;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculo de los terminos independientes %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% termino fuente conocido %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% esfuerzos nulos en la superficie libre
% continuidad de los esfuerzos y desplazamientos normales
% en los contornos fuera de la superficie libre

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vectores de condiciones  a las fronteras %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
para.coord = coord;
DWNtmp{1}=DWN;
[B,DWNtmp]	= vector_fuente3D(para,DWNtmp);
DWN = DWNtmp{1};
%%%%%%%%%%%%%%%%%%%%%%%%%%
% matriz de coeficientes %
%%%%%%%%%%%%%%%%%%%%%%%%%%

A   = zeros(nbeq2,nbeq2);
%en el caso del IBEM-DWN, se rellena solo una parte de la matrice, la que
%corresponde al interior de los contornos diferente del multi-estratos
for i=1:coord.nbpt
    %si Xi pertenece a un solo medio, entonces solo se considera la
    %ecuacion de esfuerzos en superficie libre
    %sino se considera la continuidad de los esfuerzos normales y de los
    %desplazamientos
    [Aijx,Aijy,Aijz]=Aij_Tn_3D(i,coord,para);
    %continuidad tx
    A(i,:)        = Aijx;
    %continuidad ty
    A(i+nbeq,:)   = Aijy;
    %continuidad tz
    A(i+2*nbeq,:) = Aijz;
    if sum(coord.Xim(i,:)~=0)==2
        [Aijx,Aijy,Aijz]        = Aij_Gn_3D(i,coord,para);
        %continuidad ux
        A(ju(i)+nbpt,:)         = Aijx;
        %continuidad uy
        A(ju(i)+nbpt+nbeq,:)	= Aijy;
        %continuidad uz
        A(ju(i)+nbpt+2*nbeq,:)  = Aijz;
    end
end

if para.geo(1)==3
    %en el caso del IBEM-DWN, es mas ventajoso, rellenar la matrice
    %considerando los phi, es a decir, columna por columna
    %tambien por cada punto de colocacion perteneciendo al multi-estratos
    %hay que considerar una continuidad de traciones y desplazamientos
    
    % cada fuente esta considerada por separada, la integracion se hace
    % bien, se tendria mejorar por fuentes de misma orientation, mismo z,
    % y misma longitud
    
    %hay que recordar que el medio 1 siempre es el medio de mas bajo
    %indice asi que el signo de la matriz ((m==im0) - (m~=im0))*t22(jj)
    %es obvio
%     for j=indir
%         %phi = [1,0,0]; [0,1,0]; [0,0,1]
%         
%         coordf.xs   = coord.x(j);
%         coordf.ys   = coord.y(j);
%         coordf.zs   = zrref(j==indir);
%         coordf.vnx  = coord.vnx(j);
%         coordf.vny  = coord.vny(j);
%         coordf.vnz  = coord.vnz(j);
%         coordf.dr   = coord.drxz(j);
%         coordf.x0   = coord.x0(j);
%         coordf.th   = coord.th(j);
%         coordf.drxz = coord.drxz(j);
%         coordf.nbptc= coord.nbptc(j);
%         ind_superpos= logical((xrv==coordf.xs).*(yrv==coordf.ys).*(zrref==coordf.zs));
%         signo1      = (coord.Xim(indir(ind_superpos),1)==1) - (coord.Xim(indir(ind_superpos),1)==2);
%         
%         rec.xr          = DWN.xr;
%         rec.yr          = DWN.yr;
%         rec.zr          = DWN.zr;
%         rec.zr0         = DWN.zr0;
%         rec.izr0        = DWN.izr0;
%         rec.zricrall    = DWN.zricrall;
%         rec.icrall      = DWN.icrall;
%         nrec            = length(DWN.xr);
%         
%         [U_f1,S_f1,U_f2,S_f2,U_f3,S_f3] = calcul_US_DWN_3D_polar_Ncapas_HS2(para,rec,salu,sals,coordf,[1 1 1],DWN);
%         % U_f1      = zeros(3,nrec,1);
%         % U_f2      = zeros(3,nrec,1);
%         % U_f3      = zeros(3,nrec,1);
%         %
%         % S_f1    	= zeros(3,3,nrec,1);
%         % S_f2    	= zeros(3,3,nrec,1);
%         % S_f3    	= zeros(3,3,nrec,1);
%         
%         DWN.Udiff(:,coord.phi(j,1),:)         = U_f1(:,nxrv+1:nxrv+nrr);%(3  ,nbeq,para.rec.m(1).nr)
%         if sals(nxrv+1)
%             S0  = squeeze(reshape(S_f1(:,:,nxrv+1:nrec),1,9,nrec-nxrv));
%             S0  = S0(DWN.inds,:);
%             DWN.Sdiff(:,coord.phi(j,1),:)= S0;%(nss,nbeq,para.rec.m(1).nr)
%         end
%         U_f1    = squeeze(U_f1(:,1:nxrv,1));
%         S_f1    = squeeze(S_f1(:,:,1:nxrv,1));
%         
%         DWN.Udiff(:,coord.phi(j,1)+nbeq,:)    = U_f2(:,nxrv+1:nxrv+nrr);
%         if sals(nxrv+1)
%             S0	= squeeze(reshape(S_f2(:,:,nxrv+1:nrec),1,9,nrec-nxrv));
%             S0 	= S0(DWN.inds,:);
%             DWN.Sdiff(:,coord.phi(j,1)+nbeq,:)= S0;%(nss,nbeq,para.rec.m(1).nr)
%         end
%         U_f2    = squeeze(U_f2(:,1:nxrv,1));
%         S_f2	= squeeze(S_f2(:,:,1:nxrv,1));
%         
%         DWN.Udiff(:,coord.phi(j,1)+2*nbeq,:)  = U_f3(:,nxrv+1:nxrv+nrr);
%         if sals(nxrv+1)
%             S0	= squeeze(reshape(S_f3(:,:,nxrv+1:nrec),1,9,nrec-nxrv));
%             S0 	= S0(DWN.inds,:);
%             DWN.Sdiff(:,coord.phi(j,1)+2*nbeq,:)= S0;%(nss,nbeq,para.rec.m(1).nr)
%         end
%         U_f3    = squeeze(U_f3(:,1:nxrv,1));
%         S_f3    = squeeze(S_f3(:,:,1:nxrv,1));
%         
%         %phix
%         tn1         = squeeze(S_f1(1,1,:)).'.*vn(1,:)+squeeze(S_f1(2,1,:)).'.*vn(2,:)+squeeze(S_f1(3,1,:)).'.*vn(3,:);
%         tn2         = squeeze(S_f1(1,2,:)).'.*vn(1,:)+squeeze(S_f1(2,2,:)).'.*vn(2,:)+squeeze(S_f1(3,2,:)).'.*vn(3,:);
%         tn3         = squeeze(S_f1(1,3,:)).'.*vn(1,:)+squeeze(S_f1(2,3,:)).'.*vn(2,:)+squeeze(S_f1(3,3,:)).'.*vn(3,:);
%         tn1(ind_superpos)=signo1*0.5/coord.dA(j);
%         tn2(ind_superpos)=0;
%         tn3(ind_superpos)=0;
%         %continuidad tx
%         A(indir                 ,coord.phi(j,1))= tn1*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq            ,coord.phi(j,1))= tn2*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq*2          ,coord.phi(j,1))= tn3*coord.dA(j);
%         %continuidad ux
%         A(ju(indir)+nbpt        ,coord.phi(j,1))= U_f1(1,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1))= U_f1(2,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1))= U_f1(3,:)*coord.dA(j);
%         
%         %phiy
%         tn1         = squeeze(S_f2(1,1,:)).'.*vn(1,:)+squeeze(S_f2(2,1,:)).'.*vn(2,:)+squeeze(S_f2(3,1,:)).'.*vn(3,:);
%         tn2         = squeeze(S_f2(1,2,:)).'.*vn(1,:)+squeeze(S_f2(2,2,:)).'.*vn(2,:)+squeeze(S_f2(3,2,:)).'.*vn(3,:);
%         tn3         = squeeze(S_f2(1,3,:)).'.*vn(1,:)+squeeze(S_f2(2,3,:)).'.*vn(2,:)+squeeze(S_f2(3,3,:)).'.*vn(3,:);
%         tn1(ind_superpos)=0;
%         tn2(ind_superpos)=signo1*0.5/coord.dA(j);
%         tn3(ind_superpos)=0;
%         %continuidad tx
%         A(indir                 ,coord.phi(j,1)+nbeq)= tn1*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq            ,coord.phi(j,1)+nbeq)= tn2*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq*2          ,coord.phi(j,1)+nbeq)= tn3*coord.dA(j);
%         %continuidad ux
%         A(ju(indir)+nbpt        ,coord.phi(j,1)+nbeq)= U_f2(1,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1)+nbeq)= U_f2(2,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1)+nbeq)= U_f2(3,:)*coord.dA(j);
%         
%         %phiz
%         tn1         = squeeze(S_f3(1,1,:)).'.*vn(1,:)+squeeze(S_f3(2,1,:)).'.*vn(2,:)+squeeze(S_f3(3,1,:)).'.*vn(3,:);
%         tn2         = squeeze(S_f3(1,2,:)).'.*vn(1,:)+squeeze(S_f3(2,2,:)).'.*vn(2,:)+squeeze(S_f3(3,2,:)).'.*vn(3,:);
%         tn3         = squeeze(S_f3(1,3,:)).'.*vn(1,:)+squeeze(S_f3(2,3,:)).'.*vn(2,:)+squeeze(S_f3(3,3,:)).'.*vn(3,:);
%         tn1(ind_superpos)=0;
%         tn2(ind_superpos)=0;
%         tn3(ind_superpos)=signo1*0.5/coord.dA(j);
%         %continuidad tx
%         A(indir                 ,coord.phi(j,1)+2*nbeq)= tn1*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq            ,coord.phi(j,1)+2*nbeq)= tn2*coord.dA(j);
%         %continuidad ty
%         A(indir+nbeq*2          ,coord.phi(j,1)+2*nbeq)= tn3*coord.dA(j);
%         %continuidad ux
%         A(ju(indir)+nbpt        ,coord.phi(j,1)+2*nbeq)= U_f3(1,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1)+2*nbeq)= U_f3(2,:)*coord.dA(j);
%         %continuidad uy
%         A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1)+2*nbeq)= U_f3(3,:)*coord.dA(j);
%     end

    for j1=1:length(zrfv)
        j=indir(izrfv==j1);
        nj=length(j);
        %phi = [1,0,0]; [0,1,0]; [0,0,1]
        
        coordf.xs       = coord.x(j);
        coordf.ys       = coord.y(j);
        coordf.zs       = coord.z(j);
        coordf.vnx      = coord.vnx(j);
        coordf.vny      = coord.vny(j);
        coordf.vnz      = coord.vnz(j);
        coordf.dr       = coord.drxz(j);
        coordf.x0       = coord.x0(j);
        coordf.th       = coord.th(j);
        coordf.drxz     = coord.drxz(j);
        coordf.nbptc    = coord.nbptc(j);
        
        rec.xr          = DWN.xr;
        rec.yr          = DWN.yr;
        rec.zr          = DWN.zr;
        rec.zr0         = DWN.zr0;
        rec.izr0        = DWN.izr0;
        rec.zricrall    = DWN.zricrall;
        rec.icrall      = DWN.icrall;
        nrec            = length(DWN.xr);

        ind_superpos    = false(1,nrec);
        ind_superpos(izrfv==j1)= true;
        signo1          = (coord.Xim(indir(ind_superpos),1)==1) - (coord.Xim(indir(ind_superpos),1)==2);
       
        [U_f1,S_f1,U_f2,S_f2,U_f3,S_f3] = calcul_US_DWN_3D_polar_Ncapas_HS2(para,rec,salu,sals,coordf,ones(nrec,3),1,DWN);
        
        U_f1 = permute(U_f1,[1 3 2]);
        S_f1 = permute(S_f1,[1 2 4 3]);
        U_f2 = permute(U_f2,[1 3 2]);
        S_f2 = permute(S_f2,[1 2 4 3]);
        U_f3 = permute(U_f3,[1 3 2]);
        S_f3 = permute(S_f3,[1 2 4 3]);
        
        DWN.Udiff(:,coord.phi(j,1),:)         = U_f1(:,:,nxrv+1:nxrv+nrr);%(3  ,nbeq,para.rec.m(1).nr)
        if length(sals)>nxrv && sals(nxrv+1)
            S0  = squeeze(reshape(S_f1(:,:,:,nxrv+1:nrec),1,9,nj,nrec-nxrv));
            S0  = S0(DWN.inds,:,:);
            DWN.Sdiff(:,coord.phi(j,1),:)= S0;%(nss,nbeq,para.rec.m(1).nr)
        end
        U_f1    = squeeze(U_f1(:,:,1:nxrv));
        S_f1    = squeeze(S_f1(:,:,:,1:nxrv));
        
        DWN.Udiff(:,coord.phi(j,1)+nbeq,:)    = U_f2(:,:,nxrv+1:nxrv+nrr);
        if length(sals)>nxrv && sals(nxrv+1)
            S0	= squeeze(reshape(S_f2(:,:,:,nxrv+1:nrec),1,9,nj,nrec-nxrv));
            S0 	= S0(DWN.inds,:,:);
            DWN.Sdiff(:,coord.phi(j,1)+nbeq,:)= S0;%(nss,nbeq,para.rec.m(1).nr)
        end
        U_f2    = squeeze(U_f2(:,:,1:nxrv));
        S_f2	= squeeze(S_f2(:,:,:,1:nxrv));
        
        DWN.Udiff(:,coord.phi(j,1)+2*nbeq,:)  = U_f3(:,:,nxrv+1:nxrv+nrr);
        if length(sals)>nxrv && sals(nxrv+1)
            S0	= squeeze(reshape(S_f3(:,:,:,nxrv+1:nrec),1,9,nj,nrec-nxrv));
            S0 	= S0(DWN.inds,:,:);
            DWN.Sdiff(:,coord.phi(j,1)+2*nbeq,:)= S0;%(nss,nbeq,para.rec.m(1).nr)
        end
        U_f3    = squeeze(U_f3(:,:,1:nxrv));
        S_f3    = squeeze(S_f3(:,:,:,1:nxrv));
        
        vn1=repmat(vn(1,:).',1,nj);
        vn2=repmat(vn(2,:).',1,nj);
        vn3=repmat(vn(3,:).',1,nj);
        dAj=repmat(coord.dA(j),nxrv,1);
        
        diagindsup      =zeros(nj,2);
        ind0            =1:nxrv;
        diagindsup(:,1) =ind0(ind_superpos(ind0));
        diagindsup(:,2) =1:nj;
        diagindsup=sub2ind([nxrv,nj], diagindsup(:,1), diagindsup(:,2));

        %phix
        tn1         = squeeze(S_f1(1,1,:,:)).'.*vn1+squeeze(S_f1(2,1,:,:)).'.*vn2+squeeze(S_f1(3,1,:,:)).'.*vn3;
        tn2         = squeeze(S_f1(1,2,:,:)).'.*vn1+squeeze(S_f1(2,2,:,:)).'.*vn2+squeeze(S_f1(3,2,:,:)).'.*vn3;
        tn3         = squeeze(S_f1(1,3,:,:)).'.*vn1+squeeze(S_f1(2,3,:,:)).'.*vn2+squeeze(S_f1(3,3,:,:)).'.*vn3;
        tn1(diagindsup)=signo1*0.5./coord.dA(j).';
        tn2(diagindsup)=0;
        tn3(diagindsup)=0;
        %continuidad tx
        A(indir                 ,coord.phi(j,1))= tn1.*dAj;
        %continuidad ty
        A(indir+nbeq            ,coord.phi(j,1))= tn2.*dAj;
        %continuidad ty
        A(indir+nbeq*2          ,coord.phi(j,1))= tn3.*dAj;
        if min(ju(indir))>0
            %continuidad ux
            A(ju(indir)+nbpt        ,coord.phi(j,1))= squeeze(U_f1(1,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1))= squeeze(U_f1(2,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1))= squeeze(U_f1(3,:,:)).'.*dAj;
        end
        
        %phiy
        tn1         = squeeze(S_f2(1,1,:,:)).'.*vn1+squeeze(S_f2(2,1,:,:)).'.*vn2+squeeze(S_f2(3,1,:,:)).'.*vn3;
        tn2         = squeeze(S_f2(1,2,:,:)).'.*vn1+squeeze(S_f2(2,2,:,:)).'.*vn2+squeeze(S_f2(3,2,:,:)).'.*vn3;
        tn3         = squeeze(S_f2(1,3,:,:)).'.*vn1+squeeze(S_f2(2,3,:,:)).'.*vn2+squeeze(S_f2(3,3,:,:)).'.*vn3;
        tn1(diagindsup)=0;
        tn2(diagindsup)=signo1*0.5./coord.dA(j).';
        tn3(diagindsup)=0;
        %continuidad tx
        A(indir                 ,coord.phi(j,1)+nbeq)= tn1.*dAj;
        %continuidad ty
        A(indir+nbeq            ,coord.phi(j,1)+nbeq)= tn2.*dAj;
        %continuidad ty
        A(indir+nbeq*2          ,coord.phi(j,1)+nbeq)= tn3.*dAj;
        if min(ju(indir))>0
            %continuidad ux
            A(ju(indir)+nbpt        ,coord.phi(j,1)+nbeq)= squeeze(U_f2(1,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1)+nbeq)= squeeze(U_f2(2,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1)+nbeq)= squeeze(U_f2(3,:,:)).'.*dAj;
        end
        
        %phiz
        tn1         = squeeze(S_f3(1,1,:,:)).'.*vn1+squeeze(S_f3(2,1,:,:)).'.*vn2+squeeze(S_f3(3,1,:,:)).'.*vn3;
        tn2         = squeeze(S_f3(1,2,:,:)).'.*vn1+squeeze(S_f3(2,2,:,:)).'.*vn2+squeeze(S_f3(3,2,:,:)).'.*vn3;
        tn3         = squeeze(S_f3(1,3,:,:)).'.*vn1+squeeze(S_f3(2,3,:,:)).'.*vn2+squeeze(S_f3(3,3,:,:)).'.*vn3;
        tn1(diagindsup)=0;
        tn2(diagindsup)=0;
        tn3(diagindsup)=signo1*0.5./coord.dA(j).';
        %continuidad tx
        A(indir                 ,coord.phi(j,1)+2*nbeq)= tn1.*dAj;
        %continuidad ty
        A(indir+nbeq            ,coord.phi(j,1)+2*nbeq)= tn2.*dAj;
        %continuidad ty
        A(indir+nbeq*2          ,coord.phi(j,1)+2*nbeq)= tn3.*dAj;
        if min(ju(indir))>0
            %continuidad ux
            A(ju(indir)+nbpt        ,coord.phi(j,1)+2*nbeq)= squeeze(U_f3(1,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq   ,coord.phi(j,1)+2*nbeq)= squeeze(U_f3(2,:,:)).'.*dAj;
            %continuidad uy
            A(ju(indir)+nbpt+nbeq*2 ,coord.phi(j,1)+2*nbeq)= squeeze(U_f3(3,:,:)).'.*dAj;
        end
    end
end


    para.coord = coord;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% resolucion del sistema %
%%%%%%%%%%%%%%%%%%%%%%%%%%

phi_fv =zeros(nbeq2,para.ninc);

if para.ninc>1
    A1=inv(A);
    for iinc=1:para.ninc
        phi_fv(:,iinc)=A1*B(:,iinc); %#ok<*MINV>
        %regularizacion de phi_fv
    end
else
    
    phi_fv=A\B;
    %         regularizacion_phi;
    
    %     if issparse(A), R = qr(A);
    %     else R = triu(qr(A)); end
    %     for iinc=1:para.ninc
    %         phi_fv(:,iinc) = R\(R'\(A'*B(:,iinc)));
    %         r = B(:,iinc) - A*phi_fv(:,iinc);
    %         e = R\(R'\(A'*r));
    %         phi_fv(:,iinc) = phi_fv(:,iinc) + e;
    %         regularizacion_phi;
    %     end
end
