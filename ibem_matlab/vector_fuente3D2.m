function [Bout,DWN]=vector_fuente3D2(para,DWN)
% version para cuando las fuentes pueden ser OP y FP
coord = para.coord;
% unpack
xs      = para.xs;
ys      = para.ys;
zs      = para.zs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vectores de condiciones a las fronteras %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbeq    = coord.nbeq;
nbpt    = coord.nbpt;
ju      = coord.ju;
j       = 1:coord.nbpt;

fuente = para.fuente;
kxkV = para.kxk;
kykV = para.kyk;
tipo_onda = para.tipo_onda;
xzs = para.xzs;
tipoMed = zeros(para.ninc,1);
for iinc = 1:para.ninc
  m       = xzs(iinc); 
  tipoMed(iinc) = para.tipoMed(m);
end

saluV = false(nbpt,para.nmed);
imV   = cell(para.nmed,1);
imuV   = cell(para.nmed,1); 
for m = 1:para.nmed
  %indice de los puntos perteneciendo al medio de la fuente, todos
  %estos cumplen con la continuidad de los esfuerzos
  jjx     = coord.indm(m).ind;
  
  %se busca si el medio de la fuente es el de indice mas pequeno de
  %los medios en contactos en el punto de colocacion para fijar el
  %signo de las ecuaciones
  %(cf vector fuente B y construcion matriz A)
  tmp = coord.Xim(jjx,:);
  im  = zeros(1,size(tmp,1));
  for i=1:size(tmp,1)
    im(i) = find(tmp(i,:)~=0, 1, 'first' );
  end
  imV{m} = im;
  %indice de los puntos perteneciendo al medio de la fuente y que
  %tienen que cumplir con continuidad de desplazamientos
  saluV(j(jjx),m)= sum(coord.Xim(j(jjx),:)~=0,2)==2;
  %se busca si el medio de la fuente es el de indice mas pequeno de
  %los medios en contactos en el punto de colocacion para fijar el
  %signo de las ecuaciones
  %(cf vector fuente B y construcion matriz A)
  tmp=coord.Xim(saluV(:,m),:);
  imu=zeros(1,size(tmp,1));
  for i=1:size(tmp,1)
    imu(i) = find(tmp(i,:)~=0, 1, 'first' );
  end
  imuV{m} = imu;
end

Bt  = zeros(para.ninc,3*nbeq);
U0  = cell(para.ninc,1);
S0  = cell(para.ninc,1);
for iinc = 1:para.ninc
  Ci = [];
  ksi = [];
  kpi = [];
  kri = [];
  B = zeros(3*nbeq,1);
  m       = xzs(iinc);   % medio al que pertence la fuente
  DWNtmp = DWN;
  if fuente(iinc)==1 %OP
    % incidencia de ondas planas homogeneas e inhomogeneas
    % siempre en el medio 1
    % La onda incidente es unitaria con un componente horizontal del numero de
    % onda definido con respecto al numero de onda k por un factor kxk y kyk.
    kxk     = kxkV(iinc); %cartesiana x de la normal de la OP
    kyk     = kykV(iinc); %cartesiana y de la normal de la OP
    polOP   = tipo_onda(iinc);
    
    if tipoMed(iinc) == 1 % homogeneo %para.tipoMed(m) == 1 % homogeneo
      if isfield(para,'sumb1')
        mmat    = para.subm1(m); % indice correcto de m si hubo subdivici???n
      else
        mmat    = m; % en el caso 3D gen
      end
      Ci      = para.reg(mmat).Ci;
      ksi     = para.reg(mmat).ksi;
      kpi     = para.reg(mmat).kpi;
      kri     = para.reg(mmat).kri;
    end
    
    %indice de los puntos perteneciendo al medio de la fuente, todos
    %estos cumplen con la continuidad de los esfuerzos
    jjx     = coord.indm(m).ind;
    salu = saluV(:,m);
    im = imV{m};
    imu = imuV{m};
    
%     
% %     %% testme
% %     figure;hold on
% %     plot3(coord.x,coord.y,coord.z,'k*')
% %     plot3(coord.x(jjx),coord.y(jjx),coord.z(jjx),'ro')
% %     jjnotthis = coord.indm(2).ind;
% %     plot3(coord.x(jjnotthis),coord.y(jjnotthis),coord.z(jjnotthis),'b.')
% %     %%
%     
%     %se busca si el medio de la fuente es el de indice mas pequeno de
%     %los medios en contactos en el punto de colocacion para fijar el
%     %signo de las ecuaciones
%     %(cf vector fuente B y construcion matriz A)
%     tmp = coord.Xim(jjx,:);
%     im  = zeros(1,size(tmp,1));
%     for i=1:size(tmp,1)
%         im(i) = find(tmp(i,:)~=0, 1, 'first' );
%     end
%     
%     %indice de los puntos perteneciendo al medio de la fuente y que
%     %tienen que cumplir con continuidad de desplazamientos
%     salu    = false(nbpt,1);
%     salu(j(jjx))= sum(coord.Xim(j(jjx),:)~=0,2)==2;
%     %se busca si el medio de la fuente es el de indice mas pequeno de
%     %los medios en contactos en el punto de colocacion para fijar el
%     %signo de las ecuaciones
%     %(cf vector fuente B y construcion matriz A)
%     tmp=coord.Xim(salu,:);
%     imu=zeros(1,size(tmp,1));
%     for i=1:size(tmp,1)
%         imu(i) = find(tmp(i,:)~=0, 1, 'first' );
%     end
% %     clear tmp
    
%     %% testme
%     figure;hold on
%     h = plot(jjx,'k-'); set(h,'DisplayName','jjx');
%     h = plot(im,'k--'); set(h,'DisplayName','im');
%     h = plot(salu,'b-'); set(h,'DisplayName','salu');
%     h = plot(imu,'b--'); set(h,'DisplayName','imu');
%     %%
    
    if para.tipoMed(m)==2 % Medio de Estratificado (DWN)
      [u,t,DWNtmp{m}] = campo_ref_OP3D_DWN(iinc,coord,para,DWNtmp{m},polOP,kxk);
    else % Medio homogEneo
      if para.fuenteimagen==1 % ... usando la fuente imagen (si es onda plana)
        [u,t] = campo_ref_OPHeI_3D_SE(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,kri,Ci,polOP,kxk,kyk);
        %                 t(isnan(t)) = 0;  u(isnan(u)) = 0;
      else % ... usando puntos de colocaciOn
        [u,t] = campo_ref_OPHeI_3D(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,kri,Ci,polOP,kxk,kyk);
      end
    end
    
%     %% Test me
%     figure(543);clf;dibujo_conf_geo(para,gca);title('tracciones')
%     hold on;set(gca,'Zdir','reverse');axis equal;grid on
%     quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%       real(t(1,j(jjx))),real(t(2,j(jjx))),real(t(3,j(jjx))),'LineWidth',1)
%     
%     figure(544);clf;dibujo_conf_geo(para,gca);title('desplazamientos')
%     hold on;set(gca,'Zdir','reverse');axis equal;grid on
%     quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%       real(u(1,j(jjx))),real(u(2,j(jjx))),real(u(3,j(jjx))),'LineWidth',1)
    %%
    
    %esfuerzos
    B(j(jjx)       )       = -((m==im ) - (m~=im )).*t(1,j(jjx));%tx
    B(j(jjx)+  nbeq)       = -((m==im ) - (m~=im )).*t(2,j(jjx));%ty
    B(j(jjx)+2*nbeq)       = -((m==im ) - (m~=im )).*t(3,j(jjx));%tz
    %desplazamientos
    B(nbpt+ju(salu)       )= -((m==imu) - (m~=imu)).*u(1,j(salu));%ux
    B(nbpt+ju(salu)+  nbeq)= -((m==imu) - (m~=imu)).*u(2,j(salu));%uy
    B(nbpt+ju(salu)+2*nbeq)= -((m==imu) - (m~=imu)).*u(3,j(salu));%uz

  elseif para.fuente(iinc)==2 % FP
    m       = para.xzs(iinc); %El medio al que pertenece lar fuente
    
    %indice de los puntos perteneciendo al medio de la fuente, todos
    %estos cumplen con la continuidad de los esfuerzos
    jjx     = coord.indm(m).ind;
    salu = saluV(:,m);
    im = imV{m};
    imu = imuV{m};
    
%     %indice de los puntos perteneciendo al medio de la fuente y que
%     %tienen que cumplir con continuidad de desplazamientos
%     salu    = false(nbpt,1);
%     salu(j(jjx))= sum(coord.Xim(j(jjx),:)~=0,2)==2;
%     
%     %se busca si el medio de la fuente es el de indice mas pequeno de
%     %los medios en contactos en el punto de colocacion para fijar el
%     %signo de las ecuaciones
%     %(cf vector fuente B y construcion matriz A)
%     tmp=coord.Xim(jjx,:);
%     im =zeros(1,size(tmp,1));
%     for i=1:size(tmp,1)
%       im(i) = find(tmp(i,:)~=0, 1, 'first' );
%     end
%     
%     tmp=coord.Xim(salu,:);
%     imu=zeros(1,size(tmp,1));
%     for i=1:size(tmp,1)
%       imu(i) = find(tmp(i,:)~=0, 1, 'first' );
%     end
    
    
    % se calcula los terminos fuentes
    if para.tipoMed(m)==2 % estratificado
      [u,t,DWNtmp{m}]   = campo_ref_FP3D_DWN(iinc,coord,para,DWNtmp{m});
    else
      fij = para.fij(iinc,:);
      mmat    = para.subm1(m);
      Ci      = para.reg(mmat).Ci;
      ksi     = para.reg(mmat).ksi;
      kpi     = para.reg(mmat).kpi;
      [u,t]   = campo_ref_FP3D(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,Ci,fij,salu,jjx,para);
    end
    
%     %% Test me
%     figure(543);clf;dibujo_conf_geo(para,gca);title('tracciones')
%     hold on;set(gca,'Zdir','reverse');axis equal;grid on
%     quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%       real(t(1,j(jjx))),real(t(2,j(jjx))),real(t(3,j(jjx))),'LineWidth',1)
%     
%     figure(544);clf;dibujo_conf_geo(para,gca);title('desplazamientos')
%     hold on;set(gca,'Zdir','reverse');axis equal;grid on
%     quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%       real(u(1,j(jjx))),real(u(2,j(jjx))),real(u(3,j(jjx))),'LineWidth',1)
%     %%
    %esfuerzos
    B(j(jjx)       )      	= -((m==im ) - (m~=im )).*t(1,j(jjx));%tx
    B(j(jjx)+  nbeq)        = -((m==im ) - (m~=im )).*t(2,j(jjx));%ty
    B(j(jjx)+2*nbeq)      	= -((m==im ) - (m~=im )).*t(3,j(jjx));%tz
    
    %desplazamientos
    B(nbpt+ju(salu)       )= -((m==imu) - (m~=imu)).*u(1,j(salu));%ux
    B(nbpt+ju(salu)+  nbeq)= -((m==imu) - (m~=imu)).*u(2,j(salu));%uz
    B(nbpt+ju(salu)+2*nbeq)= -((m==imu) - (m~=imu)).*u(3,j(salu));%uz
    % en un punto de colocacion, hay siempre 2 medios en contacto,
    % aunque a veces este medio es el vacio
    % las ecuaciones de continuidad de traccion o de desplazamiento
    % involven entonces siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
    % y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
    % se reorganisa entonces como :
    % sigma_diff(m)-sigma_diff(m1)=u_inc(m1)-u_inc(m)
    % los signos se fijan deacuerdo con el indice del medio,
    % + si el indice es el mas pequeno de los medios en contacto
    % - en lo contrario
  else
    % reservado para dislocacion
    warning(['la fuente ',num2str(iinc),' no se asigno'])
  end

  Bt(iinc,:) = B.';
  U0{iinc}.m = m;
  if para.tipoMed(m)==2 
  U0{iinc}.U0 = DWNtmp{m}.U0(:,:,iinc);
  S0{iinc}.S0 = DWNtmp{m}.S0(:,:,iinc);
  end
end
for iinc = 1: para.ninc
  m = U0{iinc}.m;
  if para.tipoMed(m)==2
  DWN{m}.U0(:,:,iinc) = U0{iinc}.U0;
  DWN{m}.S0(:,:,iinc) = S0{iinc}.S0;
  end
end
Bout = Bt.';
if para.GraficarCadaDiscretizacion
    %% testme
    figure(654321); clf; hold on
    plot(real(Bout(:,1)),'r')
    plot(imag(Bout(:,1)),'b')
    l = length(Bout(:,1))/3;
    plot([l l],ylim,'k-')
    plot([2*l 2*l],ylim,'k-')
    title('vector fuente')
  end
% Save_tmp_U0(DWN,para.j);
end
