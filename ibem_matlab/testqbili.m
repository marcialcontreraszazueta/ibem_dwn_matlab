function res=testqbili(f,qd,fcorner,m,b)
    if f <= fcorner
        res = qd;
    else
        res = b+f*m;
    end
end

