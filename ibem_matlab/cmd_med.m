med     =get(bouton.med,'value');
thissubmed = get(bouton.submed,'value');
if thissubmed > para.reg(med).nsubmed; thissubmed = 1; set(bouton.submed,'value',1);end
dim = get(bouton.dim,'value');
para.dim = dim;
geo = para.geo(med);
tipoMed = para.tipoMed(med);

if para.chggeo==1 % si la geometr�a ha cambiado
  para.chggeo=0;
  % Nuevo elemento de geometr�a. Inicializar variables:
  para.cont(med,1).piece{info.ThisPiece}.fileName = '';
  para.cont(med,1).piece{info.ThisPiece}.geoFileData = [];
  if para.rafraichi==0
    cmd_pol;%chgmt des sorties possibles
  end
  
  %% inicializar variables
  if geo >4 && dim ==4; geo = 1; warning ('geo > 2 and dim == 4; fixed'); end
  if geo >8; error('geo > 5'); end
  if  tipoMed == 1 % Espacio homog�neo
    % nada que preguntar sobe la geometr�a
  elseif tipoMed == 2 % medio estratificado con DWN
    if para.reg(med).nsubmed > 0
      %il faut supprimer des champs;
      para.reg(med) = [];
      para.reg(med).nsubmed = 2;
      para.reg(med).sub(1).h=1;
      para.reg(med).sub(2).h=0;
      sub0            = para.reg(med).sub(1:2);
      para.reg(med).sub = sub0;
    end
    if para.reg(med).nsubmed == 0
      para.reg(med).nsubmed = 2;
    end
    if isempty(para.reg(med).nsubmed)
      para.reg(med).nsubmed = 2;
    end
    for i=1:para.reg(med).nsubmed
      para.reg(med).sub(i).rho      = 1;
      para.reg(med).sub(i).alpha    = 2;
      para.reg(med).sub(i).bet      = 1;
      para.reg(med).sub(i).qd       = 1000;
      para.reg(med).sub(i).tipoatts = 1;
      para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
      para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
      para.reg(med).sub(i).h        = 1;
    end
    para.reg(med).sub(i).h        = 0;%ultimo estrato=semi espacio
    strsubmed      = 1:para.reg(med).nsubmed;
    set(bouton.submed ,'string',strsubmed,'value',1);
    set(bouton.nsubmed,'string',para.reg(med).nsubmed);
  end
  if dim ~= 4
    if geo == 3
      %'Semi Espacio'
      para.cont(med,1).xa=-3;
      para.cont(med,1).a = 6;
      para.cont(med,1).th= 0;
    elseif geo == 4
      %'2 contornos horizontales'
      para.cont(med,1).ba   = 0.25;
    elseif geo == 5
      %'placa ilimitada'
      para.cont(med,1).ba   = 0;
      if para.geo(1)==3 % si el fondo es un semiespacio
        para.cont(med,1).a =para.cont(1,1).a;
        para.cont(med,1).xa=para.cont(1,1).xa;
      else
        para.cont(med,1).a = 3;
        para.cont(med,1).xa=-3;
      end
    elseif geo == 6 || geo == 7
      %'semi-placa L' o 'semi-placa R'
      para.cont(med,1).ba   = 0.2;
      if para.geo(1)==3 % si el fondo es un semiespacio
        para.cont(med,1).a =para.cont(1,1).a/2;
        if  para.tipoMed(med)==2
          para.cont(med,1).xa=0;
        else
          para.cont(med,1).xa=para.cont(1,1).xa;
        end
      else
        para.cont(med,1).a = 3;
        para.cont(med,1).xa=-3;
      end
    end
    para.cont(med,2).a = para.cont(med,1).a;
    para.cont(med,2).ba = para.cont(med,1).ba;
    para.cont(med,2).xa = para.cont(med,1).xa;
    para.cont(med,2).za = para.cont(med,1).za;
    para.cont(med,2).th = para.cont(med,1).th;
  else  % 3D general
    if geo == 1+2 %no boundary
      
    else
      %'From STL file(s)'
      para.cont(med,1).piece{info.ThisPiece}.fileName = get(bouton.geoFileSelect,'string');
      para.cont(med,1).piece{info.ThisPiece}.continuosTo = 0;
      para.cont(med,1).piece{info.ThisPiece}.ColorIndex = 1;
      para.cont(med,1).piece{info.ThisPiece}.kind = 1;
%       if ~strcmp(para.cont(med,1).piece{info.ThisPiece}.fileName,'')
%         [~,~] = previewSTL(0,para.cont(med,1).piece{info.ThisPiece});
%       end
    end
  end
end

% Actualziar los botones en "Propiedades de los medios"
if tipoMed == 2 % estratificado con DWN
  set(bouton.rho    ,'Callback','cms;para.reg(med).sub(submed).rho=str2num(get(bouton.rho,''string''));cmd_lambda_mu_sub;clear med submed');
  set(bouton.alpha  ,'Callback','cms;para.reg(med).sub(submed).alpha=str2num(get(bouton.alpha,''string''));cmd_lambda_mu_sub;clear med submed');
  set(bouton.bet    ,'Callback','cms;para.reg(med).sub(submed).bet=str2num(get(bouton.bet,''string''));cmd_lambda_mu_sub;clear med submed');
  set(bouton.lstatts,'Callback','cms;para.reg(med).sub(submed).tipoatts=get(bouton.lstatts,''value'');cmd_att_sub;clear med submed');
  set(bouton.Q      ,'Callback','cms;para.reg(med).sub(submed).qd =str2num(get(bouton.Q  ,''string''));clear med submed');
  
  set(bouton.bet  ,'string',para.reg(med).sub(thissubmed).bet);
  set(bouton.alpha,'string',para.reg(med).sub(thissubmed).alpha);
  set(bouton.rho  ,'string',para.reg(med).sub(thissubmed).rho);
  set(bouton.Q    ,'string',para.reg(med).sub(thissubmed).qd);
  set(bouton.lstatts  ,'value',para.reg(med).sub(thissubmed).tipoatts);
  if para.rafraichi==0
    cmd_att_sub;
    cmd_lambda_mu_sub;
  end
  strsubmed   = 1:para.reg(med).nsubmed;
  set(bouton.submed,'string',strsubmed,'value',1);
  set(bouton.nsubmed,'string',para.reg(med).nsubmed);
  if para.rafraichi==0
    cmd_nsubmed;
  end
  cmd_submed;
  cmd_att_sub;
  cmd_lambda_mu_sub;
elseif tipoMed == 1 % medio homog�neo
  set(bouton.rho    ,'Callback','cms;para.reg(med).rho=str2num(get(bouton.rho,''string''));cmd_lambda_mu;clear med submed');
  set(bouton.alpha  ,'Callback','cms;para.reg(med).alpha=str2num(get(bouton.alpha,''string''));cmd_lambda_mu;clear med submed');
  set(bouton.bet    ,'Callback','cms;para.reg(med).bet=str2num(get(bouton.bet,''string''));cmd_lambda_mu;clear med submed');
  set(bouton.lstatts,'Callback','cms;para.reg(med).tipoatts=get(bouton.lstatts,''value'');cmd_att;clear med submed');
  set(bouton.Q      ,'Callback','cms;para.reg(med).qd =str2num(get(bouton.Q  ,''string''));clear med submed');
  
  set(bouton.bet    ,'string',para.reg(med).bet);
  set(bouton.alpha  ,'string',para.reg(med).alpha);
  set(bouton.rho    ,'string',para.reg(med).rho);
  set(bouton.Q      ,'string',para.reg(med).qd);
  set(bouton.lstatts,'value',para.reg(med).tipoatts);
  %   if para.rafraichi==0
  cmd_att;
  cmd_lambda_mu;
  %   end
end
set(bouton.tipoMed,'value',tipoMed);

% Actualizar los botones en "Geometr�a del medio"
set(info.geo,'string',['La frontera del medio ',num2str(med),' es:']);
if (dim == 4) % 3D general
  set(bouton.geo  ,'string',bouton.strgeo3D,'value',geo-2);
else % 2D, 2.5D, 3D 'axisim�trico'
  set(bouton.geo  ,'string',bouton.strgeo2D,'value',geo-1);
end
icont = get(bouton.cont,'value');
onoffgeomparam='off'; onoffSTL = 'off'; onoffdwn = 'off';
onoffgeomparetesdelcontorno = 'off';
if tipoMed == 2 %DWN
  onoffdwn = 'on';
end
if dim == 4 %STL
  if geo == 4
    onoffSTL = 'on';
  end
else
  onoffgeomparam='on';
  set(bouton.xa	,'string',para.cont(1,1).xa);
  set(bouton.a	,'string',para.cont(1,1).a);
  set(bouton.th	,'string',para.cont(1,1).th);
  if geo ~= 3
    onoffgeomparetesdelcontorno = 'on';
  end
  if geo == 2
    onoffgeomparetesdelcontorno = 'off';
    onoffgeomparam='off';
  end
end

% De las geometr�as param�tricas:
set(info.parpos ,'visible',onoffgeomparam);
set(info.xa     ,'visible',onoffgeomparam);
set(bouton.xa   ,'visible',onoffgeomparam);
set(info.za     ,'visible',onoffgeomparam);
set(bouton.za   ,'visible',onoffgeomparam);
set(info.a      ,'visible',onoffgeomparam);
set(bouton.a   	,'visible',onoffgeomparam);
set(info.th     ,'visible',onoffgeomparam);
set(bouton.th   ,'visible',onoffgeomparam);

set(info.partes ,'visible',onoffgeomparetesdelcontorno);
set(info.cont   ,'visible',onoffgeomparetesdelcontorno);
set(bouton.cont ,'visible',onoffgeomparetesdelcontorno);
set(info.contgeo,'visible',onoffgeomparetesdelcontorno);
set(bouton.contgeo,'visible',onoffgeomparetesdelcontorno);
set(info.base   ,'visible',onoffgeomparetesdelcontorno);
set(bouton.base ,'visible',onoffgeomparetesdelcontorno);
set(info.haut   ,'visible',onoffgeomparetesdelcontorno);
set(bouton.haut ,'visible',onoffgeomparetesdelcontorno);

set(info.rug   	,'visible',onoffgeomparam);
set(info.ruggeo ,'visible',onoffgeomparam);
set(bouton.ruggeo,'visible',onoffgeomparam);
if para.cont(med,icont).ruggeo==1
  onoffgeomparam='off';
else
  onoffgeomparam='on';
end
set(info.rugbase    ,'visible',onoffgeomparam);
set(bouton.rugbase  ,'visible',onoffgeomparam);
set(info.rughaut  	,'visible',onoffgeomparam);
set(bouton.rughaut	,'visible',onoffgeomparam);

set(bouton.geoFileSelect,'visible',onoffSTL);
set(bouton.geoFilePanel,'visible',onoffSTL);
set(allchild(bouton.gfPreview),'visible',onoffSTL);

set(info.DWN,'visible',onoffdwn);
set(bouton.nsubmed,'visible',onoffdwn);
set(info.DWNxl,'visible',onoffdwn);
set(bouton.DWNxl,'visible',onoffdwn);
% set(info.DWNnbptkx,'visible',onoffdwn);
% set(bouton.DWNnbptkx,'visible',onoffdwn);
set(info.estrDWN,'visible',onoffdwn);
set(info.submed,'visible',onoffdwn);
set(bouton.submed,'visible',onoffdwn);
set(info.subh,'visible',onoffdwn);
set(bouton.subh,'visible',onoffdwn);

if para.rafraichi==0
  cmd_cont;
end

if para.rafraichi==0
  cmd_fuente;%pour update des ondes planes possibles si med==1 && para.geo(1)==3
end

clear submed dim geo sub0 onoffgeomparam onoffgeomparam onoffSTL onoffdwn
