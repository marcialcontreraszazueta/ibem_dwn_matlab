function [medio] = inclusiontest3G(xs,ys,zs,para)
% El medio al que pertenece un punto(x,y,z) dado un polihedro en 3D
% Los contornos est?n formados por la rectificaci?n de su superfice
% mediante tri?ngulos. Los contornos son superfices cerradas

npts    = size(xs,1);
medio   = ones(npts,1); %Est? en el medio de fondo (suposici?n inicial)
% medio(zs<0) = 0; % Medio 0 (suposici?n inicial)


m       = para.nmed;
dr=1e-10; % tolerancia
testme = ones(npts,1); % 1: si revisar

while m>1
  if isfield(para.cont(m,1),'FV')
    if ~isempty(para.cont(m,1).FV)
      for ip = 1:npts
        if xs(ip)-min(para.cont(m,1).FV.vertices(:,1))<-dr || max(para.cont(m,1).FV.vertices(:,1))-xs(ip)<-dr || ...
            ys(ip)-min(para.cont(m,1).FV.vertices(:,2))<-dr || max(para.cont(m,1).FV.vertices(:,2))-ys(ip)<-dr || ...
            zs(ip)-min(para.cont(m,1).FV.vertices(:,3))<-dr || max(para.cont(m,1).FV.vertices(:,3))-zs(ip)<-dr
          %de plano, no pertenece a m
          testme(ip)=0; % 0: no revisar
        end
      end
      t = find(testme); % indices de los que hay que revisar
      if ~isempty(t)
        tcopy = t;
        for ip = t.'
          if abs(zs(ip)-min(para.cont(m,1).FV.vertices(:,3)))<dr && abs(max(para.cont(m,1).FV.vertices(:,3))-zs(ip))<dr
            % la superficie es plana y el receptor estA a la misma profundidad
            if min(para.cont(m,1).FV.vertices(:,1)) < xs(ip)+dr && xs(ip)-dr < max(para.cont(m,1).FV.vertices(:,1)) && ...
                min(para.cont(m,1).FV.vertices(:,2)) < ys(ip)+dr && ys(ip)-dr < max(para.cont(m,1).FV.vertices(:,2))
              % dentro de un cuadrado con las coordenads
              
              % para rApido se asume que pertenece a este medio
              medio(ip) = m;
              tcopy(tcopy==ip)=[];
            end
          end
        end
        t = tcopy;
        if ~isempty(t)
          % para toda la superficie
        in = inpolyhedron(para.cont(m,1).FV,...
          [xs(t),ys(t),zs(t)]);%,'facenormals', para.cont(m,1).FV.facenormals(t,1:3));
        medio(t(in)) = m;
          % y cada pieza
          for p=1:para.cont(m,1).NumPieces
            tFV.faces       = para.cont(m,1).piece{p}.geoFileData.F;
            tFV.vertices    = para.cont(m,1).piece{p}.geoFileData.V;
            tFV.facenormals = para.cont(m,1).piece{p}.geoFileData.N;
            in = inpolyhedron(tFV,[xs(t),ys(t),zs(t)]);
            medio(t(in)) = m;
          end
          % y si la pieza es superficie libre y el receptor esta muy cerca
          for p=1:para.cont(m,1).NumPieces
            if para.cont(m,1).piece{p}.kind ~= 2 % 1 Free surface, 2 Continuity, 3 Auxiliar
              % cada receptor
              for it = t.'
                if medio(it) == 1
                  % distancia minima a la superficie
                  d = (xs(it) - para.cont(m,1).piece{p}.geoFileData.centers(1,:)).^2 + ...
                      (ys(it) - para.cont(m,1).piece{p}.geoFileData.centers(2,:)).^2 + ...
                      (zs(it) - para.cont(m,1).piece{p}.geoFileData.centers(3,:)).^2;
                  dm = sqrt(min(d));
                  if dm <= dr
                    medio(it) = m;
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  m=m-1;
end
end