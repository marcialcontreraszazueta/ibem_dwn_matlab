function GenerarGeometriaMontanaGaussiana
% cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'
%% datos 
clear
set(0,'DefaultFigureWindowStyle','docked') % 'normal'
% S?nchez-Sesma & Luzon 1995
% a > b  && b = 0.7a && h = 0.4/a && a = 4km
% f(x,y) = h(b^2 - P^2)[1 - 2a(a-x)/P^2]
Bmin = 1.8; %km/s
fmax = 6; %Hertz
lamdamin = Bmin/fmax/6; %km
numelems = 4/lamdamin/2; %cantidad de triangulos 
disp(['numelems= ' num2str(numelems)])

nirrradio = 2.1;% 1.1;
nplatradio = 2.0;% 1.0
nespacios = 51;%(n?mero non)
numerodeDivisiones = 0; %veces que NIRR se divide entre 2 
h = 0.1;
x0=0;
y0=0;
sx=0.25;
sy=0.125;
%% generar triangulos 
tamanos = [];
alcance = 1.0*nirrradio;
xlin = linspace(-alcance,alcance,nespacios);
ylin = linspace(-alcance,alcance,nespacios);
[x,y] = meshgrid(xlin,ylin);
z = x.*0;
for i=1:length(xlin)
for j=1:length(ylin)
%   z(i,j) = -ColGau(h,x0,y0,sx,sy,x(i,j),y(i,j));
  z(i,j) = -GaussianaSS1983(x(i,j),y(i,j));
end
end
hf = figure('Visible','Off'); clf
axis tight; hold on
h = mesh(x,y,z);
XData = h.XData(:);
YData = h.YData(:);
ZData = h.ZData(:);
delete(h); close(hf)
%
DT = delaunayTriangulation([XData,YData]); %disp('lista de conectividad NIRR')
DT3 = delaunayTriangulation([XData,YData,ZData]);
Pc = DT3.Points;
TR = triangulation(DT.ConnectivityList,Pc);
%% NIRR 
% que el borde libre tenga coordenada 0
E = freeBoundary(TR);
Pc(E(:,1),3) = 0;

T = TR.ConnectivityList(:,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),2));
end
% hacerlo sim?trico en y
iflp = Cen(:,1)<0; % espejear en y
NN = sum(iflp)*2;
PcT = zeros(NN,3,3); % i,vertice,componente
i = 1;
norm = zeros(NN,3);
for ind = 1:length(iflp)
    if iflp(ind) 
        % copiar original
norm(i,:) = FN(ind,:);
PcT(i,1,1)=Pc(T(ind,1),1); PcT(i,1,2)=Pc(T(ind,1),2); PcT(i,1,3)=Pc(T(ind,1),3);
PcT(i,2,1)=Pc(T(ind,2),1); PcT(i,2,2)=Pc(T(ind,2),2); PcT(i,2,3)=Pc(T(ind,2),3);
PcT(i,3,1)=Pc(T(ind,3),1); PcT(i,3,2)=Pc(T(ind,3),2); PcT(i,3,3)=Pc(T(ind,3),3);  
        % espejear en y
norm(i+1,1:2:3) =  norm(i,1:2:3); 
norm(i+1,  2  ) = -norm(i,  2  );
PcT (i+1,3:-1:1,1:2:3) =  PcT(i,1:3,1:2:3); 
PcT (i+1,3:-1:1,  2  ) = -PcT(i,1:3,  2  );
i = i+2;
    end
end



%
disp(['NIRR= ' num2str(NN)])

%dividir cada traingulo por el factor factorNPLAT
for divisiones = 1:numerodeDivisiones
Pctaux = zeros(NN*2,3,3);
normaux = zeros(NN*2,3);
j = 1;
for i = 1:NN
  % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande
  
  % primer tri?ngulo nuevo
  Pctaux(j,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j,2,:) = PcT(i,l(lM,5),:);%primer v?rtice lado partido
  Pctaux(j,3,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  
  % segundo tri?ngulo nuevo
  Pctaux(j+1,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j+1,2,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  Pctaux(j+1,3,:) = PcT(i,l(lM,6),:);%segundo v?rtice lado partido
  
  % las normales
  normaux(j,:) = norm(i,:);
  normaux(j+1,:) = norm(i,:);
  j = j+2;
end
clear PcT
PcT = Pctaux;
clear Pctaux;
clear norm
norm = normaux;
clear normaux;

NN = size(PcT,1);
end

distac=zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1));%+aCro/8;
    Cen(i,2) = mean(PcT(i,1:3,2));
    Cen(i,3) = mean(PcT(i,1:3,3));
    distac(i) = sum(abs(Cen(i,1:2)).^2)^(1/2);
end
aux = PcT(1:NN,1:3,1:3);
clear PcT
b = distac(:)<nplatradio; %quitar los que est?n lejos
estossi = logical(b);
PcT = aux(estossi,:,:);
NN = size(PcT,1);
disp(['NPLAT= ' num2str(NN)])

disp(['NIRR= ' num2str(NN)])
Cen = zeros(NN,3);
Rad = zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1)); %media x
    Cen(i,2) = mean(PcT(i,1:3,2)); %media y
    Cen(i,3) = mean(PcT(i,1:3,3)); %media z
    
    % correcci?n de circulos:
    % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande y del vertice opeusto
    
    % normal y distancia entre centro de lado m?s largo y v?rtice opuesto
    dl = sqrt(sum(( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) .^2));
    nl = ( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) / dl;
    
    % Recorrer Centro lejos del lado m?s largo
    rd = l(lM,1)^0.5 / dl / 72; %nirr
    Cen(i,1) = Cen(i,1) + nl(1)*rd;
    Cen(i,2) = Cen(i,2) + nl(2)*rd;
    Cen(i,3) = Cen(i,3) + nl(3)*rd;
    
    % A = 1/2 |(v1 - v3) x (v2 - v3)|
    cro = cross(PcT(i,1,1:3) - PcT(i,3,1:3),...
                PcT(i,2,1:3) - PcT(i,3,1:3));
    area = 0.5 * sum(abs(cro).^2)^(1/2);
    Rad(i) = sqrt(area/pi);
end

figure(187655); clf; hold on
set(gca,'ZDir','reverse','Projection','perspective');grid on
xlabel('x [km]'); ylabel('y [km]'); zlabel('z [km]');
set(gcf,'name','nirr')
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3);
  fill3(lx,ly,lz,[0.5 1.0 0.333]);
  
%   h = plotCircle3D([Cen(i,1),Cen(i,2),Cen(i,3)],...
%     [-norm(i,1),-norm(i,2),-norm(i,3)],Rad(i),1);
%    set(h,'color',[0,0.5,0])
end
view(3); axis equal


clear nam
nam = ['MonGau_',num2str(nespacios),'_A'];
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnirr = sprintf('%s %f \n','nirr= ',NN);
tnirr = '';
tamanos(2) = NN;
for i = 1:NN % escribir nirr.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',-norm(i,1),-norm(i,2),-norm(i,3)); % FC
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),Pc(T(i,1),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),Pc(T(i,2),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),Pc(T(i,3),3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),PcT(i,1,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),PcT(i,2,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),PcT(i,3,3));
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnirr = sprintf('%s %f %f %f %f %f %f %f \n',tnirr,...
  [Cen(i,1),Cen(i,2),Cen(i,3),-norm(i,1),-norm(i,2),-norm(i,3),Rad(i)]);
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nirr.txt','w');
fprintf(fileID,'%s',tnirr);
fclose(fileID);
tgeom = sprintf('%s%s',tnirr,tnirr);
cd ..
cd ibem_matlab
disp('done')
end


function out = lado(a,b,l1,l2)
out = zeros(1,6);
out(1) = ( (a(1)-b(1))^2 + ...
           (a(2)-b(2))^2 + ...
           (a(3)-b(3))^2 ); %tama?o de la cara al cuadrado
 % punto medio:
 out(2) = b(1) + (a(1)-b(1))/2;
 out(3) = b(2) + (a(2)-b(2))/2;
 out(4) = b(3) + (a(3)-b(3))/2;
 out(5) = l1;
 out(6) = l2;
end

