function long=long_contorno(cont)
% funcion que calcula la longitud total de los contornos
% cual corresponden a las fronteras comun entre los medios
% = perimetro

%1 :tipo gaussiana
%2 :parabola
%3 :triangulo
%4 :coseno
%5 :eliptico
%6 :eliptico asimetrico
%7 :trapecio
%8 :base puesto en los bordes

h       =cont.h;
geom    =cont.geom;
a       =cont.a;

% if geom==2
%     sq  =sqrt(1.0+4.0*h^2);
%     if h~=0
%         long=sq+0.5/abs(h)*log(2.0*abs(h)+sq);
%     else
%         long=2*sq;
%     end
% else
if geom==3
    long=2.0*sqrt(h^2+a^2);
% elseif geom==5
%     long=pi*0.5*(1.5*(1.0+abs(h))-sqrt(abs(h)));
% elseif geom==8
%     long=pi*ba+2.*(1.0-ba);
elseif int8(geom)==9
  val = hardcodeGeom9;
  x = val(:,1);
  y = val(:,2);
  long = rectificar(x,y);
elseif geom==10
  val = hardcodeGeom10;
  x = val(:,1);
  y = val(:,2);
  long = rectificar(x,y);
else
  %caso general
      % el siguiente codigo funciona muy bien en matlab:
    %perimetro calculado con el integral de la valor absoluta de la derivada
%     long=quad(@(x) local_long_contorno(x,cont),-a,a,1e-6);
    % para la version compilada no se pueden usar funciones anOnimas
      % ni 'integral' ni 'quad' asi que se rectifica:
        x = linspace(-a,a,1/1e-3);
        y = eq_contour(x,cont);
        long = rectificar(x,y);
end

function long = rectificar(x,y)
long = 0;
for i = 2:length(x)
long = long + sqrt((x(i)-x(i-1))^2 + (y(i)-y(i-1))^2);
end
