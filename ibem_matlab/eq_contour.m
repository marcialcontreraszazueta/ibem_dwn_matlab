function z=eq_contour(x,cont)
% fonction donnant z(x) pour la geometrie consideree

h	=cont.h;
geom=cont.geom;
a   =cont.a;

ba 	=cont.ba/a;
x   =x/a;

x2  =x.^2;

%1 :LS
%2 :parabola
%3 :triangulo
%4 :coseno
%5 :eliptico
%6 :eliptico asimetrico
%7 :trapeze
%8 :LG
%9 :Archivo <-- Naye 2016
%10:otro archivo
if geom==1
    z=h*(1-x2)./exp(3*x2);
elseif geom==2
    z=h*(1-x2);
elseif geom==3
    z=h*(1-abs(x));
elseif geom==4
    z=h*cos(pi*x/2).^2;
elseif geom==5
    z=h*sqrt(1-x2);
elseif geom==6
    %     if x<=ba
    %         z=h*(1-((x-ba)/(1.+ba)).^2);
    %     else %if x>ba
    %         z=h*(1-((x-ba)/(1.-ba)).^2);
    %     end
    signo=sign(ba-x)+(x==ba);
    z=h*(1-((x-ba)./(1+signo*ba)).^2);
elseif geom==7
    d=0.05/a;%courbure trapeze
    p=-h/(4*d*(1-ba));
    if ba==1
        p=0;
    end
    aux1=abs(x)-(ba-d);
    aux2=abs(x)-(ba+d);
    z=h+p*(aux1.^2.*(aux1>0)-aux2.^2.*(aux2>0));
elseif geom==8
    %     if x<=(ba-1)
    %         z=sqrt(ba^2-(x+1-ba).^2);
    %     elseif x>(ba-1) && x<=(1-ba)
    %         z=ba;
    %     else %if x>(1-ba)
    %         z=sqrt(ba*ba-(x-1.+ba).^2);
    %     end
    z=sqrt(ba^2-(x+1-ba).^2)    .*(x<=(ba-1)) +...
        ba                      .*(x>(ba-1)).*(x<=(1-ba)) +...
        sqrt(ba^2-(x-1+ba).^2)  .*(x>(1-ba));
    z=z/ba*h;
elseif geom==9
%   % Leer ordendas de archivo geom9.txt y escalar por h y a
%   dat = importdata('geom9.txt');
%    dat = dat - dat(1);
%    dat = dat * (cont.h/max(dat));
%   diffz = dat(1) - cont.za;
%   dat = dat - diffz;
%   na = length(dat);  
%   xa = linspace(cont.xa,cont.xa+2*cont.a,na);
%   z = interp1(xa,dat,x*a);
  
  % Leer X,Z y usarlas sin importar h ni a
%   val = hardcodeGeom9;%importdata('../ins/seccionXZ.txt');
%   xa = val(:,1);
%   dat = val(:,2);
%   z = interp1(xa,dat,x);
  
  val = hardcodeGeom9;%importdata('../ins/seccionXZ.txt');
%   dat = val(:,2);
%   dat = dat - dat(1);
%   dat = dat * cont.h;%(cont.h/max(dat));
%   diffz = dat(1) - cont.za;
%   dat = dat - diffz;
%   na = length(dat);  
%   xa = linspace(cont.xa,cont.xa+2*cont.a,na);
%   xa = linspace(val(1,1),val(end,2),na);
%   z = interp1(xa,dat,x*a);
%   z = interp1(xa,dat,x*a);
%   x = val(:,1);
  z = val(:,2);
elseif geom == 10
  val = hardcodeGeom10;%importdata('../ins/seccionXZ.txt');
%   dat = val(:,2);
  z = val(:,2);
end
z=real(z);
if geom < 9
z(x==-1)=1e-16*sign(h);
z(x== 1)=1e-16*sign(h);
end

