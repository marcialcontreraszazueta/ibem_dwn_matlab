function name=namefile(para)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% creacion del nombre por plataforma %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
strdim          ={'2D','2.5D','axi3D','gen3D'};
strpol          ={'SH','PSV'};
strfuente       ={'OP','FP'};
strmetodomed1   ={'Hom','DWN','WFE'};

if para.dim==1
    name=[strdim{para.dim},'_',strpol{para.pol},'_',strfuente{para.fuente}, ...
        '_xs',num2str(para.xs(1)),'_zs',num2str(para.zs(1)),'_npplo',num2str(para.npplo),'_'];
    if sum(para.tipoMed == 2)>0 
        name=[name,strmetodomed1{2},'_'];
    elseif sum(para.tipoMed == 1)>0 
        name=[name,strmetodomed1{1},'_'];
    else % ==1.5
        name=[name,strmetodomed1{3},'_'];
    end
else
    name=[strdim{para.dim},'_',strfuente{para.fuente}, ...
        '_xs',num2str(para.xs(1)),'_zs',num2str(para.zs(1)),'_npplo',num2str(para.npplo),'_'];
    if sum(para.tipoMed == 2)>0 
        name=[name,strmetodomed1{2},'_'];
    else
        name=[name,strmetodomed1{1},'_'];
    end
end

% fix para.nomrep
if isunix
  v = [fileparts(pwd),'/out'];
else
  v = [fileparts(pwd),'\out'];
end
if iscell(v)
    para.nomrep = v{1};
  else
    para.nomrep = v;
end
if isunix
    name=[para.nomrep,'/',name,datestr(now,30)];
else
    name=[para.nomrep,'\',name,datestr(now,30)];
end
name=[name,'.mat'];