thissubmed     =get(bouton.submed,'value');
med = get(bouton.med,'value');

set(bouton.bet  ,'string',para.reg(med).sub(thissubmed).bet);
set(bouton.alpha,'string',para.reg(med).sub(thissubmed).alpha);
set(bouton.rho  ,'string',para.reg(med).sub(thissubmed).rho);
set(bouton.subh ,'string',para.reg(med).sub(thissubmed).h);
set(bouton.Q    ,'string',para.reg(med).sub(thissubmed).qd);
if para.reg(med).sub(thissubmed).tipoatts==0 %update field
    para.reg(med).sub(thissubmed).tipoatts=3;
end
set(bouton.lstatts  ,'value',para.reg(med).sub(thissubmed).tipoatts);

if para.rafraichi==0
    cmd_att_sub;
    cmd_lambda_mu_sub;
end
