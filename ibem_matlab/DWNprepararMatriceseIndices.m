function DWN = DWNprepararMatriceseIndices(para,DWN)
% Preparar matriz global de medios estratificados (DWN)
coord = para.coord;
nbeq    = coord.nbeq;
nbeq2   = 3*nbeq;       %hay ecuaciones para phix, phiy y phiz
for imm = 1:para.nmed
  if para.tipoMed(imm) == 2 % si este medio es estratificado
    
    % 1) calculo de la matriz global DWN
    DWN{imm}.m = imm;
    DWN{imm} = calcul_A_DWN_3D_polar_Ncapas_HS(para,DWN{imm});
    % 2) inversion de las matrices
    invA = DWN{imm}.A_DWN; % en el plano
    invB = DWN{imm}.B_DWN; % antiplano
    % parfor
    for i=1:length(DWN{imm}.kr)
      invA(:,:,i)=inv(invA(:,:,i));
      invB(:,:,i)=inv(invB(:,:,i));
    end
    DWN{imm}.A_DWN = invA;
    DWN{imm}.B_DWN = invB;
    clear notInvA notInvB invA invB
    % 3) identificacion de los puntos en los cuales se tiene que calcular DWN
    %    posicion puntos de colocacion: receptores y fuentes (virtuales)
    
    %   indpt               = 1:coord.nbpt;         %indice de todos los puntos
    indi                = coord.indm(1,imm).ind;%logical de los puntos en imm
    %   indir               = indpt(indi);          %indice de estos puntos
    
    xrv                 = coord.x(indi);
    yrv                 = coord.y(indi);
    zrv                 = coord.z(indi);
    nxrv                = length(xrv);
%     vn = zeros(3,nxrv);
%     vn(1,1:nxrv)        = coord.vnx(indi);
%     vn(2,1:nxrv)        = coord.vny(indi);
%     vn(3,1:nxrv)        = coord.vnz(indi);
    salu                = ones(nxrv,1);
    sals                = ones(nxrv,1);
    
    %reduccion du numero de profundidades de las funtes virtuales, moviendo
    %los puntos en z de manera a agrupar puntos cercanos (<lambda/30)
    [zrfv,izrfv,zrref]	= pseudo_unique(zrv,para);
    % zrfv=zrv.';zrref=zrv;izrfv=(1:length(zrv)).';
    coord.z(indi)       = zrref;
    %actualizacion de la posicion modificada de los puntos de colocacion
    
    %%% se incluye de una ves los receptores reales para el calculo de los
    %   campos difractados.
    %   El campo difractado es indepediente de las
    %   amplitudes de las fuerzas en cada punto de colocacion.
    %   Eso se toma en cuenta  hasta tomar en cuenta los phi cf inversion_3D_k
    xrr                 = para.rec.xr(para.rec.m(imm).ind).';
    yrr                 = para.rec.yr(para.rec.m(imm).ind).';
    zrr                 = para.rec.zr(para.rec.m(imm).ind).';
    nrr                 = para.rec.m(imm).nr;
    
    [zrfr,izr0,~]       = pseudo_unique(zrr,para);
    
    % inicialisacion del vector de los diffractados en los receptores
    % reales y salidas
    sal                 = para.sortie;
    %ns                  = (sal.Ux + sal.Uy + sal.Uz)*sal.Ut;
    nss                 = sal.sxx + sal.syy + sal.szz + sal.sxy + sal.sxz + sal.syz;
    %   DWN{imm}.Udiff      = zeros(ns ,nbeq2,para.rec.m(imm).nr);
    %   DWN{imm}.Sdiff      = zeros(nss,nbeq2,para.rec.m(imm).nr);
    DWN{imm}.Udiffsize      = [3 ,nbeq2,para.rec.m(imm).nr];
    DWN{imm}.Sdiffsize      = [9,nbeq2,para.rec.m(imm).nr];
    salur               = logical(ones(nrr,1)*sal.Ut);
    salsr               = logical(ones(nrr,1)*(nss>0));
    
    %concatenacion de los puntos virtuales y reales y concatenacion de las salidas
    xr                  = [xrv,xrr];
    yr                  = [yrv,yrr];
    zr                  = [zrv,zrr];
    zr0                 = [zrfv;zrfr];
    izr0                = [izrfv;izr0+length(zrfv)];
    salu                = [salu;salur];
    sals                = [sals;salsr];
    
    nzr0                = length(zr0);
    zricrall            = zeros(1,nzr0);
    icrall              = zeros(1,nzr0);
    for ir=1:nzr0
      icrv     = 1; % Valor inicial. Estrato del receptor
      zricrv   = zr0(ir); % profundidad relativa a la interface de la capa del receptor
      while zricrv>para.reg(imm).sub(icrv).h && icrv<para.reg(imm).nsubmed
        zricrv   = zricrv-para.reg(imm).sub(icrv).h;
        icrv     = icrv+1;
      end
      zricrall(ir) = zricrv;
      icrall(ir)   = icrv;
    end
    
    %reduccion du numero de profundidades de los receptores
    DWN{imm}.xr              = xr;
    DWN{imm}.yr              = yr;
    DWN{imm}.zr              = zr;
    DWN{imm}.zr0             = zr0;     % zr no repetidos o muy cercanos
    DWN{imm}.izr0            = izr0;    % Indices de zr0 a zr
    %   DWN{imm}.len_zrfv        = length(zrfv);
    %   DWN{imm}.izrfv           = izrfv;
    %   DWN{imm}.nrec            = length(xr);
    DWN{imm}.zricrall        = zricrall;% profundidad a interface de arriba
    DWN{imm}.icrall          = icrall;  % estrato del receptor
    DWN{imm}.salu            = salu;
    DWN{imm}.sals            = sals;
    DWN{imm}.nxrv            = nxrv;
    DWN{imm}.ncapas          = para.reg(imm).nsubmed;
  end
end
end