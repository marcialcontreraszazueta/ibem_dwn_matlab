% test BESJCMEX
clc;clear
cd '/Users/marshall/Documents/DOC/unit8/paraleloDelparalelo/pasarAmatlab'
load('fortest.mat','DWN')
DWN = DWN{1};
r = 150.0;

nkss = DWN.nkr;%length(DWN.kr);
J0 = zeros(nkss,3);
J1 = zeros(nkss,3);

for iiik = 1:nkss
  V = DWN.kr(iiik)*r+10.000000000000000000000001i;
  J0(iiik,1) = besselj(0,V);
  J1(iiik,1) = besselj(1,V);
  
  J01 = BESJCMEX(V);
  J0(iiik,2) = J01(1);
  J1(iiik,2) = J01(2);
  
  [J0(iiik,3),J1(iiik,3)]=BessJ01(V);
end
disp('done')
%%
figure;
subplot(4,1,1);hold on;title('real J0')
plot(real(J0(:,1)),'k-');plot(real(J0(:,2)),'r:');plot(real(J0(:,3)),'r--')
subplot(4,1,2);hold on;title('imag J0')
plot(imag(J0(:,1)),'k-');plot(imag(J0(:,2)),'b:');plot(imag(J0(:,3)),'b--')

subplot(4,1,3);hold on;title('real J1')
plot(real(J1(:,1)),'k-');plot(real(J1(:,2)),'r:');plot(real(J1(:,3)),'r--')
subplot(4,1,4);hold on;title('imag J1')
plot(imag(J1(:,1)),'k-');plot(imag(J1(:,2)),'b:');plot(imag(J1(:,3)),'b--')