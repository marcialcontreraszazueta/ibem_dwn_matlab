function GNIJ = Gij_3D_r_centroDisco(VN,... %receptor coord
  ksi,kpi,R)
%   CAKA,CAQA,R,BEALF,VN,G,EPS)
% tomado de VALL3D por FJSS.

BEALF = real(kpi/ksi);
GNIJ = zeros(3,3);% COMPLEX GNIJ(3,3)

BEA2=BEALF*BEALF;
BEA3=BEA2*BEALF;
BEA4=BEA2*BEA2;

%     GREEN FUNCTION (WITH ANALYTIC INTEGRATION)

% CAQR=kpi*R;
CAKR=ksi*R;
% AQR2=CAQR*CAQR;
AKR2=CAKR*CAKR;
% AQR3=AQR2*CAQR;
AKR3=AKR2*CAKR;
if (R == 0)%      IF(EPS.EQ.0.0)THEN
F1=1 - 1i*(2.0+BEA3)*CAKR/6.0 - (1.0+0.5*BEA4)*AKR2/9.0+1i*BEA2*BEA3*AKR3/24.0;
F1=F1/(pi*R);
F2=(1.0+BEA2)/2.0 -1i*(2.0+BEA3)*CAKR/6.0 -(2.0+BEA4)*AKR2/18.0 +1i*AKR3/24.0;
F2=F2/(pi*R);

for I=1:3
  for J=1:3
    AUX=(F2-F1)*VN(I)*VN(J);
    if (I == J)
      AUX=AUX+F1+F2;
    end
    GNIJ(I,J)=AUX/4.0;
  end
end
else
%        PG(1)=VN(2)*G(3)-VN(3)*G(2)
%        PG(2)=VN(3)*G(1)-VN(1)*G(3)
%        PG(3)=VN(1)*G(2)-VN(2)*G(1)
%        EPS2=EPS*EPS
%        EPS4=EPS2*EPS2
%        EPS6=EPS4*EPS2
%        C1=1.0-EPS2*0.25-EPS4*3./64.-EPS6*5./192.
%        C2=1.0
%        C3=1.0+EPS2*0.75-EPS4*3./64.-EPS6/256.
%        C4=1.0+EPS2*2.
%        F2C=UR*(1.+BEA2)/2.*C1-UI*(2.+BEA3)*CAKR/6.*C2 &
%            -UR*(2.+BEA4)*AKR2/18.*C3+UI*AKR3/24.*C4
%        F2C=F2C/(PI*R)
%        D1=1.0-EPS2*0.125-EPS4/64.-EPS6*5./1024.
%        D2=1.0+EPS2*0.5
%        D3=1.0+EPS2*21./8.-EPS4*21./64.-EPS6*43./1024.
%        D4=1.0+EPS2*4.
%        E1=1.0-EPS2*0.375-EPS4*5./64.-EPS6*35./1024.
%        E2=1.0-EPS2*0.5
%        E3=1.0+EPS2*1.875-EPS4*57./64.-EPS6*55./1024.
%        E4=1.0
%        F1D=UR*D1-UI*(2.+BEA3)*CAKR/6.*D2 &
%            -UR*(1.+.5*BEA4)*AKR2/9.*D3+UI*BEA2*BEA3*AKR3/24.*D4
%        F1D=F1D/(PI*R)
%        F2D=UR*(1.+BEA2)/2.*D1-UI*(2.+BEA3)*CAKR/6.*D2 &
%            -UR*(2.+BEA4)*AKR2/18.*D3+UI*AKR3/24.*D4
%        F2D=F2D/(PI*R)
% 
%        F1E=UR*E1-UI*(2.+BEA3)*CAKR/6.*E2 &
%            -UR*(1.+.5*BEA4)*AKR2/9.*E3+UI*BEA2*BEA3*AKR3/24.*E4
%        F1E=F1E/(PI*R)
%        F2E=UR*(1.+BEA2)/2.*E1-UI*(2.+BEA3)*CAKR/6.*E2 &
%           -UR*(2.+BEA4)*AKR2/18.*E3+UI*AKR3/24.*E4
%        F2E=F2E/(PI*R)
% 
%        DO 200 I=1,3
%          DO 190 J=1,3
%            AUX=(F1D-F2D)*G(I)*G(J)+(F1E-F2E)*PG(I)*PG(J)
%            IF(I.EQ.J)AUX=AUX+F2C*2.0
%            GNIJ(I,J)=AUX/4.0
%   190    CONTINUE
%   200  CONTINUE
%       END IF
%       RETURN
end
end