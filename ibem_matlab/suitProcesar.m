% suit 
% procesar resultados

%% cargar resultado 
clear
% load('../out/2.000hz.mat')

load('/Users/marshall/Documents/DOC/ibem_dwn_matlab/out/gen3D_FP_xs-5_zs0.1_npplo6_DWN_20161216T134954.mat')

disp('loaded')
%% H/V uno
j = 101;
res = 9;
gij = squeeze(uw(j,res,[res*3,res*3+1,res*3+2],1:3)).';
disp(gij)
%% linea de receptores una frecuencia
thisfrec = 300;
nr = 202;

xax = linspace(para.rec.xr(1),para.rec.xr(nr/2),nr/2);
yax = linspace(para.rec.yr(nr/2+1),para.rec.yr(end),nr/2);

dxx = xax(2)-xax(1); dkx = 2*pi/(dxx*nr/2); %kxax = linspace(-dkx*(nr/4-1),dkx*(nr/4),nr/2);
dxy = yax(2)-yax(1); dky = 2*pi/(dxy*nr/2); %kyax = linspace(-dky*(nr/4-1),dky*(nr/4),nr/2);
for iinc = 1:para.ninc
% figure(10+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' x/a'])
% figure(20+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' y/a'])
% figure(110+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' x/a'])
% figure(120+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' y/a'])
for j=thisfrec

% x/a
figure(10+iinc);hold on
ran = 1:nr/2;

subplot(3,1,1); hold on
plot(xax,squeeze(abs(uw(j+1,ran,iinc,1))),'k-','DisplayName',['u_xa_ESTReta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(xax,squeeze(abs(uw(j+1,ran,iinc,2))),'k-','DisplayName',['v_xa_ESTReta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(xax,squeeze(abs(uw(j+1,ran,iinc,3))),'k-','DisplayName',['w_xa_ESTReta ' num2str(j*2)])
xlabel('x/a');ylabel('| U_z | ')

% y/a
figure(20+iinc);hold on
ran = nr/2+1:nr;

subplot(3,1,1); hold on
plot(yax,squeeze(abs(uw(j+1,ran,iinc,1))),'k-','DisplayName',['u_ya_ESTReta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_x | ')
subplot(3,1,2); hold on
plot(yax,squeeze(abs(uw(j+1,ran,iinc,2))),'k-','DisplayName',['v_ya_ESTReta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_y | ')
subplot(3,1,3); hold on
plot(yax,squeeze(abs(uw(j+1,ran,iinc,3))),'k-','DisplayName',['w_ya_ESTReta ' num2str(j*2)])
xlabel('y/a');ylabel('| U_z | ')

end
end

%% fk
for iinc = 1:para.ninc
% figure(10+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' x/a'])
% figure(20+iinc);clf;set(gcf,'name',['Fuente ' num2str(iinc) ' y/a'])
% figure(110+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' x/a'])
% figure(120+iinc);clf;set(gcf,'name',['fk Fuente ' num2str(iinc) ' y/a'])
for j=2:4
figure(110+iinc);hold on
subplot(3,1,1); hold on
fk = fftshift(R.uw(j+1,ran,iinc,1)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['u_a_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(real(fk)),'r-','DisplayName',['u_r_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(imag(fk)),'b-','DisplayName',['u_i_kx_ESTReta ' num2str(j*2)])
xlabel('kx');ylabel('| U_x(k,\omega) | ')
subplot(3,1,2); hold on
fk = fftshift(R.uw(j+1,ran,iinc,2)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['v_a_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(real(fk)),'r-','DisplayName',['v_r_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(imag(fk)),'b-','DisplayName',['v_i_kx_ESTReta ' num2str(j*2)])
xlabel('kx');ylabel('| U_y(k,\omega) | ')
subplot(3,1,3); hold on
fk = fftshift(R.uw(j+1,ran,iinc,3)*dxx); %forward
plot(kxax,squeeze(abs(fk)),'k-','DisplayName',['w_a_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(real(fk)),'r-','DisplayName',['w_r_kx_ESTReta ' num2str(j*2)])
% plot(kxax,squeeze(imag(fk)),'b-','DisplayName',['w_i_kx_ESTReta ' num2str(j*2)])
xlabel('kx');ylabel('| U_z(k,\omega) | ')

end
end


%% funcion de amplitud
% para.pulso.tipo=3; %Ricker periodo caracter?stico tp
% para.pulso.a = 2;   % tp
% para.pulso.b = 6;    % ts
% para.pulso.c = 0;  % corrimiento
% 
% para.pulso.tipo=5; % gaussiano rise time
% para.pulso.a = 2;   % rise time 
% para.pulso.b = 5;   % retraso

para.pulso.tipo=4; % plano + tapper gaussiano
para.pulso.a = 3;   % rise time
para.pulso.b = 5;    % retraso
para.pulso.c = 0.8;  % corrimiento

% para.pulso.tipo=6; % butterworth
% para.pulso.a = 4; % n
% FREQB=2; %cutoff lowpass filter
% df      = para.fmax/(para.nf/2);
% dt = 1/(df*para.zeropad);
% para.pulso.b = FREQB/(1/(2*dt)); % Wn
% para.pulso.c = 1.2;  % corrimiento

[Fq,cspectre,tps,~] = showPulso( para , true);
% % %% expandir
% % nf = 2048;
% % disp('expandir de')
% % size(Fq)
% % disp('puntos a')
% % Fqs=linspace(0,Fq(end),nf/2+1);
% % size(Fqs)
% % uws=zeros(size(Fqs,2),size(uw,2),size(uw,3),size(uw,4));
% % 
% % for ir = 1:size(uw,2)
% % for iinc = 1:size(uw,3)
% % for icomp = 1:size(uw,4)
% % uws(:,ir,iinc,icomp)=interp1(Fq,squeeze(uw(:,ir,iinc,icomp)),Fqs,'spline');
% % end
% % end
% % end
% % disp('done')
% % 
% % uw=uws;
% % para.nf = nf;
% % [Fq,cspectre,tps,~] = showPulso( para , false);
%% inversion
[utc,~]   = inversion_w(uw,sw,para);

% graficas
% nf      = para.nf;
% df      = para.fmax/(para.nf/2);     %paso en frecuencia
% Fq      = (0:nf/2)*df;
% nfN     = nf/2+1; %Nyquist
% tps     = 0:(1/(df*2*(nfN+zerospad))*(2*(nfN+zerospad)/(2*(nfN+zerospad)-2))):1/df;
% dt = 1/(df*para.zeropad);    %disp(['dt = ',num2str(dt)])
% tps = (0:para.zeropad-1)*dt; %disp(['tmx= ',num2str(tps(end))])
tran = 1:2100;
iinc = 1;
%% un receptor frecuenia y tiempo
recep = 31;
figure(9800);
tx = [num2str(recep),':(',num2str(para.rec.xr(recep)),' , ',...
  num2str(para.rec.yr(recep)),' , ',num2str(para.rec.zr(recep)),')'];
arnam{1} = ['u_',tx]; arnam{2} = ['v_',tx]; arnam{3} = ['w_',tx];
subplot(2,1,1); hold on
for icomp = 1:3
  plot(Fq,abs(uw(:,recep,iinc,icomp).'.*cspectre),'k-','DisplayName',arnam{icomp});
  plot(Fq,real(uw(:,recep,iinc,icomp).'.*cspectre),'r-','DisplayName',arnam{icomp});
  plot(Fq,imag(uw(:,recep,iinc,icomp).'.*cspectre),'b-','DisplayName',arnam{icomp});
  plot(Fq,abs(uw(:,recep,iinc,icomp)),'k.','DisplayName',arnam{icomp});
end
title('espectros');xlabel('frecuencia [Hz]');ylabel('amplitud')
subplot(2,1,2); hold on
for icomp = 1:3
  plot(tps(tran),utc(tran,recep,iinc,icomp),'k-','DisplayName',arnam{icomp});
end
title('sismogramas sinteticos');xlabel('tiempo [segundos]');ylabel('amplitud')
%% sabana resultados en  x
ran = 1:61;
para.rec.dxr = para.rec.xr(ran(2))-para.rec.xr(ran(1));
para.rec.dyr = para.rec.yr(ran(2))-para.rec.yr(ran(1));

for icomp = 1:3
figure(9800+icomp); clf; axes; hold on
sca = 3 * para.rec.dxr / max(max(utc(tran,:,iinc,icomp)));
for ir = ran
  plot(tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ran(ir)),'k-');
%   plotwiggle(gca,tps(tran),utc(tran,ir,iinc,icomp)*sca+para.rec.xr(ir));
end
xlim([0 15])
end