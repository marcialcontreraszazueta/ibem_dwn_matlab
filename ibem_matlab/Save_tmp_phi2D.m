function Save_tmp_phi2D(phi,coord,DWN,WFE,j) % %#ok<INUSD>

name = '../out/PHI';
if j < 10
save([name,'_00',num2str(j),'tmp.mat'],'phi','coord','DWN','WFE');  
else
  if j < 100
 save([name,'_0',num2str(j),'tmp.mat'],'phi','coord','DWN','WFE');
  else
  save([name,'_',num2str(j),'tmp.mat'],'phi','coord','DWN','WFE');    
  end
end