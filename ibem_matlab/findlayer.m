function pcXout = findlayer(pcX,ncapas,z)
pcXout = ones(4,size(pcX,2)); pcXout(1:3,:) = pcX(1:3,:);
% va preguntando si la z es mayor que la profundidad de la interfaz de
% arriba de los estratos de abajo hacia arriba.
for ic = ncapas:-1:2
  pcXout(4,pcX(3,:)>z(ic)) = ic;
  pcX(3,pcX(3,:)>z(ic)) = -1000;
end