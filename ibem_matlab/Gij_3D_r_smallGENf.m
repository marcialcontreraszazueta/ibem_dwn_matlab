function [Gij] = Gij_3D_r_smallGENf(xr,... %receptor coord
        CubPts,CubWts,... % coordenadas y pesos de la cubatura (fuente)
        ks,kp,C)
% Una fuente un receptor.  
% [(u v w) x (fx fy fz)]
% El receptor muy cerca de la fuente.
% La fuente distribuida en un segmento triangular.

Gij= complex(zeros(3,3,1,1));        % El resultado

ngau = length(CubWts);
% cosenos directores entre el receptor y las fuentes
  xij = xr(1)-CubPts(1,1:ngau);
  yij = xr(2)-CubPts(2,1:ngau);
  zij = xr(3)-CubPts(3,1:ngau);
  rij = sqrt(xij.^2+yij.^2+zij.^2);
  g = zeros(3,ngau);
  g(1,1:ngau)  = xij./rij;
  g(2,1:ngau)  = yij./rij;
  g(3,1:ngau)  = zij./rij;
  
  %% Funcion de Green fuente puntual:
  grij = complex(zeros(3,3,ngau)); %#ok
  grij = Gij_3D(ks,kp,rij,g,C,ngau); %% [(fx fy fz) x (u v w)]
  % lo hacemos suponiendo que el receptor no esta sobre algun punto de la
  % cubatura (singularidad)
  for i=1:3
    for j=1:3
      Gij(j,i,1,1)	= sum(CubWts.*squeeze(grij(i,j,:)));% [(u v w) x (fx fy fz)]
    end
  end
end