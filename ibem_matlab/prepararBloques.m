function [BLOCKS,pc,pT] = prepararBloques(para)
%% vector de coords de puntos de colocacion y segmentos fuente
coord = para.coord;
nbeq = coord.nbeq;
nmed = para.nmed;
j       = 1:coord.nbpt;
if isfield(para,'cubature')
ngau = size(para.cubature,1);
else
ngau = 0;
end
pc = struct('X',zeros(3,nbeq),'Vn',zeros(3,nbeq),'m',zeros(2,nbeq),'Xim',zeros(nmed,nbeq));
pT = struct('Xi',zeros(3,nbeq),'dA',zeros(1,nbeq),'CubPts',zeros(3,ngau,nbeq),'m',zeros(1,nbeq));
for i=j
  pc.X(:,i) = [coord.x(i);coord.y(i);coord.z(i)];
  pc.Vn(:,i) = [coord.vnx(i);coord.vny(i);coord.vnz(i)];
  % En Xim cada columna es un medio y cada renglOn un triAngulo.
  % Cada triAngulo puede estar asociado hasta con dos medios.
  % im es el Indice de la(s) columnas que no son zero, es decir
  % el indice del o de los medios asociados a la fuente virtual.
  im  = find(coord.Xim(i,:)~=0);
  pc.m(1,i) = im(1); if length(im)==2; pc.m(2,i) = im(2); end %medio(s) a los que pertence
  %min de los indice de los medios en contacto que se lleva el signo +
  im0 = min(im); % (multi-ibem)
  pc.im0(:,i) = im0;
  pc.Xim(:,i) = squeeze(coord.Xim(i,:)); %contorno de arriba o abajo (multiIBEM)
  
  pT.Xi(:,coord.phi(i,im(1))) = [coord.x(i);coord.y(i);coord.z(i)];
  pT.dA(1,coord.phi(i,im(1))) = coord.dA(i);
  pT.CubPts(:,:,coord.phi(i,im(1))) = coord.CubPts(1:3,1:ngau,i);
  pT.m(1,coord.phi(i,im(1))) = im(1);
  
  if length(im)==2
    pT.Xi(:,coord.phi(i,im(2))) = [coord.x(i);coord.y(i);coord.z(i)];
    pT.dA(1,coord.phi(i,im(2))) = coord.dA(i);
    pT.CubPts(:,:,coord.phi(i,im(2))) = coord.CubPts(1:3,1:ngau,i);
    pT.m(1,coord.phi(i,im(2))) = im(2);
  end
  if sum(coord.Xim(i,:)~=0)==2
     pc.X(:,coord.nbpt+coord.ju(i)) = [coord.x(i);coord.y(i);coord.z(i)];
     pc.Vn(:,coord.nbpt+coord.ju(i)) = [coord.vnx(i);coord.vny(i);coord.vnz(i)];
     pc.m(1,coord.nbpt+coord.ju(i)) = im(1); 
     if length(im)==2; pc.m(2,coord.nbpt+coord.ju(i)) = im(2); end 
     pc.im0(:,coord.nbpt+coord.ju(i)) = im0;
     pc.Xim(:,coord.nbpt+coord.ju(i)) = squeeze(coord.Xim(i,:)); 
  end
end
%% Identificar bloques de la matriz A
iC = 1;
macrocol = [];
macrocol{iC}.m = pT.m(1);
macrocol{iC}.ci = 1;
for i=2:coord.nbeq
  if pT.m(i) ~= pT.m(i-1)
    % nueva marcrocolumna
    macrocol{iC}.cf = i-1;
    iC = iC +1;
    macrocol{iC}.m = pT.m(i);
    macrocol{iC}.ci = i;
  end
end
macrocol{iC}.cf = coord.nbeq;
% Bloques de tracciones:
% averiguar el tamaNo
ib = 0;
for mc = macrocol
  nadaAntes = true;
  %   disp(mc{1})
  for i=1:coord.nbpt
    % Un bloque de la matriz si punto de colocaciOn y fuente en mismo medio
    if sum(pc.m(:,i) == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
      end
    else %en un bloque de vacIo
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end

blockT = cell(1,ib);
ib = 0;
for mc = macrocol
  nadaAntes = true;
  %   disp(mc{1})
  for i=1:coord.nbpt
    % Un bloque de la matriz si punto de colocaciOn y fuente en mismo medio
    if sum(pc.m(:,i) == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
        blockT{ib}.m = mc{1}.m;
        blockT{ib}.im0 = pc.im0(:,i);
        blockT{ib}.ci = mc{1}.ci;
        blockT{ib}.cf = mc{1}.cf;
        blockT{ib}.tipo = 'T';
        blockT{ib}.ri = i;
        blockT{ib}.rf = i;
      else
        blockT{ib}.rf = i;
      end
    else %en un bloque de vacIo
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end

% bloques de Desplazamientos
% averiguar tamaNo
ib = 0;
for mc = macrocol
  nadaAntes = true;
  %   disp(mc{1})
  for i=1+coord.nbpt:coord.nbeq
    if sum(pc.m(:,i) == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
      end
    else %en un bloque vac�o
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end
blockG = cell(1,ib);
ib = 0;
for mc = macrocol
  nadaAntes = true;
  %   disp(mc{1})
  for i=1+coord.nbpt:coord.nbeq
    if sum(pc.m(:,i) == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
        blockG{ib}.m = mc{1}.m;
        blockG{ib}.im0 = pc.im0(:,i);
        blockG{ib}.ci = mc{1}.ci;
        blockG{ib}.cf = mc{1}.cf;
        blockG{ib}.tipo = 'G';
        blockG{ib}.ri = i;
        blockG{ib}.rf = i;
      else
        blockG{ib}.rf = i;
      end
    else %en un bloque vac�o
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end

for ibT = 1:length(blockT)
  blockT{ibT}.calcme = true;
  blockT{ibT}.relatedG = 0;
  blockT{ibT}.relatedT = 0;
  % para cada blockT comparar los m,im0,ci,cf con los blockG
  for ibG = 1:length(blockG)
    blockG{ibG}.calcme = true;
    blockG{ibG}.relatedG = 0;
    blockG{ibG}.relatedT = 0;
  end
end

% encontrar relaciones entre blockT y blockG
for ibT = 1:length(blockT)
  % para cada blockT comparar los m,im0,ci,cf con los blockG
  for ibG = 1:length(blockG)
    % solo vale la pena cuando usa DWN
    if para.tipoMed(blockG{ibG}.m) ~= 2
      continue
    end
    
    if ((blockT{ibT}.m == blockG{ibG}.m) && ...
        (blockT{ibT}.im0 == blockG{ibG}.im0))
      if ((blockT{ibT}.ci == blockG{ibG}.ci) && ...
          (blockT{ibT}.cf == blockG{ibG}.cf))
        % identificar el rango de renglones
        renT = blockT{ibT}.ri : blockT{ibT}.rf;
        renG = blockG{ibG}.ri : blockG{ibG}.rf;
        ia = renT(1); iz = renT(end);
        for iinter = renT
          if sum(pc.X(:,iinter) == pc.X(:,renG(1))) == 3
            %inicial
            ia = iinter;
            % buscar final
            for iiinter = renT(end):-1:ia+1
              if sum(pc.X(:,iiinter) == pc.X(:,renG(end))) == 3
                iz = iiinter;
                break
              end
            end
            break
          end
        end
        
        blockT{ibT}.relatedG = length(blockT) + ibG;
        blockG{ibG}.relatedT = ibT;
        blockG{ibG}.calcme = false; % no calc. solito / recibir de ibT
        blockG{ibG}.relTri = ia;
        blockG{ibG}.relTrf = iz;
      end
    end
  end
end

BLOCKS = [blockT blockG]; clear blockT blockG

% Identificar reciprocidades en el bloque mismo
for ib = 1:length(BLOCKS)
%   disp(['checkRecip [' num2str(ib) '/' num2str(length(BLOCKS)) ']'])
  % primero averiguamos si el bloque es recIproco consigo mismo
  %   comparamos las coord de los pc y pT en el bloque
  thisBpc = BLOCKS{ib}.ri:BLOCKS{ib}.rf;
  thisBpT = BLOCKS{ib}.ci:BLOCKS{ib}.cf;
  BLOCKS{ib}.isSelfReciproc = false;
  if length(thisBpc) == length(thisBpT)
    jjl = 0;
    for ijll = [1 length(thisBpc)] %primer y ultimo elementos
      jjl = jjl + double(...
        pc.X(:,thisBpc(ijll)) == pT.Xi(:,thisBpT(ijll)));
    end
    if jjl == 2
      % Es cuadrado y el primero y el Ultimo corresponden asI que
      BLOCKS{ib}.isSelfReciproc = true;
    end
  else % no es cuadrado
%     if BLOCKS{ib}.calcme == false
%       continue
%     end
    % averiguamos si parte del rectAngulo es autoreciproco y lo separamos.
    % Encontrar primera esquina del bloque autoreciproco
    parteAutorecip = [];
    encontrado_primer_esquina = false;
    for ir = thisBpc % cada renglOn
      thispc = pc.X(:,ir);
      if encontrado_primer_esquina == false
        for ic = thisBpT % vs cada columna
          if sum(thispc == pT.Xi(:,ic))==3
            % match!
            encontrado_primer_esquina = true;
            parteAutorecip.ci = ic;
            parteAutorecip.ri = ir;
            break
          end
        end
      else % segunda esquina
        ic = ic + 1; %seguir la diagonal
        if sum(thispc == pT.Xi(:,ic))==3
          parteAutorecip.cf = ic;
          parteAutorecip.rf = ir;
        else
          break % se terminO el bloque
        end
      end
    end
    %----
%     
%     
%     yaencontroPrimerRenglon = false;
%     parteAutorecip = [];
%     for ir = thisBpc % cada renglOn
%       if yaencontroPrimerRenglon == false;
%         nadaAntes = true;
%         thispc = pc.X(:,ir);
%         colcol = thisBpT;
%         for ic = colcol
%           %buscar columna inicial de la parte autoreciproca
%           if sum(thispc == pT.Xi(:,ic))==3
%             if nadaAntes
%               nadaAntes = false;
%               yaencontroPrimerRenglon = true;
%               parteAutorecip.ci = ic;
%               parteAutorecip.cf = ic;
%               parteAutorecip.ri = ir;
%               parteAutorecip.rf = ir;
%               break
%             else
%               parteAutorecip.cf = ic;
%               parteAutorecip.rf = ir;
%             end
%           end
%         end
%       else
%         % yaencontroPrimerRenglon == true
%         % Entonces solo coparar los extremos
%         thispc = pc.X(:,ir); %inicial
%         ic = parteAutorecip.ci;
%         
%       end
%       % para no buscar deeeesde el principio:
% %       if nadaAntes
%         colcol = thisBpT;
% %       else
% %         colcol = thisBpT(find(thisBpT-parteAutorecip.cf,1):end);
% %       end
%       for ic = colcol
%         if sum(thispc == pT.Xi(:,ic))==3
%           %esta parte es autoreciproca
%           if nadaAntes
%             nadaAntes = false;
%             parteAutorecip.ci = ic;
%             parteAutorecip.cf = ic;
%             parteAutorecip.ri = ir;
%             parteAutorecip.rf = ir;
%             break %no se espera encontrar este punto de nuevo
%           else
%             parteAutorecip.cf = ic;
%             parteAutorecip.rf = ir;
%           end
%         end
%       end
%     end
    if ~isempty(parteAutorecip)
      % Partimos en dos el bloque ib: separamos la parte autoreciproca
      % Lo que no es autoreciproco, en un nuevo bloque
      ibNuevo = length(BLOCKS)+1;
      BLOCKS{ibNuevo}.m = BLOCKS{ib}.m;
      BLOCKS{ibNuevo}.im0 = BLOCKS{ib}.im0;
      orig = BLOCKS{ib}.ci:BLOCKS{ib}.cf;
      restar = parteAutorecip.ci:parteAutorecip.cf;
      for iresta = restar
        orig(orig == iresta) = [];
      end
      if isempty(orig)
        %no restO nada
        orig(1) = BLOCKS{ib}.ci;
        orig(2) = BLOCKS{ib}.cf;
      end
      BLOCKS{ibNuevo}.ci = orig(1);
      BLOCKS{ibNuevo}.cf = orig(end);
      orig = BLOCKS{ib}.ri:BLOCKS{ib}.rf;
      restar = parteAutorecip.ri:parteAutorecip.rf;
      for iresta = restar
        orig(orig == iresta) = [];
      end
      if isempty(orig)
        %no restOnada
        orig(1) = BLOCKS{ib}.ri;
        orig(2) = BLOCKS{ib}.rf;
      end
      BLOCKS{ibNuevo}.tipo = BLOCKS{ib}.tipo;
      BLOCKS{ibNuevo}.ri = orig(1);
      BLOCKS{ibNuevo}.rf = orig(end);
      BLOCKS{ibNuevo}.calcme = true;
      BLOCKS{ibNuevo}.relatedG = 0;
      BLOCKS{ibNuevo}.relatedT = 0;
      BLOCKS{ibNuevo}.isSelfReciproc = false;
      % Lo que resta es sOlo la parte autorecip:
      BLOCKS{ib}.ci = parteAutorecip.ci;
      BLOCKS{ib}.cf = parteAutorecip.cf;
      BLOCKS{ib}.ri = parteAutorecip.ri;
      BLOCKS{ib}.rf = parteAutorecip.rf;
      BLOCKS{ib}.isSelfReciproc = true;
    end
  end
end
%% Se identifica entre que bloques con isSelfReciproc = false hay reciprocidad
for ib = 1:length(BLOCKS)     %  0  bloque autoreciproco o aislado (no devuelve AbR)
  BLOCKS{ib}.isrelatedTo = 0; %  br calcular el bloque completo y el conjugado asignalor a br (devuelve AbR)
end                           %  -1 no calcular, recibe el reciproco de algun otro bloque.
% TODO:

% for ib = 1:length(BLOCKS)
%   if BLOCKS{ib}.isSelfReciproc == false
%     for ibInq = ib+1:length(BLOCKS)
%       if BLOCKS{ib}.tipo == BLOCKS{ibInq}.tipo && ... %Son del mismo tipo y
%          BLOCKS{ib}.m == BLOCKS{ibInq}.m && ... %Son del mismo medio
%          BLOCKS{ib}.isrelatedTo == 0; %No es el recIproco de nadie mAs
%         % Averiguar si son recIprocos entre bloques
%         disp('Esta parte todavia no esta completa');stop
%         thisBpc = BLOCKS{ib}.ri:BLOCKS{ib}.rf;
%         thisBpT = BLOCKS{ibInq}.ci:BLOCKS{ibInq}.cf;
%         nadaAntes = true;
%         parteRecipEntreBloq = [];
%         for ir = thisBpc
%           thispc = pc.X(:,ir);%[pc{ir}.x pc{ir}.y pc{ir}.z];
%           % para no buscar deeeesde el pricipio:
%           if nadaAntes
%             colcol = thisBpT;
%           else
%             colcol = thisBpT(find(thisBpT-parteRecipEntreBloq.cf,1):end);
%           end
%           for ic = colcol
%             if sum(thispc == pT.Xi(:,ic))==3%[pT{ic}.x pT{ic}.y pT{ic}.z])==3
%               %esta parte es autoreciproca
%               if nadaAntes
%                 nadaAntes = false;
%                 parteRecipEntreBloq.ci = ic;
%                 parteRecipEntreBloq.cf = ic;
%                 parteRecipEntreBloq.ri = ir;
%                 parteRecipEntreBloq.rf = ir;
%                 break %no se espera encontrar este punto de nuevo
%               else
%                 parteRecipEntreBloq.cf = ic;
%                 parteRecipEntreBloq.rf = ir;
%               end
%             end
%           end
%         end
%       end
%     end
%   end
% end

%% Ver los bloques en la matriz
if para.GraficarCadaDiscretizacion
  colorVec =['r' 'g' 'b' 'c' 'y' 'm' 'k']; icV=1;
  figure(544); clf; hold on;set(gcf,'name','Bloques')
  nbeq = para.coord.nbeq;
  A = sparse(3*nbeq,3*nbeq);
  for ib = BLOCKS
    A = A*0;
    A(ib{1}.ri:ib{1}.rf,ib{1}.ci:ib{1}.cf) = 1;
    spy(A,colorVec(icV)); icV = 1 + icV; if icV == 8; icV = 1;end
  end
  
  nBLO = length(BLOCKS); disp(['nBLO=' num2str(nBLO) 'matriz de ' num2str(3*nbeq) '^2'])
end
end