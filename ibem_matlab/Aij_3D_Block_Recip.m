function [A,pT]=Aij_3D_Block_Recip(para,DWN,GraficarCadaDiscretizacion)
[BLOCKS,pc,pT] = prepararBloques(para);
%% Materiales en una variables compactas
mat = cell(para.nmed,1);
for m = 1:para.nmed
  mat{m}.tipoMed = para.tipoMed(m);
  if para.tipoMed(m) == 1
    mat{m}.Ci = para.reg(m).Ci;
    mat{m}.ksi = para.reg(m).ksi;
    mat{m}.kpi = para.reg(m).kpi;
  end
end

%% Agrupar variable DWN
if para.nmed ~= length(DWN)
  DWN{para.nmed} = [];
end
fields = {'U0','S0','inds','m','Udiffsize','Sdiffsize',... %,'dkr'
  'xr','yr','zr','zr0','izr0','zricrall','icrall','salu','sals','nxrv'};
for i=1:length(DWN)
  if ~isempty(DWN{i})
    if length(fieldnames(DWN{i})) > 1
      DWN{i} = rmfield(DWN{i},fields);
    else
      DWN{i} = 0;
    end
  else
    DWN{i} = 0;
  end
end
ngau = size(para.cubature,1);
CubWts = para.cubature(1:ngau,3);
nBLO = length(BLOCKS);

%% corregir .calcme
for ib = 1:nBLO
  if para.tipoMed(BLOCKS{ib}.m) == 1
    % medio homogeneo en este bloque
     BLOCKS{ib}.calcme = true; % siempre se calcula
     BLOCKS{ib}.relatedG = 0; % G y T se calculan por separado
     BLOCKS{ib}.DWN = [];
  else
    % medio estratificado en este bloque
    if BLOCKS{ib}.calcme == true
      BLOCKS{ib}.DWN = DWN{BLOCKS{ib}.m};
    end
  end
end

%% Hacer el cAlculo de cada bloque de funciones de Green
% llenar la matriz con los bloques
nbeq = para.coord.nbeq;
if 3*nbeq > 3000 %8000
    %A = zeros(3*nbeq,3*nbeq,'distributed'); %si no se distribuye, no cabe
    A = sparse(3*nbeq,3*nbeq); % asignar memoria dinamicamente
else
    A = zeros(3*nbeq,3*nbeq);
end
  
st = para.str;
fprintf('%s',[st ' nBLO[' num2str(nBLO) ']'])
%   parfor (ib = 1:nBLO,2) # quitamos parfor porque el problema es la
%   cantidad de memoria, no tanto la cantidad de tiempo disponible.
  for ib = 1:nBLO
    disp([datestr(clock) BLOCKS{ib}.tipo num2str(BLOCKS{ib}.ci) '-' num2str(BLOCKS{ib}.cf) 'x' num2str(BLOCKS{ib}.ri) '-' num2str(BLOCKS{ib}.rf)])
    % BLOCKS se convierte en una variable rebanada (sliced)
    if BLOCKS{ib}.calcme == true
      str = [st ' @B' num2str(ib)]; % disp(BLOCKS{ib})
      BLOCKS{ib} = Fill_TG_block(BLOCKS{ib},pc,pT,mat,CubWts,str,GraficarCadaDiscretizacion);
      fprintf('%s','.')

      rens = BLOCKS{ib}.ri   : BLOCKS{ib}.rf;
      cols = BLOCKS{ib}.ci   : BLOCKS{ib}.cf;
      
      A(rens       ,cols       ) = squeeze(BLOCKS{ib}.Ab(1,1,:,:)); %t_x1
      BLOCKS{ib}.Ab(1,1,:,:) = 0;
      A(rens       ,cols+nbeq  ) = squeeze(BLOCKS{ib}.Ab(1,2,:,:)); %t_x2
      BLOCKS{ib}.Ab(1,2,:,:) = 0;
      A(rens       ,cols+nbeq*2) = squeeze(BLOCKS{ib}.Ab(1,3,:,:)); %t_x3
      BLOCKS{ib}.Ab(1,3,:,:) = 0;
      A(rens+nbeq  ,cols       ) = squeeze(BLOCKS{ib}.Ab(2,1,:,:)); %t_y1
      BLOCKS{ib}.Ab(2,1,:,:) = 0;
      A(rens+nbeq  ,cols+nbeq  ) = squeeze(BLOCKS{ib}.Ab(2,2,:,:)); %t_y2
      BLOCKS{ib}.Ab(2,2,:,:) = 0;
      A(rens+nbeq  ,cols+nbeq*2) = squeeze(BLOCKS{ib}.Ab(2,3,:,:)); %t_y3
      BLOCKS{ib}.Ab(2,3,:,:) = 0;
      A(rens+nbeq*2,cols       ) = squeeze(BLOCKS{ib}.Ab(3,1,:,:)); %t_z1
      BLOCKS{ib}.Ab(3,1,:,:) = 0;
      A(rens+nbeq*2,cols+nbeq  ) = squeeze(BLOCKS{ib}.Ab(3,2,:,:)); %t_z2
      BLOCKS{ib}.Ab(3,2,:,:) = 0;
      A(rens+nbeq*2,cols+nbeq*2) = squeeze(BLOCKS{ib}.Ab(3,3,:,:)); %t_z3
      BLOCKS{ib}.Ab(3,3,:,:) = 0;
      BLOCKS{ib}.Ab=0;
       
      if BLOCKS{ib}.relatedG ~= 0
        rel = BLOCKS{ib}.relatedG;
        rens = BLOCKS{rel}.ri   : BLOCKS{rel}.rf;
        cols = BLOCKS{rel}.ci   : BLOCKS{rel}.cf;
        
        A(rens       ,cols       ) = squeeze(BLOCKS{ib}.AbG(1,1,:,:)); %g_x1
        BLOCKS{ib}.AbG(1,1,:,:) = 0;
        A(rens       ,cols+nbeq  ) = squeeze(BLOCKS{ib}.AbG(1,2,:,:)); %g_x2
        BLOCKS{ib}.AbG(1,2,:,:) = 0;
        A(rens       ,cols+nbeq*2) = squeeze(BLOCKS{ib}.AbG(1,3,:,:)); %g_x3
        BLOCKS{ib}.AbG(1,3,:,:) = 0;
        A(rens+nbeq  ,cols       ) = squeeze(BLOCKS{ib}.AbG(2,1,:,:)); %g_y1
        BLOCKS{ib}.AbG(2,1,:,:) = 0;
        A(rens+nbeq  ,cols+nbeq  ) = squeeze(BLOCKS{ib}.AbG(2,2,:,:)); %g_y2
        BLOCKS{ib}.AbG(2,2,:,:) = 0;
        A(rens+nbeq  ,cols+nbeq*2) = squeeze(BLOCKS{ib}.AbG(2,3,:,:)); %g_y3
        BLOCKS{ib}.AbG(2,3,:,:) = 0;
        A(rens+nbeq*2,cols       ) = squeeze(BLOCKS{ib}.AbG(3,1,:,:)); %g_z1
        BLOCKS{ib}.AbG(3,1,:,:) = 0;
        A(rens+nbeq*2,cols+nbeq  ) = squeeze(BLOCKS{ib}.AbG(3,2,:,:)); %g_z2
        BLOCKS{ib}.AbG(3,2,:,:) = 0;
        A(rens+nbeq*2,cols+nbeq*2) = squeeze(BLOCKS{ib}.AbG(3,3,:,:)); %g_z3
        BLOCKS{ib}.AbG(3,3,:,:)=0;
        BLOCKS{ib}.AbG=0;
      end
    end
  end

%% Test me
if GraficarCadaDiscretizacion
disp('obtained A')
    
% % A signos spy
 figure; clf; 
 aa = real(A); aa(aa>=0) = 0; 
 spy(aa,'y'); hold on
 aa = real(A); aa(aa<=0) = 0; 
 spy(aa,'b');
i = size(A,1)/3;
n = size(A,1);
hold on
line([i i],[1 n],'Color',[0.2 0.2 0.2])
line([2*i 2*i],[1 n],'Color',[0.2 0.2 0.2])
line([1 n],[i i],'Color',[0.2 0.2 0.2])
line([1 n],[2*i 2*i],'Color',[0.2 0.2 0.2])
 title('negativo(amarillo) y positivo(azul)')
% 
% % surf
% figure(54212);clf;surf(real(A),'EdgeColor','none');view(2)
% set(gcf,'name','Matriz A');set(gca,'Ydir','reverse');axis square
% % zlim([0.001 0.5])
% 
% % spy
figure(2255);clf;spy(abs(A)); hold on
[aA,bA] = find(A == 0.5);
plot(bA,aA,'r.')

[aA,bA] = find(A == -0.5);
plot(bA,aA,'b.')

i = size(A,1)/3;
n = size(A,1);
hold on
line([i i],[1 n],'Color',[0.2 0.2 0.2])
line([2*i 2*i],[1 n],'Color',[0.2 0.2 0.2])
line([1 n],[i i],'Color',[0.2 0.2 0.2])
line([1 n],[2*i 2*i],'Color',[0.2 0.2 0.2])
 title('0.5:rojo  ,,,, -0.5:azul')
end
end