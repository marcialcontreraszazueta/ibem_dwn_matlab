med     =get(bouton.med,'value');
set(bouton.xa     ,'string',para.cont(med,1).xa);
set(bouton.za     ,'string',para.cont(med,1).za);
set(bouton.a      ,'string',para.cont(med,1).a);
set(bouton.th     ,'string',para.cont(med,1).th);
if med>1
  icont   = get(bouton.cont,'value');
else
  icont   = 1;
end
set(bouton.contgeo,'value' ,para.cont(med,icont).geom);
set(bouton.base   ,'string',para.cont(med,icont).ba);
set(bouton.haut   ,'string',para.cont(med,icont).h);
set(bouton.ruggeo ,'value' ,para.cont(med,icont).ruggeo);
set(bouton.rugbase,'string',para.cont(med,icont).rba);
set(bouton.rughaut,'string',para.cont(med,icont).rh);
info.ThisPiece = 1;
set(bouton.gfThisPiece,'value',1);
if para.dim == 4
  if para.geo(med) == 4 % tiene fronteras
    set(bouton.gfNumPieces,'string',num2str(para.cont(med,1).NumPieces));
    nom = para.cont(med,1).piece{info.ThisPiece}.fileName;
    
    set(bouton.geoFileSelect,'string',nom);
    set(bouton.gfThisPieceContinuosTo,'string',num2str(para.cont(med,1).piece{info.ThisPiece}.continuosTo));
    set(bouton.gfThisPieceColor,'value',min(para.cont(med,1).piece{info.ThisPiece}.ColorIndex,7));
    set(bouton.gfThisPieceKind,'value',para.cont(med,1).piece{info.ThisPiece}.kind);
    k = strfind(nom,'.txt');
    if ~isempty(nom)
      if isempty(k)  % Es un archivo .stl
        para.cont(med,1).piece{info.ThisPiece}.isalist = false;
        [para.cont(med,1).piece{info.ThisPiece}.geoFileData,flag] = ...
          previewSTL(bouton.gfPreview,para.cont(med,1).piece{info.ThisPiece});
      else % es una lista de archivos .stl en la misma carpeta
        para.cont(med,1).piece{info.ThisPiece}.isalist = true;
        para.cont(med,1).piece{info.ThisPiece} = loadStageList(...
          para.cont(med,1).piece{info.ThisPiece},para.nf,nom);
        
        % El ultimo de la lista para que los receptores tengan la regiOn
        % correcta:
        auxcont.stage(1).fileName = para.cont(med,1).piece{info.ThisPiece}.stage(end).fileName;
        auxcont.isalist = true;
        auxcont.ColorIndex = para.cont(med,1).piece{info.ThisPiece}.ColorIndex;
        [para.cont(med,1).piece{info.ThisPiece}.geoFileData,flag] = ...
          justloadSTL(auxcont);
        clear auxcont
        if flag == 0 % cuando no se pudo cargar
          para.cont(med,1).piece{info.ThisPiece}.fileName ='X';
        end
      end
      clear nom
    else
      set(bouton.gfNumPieces,'string','0');
      set(bouton.geoFileSelect,'string','');
      set(bouton.gfThisPieceContinuosTo,'string','0');
      set(bouton.gfThisPieceColor,'value',1);
      set(bouton.gfThisPieceKind,'value',1);
      axes(bouton.gfPreview);cla
    end
  end
end

