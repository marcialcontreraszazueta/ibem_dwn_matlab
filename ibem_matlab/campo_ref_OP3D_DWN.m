function [U,t,DWN] = campo_ref_OP3D_DWN(iinc,coord,para,DWN,polOP,kxk)
% campo_ref_OP3D_DWN Desp. y Trac. dada onda plana en estratificado 3D

% Desplazamientos U y las tractiones t en puntos de colocaciOn y receptores
% reales DWN.U0, DWN.S0. 

n       = coord.nbpt; %numero de puntos de colocaciOn
U       = zeros(3,n);
t       = zeros(3,n);

% La onda plana incidente se coloca siempre en el medio m=1
indi  	= coord.indm(1,1).ind;

% posicion de puntos de colocacion y receptores:
nrv     = DWN.nxrv; %Cantidad de puntos de coloc donde hay que calcular
nrec    = length(DWN.xr); % Cantidad, incluidos los receptores reales
% salu = true(nrec,1);sals = true(nrec,1);
salu    = DWN.salu;
sals    = DWN.sals;
xr      = DWN.xr;
yr      = DWN.yr;
zr0     = DWN.zr0;
izr0    = DWN.izr0;

% fuente:
coordf.xs       = para.xs(iinc);
coordf.ys       = para.ys(iinc);
coordf.zs       = para.zs(iinc);

kzsigno     = para.kzsigno(iinc);
phi         = para.phi(iinc);

% P.ColocaciOn: Campo Inciente + Campo difractado por estratos y sup. libre
rr = (1:nrv);
[u,s]= calcul_US_DWN_3D_Ncapas_HS_incOP(para,xr(rr),yr(rr),zr0,izr0(rr),...
  salu(rr),sals(rr),coordf,kxk,polOP,kzsigno,phi,false);
U(:,indi)           = u;%(:,1:nrv);
% s                   = s(:,:,1:nrv); % para las tracciones

% Receptores:  Campo Incidente + Campo difractado por estratos y sup. libre
rr = ((nrv+1):nrec);
[u0,s0]= calcul_US_DWN_3D_Ncapas_HS_incOP(para,xr(rr),yr(rr),zr0,izr0(rr),...
  salu(rr),sals(rr),coordf,kxk,polOP,kzsigno,phi,false);
DWN.U0(:,:,iinc)    = u0; % llamado uxz0 en PSV
if length(sals)>nrv && sals(nrv+1)
%     S1              = squeeze(reshape(s(:,:,nrv+1:nrec),1,9,nrec-nrv));
    S1              = squeeze(reshape(s0(:,:,:),1,9,length(rr)));
    S1              = S1(DWN.inds,:);
    DWN.S0(:,:,iinc)= S1;
end

% %% test
% figure;hold on; plot3(xr,yr,zr0(izr0),'k*')
% ur = real(u); ui = imag(u);
% ur(1,:) = ur(1,:) - ur(1,40);
% ur(2,:) = ur(2,:) - ur(2,40);
% ur(3,:) = ur(3,:) - ur(3,40);
% ui(1,:) = ui(1,:) - ui(1,40);
% ui(2,:) = ui(2,:) - ui(2,40);
% ui(3,:) = ui(3,:) - ui(3,40);
% z = zr0(izr0);
% quiver3(xr,yr,z.',ur(1,:),ur(2,:),ur(3,:),'r','LineWidth',3.0);
% quiver3(xr,yr,z.',ui(1,:),ui(2,:),ui(3,:),'b','LineWidth',3.0);
% set(gca,'Zdir','reverse');view([0 10]); axis equal;
% %%

% % receptores reales
% DWN.U0(:,:,iinc)    = u(:,nrv+1:end); %uxz0 en PSV
% if length(sals)>nrv && sals(nrv+1)
%     S1              = squeeze(reshape(s(:,:,nrv+1:nrec),1,9,nrec-nrv));
%     S1              = S1(DWN.inds,:);
%     DWN.S0(:,:,iinc)= S1;
% end

% puntos de colocacion
% U(:,indi)           = u(:,1:nrv);
% s                   = s(:,:,1:nrv);

%calculo de la traccion en los puntos de colocacion solamente
if isfield(coord,'vnx')
  vn = zeros(3,nrv);
vn(1,:)	= coord.vnx(indi);
vn(2,:)	= coord.vny(indi);
vn(3,:)	= coord.vnz(indi);

t(1,indi)  = squeeze(s(1,1,:)).'.*vn(1,:)+squeeze(s(1,2,:)).'.*vn(2,:)+squeeze(s(1,3,:)).'.*vn(3,:);
t(2,indi)  = squeeze(s(1,2,:)).'.*vn(1,:)+squeeze(s(2,2,:)).'.*vn(2,:)+squeeze(s(2,3,:)).'.*vn(3,:);
t(3,indi)  = squeeze(s(1,3,:)).'.*vn(1,:)+squeeze(s(2,3,:)).'.*vn(2,:)+squeeze(s(3,3,:)).'.*vn(3,:);
end

% %% Test me:
% figure(1000+iinc);clf; hold on; sca = 0.025;
% plot3(coord.x(indi),coord.y(indi),coord.z(indi),'k.')
% Ua = abs(U);
% Ur = real(U);
% Ui = imag(U);
% if ~isempty(Ua)
% mama=max(max(Ua));
% Up = Ur/mama*sca;
% quiver3(coord.x(indi),coord.y(indi),coord.z(indi),Up(1,:),Up(2,:),Up(3,:),'r')
% Up = Ui/mama*sca;
% quiver3(coord.x(indi),coord.y(indi),coord.z(indi),Up(1,:),Up(2,:),Up(3,:),'b')
% Up = Ua/mama*sca;
% quiver3(coord.x(indi),coord.y(indi),coord.z(indi),Up(1,:),Up(2,:),Up(3,:),'k')
% plot3(xr(nrv+1:end),yr(nrv+1:end),DWN.zr(nrv+1:end),'k.')
% end
% Ua = abs(DWN.U0(:,:,iinc));
% Ur = real(DWN.U0(:,:,iinc));
% Ui = imag(DWN.U0(:,:,iinc));
% if ~isempty(Ua)
% mama=max(max(Ua));
% Up = Ur/mama*sca;
% quiver3(xr(nrv+1:end),yr(nrv+1:end),DWN.zr(nrv+1:end),Up(1,:),Up(2,:),Up(3,:),'r')
% Up = Ui/mama*sca;
% quiver3(xr(nrv+1:end),yr(nrv+1:end),DWN.zr(nrv+1:end),Up(1,:),Up(2,:),Up(3,:),'b')
% Up = Ua/mama*sca;
% quiver3(xr(nrv+1:end),yr(nrv+1:end),DWN.zr(nrv+1:end),Up(1,:),Up(2,:),Up(3,:),'k')
% view(3);set(gca,'Zdir','reverse')
% axis equal; xlabel('x');ylabel('y')
% end
end