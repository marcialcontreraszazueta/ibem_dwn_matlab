function verQbilin(medio)
qd = medio.qd;
qfcorner = medio.qbili.qfcorner;
b = medio.qbili.b;
m = medio.qbili.m;

qd = 15
b = -37.2
qfcorner = 20

frecs = [[0:1:qfcorner],[qfcorner+1:1:2*qfcorner]];
qss = frecs*0;
qss(1:qfcorner+1) = qd;

for ff = 1+qfcorner:1:2*qfcorner
    qss(ff+1) = b+m*ff;
end

figure(13481); hold on
plot(frecs,qss,'r-')
ylim([0,qss(end)])
xlim([0,frecs(end)])
xlabel('frecuencia Hz')
ylabel('Q')
end