function [B,DWN]=vector_fuente3D(para,DWN)

if para.dim == 4
  [B,DWN]=vector_fuente3D2(para,DWN);
  return
end

coord = para.coord;
% unpack
xs      = para.xs;
ys      = para.ys;
zs      = para.zs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vectores de condiciones a las fronteras %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbeq    = coord.nbeq;
nbpt    = coord.nbpt;
ju      = coord.ju;
j       = 1:coord.nbpt;

B       = zeros(3*nbeq,para.ninc);

if para.fuente==1 %OP
  % La incidencia de ondas planas se coloca en el medio exterior (1)
    % incidencia de ondas planas homogeneas e inhomogeneas
    
% La onda incidente es unitaria con un componente horizontal del numero de
% onda definido con respecto al numero de onda k por un factor kxk y kyk.
    kxk     = para.kxk; %cartesiana x de la normal de la OP
    kyk     = para.kyk; %cartesiana y de la normal de la OP
    polOP   = para.tipo_onda;
    
    m       = para.xzs(1);   % medio al que pertence la primer fuente
    if para.tipoMed(1) == 1 % homogeneo
      if isfield(para,'sumb1')
        mmat    = para.subm1(m); % indice correcto de m si hubo subdivici???n
      else
        mmat    = m; % en el caso 3D gen
      end
      Ci      = para.reg(mmat).Ci;
      ksi     = para.reg(mmat).ksi;
      kpi     = para.reg(mmat).kpi;
      kri     = para.reg(mmat).kri;
    end
    
    %indice de los puntos perteneciendo al medio de la fuente, todos
    %estos cumplen con la continuidad de los esfuerzos
    jjx     = coord.indm(m).ind;
    
%     %% testme
%     figure;hold on
%     plot3(coord.x,coord.y,coord.z,'k*')
%     plot3(coord.x(jjx),coord.y(jjx),coord.z(jjx),'ro')
%     jjnotthis = coord.indm(2).ind;
%     plot3(coord.x(jjnotthis),coord.y(jjnotthis),coord.z(jjnotthis),'b.')
    %%
    %se busca si el medio de la fuente es el de indice mas pequeno de
    %los medios en contactos en el punto de colocacion para fijar el
    %signo de las ecuaciones
    %(cf vector fuente B y construcion matriz A)
    tmp = coord.Xim(jjx,:);
    im  = zeros(1,size(tmp,1));
    for i=1:size(tmp,1)
        im(i) = find(tmp(i,:)~=0, 1, 'first' );
    end
    
    %indice de los puntos perteneciendo al medio de la fuente y que
    %tienen que cumplir con continuidad de desplazamientos
    salu    = false(nbpt,1);
    salu(j(jjx))= sum(coord.Xim(j(jjx),:)~=0,2)==2;
    %se busca si el medio de la fuente es el de indice mas pequeno de
    %los medios en contactos en el punto de colocacion para fijar el
    %signo de las ecuaciones
    %(cf vector fuente B y construcion matriz A)
    tmp=coord.Xim(salu,:);
    imu=zeros(1,size(tmp,1));
    for i=1:size(tmp,1)
        imu(i) = find(tmp(i,:)~=0, 1, 'first' );
    end
    clear tmp
    
%     %% testme
%     figure;hold on
%     h = plot(jjx,'k-'); set(h,'DisplayName','jjx');
%     h = plot(im,'k--'); set(h,'DisplayName','im');
%     h = plot(salu,'b-'); set(h,'DisplayName','salu');
%     h = plot(imu,'b--'); set(h,'DisplayName','imu');
    
    %%
    for iinc=1:para.ninc
        if para.tipoMed(m)==2 % Medio de Estratificado (DWN)
            [u,t,DWN{m}] = campo_ref_OP3D_DWN(iinc,coord,para,DWN{m},polOP(iinc),kxk(iinc));
        else % Medio homogEneo 
            if para.fuenteimagen==1 % ... usando la fuente imagen (si es onda plana)
                [u,t] = campo_ref_OPHeI_3D_SE(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,kri,Ci,polOP(iinc),kxk(iinc),kyk(iinc));
%                 t(isnan(t)) = 0;  u(isnan(u)) = 0;
            else % ... usando puntos de colocaciOn
                [u,t] = campo_ref_OPHeI_3D(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,kri,Ci,polOP(iinc),kxk(iinc),kyk(iinc));
            end           
        end
        
%         %% Test me
%         figure(543);clf;dibujo_conf_geo(para,gca);title('tracciones')
%         hold on;set(gca,'Zdir','reverse');axis equal;grid on
%         quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%           real(t(1,j(jjx))),real(t(2,j(jjx))),real(t(3,j(jjx))),'LineWidth',1)
%         
%         figure(544);clf;dibujo_conf_geo(para,gca);title('desplazamientos')
%         hold on;set(gca,'Zdir','reverse');axis equal;grid on
%         quiver3(coord.x(j(jjx)),coord.y(j(jjx)),coord.z(j(jjx)),...
%           real(u(1,j(jjx))),real(u(2,j(jjx))),real(u(3,j(jjx))),'LineWidth',1)
        
        %%
        %esfuerzos
        B(j(jjx)       ,iinc)       = -((m==im ) - (m~=im )).*t(1,j(jjx));%tx
        B(j(jjx)+  nbeq,iinc)       = -((m==im ) - (m~=im )).*t(2,j(jjx));%ty
        B(j(jjx)+2*nbeq,iinc)      	= -((m==im ) - (m~=im )).*t(3,j(jjx));%tz
        %desplazamientos
        B(nbpt+ju(salu)       ,iinc)= -((m==imu) - (m~=imu)).*u(1,j(salu));%ux
        B(nbpt+ju(salu)+  nbeq,iinc)= -((m==imu) - (m~=imu)).*u(2,j(salu));%uy
        B(nbpt+ju(salu)+2*nbeq,iinc)= -((m==imu) - (m~=imu)).*u(3,j(salu));%uz
    end
else %if para.fuente==2 FP
    % fuente puntual
    for iinc=1:para.ninc
        m       = para.xzs(iinc); %El medio al que pertenece lar fuente
        
        %indice de los puntos perteneciendo al medio de la fuente, todos
        %estos cumplen con la continuidad de los esfuerzos
        jjx     = coord.indm(m).ind;
        %indice de los puntos perteneciendo al medio de la fuente y que
        %tienen que cumplir con continuidad de desplazamientos
        salu    = false(nbpt,1);
        salu(j(jjx))= sum(coord.Xim(j(jjx),:)~=0,2)==2;
        
        %se busca si el medio de la fuente es el de indice mas pequeno de
        %los medios en contactos en el punto de colocacion para fijar el
        %signo de las ecuaciones
        %(cf vector fuente B y construcion matriz A)
        tmp=coord.Xim(jjx,:);
        im =zeros(1,size(tmp,1));
        for i=1:size(tmp,1)
            im(i) = find(tmp(i,:)~=0, 1, 'first' );
        end
        
        tmp=coord.Xim(salu,:);
        imu=zeros(1,size(tmp,1));
        for i=1:size(tmp,1)
            imu(i) = find(tmp(i,:)~=0, 1, 'first' );
        end
        
        
        % se calcula los terminos fuentes
        if para.tipoMed(m)==2 % estratificado
            [u,t,DWN{m}]   = campo_ref_FP3D_DWN(iinc,coord,para,DWN{m});
        else
          fij = para.fij;
          mmat    = para.subm1(m);
          Ci      = para.reg(mmat).Ci;
          ksi     = para.reg(mmat).ksi;
          kpi     = para.reg(mmat).kpi;
            [u,t]       = campo_ref_FP3D(xs(iinc),ys(iinc),zs(iinc),coord,kpi,ksi,Ci,fij(iinc,:),salu,jjx,para);
        end
        
        %esfuerzos
        B(j(jjx)       ,iinc)      	= -((m==im ) - (m~=im )).*t(1,j(jjx));%tx
        B(j(jjx)+  nbeq,iinc)       = -((m==im ) - (m~=im )).*t(2,j(jjx));%ty
        B(j(jjx)+2*nbeq,iinc)      	= -((m==im ) - (m~=im )).*t(3,j(jjx));%tz
        
        %desplazamientos
        B(nbpt+ju(salu)       ,iinc)= -((m==imu) - (m~=imu)).*u(1,j(salu));%ux
        B(nbpt+ju(salu)+  nbeq,iinc)= -((m==imu) - (m~=imu)).*u(2,j(salu));%uz
        B(nbpt+ju(salu)+2*nbeq,iinc)= -((m==imu) - (m~=imu)).*u(3,j(salu));%uz
        % en un punto de colocacion, hay siempre 2 medios en contacto,
        % aunque a veces este medio es el vacio
        % las ecuaciones de continuidad de traccion o de desplazamiento
        % involven entonces siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
        % y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
        % se reorganisa entonces como :
        % sigma_diff(m)-sigma_diff(m1)=u_inc(m1)-u_inc(m)
        % los signos se fijan deacuerdo con el indice del medio,
        % + si el indice es el mas pequeno de los medios en contacto
        % - en lo contrario
    end
end
if para.GraficarCadaDiscretizacion
%% testme
figure(654321); clf; hold on
plot(real(B(:,1)),'r')
plot(imag(B(:,1)),'b')
l = length(B)/3;
plot([l l],ylim,'k-')
plot([2*l 2*l],ylim,'k-')
title('vector fuente')
end
end
