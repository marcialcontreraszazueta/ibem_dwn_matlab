function GIJ = Gij_3DDWN_smallGENLENTO(xr,... %receptor: coord y estrato
  rij,g,dr,Vn,...
  CubPts,CubWts,icr,ics,... % coordenadas y pesos de la cubatura y estrato
  DWN)
% Una fuente distribuida un receptor.
% El receptor cerca de la fuente en un medio estratificado.
% La fuente distribuida en un segmento triangular.
%func de Green [(u v w) x (fx fy fz)]
GIJ = complex(zeros(3,3,1,1));
GijP= complex(zeros(3,3,1,1));
% Para cada receptor (punto de integraciOn)
ngau = length(CubWts);

%icr : estrato del receptor (punto de int.) .. del centroide del triang.
ksi 	= DWN.sub(icr).ksi;
kpi 	= DWN.sub(icr).kpi;
Ci    = DWN.sub(icr).Ci;
% profundidad relativa a la interface de la capa del receptor:
zricr = xr(3) -  DWN.z(icr);

for iga = 1:ngau
% Armar vector de fuente en medio estratificado 
[FsourcePSV,FsourceSH,~,~] = VecfuentDWN(CubPts(3,iga),DWN);

% Amplitud de las ondas planas que construyen el campo difractado por la
% estratificaciOn:
[SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
  DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);
        
  % tErminos de propagaciOn vertical a las ondas planas que
  % constituyen el campo difractado por la estratificaciOn
  % en el estrato del receptor [fac].
  facU = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
    SolutionPSV,SolutionSH,...
    DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,1,DWN.nkr);
  
  % fase horizontal
  Xfs = xr(1)-CubPts(1,iga); % receptor - fuente
  Yfs = xr(2)-CubPts(2,iga);
  r = sqrt(Xfs^2 + Yfs^2); % distancia
  J0      = besselj(0,DWN.kr*r);
  J1      = besselj(1,DWN.kr*r);
  dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
  d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
  dJ0     =-DWN.kr.*J1;
  d2J0    =-DWN.kr.*dJ1;
  if r<1e-6
    c = 1/sqrt(2);
    s = 1/sqrt(2);
  else
    c = Xfs/r;
    s = Yfs/r;
  end
  
  % Agregar terminos que solo dependen del radio horizontal
  % tracciones
  %     if doU % desplazamientos
  MecRadialU = campodiffporestratRadialTransv(1,...
    r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
    ksi,kpi,facU(:,:),DWN.DK); 
  
  %func de Green [(fx fy fz) x (u v w)] :
  MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6));   

  % permute -> [(u v w) x (fx fy fz)]
  GijP(1:3,1:3,1,1) = MecElemU.';
  
  % multiplicar por peso de integraciOn y acumular en GIJ
  GIJ = GIJ + GijP.*CubWts(iga); %[(u v w) x (fx fy fz)]
end % iga

% incidencia directa
if icr == ics
  if rij<=0.5*dr % jjCirculo
    BEALF = real(kpi)/real(ksi);
    if rij > 0.01*dr
      EPS = rij/dr;
      % [(u v w) x (fx fy fz)]
      GIJ = GIJ + Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),g,EPS);
    else
      % [(u v w) x (fx fy fz)]
      GIJ = GIJ + Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),0,0);
    end
  else %jjMidRange rij>0.5*dr & rij<longonda*fracCercano
   
    %func de Green [(u v w) x (fx fy fz)]
    Gij = Gij_3D_r_smallGENf(xr,...
      CubPts,CubWts,...
      ksi,kpi,Ci);
    GIJ = GIJ + squeeze(Gij); %[(u v w) x (fx fy fz)]
  end
end
end