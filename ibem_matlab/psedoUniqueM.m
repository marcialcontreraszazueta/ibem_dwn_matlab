function [M,n,disN] = psedoUniqueM(Mdist,lambda,polarsimetry)
% psedoUniqueM agrupa las distancias fuente-receptor en Mdist
%    Los grupos son intervalos de ancho lambda. Se devuelve [M,n,disN]
%    M : la matriz con indices de distancia similar y 
%    n : la cantidad de grupos de distancias similares y
% disN : los grupos de distancia.
% tic
if lambda == 0
M = Mdist;
Maplanada = reshape(M,size(M,1)*size(M,2),1);
disN = unique(Maplanada); disN(isnan(disN)) = [];
n = 0;
else
mama = max(max(Mdist));
if nargin == 3
  if polarsimetry
    % reducir lambda para que exista simetrIa a los ejes x,y
    rat=ceil(0.5*pi/lambda);
    rat=max(3,rat);
    lambda = 0.5*pi/rat;
  end
end

n = 1;
% gigante = 100*ceil(mama/lambda)+1;
M = zeros(size(Mdist));
lastM = Mdist<=lambda;
Mdist(lastM) = nan;
M = M + 1* lastM; 
for i = 2:ceil(mama/lambda)+1
  lastM = Mdist <= i * lambda;
  Mdist(lastM) = nan;
  if ~isempty(find(lastM,1))
    M = M + i * lastM;
    n = n + 1;
  end
%   mau = (Mdist <= i * lambda) - (Mdist <= (i-1) * lambda);
%   if sum(sum(mau))> 0
%     M = M + i * mau;
%     n = n + 1;
%   end
end

 %unique no se puede compilar para matrices, sOlo vectores
Maplanada = reshape(M,size(M,1)*size(M,2),1);
disN = unique(Maplanada); 

disN(disN==0) = []; 
M(M==0) = NaN; 

disN(isnan(disN)) = [];
end
% toc
end