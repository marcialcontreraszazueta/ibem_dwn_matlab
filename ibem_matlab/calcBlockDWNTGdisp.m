function [AbT,AbG] = calcBlockDWNTGdisp(...
  nrens,pcX,pcVn,pcXim,m,im0,...
  ncols,pTXi,pTdA,pTcubPts,...
  isSelfReciproc,relatedG,...
  CubWts,DWN)
% calcBlock Bloque de matriz de condiciones de frontera en medio estratif.
%    Si Xi pertenece a un solo medio (tracciones libres),
%    se establecen esfuerzos nulos. Si Xi pertence a dos medios,
%    se establece la continuidad de tracciones y desplazamientos
%    a ambos lados de Xi.

% 0.1) Fracciones de la longitud de onda para agrupar elementos: ----------
fracZ = 1/60;   %  Tij_3D reciproco en semiespacio sOlo si x(3) aprox xi(3)
fracVert = 60;  %  1/#  agrupar posiciones verticales en campo diff.
fracDist = 1/60;%  Agrupar distancias horizontales, aguas con [k] grande
fracCercano = 1/200;    % Integracion Gaussiana de campo dwn / casi nada 60
ngau = length(CubWts);
% fracMax = 1; factor * minLambda desde donde truncar la soluciOn (opcional)

% 0.2) Reconocer las fuentes a profundiades iguales: ----------------------
[Xiz_uni,uni_ref,zrref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
  pTXi(3,:),pTXi(4,:),DWN.ncapas,fracVert);

% Reconocer los receptores en [rens] a la misma profundidad
if isSelfReciproc
  X_z_uni = Xiz_uni;
% uni_ref ya fue calculado
  zXref = zrref;
else
  [X_z_uni,uni_ref,zXref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
    pcX(3,:),pcX(4,:),DWN.ncapas,fracVert);
end

% Para cada profundidad de la fuente se ensambla el vector de fuente sin
% fase radial y en coordenadas polares. Luego se obtiene la amplitud de las
% ondas que construyen el campo difractado por el medio estratificado y el
% campo difractado mismo para cada par de profundidades del receptor/fuente
% y cada distancia horizontal promedio y cada angulo azimutal promedio.

leXiz_uni = length(Xiz_uni);
leX_z_uni = length(X_z_uni);

% Arreglo de Matrices Resultado. Compartidas entre procesos.
aux = struct('matrix',complex(zeros(3,3,nrens,ncols)));
At = repmat(struct('T',aux),1,leXiz_uni);

if relatedG~=0
  nrensG = nrens;
  ncolsG = ncols;
  Ag = repmat(struct('G',aux),1,leXiz_uni);
else
  nrensG = 1;
  ncolsG = 1;
  Ag = repmat(struct('G',aux),1,1);
end
clear aux

%% ----Estas declaraciones van afuera cuando esta funcion se compila:-----
% FsourcePSV  = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok       |
% FsourceSH   = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok         |
% SolutionPSV = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok       |
% SolutionSH  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok         |
% SolutionPSVGau = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok    |
% SolutionSHGau  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok      |
% AtmpT = complex(zeros(3,3,nrens,ncols));                               |
% AtmpG = complex(zeros(3,3,nrens,ncols));                               |
%facT = complex(zeros(10,DWN.nkr,leX_z_uni));                            |
%facG = complex(zeros( 5,DWN.nkr,leX_z_uni));                            |
% fac_icr = zeros(leX_z_uni,1);                                          |
% irxs=zeros(nrens*ncols,1);ixss=zeros(nrens*ncols,1);%#ok               |
% rij=0.0;dr=0.0;%#ok                                                    |
% Mplot = zeros(nrens,ncols);                                            |
% MdisUni = zeros(nrens,ncols); disN = zeros(ceil(nrens*ncols/2),1); %#ok|
%MecElemU = complex(zeros(3,3)); %                                       |
%MecElemS = complex(zeros(3,3,3));%#ok                                   |
% ksi=complex(zeros(1,1)); kpi=complex(zeros(1,1));                      |
% Ci=complex(zeros(1,1)); %#ok                                           |
% Mi=zeros(nrens,ncols).*NaN;iDist=0.0; %#ok                             |
% nkss = DWN.nkr;                                                        |
% J0 = complex(zeros(1,nkss)); J1 = complex(zeros(1,nkss));%#ok          |
% dJ1= complex(zeros(1,nkss));d2J1= complex(zeros(1,nkss));%#ok          |
% dJ0= complex(zeros(1,nkss));d2J0= complex(zeros(1,nkss));%#ok          |
%% -----------------------------------------------------------------------

% 0.3) Si es autoreciproco, las singularidades son faciles de identificar
if isSelfReciproc
  ATsing = complex(zeros(3,3,nrens));
  if relatedG~=0
    AGsing = complex(zeros(3,3,nrens));
  end
  
  for iii = 1:nrens % ncols
    ir   = iii; % Indice global del receptor
    iixs = iii; % Indice global de la fuente
    
      % Es una singuaridad impar. El valor principal de la integral en
      % el segmento es cero. Las reacciones a la fuerza unitaria
      % son 0.5 a cada lado del elemento.
      
      % en este punto, hay discontinuidad de las tractiones ti-ti'=phi
      % el signo1 es igual a +1 (resp. -1) si la normal apunta hacia el exterior
      % (resp. a interior) del medio a que pertenece el phi del punto de
      % colocacion en la posicion singular rij=0
      % como la convencion es que las normales esten siempre dirigidas hacia
      % abajo, hay que conocer si el phi pertenece a un contorno arriba o
      % abajo. Esta informacion esta en Xim
      
      signo1= (pcXim(m,ir)==1) - (pcXim(m,ir)==2);
      ATsing(:,:,iii) = signo1*0.5*eye(3);
      
      % campo de desplazamiento por incidencia directa con Greenex
      %  nota: el campo difractado por los estratos se agrega luego
      if relatedG~=0
        icr = pcX(4,ir); % estrato
        [ksi,kpi,Ci] = theSub(DWN.sub,icr(1,1));
        BEALF = real(kpi)/real(ksi);
        Vn = pcVn(1:3,iixs);
        dr = sqrt(pTdA(iixs)/pi);
        % [(u v w) x (fx fy fz)]
        AGsing(:,:,iii)=Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),0,0)*pTdA(iixs);
      end
%     end
  end
end

parfor iXiz_uni = 1:leXiz_uni % para cada profundidad de fuentes agrupada
   disp(['[' num2str(iXiz_uni) ' / ' num2str(leXiz_uni) ']'])
  
  % Allocations,     for speeeeeeddddd
AtmpT = complex(zeros(3,3,nrens,ncols));
AtmpG = complex(zeros(3,3,nrensG,ncolsG));
facT = complex(zeros(10,DWN.nkr)); %#ok overhead
facG = complex(zeros( 5,DWN.nkr));
% FsourcePSV  = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
% FsourceSH   = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
% SolutionPSV = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
% SolutionSH  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
% SolutionPSVGau = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
% SolutionSHGau  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
% irxs=zeros(nrens*ncols,1);ixss=zeros(nrens*ncols,1);%#ok
% rij=0.0;dr=0.0;%#ok
% MdisUni = zeros(nrens,ncols); disN = zeros(ceil(nrens*ncols/2),1); %#ok
% ksi=complex(zeros(1,1)); kpi=complex(zeros(1,1)); %#ok
% Ci=complex(zeros(1,1)); %#ok
% icr = zeros(1,1); %#ok
% pcXaux=zeros(3,1);
% nkss = DWN.nkr;
% J0 = complex(zeros(1,nkss)); J1 = complex(zeros(1,nkss));%#ok
% dJ1= complex(zeros(1,nkss));d2J1= complex(zeros(1,nkss));%#ok
% dJ0= complex(zeros(1,nkss));d2J0= complex(zeros(1,nkss));%#ok
% ics = 1; %#ok para que no chiste el compilador

  % Indices de las fuentes a esta profundidad:
  j = find(zrref==iXiz_uni); % e.g.: pTXi(3,j) son todos casi iguales
  Xiz = Xiz_uni(iXiz_uni);   % profundidad de las Xi
  
  % Armar vector de fuente [lejana] en el medio estratificado:
  [FsourcePSV,FsourceSH,ics,~] = VecfuentDWN(Xiz,DWN);
  % Amplitud de las ondas planas que construyen el campo difractado por la
  % estratificaciOn:
  [SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
    DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);
  
  for iX_z_uni = 1:leX_z_uni % Para cada profundiad de receptores agrupada
    icr = pcX(4,uni_ref(iX_z_uni)); %#ok estrato del receptor
%     ir=0;iixs=0;Zfs=0.0;Xfs=0.0;Yfs=0.0;r=0.0;c=0.0;s=0.0;th=0.0; %#ok
%     Mi=zeros(nrens,ncols).*NaN;iDist=0.0; %#ok
    
    % indice de los receptores a esta profundidad  e.g.: pcX(3,res_at_iX_z)
    res_at_iX_z = find(zXref==iX_z_uni); %               son todos iguales
    % material:
    [ksi,kpi,Ci] = theSub(DWN.sub,icr(1,1));
    longonda = (2*pi)/real(ksi);
    
    % Reset -----
    MdistXY = zeros(nrens,ncols) .* NaN;
    MecRadialU= []; MecRadialS= []; rij=[]; g=[]; %     <-- reset to 0 every iX_z_uni
%     Gij  = complex(zeros(3,3,1,1)); %      <-- reset to 0 every iX_z_uni
%     Tij  = complex(zeros(3,3,1,1)); %      <-- reset to 0 every iX_z_uni
    
%  1) Matriz Mdist con las distancias SIN RECIPROCIDAD --------------------
    for Iiixs = 1:length(j.') 
    % la matriz tiene el tamaNo original y los indices son globales
      iixs = j(Iiixs); % Indice global de la fuente
      for Iir = 1:length(res_at_iX_z.')
        ir = res_at_iX_z(Iir); % Indice global del receptor
        
        Xfs = pcX(1,ir)-pTXi(1,iixs);%#ok
        Yfs = pcX(2,ir)-pTXi(2,iixs);
        %         if isSelfReciproc %&& relatedG==0
        if (Xfs == 0 && Yfs ==0)
          if ((pcX(3,ir)-pTXi(3,iixs)) == 0) % x = xi
            MdistXY(ir,iixs) = 0.0000000123; % <-- flag
            continue
          end
        end
        %         end
        MdistXY(ir,iixs) = sqrt(Xfs^2 + Yfs^2);

        % AQUI se puede incertar una regla para truncar la soluciOn.
%         if MdistXY(ir,iixs) > fracMax*longonda 
%            MdistXY(ir,iixs) = NaN;
%         end
      end
    end
    
%  0.3) Las singularidades si no es autoreciproco -------------------------
    if ~isSelfReciproc
%       error('falta checar esto')
      [Mi] = MdistXY(:,:) <= 1e-6;  
      [irxs,ixss] = find(Mi); %los elementos en Mi
      for iii = 1:length(irxs)
        ir   = irxs(iii); % Indice global del receptor
        iixs = ixss(iii); % Indice global de la fuente
        dz = pcX(3,ir)-pTXi(3,iixs);
        if dz <= 1e-6
          % Es una singuaridad impar. El valor principal de la integral en 
          % el segmento es cero. Las reacciones a la fuerza unitaria
          % son 0.5 a cada lado del elemento.
          
%            if ics == icr(1,1) %&& rij < 0.5*dr % Mismo estrato
           % en este punto, hay discontinuidad de las tractiones ti-ti'=phi
           % el signo1 es igual a +1 (resp. -1) si la normal apunta hacia el exterior
           % (resp. a interior) del medio a que pertenece el phi del punto de
           % colocacion en la posicion singular rij=0
           % como la convencion es que las normales esten siempre dirigidas hacia
           % abajo, hay que conocer si el phi pertenece a un contorno arriba o
           % abajo. Esta informacion esta en Xim
              
           signo1= (pcXim(m,ir)==1) - (pcXim(m,ir)==2); %#ok
           AtmpT(:,:,ir,iixs) = signo1*0.5*eye(3);
           
           % campo de desplazamiento por incidencia directa con Greenex
           %  nota: el campo difractado por los estratos se agrega luego
           if relatedG~=0
             BEALF = real(kpi)/real(ksi);
             Vn = pcVn(1:3,iixs); %#ok
             dr = sqrt(pTdA(iixs)/pi);  %#ok
             % [(u v w) x (fx fy fz)]
             Gij=Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),0,0);
             AtmpG(:,:,ir,iixs) = Gij * pTdA(iixs); 
           end
        else % no singular
          if relatedG==0 % Si solo se resuelven tracciones 
            Mi(ir,iixs) = false; % no borrarlo
          end
        end
      end
      
      % evitar estos puntos porque ya estan resueltos
      MdistXY(Mi) = NaN; 
    end
    
%  2) --- Difractado por estratos, cuadratura en campo cercano ------------
    if true
      % IntegraciOn gaussiana de fuente distribuida en segmento triangular
      %  Nota, aqui fuentes y receptores tienen profundidades similares
          
      [Mi] = MdistXY(:,:) <= longonda*fracCercano;%      cualquier Z, meh.. 
      [irxs,ixss] = find(Mi); %los elementos en Mi
      
      % nota: Se podrian agrupar los puntos de la cuadratura. 
      
%   2.1) Directa: .........................................................
      if ics == icr(1,1) % mismo estrato
        for iii = 1:length(irxs)
          ir   = irxs(iii); % Indice global del receptor
          iixs = ixss(iii); % Indice global de la fuente
          if MdistXY(ir,iixs) > 0.0000000123 %no los singulares
            % ------  tracciones  ------
            Tij = Tij_3D_r_smallGENf(...
              pcX(1:3,ir),pcVn(1:3,ir),...
              pTcubPts(1:3,1:ngau,iixs),CubWts,...
              ksi,kpi,0); %#ok overhead
            % Multiplicar por el Area y agregar a difractado
             AtmpT(1:3,1:3,ir,iixs) = Tij * pTdA(iixs);
           
            % ------  desplazamientos  ------
            if relatedG~=0
              pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
              rij = sum((pcXaux) .* (pcXaux))^0.5; %distancia en el espacio
              dr = sqrt(pTdA(iixs)/pi); % radio del cIrculo de Area dA(ic)
              if rij<=0.5*dr % jjCirculo
                g = pcXaux./rij;
                Vn = pcVn(1:3,ir);
                BEALF = real(kpi)/real(ksi);
                if rij > 0.01*dr
                  EPS = rij/dr;
                  % [(u v w) x (fx fy fz)]
                  Gij(:,:,1,1) = Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),g,EPS);
                else
                  % [(u v w) x (fx fy fz)]
                  Gij(:,:,1,1) = Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),0,0);
                end
              else %jjMidRange rij>0.5*dr & rij<=longonda*fracCercano
                %func de Green [(u v w) x (fx fy fz)]
                Gij(:,:,1,1) = Gij_3D_r_smallGENf(pcX(1:3,ir),...
                  pTcubPts(1:3,1:ngau,iixs),CubWts(1:ngau),...
                  ksi,kpi,Ci);
              end
              AtmpG(1:3,1:3,ir,iixs) = Gij * pTdA(iixs);
            end      
          end
        end
      end
      
%   2.2) Difractada: ......................................................
      if true 
      % Agrupar vector de indices de fuentes:
      [ixssUni] = unique(ixss,'stable');  %e.g.  ixss = ixssUni(ic);
      
      for iixs = ixssUni.' %  Para cada columna (fuente) unica,
        for igau = 1:ngau  %  Para cada punto de cuadratura
          Xizq = pTcubPts(3,igau,iixs); %profundidad de Xi
          [FsourcePSV,FsourceSH,~,~] = VecfuentDWN(Xizq,DWN); % Vec fuente.
          [SolutionPSVGau,SolutionSHGau] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
            DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr); % Amp. Ondas Planas
          
          for ir = irxs(ixss == iixs).' % seleccion en vector de receptores 
                                        % donde el vector fuente es iixs
            icrr = pcX(4,ir);
            % profundidad relativa a la interface de la capa del receptor:
            zricr = pcX(3,ir) - DWN.z(icrr);
      
            % distancia en el plano XY
            pcXaux(1:3) = pcX(1:3,ir)-pTcubPts(1:3,igau,iixs);
            r   = sum((pcXaux(1:2)) .* (pcXaux(1:2)))^0.5;
            
            J0  = besselj(0,DWN.kr*r);
            J1  = besselj(1,DWN.kr*r);
            dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
            d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
            dJ0     =-DWN.kr.*J1;
            d2J0    =-DWN.kr.*dJ1;
            
            if r < 1e-6
              c = 1/sqrt(2);
              s = 1/sqrt(2);
            else
              c = pcXaux(1)/r;
              s = pcXaux(2)/r;
            end
            
            % factores de propagacion vertical para estas prof. de fuente y recept
            if MdistXY(ir,iixs) > 0.0000000123 %no los singulares
               %traccion
              facT(:,:) = verticalFactors(icrr,DWN.ncapas,zricr,DWN.h(icrr),...
                SolutionPSVGau,SolutionSHGau,...
                DWN.kz(1,:,icrr),DWN.kz(2,:,icrr),... % <-- nu, gamma en estrato icr
                DWN.kr,-1,DWN.nkr);
              MecRadialS = campodiffporestratRadialTransv(-1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facT(:,:),DWN.DK);
              MecElemS = campodiffporestratAngular(-1,MecRadialS,c,s,Ci(6,6));
              AtmpT(1:3,1:3,ir,iixs) = AtmpT(1:3,1:3,ir,iixs) + ... 
                (Tij_fromMecElem(MecElemS,pcVn(:,ir)) * ... 
                pTdA(iixs) * CubWts(igau));
            end
            
            if relatedG~=0 
              %desplazamientos
              facG(:,:) = verticalFactors(icrr,DWN.ncapas,zricr,DWN.h(icrr),...
                SolutionPSVGau,SolutionSHGau,...
                DWN.kz(1,:,icrr),DWN.kz(2,:,icrr),...% nu, gamma en estrato icr
                DWN.kr, 1,DWN.nkr);
              MecRadialU = campodiffporestratRadialTransv(1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facG(:,:),DWN.DK);
              MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
              % MecElemU estA ordenado: [(fx fy fz) x (u v w)]
              %Gji -> Gij :  transpose to get [(u v w) x (fx fy fz)]
              AtmpG(1:3,1:3,ir,iixs) = AtmpG(1:3,1:3,ir,iixs) + ...
                (MecElemU.' * pTdA(iixs) * CubWts(igau));
%                 (Gij_fromMecElem(MecElemU,true) * pTdA(iixs) * CubWts(igau));
            end
          end %ir
        end %igau
      end %iixs
      end
      
%   2.3) Remover los indices calculados: ..................................
       % Los que restan (lejos) como fuente puntual en medio estratificado
       MdistXY(Mi) = NaN; %Remover los ya calculados (incluso X=Xi)
    end
    
%   3) Reducir Mdist para aprovechar la reciprocidad ----------------------
    if isSelfReciproc
      [irxs,ixss] = find(~isnan(MdistXY));
      for iii = 1:length(irxs)
        ir   = irxs(iii); % Indice global del receptor
        iixs = ixss(iii); % Indice global de la fuente
        if ir < iixs; %si estA por arriba de la diagonal
          % las tracciones que no estAn a la misma profundiad deben
          % resolverse directamente.
          
          Zfs = pcX(3,ir)-pTXi(3,iixs);
          if abs(Zfs) <= longonda*fracZ; % <-- empIrico
            % las profundidades son cercanas o iguales asI que es posible
            % aprovechar con el teorema de reciprocidad.
            MdistXY(ir,iixs) = nan;
          end
        end
      end
    end
    
%   4) --- Difractado por estratos, campo lejano --------------------------
    [MdisUni,~,disN] = psedoUniqueM(squeeze(MdistXY),longonda*fracDist,false);
    if ~isempty(disN) % DWN Fuente y receptor agrupados y lejos:
      
      % profundidad relativa a la interface de la capa del receptor:
      zricr = X_z_uni(iX_z_uni) - DWN.z(icr); %#ok
      
      % factores de propagacion vertical para estas prof. de fuente y recept
      facT(:,:) = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
        SolutionPSV,SolutionSH,...
        DWN.kz(1,:,icr),DWN.kz(2,:,icr),... % <-- nu, gamma en estrato icr
        DWN.kr,-1,DWN.nkr); %tracciones
      if relatedG~=0
        facG(:,:) = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
          SolutionPSV,SolutionSH,...
          DWN.kz(1,:,icr),DWN.kz(2,:,icr),... % <-- nu, gamma en estrato icr
          DWN.kr, 1,DWN.nkr); %desplazamientos
      end
      
      iDist=0.0; %#ok
      % Para cada grupo de distancia horizontal (con r casi igual)
      for IiDist = 1:length(disN.')%iDist = disN.'
        iDist = disN(IiDist);
        [Mi] = MdisUni == iDist;
        [irxs,ixss] = find(Mi); %los pares ir,iixs con radio 'r'
        
        r = MdistXY(irxs(1),ixss(1));  % la primera de las distancias agrupadas
%         r = mean(mean(MdistXY(Mi))); % distancia (en el plano) promedio
        
        %       for iiik = 1:nkss
        %         % funcion en matlab truducida de fortran:
        %         [J0(iiik),J1(iiik)] = BessJ01(DWN.kr(iiik)*r);
        %       end
        %         % funcion por teorema de reciprocidad.
        %       [J0kr,J1kr] = BessJmult(kr,Jkr,nkr,R);
        J0  = besselj(0,DWN.kr*r);
        J1  = besselj(1,DWN.kr*r);
        dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
        d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
        dJ0     =-DWN.kr.*J1;
        d2J0    =-DWN.kr.*dJ1;
        
        % Agregar terminos que solo dependen del radio horizontal e 
        % Integrar en el numero de onda radial
          MecRadialS = campodiffporestratRadialTransv(-1,...
            r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
            ksi,kpi,facT(:,:),DWN.DK);
        if relatedG~=0 % desplazamientos
          MecRadialU = campodiffporestratRadialTransv(1,...
            r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
            ksi,kpi,facG(:,:),DWN.DK);
        end 
        
        % usar psedoUniqueM con los azimuths es tardado, mejor de una vez.
          
        for iii = 1:length(irxs)
          ir   = irxs(iii); % Indice global del receptor
          iixs = ixss(iii); % Indice global de la fuente
          pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
          r = MdistXY(ir,iixs);                   %   distancia en el plano
          
          if r < 1e-6
            c = 1/sqrt(2);
            s = 1/sqrt(2);
          else
            c = pcXaux(1)/r;
            s = pcXaux(2)/r;
          end  
          
          % --- tracciones campo difractado ----
          MecElemS = campodiffporestratAngular(-1,MecRadialS,c,s,Ci(6,6));
          % Bajo la diagonal y los que no se pueden sacar de reciprocos
          
          Tij = Tij_fromMecElem(MecElemS,pcVn(:,ir));    % Tij(x,xi)
          
          % Indicencia directa
          if ics == icr(1,1)
            rij = sum((pcXaux) .* (pcXaux))^0.5;  % distancia en el espacio
            g = pcXaux./rij;
            Tij = Tij + Tij_3Done(ksi,kpi,rij,g,pcVn(1:3,ir));
          end
          
          AtmpT(1:3,1:3,ir,iixs) = Tij * pTdA(iixs);
          
          % Reciprocidad:
          if isSelfReciproc && ...
              ir > iixs && ... % tomarlo del reciproco bajo la diagonal
              abs(pcXaux(3)) <= longonda*fracZ; % misma profundidad
            MecElemR = MecElemS;
            % fuerza en direcciones X, Y. -sxx -syy -szz -sxy +sxz +syz
            MecElemR(1:2,1:2,1:2) = -MecElemS(1:2,1:2,1:2);
            MecElemR(1:2,3,3)     = -MecElemS(1:2,3,3);
            % fuerza en dirccion Z.       +sxx +syy +szz +sxy -sxz -syz
            MecElemR(3,1:2,3)     = -MecElemS(3,1:2,3);
            MecElemR(3,3,1:2)     = -MecElemS(3,3,1:2);
            % y transponiendo receptor/fuente
            
            Tij = Tij_fromMecElem(MecElemR,pcVn(:,iixs));    % Tij(x,xi)
            
            % Indicencia directa
            if ics == icr(1,1)
              Tij = Tij + Tij_3Done(ksi,kpi,rij,-g,pcVn(1:3,iixs));
            end
            
            AtmpT(1:3,1:3,iixs,ir) = Tij * pTdA(ir);
          end
          
          % --- desplazamientos campo difractado ----
          if relatedG~=0 
            if isSelfReciproc
              if ir >= iixs % en o por abajo de la diagonal
                MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
                % MecElemU ordenado como [(fx fy fz) x (u v w)]
                
                % func de Green por incidencia directa: [(fx fy fz) x (u v w)]
                if ics == icr(1,1)
                  MecElemU = MecElemU + squeeze(Gij_3Done(ksi,kpi,rij,g,Ci));
                end
                
                %Gji -> Gij :  transpose to get [(u v w) x (fx fy fz)]
                AtmpG(1:3,1:3,ir,iixs) = ...
                  squeeze(MecElemU).' * pTdA(iixs);
%                   Gij_fromMecElem(MecElemU,true) * pTdA(iixs);
                
                if ir > iixs % no la diagonal
                  % recIproco (transpuesto)
                  %Gji -> listo
                  AtmpG(1:3,1:3,iixs,ir) = ...
                    squeeze(MecElemU) * pTdA(ir);
%                     Gij_fromMecElem(MecElemU,false) * pTdA(ir);
                end
              end
            else %no es autoreciproco, hay que hacerlos todos
              MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
              % MecElemU viene como [(fx fy fz) x (u v w)]
              
              % func de Green por incidencia directa: [(fx fy fz) x (u v w)]
              if ics == icr(1,1)
                MecElemU = MecElemU + squeeze(Gij_3Done(ksi,kpi,rij,g,Ci));
              end
              
              %Gji -> Gij :  permute to get [(u v w) x (fx fy fz)]
              AtmpG(1:3,1:3,ir,iixs) = ...
                Gij_fromMecElem(MecElemU,true) * pTdA(iixs);
            end
          end
        end % iii
      end %IiDist
    end 
  end % iXiz_uni:Profundidad Unica putnos de colocaciOn
  
%  5) copiar las variables temporales al arreglo repartido entre procesos
  At(iXiz_uni).T.matrix = AtmpT;
  if relatedG~=0 % desplazamientos
    Ag(iXiz_uni).G.matrix = AtmpG;
  end
end %parfor iXiz_uni:Profundidad Unica de las fuentes
% cerarr el parfor
poolobj = gcp('nocreate');
delete(poolobj);

%  6) juntar los resultados: ----------------------------------------------
AbT  = complex(zeros(3,3,nrens,ncols));
for iXiz_uni=1:leXiz_uni
  AbT = AbT + At(iXiz_uni).T.matrix;
end
if relatedG~=0 % desplazamientos 
  AbG  = complex(zeros(3,3,ncols,nrens));
  for iXiz_uni=1:leXiz_uni
    AbG = AbG + Ag(iXiz_uni).G.matrix;
  end
else
  AbG  = complex(zeros(3,3,1,1));
end

if isSelfReciproc
  for iii = 1:nrens
  AbT(:,:,iii,iii) = ATsing(:,:,iii);
  end
  if relatedG~=0
    for iii = 1:nrens
    AbG(:,:,iii,iii) = AbG(:,:,iii,iii) + AGsing(:,:,iii);
    end
  end
end

%   7) signo de acuerdo al Indice del medio: -----------------------------
% en un punto de colocacion, hay siempre 2 medios en contacto,
% aunque a veces este medio es el vacio
% las ecuaciones de continuidad de traccion o de desplazamiento
% involucran siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
% y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
% se reorganiza entonces como :
% u_diff(m)-u_diff(m1)=u_inc(m1)-u_inc(m)
% los signos se fijan deacuerdo con el indice del medio,
% + si el indice es el mas pequeno de los medios en contacto
% - en lo contrario
for iii = 1:nrens
  AbT(:,:,iii,:) = AbT(:,:,iii,:) * ((m==im0(iii)) - (m~=im0(iii)));
end
if relatedG~=0
  for iii = 1:nrens
    AbG(:,:,iii,:) = AbG(:,:,iii,:) * ((m==im0(iii)) - (m~=im0(iii)));
  end
end
end %function

function [ksi,kpi,Ci] = theSub(sub,icr)
ksi 	= sub(icr).ksi;
kpi 	= sub(icr).kpi;
Ci    = sub(icr).Ci;
end