function [Xiz_uni,uni_ref,zrref] = psedoUnique(sub,DWNz,DWNh,z,l,ncapas,frac)

% con frac == 0 solo se agrupan profundidades idEnticas
for il = 1:ncapas
  if frac > 0
    lambda = (2*pi)/real(sub(il).ksi)/frac; % <-- empIrico
    
    % Indices de los receptores en el estrato (il):
    res = l == il;
    
%  Si el receptor estA en un estrato y la longitud de onda es mayor que el
%  espesor del estrato:
      if DWNh(il) > 0 && lambda > DWNh(il)
        z(res) = DWNz(il); % los coloca a todos en la interface de arriba
        continue 
      end
    
    % profundidades Unicas y ordenadas de menor a mayor:
    [zr0,~,izr0]  = unique(z(res));%zr0=zr(izr)  zr=zr0(izr0)
    % reviso para cada profundidad para recorrer (hacia arriba) las
    % profundidades cuya distancia a la anterior sea menor a lambda
    for i = 2:length(zr0)
      di = zr0(i)-zr0(i-1); % distancia entre dos profundidades unicas
      if (di < lambda) % si la distancia es muy pequeNa
        zr0(i) = zr0(i-1); % las junta (en la menor profundidad)
      end
    end
    % rehacer z con las profundidades amontonadas
    z(res) = zr0(izr0);
  end
end
[Xiz_uni,uni_ref,zrref] = unique(z,'stable');
end