function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo...
  (para,xr0,zr0,salu,sals,coordf,WFE)

% [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo2...
%   (para,xr0,zr0,salu,sals,coordf,WFE);
% return

% calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo. Deformacion plana en
% un espacio completo 2D con una cavidad circular.
% Metodo de expansion de funciones de onda de Mow y Pao en la iplementaci?n.
% de Perez Ruiz y SS en 2008.
% Util para:
% Obtener las funciones de Green en puntos de colocacion y receptores
% dada la fuente coordf (una columna de la matriz) en el ejercicio del
% tunel.
rcondMin = 1e-8;
%cantidad maxima de terminos en el campo difractado
nterm = 300;%ceil(5+ (35-5)*para.j/(para.nf/2)); 

nxs = 1; % una fuente
ixs = 1;
U2        = zeros(2,sum(salu),nxs);
S2        = zeros(2,2,sum(sals),nxs);
U1        = zeros(2,sum(salu),nxs);
S1        = zeros(2,2,sum(sals),nxs);

m = WFE.m; % medio donde se coloca el tunel
skip=false(2,1);
if isfield(WFE,'fij')
  fij=WFE.fij;
else
  fij=[1,1];
end
if abs(fij(1))<1e-10; skip(1)=true; end
if abs(fij(2))<1e-10; skip(2)=true; end
a = para.cont(m,3).a; % radio del tunel

% - propiedades del material
mmat = m;%mmat= para.subm1(m);
ksi  = para.reg(mmat).ksi;  % numeros de onda
kpi  = para.reg(mmat).kpi;
rho = para.reg(mmat).rho;  % densidad de masa
Ca = 2*pi*para.fj/kpi;% para.reg(mmat).alpha; % velocidades de propagacion
Cb = 2*pi*para.fj/ksi;% para.reg(mmat).bet;
A2B2 = Ca^2/Cb^2;
nu = para.reg(mmat).nu;    % Poisson
Ci  = para.reg(mmat).Ci;
mu = Ci(6,6);                 % Lame 1 (G de cortante)
lambda = (2*mu*nu)/(1-2*nu);  % Lame 2

% - posicion relativa al centro del tunel
% -- fuente
xs = coordf.x - para.cont(m,3).xa;
zs = coordf.z - para.cont(m,3).za;
rXi = (xs.^2 + zs.^2).^.5;
% -- receptores
xr = round(xr0 - para.cont(m,3).xa,5);
zr = round(zr0 - para.cont(m,3).za,5);
rrec = (xr.^2 + zr.^2).^.5;
enE = rrec > a;
nrec = length(xr);

% no hacer los receptores casi encima de la fuente         [integrar]    ?
% xij     = xr0-coordf.x;
% zij     = zr0-coordf.z;
% rij     = sqrt(xij.^2+zij.^2);
% jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% jj0 = coordf.i;

%% - sistema de referencia local
% -- angulos respecto al eje x', la fuente en (xF,0)
t1 = atan2(zs,xs); %sgn = t1/abs(t1); if t1 == 0; sgn=0;end
% -- direccion de las fuentes
THx = - t1;
THz = THx+pi/2;
TH = [THx, THz];
% -- posicion de los receptores en locales
threc = atan2(zr,xr) + THx;
% THx es el angulo entre las direcciones positivas de x, x'

%% testme
%en coordenadas globales:
% figure(54312); 
% if ~strcmp(get(gcf,'name'),'coordenadas globales')
%   % figura nueva
% hold on; t=linspace(0,2*pi);
% plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% plot(xs,zs,'r*'); % fuerza
% quiver(xs,zs,1,0,'r');
% quiver(xs,zs,0,1,'r');
% plot(xr,zr,'bo'); plot(xr(enE),zr(enE),'b.'); % receptores
% axis equal;grid on;xlabel('x');ylabel('z');%set(gca,'YDir','reverse')
% set(gcf,'name','coordenadas globales')
% clear t
% end
% % en coordenada locales (eje x'):
% figure(543122); 
% if ~strcmp(get(gcf,'name'),'coordenadas locales')
%   % figura nueva
% t=linspace(0,2*pi); 
% polar(threc,rrec,'bo');grid on; hold on
% polar(0,rXi,'r*');
% polar(t,a+zeros(1,100),'k-');
% quiver(rXi,0,1*cos(THx),1*sin(THx),'r');
% quiver(rXi,0,1*cos(THz),1*sin(THz),'r');
% set(gcf,'name','coordenadas locales')
% end
%% solucion  --------------------------------------------------------------
RES = zeros(5,2,nrec);
%           | `-- Fx, Fz
%           `-- ur,ut,srr,stt,srt
if true 
% - Calcular coeficientes de la expansion

EPS = 1;
for IN = 1:nterm
n = IN-1;
% -- funciones radiales en la superficie del tunel
r = a;
F_d = ondasRadialesE(ksi,kpi,A2B2,n,r,false);
F_i = ondasRadialesR(ksi,kpi,A2B2,n,r);
r = rXi;
HQ  = besselh(n  ,2,kpi*r);
HQ1 = besselh(n+1,2,kpi*r);
HK  = besselh(n  ,2,ksi*r);
HK1 = besselh(n+1,2,ksi*r);
% -- calculo de coeficientes
A = zeros(2,2,2); B=zeros(2,2); vec=zeros(2,2); 
% --- fz. horizontal
if ~skip(1)
A(1,1,1) = 2*mu*F_d(13); %SRR(P)
A(1,2,1) = 2*mu*F_d(8);  %SRR(S)
A(2,1,1) = 2*mu*F_d(7);  %SRT(P)
A(2,2,1) = 2*mu*F_d(6);  %SRT(S)
r = rXi;
HP  = ((n/(kpi*r)) * HQ - HQ1);
H   = ((n/(ksi*r)) * HK);
COEFP = -(1i/(4*rho*Ca^2*kpi)) * EPS * HP;
COEFS =  (1i/(4*rho*Cb^2*ksi)) * EPS * H;
B(1,1) = -COEFP*2*mu*F_i(13) - COEFS*2*mu*F_i(8); %SRR0(H)
B(2,1) = -COEFP*2*mu*F_i(7)  - COEFS*2*mu*F_i(6); %SRT0(H)
if rcond(A(:,:,1)) < rcondMin
  skip(1)=true;
else
  vec(:,1)=A(:,:,1)\B(:,1); %coef. ondas P; ondas S
end
end
if ~skip(2)
% --- fz. vertical
% A(1,1,2) = 2*mu*F_d(6)*(-1);  %SRT(S)
% A(1,2,2) = 2*mu*F_d(7);  %SRT(P)
% A(2,1,2) = 2*mu*F_d(8);  %SRR(S)
% A(2,2,2) = 2*mu*F_d(13)*(-1); %SRR(P)
A(1,1,2) = 2*mu*F_d(13)*(-1); %SRR(P)
A(1,2,2) = 2*mu*F_d(8);       %SRR(S)
A(2,1,2) = 2*mu*F_d(7);       %SRT(P)
A(2,2,2) = 2*mu*F_d(6)*(-1);  %SRT(S)
r = rXi;
HP  = ((n/(ksi*r)) * HK - HK1);
H   = ((n/(kpi*r)) * HQ);
COEFP =  (1/(4i*rho*Ca^2*kpi)) * EPS * H;
COEFS = -(1/(4i*rho*Cb^2*ksi)) * EPS * HP;
B(1,2) = -COEFP*2*mu*F_i(13) - COEFS*2*mu*F_i(8); %SRR0(V)
B(2,2) = -COEFP*2*mu*F_i(7)  - COEFS*2*mu*F_i(6)*(-1); %SRT0(V)
if rcond(A(:,:,2)) < rcondMin
  skip(2)=true;
else
  vec(:,2)=A(:,:,2)\B(:,2); %coef. ondas P; ondas S
end
end
% disp(['M:',num2str(M)])
% disp(rcond(A(:,:,1)))
% disp(vec(:,1).')
% disp('--')
% disp(rcond(A(:,:,2)))
% disp(vec(:,2).')
% disp('--')
if max(max(abs(vec))) < 1e-10
  break
end
if skip(1) && skip(2)
  break
end
% -- acumular campo difractado en receptores  
for ir = 1:nrec
  if ~enE(ir)
    RES(1:5,1:2,ir) = NaN; % dentro del tunel
    continue
  end
  r = rrec(ir);
  th = threc(ir);
  FE = ondasRadialesE(ksi,kpi,A2B2,n,r,true); %funciones de onda en rrec
  for dir = 1:2
    if skip(dir)
      continue
    end
    CO = cos( n*th + TH(dir));
    SI = sin(-n*th + TH(dir));
    if dir==2
      FE(1) = -FE(1);
      FE(2) = -FE(2);
    end
    %ur                             coef P   FE       +    coef S  FE
    RES(1,dir,ir) = RES(1,dir,ir)+vec(1,dir)*FE(1)*CO + vec(2,dir)*FE(10)*CO;
    
    %ut
    RES(2,dir,ir) = RES(2,dir,ir)+vec(1,dir)*FE(11)*SI + vec(2,dir)*FE(2)*SI;
    
    %srr
    RES(3,dir,ir) = RES(3,dir,ir)+0;
    
    %stt
    RES(4,dir,ir) = RES(4,dir,ir)+0;
    
    %srt
    RES(5,dir,ir) = RES(5,dir,ir)+0;
  end % dir
end% ir

EPS = 2;
end %n
fprintf('%s ',['n:',num2str(n)]);
%% testme
% set(0,'DefaultFigureWindowStyle','docked')
% nam ={'ur','ut','srr','stt','srt'};
% col ={'k-','b-','r-'};
% recran = nrec-300+1:nrec;
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:5
%   mx(ico) = max(squeeze(abs(RES(ico,1,recran))));
%   my(ico) = max(squeeze(abs(RES(ico,2,recran))));
% end
% 
% figure(10); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% figure(20); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% for ico = 1:2 % U
%   figure(10);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fx']);
%   figure(20);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fz']);
% end
% 
% figure(30); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% figure(40); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% for ico = 3:5 % S
%   figure(30);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fx']);
%   figure(40);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fz']);
% end
%% polares a rectangulares

% Se tienen desplazamientos y esfuerzos en coordenadas polares
% cambio a coordenadas rectangulares en el sistema x,y

for ir = 1:nrec
  an = threc(ir) -THx;
  m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
  m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
  
  % Fx
  dir = 1;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U1(1,ir,ixs) = a(1);
  U1(2,ir,ixs) = a(2);
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S1(1,1,ir,ixs) = a(1,1);
  S1(2,2,ir,ixs) = a(2,2);
  S1(1,2,ir,ixs) = a(1,2);
  % Fz
  dir = 2;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U2(1,ir,ixs) = a(1);
  U2(2,ir,ixs) = a(2);
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S2(1,1,ir,ixs) = a(1,1);
  S2(2,2,ir,ixs) = a(2,2);
  S2(1,2,ir,ixs) = a(1,2);
end
end

%% incidencia directa (funcion analitica y con itegracion gaussiana)--------
if true 
for ir = 1:nrec
  if rrec(ir) < 1.1*a
    RES(1:5,1:2,ir) = NaN;
    continue
  end
  
  xij     = xr0(ir)-coordf.x;
  zij     = zr0(ir)-coordf.z;
  rij     = sqrt(xij.^2+zij.^2);
  g(1,1)  = xij./rij;
  g(2,1)  = zij./rij;
  
  if salu(ir)==1
    if isfield(coordf,'vnx')
      if rij==0
        dr = coordf.dr;
        gn(1)=-coordf.vnz;
        gn(2)= coordf.vnx;
        Gij0 = Greenex_PSV(ksi,kpi,gn,Ci,dr);
      elseif rij<=1*para.npplo*dr
        Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,ksi,kpi,para.gaussian,Ci);
      else
        Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
      end
    else
      Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
    end
    U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
    U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
    U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
    U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
  end
  
  if sals(ir)==1
    if isfield(coordf,'vnx')
       dr = coordf.dr;
      if rij<=1*para.npplo*dr && rij~=0
        [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),ksi,kpi,Ci,[1 1],para.gaussian);
      elseif rij~=0
        [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
      end
    else
      [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
    end
    
    if rij~=0
      %Fx
      S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
      S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
      S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
      %Fz
      S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
      S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
      S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
    end
  end
end
end
%% testme
% nam ={'ur','ut','srr','stt','srt'};
% recran = 1:nrec;%nrec-300+1:nrec; %receptores de interes
% % recranlin = 1:30;
% % Deformacion por campo totales en cartesianas
% set(0,'DefaultFigureWindowStyle','docked')
% figure(54312); hold on
% % Fx
% sca=3;
% % h=plot(xr(recranlin)+real(U1(1,recranlin,ixs))*sca,...
% %   zr(recranlin)+real(U1(2,recranlin,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U1(1,recran,ixs)),...
%                                real(U1(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fx','visible','off','Color',[0.6350 0.0780 0.1840]);
% % Fz
% % h=plot(xr(recran)+real(U2(1,recran,ixs))*sca,...
% %   zr(recran)+real(U2(2,recran,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U2(1,recran,ixs)),...
%                                real(U2(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fz','visible','off','Color',[0.6350 0.0780 0.1840]);
% 
% %% desplazamientos y esfuerzos totales en polares
% Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% for ir = 1:nrec
%   % Fx
%   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
%   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
%   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
%           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,1) = a(1,1);
%   Spol(ir,2,1) = a(2,2);
%   Spol(ir,3,1) = a(1,2);
%   % Fz
%   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
%   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
%   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
%           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,2) = a(1,1);
%   Spol(ir,2,2) = a(2,2);
%   Spol(ir,3,2) = a(1,2);
% end
% 
% col ={'k-','b-','r-'};
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:2
%   mx(ico) = max(squeeze(abs(Upol(recran,ico,1))));
%   my(ico) = max(squeeze(abs(Upol(recran,ico,2))));
% end
% for ico = 1:3
%   mx(ico+2) = max(squeeze(abs(Spol(recran,ico,1))));
%   my(ico+2) = max(squeeze(abs(Spol(recran,ico,2))));
% end
% 
% figure(54312); hold on
% sca=sca/abs(lambda);
% for ico = 1:3 %   Srr, Stt, Srt
% % Fx
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,1))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','off');
% % Fz
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,2))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','off');
% end
% % en locales: 
% % figure(110); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(120); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% % figure(110);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% % figure(120);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% 
% % figure(130); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(140); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:3
% %   figure(130);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,1))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fx']);
% %   figure(140);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,2))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fz']);
% % end
%%
end

%%         -------------------- funciones ---------------------
% function F = ondasRadialesE(k,q,~,n,r,todos)
% % version MowPao71
% F = zeros(13,1);
% r2 = r^2;
% k2 = k^2;
% n2n = n^+n;
% kr = k*r;
% qr = q*r;
% K = 2;
% % los de P
% Z  = besselh(n  ,K,qr);
% Z1 = besselh(n-1,K,qr);
% F(13) = ((n2n - k2*r2/2)*Z - qr*Z1)/r2;  % e11  srr  phi cos F(13)
% F(7)  = (n2n*Z - qr*n*Z1)/r2;            % e41  srt  phi sin F(7)
% if todos
%   F(1) = (qr*Z1 - n*Z)/r;                 % e71  ur   phi cos F(1)
%   F(11)= (-n*Z)/r;                        % e81  ut   phi sin F(11)
%   F(3) = ((-n2n - k2*r2/2)*Z + qr*Z1)/r;  % e21  stt  phi cos 3,5,9?
% end
% 
% % los de S
% Z  = besselh(n  ,K,kr);
% Z1 = besselh(n-1,K,kr);
% F(8) = (-n2n*Z + n*kr*Z1)/r2;           % e12  srr  psi cos  F(8)
% F(6) = (-(n2n - k2*r2/2)*Z + kr*Z1)/r2; % e42  srt  psi sin y cos F(6)
% if todos
%   F(2) = (-kr*Z1 + n*Z)/r;                 % e82  ut   psi sin F(2)
%   F(10)= (n*Z)/r;                          % e72  ur   psi cos F(10)
%   F(4) = (n2n*Z - kr*n*Z1)/r2;             % e22  stt  psi cos 4?
% end
% end
% function F = ondasRadialesR(k,q,~,n,r)
% % version MowPao71
% F = zeros(13,1);
% r2 = r^2;
% k2 = k^2;
% n2n = n^2+n;
% kr = k*r;
% qr = q*r;
% % los de P
% Z  = besselj(n  ,qr);
% Z1 = besselj(n-1,qr);
% F(7)  = (n2n*Z - qr*n*Z1)/r2;            % e41  srt  phi sin F(7)
% F(13) = ((n2n - k2*r2/2)*Z - qr*Z1)/r2;  % e11  srr  phi cos F(13)
% 
% % los de S
% Z  = besselj(n  ,kr);
% Z1 = besselj(n-1,kr);
% F(6) = (-(n2n - k2*r2/2)*Z + kr*Z1)/r2; % e42  srt  psi sin y cos F(6)
% F(8) = (-n2n*Z + n*kr*Z1)/r2;           % e12  srr  psi cos  F(8)
% end


function F = ondasRadialesE(k,q,A2B2,n,r,todos)
F = zeros(13,1);
r2 = r^2;
k2 = k^2;
q2 = q^2;
n2_n = n^+n;%n^2-n;
% los de P
Z  = besselh(n  ,2,q*r);
% Z1 = besselh(n+1,2,q*r);
Z1 = besselh(n-1,2,q*r);
% F(7)  = (n2_n*Z - n*q*r*Z1)/r2;
F(7)  = ((n^2+n)*Z - n*q*r*Z1)/r2; %e41
% F(13) = ((n2_n - A2B2*q2*r2/2)*Z + q*r*Z1)/r2; % aguas con el +
% F(13) = ((n2_n - A2B2*q2*r2/2)*Z - q*r*Z1)/r2; % aguas con el +
F(13) = ((n^2+n - k2*r2/2)*Z - q*r*Z1)/r2; % e11
if todos
% F(1)  = n/r*Z - q*Z1;
F(1)  = q*Z1 - n/r*Z; % e71
F(3)  = (n2_n/r2 - q2)*Z + q/r*Z1; %aguas segundo +
% F(3)  = (n2_n/r2 - q2)*Z - q/r*Z1; %aguas segundo +
F(5)  = -q2*Z;
F(9)  = Z;
% F(11) = n/r*Z;
F(11) = -n/r*Z; % e81
end

% los de S
Z  = besselh(n  ,2,k*r);
% Z1 = besselh(n+1,2,k*r);
Z1 = besselh(n-1,2,k*r);
% F(6)  = ((n2_n - k2*r2/2)*Z + k*r*Z1)/r2; % aguas con el +
F(6)  = (-(n2_n + k2*r2/2)*Z + k*r*Z1)/r2; % e42
% F(6)  = ((n2_n - k2*r2/2)*Z - k*r*Z1)/r2; % aguas con el +
% F(8)  = (n2_n*Z - n*k*r*Z1)/r2;
F(8)  = ((n^2+n)*Z - n*k*r*Z1)/r2; %e12
if todos
% F(2)  = n/r*Z - k*Z1;
F(2)  = n/r*Z - k*Z1; %e82
F(4)  = (n2_n/r2 - k2)*Z + k/r*Z1; % aguas con el +
% F(4)  = (n2_n/r2 - k2)*Z - k/r*Z1; % aguas con el +
% F(10) = n/r*Z;
F(10) = -n/r*Z; % e72
end
end
function F = ondasRadialesR(k,q,A2B2,n,r)
F = zeros(13,1);
r2 = r^2;
k2 = k^2;
% q2 = q^2;
n2_n = n^2-n;
% los de P
Z  = besselj(n  ,q*r);
Z1 = besselj(n+1,q*r);
% Z1 = besselj(n-1,q*r);
% F(1)  = n/r*Z - q*Z1;
% F(3)  = (n2_n/r2 - q2)*Z + q/r*Z1; %aguas segundo +
% F(5)  = -q2*Z;
% F(7)  = (n2_n*Z - n*q*r*Z1)/r2;
F(7)  = ((n^2+n)*Z - n*q*r*Z1)/r2; %e41
% F(9)  = Z;
% F(11) = n/r*Z;
% F(13) = ((n2_n - A2B2*q2*r2/2)*Z + q*r*Z1)/r2; % aguas con el +
% F(13) = ((n2_n - A2B2*q2*r2/2)*Z - q*r*Z1)/r2; % aguas con el +
F(13) = ((n^2+n - k2*r2/2)*Z - q*r*Z1)/r2; % e11

% los de S
Z  = besselj(n  ,k*r);
Z1 = besselj(n+1,k*r);
% Z1 = besselj(n-1,k*r);
% F(2)  = n/r*Z - k*Z1;
% F(4)  = (n2_n/r2 - k2)*Z + k/r*Z1; % aguas con el +
% F(6)  = ((n2_n - k2*r2/2)*Z + k*r*Z1)/r2; % aguas con el +
F(6)  = (-(n2_n + k2*r2/2)*Z + k*r*Z1)/r2; % e42
% F(6)  = ((n2_n - k2*r2/2)*Z - k*r*Z1)/r2; % aguas con el +
% F(8)  = (n2_n*Z - n*k*r*Z1)/r2;
F(8)  = ((n^2+n)*Z - n*k*r*Z1)/r2; %e12
% F(10) = n/r*Z;
end



% function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo...
%   (para,xr0,zr0,salu,sals,coordf,WFE)
% 
% [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo0...
%   (para,xr0,zr0,salu,sals,coordf,WFE);
% return
% 
% % calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo. Deformacion plana en
% % un espacio completo 2D con un tunel circular.
% % Metodo de expansion de funciones de onda.
% % Util para:
% % Obtener las funciones de Green en puntos de colocacion y receptores
% % dada la fuente coordf (una columna de la matriz)
% 
% nterm = 100; %cantidad maxima de terminos en el campo difractado
% 
% nxs = 1; % una fuente
% ixs = 1;
% U2        = zeros(2,sum(salu),nxs);
% S2        = zeros(2,2,sum(sals),nxs);
% U1        = zeros(2,sum(salu),nxs);
% S1        = zeros(2,2,sum(sals),nxs);
% 
% m = WFE.m; % medio donde se coloca el tunel
% a = para.cont(m,3).a; % radio del tunel
% 
% % - propiedades del material
% mmat = m;%mmat= para.subm1(m);
% ksi  = para.reg(mmat).ksi;  % numeros de onda
% kpi  = para.reg(mmat).kpi;
% rho = para.reg(mmat).rho;  % densidad de masa
% Ca = para.reg(mmat).alpha; % velocidades de propagacion
% Cb = para.reg(mmat).bet;
% nu = para.reg(mmat).nu;    % Poisson
% Ci  = para.reg(mmat).Ci;
% mu = Ci(6,6);                 % Lame 1 (G de cortante)
% lambda = (2*mu*nu)/(1-2*nu);  % Lame 2
% 
% % test
% % warning('forced coordf')
% % coordf.x =   -5;
% % coordf.z =   10;
% 
% % - posicion relativa al centro del tunel
% % -- fuente
% xs = coordf.x - para.cont(m,3).xa;
% zs = coordf.z - para.cont(m,3).za;
% % -- receptores
% xr = round(xr0 - para.cont(m,3).xa,5);
% zr = round(zr0 - para.cont(m,3).za,5);
% rrec = (xr.^2 + zr.^2).^.5;
% threc = atan2(zr,xr);
% nrec        = length(xr);
% 
% % no hacer los receptores casi encima de la fuente         [integrar]    ?
% % xij     = xr0-coordf.x;
% % zij     = zr0-coordf.z;
% % rij     = sqrt(xij.^2+zij.^2);
% % jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% % jj0 = coordf.i;
% 
% %% testme
% %en coordenadas globales:
% r0 = (xs.^2 + zs.^2).^.5;
% t1 = atan2(zs,xs); 
% figure(54312); 
% if ~strcmp(get(gcf,'name'),'tunel')
%   % figura nueva
% hold on; t=linspace(0,2*pi);
% plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% plot(r0*cos(t1),r0*sin(t1),'r*'); % fuerza
% quiver(xs,zs,1,0,'r');
% quiver(xs,zs,0,1,'r');
% plot(xr,zr,'bo');axis equal;grid on; polar(threc,rrec,'b.'); % receptores
% xlabel('x');ylabel('z');set(gca,'YDir','reverse')
% set(gcf,'name','tunel')
% clear t
% end
% %% solucion  --------------------------------------------------------------
% RES = zeros(5,2,nrec);
% %           | `-- Fx, Fz
% %           `-- ur,ut,srr,stt,srt
% if true
% for ir = 1:nrec
%   %   if sum(ir == jj0) > 0 % saltarse el singular  ?
%   %     continue
%   %   end
%   
%   if rrec(ir) < a % nada dentro del tunel
%     RES(1:5,1:2,ir) = NaN;
%     continue
%   end
%   
%   % El campo incidente es distinto para cada th, el campo reflejado por
%   % un difractor circular se resuelve para cada receptor utilizando un
%   % punto de colocacion auxiliar con el mismo angulo. El campo difractado
%   % se aproxima con una serie de funciones de onda (WFE) cuyos coeficientes
%   % An y Bn se resuelven para cada n sucesivamente. Con n=0, u(d)_0= -u(i),
%   % con n=1, u(d)_1 = -u(i)-u(d)_0, ... 
%   
%   % desde el centro del tunel, angulo al receptor-real
%     th_tun = threc(ir);
%   % en ese angulo y en r=a se coloca un punto de colocacion para resolver
%   % condiciones de frontera: esfuerzos nulos [srr, srt]. El punto es:
%     xau = a * cos(th_tun);
%     zau = a * sin(th_tun);
%     xau = xau - xs; zau = zau - zs; % fuente -> punto de colocacion
%   % cuya distancia y cosenos directores desde la fuente son:
%     r_F = sqrt(xau^2 + zau^2);
%     gam_F(1) = xau/r_F;
%     gam_F(2) = zau/r_F;
%    
%   % campo incidente en r=a, th=th_tun dadas fuerzas Fx,Fz
%     u_i = incidencia(r_F,gam_F,th_tun,kpi,ksi,Ca,Cb,rho,mu,lambda);
%   
%   % campo difractado
%   % Cada termino de la serie de funciones de onda ajusta, sucesivamente,
%   % el remanente de la condicion de frontera libre.
%   seguir = true;
%   u_restante = -u_i([3,5],1:2); %[-srr -srt]
%   for n = 0:nterm % ciclo sobre la serie de funciones de onda
%     if ~seguir
%       break % salir for_n
%     end
%     % terminos t del campo difractado en polares [centrado en el tunel]
%     M = S_difractado(n,a,th_tun,kpi,ksi,mu,lambda);
%     
%     for dir = 1:2
%       % valor de los coeficientes que minimizan las tracciones
%       [A,B,seguir] = solucion(M,u_restante(:,dir),n);
%        
%       if seguir % [ur,uth,srr,srt,stt] [termino n]:
%         % campo difractado en receptor-real
%         u_dnew =difraccion(A,B,n,rrec(ir),th_tun,kpi,ksi,mu,lambda);
%         % acumular:
%         RES(1:5,dir,ir) = RES(1:5,dir,ir) + u_dnew;
%         % campo difractado en el punto de colocacion 
%         u_dnew =difraccion(A,B,n,a,th_tun,kpi,ksi,mu,lambda);
%         if max(abs(u_dnew)) < 1e-50
%           seguir = false; break
%         end
%         % nuevas tracciones restantes:
%         u_restante(:,dir) = u_restante(:,dir) - 0.95*u_dnew([3,5]);
%         % Note que el factor 0.95 promueve que la solucion se construya con
%         % mas terminos.
%       else
%         if seguir == false && dir==1
%           seguir = true; % para revisar en dir=2
%         end
%       end
%     end
%   end % n
% end % ir
% %% testme
% % set(0,'DefaultFigureWindowStyle','docked')
% % nam ={'ur','ut','srr','stt','srt'};
% % col ={'k-','b-','r-'};
% % recran = nrec-300+1:nrec;
% % mx=zeros(5,1);my=zeros(5,1);
% % for ico = 1:5
% %   mx(ico) = max(squeeze(abs(RES(ico,1,recran))));
% %   my(ico) = max(squeeze(abs(RES(ico,2,recran))));
% % end
% % 
% % figure(10); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(20); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% %   figure(10);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(20);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% % 
% % figure(30); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(40); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 3:5 % S
% %   figure(30);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(40);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% %% polares a rectangulares
% 
% % Se tienen desplazamientos y esfuerzos en coordenadas polares
% % cambio a coordenadas rectangulares en el sistema x,y
% 
% for ir = 1:nrec
%   an = threc(ir);
%   m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
%   m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
%   
%   % Fx
%   dir = 1;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U1(1,ir,ixs) = a(1);
%   U1(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S1(1,1,ir,ixs) = a(1,1);
%   S1(2,2,ir,ixs) = a(2,2);
%   S1(1,2,ir,ixs) = a(1,2);
%   % Fz
%   dir = 2;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U2(1,ir,ixs) = a(1);
%   U2(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S2(1,1,ir,ixs) = a(1,1);
%   S2(2,2,ir,ixs) = a(2,2);
%   S2(1,2,ir,ixs) = a(1,2);
% end
% end
% %% incidencia directa (funcion analitica y con itegracion gaussiana)--------
% if true
% for ir = 1:nrec
%   if rrec(ir) < 1.1*a
%     RES(1:5,1:2,ir) = NaN;
%     continue
%   end
%   
%   xij     = xr0(ir)-coordf.x;
%   zij     = zr0(ir)-coordf.z;
%   rij     = sqrt(xij.^2+zij.^2);
%   g(1,1)  = xij./rij;
%   g(2,1)  = zij./rij;
%   
%   if salu(ir)==1
%     if isfield(coordf,'vnx')
%       if rij==0
%         dr = coordf.dr;
%         gn(1)=-coordf.vnz;
%         gn(2)= coordf.vnx;
%         Gij0 = Greenex_PSV(ksi,kpi,gn,Ci,dr);
%       elseif rij<=1*para.npplo*dr
%         Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,ksi,kpi,para.gaussian,Ci);
%       else
%         Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
%       end
%     else
%       Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
%     end
%     U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
%     U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
%     U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
%     U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
% 
% %     U2(2,ir,ixs) =  Gij0(2,2); % test para solo incidencia analitica
% %     U2(1,ir,ixs) =  Gij0(1,2);
% %     U1(2,ir,ixs) =  Gij0(1,2);
% %     U1(1,ir,ixs) =  Gij0(1,1);
%   end
%   
%   if sals(ir)==1
%     if isfield(coordf,'vnx')
%        dr = coordf.dr;
%       if rij<=1*para.npplo*dr && rij~=0
%         [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),ksi,kpi,Ci,[1 1],para.gaussian);
%       elseif rij~=0
%         [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
%       end
%     else
%       [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
%     end
%     
%     if rij~=0
%       %Fx
%       S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
%       S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
%       S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
%       %Fz
%       S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
%       S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
%       S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
% 
% %       %Fx
% %       S1(2,2,ir,ixs) = S1o(2,2); % Szz
% %       S1(1,1,ir,ixs) = S1o(1,1); % Sxx
% %       S1(1,2,ir,ixs) = S1o(1,2); % Sxz
% %       %Fz
% %       S2(2,2,ir,ixs) = S2o(2,2);
% %       S2(1,1,ir,ixs) = S2o(1,1);
% %       S2(1,2,ir,ixs) = S2o(1,2);
%     end
%   end
% end
% end
% %% testme
% nam ={'ur','ut','srr','stt','srt'};
% recran = 1:nrec;%nrec-300+1:nrec; %receptores de interes
% % recranlin = 1:30;
% % Deformacion por campo totales en cartesianas
% set(0,'DefaultFigureWindowStyle','docked')
% figure(54312); hold on
% % Fx
% sca=10;
% % h=plot(xr(recranlin)+real(U1(1,recranlin,ixs))*sca,...
% %   zr(recranlin)+real(U1(2,recranlin,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U1(1,recran,ixs)),...
%                                real(U1(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fx','visible','off');
% % Fz
% % h=plot(xr(recran)+real(U2(1,recran,ixs))*sca,...
% %   zr(recran)+real(U2(2,recran,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U2(1,recran,ixs)),...
%                                real(U2(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fz','visible','off');
% 
% %% desplazamientos y esfuerzos totales en polares
% Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% for ir = 1:nrec
%   % Fx
%   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
%   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
%   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
%           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,1) = a(1,1);
%   Spol(ir,2,1) = a(2,2);
%   Spol(ir,3,1) = a(1,2);
%   % Fz
%   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
%   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
%   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
%           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,2) = a(1,1);
%   Spol(ir,2,2) = a(2,2);
%   Spol(ir,3,2) = a(1,2);
% end
% 
% col ={'k-','b-','r-'};
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:2
%   mx(ico) = max(squeeze(abs(Upol(recran,ico,1))));
%   my(ico) = max(squeeze(abs(Upol(recran,ico,2))));
% end
% for ico = 1:3
%   mx(ico+2) = max(squeeze(abs(Spol(recran,ico,1))));
%   my(ico+2) = max(squeeze(abs(Spol(recran,ico,2))));
% end
% 
% figure(54312); hold on
% sca=sca/abs(lambda);
% for ico = 1:3 %   Srr, Stt, Srt
% % Fx
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,1))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','off');
% % Fz
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,2))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','off');
% end
% % en locales: 
% % figure(110); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(120); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% % figure(110);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% % figure(120);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% 
% % figure(130); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(140); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:3
% %   figure(130);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,1))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fx']);
% %   figure(140);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,2))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fz']);
% % end
% %%
% end
% 
% %%         -------------------- funciones ---------------------
% function u_i = incidencia(r_F,gam_F,th_tun,kpi,ksi,alf,bet,rho,mu,lambda)
% % incidencia Calcula el campo incidente en coord. polares al tunel
% 
% % r_F, gam_F   : distancia y cosenos directores de fuente a receptor
% % r_tun,th_tun : distancia y angulo de cen.tunel a receptor
% % kpi,ksi      : numeros de onda de P y S
% % alf,bet      : velocidades de propagacion de P y S
% % rho          : densidad de masa
% % mu, lambda   : constantes de Lame
% 
% % coeficientes A,B,C,D
% co = coeficientes(alf,bet,kpi,ksi,r_F);
% % GyS en polares (respecto al centro del tunel)
% u_i = GyS_pol(r_F,gam_F,th_tun,co,rho,mu,lambda); %[ur,ut,srr,stt,srt]
% end
% function ond = ondsBessel(Ome_C,r) % H^(2)_nu (z) 
%   ond.H2_1 = besselh(1,2,Ome_C*r);
%   ond.H2_2 = besselh(2,2,Ome_C*r);
%   ond.H2_0 = 2/(Ome_C*r) * ond.H2_1 - ond.H2_2;  % relacion
% end
% function co = coeficientes(alf,bet,kpi,ksi,r)
% % coeficientes A,B,C,D de Sanchez-Sesma y Campillo BSSA 1991
% % H^(2)_nu (z)
% Ba = ondsBessel(kpi,r);
% Bb = ondsBessel(ksi,r);
% 
% a2 = alf^2;
% b2 = bet^2;
% co.A = Ba.H2_0/a2 + Bb.H2_0/b2;
% co.B = Ba.H2_2/a2 - Bb.H2_2/b2;
% co.Dq= (kpi*r * Ba.H2_1)/a2;
% co.Dk= (ksi*r * Bb.H2_1)/b2;
% co.C = co.Dq - co.Dk;
% end
% function u_i = GyS_pol(r,g,thNorm,co,rho,mu,lambda)
% % desplazamiento y esfuerzo en coordenadas polares [ur,ut,srr,stt,srt]
% 
% % 1- desplazamiento y tracciones en cartesianas (idem)
% u_i=zeros(5,2);% [ux,uz,sxx,szz,sxz]: x 2 direcciones FX,FZ
% I=eye(2,2);
% m1 = [[cos(thNorm) -sin(thNorm)];[sin(thNorm) cos(thNorm)]]; %para rotacion
% m2 = [[cos(thNorm) sin(thNorm)];[-sin(thNorm) cos(thNorm)]];
% 
% % Desplazamiento:
% u_i(1,1) = 1/(8i*rho)*(I(1,1)*co.A - (2*g(1)*g(1) - I(1,1))*co.B); %GxX
% u_i(2,1) = 1/(8i*rho)*(I(2,1)*co.A - (2*g(2)*g(1) - I(2,1))*co.B); %GzX
% u_i(1,2) = 1/(8i*rho)*(I(1,2)*co.A - (2*g(1)*g(2) - I(1,2))*co.B); %GxZ
% u_i(2,2) = 1/(8i*rho)*(I(2,2)*co.A - (2*g(2)*g(2) - I(2,2))*co.B); %Gzz
% for dir=1:2
%   u_i(1:2,dir) = m2*u_i(1:2,dir); % [ur,uth,...
% end
% 
% f1 = 1i*mu/(2*rho*r);
% f2 = co.B+(lambda*co.Dq)/(2*mu);
% f3 = co.B+(co.Dk/2);
% f4 = co.C-4*co.B;
% % f5 = g(1)*n(1)+g(2)*n(2);
% % % Tracciones
% % n(1) = cos(thNorm); % vector normal de la superficie
% % n(2) = sin(thNorm);
% % ii = 1; jj=1; nv_i(3,jj)= ... %Txx
% %   f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% % ii = 2; jj=1; nv_i(4,jj)= ... %Tzx
% %   f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% % ii = 1; jj=2; nv_i(3,jj)= ... %Txz
% %   f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% % ii = 2; jj=2; nv_i(4,jj)= ... %Tzz
% %   f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% % esfuerzos
% jj=1; %Fx
% n(1)=1; n(2)=0; ii =1;
% f5 = g(1)*n(1)+g(2)*n(2);
% Sxx = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% n(1)=0; n(2)=1; ii =1;
% f5 = g(1)*n(1)+g(2)*n(2);
% Sxz = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% n(1)=0; n(2)=1; ii =2;
% f5 = g(1)*n(1)+g(2)*n(2);
% Szz = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% S__x=[[Sxx Sxz];[Sxz Szz]];
% a = m2 * S__x * m1; % en polares
% u_i(3,jj)=a(1,1);%Srr
% u_i(4,jj)=a(2,2);%Stt
% u_i(5,jj)=a(1,2);%Srt
% 
% jj=2; %Fz
% n(1)=1; n(2)=0; ii =1;
% f5 = g(1)*n(1)+g(2)*n(2);
% Sxx = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% n(1)=0; n(2)=1; ii =1;
% f5 = g(1)*n(1)+g(2)*n(2);
% Sxz = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% n(1)=0; n(2)=1; ii =2;
% f5 = g(1)*n(1)+g(2)*n(2);
% Szz = f1*(f2*g(jj)*n(ii) + f3*(g(ii)*n(jj)+f5*I(ii,jj)) + f4*g(ii)*g(jj)*f5);
% S__z=[[Sxx Sxz];[Sxz Szz]];
% a = m2 * S__z * m1; % en polares
% u_i(3,jj)=a(1,1);%Srr
% u_i(4,jj)=a(2,2);%Stt
% u_i(5,jj)=a(1,2);%Srt
% end
% 
% function M = S_difractado(n,r,th,kpi,ksi,mu,lambda)
% % S_difractado Terminos A y B de los esfuerzos en polares, orden n
% Half = ondasbeselH(n,kpi*r);              %waves.Half = Half;
% Hbet = ondasbeselH(n,ksi*r);              %waves.Hbet = Hbet;
% onAlf = ondasTunelpsv(Half,kpi,r);        %waves.onAlf=onAlf;
% onBet = ondasTunelpsv(Hbet,ksi,r);        %waves.onBet=onBet;
% 
% % srr
% % dilatacion
% srrTerm1_rA =   cos(n*th)*(onAlf.t6 + (1/r)*onAlf.t3 -n^2/r^2 * Half.Hn);
% srrTerm2_rA =   cos(n*th)*onAlf.t6;
% srrTerm2_rB = n*cos(n*th)*onBet.t9;
% 
% srr_rA = lambda*srrTerm1_rA + 2*mu*srrTerm2_rA;
% srr_rB = 2*mu*srrTerm2_rB;
% 
% % srt
% srtTerm1_rA = (...
%         1/r  *( onAlf.t3 * (-n*sin(n*th)) ) ...
%        -1/r^2*( Half.Hn  * (-n*sin(n*th)) ) );
%       
% srtTerm2_rB = (...
%         1/r^2 *( Hbet.Hn   * (-n^2*sin(n*th)) )...
%         -r    *( onBet.t12 * sin(n*th)        ) );
%       
% srt_rA = mu*(2*srtTerm1_rA               );
% srt_rB = mu*(              srtTerm2_rB   );
% 
% M = [[srr_rA srr_rB];[srt_rA srt_rB]];
% end
% 
% function [A,B,seguir] = solucion(M,V,n)
% rcondMin = 1e-15; %1e-17;%
% seguir = true; A=0; B=0;
% if n==0 || sum(abs(M(1,:))) < 1e-100 * sum(abs(M(2,:)))
%   Msm = M(1,1);
%   Vsm = V(1);
%   A = Msm\Vsm;
%   B = 0;
% else
%   if rcond(M) < rcondMin
%     % cortar el calculo en este n
%     % si el sistema se resolvera con mucho error
%     seguir = false;
%   else
%     aux = M\V;
%     A = aux(1);
%     B = aux(2);
%   end
% end
% end
% 
% function u_d = difraccion(A,B,n,r,th,kpi,ksi,mu,lambda)
% Half = ondasbeselH(n,kpi*r);              %waves.Half = Half;
% Hbet = ondasbeselH(n,ksi*r);              %waves.Hbet = Hbet;
% onAlf = ondasTunelpsv(Half,kpi,r);        %waves.onAlf=onAlf;
% onBet = ondasTunelpsv(Hbet,ksi,r);        %waves.onBet=onBet;
% u_d=zeros(5,1);
% 
% % ur
% u_d(1) = ...
%   A*onAlf.t3  *cos(n*th)...
%   + 1/r*(B*Hbet.Hn*n *cos(n*th));
% % ut
% u_d(2) = ...
%   1/r*(A*Half.Hn *(-n*sin(n*th))) ...
%   - (B*onBet.t3*    sin(n*th) );
% 
% dilat_d = A *lambda*(...
%   onAlf.t6 + (1/r)*onAlf.t3 -n^2/r^2* Half.Hn )*cos(n*th);
% % srr
% u_d(3) = dilat_d + 2*mu*(A *onAlf.t6 + B *n*onBet.t9)* cos(n*th);
% % stt
% u_d(4) = dilat_d + A * 2*mu*(...
%   1/r*( onAlf.t3 +1/r*( Half.Hn*(-n^2)) )*cos(n*th) )...
%   + B * 2*mu*(...
%   1/r*(1/r*(Hbet.Hn*n) - (onBet.t3*n))*cos(n*th) );
% % srt
% u_d(5) = mu *(...
%   2*A*((1/r*onAlf.t3 -1/r^2*Half.Hn)      *(-n)*sin(n*th))...
%   +B*((-n^2/r^2*(Hbet.Hn) - r*onBet.t12)      *sin(n*th)) );
% 
% end
% function o = ondasbeselH(n,z)
% tipo = 2; % 
% o.n = n;
% o.Hn = besselh(n,tipo,z);
% o.H_n = besselh(-n,tipo,z);
% o.H1n = besselh(1+n,tipo,z);
% o.H1_n = besselh(1-n,tipo,z);
% o.Hn_1 = besselh(n-1,tipo,z);
% o.Hn_2 = besselh(n-2,tipo,z);
% o.H_1_n = besselh(-1-n,tipo,z);
% end
% function ond = ondasTunelpsv(o,k,r)
% n = o.n;
% ond.t1 = k*o.Hn - (1+n)/r * o.H1n;
% ond.t2 = k*o.H_n - (1-n)/r * o.H1_n;
% ond.t3 = k*o.Hn_1 - n/r * o.Hn;
% 
% ond.t4 = k^2*o.Hn_1 - (1+2*n)*k/r*o.Hn + ((1+n)^2+(1+n))/r^2*o.H1n;
% ond.t5 = k^2*o.H_1_n + (2*n-1)*k/r*o.H_n + ((1-n)^2+(1-n))/r^2*o.H1_n;
% ond.t6 = k^2*o.Hn_2 - (2*n-1)*k/r*o.Hn_1 + (n^2+n)/r^2*o.Hn;
% 
% ond.t7 = k/r*o.Hn - (2+n)/r^2*o.H1n;
% ond.t8 = k/r*o.H_n - (2-n)/r^2*o.H1_n;
% ond.t9 = k/r*o.Hn_1 - (n+1)/r^2*o.Hn;
% 
% ond.t10 = k^2/r*o.Hn_1 - k*2*(n+1)/r^2*o.Hn + (n+1)*(n+3)/r^3*o.H1n;
% ond.t11 = k^2/r*o.H_1_n + k*2*(n-1)/r^2*o.H_n + (n-1)*(n-3)/r^3*o.H1_n;
% ond.t12 = k^2/r*o.Hn_2 - 2*k*n/r^2*o.Hn_1 + (2*n+n^2)/r^3*o.Hn;
% end

%% para no perderlo:
% function val = JasSer(nu,z,t)
% % termino t de la serie ascendente de J_{nu}(z)
% val = (0.5*z)^nu * (-0.25*z^2)^t / (factorial(t) * gamma(nu+t+1));
% end
% function val = YasSer(nu,z,t)
% % termino t de la serie ascendente de Y_{nu}(z)
% if t <= nu-1
%   fac2 = factorial(nu-t-1)/factorial(t)*(0.25*z^2)^t;
%   val = -(0.5*z)^(-nu)/pi * fac2;
% else
%   val = 0;
% end
% if t == 0
%   val = val + 2/pi*log(0.5*z)*besselj(nu,z);
% end
% % termino t
% val = val -(0.5*z)^(nu)/pi*(psi(t+1)+psi(nu+t+1))*...
%   (-0.25*z^2)^t/factorial(t)/factorial(nu+t);
% end
% function val =  H2Ser(nu,z,t)
% Js = @JasSer;
% Ys = @YasSer;
% J  = Js( nu,z,t);
% Y  = Ys( nu,z,t);
% val = J - 1i*Y;
% end


% function val = H2SerLimit(nu,z,t,~,~)
% Js = @JasSer;
% nu = nu + 0.001;
% J  = Js( nu,z,t,0,0);
% J_n = (-1)^nu * J;
% val = 1i * csc(nu*pi) * (J_n - exp(nu*pi*1i)*J);
% end
% function val = H2Ser_Adicion(nu,Ome_C,geo,t,kmax)%(nu,z,t,v,geo,kmax)
% % termino t de la serie ascendente de H(2)_{nu}(z) 
% % trasladado v con teoremas de adicion.
% 
% % C = @H2Ser; % cualquier combinacion lineal de funciones de Bessel
% C = @H2_ana;
% % if geo{1}.a == 0
% %   C = @H2SerLimit;
% % end
% val=0; % suma para todos los triangulos
% 
% ntriangs = size(geo,2);
% for nt = 1:ntriangs % Porque el teorema de adicion se aplica a pacitos
%   u = Ome_C * geo{nt}.u;
%   v = Ome_C * geo{nt}.v;
%   a = geo{nt}.a;
% %   disp([abs(v*exp( 1i*a)) < abs(u),abs(v*exp(-1i*a)) < abs(u)])
%   w = geo{nt}.w;
%   %   X = geo{nt}.X;
%   
%   if floor(nu) == 0 || a == 0
%     % Usa Teorema de Graf,
%     % Note, cuando a = 0, degenera a Neumann
%     vals = zeros(kmax+1,1);
%     vals(1) = C(nu,u,t,0,0) * besselj(0,v);  %  k = 0
%     %   fac = 1/cos(nu*X); %= 1 si nu=0
%     
%     % fuck
%     for abk=1:kmax
%       k = -abk;
%       newval = C(nu-k,u,t,0,0)*(-1)^(nu+k) *besselj(k,v)*cos(k*a);
%       k = abk;
%       newval = C(nu+k,u,t,0,0)             *besselj(k,v)*cos(k*a) +newval;
%       if isnan(newval)
%         break %detener la serie
%       end
%       vals(k+1) = newval;
%       if k > 8 && abs(mean(vals(max(1,k-2):k+1))) < 0.05*abs(mean(vals(max(1,k-7):k-3)))
%         break % valor estable
%       end
%     end
%     val = sum(vals(1:k+1)); % el triangulo mas reciente
%   elseif floor(nu) > 0
%     % Teorema de Gegenbauer
%     fac = 2^nu*gamma(nu)*w^nu;
%     vals = zeros(kmax+1,1);
%     for k=0:kmax
%       Gp=0; %Polinomio de Gegenbauer
%       for m=0:k
%         Gp = Gp + cos((k-2*m)*a)*...
%           (gamma(nu+m)*gamma(nu+k-m))/(factorial(m)*factorial(k-m)*gamma(nu)^2);
%       end
%       newval = (nu+k)* C(nu+k,u,t,0,0)/u^nu *besselj(nu+k,v)/v^nu * Gp;
%       if isnan(newval)
%         break %detener la serie
%       end
%       vals(k+1) = newval;
%       if k > 10 && abs(mean(vals(max(1,k-2):k+1))) < 0.05*abs(mean(vals(max(1,k-7):k-3)))
%         break % valor estable
%       end
%     end
%     val = val + sum(vals(1:k+1))*fac;
%   else
%     error(' n < 0 me falta, tal vez usar Graf ');
%   end
% end
% end


% % para usar los teoremas de adicion r0 debe ser menor que r, asi que
% % los teoremas de adicion se aplican en dos o mas etapas. r0 = r01+r02+...
% % donde cada termino es menor que r.
%   geo =[];
%   it  = 1;
%   rat = 0.5;
%   geo{1}.u    = r;
%   %              .------ nf = 1 -----. .------- nf > 1 --------.
%   geo{1}.v    = r0 * ((r0+0.0001)<r) + rat*r * ((r0+0.0001)>=r);
%   geo{1}.a    = th;
%   geo{1}.w    = sqrt(r.^2 + geo{1}.v^2 - 2.*r.*geo{1}.v.*cos(geo{1}.a));
%   geo{1}.X    = asin(geo{1}.v*sin(th)./geo{1}.w);
%   rem = (r0 - geo{1}.v) * ((r0+0.0001)>=r);
%   
%   if th == 0
%     % Usar Graf con J y valor limite para Y.
%     % Con J, no hay restriccion |r0| < |r|
%     geo{1}.u    = r;
%     geo{1}.v    = r0;
%     geo{1}.a    = 0;
%     geo{1}.w    = sqrt(r.^2 + geo{1}.v^2 - 2.*r.*geo{1}.v.*cos(geo{1}.a));
%     geo{1}.X    = 0;
%   else
%   while rem>0
%     it = it + 1;
%     geo{it}.u = geo{it-1}.w;
%     geo{it}.v =  rem *(rem< 0.99*geo{it}.u) + ...
%        rat*geo{it}.u *(rem>=0.99*geo{it}.u);
%     geo{it}.a = geo{it-1}.a + geo{it-1}.X;
%     geo{it}.w = sqrt(geo{it}.u^2 + geo{it}.v^2 ...
%                 - 2* geo{it}.u   * geo{it}.v  *cos(geo{it}.a));
%     geo{it}.X = asin(geo{it}.v*sin(geo{it}.a)./geo{it}.w);
%     rem = rem - geo{it}.v;
% %     if geo{it}.a == 0 && geo{it}.X == 0 && geo{it}.w < 
%   end
%   end

% 
% function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo...
%   (para,xr0,zr0,salu,sals,coordf,WFE)
% 
% [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo5...
%   (para,xr0,zr0,salu,sals,coordf,WFE); return
% 
% % calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo planos en un espacio
% % completo 2D con un tunel circular.
% % Metodo de expansion de funciones de onda.
% % Se obtienen las funciones de Green en puntos de colocacion y receptores
% % dada la fuente coordf (una columna de la matriz)
% nmax = 12;
% rcondMin = 1e-15;%1e-3;
% 
% nxs = 1; % una fuente
% ixs = 1;
% U2        = zeros(2,sum(salu),nxs);
% S2        = zeros(2,2,sum(sals),nxs);
% U1        = zeros(2,sum(salu),nxs);
% S1        = zeros(2,2,sum(sals),nxs);
% 
% m = 2; % medio donde se coloca el tunel
% a = para.cont(m,3).a; % radio del tunel
% 
% % - propiedades del material
% mmat= para.subm1(m);
% Ci  = para.reg(mmat).Ci;
% alf  = para.reg(mmat).ksi;  %aqui alf y bet son numero de onda
% bet  = para.reg(mmat).kpi;
% rho = para.reg(mmat).rho;
% Ca = para.reg(mmat).alpha; %aqui Ca y Cb son velocidades de propagacion
% Cb = para.reg(mmat).bet;
% omega = WFE.omegac;
% mu = Ci(6,6);
% nu = para.reg(mmat).nu;
% lambda = (2*mu*nu)/(1-2*nu);
% 
% % - posicion relativa al centro del tunel
% % -- fuente
% 
% warning('forced coordf')
% coordf.x =   -4.5;
% coordf.z =   10;
% 
% xs = coordf.x - para.cont(m,3).xa;
% zs = coordf.z - para.cont(m,3).za;
% r0 = (xs.^2 + zs.^2).^.5;
% % -- receptores
% xr = round(xr0 - para.cont(m,3).xa,5);
% zr = round(zr0 - para.cont(m,3).za,5);
% rrec = (xr.^2 + zr.^2).^.5;
% nrec        = length(xr);
% 
% % no hacer los receptores casi encima de la fuente     ?
% % xij     = xr0-coordf.x;
% % zij     = zr0-coordf.z;
% % rij     = sqrt(xij.^2+zij.^2);
% % jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% jj0 = coordf.i;
% 
% % - angulos respecto al eje x' (ver dibujo)
% t1 = atan2(zs,xs); sgn = t1/abs(t1);
% % -- direccion de las fuentes
% THx = sgn*pi - t1;
% THz = THx+pi/2;
% % -- posicion de los receptores en locales
% threc = atan2(zr,xr) + THx;
% % THx es el angulo entre las direcciones positivas de x, x'
% %% testme
% % en coordenadas globales:
% figure(54312);hold on; t=linspace(0,2*pi);
% plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% plot(r0*cos(t1),r0*sin(t1),'r*'); % fuerza
% quiver(xs,zs,1,0,'r');
% quiver(xs,zs,0,1,'r');
% plot(xr,zr,'bo');axis equal;grid on; % receptores
% xlabel('x');ylabel('z');set(gca,'YDir','reverse')
% % % en coordenada locales:
% % figure(543122);clf; t=linspace(0,2*pi); polar(t,a+zeros(1,100),'k-'); hold on
% % polar(pi,r0,'r*');polar(threc,rrec,'bo');grid on;
% % quiver(-r0,0,1*cos(THx),1*sin(THx),'r');
% % quiver(-r0,0,1*cos(THz),1*sin(THz),'r');
% clear t
% %%
% 
% % campo difractado por la cavidad  ---------------------------------------
% 
% % inicializar variable de u^(d) para cada receptor, en polares
% nv_d= zeros(5,1); %difractado
% nv_i= zeros(5,1); %incidente calculado con la serie
% 
% RES = zeros(5,2,nrec);
% %           | `-- Fx, Fz
% %           `-- ur,ut,srr,stt,srt
% 
% facP = (-1i)/(4*rho*omega*Ca);
% facS = (-1i)/(4*rho*omega*Cb);
% for ir = 1:nrec
%   %   if sum(ir == jj0) > 0 % saltarse el singular  ?
%   %     continue
%   %   end
%   th = threc(ir);
%   % la solucion irremediablemente depende de theta
%   seguir = true;
%   
%   % - ciclo sobre el numero de onda
%   for n = 0:nmax
%     if ~seguir
%       break % salir for_n
%     end
%     % -- terminos comunes
%     Ja = besselj(n,alf*r0);
%     Jb = besselj(n,bet*r0);
%     JJCC = Jb/Ja * Ca/Cb;
%     
%     % para la condicion de frontera
%     r = a;
%     Half = ondasbeselH(n,alf*r);
%     Hbet = ondasbeselH(n,bet*r);
%     onAlf = ondasTunelpsv(Half,alf,r);
%     onBet = ondasTunelpsv(Hbet,bet,r);
%     
%     % para las fuerza en X, Z; (1) y (2) resp.
%     cp_(1) = cos((1+n)*th-THx);
%     cp_(2) = cos((1+n)*th-THz);
%     cn_(1) = cos((1-n)*th-THx);
%     cn_(2) = cos((1-n)*th-THz);
%     
%     sp_(1) = sin((1+n)*th-THx);
%     sp_(2) = sin((1+n)*th-THz);
%     sn_(1) = sin((1-n)*th-THx);
%     sn_(2) = sin((1-n)*th-THz);
%     bb=1;
%     
%     if n==0 % hace las veces 1/Newmann
%       cn_(1) = 0;
%       cn_(2) = 0;
%       sn_(1) = 0;
%       sn_(2) = 0;
% %       bb= 0;
%     end
%     
%     % Coeficientes de la expansion a partir de la condicion de frontera
%     for dir = 1:2 % ambas direcciones
%       cp = cp_(dir);
%       cn = cn_(dir);
%       sp = sp_(dir);
%       sn = sn_(dir);
%       
%       r = a; % para la condicion de frontera libre
%       
%       % sigma_rr
%       srrTerm1_i = facP*Ja*(... % dilatacion
%         (-1)^n * cp * (onAlf.t4 + (1/r)*onAlf.t1 + (1/r^2)*(-(1+n)^2 * Half.H1n ))...
%         + cn * (onAlf.t5 + (1/r)*onAlf.t2 + (1/r^2)*(-(1-n)^2 * Half.H1_n)) );
%       srrTerm1_rA = cos(n*th)*(onAlf.t6 + (1/r)*onAlf.t3 -n^2/r^2 * Half.Hn);
%       
%       srrTerm2_i = facP*Ja*(...
%         (-1)^n * cp * (onAlf.t4 + JJCC*(1+n)*onBet.t7)... % d2f/dr2 +
%         + cn * (onAlf.t5 + JJCC*(1-n)*onBet.t8));  % d(1/r dp/dt)/dr
%       srrTerm2_rA =   cos(n*th)*onAlf.t6;
%       srrTerm2_rB = n*cos(n*th)*onBet.t9;
%       
%       srr_i  = lambda*srrTerm1_i  + 2*mu*srrTerm2_i*bb;
%       srr_rA = lambda*srrTerm1_rA + 2*mu*srrTerm2_rA;
%       srr_rB = 2*mu*srrTerm2_rB;
%       
%       % sigma_rt
%       srtTerm1_i = facP*Ja*(... % 1/r d2f/dtdr - 1/r2 df/dt
%         1/r  *( (-1)^n*sp*(-1-n)*onAlf.t1 - (1-n)*sn*onAlf.t2  ) ...
%        -1/r^2*( (-1)^n*sp*(-1-n)*Half.H1n - (1-n)*sn*Half.H1_n ) );
%       srtTerm1_rA = (...
%         1/r  *( onAlf.t3 * (-n*sin(n*th)) ) ...
%        -1/r^2*( Half.Hn  * (-n*sin(n*th)) ) );
%       
%       srtTerm2_i = facP*Ja*JJCC*(... % 1/r2 d2p/dt2 - r d(1/r dp/dr)/dr
%         1/r^2 *(-(-1)^n*sp*(1+n)^2*Hbet.H1n - sn*(1-n)^2*Hbet.H1_n )...
%         -r    *( (-1)^n*sp*onBet.t10        + sn*onBet.t11 ) );
%       srtTerm2_rB = (...
%         1/r^2 *( Hbet.Hn   * (-n^2*sin(n*th)) )...
%         -r    *( onBet.t12 * sin(n*th)        ) );
%       
%       srt_i  = mu*(2*srtTerm1_i + srtTerm2_i*bb);
%       srt_rA = mu*(2*srtTerm1_rA               );
%       srt_rB = mu*(              srtTerm2_rB   );
%       
%       seguir = true;
%       if n==0 || sum(abs([srt_rA srt_rB])) < 1e-100 * sum(abs([srr_rA srr_rB]))
%         M = srr_rA;
%         V = -srr_i;
%         A = M\V;
%         B = 0;
%       else
%         M=[[srr_rA srr_rB]; [srt_rA srt_rB]];
%         if rcond(M) < rcondMin
%           % cortar el calculo en este n
%           % si el sistema se resolvera con mucho error
%           seguir = false;
%         else
%           V=[-srr_i ; -srt_i];
%           aux = M\V;
%           A = aux(1);
%           B = aux(2);
%         end
%       end
%       
%       if seguir == true %&& max(abs(A),abs(B)) >= margen
%         % Si la solucion es estable y la contribuicion significativa
%         
%         % acumular elementos mecanicos en el receptor
%         % NOTA: Solo campo difractado por la cavidad.
%         % quedan como comentario las expresiones para el campo incidente.
%         
%         r = rrec(ir);
%         Half = ondasbeselH(n,alf*r);
%         Hbet = ondasbeselH(n,bet*r);
%         onAlf = ondasTunelpsv(Half,alf,r);
%         onBet = ondasTunelpsv(Hbet,bet,r);
%         
%         % ur
%         nv_d(1) = ...
%                  A*onAlf.t3  *cos(n*th)...
%           + 1/r*(B*Hbet.Hn*n *cos(n*th));
%         nv_i(1) = ...
%                  facP*Ja*((-1)^n*cp*onAlf.t1       + cn*onAlf.t2) ...
%           + 1/r*(facS*Jb*((-1)^n*cp*(1+n)*Hbet.H1n + cn*(1-n)*Hbet.H1_n))*bb;
%         
%         % ut
%         nv_d(2) = ...
%           1/r*(A*Half.Hn *(-n*sin(n*th))) ...
%             - (B*onBet.t3*    sin(n*th) );
%         nv_i(2) = ...
%           1/r*(facP*Ja*((-1)^n*sp*(-1-n)*Half.H1n + sn*(-1+n)*Half.H1_n))...
%             - (facS*Jb*((-1)^n*sp*onBet.t1        + sn*onBet.t2))*bb;
%         
%         dilat_d = A *lambda*(...
%                       onAlf.t6 + (1/r)*onAlf.t3 -n^2/r^2* Half.Hn )*cos(n*th);
%         dilat_i = lambda * facP*Ja*(...
%           (-1)^n*cp* (onAlf.t4 + (1/r)*onAlf.t1 + (1/r^2)*(-(1+n)^2 * Half.H1n ))...
%                + cn* (onAlf.t5 + (1/r)*onAlf.t2 + (1/r^2)*(-(1-n)^2 * Half.H1_n)) );
%         
%         % srr
%         nv_d(3) = dilat_d + 2*mu*(A *onAlf.t6 + B *n*onBet.t9)* cos(n*th);
%         nv_i(3) = dilat_i + 2*mu* facP*Ja*(...
%           (-1)^n*cp* (onAlf.t4 + JJCC*(1+n)*onBet.t7*bb)...
%                + cn* (onAlf.t5 + JJCC*(1-n)*onBet.t8*bb) );
%         
%         % stt
%         nv_d(4) = dilat_d + A * 2*mu*(...
%            1/r*( onAlf.t3 +1/r*( Half.Hn*(-n^2)) )*cos(n*th) )...
%                           + B * 2*mu*(...
%            1/r*(1/r*(Hbet.Hn*n) - (onBet.t3*n))*cos(n*th) );
%         nv_i(4) = dilat_i + 2*mu*facP*Ja*(...
%            1/r*( (-1)^n*cp*(onAlf.t1 + 1/r*(-(1+n)^2*Half.H1n )) ...
%                       + cn*(onAlf.t2 + 1/r*(-(1-n)^2*Half.H1_n)) ) )...
%                           + 2*mu*facS*Jb*(...
%          + 1/r*( (-1)^n*cp*(1+n)* (1/r*Hbet.H1n  - onBet.t1) ...
%                       + cn*(1-n)* (1/r*Hbet.H1_n - onBet.t2)     ) )*bb;
%         % srt
%         nv_d(5) = mu *(...
%          2*A*((1/r*onAlf.t3 -1/r^2*Half.Hn)      *(-n)*sin(n*th))...
%           +B*((-n^2/r^2*(Hbet.Hn) - r*onBet.t12)      *sin(n*th)) );
%         
%         nv_i(5) = mu*facP*Ja*2*(...
%             sp *(-1)^n*(-1-n)*(1/r*onAlf.t1 -1/r^2*Half.H1n) ...
%           + sn *       (-1+n)*(1/r*onAlf.t2 -1/r^2*Half.H1_n) ) +...
%                   mu*facS*Jb*(...
%             sp *(-1)^n*(-(1+n)^2/r^2*Hbet.H1n  - r*onBet.t10) ...
%           + sn *       (-(1-n)^2/r^2*Hbet.H1_n - r*onBet.t11) )*bb;       
%         
%         % no usar resultados erroneos, si los hubiera
%         if sum(sum(isnan(nv_d))) > 0
%           continue
%         end
%         if sum(sum(isinf(nv_d))) > 0
%           continue
%         end
%         
%         % sumar termino a la serie    RES([ur,ut,srr,stt,srt],dir,ir)
%         RES(:,dir,ir) = RES(:,dir,ir) + nv_i(:);%  + nv_d(:) + nv_i(:);%nv_d(:) ;%+ nv_i(:);% 
%         
%       else
%         if seguir == false && dir==1
%           seguir = true; % para revisar en dir=2
%         end
%       end
%     end
%   end % n
%   
%   disp(['ir ',num2str(ir),' -n',num2str(n-1)])
% end % ir
% 
% % %% testme
% % set(0,'DefaultFigureWindowStyle','docked')
% % nam ={'ur','ut','srr','stt','srt'};
% % col ={'k-','b-','r-'};
% % recran = nrec-300+1:nrec;
% % mx=zeros(5,1);my=zeros(5,1);
% % for ico = 1:5
% %   mx(ico) = max(squeeze(abs(RES(ico,1,recran))));
% %   my(ico) = max(squeeze(abs(RES(ico,2,recran))));
% % end
% % 
% % figure(10); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(20); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% %   figure(10);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(20);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% % 
% % figure(30); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(40); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 3:5
% %   figure(30);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(40);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% %% polares a rectangulares
% 
% % Se tienen desplazamientos y esfuerzos en coordenadas polares
% % cambio a coordenadas rectangulares en el sistema x,y
% an = -THx; % de regreso
% m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
% m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
% for ir = 1:nrec
%   % Fx
%   dir = 1;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U1(1,ir,ixs) = a(1);
%   U1(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S1(1,1,ir,ixs) = a(1,1);
%   S1(2,2,ir,ixs) = a(2,2);
%   S1(1,2,ir,ixs) = a(1,2);
%   % Fz
%   dir = 2;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U2(1,ir,ixs) = a(1);
%   U2(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S2(1,1,ir,ixs) = a(1,1);
%   S2(2,2,ir,ixs) = a(2,2);
%   S2(1,2,ir,ixs) = a(1,2);
% end
% 
% % incidencia directa (funcion analitica y con itegracion gaussiana)--------
% for ir = 1:nrec
%   xij     = xr0(ir)-coordf.x;
%   zij     = zr0(ir)-coordf.z;
%   rij     = sqrt(xij.^2+zij.^2);
%   g(1,1)  = xij./rij;
%   g(2,1)  = zij./rij;
%   dr = coordf.dr;
%   
%   if salu(ir)==1
% %     if isfield(coordf,'vnx')
% %       if rij==0
% %         gn(1)=-coordf.vnz;
% %         gn(2)= coordf.vnx;
% %         Gij0 = Greenex_PSV(bet,alf,gn,Ci,dr);
% %       elseif rij<=1*para.npplo*dr
% %         Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,bet,alf,para.gaussian,Ci);
% %       else
% %         Gij0 = Gij_PSV(bet,alf,rij,g,Ci,1);
% %       end
% %     else
%       Gij0 = Gij_PSV(bet,alf,rij,g,Ci,1);
% %     end
% %     U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
% %     U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
% %     U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
% %     U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
% 
% %     U2(2,ir,ixs) =  Gij0(2,2);
% %     U2(1,ir,ixs) =  Gij0(1,2);
% %     U1(2,ir,ixs) =  Gij0(1,2);
% %     U1(1,ir,ixs) =  Gij0(1,1);
%   end
%   
%   if sals(ir)==1
% %     if isfield(coordf,'vnx')
% %       if rij<=1*para.npplo*dr && rij~=0
% %         [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),bet,alf,Ci,[1 1],para.gaussian);
% %       elseif rij~=0
% %         [S1o,S2o]=S_PSV(rij,g,bet,alf,Ci,[1 1]);
% %       end
% %     else
%       [S1o,S2o]=S_PSV(rij,g,bet,alf,Ci,[1 1]);
% %     end
%     
%     if rij~=0
% %       %Fx
% %       S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
% %       S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
% %       S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
% %       %Fz
% %       S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
% %       S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
% %       S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
% 
% 
% %       %Fx
% %       S1(2,2,ir,ixs) = S1o(2,2); % Szz
% %       S1(1,1,ir,ixs) = S1o(1,1); % Sxx
% %       S1(1,2,ir,ixs) = S1o(1,2); % Sxz
% %       %Fz
% %       S2(2,2,ir,ixs) = S2o(2,2);
% %       S2(1,1,ir,ixs) = S2o(1,1);
% %       S2(1,2,ir,ixs) = S2o(1,2);
%     end
%   end
% end
% 
% %% testme
% nam ={'ur','ut','srr','stt','srt'};
% recran = nrec-300+1:nrec; %receptores de interes
% 
% % Deformacion por campo totales en cartesianas
% set(0,'DefaultFigureWindowStyle','docked')
% figure(54312); hold on
% % Fx
% sca=1;
% h=plot(xr(recran)+real(U1(1,recran,ixs))*sca,...
%   zr(recran)+real(U1(2,recran,ixs))*sca,'b-');
% set(h,'DisplayName','x | Fx','visible','off');
% % Fz
% h=plot(xr(recran)+real(U2(1,recran,ixs))*sca,...
%   zr(recran)+real(U2(2,recran,ixs))*sca,'b-');
% set(h,'DisplayName','x | Fz','visible','off');
% 
% % desplazamientos y esfuerzos totales en polares
% Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% for ir = 1:nrec
%   % Fx
%   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
%   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
%   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
%           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,1) = a(1,1);
%   Spol(ir,2,1) = a(2,2);
%   Spol(ir,3,1) = a(1,2);
%   % Fz
%   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
%   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
%   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
%           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,2) = a(1,1);
%   Spol(ir,2,2) = a(2,2);
%   Spol(ir,3,2) = a(1,2);
% end
% 
% col ={'k-','b-','r-'};
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:2
%   mx(ico) = max(squeeze(abs(Upol(recran,ico,1))));
%   my(ico) = max(squeeze(abs(Upol(recran,ico,2))));
% end
% for ico = 1:3
%   mx(ico+2) = max(squeeze(abs(Spol(recran,ico,1))));
%   my(ico+2) = max(squeeze(abs(Spol(recran,ico,2))));
% end
% 
% figure(54312); hold on
% sca=sca/abs(lambda);
% for ico = 1:3 %   Srr, Stt, Srt
% % Fx
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,1))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','off');
% % Fz
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,2))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','off');
% end
% % en locales: 
% % figure(110); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(120); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% % figure(110);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% % figure(120);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% 
% % figure(130); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(140); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:3
% %   figure(130);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,1))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fx']);
% %   figure(140);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,2))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fz']);
% % end
% %%
% end
% 
% function ond = ondasbeselH(n,z)
% ond.n = n;
% tipo = 2;
% ond.Hn = besselh(n,tipo,z);
% ond.H_n = besselh(-n,tipo,z);
% ond.H1n = besselh(1+n,tipo,z);
% ond.H1_n = besselh(1-n,tipo,z);
% ond.Hn_1 = besselh(n-1,tipo,z);
% ond.Hn_2 = besselh(n-2,tipo,z);
% ond.H_1_n = besselh(-1-n,tipo,z);
% end
% 
% function ond = ondasTunelpsv(o,k,r)
% n = o.n;
% ond.t1 = k*o.Hn - (1+n)/r * o.H1n;
% ond.t2 = k*o.H_n - (1-n)/r * o.H1_n;
% ond.t3 = k*o.Hn_1 - n/r * o.Hn;
% 
% ond.t4 = k^2*o.Hn_1 - (1+2*n)*k/r*o.Hn + ((1+n)^2+(1+n))/r^2*o.H1n;
% ond.t5 = k^2*o.H_1_n + (2*n-1)*k/r*o.H_n + ((1-n)^2+(1-n))/r^2*o.H1_n;
% ond.t6 = k^2*o.Hn_2 - (2*n-1)*k/r*o.Hn_1 + (n^2+n)/r^2*o.Hn;
% 
% ond.t7 = k/r*o.Hn - (2+n)/r^2*o.H1n;
% ond.t8 = k/r*o.H_n - (2-n)/r^2*o.H1_n;
% ond.t9 = k/r*o.Hn_1 - (n+1)/r^2*o.Hn;
% 
% ond.t10 = k^2/r*o.Hn_1 - k*2*(n+1)/r^2*o.Hn + (n+1)*(n+3)/r^3*o.H1n;
% ond.t11 = k^2/r*o.H_1_n + k*2*(n-1)/r^2*o.H_n + (n-1)*(n-3)/r^3*o.H1_n;
% ond.t12 = k^2/r*o.Hn_2 - 2*k*n/r^2*o.Hn_1 + (2*n+n^2)/r^3*o.Hn;
% end


% INCIDENICA CON TEOREMA DE ADICION CORRECTO DONDE SEA
% function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo...
%   (para,xr0,zr0,salu,sals,coordf,WFE)
% 
% % calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo. Deformacion plana en
% % un espacio completo 2D con una cavidad circular.
% % Metodo de expansion de funciones de onda de Mow y Pao en la iplementaci?n.
% % de Perez Ruiz y SS en 2008.
% % Util para:
% % Obtener las funciones de Green en puntos de colocacion y receptores
% % dada la fuente coordf (una columna de la matriz) en el ejercicio del
% % tunel.
% % rcondMin = 1e-18;
% %cantidad maxima de terminos en el campo difractado
% nterm = 300;
% % ini = 90; fin = 90;
% % nterm = ceil(ini + (fin-ini)*para.j/(para.nf/2)); 
% 
% nxs = 1; % una fuente
% ixs = 1;
% U2        = zeros(2,sum(salu),nxs);
% S2        = zeros(2,2,sum(sals),nxs);
% U1        = zeros(2,sum(salu),nxs);
% S1        = zeros(2,2,sum(sals),nxs);
% 
% m = WFE.m; % medio donde se coloca el tunel
% skip=false(2,1);
% if isfield(WFE,'fij')
%   fij=WFE.fij;
% else
%   fij=[1,1];
% end
% if abs(fij(1))<1e-10; skip(1)=true; end
% if abs(fij(2))<1e-10; skip(2)=true; end
% a = para.cont(m,3).a; % radio del tunel
% 
% % - propiedades del material
% mmat = m;%mmat= para.subm1(m);
% ksi  = para.reg(mmat).ksi;  % numeros de onda
% kpi  = para.reg(mmat).kpi;
% % rho = para.reg(mmat).rho;  % densidad de masa
% Ca = para.reg(mmat).alpha; % velocidades de propagacion
% Cb = para.reg(mmat).bet;
% nu = para.reg(mmat).nu;    % Poisson
% Ci  = para.reg(mmat).Ci;
% mu = Ci(6,6);                 % Lame 1 (G de cortante)
% lambda = (2*mu*nu)/(1-2*nu);  % Lame 2
% 
% % - posicion relativa al centro del tunel
% % -- fuente
% xs = coordf.x - para.cont(m,3).xa;
% zs = coordf.z - para.cont(m,3).za;
% rFte = (xs.^2 + zs.^2).^.5;
% % -- agregar puntos de colocacion auxiliares
% nRR = length(xr0);
% nPC = 50; t=linspace(0,2*pi,nPC);
% xr0 = [a*cos(t) xr0];
% zr0 = [a*sin(t) zr0]; clear t
% % -- receptores reales y auxiliares
% xr = round(xr0 - para.cont(m,3).xa,5);
% zr = round(zr0 - para.cont(m,3).za,5);
% rrec = (xr.^2 + zr.^2).^.5;
% enE = rrec > a;
% nrec = length(xr);
% 
% % -- reptores en coordenadas locales de la fuente
% xij     = xr0-coordf.x;
% zij     = zr0-coordf.z;
% r_w     = (xij.^2+zij.^2).^.5;  % w = sqrt(u^2 + v^2 - 2*u*v*cos(ang_a));
% 
% % no hacer los receptores casi encima de la fuente         [integrar]    ?
% % xij     = xr0-coordf.x;
% % zij     = zr0-coordf.z;
% % rij     = sqrt(xij.^2+zij.^2);
% % jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% % jj0 = coordf.i;
% 
% %% - sistema de referencia local al tunel y eje x' alineado con la fuente
% % -- angulos respecto al eje x', la fuente en (xF,0)
% t1 = atan2(zs,xs); %sgn = t1/abs(t1); if t1 == 0; sgn=0;end
% % -- direccion de las fuentes
% THx = - t1;
% THz = THx+pi/2;
% TH = [THx, THz];
% % -- posicion de los receptores en locales al tunel
% threc = atan2(zr,xr);% en globales
% ang_a = threc + THx; % en locales
% % THx es el angulo entre las direcciones positivas de x, x'
% % -- angulo en locales de la fuente al receptor en el sistema x'
% threcF = atan2(zij,xij) + THx;
% 
% r_u = zeros(1,nrec);
% r_v = zeros(1,nrec);
% for ir = 1:nrec
%   if abs(rFte(ixs)*exp( 1i*ang_a(ir))) < rrec(ir) || ...
%      abs(rFte(ixs)*exp(-1i*ang_a(ir))) < rrec(ir) %  |v exp(+-i a)| < |u|
%     r_u(ir) = rrec(ir);
%     r_v(ir) = rFte;
%   else
%     r_u(ir) = rFte;
%     r_v(ir) = rrec(ir);
%   end
% end
% ang_x = asin(r_v./r_w .* sin(ang_a));
% %% testme
% % %figura en coordenadas globales:
% % figure(54312); 
% % if ~strcmp(get(gcf,'name'),'coordenadas globales')
% %   % figura nueva
% % hold on; t=linspace(0,2*pi);
% % plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% % plot(xs,zs,'r*'); % fuerza
% % quiver(xs,zs,1,0,'r');
% % quiver(xs,zs,0,1,'r');
% % plot(xr,zr,'bo'); plot(xr(enE),zr(enE),'b.'); % receptores
% % axis equal;grid on;xlabel('x');ylabel('z');%set(gca,'YDir','reverse')
% % set(gcf,'name','coordenadas globales')
% % clear t
% % end
% %% solucion  --------------------------------------------------------------
% RES = zeros(5,2,nrec);
% %           | `-- Fx, Fz
% %           `-- ur,ut,srr,stt,srt
% if true
% recN = zeros(1,nrec);
% 
% HHkv=cell(nrec,1); HHqv=cell(nrec,1);
% for ir = nrec:-1:1 % predefinir cantidad de terminos en la serie
% %   if ~enE(ir)
% %     RES(:,:,ir) = NaN;
% %     continue
% %   end
%   
% % -- primero conocer la cantidad de terminos para que el campo incidente
% % sea correcto en cada receptor. Depende de la distancia a la fuente
%   HHk_ana = besselh(0:2,2,ksi*r_w(ir)).';
%   obj = 0.01*max(abs(HHk_ana)); % 1/100 de la funcion correcta
%   Hk=zeros(3,1); 
%   for n = 0:nterm
%     HHkv{ir}{n+1}=transBessH2(ksi,r_u(ir),r_v(ir),r_w(ir),ang_a(ir),ang_x(ir),n);
%     HHqv{ir}{n+1}=transBessH2(kpi,r_u(ir),r_v(ir),r_w(ir),ang_a(ir),ang_x(ir),n);
%     Hk=Hk+HHkv{ir}{n+1};
%     if ir <= nPC
%       if n == max(recN(nPC+1:nrec))
%         recN(ir) = n;
%         break
%       end
%     else
%       if max(abs(Hk(1:3)-HHk_ana)) < obj
%         recN(ir) = n;
%         break
%       end
%     end
%   end
% end
% 
% RE = complex(zeros(5,1,1));
% B=cell(nrec,1); A= complex(zeros(2,2));
% for ir = 1:nrec
%   %% CAMPO INCIDENTE EN RECEPTORES:
%   for n = 0:recN(ir)
%     
%     % funciones H(2) con transporte (relativo al centro del tunel)
%     HHk = HHkv{ir}{n+1};
%     HHq = HHqv{ir}{n+1};
%     
%     % en coordenadas locales de la fuente
%     r = r_w(ir);
%     omeS = ksi*r;
%     omeP = kpi*r;
%     th   = threcF(ir);
%     
%     % Ur y Ut como en formulario de Kaussel
%     PH = 0.25i*((HHk(2)/omeS - (Cb/Ca)^2*HHq(2)/omeP) - HHk(1));
%     XI = 0.25i*((Cb/Ca)^2*HHq(3) - HHk(3));
%     % derivadas:
%     dPHr = PH/r + 1i/4/r*omeS*HHk(2);
%     dXIr = 1i/4/r*((Cb/Ca)^2*omeP*HHq(2) - omeS*HHk(2)) - 2*XI/r;
%     for dir = 1:2
%       if skip(dir)
%         continue
%       end
%       
%       RE(1) = 1/mu*(PH + XI)*cos(th-TH(dir)); %ur
%       RE(2) = 1/mu*PH*(-sin(th-TH(dir))); %ut
%       
%       dURr = 1/mu*(dPHr + dXIr)*cos(th-TH(dir));
%       dURt = 1/mu*(PH+XI)*(-sin(th));
%       dUTr = 1/mu*dPHr*(-sin(th-TH(dir)));
%       dUTt = 1/mu*PH*(-cos(th));
%       ep_vol = dURr + RE(1)/r + dUTt/r;
%       
%       RE(3) = lambda*ep_vol + 2*mu*dURr; %srr
%       RE(4) = lambda*ep_vol + 2*mu*(RE(1)/r + dUTt/r);  %stt
%       RE(5) = mu*(dURt/r + dUTr - RE(2)/r); %srt
%       
%       B{ir}{n+1}{dir}=[RE(3);RE(5)]; % <-- minimizar estos dos
%       
%       RES(:,dir,ir) =  RES(:,dir,ir) + RE;
%     end % dir
%   end % n
% end% ir
% 
% %% En cada n, ondas que minimizan las condicioens de frontear
% BB= complex(zeros(2*nPC,1));
% AA= complex(zeros(2*nPC,2));
% resi = zeros(nPC,2);
% for n=0:max(recN)  
%   %% CAMPO DIFRACTADO EN r=a
%   r = a;
%   F = ondasRadialesE(ksi,kpi,n,r,false);
%   %           (P)     (S)
%   A(1,:) = [F.e11, F.e12];  % Srr
%   A(2,:) = [F.e41, F.e42];  % Srt
%   A = 2*mu*A;
%   for dir = 1:2
%     if skip(dir); continue; end
%     srr=zeros(nPC,2);
%     for ir=1:nPC
%       BB(ir    )   = - B{ir}{n+1}{dir}(1) + resi(ir,1); % -srr
%       BB(ir+nPC)   = - B{ir}{n+1}{dir}(2) + resi(ir,2); % -srt
%       AA(ir    ,:) =   A(1,:)*cos(n*threc(ir)); % srr
%       AA(ir+nPC,:) =   A(2,:)*sin(n*threc(ir)); % srt
%       srr(ir,1)=B{ir}{n+1}{dir}(1);
%       srr(ir,2)=B{ir}{n+1}{dir}(2);
%     end
%     % resolver sistema sobredeterminado
%     V = pinv(AA)*BB;%  V = AA\BB;  %coef. ondas P; ondas S
%     
%     % CAMPO DIFRACTADO EN RECEPTORES
%     sss = zeros(nrec,2);
%     for ir = 1:nrec
%       if recN(ir) >= n
%         r = rrec(ir);
%         co = cos(n*threc(ir));
%         si = sin(n*threc(ir));
%         F = ondasRadialesE(ksi,kpi,n,r,true);
%         % acumular campo difractado en los
%         RE(1) =      (V(1)*F.e71 + V(2)*F.e72)*co;% ur   c
%         RE(2) =      (V(1)*F.e81 + V(2)*F.e82)*si;% ut   s
%         RE(3) = 2*mu*(V(1)*F.e11 + V(2)*F.e12)*co;% srr  c
%         RE(4) = 2*mu*(V(1)*F.e21 + V(2)*F.e22)*co;% stt  c
%         RE(5) = 2*mu*(V(1)*F.e41 + V(2)*F.e42)*si;% srt  s
%         sss(ir,1) = RE(3);
%         sss(ir,2) = RE(5);
%         RES(:,dir,ir) =  RES(:,dir,ir) + RE;
%       end
%     end
%     % residuo
%     resi(:,1) = srr(1:nPC,1)+sss(1:nPC,1);
%     resi(:,2) = srr(1:nPC,2)+sss(1:nPC,2);
% %     
% %     figure(222);clc;hold on; % incidente
% %     plot(real(srr(:,1)),'r','DisplayName',num2str(n))
% %     plot(imag(srr(:,1)),'b','DisplayName',num2str(n))
% %     
% %     figure(223);hold on;  % difractado
% %     plot(real(sss(1:nPC,1)),'r','DisplayName',num2str(n))
% %     plot(imag(sss(1:nPC,1)),'b','DisplayName',num2str(n))
% %     
% %     figure(224);hold on;  % residuo
% %     plot(real(srr(1:nPC,1)+sss(1:nPC,1)),'r','DisplayName',num2str(n))
% %     plot(imag(srr(1:nPC,1)+sss(1:nPC,1)),'b','DisplayName',num2str(n))
%   end
% end
% %% testme 
% % set(0,'DefaultFigureWindowStyle','docked')
% % nam ={'ur','ut','srr','stt','srt'};
% % col ={'k-','b-','r-'};
% % recran = 1:nPC;
% % mx=zeros(5,1);my=zeros(5,1);
% % for ico = 1:5
% %   mx(ico) = max(squeeze(abs(RES(ico,1,recran))));
% %   my(ico) = max(squeeze(abs(RES(ico,2,recran))));
% % end
% % 
% % figure(10); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(20); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% %   figure(10);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(20);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% % 
% % figure(30); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(40); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 3:5 % S
% %   figure(30);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fx']);
% %   figure(40);
% %   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% %% quitar auxiliares
% aux = RES(:,:,nPC+1:nrec);
% RES = aux; nrec = nRR;
% %% polares a rectangulares
% 
% % Se tienen desplazamientos y esfuerzos en coordenadas polares
% % cambio a coordenadas rectangulares en el sistema x,y
% 
% for ir = 1:nrec
%   an = threcF(ir)-THx; % <-- angulo en coordenadas de la fuente. 
%   m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
%   m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
%   
%   % Fx
%   dir = 1;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U1(1,ir,ixs) = a(1);
%   U1(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S1(1,1,ir,ixs) = a(1,1);
%   S1(2,2,ir,ixs) = a(2,2);
%   S1(1,2,ir,ixs) = a(1,2);
%   % Fz
%   dir = 2;
%   a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
%   U2(1,ir,ixs) = a(1);
%   U2(2,ir,ixs) = a(2);
%   a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
%   S2(1,1,ir,ixs) = a(1,1);
%   S2(2,2,ir,ixs) = a(2,2);
%   S2(1,2,ir,ixs) = a(1,2);
% end
% end
% 
% %% incidencia directa (funcion analitica y con itegracion gaussiana)--------
% if false
% for ir = 1:nrec
% %   if rrec(ir) < 1.1*a
% %     RES(1:5,1:2,ir) = NaN;
% %     continue
% %   end
%   
%   xij     = xr0(ir)-coordf.x;
%   zij     = zr0(ir)-coordf.z;
%   rij     = sqrt(xij.^2+zij.^2);
%   g(1,1)  = xij./rij;
%   g(2,1)  = zij./rij;
%   
%   if salu(ir)==1
% %     if isfield(coordf,'vnx')
% %       if rij==0
% %         dr = coordf.dr;
% %         gn(1)=-coordf.vnz;
% %         gn(2)= coordf.vnx;
% %         Gij0 = Greenex_PSV(ksi,kpi,gn,Ci,dr);
% %       elseif rij<=1*para.npplo*dr
% %         Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,ksi,kpi,para.gaussian,Ci);
% %       else
% %         Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
% %       end
% %     else
%       Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
% %     end
%     U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
%     U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
%     U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
%     U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
%   end
%   
%   if sals(ir)==1
%     if isfield(coordf,'vnx')
%        dr = coordf.dr;
%       if rij<=1*para.npplo*dr && rij~=0
%         [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),ksi,kpi,Ci,[1 1],para.gaussian);
%       elseif rij~=0
%         [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
%       end
%     else
%       [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
%     end
%     
%     if rij~=0
%       %Fx
%       S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
%       S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
%       S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
%       %Fz
%       S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
%       S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
%       S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
%     end
%   end
% end
% end
% %% testme
% % nam ={'ur','ut','srr','stt','srt'};
% % recran = 1:nrec;%nrec-300+1:nrec; %receptores de interes
% % % recranlin = 1:30;
% % % Deformacion por campo totales en cartesianas
% % set(0,'DefaultFigureWindowStyle','docked')
% % figure(54312); hold on
% % % Fx
% % sca=3;
% % % h=plot(xr(recranlin)+real(U1(1,recranlin,ixs))*sca,...
% % %   zr(recranlin)+real(U1(2,recranlin,ixs))*sca,'b-');
% % h=quiver(xr(recran),zr(recran),real(U1(1,recran,ixs)),...
% %                                real(U1(2,recran,ixs)),sca);
% % set(h,'DisplayName','x | Fx','visible','off','Color',[0.6350 0.0780 0.1840]);
% % % Fz
% % % h=plot(xr(recran)+real(U2(1,recran,ixs))*sca,...
% % %   zr(recran)+real(U2(2,recran,ixs))*sca,'b-');
% % h=quiver(xr(recran),zr(recran),real(U2(1,recran,ixs)),...
% %                                real(U2(2,recran,ixs)),sca);
% % set(h,'DisplayName','x | Fz','visible','off','Color',[0.6350 0.0780 0.1840]);
% % 
% % %% desplazamientos y esfuerzos totales en polares
% % Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% % Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% % for ir = 1:nrec
% %   % Fx
% %   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
% %   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
% %   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
% %           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
% %   Spol(ir,1,1) = a(1,1);
% %   Spol(ir,2,1) = a(2,2);
% %   Spol(ir,3,1) = a(1,2);
% %   % Fz
% %   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
% %   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
% %   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
% %           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
% %   Spol(ir,1,2) = a(1,1);
% %   Spol(ir,2,2) = a(2,2);
% %   Spol(ir,3,2) = a(1,2);
% % end
% % 
% % col ={'k-','b-','r-'};
% % mx=zeros(5,1);my=zeros(5,1);
% % for ico = 1:2
% %   mx(ico) = max(squeeze(abs(Upol(recran,ico,1))));
% %   my(ico) = max(squeeze(abs(Upol(recran,ico,2))));
% % end
% % for ico = 1:3
% %   mx(ico+2) = max(squeeze(abs(Spol(recran,ico,1))));
% %   my(ico+2) = max(squeeze(abs(Spol(recran,ico,2))));
% % end
% % 
% % figure(54312); hold on
% % sca=sca/abs(lambda);
% % for ico = 1:3 %   Srr, Stt, Srt
% % % Fx
% % h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,1))),col{ico}); 
% % set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','off');
% % % Fz
% % h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,2))),col{ico}); 
% % set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','off');
% % end
% % % en locales: 
% % % figure(110); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % % figure(120); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % % for ico = 1:2 % U
% % % figure(110);
% % %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% % % figure(120);
% % %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% % % end
% % 
% % % figure(130); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % % figure(140); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % % for ico = 1:3
% % %   figure(130);
% % %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,1))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fx']);
% % %   figure(140);
% % %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,2))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fz']);
% % % end
% end
% 
% %%         -------------------- funciones ---------------------
% function F = ondasRadialesE(k,q,n,r,todos)
%  % coeficientes a la Mow y Pao 1971. 
% % sin 2*mu en esfuerzos y 
% % con la opcion cos(n th) para srr y sin (n th) para srt
% F = [];
% r2 = r^2;
% k2 = k^2;
% n2n = n^2+n;
% qr = q*r;
% kr = k*r;
% K = 2;
% 
% % los de P
% Z  = besselh(n  ,K,qr);
% Z1 = besselh(n-1,K,qr); 
% F.e11 = ((n2n - k2*r2/2)*Z - qr*Z1)/r2;  % e11  srr  phi cos
% F.e41 = (n2n*Z - qr*n*Z1)/r2;            % e41  srt  phi sin
% % F.e41 = (-n2n*Z + qr*n*Z1)/r2;            % e41  srt  phi cos
% 
% % F(13) = ((n2_n - k2*r2/2)*Z + q*r*Z1)/r2;
% % F(7)  = (n2_n*Z - n*q*r*Z1)/r2;
% if todos 
% F.e21 = ((-n2n - k2*r2/2)*Z + qr*Z1)/r;  % e21  stt  phi cos
% F.e71 = (qr*Z1 - n*Z)/r;                 % e71  ur   phi cos
% F.e81 = (-n*Z)/r;                        % e81  ut   phi sin
% end
% 
% % los de S
% Z  = besselh(n  ,K,kr);
% Z1 = besselh(n-1,K,kr);
% F.e12  = (-n2n*Z + n*kr*Z1)/r2;           % e12  srr  psi cos
% F.e42  = (-(n2n - k2*r2/2)*Z + kr*Z1)/r2; % e42  srt  psi sin y cos
% % F(8)  = (n2_n*Z - n*k*r*Z1)/r2;
% % F(6)  = ((n2_n - k2*r2/2)*Z + k*r*Z1)/r2; 
% % F.e12  = (n2n*Z - n*kr*Z1)/r2;           % e12  srr  psi cos
% % F.e42  = ((n2n - k2*r2/2)*Z + kr*Z1)/r2; % e42  srt  psi sin y cos
% if todos
% F.e22 = (n2n*Z - kr*n*Z1)/r2;             % e22  stt  psi cos
% F.e72 = (n*Z)/r;                          % e72  ur   psi cos
% F.e82 = (-kr*Z1 + n*Z)/r;                 % e82  ut   psi sin
% end
% end
% 
% function HHH = transBessH2(wn,u,v,w,a,x,n)
% % transBessH2 funciones de besselH2 con traslacion v 
% % Usa forma degenerada de teorema de Graf (alfa y Xi == 0)
% % siempre y cuando  |v| < |u| 
% HHH=complex(zeros(3,1));
% K = 2;
% u = u*wn;
% v = v*wn;
% 
% eps = 1; if n>0 ; eps = 2; end
% s = (-1)^n;
% bj = besselj(n,v);
% 
% % H(2)_0
% HHH(1) = eps/2 * (besselh(n,K,u)*cos(n*a) + s*besselh(-n,K,u)*cos(-n*a))*bj;
% 
% if abs(cos(1*x)) >= cos(pi/4); fc = @cos; else fc = @sin; end
% fp=fc( n*a);
% fn=fc(-n*a);
% % H(2)_1
% HHH(2) = eps/2 * (besselh(1+n,K,u)*fp + s*besselh(1-n,K,u)*fn) * bj;
% HHH(2) = HHH(2)/fc(1*x);
% 
% HHH(3) = -HHH(1) + 2/wn/w*HHH(2);
% 
% % if abs(cos(2*x)) >= cos(pi/4); fc = @cos; else fc = @sin; end
% % fp=fc( n*a);
% % fn=fc(-n*a);
% % % H(2)_2
% % HHH(3) = eps/2 * (besselh(2+n,K,u)*fp + s*besselh(2-n,K,u)*fn) * bj
% % HHH(3) = HHH(3)/fc(2*x);
% end


function [U2,S2,U1,S1]=calcul_US_WFE_PSV_circulo2...
  (para,xr00,zr00,salu,sals,coordf,WFE)

% calcul_US_WFE_PSV_circulo Desplazamiento y esfuerzo. Deformacion plana en
% un espacio completo 2D con una cavidad circular.
% Metodo de expansion de funciones de onda de Mow y Pao en la iplementaci?n.
% de Perez Ruiz y SS en 2008.
% Util para:
% Obtener las funciones de Green en puntos de colocacion y receptores
% dada la fuente coordf (una columna de la matriz) en el ejercicio del
% tunel.
rcondMin = 1e-8;
%cantidad maxima de terminos en el campo difractado
nterm = 300;
% ini = 90; fin = 90;
% nterm = ceil(ini + (fin-ini)*para.j/(para.nf/2)); 

nxs = 1; % una fuente
ixs = 1;
U2        = zeros(2,sum(salu),nxs);
S2        = zeros(2,2,sum(sals),nxs);
U1        = zeros(2,sum(salu),nxs);
S1        = zeros(2,2,sum(sals),nxs);

m = WFE.m; % medio donde se coloca el tunel
skip=false(2,1);
if isfield(WFE,'fij')
  fij=WFE.fij;
else
  fij=[1,1];
end
if abs(fij(1))<1e-10; skip(1)=true; end
if abs(fij(2))<1e-10; skip(2)=true; end
a = para.cont(m,3).a; % radio del tunel

% - propiedades del material
mmat = m;%mmat= para.subm1(m);
ksi  = para.reg(mmat).ksi;  % numeros de onda
kpi  = para.reg(mmat).kpi;
% rho = para.reg(mmat).rho;  % densidad de masa
Ca = para.reg(mmat).alpha; % velocidades de propagacion
Cb = para.reg(mmat).bet;
nu = para.reg(mmat).nu;    % Poisson
Ci  = para.reg(mmat).Ci;
mu = Ci(6,6);                 % Lame 1 (G de cortante)
lambda = (2*mu*nu)/(1-2*nu);  % Lame 2

% - posicion relativa al centro del tunel
% -- fuente
xs = coordf.x - para.cont(m,3).xa;
zs = coordf.z - para.cont(m,3).za;
rFte = (xs.^2 + zs.^2).^.5;
% -- agregar puntos de colocacion auxiliares
nRR = length(xr00);
nPC = 50; t=linspace(0,2*pi,nPC);
xr0 = [a*cos(t) xr00];
zr0 = [a*sin(t) zr00]; clear t
% -- receptores reales y auxiliares
xr = round(xr0 - para.cont(m,3).xa,5);
zr = round(zr0 - para.cont(m,3).za,5);
rrec = (xr.^2 + zr.^2).^.5;
enE = rrec > a;
nrec = length(xr);

% -- reptores en coordenadas locales de la fuente
xij     = xr0-coordf.x;
zij     = zr0-coordf.z;
r_w     = (xij.^2+zij.^2).^.5;  % w = sqrt(u^2 + v^2 - 2*u*v*cos(ang_a));

% no hacer los receptores casi encima de la fuente         [integrar]    ?
% xij     = xr0-coordf.x;
% zij     = zr0-coordf.z;
% rij     = sqrt(xij.^2+zij.^2);
% jj0 = find(rij<=0.2*para.npplo*coordf.dr);
% jj0 = coordf.i;

% quitar receptores auxiliares de xr0 zr0
xr0 = xr00;
zr0 = zr00;
%% - sistema de referencia local al tunel y eje x' alineado con la fuente
% -- angulos respecto al eje x', la fuente en (xF,0)
t1 = atan2(zs,xs); %sgn = t1/abs(t1); if t1 == 0; sgn=0;end
% -- direccion de las fuentes
THx = - t1;
THz = THx+pi/2;
TH = [THx, THz];
% -- posicion de los receptores en locales al tunel
threc = atan2(zr,xr);% en globales
ang_a = threc + THx; % en locales
% THx es el angulo entre las direcciones positivas de x, x'
% -- angulo en locales de la fuente al receptor en el sistema x'
threcF = atan2(zij,xij) + THx;

r_u = zeros(1,nrec);
r_v = zeros(1,nrec);
for ir = 1:nrec
  if abs(rFte(ixs)*exp( 1i*ang_a(ir))) < rrec(ir) || ...
     abs(rFte(ixs)*exp(-1i*ang_a(ir))) < rrec(ir) %  |v exp(+-i a)| < |u|
    r_u(ir) = rrec(ir);
    r_v(ir) = rFte;
  else
    r_u(ir) = rFte;
    r_v(ir) = rrec(ir);
  end
end
ang_x = asin(r_v./r_w .* sin(ang_a));
%% testme
% %figura en coordenadas globales:
% figure(54312); 
% if ~strcmp(get(gcf,'name'),'coordenadas globales')
%   % figura nueva
% hold on; t=linspace(0,2*pi);
% plot(a*cos(t),a*sin(t),'k-');     % frontera libre del tunel
% plot(xs,zs,'r*'); % fuerza
% quiver(xs,zs,1,0,'r');
% quiver(xs,zs,0,1,'r');
% plot(xr,zr,'bo'); plot(xr(enE),zr(enE),'b.'); % receptores
% axis equal;grid on;xlabel('x');ylabel('z');%set(gca,'YDir','reverse')
% set(gcf,'name','coordenadas globales')
% clear t
% end
%% solucion  --------------------------------------------------------------
RES = zeros(5,2,nrec);
%           | `-- Fx, Fz
%           `-- ur,ut,srr,stt,srt
if true
recN = zeros(1,nrec);

HHkv=cell(nrec,1); HHqv=cell(nrec,1);
for ir = nrec:-1:1 % predefinir cantidad de terminos en la serie
%   if ~enE(ir)
%     RES(:,:,ir) = NaN;
%     continue
%   end
  
% -- primero conocer la cantidad de terminos para que el campo incidente
% sea correcto en cada receptor. Depende de la distancia a la fuente
  HHk_ana = besselh(0:2,2,ksi*r_w(ir)).';
  obj = 0.01*max(abs(HHk_ana)); % 1/100 de la funcion correcta
  Hk=zeros(3,1); 
  for n = 0:nterm
    HHkv{ir}{n+1}=transBessH2(ksi,r_u(ir),r_v(ir),r_w(ir),ang_a(ir),ang_x(ir),n);
    HHqv{ir}{n+1}=transBessH2(kpi,r_u(ir),r_v(ir),r_w(ir),ang_a(ir),ang_x(ir),n);
    Hk=Hk+HHkv{ir}{n+1};
    if ir <= nPC
      if n == max(recN(nPC+1:nrec))
        recN(ir) = n;
        break
      end
    else
      if max(abs(Hk(1:3)-HHk_ana)) < obj
        recN(ir) = n;
        break
      end
    end
  end
end

RE = complex(zeros(5,1,1));
B=cell(nrec,1); A= complex(zeros(2,2));
for ir = 1:nrec
  %% CAMPO INCIDENTE EN RECEPTORES:
  for n = 0:recN(ir) 
    % funciones H(2) con transporte (relativo al centro del tunel)
    HHk = HHkv{ir}{n+1};
    HHq = HHqv{ir}{n+1};
    
    % en coordenadas locales de la fuente
    r = r_w(ir);
    omeS = ksi*r;
    omeP = kpi*r;
    th   = threcF(ir);
    
    % Ur y Ut como en formulario de Kaussel
    PH = 0.25i*((HHk(2)/omeS - (Cb/Ca)^2*HHq(2)/omeP) - HHk(1));
    XI = 0.25i*((Cb/Ca)^2*HHq(3) - HHk(3));
    % derivadas:
    dPHr = PH/r + 1i/4/r*omeS*HHk(2);
    dXIr = 1i/4/r*((Cb/Ca)^2*omeP*HHq(2) - omeS*HHk(2)) - 2*XI/r;
    for dir = 1:2
      if skip(dir)
        continue
      end
      
      RE(1) = 1/mu*(PH + XI)*cos(th-TH(dir)); %ur
      RE(2) = 1/mu*PH*(-sin(th-TH(dir))); %ut
      
      dURr = 1/mu*(dPHr + dXIr)*cos(th-TH(dir));
      dURt = 1/mu*(PH+XI)*(-sin(th));
      dUTr = 1/mu*dPHr*(-sin(th-TH(dir)));
      dUTt = 1/mu*PH*(-cos(th));
      ep_vol = dURr + RE(1)/r + dUTt/r;
      
      RE(3) = lambda*ep_vol + 2*mu*dURr; %srr
      RE(4) = lambda*ep_vol + 2*mu*(RE(1)/r + dUTt/r);  %stt
      RE(5) = mu*(dURt/r + dUTr - RE(2)/r); %srt
      
      B{ir}{n+1}{dir}=[RE(3);RE(5)]; % <-- minimizar estos dos
      
      RES(:,dir,ir) =  RES(:,dir,ir) + RE;
    end % dir
  end % n
end% ir

if true % CAMPO DIFRACTADO 
%% En cada n, ondas que minimizan las condicioens de frontear
BB= complex(zeros(2*nPC,1));
AA= complex(zeros(2*nPC,2));
resi = zeros(nPC,2);
for n=0:max(recN)  
  %% CAMPO DIFRACTADO EN r=a
  r = a;
  F = ondasRadialesE(ksi,kpi,n,r,false);
  %           (P)     (S)
  A(1,:) = [F.e11, F.e12];  % Srr
  A(2,:) = [F.e41, F.e42];  % Srt
  A = 2*mu*A;
  if rcond(A) < rcondMin % eps
%     disp([para.j n rcond(A)]);
    break
  end
  if sum(sum(isinf(A)))>0
%     disp([para.j n sum(isinf(AA)) sum(isinf(BB))])  
    break
  end
  for dir = 1:2
    if skip(dir); continue; end
    srr=zeros(nPC,2);
    for ir=1:nPC
      BB(ir    )   = - B{ir}{n+1}{dir}(1) + resi(ir,1); % -srr
      BB(ir+nPC)   = - B{ir}{n+1}{dir}(2) + resi(ir,2); % -srt
      AA(ir    ,:) =   A(1,:)*cos(n*threc(ir)); % srr
      AA(ir+nPC,:) =   A(2,:)*sin(n*threc(ir)); % srt
      srr(ir,1)=B{ir}{n+1}{dir}(1);
      srr(ir,2)=B{ir}{n+1}{dir}(2);
    end
    % resolver sistema sobredeterminado
%     [n sum(isinf(AA)) sum(isinf(BB))]
    V = pinv(AA)*BB;%  V = AA\BB;  %coef. ondas P; ondas S
    
    % CAMPO DIFRACTADO EN RECEPTORES
    sss = zeros(nrec,2);
    for ir = 1:nrec
      if recN(ir) >= n
        r = rrec(ir);
        co = cos(n*threc(ir));
        si = sin(n*threc(ir));
        F = ondasRadialesE(ksi,kpi,n,r,true);
        % acumular campo difractado en los
        RE(1) =      (V(1)*F.e71 + V(2)*F.e72)*co;% ur   c
        RE(2) =      (V(1)*F.e81 + V(2)*F.e82)*si;% ut   s
        RE(3) = 2*mu*(V(1)*F.e11 + V(2)*F.e12)*co;% srr  c
        RE(4) = 2*mu*(V(1)*F.e21 + V(2)*F.e22)*co;% stt  c
        RE(5) = 2*mu*(V(1)*F.e41 + V(2)*F.e42)*si;% srt  s
        sss(ir,1) = RE(3);
        sss(ir,2) = RE(5);
        RES(:,dir,ir) =  RES(:,dir,ir) + RE;
      end
    end
    % residuo
    resi(:,1) = srr(1:nPC,1)+sss(1:nPC,1);
    resi(:,2) = srr(1:nPC,2)+sss(1:nPC,2);
%     
%     figure(222);clc;hold on; % incidente
%     plot(real(srr(:,1)),'r','DisplayName',num2str(n))
%     plot(imag(srr(:,1)),'b','DisplayName',num2str(n))
%     
%     figure(223);hold on;  % difractado
%     plot(real(sss(1:nPC,1)),'r','DisplayName',num2str(n))
%     plot(imag(sss(1:nPC,1)),'b','DisplayName',num2str(n))
%     
%     figure(224);hold on;  % residuo
%     plot(real(srr(1:nPC,1)+sss(1:nPC,1)),'r','DisplayName',num2str(n))
%     plot(imag(srr(1:nPC,1)+sss(1:nPC,1)),'b','DisplayName',num2str(n))
  end
end
end
%% testme
% set(0,'DefaultFigureWindowStyle','docked')
% nam ={'ur','ut','srr','stt','srt'};
% col ={'k-','b-','r-'};
% recran = 1:nPC;
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:5
%   mx(ico) = max(squeeze(abs(RES(ico,1,recran))));
%   my(ico) = max(squeeze(abs(RES(ico,2,recran))));
% end
% 
% figure(10); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% figure(20); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% for ico = 1:2 % U
%   figure(10);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fx']);
%   figure(20);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico}); set(h,'DisplayName',[nam{ico},'Fz']);
% end
% 
% figure(30); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% figure(40); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% for ico = 3:5 % S
%   figure(30);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,1,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fx']);
%   figure(40);
%   h=polar(threc(recran).',squeeze(abs(RES(ico,2,recran))),col{ico-2}); set(h,'DisplayName',[nam{ico},'Fz']);
% end
end

%% quitar auxiliares
aux = RES(:,:,nPC+1:nrec); RES = aux; threcF=threcF(nPC+1:nrec); nrec = nRR;

%% polares a rectangulares
% Se tienen desplazamientos y esfuerzos en coordenadas polares
% cambio a coordenadas rectangulares en el sistema x,y
for ir = 1:nrec
  an = threcF(ir)-THx; % <-- angulo en coordenadas de la fuente. 
  m1 = [[cos(an) -sin(an)];[sin(an) cos(an)]];
  m2 = [[cos(an) sin(an)];[-sin(an) cos(an)]];
  
  % Fx
  dir = 1;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U1(1,ir,ixs) = a(1);
  U1(2,ir,ixs) = a(2);
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S1(1,1,ir,ixs) = a(1,1);
  S1(2,2,ir,ixs) = a(2,2);
  S1(1,2,ir,ixs) = a(1,2);
  % Fz
  dir = 2;
  a = m1 * [RES(1,dir,ir);RES(2,dir,ir)];
  U2(1,ir,ixs) = a(1);
  U2(2,ir,ixs) = a(2);
  a = m1*[[RES(3,dir,ir) RES(5,dir,ir)];[RES(5,dir,ir) RES(4,dir,ir)]]*m2;
  S2(1,1,ir,ixs) = a(1,1);
  S2(2,2,ir,ixs) = a(2,2);
  S2(1,2,ir,ixs) = a(1,2);
end

%% incidencia directa (funcion analitica y con itegracion gaussiana)--------
if false
for ir = 1:nrec
%   if rrec(ir) < 1.1*a
%     RES(1:5,1:2,ir) = NaN;
%     continue
%   end
  
  xij     = xr0(ir)-coordf.x;
  zij     = zr0(ir)-coordf.z;
  rij     = sqrt(xij.^2+zij.^2);
  g(1,1)  = xij./rij;
  g(2,1)  = zij./rij;
  
  if salu(ir)==1
%     if isfield(coordf,'vnx')
%       if rij==0
%         dr = coordf.dr;
%         gn(1)=-coordf.vnz;
%         gn(2)= coordf.vnx;
%         Gij0 = Greenex_PSV(ksi,kpi,gn,Ci,dr);
%       elseif rij<=1*para.npplo*dr
%         Gij0 = Gij_PSV_r_small(coordf,xr0(ir),zr0(ir),1,ksi,kpi,para.gaussian,Ci);
%       else
%         Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
%       end
%     else
      Gij0 = Gij_PSV(ksi,kpi,rij,g,Ci,1);
%     end
    U2(2,ir,ixs) = U2(2,ir,ixs) + Gij0(2,2);
    U2(1,ir,ixs) = U2(1,ir,ixs) + Gij0(1,2);
    U1(2,ir,ixs) = U1(2,ir,ixs) + Gij0(1,2);
    U1(1,ir,ixs) = U1(1,ir,ixs) + Gij0(1,1);
  end
  
  if sals(ir)==1
    if isfield(coordf,'vnx')
       dr = coordf.dr;
      if rij<=1*para.npplo*dr && rij~=0
        [S1o,S2o]=S_PSV_r_small_2(coordf,xr0(ir),zr0(ir),ksi,kpi,Ci,[1 1],para.gaussian);
      elseif rij~=0
        [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
      end
    else
      [S1o,S2o]=S_PSV(rij,g,ksi,kpi,Ci,[1 1]);
    end
    
    if rij~=0
      %Fx
      S1(2,2,ir,ixs) = S1(2,2,ir,ixs) + S1o(2,2); % Szz
      S1(1,1,ir,ixs) = S1(1,1,ir,ixs) + S1o(1,1); % Sxx
      S1(1,2,ir,ixs) = S1(1,2,ir,ixs) + S1o(1,2); % Sxz
      %Fz
      S2(2,2,ir,ixs) = S2(2,2,ir,ixs) + S2o(2,2);
      S2(1,1,ir,ixs) = S2(1,1,ir,ixs) + S2o(1,1);
      S2(1,2,ir,ixs) = S2(1,2,ir,ixs) + S2o(1,2);
    end
  end
end
end
%% testme
% nam ={'ur','ut','srr','stt','srt'};
% recran = 1:nrec;%nrec-300+1:nrec; %receptores de interes
% % recranlin = 1:30;
% % Deformacion por campo totales en cartesianas
% set(0,'DefaultFigureWindowStyle','docked')
% figure(54312); hold on
% % Fx
% sca=3;
% % h=plot(xr(recranlin)+real(U1(1,recranlin,ixs))*sca,...
% %   zr(recranlin)+real(U1(2,recranlin,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U1(1,recran,ixs)),...
%                                real(U1(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fx','visible','off','Color',[0.6350 0.0780 0.1840]);
% % Fz
% % h=plot(xr(recran)+real(U2(1,recran,ixs))*sca,...
% %   zr(recran)+real(U2(2,recran,ixs))*sca,'b-');
% h=quiver(xr(recran),zr(recran),real(U2(1,recran,ixs)),...
%                                real(U2(2,recran,ixs)),sca);
% set(h,'DisplayName','x | Fz','visible','off','Color',[0.6350 0.0780 0.1840]);
% 
% %% desplazamientos y esfuerzos totales en polares
% Spol = zeros(nrec,3,2); % ( Srr, Stt, Srt ) x dir
% Upol = zeros(nrec,2,2); % ( Ur , Ut ) x dir
% for ir = 1:nrec
%   % Fx
%   Upol(ir,1,1) = m2(1,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ur
%   Upol(ir,2,1) = m2(2,:) * [U1(1,ir,ixs);U1(2,ir,ixs)]; %ut
%   a = m2*[[S1(1,1,ir,ixs) S1(1,2,ir,ixs)];...
%           [S1(1,2,ir,ixs) S1(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,1) = a(1,1);
%   Spol(ir,2,1) = a(2,2);
%   Spol(ir,3,1) = a(1,2);
%   % Fz
%   Upol(ir,1,2) = m2(1,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ur
%   Upol(ir,2,2) = m2(2,:) * [U2(1,ir,ixs);U2(2,ir,ixs)]; %ut
%   a = m2*[[S2(1,1,ir,ixs) S2(1,2,ir,ixs)];...
%           [S2(1,2,ir,ixs) S2(2,2,ir,ixs)]]*m1;
%   Spol(ir,1,2) = a(1,1);
%   Spol(ir,2,2) = a(2,2);
%   Spol(ir,3,2) = a(1,2);
% end
% 
% col ={'k-','b-','r-'};
% mx=zeros(5,1);my=zeros(5,1);
% for ico = 1:2
%   mx(ico) = max(squeeze(abs(Upol(recran,ico,1))));
%   my(ico) = max(squeeze(abs(Upol(recran,ico,2))));
% end
% for ico = 1:3
%   mx(ico+2) = max(squeeze(abs(Spol(recran,ico,1))));
%   my(ico+2) = max(squeeze(abs(Spol(recran,ico,2))));
% end
% 
% figure(54312); hold on
% sca=sca/abs(lambda);
% for ico = 1:3 %   Srr, Stt, Srt
% % Fx
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,1))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fx'],'visible','off');
% % Fz
% h = polar(threc(recran).'-THx,rrec(recran).'+sca*squeeze(abs(Spol(recran,ico,2))),col{ico}); 
% set(h,'DisplayName',[nam{ico+2},'Fz'],'visible','off');
% end
% % en locales: 
% % figure(110); set(gcf,'name','U Fx'); h1=polar(0,max(mx(1),mx(2)),'y.'); hold on;set(h1,'visible','off')
% % figure(120); set(gcf,'name','U Fz'); h2=polar(0,max(my(1),my(2)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:2 % U
% % figure(110);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,1))),col{ico});set(h,'DisplayName',[nam{ico},'Fx']);
% % figure(120);
% %  h=polar(threc(recran).',squeeze(abs(Upol(recran,ico,2))),col{ico});set(h,'DisplayName',[nam{ico},'Fz']);
% % end
% 
% % figure(130); set(gcf,'name','S Fx'); h1=polar(0,max(mx(3:5)),'y.'); hold on;set(h1,'visible','off')
% % figure(140); set(gcf,'name','S Fz'); h2=polar(0,max(my(3:5)),'y.'); hold on;set(h2,'visible','off')
% % for ico = 1:3
% %   figure(130);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,1))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fx']);
% %   figure(140);
% %   h=polar(threc(recran).',squeeze(abs(Spol(recran,ico,2))),col{ico}); set(h,'DisplayName',[nam{ico+2},'Fz']);
% % end
end

%%         -------------------- funciones ---------------------
function F = ondasRadialesE(k,q,n,r,todos)
 % coeficientes a la Mow y Pao 1971. 
% sin 2*mu en esfuerzos y 
% con la opcion cos(n th) para srr y sin (n th) para srt
F = [];
r2 = r^2;
k2 = k^2;
n2n = n^2+n;
qr = q*r;
kr = k*r;
K = 2;

% los de P
Z  = besselh(n  ,K,qr);
Z1 = besselh(n-1,K,qr); 
F.e11 = ((n2n - k2*r2/2)*Z - qr*Z1)/r2;  % e11  srr  phi cos F(13)
F.e41 = (n2n*Z - qr*n*Z1)/r2;            % e41  srt  phi sin F(7)
if todos 
F.e21 = ((-n2n - k2*r2/2)*Z + qr*Z1)/r;  % e21  stt  phi cos 3,5,9?
F.e71 = (qr*Z1 - n*Z)/r;                 % e71  ur   phi cos F(1)
F.e81 = (-n*Z)/r;                        % e81  ut   phi sin F(11)
end

% los de S
Z  = besselh(n  ,K,kr);
Z1 = besselh(n-1,K,kr);
F.e12  = (-n2n*Z + n*kr*Z1)/r2;           % e12  srr  psi cos  F(8)
F.e42  = (-(n2n - k2*r2/2)*Z + kr*Z1)/r2; % e42  srt  psi sin y cos F(6)
if todos
F.e22 = (n2n*Z - kr*n*Z1)/r2;             % e22  stt  psi cos 4?
F.e72 = (n*Z)/r;                          % e72  ur   psi cos F(10)
F.e82 = (-kr*Z1 + n*Z)/r;                 % e82  ut   psi sin F(2)
end
end

function HHH = transBessH2(wn,u,v,w,a,x,n)
% transBessH2 funciones de besselH2 con traslacion v 
% Usa forma degenerada de teorema de Graf (alfa y Xi == 0)
% siempre y cuando  |v| < |u| 
HHH=complex(zeros(3,1));
K = 2;
u = u*wn;
v = v*wn;

eps = 1; if n>0 ; eps = 2; end
s = (-1)^n;
bj = besselj(n,v);

% H(2)_0
HHH(1) = eps/2 * (besselh(n,K,u)*cos(n*a) + s*besselh(-n,K,u)*cos(-n*a))*bj;

if abs(cos(1*x)) >= cos(pi/4); fc = @cos; else fc = @sin; end
fp=fc( n*a);
fn=fc(-n*a);
% H(2)_1
HHH(2) = eps/2 * (besselh(1+n,K,u)*fp + s*besselh(1-n,K,u)*fn) * bj;
HHH(2) = HHH(2)/fc(1*x);

HHH(3) = -HHH(1) + 2/wn/w*HHH(2);

% if abs(cos(2*x)) >= cos(pi/4); fc = @cos; else fc = @sin; end
% fp=fc( n*a);
% fn=fc(-n*a);
% % H(2)_2
% HHH(3) = eps/2 * (besselh(2+n,K,u)*fp + s*besselh(2-n,K,u)*fn) * bj
% HHH(3) = HHH(3)/fc(2*x);
end