function plotwiggle(h,x,tr)
% PlotWig: Graficar trazas coloradas como los sismologos
axes(h);

m = mean(tr);
paso = min(2^4,length(tr)/200);paso = max(paso,1);
tr = tr(1:paso:end);
x  =  x(1:paso:end);

trP = tr;
trP(trP < m) = m;
fill( x , trP,  'k','edgecolor','b');

trN = tr;
trN(trN > m) = m;
fill( x , trN,  'r','edgecolor','none');