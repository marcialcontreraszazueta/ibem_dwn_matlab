function spectre=correction_spectre(para,nfN,df)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% correction spectre espace et temps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

w   = 2*pi*(df*((1:nfN)-1));
%%%%%%%%%%
% tiempo %
%%%%%%%%%%
if para.pulso.tipo==1 % file
    disp('input file')
    [a,b] = uigetfile;
    para.arch_pulso = [b,a];
    dt=(1/(df*2*nfN)*(2*nfN/(2*nfN-2))); % dt simul
    pulsoinput=load(para.arch_pulso);
    dt0=pulsoinput(2,1)-pulsoinput(1,1);
    if dt0>dt
        inputsignal=resample(pulsoinput(:,2),round(dt0/dt),1);
    else
        inputsignal=resample(pulsoinput(:,2),1,round(dt/dt0));
    end
    cr=fft(inputsignal,2*nfN+1);
    spectre=cr(1:nfN);
    spectre=spectre.*exp(1i*w*(para.pulso.c));
elseif para.pulso.tipo==2 %Ricker duracion total de la ondicula
    dt=(1/(df*2*nfN)*(nfN/(nfN-1)));
    tp=0.5*para.pulso.a;
    cr=ric(1:(2*nfN-2),dt,tp);
    cr=2*(2*nfN-2)*ifft(0.5*cr);
    spectre=cr(1:nfN);
    spectre(1)=0;
    spectre=spectre.*exp(1i*w*(para.pulso.c));  
elseif para.pulso.tipo==2.5 % Campana de Gauss
    spectre=ones(1,nfN);
    % media
    me = para.pulso.a*2*pi;
    % desviacion estandar
    sig = para.pulso.c*2*pi;
    spectre=spectre.*exp(-0.5*((w-me)/sig).^2);
    spectre=spectre.*exp(-1i*w*(para.pulso.b));
    
elseif para.pulso.tipo==3 %Ricker periodo caracter?stico tp
dt=(1/(df*2*nfN)*(nfN/(nfN-1)));
tp=para.pulso.a;
ts=para.pulso.b;
a = pi*(-ts)/tp;
ntiempo = (2*nfN-2);
cr = zeros(1,ntiempo);
cr(1) = (a*a-0.5)*exp(-a*a);
for i=2:ntiempo/2+1
    a = pi*(dt*(i-1)-ts)/tp;
    a = a*a;
    cr(i) = (a-0.5)*exp(-a);
end
%cr = -cr;
spectre=fft(cr*dt); %forward
spectre=spectre(1:nfN); % se?al en frecuencia
spectre=spectre.*exp(1i*w*(para.pulso.c));
elseif floor(para.pulso.tipo)==4 % plano + tapper gaussian
  % Definicion formal
  tp=para.pulso.a;
  ts=para.pulso.b;
  fmax = (nfN-1) * df;
  f = linspace(0,fmax,nfN);
  omega_p = 2*pi / tp;
  omega = 2*pi*f;
  b = omega / omega_p;
%   factorDeEscala = (tp/pi^.5); % Factor de escala te?rico para regresar a
%   al tiempo y recuperar amplitud m?xima 1.0
  spectre =  exp(-b.^2) ;%.* exp(-1i * omega * ts);
  
  a = para.pulso.tipo - floor(para.pulso.tipo);
  if a ~= 0
  spectre = spectre.^a;
  end
  % aplica un corrimiento manteniendo plano el espectro hasta .c
  if para.pulso.c > 0
    init = find(w/2/pi > para.pulso.c); 
    if ~isempty(init)
    init = max(1,init(1));
    spectre = circshift(spectre,init,2);
    spectre(1:init) = 1;
    end
  end
  spectre=spectre.*exp(-1i*w*(ts));
elseif para.pulso.tipo==5 % gaussiano rise time
  
  % Definicion formal
  tp=para.pulso.a * pi/4;
  ts=para.pulso.b;
  fmax = (nfN-1) * df;
  f = linspace(0,fmax,nfN);
  omega_p = 2*pi / tp;
  omega = 2*pi*f;
  b = omega / omega_p;
  spectre =  exp(-b.^2) .* exp(-1i * omega * ts); 
elseif para.pulso.tipo==6 % Butterworth
  n =para.pulso.a;
  Wn=para.pulso.b;
  [b,a]=butter(n,Wn);
  nfN      = para.nf;
  nfN     = nfN/2+1;
  N = nfN;
  w = linspace(0, pi, N+1); w(end) = [];
  ze = exp(-1j*w); % Pre-compute exponent
  spectre = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function
  spectre = reshape(spectre,1,nfN);
  df      = para.fmax/nfN;     disp(['df = ',num2str(df)])
  f      = (0:nfN/2)*df;
  omega = 2*pi*f;
  spectre = spectre .*  exp(-1i * omega * para.pulso.c);
   figure(42415)
   freqz(b,a)
elseif para.pulso.tipo==7 % dirac
    spectre=ones(1,nfN);
    spectre=spectre.*exp(-1i*w*(para.pulso.b));
end
