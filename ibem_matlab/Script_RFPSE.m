% Script RFPSE

% Corroborar reciprocidad de desplazamientos y esfuerzos entre dos
% puntos en un semiespacio ante fuerzas puntuales

% Los puntos est�n a la misma profundidad. De otro modo no existe
% reciprocidad en los esfuerzos.

function Script_RFPSE
%% Generar resultados
% caso a
X  = [0.7856, -0.5708, 0.3433];
Xi = [-0.0156, -0.9236, 0.3433];
batchGenerarEjercicio_X_Xi(X,Xi)
[RESULT_a,para_a] = BatchScript('SCRIPTEjercicio01.mat');

% caso b
Xi = [0.7856, -0.5708, 0.3433];
X  = [-0.0156, -0.9236, 0.3433];
batchGenerarEjercicio_X_Xi(X,Xi)
[RESULT_b,para_b] = BatchScript('SCRIPTEjercicio01.mat');
% 
save('aux.mat','RESULT_a','RESULT_b','para_a','para_b');
% load('aux.mat')
%%
uwa = RESULT_a.uw;
swa = RESULT_a.sw;

uwb = RESULT_b.uw;
swb = RESULT_b.sw;

uwc = zeros(size(RESULT_a.uw));
swc = zeros(size(RESULT_a.sw));

% Reciprocidad de esfuerzos:
i = 1;
uwc(:,1,i,1) = uwb(:,1,1,i); 
uwc(:,1,i,2) = uwb(:,1,2,i);
uwc(:,1,i,3) = uwb(:,1,3,i);
swc(:,1,i,1) = -swb(:,1,i,1); %sxx
swc(:,1,i,2) = -swb(:,1,i,2); %syy    
swc(:,1,i,3) = -swb(:,1,i,3); %szz
swc(:,1,i,4) = -swb(:,1,i,4); %sxy 
swc(:,1,i,5) = swb(:,1,i,5); %sxz
swc(:,1,i,6) = swb(:,1,i,6); %syz
i = 2;
uwc(:,1,i,1) = uwb(:,1,1,i); 
uwc(:,1,i,2) = uwb(:,1,2,i);
uwc(:,1,i,3) = uwb(:,1,3,i);
swc(:,1,i,1) = -swb(:,1,i,1); %sxx
swc(:,1,i,2) = -swb(:,1,i,2); %syy    
swc(:,1,i,3) = -swb(:,1,i,3); %szz
swc(:,1,i,4) = -swb(:,1,i,4); %sxy 
swc(:,1,i,5) = swb(:,1,i,5); %sxz
swc(:,1,i,6) = swb(:,1,i,6); %syz
i = 3;
uwc(:,1,i,1) = uwb(:,1,1,i); 
uwc(:,1,i,2) = uwb(:,1,2,i);
uwc(:,1,i,3) = uwb(:,1,3,i);
swc(:,1,i,1) = swb(:,1,i,1); %sxx
swc(:,1,i,2) = swb(:,1,i,2); %syy    
swc(:,1,i,3) = swb(:,1,i,3); %szz
swc(:,1,i,4) = swb(:,1,i,4); %sxy 
swc(:,1,i,5) = -swb(:,1,i,5); %sxz
swc(:,1,i,6) = -swb(:,1,i,6); %syz
%% Hacer gr�ficos
% Fuerza en x en caso a
for i=1:3

figure(i); clf; hold on

nfN = size(RESULT_a.uw,1);
r = 1;

tx = ['x(',num2str(para_a.rec.xr),...
  ',',num2str(para_a.rec.yr),...
  ',',num2str(para_a.rec.zr),')'...
  ' xi(',num2str(para_a.xs(1)),...
  ',',num2str(para_a.ys(1)),...
  ',',num2str(para_a.zs(1)),')  Fza. dir ', num2str(i)];
title(tx)

cR = 'r-';
cI = 'b-';
df = para_a.fmax/(para_a.nf/2); %paso en frecuencia
Fq = (0:para_a.nf/2)*df;
cs = 1; % funcion de amplitud
ha = zeros(1,12);

subplot(4,3,1);  tT('u',uwa(1:nfN,r,i,1),tx,cR,cI,Fq,cs); ha(1)=gca;
subplot(4,3,2);  tT('v',uwa(1:nfN,r,i,2),tx,cR,cI,Fq,cs); ha(2)=gca;
subplot(4,3,3);  tT('w',uwa(1:nfN,r,i,3),tx,cR,cI,Fq,cs); ha(3)=gca;
if size(swa,4) ~= 0
subplot(4,3,4);  tT('sxx',swa(1:nfN,r,i,1),tx,cR,cI,Fq,cs); ha(4)=gca;
subplot(4,3,5);  tT('sxy',swa(1:nfN,r,i,4),tx,cR,cI,Fq,cs); ha(5)=gca;
subplot(4,3,6);  tT('sxz',swa(1:nfN,r,i,5),tx,cR,cI,Fq,cs); ha(6)=gca;
subplot(4,3,8);  tT('syy',swa(1:nfN,r,i,2),tx,cR,cI,Fq,cs); ha(8)=gca;
subplot(4,3,9);  tT('syz',swa(1:nfN,r,i,6),tx,cR,cI,Fq,cs); ha(9)=gca;
subplot(4,3,12); tT('szz',swa(1:nfN,r,i,3),tx,cR,cI,Fq,cs); ha(12)=gca;
end
linkaxes(ha,'x');

% caso b
tx = ['x(',num2str(para_b.rec.xr),...
  ',',num2str(para_b.rec.yr),...
  ',',num2str(para_b.rec.zr),')'...
  ' xi(',num2str(para_b.xs(1)),...
  ',',num2str(para_b.ys(1)),...
  ',',num2str(para_b.zs(1)),')  Fza. dir ', num2str(i)];
cR = 'r--';
cI = 'b--';
subplot(4,3,1);  tT('u',uwc(1:nfN,r,i,1),tx,cR,cI,Fq,cs); 
subplot(4,3,2);  tT('v',uwc(1:nfN,r,i,2),tx,cR,cI,Fq,cs); 
subplot(4,3,3);  tT('w',uwc(1:nfN,r,i,3),tx,cR,cI,Fq,cs); 
if size(swc,4) ~= 0
subplot(4,3,4);  tT('sxx',swc(1:nfN,r,i,1),tx,cR,cI,Fq,cs); 
subplot(4,3,5);  tT('sxy',swc(1:nfN,r,i,4),tx,cR,cI,Fq,cs); 
subplot(4,3,6);  tT('sxz',swc(1:nfN,r,i,5),tx,cR,cI,Fq,cs); 
subplot(4,3,8);  tT('syy',swc(1:nfN,r,i,2),tx,cR,cI,Fq,cs); 
subplot(4,3,9);  tT('syz',swc(1:nfN,r,i,6),tx,cR,cI,Fq,cs); 
subplot(4,3,12); tT('szz',swc(1:nfN,r,i,3),tx,cR,cI,Fq,cs); 
end
end
end

function tT(tit,var,tx,cR,cI,Fq,cs)
if max(max(var)) == 0; return;end

espe = var.*cs.';
hold on
hr=plot(Fq,real(espe),cR,'LineWidth',0.5);
hi=plot(Fq,imag(espe),cI,'LineWidth',1.5);
set(hr,{'DisplayName'},{['fRe ',tx]});
set(hi,{'DisplayName'},{['fIm ',tx]});
xlabel('frequency')
ylabel('amplitude')
title(tit)
end
