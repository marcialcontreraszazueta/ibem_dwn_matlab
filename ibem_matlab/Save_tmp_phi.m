function Save_tmp_phi(phi,para,pT,j) % %#ok<INUSD>

% name = ['../out/' para.nametmp(1:end-4)];
name = '../out/PHI';
if j < 10
save([name,'_00',num2str(j),'tmp.mat'],'phi','para','pT');  
else
  if j < 100
 save([name,'_0',num2str(j),'tmp.mat'],'phi','para','pT');
  else
  save([name,'_',num2str(j),'tmp.mat'],'phi','para','pT');    
  end
end
