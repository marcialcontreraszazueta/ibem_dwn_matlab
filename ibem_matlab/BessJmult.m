function [J0kr,J1kr] = BessJmult(kr,Jkr,nkr,R)
% Teorema de multiplicaciOn 9.1.74 en Abramowitz and Stegun
% Se ha precalculado J_n(kr) con n=0,1,...30 y ahora con el teorema 
% se obtiene J_0(kr*r) y J_1(kr*r)

% Esto funcionea pero se tarda mAs que hacer J_0 y J_1 cada vez.

J0kr = complex(zeros(1,nkr));
J1kr = complex(zeros(1,nkr));
c = complex(zeros(1,nkr)); %#ok
for k=0:30 % tambien con 20, con menos truena
  c = (-1)^k * (R^2-1)^k * (0.5 * kr).^k / factorial(k);
  J0kr = J0kr + c .* Jkr(0+k +1,:);
  J1kr = J1kr + c .* Jkr(1+k +1,:);
end
% J0kr = J0kr * R^0;
  J1kr = J1kr * R^1;
end