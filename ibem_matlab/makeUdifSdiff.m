function [DWNout] = makeUdifSdiff(para,DWNorig,pT)
% makeUdifSdiff calcula las funciones de Green entre puntos de colocaciOn y
% receptores
% fracVert=10^6;
% fracDist = 1/60;
% fracAz = 0.1;
fracVert = 30;
fracDist = 1/20;
fracAz = 0.1;
%    ######�Es casi igualito a calcBlockDWN.m  #######
nbeq = para.coord.nbeq;
CubWts = para.cubature(:,3);
ngau = size(CubWts,1);
paraux.gaussian.ngau = ngau;
paraux.cubature = para.cubature(1:ngau,3);
DWNout = cell(size(DWNorig));

sal = para.sortie;
doU = (sal.Ux + sal.Uy + sal.Uz) >= 1;
doS = (sal.sxx + sal.syy + sal.szz + sal.sxy + sal.sxz + sal.syz) >= 1;

for im = 1:para.nmed
  if para.tipoMed(im) == 2 % si este medio es estratificado
    DWN = DWNorig{im};
    if doU; DWN.Udiff = zeros(DWNorig{im}.Udiffsize); else DWN.Udiff = 0; end
    if doS; DWN.Sdiff = zeros(DWNorig{im}.Sdiffsize); else DWN.Sdiff = 0; end
    % fuentes y receptores en el medio im
    cols = pT.m(1,:) == im;       if isempty(cols); continue;end
    rens = para.rec.m(im).ind;    if isempty(rens); continue;end
    
    pTXi = pT.Xi(:,cols);
    pTXi = findlayer(pTXi,DWN.ncapas,DWN.z);
    % Reconocer las fuentes a profundiades iguales:
    [Xiz_uni,~,zrref] = psedoUnique(DWN.sub,...
      DWN.z,DWN.h,pTXi(3,:),pTXi(4,:),DWN.ncapas,fracVert);
    pTdA = pT.dA(:,cols);
    pTcubPts = pT.CubPts(:,:,cols);
    
    pcX = zeros(4,length(rens));
    pcX(3,:) = para.rec.zr(rens);
    pcX(2,:) = para.rec.yr(rens);
    pcX(1,:) = para.rec.xr(rens);
    pcX  = findlayer(pcX ,DWN.ncapas,DWN.z);
    % Reconocer los receptores en [rens] a la misma profundidad
    [X_z_uni,uni_ref,zXref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
      pcX(3,:),pcX(4,:),DWN.ncapas,fracVert);
    
    MdistAzi = zeros(4,length(rens),sum(cols)) .* NaN;
%     Mazi     = zeros(  length(rens),sum(cols)) .* NaN;
    Gij = complex(zeros(3,3,1,1));
    GijP= complex(zeros(3,3,1));
    for iXiz_uni = 1:length(Xiz_uni) % para cada profundidad de las fuentes
      % Indices globales de las fuentes a esta profundidad:
      j = find(zrref==iXiz_uni); %e.g.: pTXi(3,j) son todos iguales
      
      % Armar vector de fuente (en medio estratificado):
      [FsourcePSV,FsourceSH,ics,~] = VecfuentDWN(Xiz_uni(iXiz_uni),DWN);
      
      % Amplitud de las ondas planas que construyen el campo difractado por la
      % estratificaciOn:
      [SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
        DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);
      
      % Para cada profundidad �nica de los receptores:
      for iX_z_uni = 1:length(X_z_uni) % Para cada profundiad de receptores
        % indices globales de los receptores a esta profundidad:
        res_at_iX_z = find(zXref==iX_z_uni); %e.g.: pcX(3,res_at_iX_z) son todos iguales
        icr = pcX(4,uni_ref(iX_z_uni)); % estrato del receptor
        
        ksi 	= DWN.sub(icr).ksi;
        kpi 	= DWN.sub(icr).kpi;
        Ci    = DWN.sub(icr).Ci;
        longonda = 2*pi/real(ksi);
        
        % profundidad relativa a la interface de la capa del receptor:
        zricr = X_z_uni(iX_z_uni) -  DWN.z(icr);
        
        % matriz distancia horizontal, cosenos directores y acimut entre
        %    res_at_iX_z(:)j(:) cantidad de receptores a una profundidad
        %    nj                 cantidad de fuentes a una profundidad
        % la matriz tiene el tama�o original y los indices son globales
        for Iiixs = 1:length(j.') %Matriz r,c,s,th. 
          iixs = j(Iiixs);  % Indice global de la fuente
          for Iir = 1:length(res_at_iX_z.')
            ir = res_at_iX_z(Iir); % Indice global del receptor
            Xfs = pcX(1,ir)-pTXi(1,iixs);
            Yfs = pcX(2,ir)-pTXi(2,iixs);
            r = sqrt(Xfs^2 + Yfs^2);
            MdistAzi(4,ir,iixs) = r;
            if r<1e-6
              c = 1/sqrt(2);
              s = 1/sqrt(2);
            else
              c = Xfs/r;
              s = Yfs/r;
            end
            MdistAzi(1,ir,iixs) = c;
            MdistAzi(2,ir,iixs) = s;
            th = real(-1i .* log(c + 1i.*s));
            if th < 0
              th = 2*pi + th;
            end
            MdistAzi(3,ir,iixs) = th;
          end
        end
        
        % DWN.kz(1,kr,estrato) = nu    para todos los kr en ese estrato
        % DWN.kz(2,kr,estrato) = gamma   ''
        if doU % DWN Fuente y receptor cerca:
          [Mi] = squeeze(MdistAzi(4,:,:)) <= longonda/1; % cualquier Z, meh
          
          % Integraci�n gaussiana para fuente en segmento triangular en un
          % medio estratificado (desplazamientos)
          [irxs,ixss] = find(Mi); %los pares ir,iixs de wthThisAz
          for iii = 1:length(irxs)
            ir   = irxs(iii); % Indice global del receptor
            iixs = ixss(iii); % Indice global de la fuente
          
              % 1)Fuerza en el receptor y desplazamientos en puntos de
              % integraci�n. 2) Reciprocidad y suma con pesos y Area.
              
              % aguas: nada mas es campo difractado por estratos
              Gij = Gij_3DDWN_smallGEN(pcX(1:4,ir),...
                pTcubPts(1:3,1:ngau,iixs),CubWts,pTXi(4,iixs),...
                DWN);  %func de Green [(u v w) x (fx fy fz)]
              
              cophi = para.coord.phi(iixs,im); % indice del phi correspondiente a la fuente
              % Multiplicar por Area del segmento. Ya es [(u v w) x (fx fy fz)]
              DWN.Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) = Gij.* ...
                pTdA(iixs);
%             end
          end %iii
          
          % Los que restan (lejos) como fuente puntual en medio estratificado
          MdistAzi(:,Mi) = nan;
        end
        
        % agrupar distancias horizontales:
        [MdisUni,~,disN] = psedoUniqueM(squeeze(MdistAzi(4,:,:)),longonda*fracDist,false);
        if ~isempty(disN) % DWN Fuente y receptor lejos: 
          % tErminos de propagaciOn vertical a las ondas planas que
          % constituyen el campo difractado por la estratificaciOn
          % en el estrato del receptor [fac].
          % DWN.kz(1,kr,estrato) = nu    para todos los kr en ese estrato
          % DWN.kz(2,kr,estrato) = gamma   ''
          if doU % desplazamientos
            facU = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
              SolutionPSV,SolutionSH,...
              DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,1,DWN.nkr);
          end
          if doS % esfuerzos
            facS = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
              SolutionPSV,SolutionSH,...
              DWN.kz(1,:,icr),DWN.kz(2,:,icr),DWN.kr,-1,DWN.nkr);
          end
          
          % Para cada grupo de distancia horizontal
          for IiDist = 1:length(disN.')%iDist = disN.'
            iDist = disN(IiDist);
            [Mi] = MdisUni == iDist;
            r = mean(mean(MdistAzi(4,Mi))); % distancia (en el plano) promedio
            %           if DWN.usarBESJCMEX
            %             nkss = length(DWN.kr);
            %             J0 = zeros(1,nkss);
            %             J1 = zeros(1,nkss);
            %             for iiik = 1:nkss
            %               J01 = BESJCMEX(DWN.kr(iiik)*r+0.000000000000000000000001i);
            %               J0(iiik) = J01(1);
            %               J1(iiik) = J01(2);
            %               [J0(iiik),J1(iiik)] = BessJ01(DWN.kr(iiik)*r);
            %             end
            %           else
            J0      = besselj(0,DWN.kr*r);
            J1      = besselj(1,DWN.kr*r);
            %           end
            dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
            d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
            dJ0     =-DWN.kr.*J1;
            d2J0    =-DWN.kr.*dJ1;
            
            % Agregar terminos que solo dependen del radio horizontal
            % tracciones
            if doU % desplazamientos
              MecRadialU = campodiffporestratRadialTransv(1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facU,DWN.DK);
            end
            if doS
              MecRadialS = campodiffporestratRadialTransv(-1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facS,DWN.DK);
            end
            
            % agrupar acimuts casi iguales de este grupo de dist horizontal
            Mazi = zeros(size(Mi)).*nan;
            Mazi(Mi) = MdistAzi(3,Mi); % th
            [MasiUni,~,aziN] = psedoUniqueM(Mazi,longonda*fracDist/r*fracAz,true);% <-- empIrico
%             Mazi(Mi) = nan; % a cambio de Linea 192
            % Para cada intervalo de acimuts:
            for IiAzimuth = 1:length(aziN.') % Intervalos de acimuts:
              iAzimuth = aziN(IiAzimuth);
              [wthThisAz] = MasiUni == iAzimuth;
              if ~isempty(find(wthThisAz,1))
                % cosenos directores:
                ct = mean(mean(MdistAzi(1,wthThisAz)));
                st = mean(mean(MdistAzi(2,wthThisAz)));
                
                % cAlculo del campo difractado por la estratificaciOn
                if doU
                  % (3x3). Primer Indice fx,fy,fz. Segundo u,v,w
                  MecElemU = campodiffporestratAngular(1,MecRadialU,ct,st,Ci(6,6));
                  %                 MecElemU = campodiffporestrat2(1,...
                  %                   r,J0,J1,dJ1,d2J1,dJ0,d2J0,ct,st,...
                  %                   ksi,kpi,Ci(6,6),facU,DWN.DK);
                end
                if doS
                  % (3x3x3). Primer Indice fx,fy,fz. 2o y 3o el tensor en
                  % cartesianas. | sxx syx szx |
                  %              | sxy syy szy |
                  %              | sxz syz szz |
                  MecElemS = campodiffporestratAngular(-1,MecRadialS,ct,st,Ci(6,6));
                  %                 MecElemS = campodiffporestrat2(-1,...
                  %                   r,J0,J1,dJ1,d2J1,dJ0,d2J0,ct,st,...
                  %                   ksi,kpi,Ci(6,6),facS,DWN.DK);
                end
                % Acomodar el campo difractado en la matriz
                [irxs,ixss] = find(wthThisAz); %los pares ir,iixs de wthThisAz
                for iii = 1:length(irxs)
                  ir   = irxs(iii); % Indice global del receptor
                  iixs = ixss(iii); % Indice global de la fuente
                  %                 iixs = j(ixss(iii));         % Indice global de la fuente
                  cophi = para.coord.phi(iixs,im); % indice del phi correspondiente a la fuente
                  %                 ir = res_at_iX_z(irxs(iii)); % Indice global del receptor
                  
                  % Acomodar el campo difractado en las matrices Udiff Sdiff
                  if doU % desplazamientos
                    %func de Green [(fx fy fz) x (u v w)]
                    Gij = Gij_fromMecElem(MecElemU,false);
                    % permute to get [(u v w) x (fx fy fz)]
                    for i=1:3
                      for k=1:3
                        GijP(i,k,1) = Gij(k,i,1,1);
                      end
                    end
                    DWN.Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) = GijP.* ...
                      pTdA(iixs);
                  end
                  if doS % esfuerzos
                    Sij = Tij_fromMecElem(MecElemS);
                    DWN.Sdiff(:,[cophi cophi+nbeq cophi+2*nbeq],ir) = Sij .* ...
                      pTdA(iixs);
                  end
                end % iii
              end % ~isempty(find(wthThisAz,1))
            end %iAzimuth
            
            % Recetear la matriz de radio, cosenos y azimut
            MdistAzi(1:4,Mi) = nan;
          end %iDist
        end %~isempty(disN)
        
        % Incidencia directa si fuente y receptor estAn en el mismo estrato
        if ics == icr
          for Iiixs = 1:length(j.')
            iixs = j(Iiixs); % Indice global de la fuente
            cophi = para.coord.phi(iixs,im); % indice del phi correspondiente a la fuente
            for Iir = 1:length(res_at_iX_z.')
              ir = res_at_iX_z(Iir); % Indice global del receptor
              pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
              rij = sum((pcXaux) .* (pcXaux))^0.5;
              dr = sqrt(pTdA(iixs)/pi); %radio del cIrculo de Area dA(ic)
              g = pcXaux./rij;
              
              jjCirculo  = rij<0.5*dr;
              jjMidRange = rij>=0.5*dr & rij<=longonda/3;  %/6
              jjLejos = rij>longonda/3;
              
              if jjCirculo % campo muy cercano
                if doU % desplazamientos
                  BEALF = real(kpi)/real(ksi);
                  %indice lOgico de la posicion de las fuentes auxiliares en m
                  ii = para.coord.Xim(:,im) ~= 0;
                  iXi = iixs;
                  Vn(1,:)=para.coord.vnx(ii);
                  Vn(2,:)=para.coord.vny(ii);
                  Vn(3,:)=para.coord.vnz(ii);
                  if rij > 0.1*dr
                    EPS = rij/dr;
                    % [(u v w) x (fx fy fz)]
                    GijP=Greenex_3D(ksi,dr,BEALF,Vn(1:3,iXi),Ci(6,6),g(1:3),EPS);
                  else
                    GijP=Greenex_3D(ksi,dr,BEALF,Vn(1:3,iXi),Ci(6,6),0,0);
                  end
                end
                if doS %tracciones
                  error('falta')
                  %                   S_fx = 0.5*eye(3);
                  %                   S_fy = 0.5*eye(3);
                  %                   S_fz = 0.5*eye(3);
                end
              end
              
              if jjMidRange % campo intermedio
                if doU % desplazamientos
                  %func de Green [(fx fy fz) x (u v w)]
                  Gij = Gij_3D_r_smallGENf(pcX(1:3,ir),...
                    pTcubPts(1:3,1:ngau,iixs),CubWts,...
                    ksi,kpi,Ci); 
                  % permute: [(u v w) x (fx fy fz)]
                  for i=1:3
                    for k=1:3
                      GijP(i,k,1) = Gij(k,i,1,1);
                    end
                  end
                end
                
                if doS % esfuerzos
                  if rij >= 0.5*dr % mAs rudo que: ir ~= iixs
                    coordf0.CubPts = para.coord.CubPts(:,:,iixs);
                    [S_fx,S_fy,S_fz]=S_3D_r_smallGEN(coordf0,...
                      pcX(1:3,ir),1,ksi,kpi,paraux);
                  end
                end
              end
              
              if jjLejos
                % rij > 2dr campo lejano mismo estrato
                if doU % desplazamientos
                  GijP = Gij_3D(ksi,kpi,rij,g,Ci,1);  % [(u v w) x (fx fy fz)]
                end
                if doS % esfuerzos
                  [S_fx,S_fy,S_fz]=S_3D(rij,g,ksi,kpi,[1 1 1]);
                end
              end
              
              % Agregar a campo difractado
              if doU
                DWN.Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) = ...
                  DWN.Udiff(1:3,[cophi cophi+nbeq cophi+2*nbeq],ir) + ...
                  GijP .* pTdA(iixs); %[(u v w) x (fx fy fz)]
              end
              if doS
                DWN.Sdiff(:,cophi,ir) = DWN.Sdiff(:,cophi,ir) + ...
                  reshape(S_fx,9,1) .* pTdA(iixs);
                DWN.Sdiff(:,cophi+nbeq,ir) = DWN.Sdiff(:,cophi+nbeq,ir) + ...
                  reshape(S_fy,9,1) .* pTdA(iixs);
                DWN.Sdiff(:,cophi+2*nbeq,ir) = DWN.Sdiff(:,cophi+2*nbeq,ir) + ...
                  reshape(S_fz,9,1) .* pTdA(iixs);
              end
            end
          end
        end %ics == icr
        
      end % cada profundidad Unica de los receptores
    end % cada profundidad Unica de las fuentes
    clear pcX
    DWNout{im} = DWN;
  end
end

% %% test me
%
% im = 1;
% thisXi = 99;
% this_X = 1:30;
%
% tam = 1/abs(max(max(max(DWN.Udiff))));
% cols = pT.m(1,:) == im;
% rens = para.rec.m(im).ind;
% pTXi = pT.Xi(:,cols);
% % pTdA = pT.dA(:,cols);
% pcX(3,:) = para.rec.zr(rens);
% pcX(2,:) = para.rec.yr(rens);
% pcX(1,:) = para.rec.xr(rens);
% for iixs = thisXi % Indice global de la fuente
%   for ir = this_X % Indice global del receptor
%     figure(1); hold on
%     plot3(pcX(1,ir),pcX(2,ir),pcX(3,ir),'r^');
%     plot3(pTXi(1,iixs),pTXi(2,iixs),pTXi(3,iixs),'r*');
%
%     cophi = para.coord.phi(iixs,im); % indice del phi correspondiente a la fuente
%
%     % fx
%     phh = cophi;
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       real(DWN.Udiff(1,phh,ir)),real(DWN.Udiff(2,phh,ir)),real(DWN.Udiff(3,phh,ir)),tam,'r-');
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       imag(DWN.Udiff(1,phh,ir)),imag(DWN.Udiff(2,phh,ir)),imag(DWN.Udiff(3,phh,ir)),tam,'r--');
%
%     % fy
%     phh = cophi+nbeq;
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       real(DWN.Udiff(1,phh,ir)),real(DWN.Udiff(2,phh,ir)),real(DWN.Udiff(3,phh,ir)),tam,'g-');
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       imag(DWN.Udiff(1,phh,ir)),imag(DWN.Udiff(2,phh,ir)),imag(DWN.Udiff(3,phh,ir)),tam,'g--');
%
%     % fz
%     phh = cophi+2*nbeq;
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       real(DWN.Udiff(1,phh,ir)),real(DWN.Udiff(2,phh,ir)),real(DWN.Udiff(3,phh,ir)),tam,'b-');
%     quiver3(pcX(1,ir),pcX(2,ir),pcX(3,ir),...
%       imag(DWN.Udiff(1,phh,ir)),imag(DWN.Udiff(2,phh,ir)),imag(DWN.Udiff(3,phh,ir)),tam,'b--');
%
%   end
% end

end