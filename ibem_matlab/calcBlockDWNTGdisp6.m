function [AbT,AbG] = calcBlockDWNTGdisp6(...
  nrens,pcX,pcVn,pcXim,m,im0,...
  ncols,pTXi,pTdA,pTcubPts,...
  isSelfReciproc,relatedG,...
  CubWts,DWN)
% calcBlock llena un bloque de la matriz de condiciones de frontera.
%    Si Xi pertenece a un solo medio (tracciones libres),
%    se establecen esfuerzos nulos. Si Xi pertence a dos medios,
%    se establece la continuidad de tracciones y desplazamientos
%    a ambos lados de Xi.
%  Esta versiOn:IntegraciOn gaussiana para el campo difractado por estrat.

%    Preferimos calcular por columnas. Es mAs eficiente cuando es con DWN
fracZ = 1/60;     % Tij_3D reciproco en semiespacio sOlo si x(3) aprox xi(3)
fracVert = 60;   %  1/#  agrupar posiciones verticales en campo diff.
fracDist = 1/60; % Agrupar distancias horizontales
fracCercano = 1/60;% Integracion Gaussiana de campo difractado
facLejecitos = 0.1;
ngau = length(CubWts);
% Rmax = (4)*DWN.sub(1).ksi; % para truncar la soluciOn

% % A lo mejor hay que usar las normales hacia afuera siempre
% % Para cada punto de colocacion 
% for ir = 1:nrens
%   if (pcXim(m,ir)==2) % normales invertidas
%     pcVn(:,ir) = -pcVn(:,ir);
%     pcXim(m,ir) = 1;
%   end
% end
% Reconocer las fuentes a profundiades iguales:
[Xiz_uni,uni_ref,zrref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
  pTXi(3,:),pTXi(4,:),DWN.ncapas,fracVert);

% Reconocer los receptores en [rens] a la misma profundidad
if isSelfReciproc
  X_z_uni = Xiz_uni;
% uni_ref ya fue calculado
  zXref = zrref;
else
  [X_z_uni,uni_ref,zXref] = psedoUnique(DWN.sub,DWN.z,DWN.h,...
    pcX(3,:),pcX(4,:),DWN.ncapas,fracVert);
end

% Para cada profundidad de la fuente se ensambla el vector de fuente sin
% fase radial y en coordenadas polares. Luego se obtiene la amplitud de las
% ondas que construyen el campo difractado por el medio estratificado y el
% campo difractado mismo para cada par de profundidades del receptor/fuente
% y cada distancia horizontal promedio y cada angulo azimutal promedio.

leXiz_uni = length(Xiz_uni);
leX_z_uni = length(X_z_uni);

% Conjunto de Matrices donde se guarda la solucion obtenida en paralelo:
aux = struct('matrix',complex(zeros(3,3,nrens,ncols)));
At = repmat(struct('T',aux),1,leXiz_uni);

if relatedG~=0
  nrensG = nrens;
  ncolsG = ncols;
  Ag = repmat(struct('G',aux),1,leXiz_uni);
else
  nrensG = 1;
  ncolsG = 1;
  Ag = repmat(struct('G',aux),1,1);
end

clear aux
FsourcePSV  = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
FsourceSH   = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
SolutionPSV = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
SolutionSH  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
SolutionPSVGau = complex(zeros(4*(DWN.ncapas-1)+2,DWN.nkr,2)); %#ok
SolutionSHGau  = complex(zeros(2*(DWN.ncapas-1)+1,DWN.nkr)); %#ok
% AtmpT = complex(zeros(3,3,nrens,ncols));
% AtmpG = complex(zeros(3,3,nrens,ncols));
%facT = complex(zeros(10,DWN.nkr,leX_z_uni));
%facG = complex(zeros( 5,DWN.nkr,leX_z_uni));
% fac_icr = zeros(leX_z_uni,1);
%MdistAzi = zeros(4,nrens,ncols) .* NaN;
MasiUni = zeros(nrens,ncols); aziN = zeros(ceil(nrens*ncols/2),1); %#ok
irxs=zeros(nrens*ncols,1);ixss=zeros(nrens*ncols,1);%#ok
rij=0.0;dr=0.0;%#ok
% Mplot = zeros(nrens,ncols);
MdisUni = zeros(nrens,ncols); disN = zeros(ceil(nrens*ncols/2),1); %#ok
%MecElemU = complex(zeros(3,3));
%MecElemS = complex(zeros(3,3,3));%#ok
ksi=complex(zeros(1,1)); kpi=complex(zeros(1,1)); Ci=complex(zeros(1,1)); %#ok
% Mi=zeros(nrens,ncols).*NaN;iDist=0.0; %#ok
% nkss = DWN.nkr;
% J0 = complex(zeros(1,nkss)); J1 = complex(zeros(1,nkss));%#ok
% dJ1= complex(zeros(1,nkss));d2J1= complex(zeros(1,nkss));%#ok
% dJ0= complex(zeros(1,nkss));d2J0= complex(zeros(1,nkss));%#ok
% warning('noparfor')
disp('starting parfor cycle on Xiz_uni...')
% parfor
for iXiz_uni = 1:leXiz_uni % para cada profundidad de fuentes agrupada
  disp([num2str(iXiz_uni) ' / ' num2str(leXiz_uni) '...'])
  
  % Indices de las fuentes a esta profundidad:
  j = find(zrref==iXiz_uni); %e.g.: pTXi(3,j) son todos casi iguales
  Xiz = Xiz_uni(iXiz_uni); %profundidad de las Xi
  
%---- Para el caso de campo difractado lejano ----------------------------
  % Armar vector de fuente en el medio estratificado:
%   ics = 1; %#ok para que no chiste el compilador
  [FsourcePSV,FsourceSH,ics,~] = VecfuentDWN(Xiz,DWN); %~htop
  % Amplitud de las ondas planas que construyen el campo difractado por la
  % estratificaciOn:                     dada Fx,Fz  ----.
  [SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
    DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr);
  
  % Allocations:
  AtmpT = complex(zeros(3,3,nrens,ncols));%  AtmpT(:,:,:,:)=0;
  AtmpG = complex(zeros(3,3,nrensG,ncolsG));%  AtmpG(:,:,:,:)=0;
  icr = zeros(1,1); %#ok
  facT = complex(zeros(10,DWN.nkr));
  facG = complex(zeros( 5,DWN.nkr));
  pcXaux=zeros(3,1);
  for iX_z_uni = 1:leX_z_uni % Para cada profundiad de receptores agrupada
    icr = pcX(4,uni_ref(iX_z_uni)); %#ok estrato del receptor
    
    % indice de los receptores a esta profundidad  e.g.: pcX(3,res_at_iX_z)
    res_at_iX_z = find(zXref==iX_z_uni); %               son todos iguales
    % material:
    [ksi,kpi,Ci] = theSub(DWN.sub,icr(1,1));
    longonda = (2*pi)/real(ksi);
    
    MdistXY = zeros(nrens,ncols) .* NaN;
    Mi=zeros(nrens,ncols).*NaN;iDist=0.0; %#ok
    MecRadialU = [];  MecRadialS = []; 
    ir=0;iixs=0;Zfs=0.0;Xfs=0.0;Yfs=0.0;r=0.0;c=0.0;s=0.0;th=0.0; %#ok
    nkss = DWN.nkr;
    J0 = complex(zeros(1,nkss)); J1 = complex(zeros(1,nkss));%#ok
    dJ1= complex(zeros(1,nkss));d2J1= complex(zeros(1,nkss));%#ok
    dJ0= complex(zeros(1,nkss));d2J0= complex(zeros(1,nkss));%#ok
    
    for Iiixs = 1:length(j.') %Matriz con las distancias SIN RECIPROCIDAD
    % la matriz tiene el tamaNo original y los indices son globales
      iixs = j(Iiixs); % Indice global de la fuente
      for Iir = 1:length(res_at_iX_z.')
        ir = res_at_iX_z(Iir); % Indice global del receptor
        
        Xfs = pcX(1,ir)-pTXi(1,iixs);%#ok
        Yfs = pcX(2,ir)-pTXi(2,iixs);
        MdistXY(ir,iixs) = sqrt(Xfs^2 + Yfs^2);

        % AQUI se puede incertar una regla para truncar la soluciOn.
        %         R = sqrt(Xfs^2 + Yfs^2 + Zfs^2);
        %         if R > 0.9 * lambda
        %           continue
        %         end
      end
    end
    
%---- Las singularidades en el campo de tracciones ------------------------
    if true
      [Mi] = MdistXY(:,:) <= 1e-6;  
      [irxs,ixss] = find(Mi); %los elementos en Mi
      for iii = 1:length(irxs)
        ir   = irxs(iii); % Indice global del receptor
        iixs = ixss(iii); % Indice global de la fuente
        dz = pcX(3,ir)-pTXi(3,iixs);
        if dz <= 1e-6
          % Es una singuaridad impar. El valor principal de la integral en 
          % el segmento es cero. Las reacciones a la fuerza unitaria
          % son 0.5 a cada lado del elemento.
          
%            if ics == icr(1,1) %&& rij < 0.5*dr % Mismo estrato
           % en este punto, hay discontinuidad de las tractiones ti-ti'=phi
           % el signo1 es igual a +1 (resp. -1) si la normal apunta hacia el exterior
           % (resp. a interior) del medio a que pertenece el phi del punto de
           % colocacion en la posicion singular rij=0
           % como la convencion es que las normales esten siempre dirigidas hacia
           % abajo, hay que conocer si el phi pertenece a un contorno arriba o
           % abajo. Esta informacion esta en Xim
              
           signo1= (pcXim(m,ir)==1) - (pcXim(m,ir)==2); %#ok
           AtmpT(:,:,ir,iixs) = signo1*0.5*eye(3);
%             end
        else % no singular
          if relatedG==0 % Si solo se resuelven tracciones 
            Mi(ir,iixs) = false; %este no borrarlo
          end
        end
      end
      
      if relatedG==0
        % evitar estos puntos porque ya estan resueltos
         MdistXY(Mi) = NaN; %Remover los ya calculados
      end
    end
    
%-------- Difractado por estratos, cuadratura en campo cercano ------------
    if false % should be true
      % IntegraciOn gaussiana de fuente distribuida en segmento triangular
      %  Nota, aqui fuentes y receptores tienen profundidades similares
          
      [Mi] = MdistXY(:,:) <= longonda*fracCercano;% ~isnan(Mdist(:,:));  % cualquier Z, meh..  
      [irxs,ixss] = find(Mi); %los elementos en Mi
      % Agrupar indices de fuentes (columnas unicas de la matriz):
      [ixssUni] = unique(ixss,'stable');  %e.g.  ixss = ixssUni(ic);
      
      % nota: Se podrian agrupar los puntos de la cuadratura. 
      
      % iixs : Indice global de la fuente (columna)
      %  ir  : Indice global del receptor (renglon)
      
%       su = 0; le = length(ixssUni);
      for iixs = ixssUni.' %  Para cada columna (fuente),
%         disp([num2str(su/le*100) '%']); su = su+1;
        dr = sqrt(pTdA(iixs)/pi); %#ok radio del cIrculo de Area dA(ic)
        for igau = 1:ngau  %  Para cada punto de cuadratura
          Xizq = pTcubPts(3,igau,iixs); %#ok %profundidad de Xi
          [FsourcePSV,FsourceSH,~,~] = VecfuentDWN(Xizq,DWN); % Vec fuente.
          [SolutionPSVGau,SolutionSHGau] = MultAxVecfuenteDWN(DWN.ncapas,DWN.A_DWN, ...
            DWN.B_DWN,FsourcePSV,FsourceSH,DWN.nkr); % Amp. Ondas Planas
          
          for ir = irxs(ixss == iixs).' % todos los receptores que le tocan
            %  distancia en el espacio entre pc y centro d fuente
            pcXaux(1:3) = pcX(1:3,ir)-pTXi(1:3,iixs);
            rij = sum((pcXaux(1:3)) .* (pcXaux(1:3)))^0.5;
            if rij <= facLejecitos*dr && relatedG == 0
              continue
            end
            
            icrr = pcX(4,ir);
            % profundidad relativa a la interface de la capa del receptor:
            zricr = pcX(3,ir) - DWN.z(icrr);
      
            % distancia en el plano XY
            pcXaux(1:3) = pcX(1:3,ir)-pTcubPts(1:3,igau,iixs);
            r   = sum((pcXaux(1:2)) .* (pcXaux(1:2)))^0.5;
            
            % factores de propagacion vertical para estas prof. de fuente y recept
            if rij > facLejecitos*dr
              facT(:,:) = verticalFactors(icrr,DWN.ncapas,zricr,DWN.h(icrr),...
                SolutionPSVGau,SolutionSHGau,...
                DWN.kz(1,:,icrr),DWN.kz(2,:,icrr),... % <-- nu, gamma en estrato icr
                DWN.kr,-1,DWN.nkr); %traccion
            end
            if relatedG~=0
              facG(:,:) = verticalFactors(icrr,DWN.ncapas,zricr,DWN.h(icrr),...
                SolutionPSV,SolutionSH,...
                DWN.kz(1,:,icrr),DWN.kz(2,:,icrr),... % <-- nu, gamma en estrato icr
                DWN.kr, 1,DWN.nkr); %desplazamientos
            end
            
            J0  = besselj(0,DWN.kr*r);
            J1  = besselj(1,DWN.kr*r);
            dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
            d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
            dJ0     =-DWN.kr.*J1;
            d2J0    =-DWN.kr.*dJ1;
            
            if rij > facLejecitos*dr
              MecRadialS = campodiffporestratRadialTransv(-1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facT(:,:),DWN.DK);
            end
            if relatedG~=0 % desplazamientos
              MecRadialU = campodiffporestratRadialTransv(1,...
                r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
                ksi,kpi,facG(:,:),DWN.DK);
            end
            
            if r < 1e-6
              c = 1/sqrt(2);
              s = 1/sqrt(2);
            else
              c = pcXaux(1)/r;
              s = pcXaux(2)/r;
            end
            
            if rij > facLejecitos*dr % ir ~= iixs ?    lejecitos
              MecElemS = campodiffporestratAngular(-1,MecRadialS,c,s,Ci(6,6));
              
              AtmpT(1:3,1:3,ir,iixs) = AtmpT(1:3,1:3,ir,iixs) + ...  % Tij(x,xi)
                Tij_fromMecElem(MecElemS,pcVn(:,ir)) * pTdA(iixs) * CubWts(igau); %#ok  
            end
            if relatedG~=0
              MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
              % MecElemU viene como [(fx fy fz) x (u v w)]
              %Gji -> Gij :  permute to get [(u v w) x (fx fy fz)]
              AtmpG(1:3,1:3,ir,iixs) = AtmpG(1:3,1:3,ir,iixs) + ...
                Gij_fromMecElem(MecElemU,true) .* pTdA(iixs) * CubWts(igau);
            end
          end %ir
        end %igau
      end %iixs
      
       % Los que restan (lejos) como fuente puntual en medio estratificado
       MdistXY(Mi) = NaN; %Remover los ya calculados
    end
    
    % Reducir Mdist para aprovechar la reciprocidad
    if isSelfReciproc
      [irxs,ixss] = find(~isnan(MdistXY));
      for iii = 1:length(irxs)
        ir   = irxs(iii); % Indice global del receptor
        iixs = ixss(iii); % Indice global de la fuente
        if ir < iixs; %si estA por arriba de la diagonal
          % las tracciones que no estAn a la misma profundiad deben
          % resolverse directamente.
          
          Zfs = pcX(3,ir)-pTXi(3,iixs);
          if abs(Zfs) <= longonda*fracZ; % <-- empIrico
            % las profundidades son cercanas o iguales asI que es posible
            % aprovechar con el teorema de reciprocidad.
            MdistXY(ir,iixs) = nan;
          end
        end
      end
    end
    
%----------------- Difractado por estratos, campo lejano ------------------
    [MdisUni,~,disN] = psedoUniqueM(squeeze(MdistXY(:,:)),longonda*fracDist,false);
    if ~isempty(disN) % DWN Fuente y receptor agrupados lejos:
      
      % profundidad relativa a la interface de la capa del receptor:
      zricr = X_z_uni(iX_z_uni) - DWN.z(icr); %#ok
      
      % factores de propagacion vertical para estas prof. de fuente y recept
      facT(:,:) = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
        SolutionPSV,SolutionSH,...
        DWN.kz(1,:,icr),DWN.kz(2,:,icr),... % <-- nu, gamma en estrato icr
        DWN.kr,-1,DWN.nkr); %tracciones
      if relatedG~=0
        facG(:,:) = verticalFactors(icr,DWN.ncapas,zricr,DWN.h(icr),...
          SolutionPSV,SolutionSH,...
          DWN.kz(1,:,icr),DWN.kz(2,:,icr),... % <-- nu, gamma en estrato icr
          DWN.kr, 1,DWN.nkr); %desplazamientos
      end
      
      iDist=0.0; %#ok
      
      % Para cada grupo de distancia horizontal
      for IiDist = 1:length(disN.')%iDist = disN.'
        iDist = disN(IiDist);
        [Mi] = MdisUni == iDist;
        r = mean(mean(MdistXY(Mi))); % distancia (en el plano) promedio
        
%         if r > 1e-6 || relatedG~=0
        %       for iiik = 1:nkss
        %         % funcion en matlab truducida de fortran:
        %         [J0(iiik),J1(iiik)] = BessJ01(DWN.kr(iiik)*r);
        %       end
        %         % funcion por teorema de reciprocidad.
        %       [J0kr,J1kr] = BessJmult(kr,Jkr,nkr,R);
        J0  = besselj(0,DWN.kr*r);
        J1  = besselj(1,DWN.kr*r);
        dJ1     = DWN.kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
        d2J1    =-DWN.kr/r.*J0+J1.*(2/r^2-DWN.kr.^2);
        dJ0     =-DWN.kr.*J1;
        d2J0    =-DWN.kr.*dJ1;
%         end
        % Agregar terminos que solo dependen del radio horizontal e 
        % Integrar en el numero de onda radial
%         if  r > 1e-6
          MecRadialS = campodiffporestratRadialTransv(-1,...
            r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
            ksi,kpi,facT(:,:),DWN.DK);
%         end
        if relatedG~=0 % desplazamientos
          MecRadialU = campodiffporestratRadialTransv(1,...
            r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
            ksi,kpi,facG(:,:),DWN.DK);
        end 
        
        % usar psedoUniqueM con los azimuths es tardado, mejor de una vez.
          
        [irxs,ixss] = find(Mi); %los pares ir,iixs con radio 'r'
        for iii = 1:length(irxs)
          ir   = irxs(iii); % Indice global del receptor
          iixs = ixss(iii); % Indice global de la fuente
          pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
          rij = sum((pcXaux) .* (pcXaux))^0.5;  %   distancia en el espacio
          r = MdistXY(ir,iixs);                   %   distancia en el plano
          dr = sqrt(pTdA(iixs)/pi); %#ok radio del cIrculo de Area dA(ic)
          if r < 1e-6
            c = 1/sqrt(2);
            s = 1/sqrt(2);
          else
            c = pcXaux(1)/r;
            s = pcXaux(2)/r;
          end  
          % --- tracciones campo difractado ----
          if rij > 1e-6 % ir ~= iixs %  % lejecitos  0.5*dr ?
            MecElemS = campodiffporestratAngular(-1,MecRadialS,c,s,Ci(6,6));
            % Bajo la diagonal y los que no se pueden sacar de reciprocos
            AtmpT(1:3,1:3,ir,iixs) = ...  % Tij(x,xi)
              Tij_fromMecElem(MecElemS,pcVn(:,ir)) .* pTdA(iixs);
            
            % Reciprocidad:
            if isSelfReciproc && ... 
                ir > iixs && ... % tomarlo del reciproco bajo la diagonal
                abs(pcXaux(3)) <= longonda*fracZ; % misma profundidad
              MecElemR = MecElemS;
              % fuerza en direcciones X, Y. -sxx -syy -szz -sxy +sxz +syz
              MecElemR(1:2,1:2,1:2) = -MecElemS(1:2,1:2,1:2);
              MecElemR(1:2,3,3)     = -MecElemS(1:2,3,3);
              % fuerza en dirccion Z.       +sxx +syy +szz +sxy -sxz -syz
              MecElemR(3,1:2,3)     = -MecElemS(3,1:2,3);
              MecElemR(3,3,1:2)     = -MecElemS(3,3,1:2);
              % y transponiendo receptor/fuente
              AtmpT(1:3,1:3,iixs,ir) = ...  % Tij(x,xi)
                Tij_fromMecElem(MecElemR,pcVn(:,iixs)) .* pTdA(ir);
            end
%           else % encimados 
%             disp([rij ir iixs])
%             error('singular')
          end
          
          % --- desplazamientos campo difractado ----
          if relatedG~=0 % desplazamientos Gji : [(fx fy fz) x (u v w)]
            if isSelfReciproc
              if ir >= iixs % en o por abajo de la diagonal
                MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
                % MecElemU viene como [(fx fy fz) x (u v w)]
                %Gji -> Gij :  permute to get [(u v w) x (fx fy fz)]
                AtmpG(1:3,1:3,ir,iixs) = ...
                  Gij_fromMecElem(MecElemU,true) .* pTdA(iixs);
                
                if ir > iixs % no la diagonal
                  % recIproco (transpuesto)
                  %Gji -> ok
                  AtmpG(1:3,1:3,iixs,ir) = ...
                    Gij_fromMecElem(MecElemU,false) .* pTdA(ir);
                end
              end
            else %no es autoreciproco, hay que hacerlos todos
              MecElemU = campodiffporestratAngular(1,MecRadialU,c,s,Ci(6,6)); 
              % MecElemU viene como [(fx fy fz) x (u v w)]
              %Gji -> Gij :  permute to get [(u v w) x (fx fy fz)]
              AtmpG(1:3,1:3,ir,iixs) = ...
                Gij_fromMecElem(MecElemU,true) .* pTdA(iixs);
            end
          end
        end % iii
      end %IiDist
    end 
    
%-------- Incidencia directa ----------------------------------------------
    % si la fuente estA en una interfaz el campo ya estA completo
    if true%htop ~= 0
    % si fuente y receptor estAn en el mismo estrato
    if ics == icr(1,1)
      for Iiixs = 1:length(j.') 
        iixs = j(Iiixs); % Indice global de la fuente
        for Iir = 1:length(res_at_iX_z.')
          ir = res_at_iX_z(Iir); % Indice global del receptor
          pcXaux = pcX(1:3,ir)-pTXi(1:3,iixs);
          rij = sum((pcXaux) .* (pcXaux))^0.5;
          dr = sqrt(pTdA(iixs)/pi); %radio del cIrculo de Area dA(ic)
          g = pcXaux./rij;
          
          jjCirculo  = rij <= 0.5*dr;
          jjMidRange = rij >  0.5*dr & rij <= longonda/2;  %/6
          jjLejos    = rij >  longonda/2;
          
%           jjCirculo  = ir == iixs;%false;%rij<1.0*dr;
%           jjMidRange = ir ~= iixs & rij<=2*dr;%1.0*dr & rij<=longonda/3;  %/6
%           jjLejos    = ir ~= iixs & rij>2*dr;%longonda/3;

%           jjCirculo = rij <= 1e-6;
%           jjMidRange = false;
%           jjLejos = ~jjCirculo;
          
          Gij  = complex(zeros(3,3,1,1));
          Tij  = complex(zeros(3,3,1,1));
          if jjCirculo % campo muy cercano
            % ------  tracciones  ------
            % en la diagonal o demasiado cerca
            %   +-0.5   ya se ha encargado [ campo diff por estr ].
%              signo1= (pcXim(m,ir)==1) - (pcXim(m,ir)==2);
%               AtmpT(:,:,ir,iixs) = signo1*0.5*eye(3);
              
            % ------  desplazamientos  ------
            if relatedG~=0
              BEALF = real(kpi)/real(ksi);
              Vn = pcVn(1:3,iixs);
              if rij > facLejecitos*dr
                EPS = rij/dr;
                % [(u v w) x (fx fy fz)]
                Gij=Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),g(1:3),EPS);
              else
                Gij=Greenex_3D(ksi,dr,BEALF,Vn,Ci(6,6),0,0);
              end
            end
          end
          
          if jjMidRange  % campo intermedio
            % ------  tracciones  ------
            Tij = Tij_3D_r_smallGENf(...
              pcX(1:3,ir),pcVn(1:3,ir),...
              pTcubPts(1:3,1:ngau,iixs),CubWts,...
              ksi,kpi,0);
            % ------  desplazamientos  ------
            if relatedG~=0
              %func de Green [(fx fy fz) x (u v w)]
              Gij = Gij_3D_r_smallGENf(pcX(1:3,ir),...
                pTcubPts(1:3,1:ngau,iixs),CubWts(1:ngau),...
                ksi,kpi,Ci);
              % permute: [(u v w) x (fx fy fz)]
              Gij(:,:,1,1) = squeeze(Gij).';
            end
          end
          
          if jjLejos
            % ------  tracciones  ------
            Tij = Tij_3Done(ksi,kpi,rij,g,pcVn(:,ir));
            % ------  desplazamientos  ------
            if relatedG~=0
              Gij = Gij_3Done(ksi,kpi,rij,g,Ci);
            end
          end
          
          % Multiplicar por el Area y agregar a difractado
          AtmpT(1:3,1:3,ir,iixs) = AtmpT(1:3,1:3,ir,iixs) + Tij .* pTdA(iixs);
          if relatedG~=0
            AtmpG(1:3,1:3,ir,iixs) = AtmpG(1:3,1:3,ir,iixs) + Gij .* pTdA(iixs);
          end
        end
      end
    end
    end
  end % iXiz_uni:Profundidad Unica putnos de colocaciOn
  
  % copiar las variables temporales al arreglo repartido entre procesos
  At(iXiz_uni).T.matrix = AtmpT;
  if relatedG~=0 % desplazamientos
    Ag(iXiz_uni).G.matrix = AtmpG;
  end
end %parfor iXiz_uni:Profundidad Unica de las fuentes

% juntar los resultados:
AbT  = complex(zeros(3,3,nrens,ncols));
for iXiz_uni=1:leXiz_uni
  AbT = AbT + At(iXiz_uni).T.matrix;
end

if relatedG~=0 % desplazamientos 
  AbG  = complex(zeros(3,3,ncols,nrens));
  for iXiz_uni=1:leXiz_uni
    AbG = AbG + Ag(iXiz_uni).G.matrix;
  end
else
  AbG  = complex(zeros(3,3,1,1));
end
% signo de acuerdo al Indice del medio
% en un punto de colocacion, hay siempre 2 medios en contacto,
% aunque a veces este medio es el vacio
% las ecuaciones de continuidad de traccion o de desplazamiento
% involucran siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
% y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
% se reorganiza entonces como :
% u_diff(m)-u_diff(m1)=u_inc(m1)-u_inc(m)
% los signos se fijan deacuerdo con el indice del medio,
% + si el indice es el mas pequeno de los medios en contacto
% - en lo contrario
for iii = 1:nrens
  AbT(:,:,iii,:) = AbT(:,:,iii,:) * ((m==im0(iii)) - (m~=im0(iii)));
end
if relatedG~=0
  for iii = 1:nrens
    AbG(:,:,iii,:) = AbG(:,:,iii,:) * ((m==im0(iii)) - (m~=im0(iii)));
  end
end
end %function

function [ksi,kpi,Ci] = theSub(sub,icr)
ksi 	= sub(icr).ksi;
kpi 	= sub(icr).kpi;
Ci    = sub(icr).Ci;
end

