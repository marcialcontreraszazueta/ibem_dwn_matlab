function dibujo_estratifi(para,bouton,INaxe_estrDWN)
%% Graficar la estratificación
otrapag = false;
if ishandle(INaxe_estrDWN)
  axe_estrDWN = INaxe_estrDWN;
  if isfield(bouton,'med') && ishandle(bouton.med)
    med     =get(bouton.med,'value');
  else
    otrapag = true;
  end
else
  otrapag = true;
end

if otrapag
  figure(32947202)
  set(gcf,'Name','Estratificacion','numberTitle','off');
  axe_estrDWN = gca;
  med = 1:para.nmed;
end

if ishandle(axe_estrDWN)
axes(axe_estrDWN); iim = 1;
for im = 1:length(med)
if para.tipoMed(med(im))==2 %&& ishandle(axe_estrDWN)
  if length(med) > 1
  subplot(1,length(med),iim);
  end
  cla;
  title(['medio ' num2str(med(im))]);
  iim=iim+1;
%   hfigProf = figure(321);
%   set(hfigProf,...
%     'Units','normalized','name','Estratificación',...%'color',[1 1 1], ...
%     'numberTitle','off','DockControls','off');
%     'position',[0.005 0.01 .51 .9], ...
     ParaRegSub = para.reg(med(im)).sub(:);
     if para.dim == 1 && para.pol == 1
       ShouldUseAlf = false;
     else
       ShouldUseAlf = true;
     end
  plotProfile(ParaRegSub,ShouldUseAlf);
end
end
end


end