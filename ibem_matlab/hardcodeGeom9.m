function val = hardcodeGeom9

% circular
nn = 100;
val = zeros(nn,2);
th = linspace(pi,0,nn);
for ii=1:1:nn
    val(ii,1)=cos(th(ii));
    val(ii,2)=sin(th(ii));
end


% rio de los remedios
% val = [[2 0];
%        [9 5];
%        [15.5 5];
%        [22.5 0]];

% para ejercicio 031
% val =[[2000 50];
%       [3000 0];
%       [3050 0]];

% figure;plot(val(:,1),val(:,2))
end