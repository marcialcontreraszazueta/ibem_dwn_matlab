% script para pegar resultados de varia corridas

%% Ejercicio 1    0-9.84Hz @0.12 con R = 1
nres = 42;
ninci = 3;
jini = 0;
jfin = 82;

resrango = 22:42; % 1:21;

uwo0 = complex(zeros(83,nres,ninci,3));
k = 1;

  % frecuencia cero que se me hab�a olvidado
  j=0;
  name = ['../out/clu0/tmp','_',num2str(j),'tmp.mat'];
  a = exist(name,'file');
  if a==2
    load(name,'uw');
    uwo0(k,:,:,:) = uw;
  else
    error('buh0');
  end
  k = k+1;
  
for j=jini+1:jfin
  % desde clu3   0.12, 0.24, ...
  name = ['../out/clu1/tmp','_',num2str(j),'tmp.mat'];
  a = exist(name,'file');
  if a==2
    load(name,'uw');
    uwo0(k,:,:,:) = uw;
  else
    error(['buh' num2str(j)]);
  end
  k = k+1;
end
disp('done')

%% verlos
figure; hold on
nfrecs = 83;
% resrango = 22:42; % 1:21;
xax = linspace(0,9.84,nfrecs);
offset = max(max(real(uwo0(:,resrango,1,1))))/3;
for ir = resrango
plot(xax,squeeze(real(uwo0(:,ir,1,1)))+ offset*(ir-1),'k-')
end

%% Ejercicios hasta 5Hz @0.06 con R = 2
nres = 42;
ninci = 3;
jini = 1;
jfin = 41; %  x 2

uwo = complex(zeros(83,nres,ninci,3));
k = 1;

  % frecuencia cero que se me hab�a olvidado
  j=0;
  name = ['../out/clu0/tmp','_',num2str(j),'tmp.mat'];
  a = exist(name,'file');
  if a==2
    load(name,'uw');
    uwo(k,:,:,:) = uw;
  else
    error('buh0');
  end
  k = k+1;

for j=jini:jfin
  % desde clu3 0.00, 0.12, 0.24, ...
  name = ['../out/clu3/tmp','_',num2str(j),'tmp.mat'];
  a = exist(name,'file');
  if a==2
    load(name,'uw');
    uwo(k,:,:,:) = uw;
  else
    error('buh');
  end
  
  % desde clu2 (con desfase df/2)  0.06, 0.18, ...
  name = ['../out/clu2/tmp','_',num2str(j),'tmp.mat'];
  a = exist(name,'file');
  if a==2
    load(name,'uw');
    uwo(k+1,:,:,:) = uw;
  else
    error('buh');
  end
  
  k = k+2;
end
disp('done')

%% verlos
% figure; 
hold on
nfrecs = 83;
% resrango = 22:42; % 1:21;
xax = linspace(0,4.92,nfrecs);
% offset = max(max(real(uwo(:,resrango,1,1))))/10;
for ir = resrango
plot(xax,squeeze(real(uwo(:,ir,1,1)))+ offset*(ir-1),'r-')
end
%% Ejercicio hasta 10Hz

% frecuencias entre 5 y 10Hz e interpolar resultados para mantener DeltaF
nres = 42;
ninci = 3;
jini = 42; % 4.92Hz
jfin = 83; % 9.84Hz
uwo2 = uwo0(jini:jfin,:,:,:); % El tramo que se va a interpolar
uwo3 = complex(zeros(42*2,nres,ninci,3));
for ir = 1:nres
  for iinc = 1:ninci
    for comp = 1:3
      yr = interp(real(uwo2(:,ir,iinc,comp)),2);
      yi = interp(imag(uwo2(:,ir,iinc,comp)),2);
      uwo3(:,ir,iinc,comp) = yr + 1i*yi;
    end
  end
end
disp('done')
%% verlos
% figure; 
hold on
nfrecs = 84;
% resrango = 22:42; % 1:21;
xax = linspace(4.92,9.84+0.06,nfrecs);
% offset = max(max(real(uwo(:,resrango,1,1))))/10;
for ir = resrango
plot(xax,squeeze(real(uwo3(:,ir,1,1)))+ offset*(ir-1),'b-')
end

%% Pegar uwo (rojo) y uwo3 (azul)
nres = 42;
ninci = 3;
uw = complex(zeros(83+83,nres,ninci,3));
uw(1:83,:,:,:) = uwo;    % ok
uw(84:end,:,:,:) = uwo3(2:end,:,:,:); %ok
% promediar el punto de match
for ir = 1:nres
  for iinc = 1:ninci
    for comp = 1:3
      uw(84,ir,iinc,comp) = mean([uw(83,ir,iinc,comp) uw(85,ir,iinc,comp)]); 
    end
  end
end

% guardarlos
save('resultEnFrec.mat','para','uw')
disp('guardado')
%% verlo
hold on
nfrecs = 83+83; disp(nfrecs)
% resrango = 22:42; % 1:21;
xax = 0:(nfrecs-1);
xax=xax.*0.06;%linspace(0,9.84+0.06,nfrecs);
fmax = xax(end);
% offset = max(max(real(uwo(:,resrango,1,1))))/10;
for ir = resrango
plot(xax,squeeze(real(uw(:,ir,1,1)))+ offset*(ir-1),'k*-')
end
%% Hacer sismogramas y verlos
disp('Inversion de los espectros');
nfN = nfrecs;
nf = (nfN-1)*2;  disp(['nf = ',num2str(nf)])
para.nf = nf;
para.fmax = fmax;
[utc,~]   = inversion_w(uw,[],para);
disp('done')


df      = para.fmax/nfN;     disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       disp(['Fmx= ',num2str(Fq(end))])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
tps = (0:para.zeropad-1)*dt; disp(['tmx= ',num2str(tps(end))])
save('resultEnTiemp.mat','utc','tps')
disp('guardado')



%% graficar un receptor en particular
ir = 3;
iinc = 3;
icomp = 1;

%
figure;
set(gcf,'name','espectro')
subplot(2,1,1)
cla
plot(Fq,real(uw(:,ir,iinc,icomp)),'r');hold on;
plot(Fq,imag(uw(:,ir,iinc,icomp)),'b');
plot(Fq,abs(uw(:,ir,iinc,icomp)),'k')
ax1 = gca;
ax1_pos = ax1.Position;
ax1.Box = 'off';
xlabel('frecuencia en Hertz')
ax2=axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');
xlim(ax1.XLim)
nn = length(ax1.XTick);
ax2.XTickMode = 'auto';
ax2.XTickLabelMode = 'manual';
cl = cell(nn,1);
cl{1} = ' ';
for i=2:nn
    cl{i} = num2str(1/ax1.XTick(i),3);
end
ax2.XTickLabel = cl;
set(gca,'ytick',[])
title(['ir' num2str(ir) ' iinc' num2str(iinc) ' icomp' num2str(icomp)])
xlabel('Periodo en segundos')
subplot(2,1,2)
cla
plot(tps,real(utc(:,ir,iinc,icomp)),'r');hold on;
plot(tps,imag(utc(:,ir,iinc,icomp)),'b');
title('Ond�cula')
xlabel('tiempo en segundos')




