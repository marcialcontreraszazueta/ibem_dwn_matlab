% cargar para RESULT
clear
load('../out/ResMonte/Montb.mat')
whos
%% dibujar configuracion geometrica
bouton = [];
bouton.iinc = 1;
figure(1001)
set(gcf,'Name','Configuracion geom?trica','numberTitle','off');
dibujo_conf_geo(para,gca)
axe_conf_geo = gca;

%% graficar resultados
bouton.dessinsp.Value=1;
bouton.normalise.Value=1;
bouton.couleur.Value=2;
bouton.couleur.String = {'b','k','r','m','c','g'};
para.redraw = 1;
paras = dibujo(para,bouton,RESULT,true);
%% graficar un resultado
figure(543); clf; hold on
recep = 1;
inc = 1;
var = RESULT.stc(:,recep,inc,1);
plot(real(var),'k-')
plot(imag(var),'k--')
%% graficar elementos mecanicos alrededor del tunel
% recep = 1:61;
recep = 62:122;
j = 2;
inc = 1;
th = linspace(0,2*pi,length(recep));
sxx = (RESULT.sw(j,recep,inc,1));
sxz = (RESULT.sw(j,recep,inc,2));
szz = (RESULT.sw(j,recep,inc,3));

si = sin(th);
co = cos(th);
stt = abs(si.^2.*sxx+co.^2.*szz-2.*si.*co.*sxz);
srt = abs(si.*co.*(szz-sxx)+sxz.*(co.^2-si.^2));
% figure(765432);clf;polar(th,stt);
figure(1);clf;plot(th,stt); title('stt');
% figure(7);clf;polar(th,srt); title('srt');

u = (RESULT.uw(j,recep,inc,1));
w = (RESULT.uw(j,recep,inc,2));
% figure(765432);clf;polar(th,u)
figure(2);clf;plot(th,abs(u)); title('u')
figure(3);clf;plot(th,abs(w)); title('w')
%
figure(4);clf;polar(th,stt);title('stt');
figure(5);clf;polar(th,abs(u)); title('u')
figure(6);clf;polar(th,abs(w)); title('w')
%
figure(8);clf;plot(th,abs(sxx)); title('sxx');
figure(9);clf;plot(th,abs(szz)); title('szz');
figure(10);clf;plot(th,abs(sxz));title('sxz');
%
figure(8);clf;polar(th,sxx); title('sxx');
figure(9);clf;polar(th,szz); title('szz');
figure(10);clf;polar(th,sxz);title('sxz');
%% colocar receptores en tunel
cen = [0,0,1.5];

off = 0.001; % offset desde la frontera
rex = 1.1 - off;
rin = 0.5 + off;
nsec = 60; % numero de puntos todo alrededor
dth = (360/nsec)*pi/180;

nrecep = nsec * 2 +2;
res = zeros(nrecep,3);

for i = 0:nsec
  th = dth*i;
  p = cen + rex.*[cos(th),0,sin(th)];
  res(i+1,:) = p;
  p = cen + rin.*[cos(th),0,sin(th)];
  res(i+1+nsec+1,:) = p;
end

% graf
figure(6543);clf;hold on; axis equal
plot(res(1:nsec+1,1),res(1:nsec+1,3),'k.')
plot(res(nsec+2:end,1),res(nsec+2:end,3),'r.')

% escribir ../ins/Recep.txt
dlmwrite('../ins/Recep.txt',res,' ')

%% Receptores Croissant
a = 1;
d = 0.075745;
v = linspace(0,47,48)*d-1.82;
res = zeros(48*2,3);
for i=1:48
  res(i,1) = v(i)*a;
  res(i+48,2) = v(i)*a;
end
dlmwrite('../ins/Recep.txt',res,' ')
disp('done')

%% Receptores en Gaussiana asim
h = 0.18;
x0=0;
y0=0;
sx=0.25;
sy=0.125;
nrecep = 21;
res = zeros(2*nrecep,3);
xini = 1.0;
for i=1:nrecep
  res(i,1) = -xini + (i-1)*(2*xini)/(nrecep-1);
  res(i,3) = -ColGau(h,x0,y0,sx,sy,res(i,1),0);
end
yini = 1.0;
for i=1+nrecep:2*nrecep
  res(i,2) = -yini + (i-1-nrecep)*(2*yini)/(nrecep-1);
  res(i,3) = -ColGau(h,x0,y0,sx,sy,res(i,2),0);
end
dlmwrite('../ins/Recep.txt',res,' ')
disp('done')

%% Receptores en Gaussiana axisim
nrecep = 61;
res = zeros(nrecep,3);
xini = 0.0;
xfin = 2.0;
for i=1:nrecep
  res(i,1) = -xini + (i-1)*(xfin)/(nrecep-1);
  res(i,3) = GaussianaSS1983(res(i,1),0);
end
dlmwrite('../ins/Recep.txt',res,' ')
disp('done')

%% Figura Gauss axisim
[RGB] = imread('/Users/marshall/Documents/DOC/unit9/ridge/fondoGau.png');
figure; image(RGB)
axes
%% Receptores en Circular
a = 1;
nrecep = 30;
res = zeros(nrecep,3);
x0=0;
y0=0;
xini = 0.0;
xfin = -2.5;
for i=1:nrecep
  x= xini + (i-1)*(xfin)/(nrecep-1);
  res(i,1) = x;
  res(i,3) = SemiEsfera(a,x0,y0,x,0) + 0.01;
end
dlmwrite('../ins/Recep.txt',res,' ')
disp('done')

%% graficar respuesta en croissant

% cargar frec 0
r = load('../out/ValCroiss_FF0Hz.mat','uw');
uw = r.uw; clear r
j = 1;

r = load('../out/ValCroiss0_125Hz.mat','uw');
uw(2,:,:,:) = r.uw;
size(uw)
%%
figure(43);hold on;
plot(squeeze(abs(uw(2,1:11,1,1))),'k*-')
% plot(squeeze(abs(uw(2,1:11,1,2))),'r*-')
% plot(squeeze(abs(uw(2,1:11,1,3))),'b*-')
%%
figure(46);hold on;
plot(squeeze(abs(uw(1,1:21,3,1))),'r-')
plot(squeeze(abs(uw(1,1:21,3,2))),'g-')
plot(squeeze(abs(uw(1,1:21,3,3))),'b-')

figure(47);hold on;
plot(squeeze(abs(uw(1,22:42,3,1))),'r-')
plot(squeeze(abs(uw(1,22:42,3,2))),'g-')
plot(squeeze(abs(uw(1,22:42,3,3))),'b-')
%% receptores
figure(43);hold on;
plot(squeeze(abs(uw(j+1,1:48,1,1))),'k*-')
plot(squeeze(abs(uw(j+1,1:48,1,2))),'r*-')
plot(squeeze(abs(uw(j+1,1:48,1,3))),'b*-')
figure(45);hold on;
plot(squeeze(abs(uw(j+1,49:96,1,1))),'k*-')
plot(squeeze(abs(uw(j+1,49:96,1,2))),'r*-')
plot(squeeze(abs(uw(j+1,49:96,1,3))),'b*-')
%% receptores
figure(43);hold on;
plot(squeeze(abs(uw(j+1,1:48,1,1))),'k-')
plot(squeeze(abs(uw(j+1,1:48,1,2))),'r-')
plot(squeeze(abs(uw(j+1,1:48,1,3))),'b-')
figure(45);hold on;
plot(squeeze(abs(uw(j+1,49:96,1,1))),'k-')
plot(squeeze(abs(uw(j+1,49:96,1,2))),'r-')
plot(squeeze(abs(uw(j+1,49:96,1,3))),'b-')
% receptores
figure(43);hold on;
plot(squeeze(abs(uw(j+1,1:48,1,1))),'k.-')
plot(squeeze(abs(uw(j+1,1:48,1,2))),'r.-')
plot(squeeze(abs(uw(j+1,1:48,1,3))),'b.-')
figure(45);hold on;
plot(squeeze(abs(uw(j+1,49:96,1,1))),'k.-')
plot(squeeze(abs(uw(j+1,49:96,1,2))),'r.-')
plot(squeeze(abs(uw(j+1,49:96,1,3))),'b.-')
% receptores
figure(43);hold on;
plot(squeeze(abs(uw(j+1,1:48,1,1))),'k--')
plot(squeeze(abs(uw(j+1,1:48,1,2))),'r--')
plot(squeeze(abs(uw(j+1,1:48,1,3))),'b--')
figure(45);hold on;
plot(squeeze(abs(uw(j+1,49:96,1,1))),'k--')
plot(squeeze(abs(uw(j+1,49:96,1,2))),'r--')
plot(squeeze(abs(uw(j+1,49:96,1,3))),'b--')
%
figure(43);hold on;
plot(squeeze(abs(uw(2,1:48,2,1))),'k*-')
plot(squeeze(abs(uw(2,1:48,2,2))),'r*-')
plot(squeeze(abs(uw(2,1:48,2,3))),'b*-')
figure(45);hold on;
plot(squeeze(abs(uw(2,49:96,2,1))),'k*-')
plot(squeeze(abs(uw(2,49:96,2,2))),'r*-')
plot(squeeze(abs(uw(2,49:96,2,3))),'b*-')
%
figure(542332);clf;
col = ['r' 'g' 'b'];
mark = ['x' 'v' '.'];
for iiii=1:3
  for jjjj=1:3
    spy(squeeze(abs(Ab(iiii,jjjj,:,:))),...
      [col(iiii) mark(jjjj)],15);hold on
  end
end
%
figure(52342);clf;
col = ['r' 'g' 'b'];
mark = ['x' 'v' '.'];
for iiii=1:3
  for jjjj=1:3
    spy(squeeze(abs(AbR(iiii,jjjj,:,:))),...
      [col(iiii) mark(jjjj)],15);hold on
  end
end
%
figure(43);hold on;
plot(squeeze(real(uw(2,1:48,1,1))),'k*-')
plot(squeeze(real(uw(2,1:48,1,2))),'r*-')
plot(squeeze(real(uw(2,1:48,1,3))),'b*-')
figure(45);hold on;
plot(squeeze(real(uw(2,49:96,1,1))),'k*-')
plot(squeeze(real(uw(2,49:96,1,2))),'r*-')
plot(squeeze(real(uw(2,49:96,1,3))),'b*-')
%
figure(43);hold on;
plot(squeeze(imag(uw(2,1:48,1,1))),'k*-')
plot(squeeze(imag(uw(2,1:48,1,2))),'r*-')
plot(squeeze(imag(uw(2,1:48,1,3))),'b*-')
figure(45);hold on;
plot(squeeze(imag(uw(2,49:96,1,1))),'k*-')
plot(squeeze(imag(uw(2,49:96,1,2))),'r*-')
plot(squeeze(imag(uw(2,49:96,1,3))),'b*-')
%% cargar desde IBEM homogeneo 0
% cd '/Users/marshall/Documents/DOC/unit9/canonSemiEsfe/enHomog'
cd '/Users/marshall/Documents/DOC/unit8/teamHomogeneo/esfe'
arch= 'OndaP1.txt';
nrecep = 50;
nfrec = 1;
% leer datos

fileID = fopen(arch,'r');
vfrec = zeros(nfrec,1);
Uu = zeros(nfrec,nrecep);
Uv = zeros(nfrec,nrecep);
Uw = zeros(nfrec,nrecep);
for j=1:nfrec
a = fscanf(fileID,'%f %d',2); vfrec(j)=a(1); 
disp(['[' num2str(j) ']= ' num2str(vfrec(j)) ' Hertz'])
au = fscanf(fileID,'%f',2*nrecep);
Uu(j,:) = au(1:2:2*nrecep)+au(2:2:2*nrecep)*1i;
av = fscanf(fileID,'%f',2*nrecep);
Uv(j,:) = av(1:2:2*nrecep)+av(2:2:2*nrecep)*1i;
aw = fscanf(fileID,'%f',2*nrecep);
Uw(j,:) = aw(1:2:2*nrecep)+aw(2:2:2*nrecep)*1i; 
end
fclose(fileID);
cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'
disp('done')
figure(1);hold on;
xax = linspace(0,3,nrecep);
plot(xax,squeeze(abs(Uu(j,1:nrecep))),'k-')
plot(xax,squeeze(abs(Uv(j,1:nrecep))),'r-')
plot(xax,squeeze(abs(Uw(j,1:nrecep))),'b-')

%% cargar desde resultados IBEM homogeneo
cd '/Users/marshall/Documents/DOC/unit8/teamHomogeneo/test0/'
arch= 'OndaP2.txt';
nrecep = 48*2;
nfrec = 1024+1;
factor = 1;

% leer datos

fileID = fopen(arch,'r');
vfrec = zeros(nfrec,1);
Uu = zeros(nfrec,nrecep);
Uv = zeros(nfrec,nrecep);
Uw = zeros(nfrec,nrecep);
for j=1:nfrec
a = fscanf(fileID,'%f %d',2); vfrec(j)=a(1); 
% disp(['[' num2str(j) ']= ' num2str(vfrec(j)) ' Hertz'])
au = fscanf(fileID,'%f',2*nrecep);
Uu(j,:) = au(1:2:2*nrecep)+au(2:2:2*nrecep)*1i;
av = fscanf(fileID,'%f',2*nrecep);
Uv(j,:) = av(1:2:2*nrecep)+av(2:2:2*nrecep)*1i;
aw = fscanf(fileID,'%f',2*nrecep);
Uw(j,:) = aw(1:2:2*nrecep)+aw(2:2:2*nrecep)*1i; 
end
fclose(fileID);


cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'
disp(['Done reading file ' arch])

nfN = nfrec;    % indice de frecuenica de Nyquist
nf = 2*(nfN-1); % cantidad de frecuencias positivas y negativas
fmax = vfrec(end); % vector de frecuencias

fmax = fmax / factor;

df = fmax/(nf/2);  % paso de frecuencia
Fq = (0:nf/2)*df;  % vector con las frecuencias
disp(['fini =',num2str(vfrec(1))])
disp(['T =',num2str(1/df)])
disp(['df =',num2str(df)])
disp(['fmax =',num2str(vfrec(end))])
%
thisfrec = 2;%129; % 129 para 0.5
fra = 1;
disp(vfrec(thisfrec))
figure(43);hold on;
plot(squeeze(abs(Uu(thisfrec,1:48))/fra),'k*-')
plot(squeeze(abs(Uv(thisfrec,1:48))/fra),'r*-')
plot(squeeze(abs(Uw(thisfrec,1:48))/fra),'b*-')

figure(45);hold on;
plot(squeeze(abs(Uu(thisfrec,49:96))/fra),'k*-')
plot(squeeze(abs(Uv(thisfrec,49:96))/fra),'r*-')
plot(squeeze(abs(Uw(thisfrec,49:96))/fra),'b*-')


%% Revisar GREENEX
X = [0.00000000      -1.50000000       0.00000000];
XI= [ -4.70249988E-02  -1.44702494       0.00000000 ];
XNM=X(1)-XI(1);
YNM=X(2)-XI(2);
ZNM=X(3)-XI(3);
RNM=sqrt(XNM^2+YNM^2+ZNM^2);
CAKA = 6.28318548 + -6.28318521E-04i;
R = 7.97879994E-02; % radio del circulo
BEALF = 0.577350259;
VN = [0 0 -1];
EPS=RNM/R;
g(1)=XNM/RNM;
g(2)=YNM/RNM;
g(3)=ZNM/RNM;
mu = 1;
GNIJ=Greenex_3D(CAKA,R,BEALF,VN,mu,g,EPS)

%  - : - comparar con: OK
%  (  1.16948664    ,-0.350118995    )  |  ( -4.12974954E-02,  7.66981114E-03)  |  (  0.00000000    ,  0.00000000    )
%  ( -4.12975028E-02,  7.66981114E-03)  |  (  1.17935050    ,-0.351950914    )  |  (  0.00000000    ,  0.00000000    )
%  (  0.00000000    ,  0.00000000    )  |  (  0.00000000    ,  0.00000000    )  |  ( 0.920338333    ,-0.338410586    )
%  -----