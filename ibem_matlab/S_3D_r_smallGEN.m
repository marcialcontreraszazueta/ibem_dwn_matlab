function [S_fx,S_fy,S_fz]=S_3D_r_smallGEN(coord,xr,jjpb,ks,kp,para)
% xr son las coordenadas del receptor
nj      = length(jjpb);   % cantidad de fuentes virtuales
S_fx=zeros(3,3,nj); S_fy=zeros(3,3,nj); S_fz=zeros(3,3,nj);

% Para cada fuente virtual se integre de todos los puntos
for iXi = 1:size(jjpb,2)
  ngau    = para.gaussian.ngau;% cantidad de puntos gaussianos por fuente
  CubPts = coord.CubPts(1:3,1:ngau);
  CubWts = para.cubature(1:ngau);
  
  % cosenos directores entre el receptor y las fuentes
  xij = xr(1)-CubPts(1,:);
  yij = xr(2)-CubPts(2,:);
  zij = xr(3)-CubPts(3,:);
  rij     = sqrt(xij.^2+yij.^2+zij.^2);
  g(1,:)  = xij./rij;
  g(2,:)  = yij./rij;
  g(3,:)  = zij./rij;
  
  %% Funci�n de Green fuente puntual:
  [sr_fx,sr_fy,sr_fz]=S_3D(rij,g,ks,kp,[1 1 1]);
  if nj~=1
    for i=1:3
      for j=1:3
        S_fx(i,j,iXi) = sum(CubWts.*squeeze(sr_fx(i,j,:)),1);
        S_fy(i,j,iXi) = sum(CubWts.*squeeze(sr_fy(i,j,:)),1);
        S_fz(i,j,iXi) = sum(CubWts.*squeeze(sr_fz(i,j,:)),1);
      end
    end
  else
    for i=1:3
      for j=1:3
        S_fx(i,j,iXi) = sum(CubWts.*squeeze(sr_fx(i,j,:)));
        S_fy(i,j,iXi) = sum(CubWts.*squeeze(sr_fy(i,j,:)));
        S_fz(i,j,iXi) = sum(CubWts.*squeeze(sr_fz(i,j,:)));
      end
    end
  end
end                    
end