function [Udiff,Sdiff]=int_fv_Gij_3D(phi_fv,coordf,para,m,xr,yr,zr,gaussian,kpi,ksi,C)
% int_fv_Gij_3D Integrar funciones de Green entre superficie y receptor y
% multplicar por densidad de fuerza.
fracCercano = 1/3;

%variables solicitadas
ninc    = para.ninc;
sal     = para.sortie;
ns      = (sal.Ux + sal.Uy + sal.Uz)*sal.Ut;
nss   	= sal.sxx + sal.syy + sal.szz+sal.sxy+sal.sxz+sal.syz;
%init el campo difractado
Udiff   = zeros(ns ,ninc);
Sdiff   = zeros(nss,ninc);

%posicion de TODOS las fuentes auxiliares
xf      = coordf.x;
yf      = coordf.y;
zf      = coordf.z;
dr      = coordf.drxz; % radio de un circulo de area dA
dA      = coordf.dA; % area del segmento escudo o triangular
%indice lOgico de la posicion de las fuentes auxiliares en m
ii = coordf.Xim(:,m) ~= 0;
iifn = find(ii);
%indice de los phi en el medio m
jj = coordf.phi(:,m);
jj(jj==0)=[];
njj     = length(jj);

%cantdiad de ecuaciones
nbeq    = coordf.nbeq;

%calculo de la suma de cada una de las contribuciones
xfm = xf(ii);
yfm = yf(ii);
zfm = zf(ii);

xij     = xr-xfm;
yij     = yr-yfm;
zij     = zr-zfm;
drj     = dr(ii);
dAj     = dA(ii);
RNM     = sqrt(xij.^2+yij.^2+zij.^2);
g       = zeros(3,njj);
g(1,:)  = xij./RNM;
g(2,:)  = yij./RNM;
g(3,:)  = zij./RNM;

longonda = 2*pi/ksi;
BEALF = real(kpi)/real(ksi);

%integracion gaussiana donde se requiere
if para.dim == 4 % 3Dgeneral
  jjCirculo  = find(RNM<=0.5*drj);
  jjMidRange = find(RNM>0.5*drj & RNM<=fracCercano*longonda);
else %3D axisimEtrico
  jjCirculo  = find(RNM<para.npplo/2*drj);
end

% %% test me
%  figure(2654321);clf; plot3(xfm,yfm,zfm,'ko');set(gca,'Zdir','reverse'); axis equal
%  figure(2654321);hold on; plot3(xr,yr,zr,'r*')
%  figure(2654321);hold on;a = iifn(jjCirculo); plot3(xf(a),yf(a),zf(a),'mo')
%  figure(2654321);hold on;a = iifn(jjMidRange);plot3(xf(a),yf(a),zf(a),'bo')
% %%

if sal.Ut==1
  % [(fx fy fz) x (u v w)]==[(u v w) x (fx fy fz)]
  Gij = Gij_3D(ksi,kpi,RNM,g,C,njj);   %todos aunque algunos que no se usan
  if ~isempty(jjMidRange)
    % campo intermedio
    % [(u v w) x (fx fy fz)]
    Gij(:,:,jjMidRange) = Gij_3D_r_smallGEN(...
      coordf,[xr,yr,zr],iifn(jjMidRange),... %<-- usa iifn( para referir a indice global en coordf
      ksi,kpi,para,C,true);
  end
  
  if ~isempty(jjCirculo)
    % campo cercano
    Vn(1,:)=coordf.vnx(ii);
    Vn(2,:)=coordf.vny(ii);
    Vn(3,:)=coordf.vnz(ii);
    for iXi = jjCirculo
      if RNM(iXi) > 0.01*drj(iXi)
        EPS = RNM(iXi)/drj(iXi);
        gg = g(1:3,iXi); % [(u v w) x (fx fy fz)]
        GNIJ=Greenex_3D(ksi,drj(iXi),BEALF,Vn(1:3,iXi),C(6,6),gg,EPS);
      else
        GNIJ=Greenex_3D(ksi,drj(iXi),BEALF,Vn(1:3,iXi),C(6,6),0,0);
      end
      Gij(:,:,iXi) = GNIJ; % [(u v w) x (fx fy fz)]
    end
  end

%   %% testme
% figure;
% a =[squeeze(Gij(1,1,:));squeeze(Gij(1,2,:));squeeze(Gij(1,3,:))]*dAj(1);
% subplot(3,1,1);hold on;plot(real(a),'r');plot(imag(a),'b')
% a =[squeeze(Gij(2,1,:));squeeze(Gij(2,2,:));squeeze(Gij(2,3,:))]*dAj(1);
% subplot(3,1,2);hold on;plot(real(a),'r');plot(imag(a),'b')
% a =[squeeze(Gij(3,1,:));squeeze(Gij(3,2,:));squeeze(Gij(3,3,:))]*dAj(1);
% subplot(3,1,3);hold on;plot(real(a),'r');plot(imag(a),'b')
  
end

if sal.sxx==1 || sal.syy==1 || sal.szz==1 || sal.sxy==1 || sal.sxz==1 || sal.syz==1
  [S_fx,S_fy,S_fz]    = S_3D(RNM,g,ksi,kpi,[1 1 1]);
  if ~isempty(jjCirculo)
    if para.dim == 4
      [S_fx(:,:,jjCirculo),S_fy(:,:,jjCirculo),S_fz(:,:,jjCirculo)] = ...
        S_3D_r_smallGEN(coordf,[xr,yr,zr],jjCirculo,ksi,kpi,para);
    else
      [S_fx(:,:,jjCirculo),S_fy(:,:,jjCirculo),S_fz(:,:,jjCirculo)] = ...
        S_3D_r_small(coordf,jjCirculo,xr,yr,zr,ksi,kpi,[1 1 1],gaussian);
    end
  end
end

% %% testme
%   figure(2654321);hold on;
%   iinc=1;
%   sca=max(max(abs(phi_fv)))*0.002;
%   pxO=phi_fv(jj+0*nbeq,iinc).';
%   pyO=phi_fv(jj+1*nbeq,iinc).';
%   pzO=phi_fv(jj+2*nbeq,iinc).';
%   
%   px = real(pxO); py=real(pyO); pz=real(pzO);
%   mamama = max([px py pz]);
%   px = px/mamama*sca;
%   py = py/mamama*sca;
%   pz = pz/mamama*sca;
%   
%   pxI = imag(pxO); pyI=imag(pyO); pzI=imag(pzO);
%   mamamaI = max([pxI pyI pzI]);
%   pxI = pxI/mamamaI*sca;
%   pyI = pyI/mamamaI*sca;
%   pzI = pzI/mamamaI*sca;
%   disp([m mamama + 1i*mamamaI])
%   
% %   figure;hold on;set(gca,'Zdir','reverse'); 
% %   axis equal;view(3);grid on; set(gca,'Projection','perspective')
%   title(['medio ' num2str(m)])
%   x = para.coord.x;
%   y = para.coord.y;
%   z = para.coord.z;
%   ii = para.coord.Xim(:,m) ~= 0;
%   quiver3(x(ii),y(ii),z(ii),px,py,pz,'r')
%   quiver3(x(ii),y(ii),z(ii),pxI,pyI,pzI,'b')
% 


%%
for iinc=1:ninc
  k = 1;
  if sal.Ut==1
    if sal.Ux==1
      Udiff(k,iinc)=sum(dAj.'.*(...
        squeeze(Gij(1,1,:)).*phi_fv(jj       ,iinc)+...
        squeeze(Gij(1,2,:)).*phi_fv(jj+  nbeq,iinc)+...
        squeeze(Gij(1,3,:)).*phi_fv(jj+2*nbeq,iinc)));
      
      k=k+1;
    end
    if sal.Uy==1
      Udiff(k,iinc)=sum(dAj.'.*(...
        squeeze(Gij(2,1,:)).*phi_fv(jj       ,iinc)+...
        squeeze(Gij(2,2,:)).*phi_fv(jj+  nbeq,iinc)+...
        squeeze(Gij(2,3,:)).*phi_fv(jj+2*nbeq,iinc)));
      k=k+1;
    end
    if sal.Uz==1
      Udiff(k,iinc)=sum(dAj.'.*(...
        squeeze(Gij(3,1,:)).*phi_fv(jj       ,iinc)+...
        squeeze(Gij(3,2,:)).*phi_fv(jj+  nbeq,iinc)+...
        squeeze(Gij(3,3,:)).*phi_fv(jj+2*nbeq,iinc)));
    end
  end
  k = 1;
  if sal.sxx==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(1,1,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(1,1,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(1,1,:)).*phi_fv(jj+2*nbeq,iinc)));
    k=k+1;
  end
  if sal.syy==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(2,2,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(2,2,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(2,2,:)).*phi_fv(jj+2*nbeq,iinc)));
    k=k+1;
  end
  if sal.szz==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(3,3,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(3,3,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(3,3,:)).*phi_fv(jj+2*nbeq,iinc)));
    k=k+1;
  end
  if sal.sxy==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(1,2,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(1,2,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(1,2,:)).*phi_fv(jj+2*nbeq,iinc)));
    k=k+1;
  end
  if sal.sxz==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(1,3,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(1,3,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(1,3,:)).*phi_fv(jj+2*nbeq,iinc)));
    k=k+1;
  end
  if sal.syz==1
    Sdiff(k,iinc)=sum(dAj.'.*(...
      squeeze(S_fx(2,3,:)).*phi_fv(jj       ,iinc)+...
      squeeze(S_fy(2,3,:)).*phi_fv(jj+  nbeq,iinc)+...
      squeeze(S_fz(2,3,:)).*phi_fv(jj+2*nbeq,iinc)));
  end
end