function [phi,DWN,para] = loadPHI_calcG(paratmpIN,DWN,j)
% loadPHI_calcG : cargar PHI resuelto en otra corrida, calcular Gij en
% receptores definidos en paratmpIN (en medios estratificados)

%% cargar PHI
phi=[]; para=[];
if j < 10
  name = ['../out/loadme/PHI','_00',num2str(j),'tmp.mat'];
else
  if j < 100
    name = ['../out/loadme/PHI','_0',num2str(j),'tmp.mat'];
  else
    name = ['../out/loadme/PHI','_',num2str(j),'tmp.mat'];
  end
end
load(name,'phi','para','pT');  
fprintf('%s',['loaded ', name,' | calculando funciones de Green...'])

if sum(para.tipoMed == 2)>0 %Por lo menos un medio es estratificado
  % Actualizar la lista de receptores con los que se indican en paratmpIN
  para.rec = paratmpIN.rec;
  
  DWN = DWNprepararMatriceseIndices(para,DWN); fprintf('.');
  [~,DWN]	= vector_fuente3D2(para,DWN); fprintf(':'); % y campo incidente
  
  % Ahora resolver las funciones de Green entre puntos de colocaciOn y
  % receptores:  Udiff (Sdiff falta)
  [DWN] = makeUdifSdifDisp(para,pT,DWN); fprintf('|\n') 
end
fprintf('%s','done')
% save('DWNtmp.mat','phi','DWNtmp','para')
end