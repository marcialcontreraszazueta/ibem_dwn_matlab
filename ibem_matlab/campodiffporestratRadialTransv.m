function [U] = campodiffporestratRadialTransv(tipo,...
  r,J0,J1,dJ1,d2J1,dJ0,d2J0,...
  ksi,kpi,fac,DK)
% campodiffporestratRadialTransv. campodiffporestrat2 pero sin ct, st 
% if r < 1e-6
%   J0 = 1;
%   dJ0 = 0;
%   d2J0 = -0.5;
%   J1 = 0;
%   dJ1 = 0.5;
%   d2J1 = 0;
%   error('falta !!!')
% end

if tipo == 1 % Desplazamientos
%   if r < 1e-6
%     U.Urx	= 0;
%     U.Utx = 0;
%     U.Uzx = 0;
%     U.Urz	= 0;
%     U.Uzz = 0;
%     return
%   end
%   MecElem = complex(zeros(3,3)); % Primer �ndice [fx,fy,fz], segundo el componente
  % fx %
  Urx = dJ1 .*fac(1,:,1)+J1/r.*fac(2,:,1); % dJ1 .*facUPSV+J1/r.*facUSH;
  Utx =-J1/r.*fac(1,:,1)-dJ1 .*fac(2,:,1); %-J1/r.*facUPSV-dJ1 .*facUSH;
  Uzx =   J1.*fac(3,:,1);                % J1.*facUz;
  
  Urx(isnan(Urx))=0;
  Utx(isnan(Utx))=0;
  Uzx(isnan(Uzx))=0;
  
  U.Urx	= DK*sum(Urx); %sum(DK.*Urx); % DK cte
  U.Utx = DK*sum(Utx); %sum(DK.*Utx);
  U.Uzx = DK*sum(Uzx); %sum(DK.*Uzx);
  
  % fz %
  Urz = dJ0.*fac(4,:,1); %farUrz
  Uzz = J0 .*fac(5,:,1); %facUzz
  
  Urz(isnan(Urz))=0;
  Uzz(isnan(Uzz))=0;
  
  U.Urz	= DK*sum(Urz);
  U.Uzz = DK*sum(Uzz);
end
if tipo == -1 % Esfuerzos
%   if r < 1e-6
%     U.Szrx = 0;
%     U.Sztx = 0;
%     U.Szzx = 0;
%     U.Srrx = 0;
%     U.Sttx = 0;
%     U.Srtx = 0;
%     U.Szrz = 0;
%     U.Sztz = 0;
%     U.Szzz = 0;
%     U.Srrz = 0;
%     U.Sttz = 0;
%     U.Srtz = 0;
%     return
%   end
  % fx *
  Szrx = dJ1 .*fac(1,:,1) + J1/r.*fac(2,:,1);
  Sztx = J1/r.*fac(1,:,1) + dJ1 .*fac(2,:,1);
  Szzx = J1  .*fac(3,:,1);
  Srrx = 2*((d2J1-(ksi^2/2-kpi^2)*J1)         .*fac(4,:,1) + d2J1          .*fac(5,:,1) + (dJ1/r-J1/r^2).*fac(6,:,1));
  Sttx = 2*( (-(ksi^2/2-kpi^2+1/r^2)*J1+dJ1/r).*fac(4,:,1) + (dJ1/r-J1/r^2).*fac(5,:,1) + (J1/r^2-dJ1/r).*fac(6,:,1));
  Srtx = 2*(dJ1/r-J1/r^2).*(fac(4,:,1)+fac(5,:,1))                               + (d2J1-dJ1/r+J1/(r^2)).*fac(6,:,1);
  
  Szrx(isnan(Szrx))=0;
  Sztx(isnan(Sztx))=0;
  Szzx(isnan(Szzx))=0;
  Srrx(isnan(Srrx))=0;
  Sttx(isnan(Sttx))=0;
  Srtx(isnan(Srtx))=0;
  
  U.Szrx = DK*sum(Szrx);
  U.Sztx = DK*sum(Sztx);
  U.Szzx = DK*sum(Szzx);
  U.Srrx = DK*sum(Srrx);
  U.Sttx = DK*sum(Sttx);
  U.Srtx = DK*sum(Srtx);
  
  % fz %
  Szzz = J0 .*fac(8,:,1); %facSzz
  Szrz = dJ0.*fac(7,:,1); %facSzr
  Sztz = 0*Szrz;
  Srtz = 0*Szrz;
  Srrz = 2*( (d2J0-(ksi^2/2-kpi^2)*J0)  .*fac(9,:,1) + d2J0   .*fac(10,:,1)); %facSrrP,facSrrSV
  Sttz = 2*( (-(ksi^2/2-kpi^2)*J0+dJ0/r).*fac(9,:,1) + (dJ0/r).*fac(10,:,1)); %facSrrP,facSrrSV
  
  Szrz(isnan(Szrz))=0;
  Sztz(isnan(Sztz))=0;
  Szzz(isnan(Szzz))=0;
  Srrz(isnan(Srrz))=0;
  Sttz(isnan(Sttz))=0;
  Srtz(isnan(Srtz))=0;
  
  U.Szrz = DK*sum(Szrz);
  U.Sztz = DK*sum(Sztz);
  U.Szzz = DK*sum(Szzz);
  U.Srrz = DK*sum(Srrz);
  U.Sttz = DK*sum(Sttz);
  U.Srtz = DK*sum(Srtz);
  
end
end