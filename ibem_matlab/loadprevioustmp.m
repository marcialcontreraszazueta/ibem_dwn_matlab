function [uw,next] = loadprevioustmp(j)
next = false; uw=zeros(1,1,1,1);
if j < 10
name = ['../out/tmp','_00',num2str(j),'tmp.mat'];  
else
  if j < 100
name = ['../out/tmp','_0',num2str(j),'tmp.mat'];    
  else
name = ['../out/tmp','_',num2str(j),'tmp.mat'];
  end
end
a = exist(name,'file');
if a==2
  load(name,'uw');
%   load(name,'sw');
  next = true;
end
end
