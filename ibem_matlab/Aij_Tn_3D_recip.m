function [A]=Aij_Tn_3D_recip(A,nbeq,coord,para)

% Coeficientes de A correspondientes a la continuidad de tracciones
%
% i : indice de (x) el punto de colocaciOn donde t_ij(x) = Int Tij(x,xi)
%save('burr.mat')
Arecip = A*0;

x 	=coord.x;
y 	=coord.y;
z 	=coord.z;
vn(1,:) = coord.vnx;
vn(2,:) = coord.vny;
vn(3,:) = coord.vnz;
dr  =coord.drxz;
dA	=coord.dA;
phi =coord.phi;

j       = 1:coord.nbpt; % indice de triAngulo
jphi    = 1:coord.nbeq; % indice de phi

%% vector de coords de puntos de colocacion
pc = cell(coord.nbeq,1);
ph = cell(coord.nbeq,1);
j       = 1:coord.nbpt;
for i=j
  pc{i}.x = coord.x(i);
  pc{i}.y = coord.y(i);
  pc{i}.z = coord.z(i);
  pc{i}.vnx = coord.vnx(i);
  pc{i}.vny = coord.vny(i);
  pc{i}.vnz = coord.vnz(i);
  im  = find(coord.Xim(i,:)~=0);
  pc{i}.m = im;
  ph{coord.phi(i,im(1))}.x = coord.x(i);
  ph{coord.phi(i,im(1))}.y = coord.y(i);
  ph{coord.phi(i,im(1))}.z = coord.z(i);
  ph{coord.phi(i,im(1))}.dA = coord.dA(i);
  ph{coord.phi(i,im(1))}.m = im(1);
  if length(im)==2
    ph{coord.phi(i,im(2))}.x = coord.x(i);
    ph{coord.phi(i,im(2))}.y = coord.y(i);
    ph{coord.phi(i,im(2))}.z = coord.z(i);
    ph{coord.phi(i,im(2))}.dA = coord.dA(i);
    ph{coord.phi(i,im(2))}.m = im(2);
  end
  if sum(coord.Xim(i,:)~=0)==2
    pc{coord.nbpt+coord.ju(i)}.x = coord.x(i);
    pc{coord.nbpt+coord.ju(i)}.y = coord.y(i);
    pc{coord.nbpt+coord.ju(i)}.z = coord.z(i);
    pc{coord.nbpt+coord.ju(i)}.vnx = coord.vnx(i);
    pc{coord.nbpt+coord.ju(i)}.vny = coord.vny(i);
    pc{coord.nbpt+coord.ju(i)}.vnz = coord.vnz(i);
    pc{coord.nbpt+coord.ju(i)}.m = im;
  end
end
%%
iC = 1;
macrocol = [];
macrocol{iC}.m = ph{1}.m;
macrocol{iC}.ci = 1;
for i=2:coord.nbeq
  if ph{i}.m ~= ph{i-1}.m
    % nueva marcrocolumna
    macrocol{iC}.cf = i-1;
    iC = iC +1;
    macrocol{iC}.m = ph{i}.m;
    macrocol{iC}.ci = i;
  end
end
macrocol{iC}.cf = coord.nbeq;
%%
blockT = [];
ib = 0; 
for mc = macrocol
nadaAntes = true;
%   disp(mc{1})
  for i=1:coord.nbpt
    if sum(pc{i}.m == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
        blockT{ib}.mc = mc{1};
        blockT{ib}.ri = i;
        blockT{ib}.rf = i;
      else
        blockT{ib}.rf = i;
      end
    else %en un bloque vacIo
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end
%%
blockG = [];
ib = 0; 
for mc = macrocol
nadaAntes = true;
%   disp(mc{1})
  for i=1+coord.nbpt:coord.nbeq
    if sum(pc{i}.m == mc{1}.m)>=1 %en un bloque no vacIo
      if nadaAntes %el primero de un bloque
        ib = ib+1;
        nadaAntes = false;
        blockG{ib}.mc = mc{1};
        blockG{ib}.ri = i;
        blockG{ib}.rf = i;
      else
        blockG{ib}.rf = i;
      end
    else %en un bloque vacIo
      if nadaAntes
        %no ha encontrada nada aUn
      else
        %ya habIa por lo menos un bloque antes
        nadaAntes = true;
      end
    end
  end
end
%%
for i=j % cada triAngulo como la fuente virtual
  
  tij     = zeros(3,3,coord.nbeq); %tensor de tracciones
  tijR    = zeros(3,3,coord.nbeq); %tensor de tracciones recIprocas
  Gij     = zeros(3,3,coord.nbeq); %tensor de desplazamientos
  % En Xim cada columna es un medio y cada renglOn un triAngulo.
  % Cada triAngulo puede estar asociado hasta con dos medios.
  % im es el Indice de la(s) columnas que no son zero, es decir
  % el indice del o de los medios asociados a la fuente virtual.
  im  = find(coord.Xim(i,:)~=0);
  
  %min de los indice de los medios en contacto que se lleva el signo +
  im0 = min(im); % (multi-ibem)
  
  for m=im; %para cada medio a cada lado de la fuente.
    if para.tipoMed(m) == 2% Si m es un medio estratificado (dwn)
      continue        % nos lo saltamos, esto se resuelve despuEs.
    end
    %     if isfield(para,'subm1') % material de adentro de la superficie m
    %       mmat= para.subm1(m); % subm1 permite encontrar m incluso si se divide
    %     else                   %
    mmat = m;            %  ?
    %     end
    Ci  = para.reg(mmat).Ci;
    ksi = para.reg(mmat).ksi;
    kpi = para.reg(mmat).kpi;
    %% 1) Indices
    %indices de los puntos de colocacion perteneciendo a m
    jlt     = coord.indm(m).ind;  % logical respecto a triAngulos
    jnt     = j(jlt);             % natural respecto a triAngulos
    jll     = true(length(jnt),1);% logical local para drj dA rij g
    %     jnl     = 1:length(jll);      % natural local para
    offlt   = find(jlt,1)-1;      % cantidad de triAngulos antes de los del bloque local
    jlA     = false(coord.nbeq,1);% logical respecto a la matriz A
    jlA(phi(j(jlt),m)) = true(1);
    jnA     = jphi(jlA);          % natural local respecto a la matriz A
    njj     = length(jnA);        % cantidad local de triAngulos
    
    %% 2) Propiedades geomEtricas de los triAngulos (en indice local)
    drj     = dr(jlt); % radio de un cIrculo con Area dA
    dAj     = dA(jlt); % Area del triAngulo
    xij     = x(i)-x(jlt);
    yij     = y(i)-y(jlt);
    zij     = z(i)-z(jlt);
    rij     = sqrt(xij.^2+yij.^2+zij.^2);
    g       = zeros(3,njj);
    g(1,:)  = xij./rij;
    g(2,:)  = yij./rij;
    g(3,:)  = zij./rij;
    
    
    %% 3) cherrypicking indices
    % 3.1) Los que requerirAn integraciOn gaussiana (en locales):
    if para.dim == 4 % 3Dgeneral
      jnlgau  = find((rij<=2*dr(i)).*(rij~=0)); % a menos de 2 radios
    else %3D axisimEtrico
      jnlgau  = find((rij<para.npplo/2*drj).*(rij~=0));
    end
    %) 3.1.1) No considerar los de campo cercano, que ya estAn hechos
    jnlgau((jnlgau+offlt) <= i) = []; %(a la izquierda de la diagonal)
    
    % 3.2) No incluir el que sabemos que es +-1/2:
    jlt(rij==0+offlt) = false;
    
    % 3.3) Tampoco lo que ya se calculO con teorema de reciprocidad:
    if i>1; jlt(1:i-1) = false; end
    
    % 3.4) No hacer los que requerirAn integraciOn gaussiana:
    if ~isempty(jnlgau); jlt(jnlgau+offlt) = false; end
    
    % 3.5) Actualizar el Indice logical local
    jll = logical(jll .* jlt(offlt+1:offlt+njj));
    
    % 3.6) Actualizar los Indices locales respecto a la matriz A:
    jlAnotthese = phi(j(~jlt),m); jlAnotthese(jlAnotthese==0)=[]; %disp(length(jlAnotthese))
    if ~isempty(jlAnotthese);
      jlA(jlAnotthese) = false(1);
      jnA     = jphi(jlA);          % natural local respecto a la matriz A
      %       njj     = length(jnA);        % cantidad local de triAngulos
    else
      warning('length(notthese)==0')
    end
    % 3.7) Hacer indice de jnlgau a phi
    jlAgau = false(coord.nbeq,1);
    jlAgau(phi(j(jnlgau+offlt),m)) = true(1);
    
    % Uso el teorema de reciprocidad de las funciones de Green:
    %    Aprovechar los resultados para el renglOn i, columnas jjphi
    %    en los renglones jjphiR, columna i:
    
    % Tracciones y tracciones recIprocas:
    [tij(:,:,jlA),tijR(:,:,jlA)]=Tij_3D(ksi,kpi,rij(jll),g(:,jll),Ci,vn,i,jlt);
    for i0=1:3 % multiplicar por el Area de las fuentes
      for j0=1:3
        tij (i0,j0,jlA) = squeeze(tij (i0,j0,jlA)).*dAj(jll).';
        tijR(i0,j0,jlA) = squeeze(tijR(i0,j0,jlA)) *dA(i);
      end
    end
    %% 4) Desplazamientos (sin recIprocos) (sin signo) (sin dA)
    if sum(coord.Xim(i,:)~=0)==2 % hay algo a ambos lados del punto de colocaciOn
      % 4.1) Indices de los calculados:
      col0 = phi(find(phi(:,m),1),m);
      coli = col0;
      colf = phi(find(phi(:,m),1,'last'),m);
      pivoteGR = find(phi(:,im(2)),1); %
      jllG = true(length(jnt),1);
      if m==im(1)
        colf = i; % para no hacer los que pueden salir de reciprocos
        
        jllG(colf+1:end) = false;
      else
        coli = phi(i,m);% para no hacer los que pueden salir de reciprocos
        
        jllG(1:coli-col0) = false;
      end
      jnAG = coli:colf;
      
      % 4.2) Menos los de campo cercano 
      % indices de los que requeriran integracion  gaussiana (en locales)
      if para.dim == 4 % 3Dgeneral
      jnlgauG  = find((rij(jllG)<=2*dr(i))); % a menos de 2 radios
      else %3D axisimEtrico
      jnlgauG  = find((rij(jllG)<para.npplo/2*drj));
      end
      % quitarlos de jllG y de jnAG
      jllG(jnlgauG+coli-col0) = false;
      jnAG(jnlgauG) = [];
      
      % 4.3) Calcular
      % 4.3.1) Los de campo lejano
      Gij(:,:,jnAG) = Gij_3D(ksi,kpi,rij(jllG),g(:,jllG),Ci,sum(jllG));
      % 4.3.2) Los de campo cercano
      if para.dim == 4
        Gij(:,:,jnlgauG) = Gij_3D_r_smallGEN(coord,i,jnt(jnlgauG),ksi,kpi,para,Ci,false);
      else
        Gij(:,:,jnlgauG) = Gij_3D_r_small(coord,x(i),y(i),z(i),jnt(jnlgauG),ksi,kpi,gaussian,Ci);
      end
    end
    
    %integracion gaussiana donde se requiere    ,--- receptor y
    if para.dim == 4 % 3Dgeneral                |  ,--- fuentes (en indice de triAng)
      tij(:,:,jlAgau) = Tij_3D_r_smallGEN(coord,i,jnt(jnlgau),ksi,kpi,para,Ci);
      for i0=1:3
        for j0=1:3
          tij(i0,j0,jlAgau) = squeeze(tij(i0,j0,jlAgau)).*dAj(jnlgau).';
        end
      end
      hh = 1; rel=jnt(jnlgau);            %          ,--- receptores
      for h = jphi(jlAgau) % los recIprocos          |      ,--- fuente
        tijR(:,:,jphi(h)) = Tij_3D_r_smallGEN(coord,rel(hh),i,ksi,kpi,para,Ci);
        for i0=1:3
          for j0=1:3
            tijR(i0,j0,jphi(h)) = squeeze(tijR(i0,j0,jphi(h)))*dA(i);
          end
        end
        hh=hh+1;
      end
    else %3D axisimEtrico
      % quien sabe si estA bien
      tij(:,:,jnA(jnlgau)) = Tij_3D_r_small(coord,i,jnt(jnlgau),ksi,kpi,para.gaussian,Ci);
      for i0=1:3
        for j0=1:3
          tij(i0,j0,jnA) = squeeze(tij(i0,j0,jnA)).*dAj.';
        end
      end
    end
    
    
    % cuando rij=0
    % en este punto, hay discontinuidad de las tractiones ti-ti'=phi
    % el signo1 es igual a +1 (resp. -1) si la normal apunta hacia el exterior
    % (resp. a interior) del medio a que pertenece el phi del punto de
    % colocacion en la posicion singular rij=0
    % como la convencion es que las normales esten siempre dirigidas hacia
    % abajo, hay que conocer si el phi pertenece a un contorno arriba o
    % abajo. Esta informacion esta en Xim
    signo1= (coord.Xim(i,m)==1) - (coord.Xim(i,m)==2);
    
    jlAr0 = false(coord.nbeq,1);
    jlAr0(phi(j(find(rij==0)+offlt),m)) = true(1);
    
    tij(:,:,jlAr0)  = signo1*0.5*eye(3);
    
    % en un punto de colocacion, hay siempre 2 medios en contacto,
    % aunque a veces este medio es el vacio
    % las ecuaciones de continuidad de traccion o de desplazamiento
    % involucran siempre 2 medios  : sigma(m)=sigma(m1) o u(m)=u(m1)
    % y como cada campo corresponde a (por ejemplo) u=u_diff+u_inc
    % se reorganiza entonces como :
    % sigma_diff(m)-sigma_diff(m1)=u_inc(m1)-u_inc(m)
    % los signos se fijan deacuerdo con el indice del medio,
    % + si el indice es el mas pequeno de los medios en contacto
    % - en lo contrario
    jlA     = false(coord.nbeq,1);% logical respecto a la matriz A
    jlA(phi(j(coord.indm(m).ind),m)) = true(1);
    jlA(1:i-1) = false(1);
    tij(:,:,jlA) = ((m==im0) - (m~=im0))*tij(:,:,jlA);
    tijR(:,:,jlA) = ((m==im0) - (m~=im0))*tijR(:,:,jlA);
  end
  % Con las funciones de Green calculadas directamente,
  % rellenar renglOn de la matriz:
  
  % Tracciones:
  jlA     = false(coord.nbeq,1);% logical respecto a la matriz A
  for m=im
    jlA(phi(j(coord.indm(m).ind),m)) = true(1);
  end
  jlA(1:i-1) = false(1);
  jl0 = false(coord.nbeq,1);
  colx = [jlA;jl0;jl0];
  coly = [jl0;jlA;jl0];
  colz = [jl0;jl0;jlA];
  
  A(i,colx)=squeeze(tij(1,1,jlA));%t_x=t_x1.Phi_j
  A(i,coly)=squeeze(tij(1,2,jlA));%t_x=t_x2.Phi_j
  A(i,colz)=squeeze(tij(1,3,jlA));%t_x=t_x3.Phi_j
  
  A(i+nbeq,colx)=squeeze(tij(2,1,jlA));%t_y=t_y1.Phi_j
  A(i+nbeq,coly)=squeeze(tij(2,2,jlA));%t_y=t_y2.Phi_j
  A(i+nbeq,colz)=squeeze(tij(2,3,jlA));%t_y=t_y3.Phi_j
  
  A(i+2*nbeq,colx)=squeeze(tij(3,1,jlA));%t_z=t_z1.Phi_j
  A(i+2*nbeq,coly)=squeeze(tij(3,2,jlA));%t_z=t_z2.Phi_j
  A(i+2*nbeq,colz)=squeeze(tij(3,3,jlA));%t_z=t_z3.Phi_j
  
  % Desplazamientos:
  if sum(coord.Xim(i,:)~=0)==2 % hay algo a ambos lados del punto de colocaciOn
    jlA = true(coord.nbeq,1);
    colx = [jlA;jl0;jl0];
    coly = [jl0;jlA;jl0];
    colz = [jl0;jl0;jlA];
    A(coord.nbpt+(i-pivoteGR+1),colx) = squeeze(Gij(1,1,jlA));%continuidad ux1
    A(coord.nbpt+(i-pivoteGR+1),coly) = squeeze(Gij(1,2,jlA));%continuidad ux2
    A(coord.nbpt+(i-pivoteGR+1),colz) = squeeze(Gij(1,3,jlA));%continuidad ux3
    
    A(coord.nbpt+(i-pivoteGR+1)+nbeq,colx) = squeeze(Gij(2,1,jlA));%continuidad uy1
    A(coord.nbpt+(i-pivoteGR+1)+nbeq,coly) = squeeze(Gij(2,2,jlA));%continuidad uy2
    A(coord.nbpt+(i-pivoteGR+1)+nbeq,colz) = squeeze(Gij(2,3,jlA));%continuidad uy3
    
    A(coord.nbpt+(i-pivoteGR+1)+nbeq*2,colx) = squeeze(Gij(3,1,jlA));%continuidad uz1
    A(coord.nbpt+(i-pivoteGR+1)+nbeq*2,coly) = squeeze(Gij(3,2,jlA));%continuidad uz2
    A(coord.nbpt+(i-pivoteGR+1)+nbeq*2,colz) = squeeze(Gij(3,3,jlA));%continuidad uz3
  end
  
  
  % Con las funciones de Green recIprocas,
  % rellenar columnas de la matriz:
  % Tracciones:
  for m = im
    % las coordenadas pivote de este bloque en la matriz A:
    pivoteR = find(phi(:,m),1);
    pivoteC = phi(pivoteR,m);
    
    jnA = phi(:,m); jnA(jnA==0)=[];
    if ~isempty(jnA)
      c0 = pivoteC + (i-pivoteR);
      r0 = pivoteR + (i-pivoteR);
      
      jlA = false(coord.nbeq,1);
      jlA(jnA)=true(1);
      jlA(1:c0) = false(1);
      
      jlAph = false(coord.nbeq,1);
      jlAph(r0+1:r0+sum(jlA)) = true(1);
      
      renx = [jlAph;jl0;jl0];
      reny = [jl0;jlAph;jl0];
      renz = [jl0;jl0;jlAph];
      colx = c0;
      coly = c0+nbeq;
      colz = c0+nbeq*2;
      
      Arecip(renx,colx) = squeeze(tijR(1,1,jlA));
      Arecip(renx,coly) = squeeze(tijR(1,2,jlA));
      Arecip(renx,colz) = squeeze(tijR(1,3,jlA));
      
      Arecip(reny,colx) = squeeze(tijR(2,1,jlA));
      Arecip(reny,coly) = squeeze(tijR(2,2,jlA));
      Arecip(reny,colz) = squeeze(tijR(2,3,jlA));
      
      Arecip(renz,colx) = squeeze(tijR(3,1,jlA));
      Arecip(renz,coly) = squeeze(tijR(3,2,jlA));
      Arecip(renz,colz) = squeeze(tijR(3,3,jlA));
    end
  end
  %   if i==550 || i==632 || i==774
  %     disp(i);figure(193+i);clf;spy(A)
  %   end
end
 % RecIprocos de los desplazamientos 

for i=j
  if sum(coord.Xim(i,:)~=0)==2 % hay algo a ambos lados del punto de colocaciOn
    
  im  = find(coord.Xim(i,:)~=0);
  pivoteGR = find(phi(:,im(2)),1);
  for m = im
      % 4.4) RecIprocos
      % 4.4.1) Indices para los recIprocos:
      doit = false;
      if m==im(1)
        if pivoteGR ~= i
        % los recIprocos son por arriba del eje i
        % se toman de:
        renRfrom = coord.nbpt+(i-pivoteGR+1);%i;
        colRfrom = phi(pivoteGR,m):(phi(i,m)-1);
        % y se pega en:
        colR = phi(i,m);%i;
        renRi = coord.nbpt+(i-pivoteGR+1)-1;%i-1;%phi(i-1,m);
        renRf = coord.nbpt+(phi(pivoteGR,m)-pivoteGR+1);%phi(pivoteGR,m);
        step = -1;
        renR = renRi:step:renRf;
        doit =true;
        end
      else
        ultimoi=min(find(phi(:,im(1)),1,'last'),...
                    find(phi(:,im(2)),1,'last'));
        ultimophi= max(phi(ultimoi,im(1)),...
                       phi(ultimoi,im(2)));
        
        if ultimoi ~= i
        % los recIprocos son por abajo del eje i
        p = phi(i,im); p(p==0)=[];
        nbpContthisi = p(2)-p(1);
        
         % tomados de:
        renRfrom = coord.nbpt+(i-pivoteGR+1);%i;
        colRfrom = phi(i,m)+1:ultimophi; %i+salto:ultimoR+salto;
        % y se pega en:
        renRi = renRfrom+1;%i+1;%phi(i,im(1))+1;
        renRf = coord.nbpt+nbpContthisi;
        step = 1;
        renR = renRi:step:renRf;
        colR = phi(i,m);%i;
        doit =true;
        end
      end
      if doit
      % 4.4.2) Rellenar A con los recIprocos (campo lejano y cercano(mal))
      renx = renR;
      reny = renR+nbeq;
      renz = renR+nbeq*2;
      colx = colR;
      coly = colR+nbeq;
      colz = colR+nbeq*2;
      
      renxfrom = renRfrom;
      renyfrom = renRfrom+nbeq;
      renzfrom = renRfrom+nbeq*2;
      colxfrom = colRfrom;
      colyfrom = colRfrom+nbeq;
      colzfrom = colRfrom+nbeq*2;
      
%       figure(2);clf;spy(Arecip,'r'); hold on
      
      Arecip(renx,colx) = A(renxfrom,colxfrom).'; %ux1 from ux1
      Arecip(renx,coly) = A(renyfrom,colxfrom).'; %ux2 from uy1
      Arecip(renx,colz) = A(renzfrom,colxfrom).'; %ux3 from uz1
      
      Arecip(reny,colx) = A(renxfrom,colyfrom).'; %uy1 from ux2
      Arecip(reny,coly) = A(renyfrom,colyfrom).'; %uy2 from uy2
      Arecip(reny,colz) = A(renzfrom,colyfrom).'; %uy3 from uz2
      
      Arecip(renz,colx) = A(renxfrom,colzfrom).'; %uz1 from ux3
      Arecip(renz,coly) = A(renyfrom,colzfrom).'; %uz2 from uy3
      Arecip(renz,colz) = A(renzfrom,colzfrom).'; %uz3 from uz3
      
      % de los que se han copiado, algunos requieren integraciOn gaussiana
      
      % 4.4.3) Los RecIprocos de campo cercano 
      if m == im(1)
        fuentes = renR; 
        recepto = colR;
      else
        fuentes = colR;
        recepto = renR;
      end
      for ifu = fuentes %(columna)
        % coordenada de la fuente
        itriangFuent = j(phi(:,m)==ifu);
        for ir = recepto %(renglon)
          itriangRecep = i;
        end
      end
      
    jlt     = false(coord.nbpt,1);
    for tt=colRfrom
    jlt(phi(:,m)==tt)=true;
    end
    jnt     = j(jlt);
    drj     = dr(jlt);
    xij     = x(i)-x(jlt);
    yij     = y(i)-y(jlt);
    zij     = z(i)-z(jlt);
    rij     = sqrt(xij.^2+yij.^2+zij.^2);
    
      if para.dim == 4 % 3Dgeneral
      jnlgauG  = find((rij<=2*dr(i))); % a menos de 2 radios
      else %3D axisimEtrico
      jnlgauG  = find((rij<para.npplo/2*drj));
      end
      if m == im(1)
        %un receptor, varias fuentes
        xr = i;
        jjpb = jnt(jnlgauG);
      Gres=Gij_3D_r_smallGEN(coord,xr,jjpb,ksi,kpi,para,Ci,false);
      
      
      else
        %varios receptores, una fuente
        jjpb = i; %la fuente (columnas colx coly colz) 
        for xr = jnt(jnlgauG) %para cada receptor (renglon)
        Gres=Gij_3D_r_smallGEN(coord,xr,jjpb,ksi,kpi,para,Ci,false);
          %Acomodar en la matrix A
          
        end
      end
      
      end
  end 
      
     % 5) Corregir directos y recIprocos de G en la matriz A
     % 5.1) Signo segUn el indice del medio
     % 5.2) multiplicar calculados y recIprocos por dA
  end
end
% sumar calculados y recIprocos:

figure(1);clf;spy(A,'b'); hold on
% figure(2);clf;
spy(Arecip,'r'); hold on
A = A + Arecip;
end