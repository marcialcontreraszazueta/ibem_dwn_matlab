function [ batch ] = batch_run(casoIni)
%RUN all cases inside batch.mat variable {:,1} and save results {:,2}
gcp % crear una alberca de procesos nueva O describir la existente
clear batch
batch = cell(1);
if exist(fullfile(cd, '../out/batch.mat'), 'file')
  load('../out/batch.mat','batch') %cargar variable
  cd ../out
  [pathstr1,pathstr2,~] = fileparts(pwd);
  cd ../ibem_matlab
  if iscell(batch)
    nCasos = size(batch,1);
    if nargin ~= 1
      casoIni = 1;
    end
    for iCaso = casoIni:nCasos
      disp('**********************************************')
      disp(['************** caso' num2str(iCaso) '/' num2str(nCasos) '************************'])
      disp('**********************************************')
      
      % ajustar variables dependientes del sistema
      [~,name,ext] = fileparts(batch{iCaso,1}.name);
      batch{iCaso,1}.name = [pathstr1,pathstr1(1),pathstr2,pathstr1(1),name,ext];
      batch{iCaso,1}.nametmp = batch{iCaso,1}.name;
      cd ../out
      batch{iCaso,1}.nomcarpeta = pwd;
      cd ../ibem_matlab
      batch{iCaso,1}.nomrep = [pathstr1,pathstr1(1),pathstr2];
      % correr programa
      % Los para. de cada caso est� en batch{:,1}
      batch{iCaso,1}.espyinv=1;
      
%       batch{iCaso,1}.gam = 0; warning('gam=0')
      
      [RESULT,para] = calculo(batch{iCaso,1});
      %batch{iCaso,1} = para;
      %batch{iCaso,2} = RESULT;
      txnm = ['RESULT_' num2str(iCaso) '.mat'];
      disp('****************** done **********************')
      disp(['saving results and parameters onto ' txnm])
      disp(para.nomrep)
      disp(RESULT)
      cd ..
      cd out
      RESULT = rmfield(RESULT,'utc');
      RESULT = rmfield(RESULT,'stc');
      save(txnm,'RESULT','para')
      cd ..
      cd ibem_matlab
      %clear OUTPUT
    end
  else
    disp('problem with variable batch ')
    whos batch
  end
else
  disp('batch.mat does not exist ../out/')
end
end