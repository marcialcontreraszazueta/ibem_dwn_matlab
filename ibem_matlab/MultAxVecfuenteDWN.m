function [SolutionPSV,SolutionSH] = MultAxVecfuenteDWN(ncapas,A_DWN, ...
                             B_DWN,FsourcePSV,FsourceSH,nkr)
% Multiplicar Fsource por la matriz del medio estratificado ya invertida
SolutionPSV = complex(zeros(4*(ncapas-1)+2,nkr,2)); % Ultimo Indice para Fx y Fz
SolutionSH  = complex(zeros(2*(ncapas-1)+1,nkr));
%%%%%%
% fx %
%%%%%% %Resolution du systeme des conditions aux limites
    for j=1:4*(ncapas-1)+2
        SolutionPSV(j,:,1)= sum(squeeze(A_DWN(j,:,:)).*FsourcePSV(:,:,1),1);
    end
    if ncapas==1
        j = 1;
        SolutionSH(:,:)	= (FsourceSH.*squeeze(B_DWN(j,:,:)).');
    else
        for j=1:2*(ncapas-1)+1
            SolutionSH(j,:)	= sum(squeeze(B_DWN(j,:,:)).*FsourceSH,1);
        end
    end

%%%%%%
% fz %
%%%%%%
    for j=1:4*(ncapas-1)+2
        SolutionPSV(j,:,2)   = sum(squeeze(A_DWN(j,:,:)).*FsourcePSV(:,:,2),1);
    end
end