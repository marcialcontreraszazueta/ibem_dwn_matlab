function long=long_contorno_m1(cont,geo)
% funcion que calcula la longitud total del background

rh     	= cont.rh;
ruggeo	= cont.ruggeo;
a       = cont.a;
rba 	= cont.rba;

if geo==3
    %semi espacio
    if ruggeo==1 %plano
        % dzdx=0;
        long = a;
    elseif ruggeo==2 %sinus
      % el siguiente codigo funciona muy bien en matlab:
%         dl =  @(x) sqrt((rh/2*2*pi/rba*cos(2*pi*x/rba)).^2+1);
%         long = integral(dl,0,a,'RelTol',1e-6);
      % para la version compilada no se pueden usar funciones anOnimas
      % ni 'integral' asi que se rectifica:
        x = linspace(0,rba,1/1e-3); %por un ciclo
        y = yval(x,rh/2,rba);
        long = rectificar(x,y) * a/rba;
    elseif ruggeo==3 %triangle
        % dzdx = 2*rh/rba;
        long = sqrt((2*rh/rba)^2+1)*a;
    end
else
    long = 0;
end
end

function y = yval(x,rh,rba)
y=rh.*sin(2*pi*x/rba); % sin la fase porque se cuentan ciclos completos
end

function long = rectificar(x,y)
long = 0;
for i = 2:length(x)
long = long + sqrt((x(i)-x(i-1))^2 + (y(i)-y(i-1))^2);
end
end