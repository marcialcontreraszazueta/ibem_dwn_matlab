function tNuevos = splitThisTriangle(sdD,t,splits)
% Partir un triAngulo end dos desde el lado mAs largo al vErtice opuesto o
% en cuatro desde los centros de cada vErtice.
%  sdD contiene el para.cont(m,1).piece{p}.subdibData
% .triangles [3x3xn]   n : el n�mero de tri�ngulos
%             | '--------- �ndice de v�rtice
%             '----------- �ndice de coordenada

% Encontrar lado m�s largo, su centro y el �ndice de v�rtice opuesto
l = zeros(3,6);
l(1,1:6) = lado(sdD.triangles(:,2,t),sdD.triangles(:,3,t),2,3);
l(2,1:6) = lado(sdD.triangles(:,1,t),sdD.triangles(:,3,t),1,3);
l(3,1:6) = lado(sdD.triangles(:,1,t),sdD.triangles(:,2,t),1,2);

if splits == 1 %dividir en dos
  lz = find(sdD.triangles(3,:,t) == max(sdD.triangles(3,:,t)));
  if length(lz) == 2
    ss = ones(1,3); ss(lz) = 0; lz = find(ss);
  end
  lM = find(l == max(l(:,1)),1);
  
  % la que produzca un lado menor:
  l1 = sqrt(sum((sdD.triangles(:,lM,t) - l(lM,2:4).').^2));
  l2 = sqrt(sum((sdD.triangles(:,lz,t) - l(lz,2:4).').^2));
  if 1.3*l1 >= l2
    lM = lz(1);
  end
%   disp('---')
% lM = find(l == max(l(:,1)),1); %indice del lado m�s grande

% primer tri�ngulo nuevo
tNuevos.triangles(:,1,1) = sdD.triangles(:,lM,t); % v�rtice opuesto
tNuevos.triangles(:,2,1) = sdD.triangles(:,l(lM,5),t); %primer v�rtice lado partido
tNuevos.triangles(:,3,1) = l(lM,2:4); %nuevo v�rtice (mitad de lado partido)

%segundo tri�ngulo neuvo
tNuevos.triangles(:,1,2) = sdD.triangles(:,lM,t); % v�rtice opuesto
tNuevos.triangles(:,2,2) = l(lM,2:4); %nuevo v�rtice (mitad de lado partido)
tNuevos.triangles(:,3,2) = sdD.triangles(:,l(lM,6),t); %segundo v�rtice anterior

% Las dem�s varialbes .centers .areas .N  ;

tNuevos.centers(:,1) = mean(tNuevos.triangles(:,:,1),2); %primer nuevo centro
tNuevos.centers(:,2) = mean(tNuevos.triangles(:,:,2),2); %segundo neuvo centro
tNuevos.centers(abs(tNuevos.centers)<1/1000) = 0;

tNuevos.areas(1) = HeronsArea(tNuevos.triangles(:,:,1)); %primer �rea
tNuevos.areas(2) = HeronsArea(tNuevos.triangles(:,:,2)); %sgunda �rea

tNuevos.N(1,:) = sdD.N(t,:); % la misma normal para ambos
tNuevos.N(2,:) = sdD.N(t,:); % la misma normal para ambos

tNuevos.cv(1,:) = sdD.cv(t); % 1 o 2 si es contorno de arriba o abajo
tNuevos.cv(2,:) = sdD.cv(t);

tNuevos.radios(1) = sqrt(tNuevos.areas(1)/pi);
tNuevos.radios(2) = sqrt(tNuevos.areas(2)/pi);
elseif splits == 1.5 %dividir en 3
  %primer tri�ngulo
  tNuevos.triangles(:,1,1) = sdD.triangles(:,1,t); % v1
  tNuevos.triangles(:,2,1) = sdD.triangles(:,2,t); % v2
  tNuevos.triangles(:,3,1) = sdD.centers(:,t);
  %segundo tri�ngulo
  tNuevos.triangles(:,1,2) = sdD.triangles(:,2,t); % v2
  tNuevos.triangles(:,2,2) = sdD.triangles(:,3,t); % v3
  tNuevos.triangles(:,3,2) = sdD.centers(:,t);
  %tercer tri�ngulo
  tNuevos.triangles(:,1,3) = sdD.triangles(:,3,t); % v3
  tNuevos.triangles(:,2,3) = sdD.triangles(:,1,t); % v1
  tNuevos.triangles(:,3,3) = sdD.centers(:,t);
  % Las dem�s varialbes .centers .areas .N  ;
  tNuevos.centers(:,1) = mean(tNuevos.triangles(:,:,1),2); %centros
  tNuevos.centers(:,2) = mean(tNuevos.triangles(:,:,2),2);
  tNuevos.centers(:,3) = mean(tNuevos.triangles(:,:,3),2);
  tNuevos.centers(abs(tNuevos.centers)<1/1000) = 0;
  
  tNuevos.areas(1) = HeronsArea(tNuevos.triangles(:,:,1)); %�reas
  tNuevos.areas(2) = HeronsArea(tNuevos.triangles(:,:,2));
  tNuevos.areas(3) = HeronsArea(tNuevos.triangles(:,:,3));
  
  tNuevos.N(1,:) = sdD.N(t,:); % la misma normal para todos
  tNuevos.N(2,:) = sdD.N(t,:);
  tNuevos.N(3,:) = sdD.N(t,:);
  
  tNuevos.cv(1,:) = sdD.cv(t); % 1 o 2 si es contorno de arriba o abajo
  tNuevos.cv(2,:) = sdD.cv(t);
  tNuevos.cv(3,:) = sdD.cv(t);
  
  tNuevos.radios(1) = sqrt(tNuevos.areas(1)/pi);
  tNuevos.radios(2) = sqrt(tNuevos.areas(2)/pi);
  tNuevos.radios(3) = sqrt(tNuevos.areas(3)/pi);
  
elseif splits == 2 %dividir en 4
  %primer tri�ngulo nuevo
  tNuevos.triangles(:,1,1) = sdD.triangles(:,1,t); % v1
  tNuevos.triangles(:,2,1) = l(3,2:4); %mitad de lado 3
  tNuevos.triangles(:,3,1) = l(2,2:4); %mitad de lado 2
  %segundo triangulo nuevo
  tNuevos.triangles(:,1,2) = l(3,2:4); %mitad de lado 3
  tNuevos.triangles(:,2,2) = sdD.triangles(:,2,t); % v2
  tNuevos.triangles(:,3,2) = l(1,2:4); %mitad de lado 1
  %tercer tri�ngulo nuevo
  tNuevos.triangles(:,1,3) = l(2,2:4); %mitad de lado 2
  tNuevos.triangles(:,2,3) = l(1,2:4); %mitad de lado 1
  tNuevos.triangles(:,3,3) = sdD.triangles(:,3,t); % v3
  %cuarto tri�ngulo nuevo
  tNuevos.triangles(:,1,4) = l(1,2:4); %mitad de lado 1
  tNuevos.triangles(:,2,4) = l(2,2:4); %mitad de lado 2
  tNuevos.triangles(:,3,4) = l(3,2:4); %mitad de lado 3
  
  % Las dem�s varialbes .centers .areas .N  ;
  tNuevos.centers(:,1) = mean(tNuevos.triangles(:,:,1),2); %centros
  tNuevos.centers(:,2) = mean(tNuevos.triangles(:,:,2),2);
  tNuevos.centers(:,3) = mean(tNuevos.triangles(:,:,3),2);
  tNuevos.centers(:,4) = mean(tNuevos.triangles(:,:,4),2);
  tNuevos.centers(abs(tNuevos.centers)<1/1000) = 0;
  
  tNuevos.areas(1) = HeronsArea(tNuevos.triangles(:,:,1)); %�reas
  tNuevos.areas(2) = HeronsArea(tNuevos.triangles(:,:,2));
  tNuevos.areas(3) = HeronsArea(tNuevos.triangles(:,:,3));
  tNuevos.areas(4) = HeronsArea(tNuevos.triangles(:,:,4));
  
  tNuevos.N(1,:) = sdD.N(t,:); % la misma normal para todos
  tNuevos.N(2,:) = sdD.N(t,:);
  tNuevos.N(3,:) = sdD.N(t,:);
  tNuevos.N(4,:) = sdD.N(t,:);
  
  tNuevos.cv(1,:) = sdD.cv(t); % 1 o 2 si es contorno de arriba o abajo
  tNuevos.cv(2,:) = sdD.cv(t);
  tNuevos.cv(3,:) = sdD.cv(t);
  tNuevos.cv(4,:) = sdD.cv(t);
  
  tNuevos.radios(1) = sqrt(tNuevos.areas(1)/pi);
  tNuevos.radios(2) = sqrt(tNuevos.areas(2)/pi);
  tNuevos.radios(3) = sqrt(tNuevos.areas(3)/pi);
  tNuevos.radios(4) = sqrt(tNuevos.areas(4)/pi);
end

tNuevos.minVel = sdD.minVel;

% %% Test me:
% figure(343); hold on; plot3(sdD.triangles(1,[1 2 3 1],t),...
%                        sdD.triangles(2,[1 2 3 1],t),...
%                        sdD.triangles(3,[1 2 3 1],t),'k-','LineWidth',0.5)
%                  plot3(tNuevos.triangles(1,[1 2 3 1],1),...
%                        tNuevos.triangles(2,[1 2 3 1],1),...
%                        tNuevos.triangles(3,[1 2 3 1],1),'b--','LineWidth',3)
%                  plot3(tNuevos.triangles(1,[1 2 3 1],2),...
%                        tNuevos.triangles(2,[1 2 3 1],2),...
%                        tNuevos.triangles(3,[1 2 3 1],2),'r--','LineWidth',3)
%                  plot3(tNuevos.centers(1,1),...
%                        tNuevos.centers(2,1),...
%                        tNuevos.centers(3,1),'b.')
%                  plot3(tNuevos.centers(1,2),...
%                        tNuevos.centers(2,2),...
%                        tNuevos.centers(3,2),'r.')
%  if splits == 2
%                  plot3(tNuevos.triangles(1,[1 2 3 1],3),...
%                        tNuevos.triangles(2,[1 2 3 1],3),...
%                        tNuevos.triangles(3,[1 2 3 1],3),'g--','LineWidth',3)
%                  plot3(tNuevos.triangles(1,[1 2 3 1],4),...
%                        tNuevos.triangles(2,[1 2 3 1],4),...
%                        tNuevos.triangles(3,[1 2 3 1],4),'c--','LineWidth',1.5)
%                  plot3(tNuevos.centers(1,3),...
%                        tNuevos.centers(2,3),...
%                        tNuevos.centers(3,3),'g.')
%                  plot3(tNuevos.centers(1,4),...
%                        tNuevos.centers(2,4),...
%                        tNuevos.centers(3,4),'c.')
%  end
%                  view([-44 64]); grid on
end

function out = lado(a,b,l1,l2)
out = zeros(1,6);
out(1) = ( (a(1)-b(1))^2 + ...
           (a(2)-b(2))^2 + ...
           (a(3)-b(3))^2 ); %tama�o de la cara al cuadrado
 % punto medio:
 out(2) = b(1) + (a(1)-b(1))/2;
 out(3) = b(2) + (a(2)-b(2))/2;
 out(4) = b(3) + (a(3)-b(3))/2;
 out(5) = l1;
 out(6) = l2;
end