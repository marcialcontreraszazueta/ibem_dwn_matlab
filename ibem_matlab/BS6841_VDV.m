function [VDV] = BS6841_VDV(utcz,dt)
% Dosis de vibracion
% utcz es la muestra de TT segundos, en m/s2
% dt en segundos

integral = sum(utcz.^4,1)*dt; % m4/s7
VDV = integral.^0.25; 
% VDV tiene unidades de m/s^1.75
end