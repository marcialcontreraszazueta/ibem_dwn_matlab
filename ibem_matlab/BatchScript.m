function [RESULT,para] = BatchScript(file,justview)
%   [RESULT,para] = BATCHSCRIPT Loads parameters from a 
%   MAT-file script and runs calculo.m 

if strcmp(file(1),'/')
  nombreCompleto = file;
else
  v = [fileparts(pwd),'/ins/',file];
  if iscell(v)
    nombreCompleto = v{1};
  else
    nombreCompleto = v;
  end
end
if exist(nombreCompleto,'file') == 2
  out = load(nombreCompleto,'para');
  para = out.para; clear out
  
  % dibujar configuracion geometrica
  if nargin==2
    if justview == true
      figure(1001);clf
      set(gcf,'Name','Configuracion geometrica','numberTitle','off');
      dibujo_conf_geo(para,gca)
      if para.dim > 2
        view([0 35])
      else
        view([0 90])
      end
      mostrarTablaDeMateriales(para);
      return
    end
  end
  
  % Ejecucion del analisis
  para.espyinv=1;
  tic
  [RESULT,para]=calculo(para);
  % los tensores estan en
  %    RESULT.sw(:,:,:,:)
  %              | | | '--- componente: sxx syy szz sxy sxz syz
  %              | | '----- fuente: [1:n dirx  1:n diry  1:n dirz]
  %              | '------- receptor: 1:nFault
  %              '--------- datos en frecuenica positiva 1:(para.nf/2+1)
  toc
  disp('done')
  
else
  disp('The file does not exist or it could not be reached.')
end
end