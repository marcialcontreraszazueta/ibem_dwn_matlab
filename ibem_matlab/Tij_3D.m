function [T,TR]=Tij_3D(ks,kp,rij,gam,~,vnx,vnXi)
% FunciOn de Green de tracciones en espacio completo homogEneo
% Se usa teorema de reciprocidad si se indica vnXi la normal en la fuente.
% Algebra de:  BSSA, Vol. 85, No. 1, pp. 269-284, 1995
% Seismic Response of Three-Dimensional Alluvial Valleys for Incident P, S, and Rayleigh Waves
% by Francisco J. Sanchez-Sesma and Francisco Luzon

n       = length(rij);
ba      = kp/ks;%beta/alpha
kpr     = kp*rij;
ksr     = ks*rij;
ksrm1   = 1./ksr;
g       = complex(zeros(3,n));

if n==1
val= ...
    (                      4-12i*ksrm1-12*ksrm1.^2).*exp(-1i*ksr)+...
    (-1i*ba*ksr-4*ba^2-1+ 12i*ba*ksrm1+12*ksrm1.^2).*exp(-1i*kpr);
g(1) = val(1,1);

val= ...
    (                             -2+6i*ksrm1+6*ksrm1.^2).*exp(-1i*ksr)+...
    ( 1i*(2*ba^3-ba)*ksr+4*ba^2-1-6i*ba*ksrm1-6*ksrm1.^2).*exp(-1i*kpr);
g(2) = val(1,1);

val= ...
    (-1i*ksr-3+ 6i*ksrm1+6*ksrm1.^2).*exp(-1i*ksr)+...
    (2*ba^2 -6i*ba*ksrm1-6*ksrm1.^2).*exp(-1i*kpr);
g(3) = val(1,1);
else
g(1,:)= ...
    (                      4-12i*ksrm1-12*ksrm1.^2).*exp(-1i*ksr)+...
    (-1i*ba*ksr-4*ba^2-1+ 12i*ba*ksrm1+12*ksrm1.^2).*exp(-1i*kpr);

g(2,:)= ...
    (                             -2+6i*ksrm1+6*ksrm1.^2).*exp(-1i*ksr)+...
    ( 1i*(2*ba^3-ba)*ksr+4*ba^2-1-6i*ba*ksrm1-6*ksrm1.^2).*exp(-1i*kpr);

g(3,:)= ...
    (-1i*ksr-3+ 6i*ksrm1+6*ksrm1.^2).*exp(-1i*ksr)+...
    (2*ba^2 -6i*ba*ksrm1-6*ksrm1.^2).*exp(-1i*kpr);
end

T   = complex(zeros(3,3,n));
d   = eye(3);
g0  = g(1,:)-g(2,:)-2*g(3,:);
fac = 1./(4*pi*rij.^2);

if nargin == 6
  vn = vnx;
  gknk= gam(1,:).*vn(1,:)+gam(2,:).*vn(2,:)+gam(3,:).*vn(3,:);
  % Tracciones: (cAlculo directo)
  for i=1:3
    for j=1:3
      T(i,j,:)=fac.*(g0.*gam(i,:).*gam(j,:).*gknk +...
        g(3,:).*(gam(i,:).*vn(j,:)+gknk.*d(i,j))+g(2,:).*gam(j,:).*vn(i,:));
    end
  end
  TR = 0; %sikj=0;
else
  % Esfuerzos: (el cAlculo se puede reusar con el teorema de reciprocidad
  TR  = complex(zeros(3,3,n));
  sikj = complex(zeros(3,3,3,n)); % tensor de esfuerzo, dada cada una de las fuentes
  for j = 1:3 % para cada direcci?n de la fuerza aplicada
    for i = 1:3
      for k = 1:3
        sikj(i,k,j,:) = fac.*(g0.*gam(i,:).*gam(j,:).*gam(k,:) + ...
          g(3,:).*(gam(i,:).*d(k,j)+gam(k,:).*d(i,j))+g(2,:).*gam(j,:).*d(k,i));
      end
      
    % Tracciones en el receptor (x) por todas las fuentes (xi)
    T(i,j,:) =  squeeze(sikj(i,1,j,:)).*(vnx(1,:).')...
               +squeeze(sikj(i,2,j,:)).*(vnx(2,:).')...
               +squeeze(sikj(i,3,j,:)).*(vnx(3,:).');
    
    % Tracciones en los receptores (xi) por la fuente en (x)
    TR(i,j,:) =  -squeeze(sikj(i,1,j,:)).*(vnXi(1,1).')...
                 -squeeze(sikj(i,2,j,:)).*(vnXi(2,1).')...
                 -squeeze(sikj(i,3,j,:)).*(vnXi(3,1).');
    end
  end
end
% disp(max(max(max(T/TR)))) 
end