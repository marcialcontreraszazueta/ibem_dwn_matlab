% cargar espectros a la memoria
load('resultEnFrec.mat','para','uw')

% para.pulso.a = 1/1.5;

nfrecs = size(uw,1);
Fq = 0:(nfrecs-1);  Fq=Fq.*0.06; % <- 
fmax = Fq(end);
% la funcion de amplitud es un Ricker: para.pulso.tipo = 3
% el periodo caracteristico es 1/para.pulso.a
disp(['Ricker T= ' num2str(1/para.pulso.a)])
%y el desfase en tiempo (centro de la ondicula)
disp(['ts = ' num2str(para.pulso.b)])

%% Hacer todos los sismogramas
disp('Inversion de los espectros');
nfN = nfrecs;
nf = (nfN-1)*2;  disp(['nf = ',num2str(nf)])
para.nf = nf;
para.fmax = fmax;
% [utc,~]   = inversion_w(uw,[],para);
[utc,~]   = inversion_w(uw,[],para,spectre);
disp('done')

df      = para.fmax/nfN;     disp(['df = ',num2str(df)])
Fq      = (0:nf/2)*df;       disp(['Fmx= ',num2str(Fq(end))])
dt = 1/(df*para.zeropad);    disp(['dt = ',num2str(dt)])
tps = (0:para.zeropad-1)*dt; disp(['tmx= ',num2str(tps(end))])
save('resultEnTiemp.mat','utc','tps')
disp('guardado')


%% graficar un receptor en particular
ir = 3;         % RECEPTORES 1:21 linea en x;  22:42 linea en y
iinc = 3;       % Tres INCIDENCIAS 1: fuerza puntual en dir X, 2:Fy  3:Fz
icomp = 1;      % %COMPONENTE del desplazamiento 1:x, 2:y, 3:z

%
figure;
set(gcf,'name','espectro')
subplot(2,1,1)
cla
plot(Fq,real(uw(:,ir,iinc,icomp)),'r');hold on;
plot(Fq,imag(uw(:,ir,iinc,icomp)),'b');
plot(Fq,abs(uw(:,ir,iinc,icomp)),'k')
ax1 = gca;
ax1_pos = ax1.Position;
ax1.Box = 'off';
xlabel('frecuencia en Hertz')
ylabel('Espectro sin convolucionar')
ax2=axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');
xlim(ax1.XLim)
nn = length(ax1.XTick);
ax2.XTickMode = 'auto';
ax2.XTickLabelMode = 'manual';
cl = cell(nn,1);
cl{1} = ' ';
for i=2:nn
    cl{i} = num2str(1/ax1.XTick(i),3);
end
ax2.XTickLabel = cl;
set(gca,'ytick',[])
title(['ir' num2str(ir) ' iinc' num2str(iinc) ' icomp' num2str(icomp)])
xlabel('Periodo en segundos')
subplot(2,1,2)
cla
plot(tps,real(utc(:,ir,iinc,icomp)),'r');hold on;
plot(tps,imag(utc(:,ir,iinc,icomp)),'b');
title('Sismograma sintético')
xlabel('tiempo en segundos')

%% sAbana
figure;
title ('U_y Fz')
iinc = 3;       % Tres INCIDENCIAS 1: fuerza puntual en dir X, 2:Fy  3:Fz
comp = 2;       % COMPONENTE del desplazamiento 1:x, 2:y, 3:z
hold on
% resrango = 1:21; % a lo largo de x
resrango = 22:42; % a lo largo de y
offset = max(max(real(utc(:,resrango,iinc,comp))))/1.5;
 
for ir = resrango
plot(tps,squeeze(real(utc(:,ir,iinc,comp)))+ offset*(ir-resrango(1)),'k-')
end
xlim([0 5])