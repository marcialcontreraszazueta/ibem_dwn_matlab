nsubmednew=str2double(get(bouton.nsubmed,'string'));
med     =get(bouton.med,'value');

if nsubmednew>para.reg(med).nsubmed
    %il faut initialiser des champs;
    for i=para.reg(med).nsubmed+1:nsubmednew
        para.reg(med).sub(i).rho      = 1;
        para.reg(med).sub(i).alpha    = 2;
        para.reg(med).sub(i).bet      = 1;
        para.reg(med).sub(i).qd       = 1000;
        para.reg(med).sub(i).tipoatts = 1;
        para.reg(med).sub(i).lambda   = para.reg(med).sub(i).rho*(para.reg(med).sub(i).alpha^2-2*para.reg(med).sub(i).bet^2);
        para.reg(med).sub(i).mu       = para.reg(med).sub(i).rho*para.reg(med).sub(i).bet^2;
        para.reg(med).sub(i).h        = 1;
    end
    para.reg(med).sub(i).h        = 0;%ultimo estrato=semi espacio
    para.reg(med).nsubmed = nsubmednew;
elseif nsubmednew<para.reg(med).nsubmed
    %il faut supprimer des champs;
    if nsubmednew==0
        nsubmednew=1;
        warndlg('No seas ...');
    end
    sub0            = para.reg(med).sub(1:nsubmednew);
    para.reg(med).sub = sub0;
    para.reg(med).sub(nsubmednew).h        = 0;%ultimo estrato=semi espacio
    para.reg(med).nsubmed = nsubmednew;
end

% para.nsubmed   = nsubmednew;
strsubmed      = 1:para.reg(med).nsubmed;
set(bouton.submed,'string',strsubmed,'value',1);
cmd_submed;