function [ThisPiece] = loadStageList(ThisPiece,nf,nom)
%loadStageList Load from a txt file with a list of stl files

% la lista es por ejemplo:
% Cuerno_5_A.stl 0 12
% Cuerno_7_A.stl 13 17
% Cuerno_11_A.stl 18 24
% Cuerno_13_A.stl 25 30
% Cuerno_15_A.stl 31 41
% Cuerno_17_A.stl 42 47
% Cuerno_19_A.stl 48 52
% Cuerno_21_A.stl 53 58
% Cuerno_23_A.stl 59 65
% Cuerno_25_A.stl 66 69
% Cuerno_27_A.stl 70 75
% Cuerno_29_A.stl 76 79
% Cuerno_31_A.stl 80 86
% Cuerno_33_A.stl 87 92
% Cuerno_35_A.stl 93 97
% Cuerno_37_A.stl 98 100 
%
% donde la primera columna contiene un nombre de archivo en el directorio
% local y la segunda y tercera el rango de aplicaciOn de este archivo
% expresados como porcentaje de la frecuencia mAxima (nf/2).

    ThisPiece.stagelist=(0:nf/2)*0;
    data = importdata(nom);
    nn = length(data.rowheaders);
    stage = struct('fileName','','jini',0,'jfin',0);
    for idat = 1:nn
      v = data.rowheaders(idat);
      stage(idat).fileName = v{1};
      stage(idat).jini = (data.data(idat,1)*(nf/2)/100);
      if idat>1
        stage(idat-1).jfin = stage(idat).jini-1;
      end
    end
    stage(nn).jfin = nf/2;
    for idat = 1:nn
      ThisPiece. ...
        stagelist(stage(idat).jini+1:stage(idat).jfin+1) = idat;
    end
    ThisPiece.stage = stage;
end