function T = Tij_3Done(ks,kp,rij,gam,vn)
% Funcion de Green de tracciones.
% BSSA, Vol. 85, No. 1, pp. 269-284, 1995
% Seismic Response of Three-Dimensional Alluvial Valleys for Incident P, S,
% and Rayleigh Waves by Francisco J. Sanchez-Sesma and Francisco Luzon

ba      = kp/ks;%beta/alpha
kpr     = kp*rij;
ksr     = ks*rij;
ksrm1   = 1/ksr;
g       = complex(zeros(3,1));

g(1)= ...
    (                      4-12i*ksrm1-12*ksrm1^2)*exp(-1i*ksr)+...
    (-1i*ba*ksr-4*ba^2-1+ 12i*ba*ksrm1+12*ksrm1^2)*exp(-1i*kpr);


g(2)= ...
    (                             -2+6i*ksrm1+6*ksrm1.^2).*exp(-1i*ksr)+...
    ( 1i*(2*ba^3-ba)*ksr+4*ba^2-1-6i*ba*ksrm1-6*ksrm1.^2).*exp(-1i*kpr);


g(3)= ...
    (-1i*ksr-3+ 6i*ksrm1+6*ksrm1^2)*exp(-1i*ksr)+...
    (2*ba^2 -6i*ba*ksrm1-6*ksrm1^2)*exp(-1i*kpr);


T   = complex(zeros(3,3,1,1));
d   = eye(3);
g0  = g(1)-g(2)-2*g(3);
fac = 1/(4*pi*rij^2);
gknk= gam(1)*vn(1)+gam(2)*vn(2)+gam(3)*vn(3);
% Tracciones: (calculo directo)
for i=1:3
  for j=1:3
    T(i,j,1,1)=fac*(g0*gam(i)*gam(j)*gknk +...
      g(3)*(gam(i)*vn(j)+gknk*d(i,j))+g(2)*gam(j)*vn(i));
  end
end
end