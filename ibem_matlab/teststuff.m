set(0,'DefaultFigureWindowStyle','normal')
close all; clear; main; %load('../out/init_vallenorm.mat');%rafraichi;
% load('burr.mat');rafraichi;
%%
clear;
set(0,'DefaultFigureWindowStyle','docked')
clc; load('burr.mat');
J = 1;
fj = 0.1;
     cmd_setCubatureTriangle7p; 
para    = normalizacion(para);
  para = getConnectivityStruct(para);
    para     = attenuation(para,fj);
  para = malla_geom3Dgen(para,fj);
%   coord = para.coord;
% nbeq    = coord.nbeq;
% nbeq2   = 3*nbeq;
% A = zeros(nbeq2,nbeq2);
[A]=Aij_3D_Block_Recip(para);
set(0,'DefaultFigureWindowStyle','normal')


%%
figure(6543);hold on
% jA = 1:coord.nbeq;
plot(colx,'Color',[0 0 i/coord.nbeq])
find(colx,1)
%%
clc;clear; load('burr.mat');
A     = zeros(coord.nbeq*3,coord.nbeq*3);
for i=491:632%1:coord.nbpt
%   [A(i,:),...       %continuidad tx
%    A(i+coord.nbeq,:),...  %continuidad ty
%    A(i+2*coord.nbeq,:)]...%continuidad tz
%    = Aij_Tn_3D(i,coord,para);
  if sum(coord.Xim(i,:)~=0)==2
    [A(coord.ju(i)+coord.nbpt,:),...       %continuidad ux
     A(coord.ju(i)+coord.nbpt+coord.nbeq,:),...  %continuidad uy
     A(coord.ju(i)+coord.nbpt+2*coord.nbeq,:)]...%continuidad uz
     = Aij_Gn_3D(i,coord,para);
  end
end
figure(3456);spy(A);
Adirect = A;
%%
o = gco;
figure;copyobj(o,gca)
set(gca,'ydir','reverse','dataaspectratio',[1 1 1]);
hold on
%%
hold on
jA = 1:nbeq*3;
plot(i,jA(colx),'go');
plot(i,jA(coly),'go');
plot(i,jA(colz),'go');
plot(i+nbeq,jA(colx),'go');
plot(i+nbeq,jA(coly),'go');
plot(i+nbeq,jA(colz),'go');
plot(i+nbeq*2,jA(colx),'go');
plot(i+nbeq*2,jA(coly),'go');
plot(i+nbeq*2,jA(colz),'go');

plot(jA(renx),i,'ro');
plot(jA(renx),i+nbeq,'ro');
plot(jA(renx),i+nbeq*2,'ro');
plot(jA(reny),i,'ro');
plot(jA(reny),i+nbeq,'ro');
plot(jA(reny),i+nbeq*2,'ro');
plot(jA(renz),i,'ro');
plot(jA(renz),i+nbeq,'ro');
plot(jA(renz),i+nbeq*2,'ro');

%% dibujar croissant
a = 1;
b = 0.3*a;
cont = 1;
for i=1:3
  for j=1:3
subplot(3,3,cont);
hold on
thneg =  pi/2:0.03:3*pi/2;
thpos = -pi/2:0.03:pi/2;
x = a*cos(thneg);
y = a*sin(thneg);
plot(x,y,'k'); hold on
x = min(a*cos(thpos),a+b*cos(thneg));
y = a*sin(thpos);
plot(x,y,'k'); hold on
plot3(pcX(1,ic),pcX(2,ic),pcX(3,ic),'r*')
cont = cont+1;
  end
end
%%
figure;hold on
for id = 1:284; plot3(pcX(1,id),pcX(2,id),pcX(3,id),'k.');end
%for id = 1:142; plot3(pTXi(1,id),pTXi(2,id),pTXi(3,id),'b*');end
plot3(pcX(1,jren(lrensGau)),pcX(2,jren(lrensGau)),pcX(3,jren(lrensGau)),'mo')
plot3(pcX(1,niG),pcX(2,niG),pcX(3,niG),'ro')
for iq=1:7;plot3(pTcubPts(1,iq,ic),pTcubPts(2,iq,ic),pTcubPts(3,iq,ic),'r*');end
%% Graficar Croissant chido
a = 1;
b = 0.7*a;
thneg =  pi/2:0.01:3*pi/2;
x = a*cos(thneg);
y = a*sin(thneg);
plot(x,y,'k'); hold on
clear x y
x=-a*cos(thneg);
y=a*sin(thneg);
for ith = 1:length(thneg)
  % distancia entre (a,0) y el punto actual:
  ra = sqrt((x(ith)-a)^2 + (y(ith)-0)^2);
  if ra < b
    x(ith)=  -sqrt(b^2-y(ith)^2)+a ;
  end
end
plot(x,y,'k'); hold on
%%
figure;clf;set(gcf,'name','bloque 1')
cont=1;
for i=1:3
  for j=1:3
    subplot(3,3,cont);
surf(squeeze(abs(BLOCKS{1}.Ab(i,j,:,:))),'EdgeColor','none');
cont=cont+1;
  end
end