function GenerarGeometrias(flag)
clc;
MAPA;
return

% [NPLAT,NIRR,NIRK] = cuernito(flag);
gausaXi(flag);
return
% verlas

cd '/Users/marshall/Documents/DOC/unit8/teamHomogeneo'
G = importdata('geom.txt');
disp('impotados')
cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'

n = size(G,1);
c=[NPLAT 2*NIRR NIRK];
col = zeros(n,3); 
col(1:c(1),2)=0.5; % NPLAT
col(1+c(1):c(1)+c(2),1)=0.5;  % NIRR
col(1+c(1)+c(2):c(1)+c(2)+c(3),3)=0.5;  % NIRK

figure(7771); clf; hold on
title('NPLAT')
for i=1:c(1)
   h = plotCircle3D(G(i,1:3),G(i,4:6),G(i,7),1);
   set(h,'color',col(i,:))
end
view(2); axis equal
  set(gca,'zdir','reverse','dataaspectratio',[1 1 1]);
  grid on
  set(gca,'Projection','perspective','Box','off')
  set(gca, 'XColor', 'r');set(gca, 'YColor', 'g');set(gca, 'ZColor', 'b')
  xlabel('x');  ylabel('y');  zlabel('z')
figure(7772); clf; hold on
title('NIRR')
for i=1+c(1):c(1)+c(2)
   h = plotCircle3D(G(i,1:3),G(i,4:6),G(i,7),1);
   set(h,'color',col(i,:))
end
view(2); axis equal
  set(gca,'zdir','reverse','dataaspectratio',[1 1 1]);
  grid on
  set(gca,'Projection','perspective','Box','off')
  set(gca, 'XColor', 'r');set(gca, 'YColor', 'g');set(gca, 'ZColor', 'b')
  xlabel('x');  ylabel('y');  zlabel('z')
figure(7773); clf; hold on
title('NIRK')
for i=1+c(1)+c(2):c(1)+c(2)+c(3)
   h = plotCircle3D(G(i,1:3),G(i,4:6),G(i,7),1);
   set(h,'color',col(i,:))
end
view(2); axis equal
  set(gca,'zdir','reverse','dataaspectratio',[1 1 1]);
  grid on
  set(gca,'Projection','perspective','Box','off')
  set(gca, 'XColor', 'r');set(gca, 'YColor', 'g');set(gca, 'ZColor', 'b')
  xlabel('x');  ylabel('y');  zlabel('z')
  
figure(7654); hold on
for i=1:n
   h = plotCircle3D(G(i,1:3),G(i,4:6),G(i,7),1);
   set(h,'color',col(i,:))
end
view(2); axis equal
  set(gca,'zdir','reverse','dataaspectratio',[1 1 1]);
  grid on
  set(gca,'Projection','perspective','Box','off')
  set(gca, 'XColor', 'r');set(gca, 'YColor', 'g');set(gca, 'ZColor', 'b')
  xlabel('x');  ylabel('y');  zlabel('z')

end

function MAPA
%% MAPA importar datos 
clear
cd ..
cd ins
rawdata = load('MAPA.DAT');
cd ..
cd ibem_matlab
% quitar los que est?n fuera de la zona lacustre
%del = [540 547 550 551 552 553 570 593 595];
del = [541 547 555 570 593 595 616 617 618 630 639 640 641 642 645 659 675];
% del = [];
rawdata(del,:) = [];
ndel = length(del);
data = rawdata(1:1010-ndel,:);      % lat lon periodo
borde = rawdata(1011-ndel:2516-ndel,:); % lat lon periodo
allda = rawdata(1:2516-ndel,:);

figure('Color','white'); hold on
axis on; grid on;
plot(borde(:,1),borde(:,2),'r.')
% plot(data(:,1),data(:,2),'b.')
plot3(data(:,1),data(:,2),data(:,3),'k.')
%
f = scatteredInterpolant(allda(:,1:2),allda(:,3));
%
n = 150;%90;
xlin = linspace(min(rawdata(:,1)),max(rawdata(:,1)),n);
ylin = linspace(min(rawdata(:,2)),max(rawdata(:,2)),n);
[X,Y] = meshgrid(xlin,ylin);
Z = f(X,Y);
% figure
% mesh(X,Y,Z) %interpolated
axis tight; hold on
contour(X,Y,Z,50)
% plot3(X,Y,Z,'.','MarkerSize',15) %nonuniform

clear
close all
% cd /Users/marshall/Documents/DOC/coco/ibem_matlab
cd ..
cd ins
I = imread('profs.png');
I = flipud(I);
rawdata = load('MAPA.DAT');
cd ..
cd ibem_matlab


% tama?o de imagen en pixeles
HorDimPx = size(I,2);
VerDimPx = size(I,1);
% WorldLimits
pasitoD = 10/60/16;
pasito = deg2km(pasitoD); % distancia en km entre crucecitas
escala = 1;
% en km
% xWorldLimits = [deg2km(-(99+10/60))-6.2*pasito deg2km(-(99))+15.4*pasito];%[0 HorDimPx*escala];%
% yWorldLimits = [deg2km(19+20/60)-13.8*pasito deg2km(19+30/60)+9.2*pasito];%[0 VerDimPx*escala];%
% en grados
xWorldLimits = [(-(99+10/60))-6.2*pasitoD (-(99))+15.4*pasitoD];%[0 HorDimPx*escala];%
yWorldLimits = [(19+20/60)-13.85*pasitoD (19+30/60)+9.2*pasitoD];%[0 VerDimPx*escala];%
R = imref2d(size(I),xWorldLimits,yWorldLimits) ;

% reducir ruido
% h = ones(6,6)/36; % <--- parametro que se ha calibrado
% h = ones(8,8)/64; % <--- parametro que se ha calibrado
% I = imfilter(I,h);

% convertirla en una imagen binaria a partir de un umbral de intensidad.
umbral = 0.4; 
BW = im2bw(I, umbral); % <--- parametro que se ha calibrado

% extraer contornos de la imagen
figure('Name','Datos de curvas de nivel'); hold on
% identificar las fronteras:
[B,L,N] = bwboundaries(BW,4,'noholes'); % en coordenadas de pixeles
imshow(BW,R,'InitialMagnification','fit');
hold on
% del analisis de los contornos elegimos solo as curvas de nivel:
clear ik
i = 1; ik(i) = 3 ; la(i) = 10; ii(i) = 1; ff(i) = 9161;
i = 2; ik(i) = 3 ; la(i) = 20; ii(i) = 9287; ff(i) = 15662;
i = 3; ik(i) = 3 ; la(i) = 10; ii(i) = 15723; ff(i) = length(B{ik(end),1}(:,2));
i = 4; ik(i) = 6 ; la(i) = 30; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 5; ik(i) = 7 ; la(i) = 40; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 6; ik(i) = 8 ; la(i) = 20; ii(i) = 1; ff(i) = 1677;
i = 7; ik(i) = 8 ; la(i) = 30; ii(i) = 1678; ff(i) = 5453;
i = 8; ik(i) = 8 ; la(i) = 20; ii(i) = 5454; ff(i) = length(B{ik(end),1}(:,2));
i = 9; ik(i) = 11 ; la(i) = 40; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 10;ik(i) = 13 ; la(i) = 10; ii(i) = 1; ff(i) = 545;
i = 11;ik(i) = 13 ; la(i) = 10; ii(i) = 1278; ff(i) = length(B{ik(end),1}(:,2));
i = 12;ik(i) = 15 ; la(i) = 50; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 13;ik(i) = 22 ; la(i) = 30; ii(i) = 1; ff(i) = 161;
i = 14;ik(i) = 22 ; la(i) = 20; ii(i) = 162; ff(i) = 568;
i = 15;ik(i) = 22 ; la(i) = 30; ii(i) = 569; ff(i) = length(B{ik(end),1}(:,2));
i = 16;ik(i) = 25 ; la(i) = 10; ii(i) = 1; ff(i) = 100;
i = 17;ik(i) = 25 ; la(i) = 10; ii(i) = 318; ff(i) = length(B{ik(end),1}(:,2));
i = 18;ik(i) = 40 ; la(i) = 20; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 19;ik(i) = 41 ; la(i) = 10; ii(i) = 1; ff(i) = 122;
i = 20;ik(i) = 41 ; la(i) = 10; ii(i) = 482; ff(i) = length(B{ik(end),1}(:,2));
i = 21;ik(i) = 52 ; la(i) = 20; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 22;ik(i) = 56 ; la(i) = 10; ii(i) = 1; ff(i) = 469;
i = 23;ik(i) = 56 ; la(i) = 10; ii(i) = 813; ff(i) = length(B{ik(end),1}(:,2));
i = 24;ik(i) = 57 ; la(i) = 10; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 25;ik(i) = 61 ; la(i) = 20; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 26;ik(i) = 66 ; la(i) = 20; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));
i = 27;ik(i) = 67 ; la(i) = 20; ii(i) = 1; ff(i) = length(B{ik(end),1}(:,2));

% RR para referenciar al sistema coordenado del mapa
RR = makerefmat(R.YWorldLimits(1), R.XWorldLimits(1), ...
(R.YWorldLimits(2) - R.YWorldLimits(1))/(R.ImageSize(1)),...
(R.XWorldLimits(2) - R.XWorldLimits(1))/(R.ImageSize(2))...
);

% trazar curvas de nivel
col = linspace(0.5,0.9,50);
latS = []; lonS = []; proF = [];
for k = 1:length(ik)
   [lat,lon] = pix2latlon(RR,B{ik(k),1}(ii(k):ff(k),2).*escala,...
                             B{ik(k),1}(ii(k):ff(k),1).*escala);
   latS = [latS;lat];
   lonS = [lonS;lon];
   proF = [proF;lat.*0 + la(k)];
   plot(lat,lon,'--','Color',col(la(k)).*[0 0 1], 'LineWidth', 0.1,'DisplayName',num2str(la(k)));
end

% trazar el borde de la cuenca de la otra serie de datos
borde = rawdata(1011:2516,1:2); % lat lon borde de la cuenca
plot(borde(:,1),borde(:,2),'r.')
set(gca,'yDir', 'normal')
latS = [latS;borde(:,1)];
lonS = [lonS;borde(:,2)];
proF = [proF;borde(:,1).*0];

% bordes extra para mejorar la interpolaci?n
latS = [latS;linspace(min(borde(:,1)),max(borde(:,1)),10)';...
             linspace(min(borde(:,1)),min(borde(:,1)),10)';...
             linspace(max(borde(:,1)),min(borde(:,1)),10)';...
             linspace(max(borde(:,1)),max(borde(:,1)),10)'];
lonS = [lonS;linspace(min(borde(:,2)),min(borde(:,2)),10)';...
             linspace(min(borde(:,2)),max(borde(:,2)),10)';...
             linspace(max(borde(:,2)),max(borde(:,2)),10)';...
             linspace(max(borde(:,2)),min(borde(:,2)),10)'];
proF = [proF;linspace(min(borde(:,1)),max(borde(:,1)),10)'.*0;...
             linspace(min(borde(:,1)),min(borde(:,1)),10)'.*0;...
             linspace(max(borde(:,1)),min(borde(:,1)),10)'.*0;...
             linspace(max(borde(:,1)),max(borde(:,1)),10)'.*0];

latS = [latS;[-99.03;-99.03;-99.07;-99.05;-99.09;-99.05;-99.032;-99.01;-98.99]];
lonS = [lonS;[19.45 ;19.43 ;19.41 ;19.42 ;19.28 ;19.28 ;19.275 ;19.27; 19.26 ]];
proF = [proF;[60;60;60;60;45;40;40;40;40]];
% MAPA interpolar una malla de datos
n = 200; %30 % El tama?o de la malla

f = scatteredInterpolant(latS,lonS,proF);
xlin = linspace(min(rawdata(:,1)),max(rawdata(:,1)),n);
ylin = linspace(min(rawdata(:,2)),max(rawdata(:,2)),n);
[X,Y] = meshgrid(xlin,ylin);
Z = f(X,Y);
figure('Name','Curvas de nivel interpoladas');
axis tight; hold on
contour(X,Y,Z,7)
plot(borde(:,1),borde(:,2),'r.')

figure('Name','Malla interpolada');
axis tight; hold on
h = mesh(X,Y,Z); %interpolated
plot(borde(:,1),borde(:,2),'r.')

% selecciona solo los que tengan normal no vertical y no esten en la superf
bol = h.VertexNormals(:,:,3)<0.99 * h.ZData(:,:) > 0;

% bol = true(size(bol)); % todas de nuevo

% Xochimilco
%bol2 = (h.YData(:,:) < 19.33) .* (h.XData(:,:) < -99.02);

% Chapultepec
bol2 = (h.YData(:,:) > 19.4) .* (h.YData(:,:) < 19.44) .* ...
       (h.XData(:,:) > -99.21) .* (h.XData(:,:) < -99.15);

bol = logical(bol .* bol2);

XData = h.XData(bol);%XData = [h.XData(bol);borde(:,1)];
YData = h.YData(bol);%YData = [h.YData(bol);borde(:,2)];
ZData = h.ZData(bol);%ZData = [h.ZData(bol);borde(:,1).*0];

ZData = ZData .*0;

figure('Name','V?rtices seleccionados');
plot3(XData,YData,ZData,'.')
hold on; plot(borde(:,1),borde(:,2),'r.')
contour(X,Y,Z,7)
% 
% %% seccion 1
% indSec = XData == XData(50000);
% is = find(indSec);
% 
% sec1 = [YData(is) ZData(is)];
% sec1(1,2) = 0;
% sec1(1,1) = sec1(1,1) - (sec1(2,1)-sec1(1,1))*6;
% sec1(120,2) = 0;
% sec1(121,2) = 0;
% sec1(end,2) = 0;
% 
% figure('Name','Curvas de nivel interpoladas');
% axis tight; hold on
% contour(X,Y,Z,7)
% plot(borde(:,1),borde(:,2),'r.')
% plot3(XData(is),YData(is),ZData(is).*0,'k-','LineWidth',2)
% set(gcf,'Color',[1 1 1]);
% 
% sec1(:,1) = deg2km(sec1(:,1))*1000; % en metros
% sec1(:,1) = sec1(:,1) - sec1(1,1);  % origen en el borde del valle
% % exportarlo como texto
% t = '';
% for i = 1:size(sec1,1);
%   t = sprintf('%s %f %f \n',t,sec1(i,1),sec1(i,2));
% end
% cd ..
% cd out
% nam = 'seccion1.txt';
% fileID = fopen(nam,'w');
% fprintf(fileID,'%s',t);
% fclose(fileID);
% cd ..
% cd ibem_matlab
% disp('done escribir sec1')
% %
% figure;hold on; title('seccion 1')
% plot(sec1(:,1)/1000,sec1(:,2),'.-'); set(gca,'Ydir','reverse')
% fill(sec1(:,1)/1000,sec1(:,2),  'g','edgecolor','none');
% set(gcf,'Color',[1 1 1]);
% xlabel('Sur -------- Norte   [Km]')
% ylabel('profundidad [m]')
% 
% FAS = sec1(:,2);
% FAS(FAS >= 33) = 33;
% fill(sec1(:,1)/1000,FAS,  'y','edgecolor','none');
% 
% CD = sec1(:,2);
% CD(CD <= 33) = 33;
% CD(CD >= 36) = 36;
% fill(sec1(:,1)/1000,CD,  'k','edgecolor','none');
% 
% % seccion 2
% indSec = XData == XData(29000);
% is = find(indSec);
% 
% sec1 = [YData(is) ZData(is)];
% sec1(1,2) = 0;
% % sec1(1,1) = sec1(1,1) - (sec1(2,1)-sec1(1,1))*6;
% sec1(end,2) = 0;
% 
% figure('Name','Curvas de nivel interpoladas');
% axis tight; hold on
% contour(X,Y,Z,7)
% plot(borde(:,1),borde(:,2),'r.')
% plot3(XData(is),YData(is),ZData(is).*0,'k-','LineWidth',2)
% set(gcf,'Color',[1 1 1]);
% 
% sec1(:,1) = deg2km(sec1(:,1))*1000; % en metros
% sec1(:,1) = sec1(:,1) - sec1(1,1);  % origen en el borde del valle
% % exportarlo como texto
% t = '';
% for i = 1:size(sec1,1);
%   t = sprintf('%s %f %f \n',t,sec1(i,1),sec1(i,2));
% end
% cd ..
% cd out
% nam = 'seccion2.txt';
% fileID = fopen(nam,'w');
% fprintf(fileID,'%s',t);
% fclose(fileID);
% cd ..
% cd ibem_matlab
% disp('done escribir sec2')
% %
% figure;hold on; title('seccion 2')
% plot(sec1(:,1)/1000,sec1(:,2),'.-'); set(gca,'Ydir','reverse')
% fill(sec1(:,1)/1000,sec1(:,2),  'g','edgecolor','none');
% set(gcf,'Color',[1 1 1]);
% xlabel('Sur -------- Norte   [Km]')
% ylabel('profundidad [m]')
% 
% FAS = sec1(:,2);
% FAS(FAS >= 33) = 33;
% fill(sec1(:,1)/1000,FAS,  'y','edgecolor','none');
% 
% CD = sec1(:,2);
% CD(CD <= 33) = 33;
% CD(CD >= 36) = 36;
% fill(sec1(:,1)/1000,CD,  'k','edgecolor','none');
% 


% MAPA Triangular
DT = delaunayTriangulation([XData,YData]); %lista de conectividad
DT3 = delaunayTriangulation([XData,YData,ZData]);
P = DT3.Points;
TR = triangulation(DT.ConnectivityList,P);
CN = incenter(TR);
inside = true(length(CN),1); 
area = zeros(length(CN),1);
for i=1:length(CN)
  t = TR.Points(TR.ConnectivityList(i,:),:);
  % lados del tr?angulo al cuadrado
  a = (t(1,1:2) - t(2,1:2)).^2; a2 = a(1)+a(2);% v1 a v2
  b = (t(2,1:2) - t(3,1:2)).^2; b2 = b(1)+b(2); % v2 a v3
  c = (t(3,1:2) - t(1,1:2)).^2; c2 = c(1)+c(2); % v3 a v1 
sum = a2+b2+c2;
area(i) = 0.25 * sqrt(4*a2*b2 - (a2+b2-c2)^2);
dis = 0.2;
if (a2 < dis*sum) || (b2 < dis*sum) || (c2 < dis*sum)
  inside(i) = false;
end
end
aProm = mean(area);
inside(area>2*aProm) = false;

T = DT.ConnectivityList(inside,:);

% Ajustar el borde de la malla a la superficie libre
% TR = triangulation(T,P);
% E = freeBoundary(TR);

%Xochimilco;
% for hh = 1:length(E(:,1))
%   if P(E(hh,1),1) < -99.03 && P(E(hh,2),1) < 19.32
%     P(E(hh,1),3) = 0;
%   end
% end

% otros
% P(E(:,1),3) = 0;

% la malla final
TR = triangulation(T,P);
FN = faceNormal(TR);
% CN = incenter(TR);

figure('Name',['Malla de ' num2str(length(FN)) ' tri?ngulos']);
set(gcf,'Color',[1 1 1]);
trimesh(TR);
hold on; plot(borde(:,1),borde(:,2),'r.')
% plot(TR.Points(E(:,1),1),TR.Points(E(:,1),2),'g.') %
view([0,90])


% grados a metros
centro(1) = mean(P(:,1)); centro(2) = mean(P(:,2)); centro(3) = 0;
Pc(:,1) = deg2km(P(:,1) - centro(1))*1000;
Pc(:,2) = deg2km(P(:,2) - centro(2))*1000;
Pc(:,3) = P(:,3); 
% TR = triangulation(T,Pc);
% trimesh(TR);
%% MAPA escribir STL de frontera de continuidad
clear t
t = sprintf('%s %d \n','solid ValleMex ',length(FN));
for i = 1:length(FN)
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',FN(i,1),FN(i,2),FN(i,3));
t = sprintf('%s%s \n',t,'outer loop');
t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),Pc(T(i,1),3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),Pc(T(i,2),3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),Pc(T(i,3),3));
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
end
t = sprintf('%s%s',t,'endsolid ValleMex');
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
nam = ['ValleChapulmedio2' num2str(length(FN)) '.stl'];
fileID = fopen(nam,'w');
fprintf(fileID,'%s',t);
fclose(fileID);
cd ..
cd ibem_matlab
disp('done')
end

function [NPLAT,NIRR,NIRK] = cuernito(flag)
cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'
%% datos 
% clear
set(0,'DefaultFigureWindowStyle','docked') % 'normal'
% S?nchez-Sesma & Luzon 1995
% a > b  && b = 0.7a && h = 0.4/a && a = 4km
% f(x,y) = h(b^2 - P^2)[1 - 2a(a-x)/P^2]
Bmin = 1; %km/s
fmax = 0.5; %Hertz
lamdamin = Bmin/fmax/6; %km
numelems = 4/lamdamin/2; %cantidad de triangulos 
disp(['numelems= ' num2str(numelems)])

aCro = 1;
HCro = 0.4/aCro;
bCro = 0.7*aCro;
nplatradio = 1.0;
nirrradio = 1.0;
nespacios = 22;%(n?mero non)
numerodeDivisiones = 0; %veces que NIRR se divide entre 2 

%% generar triangulos 
tamanos = [];
alcance = 1.0*nirrradio;
xlin = linspace(-alcance,alcance,nespacios);
ylin = linspace(-alcance,alcance,nespacios);
[x,y] = meshgrid(xlin,ylin);
z = x.*0;
for i=1:length(xlin)
for j=1:length(ylin)
% z(i,j) = Croissant(aCro,bCro,HCro,x(i,j),y(i,j));
z(i,j) = GaussianaSS1983(x(i,j),y(i,j));
end
end
% hf = figure; clf
hf = figure('Visible','Off'); clf
axis tight; hold on
h = mesh(x,y,z);
XData = h.XData(:);
YData = h.YData(:);
ZData = h.ZData(:);

if flag == 0
  set(hf,'Visible','on')
  return
else
  delete(h); close(hf)
end
DT = delaunayTriangulation([XData,YData]); %disp('lista de conectividad NIRR')
DT3 = delaunayTriangulation([XData,YData,ZData]);
Pc = DT3.Points;
TR = triangulation(DT.ConnectivityList,Pc);


%% NIRR 
% que el borde libre tenga coordenada 0
E = freeBoundary(TR);
Pc(E(:,1),3) = 0;

% quitar los triangulos en los que todos los vertices z=0
FN = faceNormal(TR); %disp(['Numero de caras del cuernito: ', num2str(length(FN))])
b = FN(:,3) == 1; % con normal vertical
% 
btru = true(size(b));
T = TR.ConnectivityList(btru,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),3));
end
bb = Cen == 0; % con z = 0
b = b & bb;

bol = ~b; %los que no tienen normal vertical

T = TR.ConnectivityList(bol,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),2));
end
% hacerlo sim?trico en y
if mod(nespacios,2) ~= 0
iflp = Cen(:,1)<=0; %alcance/(nespacios-1); % espejear
NN = sum(iflp)*2;
else
iflp = true(size(Cen(:,1)));
NN = sum(iflp);
end
PcT = zeros(NN,3,3); % i,vertice,componente
i = 1;
norm = zeros(NN,3);
for ind = 1:length(iflp)
    if iflp(ind) 
        % copiar original
norm(i,:) = FN(ind,:);
PcT(i,1,1)=Pc(T(ind,1),1); PcT(i,1,2)=Pc(T(ind,1),2); PcT(i,1,3)=Pc(T(ind,1),3);
PcT(i,2,1)=Pc(T(ind,2),1); PcT(i,2,2)=Pc(T(ind,2),2); PcT(i,2,3)=Pc(T(ind,2),3);
PcT(i,3,1)=Pc(T(ind,3),1); PcT(i,3,2)=Pc(T(ind,3),2); PcT(i,3,3)=Pc(T(ind,3),3);  
if mod(nespacios,2) ==0 % Cen(ind,1) > -alcance/(nespacios-1)
  i = i+1;
  continue
end
        % espejear en y
norm(i+1,1:2:3) =  norm(i,1:2:3); 
norm(i+1,  2  ) = -norm(i,  2  );
PcT (i+1,3:-1:1,1:2:3) =  PcT(i,1:3,1:2:3); 
PcT (i+1,3:-1:1,  2  ) = -PcT(i,1:3,  2  );
i = i+2;
    end
end
%
disp(['NIRR= ' num2str(NN)])

%dividir cada traingulo por el factor factorNPLAT
for divisiones = 1:numerodeDivisiones
Pctaux = zeros(NN*2,3,3);
normaux = zeros(NN*2,3);
j = 1;
for i = 1:NN
  % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande
  
  % primer tri?ngulo nuevo
  Pctaux(j,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j,2,:) = PcT(i,l(lM,5),:);%primer v?rtice lado partido
  Pctaux(j,3,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  
  % segundo tri?ngulo nuevo
  Pctaux(j+1,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j+1,2,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  Pctaux(j+1,3,:) = PcT(i,l(lM,6),:);%segundo v?rtice lado partido
  
  % las normales
  normaux(j,:) = norm(i,:);
  normaux(j+1,:) = norm(i,:);
  j = j+2;
end
clear PcT
PcT = Pctaux;
clear Pctaux;
clear norm
norm = normaux;
clear normaux;

NN = size(PcT,1);
end

disp(['NIRR= ' num2str(NN)])
Cen = zeros(NN,3);
Rad = zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1)); %media x
    Cen(i,2) = mean(PcT(i,1:3,2)); %media y
    Cen(i,3) = mean(PcT(i,1:3,3)); %media z
    
    % correcci?n de circulos:
    % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande y del vertice opeusto
    
    % normal y distancia entre centro de lado m?s largo y v?rtice opuesto
    dl = sqrt(sum(( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) .^2));
    nl = ( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) / dl;
    
    % Recorrer Centro lejos del lado m?s largo
    rd = l(lM,1)^0.5 / dl / 72; %nirr
    Cen(i,1) = Cen(i,1) + nl(1)*rd;
    Cen(i,2) = Cen(i,2) + nl(2)*rd;
    Cen(i,3) = Cen(i,3) + nl(3)*rd;
    
    % A = 1/2 |(v1 - v3) x (v2 - v3)|
    cro = cross(PcT(i,1,1:3) - PcT(i,3,1:3),...
                PcT(i,2,1:3) - PcT(i,3,1:3));
    area = 0.5 * sum(abs(cro).^2)^(1/2);
    Rad(i) = sqrt(area/pi);
end

figure; clf; hold on
set(gcf,'name','nirr')
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3);
  fill3(lx,ly,lz,[0.5 1.0 0.333]);
  
  h = plotCircle3D([Cen(i,1),Cen(i,2),Cen(i,3)],...
    [-norm(i,1),-norm(i,2),-norm(i,3)],Rad(i),1);
   set(h,'color',[0,0.5,0])
end
view(2); axis equal


clear nam
nam = ['Cuerno_',num2str(nespacios),'_A'];
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnirr = sprintf('%s %f \n','nirr= ',NN);
tnirr = '';
tamanos(2) = NN;
for i = 1:NN % escribir nirr.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',-norm(i,1),-norm(i,2),-norm(i,3)); % FC
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),Pc(T(i,1),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),Pc(T(i,2),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),Pc(T(i,3),3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),PcT(i,1,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),PcT(i,2,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),PcT(i,3,3));
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnirr = sprintf('%s %f %f %f %f %f %f %f \n',tnirr,...
  [Cen(i,1),Cen(i,2),Cen(i,3),-norm(i,1),-norm(i,2),-norm(i,3),Rad(i)]);
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nirr.txt','w');
fprintf(fileID,'%s',tnirr);
fclose(fileID);
tgeom = sprintf('%s%s',tnirr,tnirr);
cd ..
cd ibem_matlab

if flag == 1
  NPLAT=0;
  NIRR=tamanos(2);
  NIRK=0;
  return
end

%% NIRK 
disp(['NIRK= ' num2str(NN)])
clear nam
nam = ['Cuerno_',num2str(nespacios),'_B'];
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnirk = sprintf('%s %f \n','nirk= ',NN);
tnirk = '';
tamanos(3) = NN;
for i = 1:NN% escribir nirk.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',0,0,-1);
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),0);
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnirk = sprintf('%s %f %f %f %f %f %f %f \n',tnirk,...
  [Cen(i,1),Cen(i,2),0,0,0,1,Rad(i)]);
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nirk.txt','w');
fprintf(fileID,'%s',tnirk);
fclose(fileID);
tgeom = sprintf('%s%s',tgeom,tnirk);
cd ..
cd ibem_matlab

figure(2); clf; hold on
set(gcf,'name','nirk')
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3) * 0;
  fill3(lx,ly,lz,[1.0 0.5 0.333]);
  
  h = plotCircle3D([Cen(i,1),Cen(i,2),0],...
    [0,0,1],Rad(i),1);
   set(h,'color',[0.5,0,0])
end
view(2); axis equal
%% NPLAT 
alcance = 1.2*nplatradio;
xlin = linspace(-alcance,alcance,nespacios);
ylin = linspace(-alcance,alcance,nespacios);
[x,y] = meshgrid(xlin,ylin);
z = x.*0;
for i=1:length(xlin)
for j=1:length(ylin)
% z(i,j) = Croissant(aCro,bCro,HCro,x(i,j),y(i,j));
z(i,j) = GaussianaSS1983(x(i,j),y(i,j));
end
end
hf = figure('Visible','Off'); clf
axis tight; hold on
h = mesh(x,y,z);
XData = h.XData(:);
YData = h.YData(:);
ZData = h.ZData(:);
delete(h); close(hf)

DT = delaunayTriangulation([XData,YData]);
DT3 = delaunayTriangulation([XData,YData,ZData]);
Pc = DT3.Points;
TR = triangulation(DT.ConnectivityList,Pc);
% E = freeBoundary(TR);
% Pc(E(:,1),3) = 0;
FN = faceNormal(TR);
bol = FN(:,3) == 1;
T = TR.ConnectivityList(bol,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
% espejear
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),2));
end

iflp = Cen(:,1)<0;
NN = sum(iflp)*2;
PcT = zeros(NN,3,3); % i,vertice,componente
i = 1;
norm = zeros(NN,3);
for ind = 1:length(iflp)
    if iflp(ind) 
        % copiar original
norm(i,:) = FN(ind,:);
PcT(i,1,1)=Pc(T(ind,1),1); PcT(i,1,2)=Pc(T(ind,1),2); PcT(i,1,3)=Pc(T(ind,1),3);
PcT(i,2,1)=Pc(T(ind,2),1); PcT(i,2,2)=Pc(T(ind,2),2); PcT(i,2,3)=Pc(T(ind,2),3);
PcT(i,3,1)=Pc(T(ind,3),1); PcT(i,3,2)=Pc(T(ind,3),2); PcT(i,3,3)=Pc(T(ind,3),3);  
        % espejear en y
norm(i+1,1:2:3) =  norm(i,1:2:3); 
norm(i+1,  2  ) = -norm(i,  2  );
PcT (i+1,3:-1:1,1:2:3) =  PcT(i,1:3,1:2:3); 
PcT (i+1,3:-1:1,  2  ) = -PcT(i,1:3,  2  );
i = i+2;
    end
end

% disp(['en lugar de ' num2str(NN)])
distac=zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1));%+aCro/8;
    Cen(i,2) = mean(PcT(i,1:3,2));
    Cen(i,3) = mean(PcT(i,1:3,3));
    distac(i) = sum(abs(Cen(i,1:2)).^2)^(1/2);
end
aux = PcT(1:NN,1:3,1:3);
clear PcT
b = distac(:)<nplatradio; %quitar los que est?n lejos
estossi = logical(b);
PcT = aux(estossi,:,:);
NN = size(PcT,1);
disp(['NPLAT= ' num2str(NN)])

Cen = zeros(NN,3);
Rad = zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1));
    Cen(i,2) = mean(PcT(i,1:3,2));
    Cen(i,3) = mean(PcT(i,1:3,3));
    
    % correcci?n
    % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande y del vertice opeusto
    
    % normal y distancia entre centro de lado m?s largo y v?rtice opuesto
    dl = sqrt(sum(( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) .^2));
    nl = ( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) / dl;
    
    % Recorrer Centro lejos del lado m?s largo
    rd = l(lM,1)^0.5 / dl / 72;
    Cen(i,1) = Cen(i,1) + nl(1)*rd;
    Cen(i,2) = Cen(i,2) + nl(2)*rd;
    Cen(i,3) = Cen(i,3) + nl(3)*rd;
    
    % A = 1/2 |(v1 - v3) x (v2 - v3)|
%     area = 0.5 * norm(cross(PcT(i,1:3,1) - PcT(i,1:3,3),...
%                             PcT(i,1:3,2) - PcT(i,1:3,3)));
    cro = cross(PcT(i,1,1:3) - PcT(i,3,1:3),...
                PcT(i,2,1:3) - PcT(i,3,1:3));
    area = 0.5 * sum(abs(cro).^2)^(1/2);
    Rad(i) = sqrt(area/pi);
end
%
clear nam
nam = ['Cuerno_',num2str(nespacios),'_C'];
clear tnplat
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnplat = sprintf('%s %f \n','nplat= ',NN);
tnplat = '';
tamanos(1) = NN;
for i = 1:NN% escribir nplat.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',0,0,-1);
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),0); 
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnplat = sprintf('%s %f %f %f %f %f %f %f \n',tnplat,...
  [Cen(i,1),Cen(i,2),0,0,0,-1,Rad(i)]);
% if i ~= NN
%   tnplat = sprintf('%s \n',tnplat);
% end
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nplat.txt','w');
fprintf(fileID,'%s',tnplat);
fclose(fileID);
cd ..
cd ibem_matlab

figure(3); clf;
set(gcf,'name','nplat')
hold on
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3);
  fill3(lx,ly,lz,[1.0 0.6 0.8]);
  
  h = plotCircle3D([Cen(i,1),Cen(i,2),0.1],[0,0,-1],Rad(i),1);
   set(h,'color',[0,0,0.5])
end
view(2); axis equal
%% NPLATPLAT 
alcance = 1.2*nplatradio;
xlin = linspace(-alcance,alcance,nespacios);
ylin = linspace(-alcance,alcance,nespacios);
[x,y] = meshgrid(xlin,ylin);
z = x.*0;
% for i=1:length(xlin)
% for j=1:length(ylin)
% z(i,j) = Croissant(aCro,bCro,HCro,x(i,j),y(i,j));
% end
% end
hf = figure('Visible','Off'); clf
axis tight; hold on
h = mesh(x,y,z);
XData = h.XData(:);
YData = h.YData(:);
ZData = h.ZData(:);
delete(h); close(hf)

DT = delaunayTriangulation([XData,YData]);
DT3 = delaunayTriangulation([XData,YData,ZData]);
Pc = DT3.Points;
TR = triangulation(DT.ConnectivityList,Pc);
% E = freeBoundary(TR);
% Pc(E(:,1),3) = 0;
FN = faceNormal(TR);
bol = FN(:,3) == 1;
T = TR.ConnectivityList(bol,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
% espejear
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),2));
end

iflp = Cen(:,1)<0;
NN = sum(iflp)*2;
PcT = zeros(NN,3,3); % i,vertice,componente
i = 1;
norm = zeros(NN,3);
for ind = 1:length(iflp)
    if iflp(ind) 
        % copiar original
norm(i,:) = FN(ind,:);
PcT(i,1,1)=Pc(T(ind,1),1); PcT(i,1,2)=Pc(T(ind,1),2); PcT(i,1,3)=Pc(T(ind,1),3);
PcT(i,2,1)=Pc(T(ind,2),1); PcT(i,2,2)=Pc(T(ind,2),2); PcT(i,2,3)=Pc(T(ind,2),3);
PcT(i,3,1)=Pc(T(ind,3),1); PcT(i,3,2)=Pc(T(ind,3),2); PcT(i,3,3)=Pc(T(ind,3),3);  
        % espejear en y
norm(i+1,1:2:3) =  norm(i,1:2:3); 
norm(i+1,  2  ) = -norm(i,  2  );
PcT (i+1,3:-1:1,1:2:3) =  PcT(i,1:3,1:2:3); 
PcT (i+1,3:-1:1,  2  ) = -PcT(i,1:3,  2  );
i = i+2;
    end
end

% disp(['en lugar de ' num2str(NN)])
distac=zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1));%+aCro/8;
    Cen(i,2) = mean(PcT(i,1:3,2));
    Cen(i,3) = mean(PcT(i,1:3,3));
    distac(i) = sum(abs(Cen(i,1:2)).^2)^(1/2);
end
aux = PcT(1:NN,1:3,1:3);
clear PcT
b = distac(:)<nplatradio; %quitar los que est?n lejos
estossi = logical(b);
PcT = aux(estossi,:,:);
NN = size(PcT,1);
disp(['NPLATPLAT= ' num2str(NN)])

Cen = zeros(NN,3);
Rad = zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1));
    Cen(i,2) = mean(PcT(i,1:3,2));
    Cen(i,3) = mean(PcT(i,1:3,3));
    
    % correcci?n
    % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande y del vertice opeusto
    
    % normal y distancia entre centro de lado m?s largo y v?rtice opuesto
    dl = sqrt(sum(( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) .^2));
    nl = ( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) / dl;
    
    % Recorrer Centro lejos del lado m?s largo
    rd = l(lM,1)^0.5 / dl / 72;
    Cen(i,1) = Cen(i,1) + nl(1)*rd;
    Cen(i,2) = Cen(i,2) + nl(2)*rd;
    Cen(i,3) = Cen(i,3) + nl(3)*rd;
    
    % A = 1/2 |(v1 - v3) x (v2 - v3)|
%     area = 0.5 * norm(cross(PcT(i,1:3,1) - PcT(i,1:3,3),...
%                             PcT(i,1:3,2) - PcT(i,1:3,3)));
    cro = cross(PcT(i,1,1:3) - PcT(i,3,1:3),...
                PcT(i,2,1:3) - PcT(i,3,1:3));
    area = 0.5 * sum(abs(cro).^2)^(1/2);
    Rad(i) = sqrt(area/pi);
end
%
clear nam
nam = ['Cuerno_',num2str(nespacios),'_D'];
clear tnplat
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnplat = sprintf('%s %f \n','nplat= ',NN);
tnplat = '';
tamanos(1) = NN;
for i = 1:NN% escribir nplat.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',0,0,-1);
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),0); 
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnplat = sprintf('%s %f %f %f %f %f %f %f \n',tnplat,...
  [Cen(i,1),Cen(i,2),0,0,0,-1,Rad(i)]);
% if i ~= NN
%   tnplat = sprintf('%s \n',tnplat);
% end
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nplat.txt','w');
fprintf(fileID,'%s',tnplat);
fclose(fileID);
cd ..
cd ibem_matlab

figure(3); clf;
set(gcf,'name','nplat')
hold on
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3);
  fill3(lx,ly,lz,[1.0 0.6 0.8]);
  
  h = plotCircle3D([Cen(i,1),Cen(i,2),0.1],[0,0,-1],Rad(i),1);
   set(h,'color',[0,0,0.5])
end
view(2); axis equal

%% escribir geom.txt 
cd '/Users/marshall/Documents/DOC/unit8/teamHomogeneo'
tgeom = sprintf('%s%s',tnplat,tgeom);
fileID = fopen('geom.txt','w');
fprintf(fileID,'%s',tgeom);
fclose(fileID);
cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'


disp(['NBP = ' num2str(tamanos(1)+2*tamanos(2)+tamanos(3))])
disp(tamanos)
disp('end')
NPLAT=tamanos(1);
NIRR=tamanos(2);
NIRK=tamanos(3);
%% malla de receptores
% clc
% clear
% x0 = -3;
% y0 = -1.5;
% d = (1.5 +1.5)/20;
% m = zeros(21*21,4);
% ind = 1;
% for i = 1:21
%   for j = 1:21
%     m(ind,:) = [x0+(i-1)*d y0+(j-1)*d 0 2];
%     ind = ind+1;
%   end
% end
% disp(num2str(m(:,1:4)))
%% linea de receptores
% clc
% clear
% aCro = 1;
% x0 = 0;
% y0 = -1.5;
% yf = 1.5;
% nrecep = 100;
% d = linspace(y0,yf,nrecep);
% m = zeros(nrecep,4);
% ind = 1;
% for i = 1:nrecep
%     reg = 1;
%     if abs(d(i)) <= aCro
%       reg = 2;
%     end
%     m(ind,:) = [x0 d(i) 0 reg];
%     ind = ind+1;
% end
% disp(num2str(m(:,1:4)))
end

function out = lado(a,b,l1,l2)
out = zeros(1,6);
out(1) = ( (a(1)-b(1))^2 + ...
           (a(2)-b(2))^2 + ...
           (a(3)-b(3))^2 ); %tama?o de la cara al cuadrado
 % punto medio:
 out(2) = b(1) + (a(1)-b(1))/2;
 out(3) = b(2) + (a(2)-b(2))/2;
 out(4) = b(3) + (a(3)-b(3))/2;
 out(5) = l1;
 out(6) = l2;
end

% function z = GaussianaSS1983(x,y)
% a = 1;
% h = a * 0.5;
% xi = 1/a*sqrt(x^2+y^2);
% if 0 <= xi && xi <= 1
%   z = -h * (1 -3*xi^2 +2*xi^3);
% else
%   z=0;
% end
% end

function gausaXi(flag)
cd '/Users/marshall/Documents/DOC/coco/ibem_matlab'
%% datos 
% clear
set(0,'DefaultFigureWindowStyle','docked') % 'normal'
Bmin = 1; %km/s
fmax = 0.5; %Hertz
lamdamin = Bmin/fmax/6; %km
numelems = 4/lamdamin/2; %cantidad de triangulos 
disp(['numelems= ' num2str(numelems)])

aCro = 1;
nplatradio = 1.0;
nirrradio = 1.0;
nespacios = 11;%(n?mero non)
numerodeDivisiones = 0; %veces que NIRR se divide entre 2 

%% generar triangulos 
tamanos = [];
alcance = 1.0*nirrradio;
xlin = linspace(-alcance,alcance,nespacios);
ylin = linspace(-alcance,alcance,nespacios);
[x,y] = meshgrid(xlin,ylin);
z = x.*0;
for i=1:length(xlin)
for j=1:length(ylin)
z(i,j) = GaussianaSS1983(x(i,j),y(i,j));
end
end
% hf = figure; clf
hf = figure('Visible','Off'); clf
axis tight; hold on
h = mesh(x,y,z);
XData = h.XData(:);
YData = h.YData(:);
ZData = h.ZData(:);

if flag == 0
  set(hf,'Visible','on')
  return
else
  delete(h); close(hf)
end
DT = delaunayTriangulation([XData,YData]); %disp('lista de conectividad NIRR')
DT3 = delaunayTriangulation([XData,YData,ZData]);
Pc = DT3.Points;
TR = triangulation(DT.ConnectivityList,Pc);


%% NIRR 
% que el borde libre tenga coordenada 0
E = freeBoundary(TR);
Pc(E(:,1),3) = 0;

% quitar los triangulos en los que todos los vertices z=0
FN = faceNormal(TR); %disp(['Numero de caras del cuernito: ', num2str(length(FN))])
b = FN(:,3) == 1; % con normal vertical
% 
btru = true(size(b));
T = TR.ConnectivityList(btru,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),3));
end
bb = Cen == 0; % con z = 0
b = b & bb;

bol = ~b; %los que no tienen normal vertical

T = TR.ConnectivityList(bol,:);
TR = triangulation(T,Pc);
FN = faceNormal(TR);
Cen = zeros(length(FN),1);
for i = 1:length(FN)
    Cen(i) = mean(Pc(T(i,1:3),2));
end
% hacerlo sim?trico en y
if mod(nespacios,2) ~= 0
iflp = Cen(:,1)<=0; %alcance/(nespacios-1); % espejear
NN = sum(iflp)*2;
else
iflp = true(size(Cen(:,1)));
NN = sum(iflp);
end
PcT = zeros(NN,3,3); % i,vertice,componente
i = 1;
norm = zeros(NN,3);
for ind = 1:length(iflp)
    if iflp(ind) 
        % copiar original
norm(i,:) = FN(ind,:);
PcT(i,1,1)=Pc(T(ind,1),1); PcT(i,1,2)=Pc(T(ind,1),2); PcT(i,1,3)=Pc(T(ind,1),3);
PcT(i,2,1)=Pc(T(ind,2),1); PcT(i,2,2)=Pc(T(ind,2),2); PcT(i,2,3)=Pc(T(ind,2),3);
PcT(i,3,1)=Pc(T(ind,3),1); PcT(i,3,2)=Pc(T(ind,3),2); PcT(i,3,3)=Pc(T(ind,3),3);  
if mod(nespacios,2) ==0 % Cen(ind,1) > -alcance/(nespacios-1)
  i = i+1;
  continue
end
        % espejear en y
norm(i+1,1:2:3) =  norm(i,1:2:3); 
norm(i+1,  2  ) = -norm(i,  2  );
PcT (i+1,3:-1:1,1:2:3) =  PcT(i,1:3,1:2:3); 
PcT (i+1,3:-1:1,  2  ) = -PcT(i,1:3,  2  );
i = i+2;
    end
end
%
disp(['NIRR= ' num2str(NN)])

%dividir cada traingulo por el factor factorNPLAT
for divisiones = 1:numerodeDivisiones
Pctaux = zeros(NN*2,3,3);
normaux = zeros(NN*2,3);
j = 1;
for i = 1:NN
  % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande
  
  % primer tri?ngulo nuevo
  Pctaux(j,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j,2,:) = PcT(i,l(lM,5),:);%primer v?rtice lado partido
  Pctaux(j,3,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  
  % segundo tri?ngulo nuevo
  Pctaux(j+1,1,:) = PcT(i,lM,:);% v?rtice opuesto al lado m?s largo
  Pctaux(j+1,2,:) = l(lM,2:4);%nuevo v?rtice (mitad de lado partido)
  Pctaux(j+1,3,:) = PcT(i,l(lM,6),:);%segundo v?rtice lado partido
  
  % las normales
  normaux(j,:) = norm(i,:);
  normaux(j+1,:) = norm(i,:);
  j = j+2;
end
clear PcT
PcT = Pctaux;
clear Pctaux;
clear norm
norm = normaux;
clear normaux;

NN = size(PcT,1);
end

disp(['NIRK= ' num2str(NN)])
Cen = zeros(NN,3);
Rad = zeros(NN,1);
for i = 1:NN
    Cen(i,1) = mean(PcT(i,1:3,1)); %media x
    Cen(i,2) = mean(PcT(i,1:3,2)); %media y
    Cen(i,3) = mean(PcT(i,1:3,3)); %media z
    
    % correcci?n de circulos:
    % Encontrar el lado m?s largo, su centro y el ?ndice de v?rtice opuesto
  l(3,1:6) = lado(PcT(i,1,:),PcT(i,2,:),1,2);
  l(1,1:6) = lado(PcT(i,2,:),PcT(i,3,:),2,3);
  l(2,1:6) = lado(PcT(i,1,:),PcT(i,3,:),1,3);
  lM = find(l == max(l(:,1)),1); %indice del lado m?s grande y del vertice opeusto
    
    % normal y distancia entre centro de lado m?s largo y v?rtice opuesto
    dl = sqrt(sum(( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) .^2));
    nl = ( squeeze(PcT(i,lM,:)) - l(lM,2:4).' ) / dl;
    
    % Recorrer Centro lejos del lado m?s largo
    rd = l(lM,1)^0.5 / dl / 72; %nirr
    Cen(i,1) = Cen(i,1) + nl(1)*rd;
    Cen(i,2) = Cen(i,2) + nl(2)*rd;
    Cen(i,3) = Cen(i,3) + nl(3)*rd;
    
    % A = 1/2 |(v1 - v3) x (v2 - v3)|
    cro = cross(PcT(i,1,1:3) - PcT(i,3,1:3),...
                PcT(i,2,1:3) - PcT(i,3,1:3));
    area = 0.5 * sum(abs(cro).^2)^(1/2);
    Rad(i) = sqrt(area/pi);
end

figure; clf; hold on
set(gcf,'name','nirk')
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3);
  fill3(lx,ly,lz,[0.5 1.0 0.333]);
  
%   h = plotCircle3D([Cen(i,1),Cen(i,2),Cen(i,3)],...
%     [-norm(i,1),-norm(i,2),-norm(i,3)],Rad(i),1);
%    set(h,'color',[0,0.5,0])
end
view(2); axis equal


clear nam
nam = ['Gau_',num2str(nespacios),'_B'];
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnirr = sprintf('%s %f \n','nirr= ',NN);
tnirr = '';
tamanos(2) = NN;
for i = 1:NN % escribir nirr.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',-norm(i,1),-norm(i,2),-norm(i,3)); % FC
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),Pc(T(i,1),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),Pc(T(i,2),3));
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),Pc(T(i,3),3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),PcT(i,1,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),PcT(i,2,3));
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),PcT(i,3,3));
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnirr = sprintf('%s %f %f %f %f %f %f %f \n',tnirr,...
  [Cen(i,1),Cen(i,2),Cen(i,3),-norm(i,1),-norm(i,2),-norm(i,3),Rad(i)]);
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nirk.txt','w');
fprintf(fileID,'%s',tnirr);
fclose(fileID);
tgeom = sprintf('%s%s',tnirr,tnirr);
cd ..
cd ibem_matlab

if flag == 1
  return
end

%% NIRK 
disp(['NIRR= ' num2str(NN)])
clear nam
nam = ['Gau_',num2str(nespacios),'_A'];
clear t
t = sprintf('%s \n',['solid ',nam]);
% tnirk = sprintf('%s %f \n','nirk= ',NN);
tnirk = '';
tamanos(3) = NN;
for i = 1:NN% escribir nirk.txt
t = sprintf('%s%s %f %f %f \n',t,'facet normal ',0,0,1);
t = sprintf('%s%s \n',t,'outer loop');
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,3),1),Pc(T(i,3),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,2),1),Pc(T(i,2),2),0);
% t = sprintf('%s%s %f %f %f \n',t,'vertex ',Pc(T(i,1),1),Pc(T(i,1),2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,3,1),PcT(i,3,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,2,1),PcT(i,2,2),0);
t = sprintf('%s%s %f %f %f \n',t,'vertex ',PcT(i,1,1),PcT(i,1,2),0);
t = sprintf('%s%s \n',t,'endloop');
t = sprintf('%s%s \n',t,'endfacet');
tnirk = sprintf('%s %f %f %f %f %f %f %f \n',tnirk,...
  [Cen(i,1),Cen(i,2),0,0,0,1,Rad(i)]);
end
t = sprintf('%s%s',t,['endsolid ',nam]);
t = sprintf('%s%s \n',t,'endfacet');
cd ..
cd out
fileID = fopen([nam,'.stl'],'w');
fprintf(fileID,'%s',t);
fclose(fileID);
fileID = fopen('nirk.txt','w');
fprintf(fileID,'%s',tnirk);
fclose(fileID);
tgeom = sprintf('%s%s',tgeom,tnirk);
cd ..
cd ibem_matlab

figure; clf; hold on
set(gcf,'name','nirr')
for i=1:NN
  lx = PcT(i,:,1);
  ly = PcT(i,:,2);
  lz = PcT(i,:,3) * 0;
  fill3(lx,ly,lz,[1.0 0.5 0.333]);
%   
%   h = plotCircle3D([Cen(i,1),Cen(i,2),0],...
%     [0,0,1],Rad(i),1);
%    set(h,'color',[0.5,0,0])
end
view(2); axis equal

end

function km = deg2km(deg)
% seria parte del Mapping toolbox

% km = deg2km(deg) converts distances from degrees to kilometers as 
% measured along a great circle on a sphere with a radius of 6371 km, 
% the mean radius of the Earth.

km = (6371 * deg);
end