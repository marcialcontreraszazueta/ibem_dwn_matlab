function Save_tmp_U0(DWNin,j) % %#ok<INUSD>

DWN=cell(size(DWNin));
for im=1:size(DWNin,1)
  DWN{im}.U0=DWNin{im}.U0;
  % S0 ?
end

% name = ['../out/' para.nametmp(1:end-4)];

name = '../out/U0';
if j < 10
save([name,'_00',num2str(j),'tmp.mat'],'DWN');  
else
  if j < 100
 save([name,'_0',num2str(j),'tmp.mat'],'DWN');
  else
  save([name,'_',num2str(j),'tmp.mat'],'DWN');    
  end
end