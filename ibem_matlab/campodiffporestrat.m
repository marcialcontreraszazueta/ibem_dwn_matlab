function MecElem = campodiffporestrat(tipo,rad,ct,st,ksi,kpi,Ci66,fac,DK)
%campodiffporestrat(tipo,Xfs,Yfs,kr,ksi,kpi,Ci,fac,DK)
% r       = sqrt(Xfs^2+Yfs^2);
% J0      = besselj(0,kr*r);
% J1      = besselj(1,kr*r);
% dJ1     = kr.*J0-J1/r;%kr.*(J0-J1./(kr*r));
% d2J1    =-kr/r.*J0+J1.*(2/r^2-kr.^2);
% dJ0     =-kr.*J1;
% d2J0    =-kr.*dJ1;
% if r<1e-6
%   ct  = 1/sqrt(2);
%   st  = 1/sqrt(2);
% else
%   ct  = Xfs/r;
%   st  = Yfs/r;
% end
r  = rad.r;
J0 = rad.J0;
J1 = rad.J1;
dJ1  = rad.dJ1;
d2J1 = rad.d2J1;
dJ0  = rad.dJ0;
d2J0 = rad.d2J0; 
if tipo == 1
MecElem = complex(zeros(3,3));
else
MecElem = complex(zeros(3,3,3));
end
if tipo == 1 % Desplazamientos
%   MecElem = complex(zeros(3,3)); % Primer �ndice [fx,fy,fz], segundo el componente
  % fx %
  Urx = dJ1 .*fac(1,:)+J1/r.*fac(2,:); % dJ1 .*facUPSV+J1/r.*facUSH;
  Utx =-J1/r.*fac(1,:)-dJ1 .*fac(2,:); %-J1/r.*facUPSV-dJ1 .*facUSH;
  Uzx =   J1.*fac(3,:);                % J1.*facUz;
  
  Urx(isnan(Urx))=0;
  Utx(isnan(Utx))=0;
  Uzx(isnan(Uzx))=0;
  
  Urx	= DK*sum(Urx); %sum(DK.*Urx); % DK cte
  Utx = DK*sum(Utx); %sum(DK.*Utx);
  Uzx = DK*sum(Uzx); %sum(DK.*Uzx);
  
  %UXW_fx(1,ir,ixs) 
  MecElem(1,1) = (Urx*ct^2-Utx*st^2);          %Uxx
%   UXW_fx(2,ir,ixs) 
  MecElem(1,2) = (Urx+Utx)*ct*st;              %Uyx
%   UXW_fx(3,ir,ixs) 
  MecElem(1,3) = Uzx*ct;                       %Uzx
  
  
%   UXW_fy(1,ir,ixs)
  MecElem(2,1) = (Urx+Utx)*ct*st;              %Uxy=Uyx, G12=G21
%   UXW_fy(2,ir,ixs) 
  MecElem(2,2) = (Urx*st^2-Utx*ct^2);          %Uyy=Uxx, th= -(pi/2-th), ct->st,st>-ct
%   UXW_fy(3,ir,ixs) 
  MecElem(2,3) =  Uzx*st;
  
  % fz %
  Urz = dJ0.*fac(4,:); %farUrz
  Uzz = J0 .*fac(5,:); %facUzz
  
  Urz(isnan(Urz))=0;
  Uzz(isnan(Uzz))=0;
  
  Urz	= DK*sum(Urz);
  Uzz = DK*sum(Uzz);
  
%   UXW_fz(1,ir,ixs) 
  MecElem(3,1) = Urz*ct;                   %UxzW
%   UXW_fz(2,ir,ixs) 
  MecElem(3,2) = Urz*st;                   %UyzW
%   UXW_fz(3,ir,ixs) 
  MecElem(3,3) = Uzz;                      %UzzW
end
if tipo == -1 % Esfuerzos
%   MecElem = complex(zeros(3,3,3));% Primer �ndice [fx,fy,fz], segundo y tercero tensor
  % fx *
  Szrx = dJ1 .*fac(1,:) + J1/r.*fac(2,:);
  Sztx = J1/r.*fac(1,:) + dJ1 .*fac(2,:);
  Szzx = J1  .*fac(3,:);
  Srrx = 2*((d2J1-(ksi^2/2-kpi^2)*J1)         .*fac(4,:) + d2J1          .*fac(5,:) + (dJ1/r-J1/r^2).*fac(6,:));
  Sttx = 2*( (-(ksi^2/2-kpi^2+1/r^2)*J1+dJ1/r).*fac(4,:) + (dJ1/r-J1/r^2).*fac(5,:) + (J1/r^2-dJ1/r).*fac(6,:));
  Srtx = 2*(dJ1/r-J1/r^2).*(fac(4,:)+fac(5,:))                               + (d2J1-dJ1/r+J1/(r^2)).*fac(6,:);
  
  Szrx(isnan(Szrx))=0;
  Sztx(isnan(Sztx))=0;
  Szzx(isnan(Szzx))=0;
  Srrx(isnan(Srrx))=0;
  Sttx(isnan(Sttx))=0;
  Srtx(isnan(Srtx))=0;
  
  Szrx = DK*sum(Szrx);
  Sztx = DK*sum(Sztx);
  Szzx = DK*sum(Szzx);
  Srrx = DK*sum(Srrx);
  Sttx = DK*sum(Sttx);
  Srtx = DK*sum(Srtx);
  
  % Sp=[Srrx Srtx Srzx; Strx Sttx Stzx; Szrx Sztx Szzx];
  Sp=[ct*Srrx -st*Srtx ct*Szrx; -st*Srtx ct*Sttx -st*Sztx; ct*Szrx -st*Sztx ct*Szzx];
  T =[ ct    -st    0  ;   st    ct    0  ;   0     0     1  ];
  Sc= T*Sp*(T.');
  %Sc=[SxxxW SxyxW SxzxW; SyxxW SyyxW SyzxW; SzxxW SzyxW SzzxW];
%   SXW_fx(:,:,ir,ixs) 
%   MecElem(1,:,:) = Ci66*Sc;
  for i=1:3
    for j=1:3
      MecElem(1,i,j) = Ci66(1,1)*Sc(i,j);
    end
  end
  
  % theta de la fuerza solamente: th= -(pi/2-th), ct->st,st>-ct
  Sp=[st*Srrx ct*Srtx st*Szrx; ct*Srtx st*Sttx ct*Sztx; st*Szrx ct*Sztx st*Szzx];
  T =[ ct    -st    0  ;   st    ct    0  ;   0     0     1  ];
  Sc= T*Sp*(T.');
%   SXW_fy(:,:,ir,ixs) 
%   MecElem(2,:,:) = Ci66*Sc;
  for i=1:3
    for j=1:3
      MecElem(2,i,j) = Ci66(1,1)*Sc(i,j);
    end
  end
  % fz %
  Szzz = J0 .*fac(8,:); %facSzz
  Szrz = dJ0.*fac(7,:); %facSzr
  Sztz = 0*Szrz;
  Srtz = 0*Szrz;
  Srrz = 2*( (d2J0-(ksi^2/2-kpi^2)*J0)  .*fac(9,:) + d2J0   .*fac(10,:)); %facSrrP,facSrrSV
  Sttz = 2*( (-(ksi^2/2-kpi^2)*J0+dJ0/r).*fac(9,:) + (dJ0/r).*fac(10,:)); %facSrrP,facSrrSV
  
  Szrz(isnan(Szrz))=0;
  Sztz(isnan(Sztz))=0;
  Szzz(isnan(Szzz))=0;
  Srrz(isnan(Srrz))=0;
  Sttz(isnan(Sttz))=0;
  Srtz(isnan(Srtz))=0;
  
  Szrz = DK*sum(Szrz);
  Sztz = DK*sum(Sztz);
  Szzz = DK*sum(Szzz);
  Srrz = DK*sum(Srrz);
  Sttz = DK*sum(Sttz);
  Srtz = DK*sum(Srtz);
  
  %Sp=[Srrz Srtz Srzz; Strz Sttz Stzz; Szrz Sztz Szzz];
  Sp=[Srrz Srtz Szrz; Srtz Sttz Sztz; Szrz Sztz Szzz];
  T =[ ct   -st   0 ;   st   ct   0 ;   0    0    1 ];
  Sc= T*Sp*T.'; % a coordendas cartesianas
  
%   SXW_fz(:,:,ir,ixs) 
%   MecElem(3,:,:) = Ci66*Sc;
  for i=1:3
    for j=1:3
      MecElem(3,i,j) = Ci66(1,1)*Sc(i,j);
    end
  end
end
end