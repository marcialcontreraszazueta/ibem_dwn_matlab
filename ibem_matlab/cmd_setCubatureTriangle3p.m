% D. A. Dunavant, 
% High degree efficient symmetrical Gaussian quadrature rules for the triangle, 
% Int. J. Num. Meth. Engng, 21(1985):1129-1148.

 % n = 2
para.cubature ...
   = [0.16666666666667 0.16666666666667 0.33333333333333 ;
      0.16666666666667 0.66666666666667 0.33333333333333 ;
      0.66666666666667 0.16666666666667 0.33333333333333];
    
para.gaussian.ngau  = size(para.cubature,1);