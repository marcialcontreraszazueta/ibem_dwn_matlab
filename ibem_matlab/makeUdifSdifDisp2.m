function [Udi,Sdi] = makeUdifSdifDisp2(nbeq,coordphi,CubWts,pcX,...
            DWN,pT,im,leXiz_uni,zrref,Xiz_uni,fracVert)
        
        aux = struct('matrix',complex(zeros(DWN.Udiffsize)));
        Udiffc = repmat(struct('U',aux),1,leXiz_uni);
               
        parfor iXiz_uni = 1:leXiz_uni % para cada profundidad de las fuentes

          % Indices globales de las fuentes a esta profundidad:
          j = find(zrref==iXiz_uni);
          
          Xiz = Xiz_uni(iXiz_uni);
          [Udiffc(iXiz_uni).U.matrix,~] = makeUdifSdifDisp3(...
            nbeq,coordphi,CubWts,pcX,DWN,pT,im,j,Xiz,fracVert);
        end
        
        Udi = complex(zeros(DWN.Udiffsize));
        Sdi = 0;
        for iXiz_uni = 1:leXiz_uni % juntarlos
          Udi=Udi + Udiffc(iXiz_uni).U.matrix;
        end
end