function dispwithnamesize(varargin)
% DISPWITHNAME(X) -- Display scalar variable(s) in argument list with name(s)
for i=1:nargin
  disp([inputname(i) '= [' num2str(size(varargin{i})) '] ' class(varargin{i})])
end