function [oututc] = rmsDecimate(ut,dt,d1sec)
% root mean square sin traslape. Decima la senal cada d1sec muestras.
oututc = ut.*0;
TT = (dt*d1sec);
for ii = 1:d1sec:size(ut,1)-d1sec
    oututc(ii) = (sum(ut(ii:ii+d1sec-1).^2)*dt/TT)^0.5;
end
end