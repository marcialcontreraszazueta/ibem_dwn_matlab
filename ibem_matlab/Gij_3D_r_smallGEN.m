function Gij=Gij_3D_r_smallGEN(coord,xr,jjpb,ks,kp,para,C,transpose)
% FunciOn de Green de DESPLAZAMIENTO en el campo cercano
% [(fx fy fz) x (u v w)]
% Trata el calculo de la contribution de un segmento centrado en xj,yj,zj
% sobre un punto de colocacion xi,yi,zi cual esta mas cercano que dr a xj,yj,zj
% La cubatura se hace sobre un elemento triangular.
%
% coord   :  coordenas de putnos de colucaciOn / y fuentes virtuales
% xr yr zr:  punto o element (de surface) sur lequel se calcul la contribution de toutes
% ii      :  indice de l element (de surface) sur lequel se calcul la contribution de toutes
% jjpb    :  les autres sources virtuelles j (ponctuelle)
% ksi
% kpi

% #ij     :  Las coordenadas de los puntos de integraciOn ya fueron calculadas en
%            la funciOn malla_geom3Dgen.m con la funciOn XYZ_FromBarycentric_Triang.m

nj      = length(jjpb);   % cantidad de fuentes virtuales
Gij     = zeros(3,3,nj);        % El resultado

% El receptor ya estA en xr, yr, zr
% xr(1,1) = coord.x(ii);
% xr(2,1) = coord.y(ii);
% xr(3,1) = coord.z(ii);

% Para cada fuente virtual se integre de todos los puntos
for iXi = 1:nj
  % coordenadas y pesos de los putnos de la cubatura
%   if ex
%   ngau    = para.gaussian.ngauex;% cantidad de puntos gaussianos por fuente
%   CubPts = coord.CubPtsex(1:3,1:ngau,jjpb(iXi));
%   CubWts = para.cubatureex(1:ngau,3);
%   else
  ngau    = para.gaussian.ngau;% cantidad de puntos gaussianos por fuente
  CubPts = coord.CubPts(1:3,1:ngau,jjpb(iXi));
  
  %% testme
% figure(2654321);hold on;plot3(CubPts(1,:),CubPts(2,:),CubPts(3,:),'g*');
  %%
  CubWts = para.cubature(1:ngau,3);
%   end
  % cosenos directores entre el receptor y las fuentes
  xij = xr(1)-CubPts(1,:);
  yij = xr(2)-CubPts(2,:);
  zij = xr(3)-CubPts(3,:);
  rij     = sqrt(xij.^2+yij.^2+zij.^2);
  g = zeros(3,ngau);
  g(1,:)  = xij./rij;
  g(2,:)  = yij./rij;
  g(3,:)  = zij./rij;
  
  %% FunciOn de Green fuente puntual:
  % grij [(fx fy fz) x (u v w)]
  grij    = Gij_3D(ks,kp,rij,g,C,length(rij));
  indpb   = (rij<=1E-15);
  if sum(indpb) > 0
    disp(xr)
    warning('receptor o punto de colocacion muy cerca de punto de cubatura')
  end
  grij(:,:,indpb) =0;
  % suma de contribuciones de todos los puntos Gaussianos
  if nj~=1
    for i=1:3
      for j=i:3
        Gij(i,j,iXi)	= sum(CubWts.*squeeze(grij(i,j,:)),1);
        Gij(j,i,iXi)	= Gij(i,j,iXi);
      end
    end
  else
    for i=1:3
      for j=i:3
        Gij(i,j,iXi)	= sum(CubWts.*squeeze(grij(i,j,:)));
        Gij(j,i,iXi)	= Gij(i,j,iXi);
      end
    end
  end
  if transpose
    Gij(1:3,1:3,iXi) = squeeze(Gij(1:3,1:3,iXi)).';
  end
end
end