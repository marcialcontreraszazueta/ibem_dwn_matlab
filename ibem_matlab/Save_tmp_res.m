function Save_tmp_res(~,uw,j) % %#ok<INUSD>

% name = ['../out/' para.nametmp(1:end-4)];
name = '../out/tmp';
if j < 10
   save([name,'_00',num2str(j),'tmp.mat'],'uw');
else
  if j < 100
    save([name,'_0',num2str(j),'tmp.mat'],'uw');
  else
     save([name,'_',num2str(j),'tmp.mat'],'uw');
  end
end
% disp(['saved uw in: ' name,'_',num2str(j),'tmp.mat'])
