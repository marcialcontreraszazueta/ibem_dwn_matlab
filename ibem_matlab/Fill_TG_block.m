function BBout = Fill_TG_block(BB,pc,pT,mat,CubWts,str,GraficarCadaDiscretizacion)
% Fill_TG_block : Comanda el llenado de una bloque de funciones de Green

% Seleccionar valores para el bloque:
rens = BB.ri   : BB.rf;
cols = BB.ci   : BB.cf;

pcX = pc.X(:,rens);
pcVn = pc.Vn(:,rens);
pcXim = pc.Xim(:,rens);
m = BB.m; % medio en el que se resuelve G o T
im0 = pc.im0(:,rens); % minimo indice a cada lado de la superficie
pTXi = pT.Xi(:,cols);
pTdA = pT.dA(:,cols);
pTcubPts = pT.CubPts(:,:,cols);

% testme
if GraficarCadaDiscretizacion
  figure;clf;hold on
  set(gca,'Zdir','reverse');axis equal; grid on; view(3)
  plot3(pcX(1,:),pcX(2,:),pcX(3,:),'r.')
  plot3(pTXi(1,:),pTXi(2,:),pTXi(3,:),'ko')
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if mat{m}.tipoMed == 1 %Medio homogEneo
  Ci = mat{m}.Ci;
  ksi = mat{m}.ksi;
  kpi = mat{m}.kpi;
  
  if strcmp(BB.tipo,'T')
    if GraficarCadaDiscretizacion; disp([str ' calcBloc_T ']); end
    Ab = calcBlock(-1,... % tracciones
    length(rens),pcX,pcVn,pcXim,m,im0,...
    length(cols),pTXi,pTdA,pTcubPts,...
    BB.isSelfReciproc,0,...
    Ci,ksi,kpi,CubWts);
  elseif strcmp(BB.tipo,'G')
    if GraficarCadaDiscretizacion; disp([str ' calcBlock_G ']); end
    Ab = calcBlock(1,... % desplazamientos
      length(rens),pcX,pcVn,pcXim,m,im0,...
      length(cols),pTXi,pTdA,pTcubPts,...
      BB.isSelfReciproc,0,...
      Ci,ksi,kpi,CubWts);
  end
  AbG = 0;
else                   %Medio estratificado
  pcX = findlayer(pcX,BB.DWN.ncapas,BB.DWN.z);
  pTXi = findlayer(pTXi,BB.DWN.ncapas,BB.DWN.z);

%   if ~(exist('calcBlockDWNTGdisp2_mex.mexa64','file')==3) % <- la mac
    % Calculo en hasta 12 procesadores de matlab
    if GraficarCadaDiscretizacion; disp([str ' calcBlockDWNTGdisp ']); end
    [Ab,AbG] = calcBlockDWNTGdisp(...
      length(rens),pcX,pcVn,pcXim,m,im0,...
      length(cols),pTXi,pTdA,pTcubPts,...
      BB.isSelfReciproc,BB.relatedG,...
      CubWts,BB.DWN);
%   else
%     % Calculo hasta N procesadores en proceso compilado ".mexa64"
%     disp([str ' calcBlockDWNTGdisp_mex ' num2str(length(Xiz_uni))])
%     [Ab,AbG] = calcBlockDWNTGdisp_mex(...
%       length(rens),pcX,pcVn,pcXim,m,im0,...
%       length(cols),pTXi,pTdA,pTcubPts,...
%       BB.isSelfReciproc,BB.relatedG,...
%       CubWts,BB.DWN);
%   end
end

BBout.Ab = Ab;
BBout.AbG = AbG;
BBout.calcme = true;
BBout.relatedG = BB.relatedG;
BBout.ri = BB.ri; BBout.rf = BB.rf;
BBout.ci = BB.ci; BBout.cf = BB.cf;
BBout.tipo=BB.tipo;
BBout.m=BB.m;
end

