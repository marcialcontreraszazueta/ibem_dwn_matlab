function [RESULT,para]=calculo(para)
% calculo  Obtener respuesta en frecuencia de difraccion de ondas elasticas
% con metodo indirecto de elementos de contorno. Acepta medios con estratos
% Ultima revision: Noviembre 2016

  %% carpeta de trabajo                      
tstart  = tic;
clearfield;
if ~isfield(para,'GraficarCadaDiscretizacion')
para.GraficarCadaDiscretizacion = true;
disp('set para.GraficarCadaDiscretizacion=true')
end
if ~isfield(para,'siDesktop'); para.siDesktop = false; end

para.nomcarpeta = pwd;
if exist('../out','dir')
cd ../out
else
cd ..
mkdir out
cd out
end
para.nomrep = pwd;
cd ../ibem_matlab
nametmp        = namefile(para);
para.nametmp   = nametmp;
name        = nametmp;
para.name   = name;
diary off
diary([para.name(1:end-3),'txt'])
mostrarTablaDeMateriales(para);
  %% constantes                              
  %%%%%%%%%%%%%%%
  
  % abscisas y pesos de integracion gaussiana 
if para.dim==1 % 2D
  ngau        = 21;
  gaussian       = Gauss_Legendre(ngau);
  para.gaussian  = gaussian;
  gaussex=gaussian;
elseif para.dim == 4 % 3D gen
  % variable  para.cubature  por renglones: [coordSimplex1 coordSimplex2 W]
  para.gaussian = [];
  gaussian = 0;
  gaussex = 0;
  % (No importa si incluyen el centroide porque, entonces usaria Greenex)
  % La integral es: I = A * SUM( w_i f(x_i)), donde A es el Area, w_i los
  % pesos y x_i los puntos de cuadratura
%   cmd_setCubatureTriangle1p;
%   cmd_setCubatureTriangle3p;
%   cmd_setCubatureTriangle4p;
  cmd_setCubatureTriangle6p;
%   cmd_setCubatureTriangle7p;
%   cmd_setCubatureTriangle9p;
%   cmd_setCubatureTriangle12p;
%   cmd_setCubatureTriangle16p;
%   cmd_setCubatureTriangle24p;
  
else % 2D, 2.5D y 3D axisimetrico
  ngau        = 21;
  gaussex  	= Gauss_Legendre(ngau);
  para.gaussex= gaussex;
  
  ngau        = 11;
  gaussian       = Gauss_Legendre(ngau);
  para.gaussian  = gaussian;
end

if para.dim ~= 4
  if sum(para.tipoMed==2) > 0 % Alguno Estratificado con DWN
    ngau        = 3;
    gaussDWN    = Gauss_Legendre(ngau);
    para.gDWN   = gaussDWN;
  end
end

para    = normalizacion(para);
sal     = para.sortie;
nf      = para.nf;
para.df = para.fmax/(nf/2);     %paso en frecuencia
df      = para.df;
if isfield(para,'jini')
  jini = para.jini;
else
  jini = 0; 
end
if isfield(para,'jfin')
  jfin = para.jfin;
else
  jfin = nf/2;
end
if isfield(para,'jvec')
  jvec = para.jvec;
else
  jvec = jini:jfin;
end
Pleaseloadprevioustmp = para.loadprevioustmp;
  %% discretizacion inicial de la geometria  
  para.fuenteimagen=0;
  if para.nmed~=1 || (para.nmed==1 && para.geo(1) >= 3) || (para.nmed==1 && para.geo(1)==2) %Si existen contornos
    if para.dim < 4 % 2D,2.5D,3Daxisim
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % dicretizacion de los contornos originales 2D %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      %discretiza los contornos iniciales
      para = malla_fina_init_2(para);
      
      %calcula los verdaderos contornos, buscando los puntos de intersecion,
      %los contornos que se quedan y los que se quitan
      para = recalculate_contornos(para);
      
      % superficie de revolucion:
      if     para.dim==3 && (para.nmed>1 || (para.nmed==1 && para.tipoMed(1)==1))
        % caso considerado a partir del modelo 2D,
        % se ocupa no mas la discretisacion hacia el centro del objeto
        % y se hara una revolucion alrededor del eje z
        para = axi_contornos(para);
      end
    elseif para.dim == 4 % 3Dgeneral
      para = cargarSTLiniciales(para);
      % conectividad de los contornos: para.cont1
      para = getConnectivityStruct(para);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define si usar fuente imagen %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if para.dim < 3 % en 2D
      if para.geo(1)==3 %semi-espacio
        %a priori se puede tomar en cuenta una fuente imagen
        para.fuenteimagen=1;
        if para.cont(1).ruggeo~=1
          %la superficie del semi-espacio tiene regosidad
          para.fuenteimagen=0;
        else
          % si hay estratos infinitos, no hay fuente imagen
          for m=2:para.nmed
            if para.geo(m)>=5 %placa o semiplaca
              para.fuenteimagen=0;
            end
          end
        end
        
        if para.fuente(1)==2 && ( (para.dim==1 && para.pol==2) || para.dim>=3 )
          % para ondas P SV, se considera no mas OP incidentes imagen, no las FP
          para.fuenteimagen=0;
        end
      else
        para.fuenteimagen=0;
      end
%     else
%       if para.fuente == 1
%         para.fuenteimagen=1;
%       end
    end
  else %caso de para.nmed==1 && espacio completo o semiespacio estratificado
    if isfield(para,'cont1')
      para.cont1=[];
    end
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% medio en que esta la fuente             
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %  para.xzs  :  el medio al que pertenece la fuente
  if para.dim < 4 %2D,2.5D,3Daxisim
    if para.fuente==1 %OP
      para.xzs=ones(1,para.ninc)*inclusiontest(0,1e6,para,1);
    elseif para.fuente==2 %FP
      para.xzs=ones(1,para.ninc);%init
      for iinc=1:para.ninc
        para.xzs(iinc)=inclusiontest(para.xs(iinc),para.zs(iinc),para,1);
      end
    end
  else %3Dgen
    if ~isfield(para,'xzs')
      para.xzs=ones(1,para.ninc);
      
      for iinc=1:para.ninc
        if para.fuente==1 %OP
          para.xzs(iinc)=1;
        elseif para.fuente==2 %FP
          para.xzs(iinc)=inclusiontest3G(...
            para.xs(iinc),para.ys(iinc),para.zs(iinc),para);
        end
      end
    else
      % del archivo de datos
    end
        
%     if para.fuente==1 %OP
%       % la onda plana surge del medio de (1) siempre:
%       para.xzs=ones(1,para.ninc);%*inclusiontest3G(0,0,1e6,para);
%     elseif para.fuente==2 %FP
%       if ~isfield(para,'xzs')
%       para.xzs=inclusiontest3G(...
%         para.xs(1:para.ninc),...
%         para.ys(1:para.ninc),...
%         para.zs(1:para.ninc),para);
%       else
%         disp('El medio de la fuente se indico en el archivo de datos')
%       end
%     end

    for iinc=1:para.ninc
      disp(['F(',num2str(iinc),') at m=',num2str(para.xzs(iinc))])
    end
  end
  %%%%%%%%%%%%%%%%%%%%%%%%
  %% modos de dispersion                     
  %%%%%%%%%%%%%%%%%%%%%%%%
  if para.aLoMejorSiChecarLasCurvasDeDispersion
    if sum(para.tipoMed==2)>0 && para.fuente==1 && para.reg(1).nsubmed>1 && ...
        (para.dim==1 && ((max(para.tipo_onda==2)==1 && para.pol==1) || ...
        (max(para.tipo_onda==3)==1 && para.pol==2))) || ...
        (para.dim>=3 && (max(para.tipo_onda==4)==1 || max(para.tipo_onda==5)==1))
      if para.siDesktop
        disp('Calculo de las curvas de dispersion');
      end
      
      %         [~,~,k2,w0,ikmax]	= dispersion_curve_kfix_MG_fast2(para);
      jj               = 0:nf/2;
      wi              = 2*pi*(jj*df + 0.01*df*(jj==0));
      %         tic;[vg,f1,ikmax2]   = dispersion_curve_wfix_Haskel_4inv_adapt(para,wi);toc
      [vg,~,f1,ikmax,kx]=dispersion_curve_VP_wfix_Haskel_4inv_u2d2(para,wi);
      
      para.mode.w0    = f1*2*pi;
      para.mode.k2    = kx;
      para.mode.ikmax = ikmax;
      para.mode.vg    = vg;
      %         para.mode.f1    = f1;
      %         para.mode.ikmax2= ikmax2;
      para.ninc       = size(w0,2);
    end
    
    if  sum(para.tipoMed == 2)>0 && para.fuente==2 && para.meth_PS==1 && para.reg(1).nsubmed>1
      %calcul des courbes de dispersion en vue du calcul des IMGIJ dans
      %un multicouche par la methode des correlations
      if para.siDesktop
        disp('Calculo de las curvas de dispersion');
      end
      
      jj               = 0:nf/2;
      wi              = 2*pi*(jj*df + 0.01*df*(jj==0));
      %         tic
      %         [vg,~,f1,ikmax,kx]=dispersion_curve_VP_wfix_Haskel_4inv_u2d3(para,wi);
      %         toc
      %     tic
      [vg,~,f1,ikmax,kx]=dispersion_curve_VP_wfix_Haskel_4inv_d2u3(para,wi);
      %     toc
      %se suprima el ultimo modo si tiene no mas un punto
      indpb           = find(ikmax==1);
      kx(indpb,:)     = [];
      ikmax(indpb,:)  = [];
      vg(indpb,:)     = [];
      f1(indpb,:)     = [];
      %se guarda los resultados en para.mode
      para.mode.k2    = kx;
      para.mode.ikmax = ikmax;
      para.mode.vg    = vg;
      para.mode.f1    = f1;
      
      %         %numero initial de ondas planas para la convergencia
      %         para.nincBWOP   = 200;
    end
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% posicion de receptores                  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
  para = pos_rec(para);
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% init variables de salida                
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if para.dim==1 %2D
    if para.pol==1 %SH
      ns  = sal.Ut+sal.USh+sal.UIh+sal.USt;
      uw  = zeros(nf/2+1,para.rec.nrec,para.ninc,ns);
      nss = sal.sxy+sal.syz;
      inds= [(sal.sxy==1)*1 (sal.syz==1)*2];
      sw  = zeros(nf/2+1,para.rec.nrec,para.ninc,nss);
    elseif para.pol==2 %PSV
      ns  = (sal.Ux + sal.Uz)*(sal.Ut+sal.UPh+sal.USh+sal.UIh+sal.UPt+sal.USt);
      uw  = zeros(nf/2+1,para.rec.nrec,para.ninc,ns);
      nss = sal.sxx+sal.szz+sal.sxz;
      inds= [(sal.sxx==1)*1 (sal.szz==1)*2 (sal.sxz==1)*3];
      sw  = zeros(nf/2+1,para.rec.nrec,para.ninc,nss);
    end
  else %2.5D,3Daxisim,3D
    ns = (sal.Ux + sal.Uy + sal.Uz)*(sal.Ut);
    uw = zeros(nf/2+1,para.rec.nrec,para.ninc,ns);
    nss= sal.sxx + sal.syy + sal.szz+sal.sxy+sal.sxz+sal.syz;
    sw = zeros(nf/2+1,para.rec.nrec,para.ninc,nss);
    %indice de los componentes del tensor de esfuerzos a guardar
    inds = [(sal.sxx==1)*1 (sal.syy==1)*5 (sal.szz==1)*9 (sal.sxy==1)*2 (sal.sxz==1)*3 (sal.syz==1)*6];
  end
  inds(inds==0)   = [];
  
  para.tmax = (para.zeropad)/(para.fmax/(para.nf/2)*para.zeropad);
  para.tmaxinteres = min(para.tmaxinteres,para.tmax);
  %%%%%%%%%%%%%%%%%%%%%%%
  %% init variables DWN                      
  %%%%%%%%%%%%%%%%%%%%%%%
  if sum(para.tipoMed == 2)>0 %Por lo menos un medio es estratificado
    DWN = cell(para.nmed,1);
    for im = 1:para.nmed
      DWN{im}.U0=zeros(3,2,1);
      DWN{im}.S0=zeros(0,2,1);
      if para.tipoMed(im) == 2
        if ((para.nmed~=1) || (para.cont(im,1).NumPieces ~= 0)) %solo con IBEM-DWN 
          % indepedientemente de la frecuencia, se alloca el espacio para el
          % campo de desplazamiento incidente en los receptores
          para.rec.m(im).nr = length(para.rec.m(im).ind); %cantidad de receptores
          
            if para.dim==1
              if para.pol==1 && para.reg(im).nsubmed>1
                DWN{im}.uy0     = zeros(para.rec.m(im).nr,para.ninc);
                DWN{im}.s0      = zeros(nss,para.rec.m(im).nr,para.ninc);
              elseif para.pol==1 && para.reg(im).submed==1
                DWN{im}.no      = 0;
              else%PSV
                DWN{im}.uxz0  	= zeros(2,para.rec.m(im).nr,para.ninc);
                DWN{im}.sxz0    = zeros(nss,para.rec.m(im).nr,para.ninc);
              end
            else
              DWN{im}.U0          = zeros(3,para.rec.m(im).nr,para.ninc);
              DWN{im}.S0          = zeros(nss,para.rec.m(im).nr,para.ninc);
            end
          DWN{im}.inds        = inds;
        elseif para.nmed==1
          DWN{im}.inds        = inds;
        end
        % Paso en k constante (mAs robusto que con paso variable)
        if isfield(para.reg(im),'xl')
          xl = para.reg(im).xl;
        else
          xl = para.DWNxl;
        end
        para.DK(im) = 2*pi/xl;
        DWN{im}.DK = 2*pi/xl;
    disp(['m',num2str(im),' Periodicidad de la fuente =',num2str(xl)])
      else
        para.DK(im) = NaN;
        DWN{im} = [];
      end
    end
    % parte imaginaria de la frecuencia
    % define la atenuaci?n que se imprime en el integrando en k
    % el efecto se corrige en la integraci?n al tiempo.
    para.DWNomei = para.fac_omei_DWN * pi/para.tmaxinteres; % puede ser 2.0 en lugar de 1.0
    DWNomei = para.DWNomei;
  else
    DWN{1}.h=0;
    para.DWNomei = 0;
    DWNomei = 0;
  end
  
    disp(['omega_i = ' num2str(DWNomei)])
    disp(['tmax de interes = ' num2str(para.tmaxinteres)])
    
  %%%%%%%%%%%%%%%%%%%%%%%%%
  %% CICLO DE FRECUENCIAS                    
  %%%%%%%%%%%%%%%%%%%%%%%%%
  
  nmed    =para.nmed;  dim     =para.dim;
  disp(['  ' num2str(nf/2) ' frecuencias @' num2str(df) ' Hz']); disp('')
  % Frecuencia de discretizacion minima:
  fjbrk = (nf/2)/5*df - 1i*DWNomei/2/pi; %<-- empirico 
  
   parfor j=jini:jfin
%   for j=jini:jfin

    % para solo hacer algunos resultados
    if sum(j == jvec) == 1
      % hacerlo
    else
%       disp(['skip ',num2str(j)])
      continue % no hacerlo
    end
    
    fj    = j*df + 0.01*df*(j==0) - 1i*DWNomei/2/pi;
    str = sprintf('[%d/%d %6.3f Hz]',round(j),round(nf/2),fj);
%  % Para frecuencias 'intermedias'. Mejorar el muestreo del espectro:
%     fj          = j*df + df/2 - 1i*DWNomei/2/pi; 

 % Cargar resultados temporales de una corrida anterior:
    if Pleaseloadprevioustmp
    [newuw,next]=loadprevioustmp(j);
    if next
      disp([str '  found'])
      uw(j+1,:,:,:) = newuw;
      continue
    else
      disp([str '<----------------'])
    end
    end
    
    %</ inicio de paratmp  ( cuando se usa parfor en j )
    coord = j;    phi_fv = j;  WFE = j;
    paratmp     = attenuation(para,fj);
    paratmp.j   = j;
    paratmp.fj  = fj;
    DWNtmp = DWN;
    
    % kmax para cada frecuencia y medio:
    if sum(paratmp.tipoMed == 2) > 0 % Si algun medio es estratificado
      minbeta = 100000000000000000000000;
      for imm = 1:nmed
        paratmp.DWNkmax(imm) = NaN;
        if paratmp.tipoMed(imm) == 2 % si este medio es estratificado
          for im = 1:paratmp.reg(imm).nsubmed
            minbeta = min(minbeta,paratmp.reg(imm).sub(im).bet);
          end
          % regla ok:
          paratmp.DWNkmax(imm) = 2*pi*(max(0.25*paratmp.fmax,real(fj))+0.25*paratmp.fmax)/(0.95 * minbeta); 
          
%           paratmp.DWNkmax(imm) = 2*pi*real(fj) / (0.94 * minbeta); 
          
          paratmp.DWNnbptkx(imm) = max(300,fix(paratmp.DWNkmax(imm)/paratmp.DK(imm))+1);
          DWNtmp{imm}.kr = (0.5+(0:paratmp.DWNnbptkx(imm)-1))*paratmp.DK(imm);
          DWNtmp{imm}.dkr = DWNtmp{imm}.kr*0 + paratmp.DK(imm);
          DWNtmp{imm}.omegac  = 2*pi*fj;
          DWNtmp{imm}.m = imm;
          tstr =['[m' num2str(imm) ': kmx' num2str(paratmp.DWNkmax(imm),2) ...
            ' nk' num2str(paratmp.DWNnbptkx(imm)) ']'];
          str = sprintf('%s%s',str,tstr);
        end
      end
    end
    paratmp.str = str;
    
    if nmed==1 && floor(paratmp.tipoMed(1)) == 1 && paratmp.nmedf>1
      %campo incidente solo en espacio completo
      if paratmp.fuente(1)==2 && paratmp.meth_PS==1
        %calculo a partir de las correlaciones y teorema de
        %equiparticion
        [uw(j+1,:,:,:),sw(j+1,:,:,:)]	= Pure_correlation_equipartition(paratmp,fj);
      else
        tic  
        [auw,asw] = campo_inc(paratmp); %disp(paratmp.str)
        els = toc; disp([paratmp.str ' ' num2str(els)]);
        if max(max(max(auw)))~=0;  uw(j+1,:,:,:)=auw; end
        if max(max(max(asw)))~=0;  sw(j+1,:,:,:)=asw; end
%                 clear auw asw
      end
    elseif nmed==1 && paratmp.tipoMed(1) == 2 && paratmp.cont(1,1).NumPieces == 0
      % campo incidente solo en medio estratificado
      if paratmp.fuente(1)==2 && paratmp.meth_PS==1
        %calculo a partir de las correlaciones y teorema de
        %equiparticion
        [uw(j+1,:,:,:),sw(j+1,:,:,:)]	= Pure_correlation_equipartition(paratmp,fj);
      else
        disp([str ' pureDWN']);
        %Pure DWN
        [uw(j+1,:,:,:),~,sw(j+1,:,:,:)]	= Pure_DWN(paratmp,DWNtmp{1},fj);
      end
    else
      %IBEM
      %-------------------------------------%
      % calculo de las densidades de fuerza %
      %-------------------------------------%
      
      % calculo de las densidades de fuerzas "phi" para cada fuente virtual
      % la coordenades de cada fuerza esta dada en coord y estas cambian por
      % cada frecuencia
      if dim==1 %2D
        if para.loadme
          % cargar PHI precalculado, resolver U0, Gij a receptores (nuevos)
          [phi_fv,coord,DWNtmp,WFE] = loadPHI_calcG2d(paratmp,DWNtmp{1},j);
        else
          % calcular PHI, U0, Gij
          [phi_fv,coord,DWNtmp,WFE]  = sol_dfv(paratmp,fj,DWNtmp{1});
          Save_tmp_phi2D(phi_fv,coord,DWNtmp,WFE,j)
        end
      elseif dim==3 %3D axi
        [phi_fv,DWNtmp,paratmp]  = sol3D_dfvAxi(paratmp,fj,DWNtmp{1});
      elseif dim==4 %3D general
        paratmp = cargardelista(paratmp,j); % lista de geometria prescrita
        paratmp = malla_geom3Dgen(paratmp,max(fj,fjbrk));
        paratmp.str = sprintf('%s%s',paratmp.str,['[nbeq' num2str(3*paratmp.coord.nbeq) ']']);
        if paratmp.loadme
          % cargar PHI precalculado, resolver U0, Gij a receptores (nuevos)
          [phi_fv,DWNtmp,paratmp] = loadPHI_calcG(paratmp,DWNtmp,j);
        else
          % calcular PHI, U0, Gij
          [phi_fv,DWNtmp,pT] = sol3D_dfv(paratmp,DWNtmp,j);
          Save_tmp_phi(phi_fv,paratmp,pT,j)
        end
      end
      
      
      %--------------------------%
      % calculo en cada receptor %
      %--------------------------%
      %suma de las contribuciones de cada fuente virtual en cada receptor
      if dim==1
        if paratmp.pol==1
          [uw(j+1,:,:,:),sw(j+1,:,:,:)] 	= inversion_SH_k(paratmp,coord,phi_fv,gaussian,DWNtmp); %uy
        elseif paratmp.pol==2
          [uw(j+1,:,:,:),sw(j+1,:,:,:)]	= inversion_PSV_k(paratmp,coord,phi_fv,gaussian,DWNtmp,WFE); %u(1)=ux, u(2)=uz
        end
      else  % AxisimEtrico y GeometrIa irregular
        [uw(j+1,:,:,:),sw(j+1,:,:,:)] 	= inversion_3D_k(paratmp,phi_fv,gaussex,DWNtmp);
      end
      Save_tmp_res(paratmp,uw(j+1,:,:,:),j)
    end
  
  end % end for
  
  % >/ fin paratmp
  
  uw(isnan(uw))=0;
  sw(isnan(sw))=0;
  %% Guardar resultados en frecuencia        
RESULT.uw=uw;
RESULT.sw=sw;


if isfield(para,'cont1')
  cont1 = para.cont1;
end
if exist('cont1','var')
  para.cont1  = cont1;
else
  cont1=[];
end
save(name,'para','uw','sw','cont1');
disp('Resultados uw sw para (en frecuencia) guardados en archivo');
disp(name);
diary off
  %% sismogramas sinteticos                  
if (jini == 0) && (jfin == para.nf/2)
  if para.spct==0
    [utc,stc]   = inversion_w(uw,sw,para);
    save([name(1:end-3) 't' name(end-3:end)],'para','utc','stc','cont1');
      disp('Guardados sismogramas sintenticos');
  else
    utc         = 0;
    stc         = 0;
  end
else
  utc         = 0;
  stc         = 0;
end
  %% tiempo de computo                       
txttime     = conv_tps(tstart);
% if para.smallscreen
  disp(['calculation time: ',txttime])
  %% salida                                  
RESULT.utc=utc;     clear utc
RESULT.uw=uw;       clear uw
RESULT.stc=stc;     clear stc
RESULT.sw=sw;       clear sw
RESULT.name=name;   clear name
RESULT.cont1=cont1; clear cont1
end