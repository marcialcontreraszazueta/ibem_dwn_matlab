function val = hardcodeGeom10
% Este contorno de descarta siempre, nada mas es para que no
% proteste la funcion que grafica

val = [[-1 0];[1 0]];

% rio de los remedios
% val = [[2 0];
%        [22.5 0]];

% para ejercicio 031
% val =[[2000 50];
%       [2010 55];
%       [3040 55];
%       [3050 0]];
end