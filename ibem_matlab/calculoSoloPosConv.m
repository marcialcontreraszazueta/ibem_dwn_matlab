function [RESULT,para]=calculoSoloPosConv(para)

para.nomcarpeta = pwd;
cd ../out
para.nomrep = pwd;
cd ../ibem_matlab
nametmp        = namefile(para);
para.nametmp   = nametmp;

%% constantes                              
%%%%%%%%%%%%%%%
para    = normalizacion(para);
nf      = para.nf;
para.df = para.fmax/(nf/2);     %paso en frecuencia

if isfield(para,'jini')
  jini = para.jini;
else
  jini = 0; 
end
jfin = nf/2;

%% si solo hay que convolucionar unos espectros
  para0           = para;
  disp('Utilizar uw y sw de RESULT (1) o cargar dede el archivo siguiente (2)')
  disp(para.name)
  inp = input('(1) RESULT, (2) archivo: ');
  if inp == 1
    au = evalin('base','RESULT');
    uw = au.uw;
    sw = au.sw;
  else
    load(para.name,'para','uw','sw');
    uw(isnan(uw))   = 0; %#ok<NODEF>
    if exist('sw','var')
      sw(isnan(sw))   = 0; %#ok<NODEF>
    end
  end
  para.pulso.tipo 	= para0.pulso.tipo ;
  para.pulso.a	= para0.pulso.a;
  para.pulso.b  = para0.pulso.b;
  para.pulso.c  = para0.pulso.c;
  para.spct     = para0.spct;
  para.siDesktop   	= para0.siDesktop;
  
  cd ../out
  [pathstr1,pathstr2,~] = fileparts(pwd);
  cd ../ibem_matlab
  [~,name,ext] = fileparts(para.name);
  nametmp = [pathstr1,pathstr1(1),pathstr2,pathstr1(1),name,ext];
  
%% variables de salida
name        = nametmp;
para.name   = name;

if isfield(para,'cont1')
  cont1 = para.cont1;
end
if exist('cont1','var')
  para.cont1  = cont1;
else
  cont1=[];
end


RESULT.uw=uw;
RESULT.sw=sw;

%%%%%%%%%%%%%%%%
%% inversion w %
%%%%%%%%%%%%%%%%
if jini ~= jfin
  if para.spct==0
    %   save([name,'tmp'],'para','uw','sw');%por si acaso
    
    if para.siDesktop
      waitbarSwitch(0,h,'Inversion de los espectros');
    else
      disp('Inversion de los espectros');
    end
    if para.zeropad < para.nf; para.zeropad = para.nf; end
    [utc,stc]   = inversion_w(uw,sw,para);
    
    if para.siDesktop
      waitbarSwitch(0,h,'Guardando los resultados ...');
    else
      disp('Guardando los resultados ...');
    end
  else
    if para.siDesktop
      waitbarSwitch(0,h,'Guardando los resultados ...');
    else
      disp('Guardando los resultados ...');
    end
    utc         = 0;
    stc         = 0;
  end
  save([name(1:end-3) 't' name(end-3:end)],'para','utc','stc','cont1');
else
  utc         = 0;
  stc         = 0;
end

%% salida
RESULT.utc=utc;     clear utc
RESULT.uw=uw;       clear uw
RESULT.stc=stc;     clear stc
RESULT.sw=sw;       clear sw
RESULT.name=name;   clear name
RESULT.cont1=cont1; clear cont1
end
