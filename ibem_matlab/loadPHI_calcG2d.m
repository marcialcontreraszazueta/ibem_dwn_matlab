function [phi,coord,DWN,WFE] = loadPHI_calcG2d(para,DWNin,j)
% loadPHI_calcG : cargar PHI resuelto en otra corrida, calcular Gij en
% receptores definidos en paratmpIN (en medios estratificados)
%% cargar PHI
phi=[]; coord=[];
if j < 10
  name = ['../out/loadme/PHI','_00',num2str(j),'tmp.mat'];
else
  if j < 100
    name = ['../out/loadme/PHI','_0',num2str(j),'tmp.mat'];
  else
    name = ['../out/loadme/PHI','_',num2str(j),'tmp.mat'];
  end
end
load(name,'phi','coord','DWN','WFE');  DWNlo = DWN; DWN = DWNin; clear DWNin
fprintf('%s',['loaded ', name,' | calculando funciones de Green...'])

%% calcular Gij de los nuevos
if para.tipoMed(1) == 2 %Por lo menos un medio es estratificado
  % Actualizar la lista de receptores con los que se indican en paratmpIN
  para.rec = para.rec;
  sal     = para.sortie;
  nbeq    = coord.nbeq;
  para.nsubmed = para.reg(1).nsubmed;
  WFE = [];
  if para.pol==1
    nbeq2   = nbeq;
  elseif para.pol==2
    %hay una ecuacion para phix y otra para phiz
    nbeq2   = 2*nbeq-coord.nfl;
  end
  coord.nbeq2 = nbeq2;

  %se calcula el vector de valores de la componente horizontal de k
  %         xmax        = para.DX*para.DWNnbptkx/2;
  nk          = para.DWNnbptkx;
  nk0         = nk;
  DK          = 2*para.DWNkmax(1)/(nk0);
  %xmax doit etre > vmax*tmax=alpha/dfe
  k2          = (0:(fix(nk0/2)))*DK;
  k2(1)       = k2(2)/1000;
  DWN.k2      = k2;
  DWN.dk2     = k2*0+DK;
  clear k2
  
  % calculo de la matriz del DWN
  % y inicialisacion del vector de los diffractados
  
  if  para.pol==1
%     DWN = calcul_A_DWN_SH_Ncapas_HS(para,DWN);
    DWN.A_DWN = DWNlo.A_DWN;
    
    ns          = sal.Ut;
    DWN.uydiff  = zeros(nbeq,para.rec.m(1).nr);
    nss         = sal.sxy + sal.syz;
    DWN.sdiff   = zeros(nss,nbeq,para.rec.m(1).nr);
  elseif para.pol==2
%     DWN = calcul_A_DWN_PSV_Ncapas_HS(para,DWN);
    DWN.A_DWN = DWNlo.A_DWN; 
    DWN.k1 = DWNlo.k1; 
    DWN.u1 = DWNlo.u1; 
    DWN.u2 = DWNlo.u2; 
    clear DWNlo
    
    ns         = sal.Ut;
    DWN.uxzdiff=zeros(2,nbeq2,para.rec.m(1).nr);
    nss         = sal.sxx + sal.sxz + sal.szz;
    DWN.szxdiff=zeros(3,nbeq2,para.rec.m(1).nr);
  end
  
  indi            = coord.indm(1,1).ind;%true-false de los puntos perteneciendo a m=1
  zrv                 = coord.z(indi);
  [~,~,zrref]	= pseudo_unique(zrv,para);
  
  %posicion receptor reales
  xrr                 = para.rec.xr(para.rec.m(1).ind).';
  zrr                 = para.rec.zr(para.rec.m(1).ind).';
  nrr                 = length(xrr);
  %reduccion du numero de profundidades de los receptores
  [zrr,izr0,~]        = pseudo_unique(zrr,para);
  %         zrr=zrr.';izr0=(1:length(zrr)).';
  
  %concatenacion de los puntos y de las salidas
  xr                  = xrr;
  zr0                 = zrr;
  
  salu                = ones(nrr,1)*(ns>0);
  sals                = ones(nrr,1)*(nss>0);
  
  DWN.zr0             = zr0;
  DWN.izr0            = izr0;
  DWN.xr              = xr;
  DWN.salu            = salu;
  DWN.sals            = sals;
  
  xrv                 = coord.x(indi);
  nxrv                = length(xrv);
%   nxrv            = ;
  DWN.nxrv = nxrv;
  
  % incidencia de la fuente
  if para.pol==1
    [~,DWN]	= vector_fuente_SH(para,coord,DWN);
  elseif para.pol==2
    [~,DWN]	= vector_fuente_PSV(para,coord,DWN);  
  end
  
  indpt           = 1:coord.nbpt;
  indi            = coord.indm(1,1).ind;%true-false de los puntos perteneciendo a m=1
  indir           = indpt(indi);%indice de estos puntos
  
  for j=indir %ciclo sobre las fuentes virtuales
    if para.pol==2
      coordf.xs   = coord.x(j);
      coordf.zs   = zrref(j==indir);
      coordf.vnx  = coord.vnx(j);
      coordf.vnz  = coord.vnz(j);
      coordf.dr   = coord.dr(j);
      
      [U2,S2,U1,S1] = calcul_US_DWN_PSV_Ncapas_HS(para,xr,zr0,izr0,salu,sals,coordf,[1 1],DWN);%1& 2 oppose ds IBEM & DWN
      
      DWN.uxzdiff(:,coord.phi(j,1),:)         = U1(:,nxrv+1:nxrv+nrr);%(2,nbeq2,para.rec.m(1).nr)
      jj   = 1;
      if sal.sxx; DWN.szxdiff(jj,coord.phi(j,1),:) = S1(1,1,nxrv+1:nxrv+nrr);jj=jj+1; end;
      if sal.szz; DWN.szxdiff(jj,coord.phi(j,1),:) = S1(2,2,nxrv+1:nxrv+nrr);jj=jj+1; end;
      if sal.sxz; DWN.szxdiff(jj,coord.phi(j,1),:) = S1(1,2,nxrv+1:nxrv+nrr);         end;
      
      DWN.uxzdiff(:,coord.phi(j,1)+nbeq,:)    = U2(:,nxrv+1:nxrv+nrr);
      jj   = 1;
      if sal.sxx; DWN.szxdiff(jj,coord.phi(j,1)+nbeq,:) = S2(1,1,nxrv+1:nxrv+nrr);jj=jj+1; end;
      if sal.szz; DWN.szxdiff(jj,coord.phi(j,1)+nbeq,:) = S2(2,2,nxrv+1:nxrv+nrr);jj=jj+1; end;
      if sal.sxz; DWN.szxdiff(jj,coord.phi(j,1)+nbeq,:) = S2(1,2,nxrv+1:nxrv+nrr);         end;
    else
      error('falta SH')
    end
  end
end

fprintf('%s','done')
% save('DWNtmp.mat','phi','DWNtmp','para')
end