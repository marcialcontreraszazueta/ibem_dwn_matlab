function [T] = Tij_fromMecElem(MecElem,vnx)
% Tij_fromMecElem: Esfuerzos o Tracciones
% En MecElem est�n los tres tensores de esfuerzo
% (3x3x3). Primer Indice fx,fy,fz. 2o y 3o el tensor en
                % cartesianas. | sxx sxy sxz |
                %              | sxy syy syz |
                %              | sxz syz szz |

if nargin == 1
  % Todos los esfuerzos
  T = complex(zeros(9,3,1,1));
  T(:,1,1,1) = reshape(MecElem(1,:,:),1,9);
  T(:,2,1,1) = reshape(MecElem(2,:,:),1,9);
  T(:,3,1,1) = reshape(MecElem(3,:,:),1,9);
  return
end
T   = complex(zeros(3,3,1,1));
for dirf = 1:3 % para cada direcci�n de la fuerza aplicada
    for i = 1:3
    T(i,dirf,1,1) = MecElem(dirf,i,1)*vnx(1)+...
                    MecElem(dirf,i,2)*vnx(2)+...
                    MecElem(dirf,i,3)*vnx(3);
    end
end
end