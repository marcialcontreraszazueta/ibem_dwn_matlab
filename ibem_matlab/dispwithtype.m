function dispwithtype(varargin)
% DISPWITHNAME(X) -- Display scalar variable(s) in argument list with name(s)
for i=1:nargin
  disp([inputname(i) ' ' class(varargin{i})])
end