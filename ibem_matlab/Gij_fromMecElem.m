function G = Gij_fromMecElem(MecElem,doRe)
G = complex(zeros(3,3,1,1));
if ~doRe
  % G = MecElem;
  for i=1:3
    for j=1:3
      G(i,j,1,1) = MecElem(i,j);
    end
  end
else
  % GR_ij(xi,x) = G_ji(x,xi)
  %   GR = MecElem.';
  G(1,1,1,1) = MecElem(1,1);
  G(2,2,1,1) = MecElem(2,2);
  G(3,3,1,1) = MecElem(3,3);
  G(1,2,1,1) = MecElem(2,1);
  G(1,3,1,1) = MecElem(3,1);
  G(2,3,1,1) = MecElem(3,2);
  G(2,1,1,1) = MecElem(1,2);
  G(3,1,1,1) = MecElem(1,3);
  G(3,2,1,1) = MecElem(2,3);
end
end
%  MecElem:
% (3x3). Primer Indice fx,fy,fz. Segundo u,v,w

