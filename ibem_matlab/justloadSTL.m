function [out,flag] = justloadSTL(cont)
% Adaptado de CAD2MATDEMO de Don Riley (c) 2003
out = []; flag = 0; 

if cont.isalist
  v = [fileparts(pwd),'/ins/',cont.stage(end).fileName];
  if iscell(v)
    nombreCompleto = v{1};
  else
    nombreCompleto = v;
  end
else
  if strcmp(cont.fileName(1),'/')
    nombreCompleto = cont.fileName;
  else
  [~,a2,a3]=fileparts(cont.fileName);
  v = [fileparts(pwd),'/ins/',a2,a3];
  if iscell(v)
    nombreCompleto = v{1};
  else
    nombreCompleto = v;
  end
  end
end

% disp(['loading: ' nombreCompleto]);
[F,V,N,C,T,A,name,errmsg] = rndread(nombreCompleto);

% algunos STL estan bien chafas y tienen caras ficticias con normal (0,0,0)
aN = N(1,:)+N(2,:)+N(3,:);
rmvMe = find(abs(aN)<0.01);
C(:,rmvMe) = [];
T(:,:,rmvMe) = [];
A(rmvMe) = [];
N(:,rmvMe) = [];

% disp(['cant de puntos de colocaci?n: ' num2str(length(C))])
if ~strcmp(errmsg,''); return; end
out.fileName = nombreCompleto;
out.name = name;
out.centers = C;
out.triangles = T;
out.areas = A;
out.F = F;
out.V = V;
out.N = N';

flag = 1;
end
