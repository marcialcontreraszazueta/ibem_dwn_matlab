function [RESULT,b_dib]=callbackdessin(para,nomcarpeta,bouton)

cd(para.nomrep)
[FileName,Pathname]=uigetfile('*.mat','archivo de datos y salida','MultiSelect','on');
if isequal(Pathname,0) || isequal(FileName,0)
    errordlg('entra un archivo de tipo configparatmp.mat','info');
else
  if iscell(FileName)
    for i=1:length(FileName)
    name=[Pathname,FileName{i}];
    load(name);
    end
  else
    name=[Pathname,FileName];
    load(name);
  end
end
b_dib(1).name=name;
para.redraw=1;

cd(nomcarpeta)
if ~exist('utc','var'); utc=0;end
if ~exist('stc','var'); stc=zeros(4,1);end
if ~exist('uw','var'); uw=0;end
if ~exist('sw','var'); sw=zeros(4,1);end

RESULT.utc=utc;
RESULT.uw=uw;
RESULT.stc=stc;
RESULT.sw=sw;
RESULT.name=name;
RESULT.cont1=cont1;

dibujo(para,bouton,RESULT);

end
