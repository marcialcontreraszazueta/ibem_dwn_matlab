function [DWN] = makeUdifSdifDisp(para,pT,DWN)
% makeUdifSdifDisp calcula funciones de Green (p. coloc. - receptor)

nbeq = para.coord.nbeq;
ngau = length(para.cubature(:,1));
CubWts = para.cubature(1:ngau,3);
fracVert = 60; %  0   1/#  agrupar posiciones verticales en campo diff.

%agregar Vn
j = 1:para.coord.nbpt;
pT.Vn = zeros(3,nbeq);
for i=j;
  im  = find(para.coord.Xim(i,:)~=0);
  pT.Vn(:,para.coord.phi(i,im(1))) = [para.coord.vnx(i);para.coord.vny(i);para.coord.vnz(i)];
end

for im = 1:para.nmed
  if para.tipoMed(im) == 2 % si este medio es estratificado
    cols = pT.m(1,:) == im;       if isempty(cols); continue;end
    rens = para.rec.m(im).ind;    if isempty(rens); continue;end
    
    pTXi = pT.Xi(:,cols);
    pTXi = findlayer(pTXi,DWN{im}.ncapas,DWN{im}.z);
    % Reconocer las fuentes a profundiades iguales:
    [Xiz_uni,~,zrref] = psedoUnique(DWN{im}.sub,...
      DWN{im}.z,DWN{im}.h,pTXi(3,:),pTXi(4,:),DWN{im}.ncapas,fracVert);
    
    pcX = zeros(4,length(rens));
    pcX(3,:) = para.rec.zr(rens);
    pcX(2,:) = para.rec.yr(rens);
    pcX(1,:) = para.rec.xr(rens);
    pcX  = findlayer(pcX ,DWN{im}.ncapas,DWN{im}.z);
    
    % repartir las fuentes
    leXiz_uni = length(Xiz_uni);
    coordphi = para.coord.phi(:,im); % indice del phi correspondiente a la fuente
    coordphi(coordphi==0)=[];
    thisDWNim = DWN{im};
    if ~(exist('makeUdifSdifDisp2_mex.mexa64','file')==3)
      % se reparten los procesos en matlab
      %         disp([para.str ' makeUdifSdifDisp2'])
      [compUdiff1,~] = makeUdifSdifDisp2(nbeq,coordphi,CubWts,pcX,...
        thisDWNim,pT,im,leXiz_uni,zrref,Xiz_uni,fracVert);
      DWN{im}.Udiff = compUdiff1;
      DWN{im}.Sdiff = 0;
    else
      % funcion compilada
      disp([para.str ' makeUdifSdifDisp2_mex'])
      [compUdiff2,~] = makeUdifSdifDisp2_mex(nbeq,coordphi,CubWts,pcX,...
        thisDWNim,pT,im,leXiz_uni,zrref,Xiz_uni);
      DWN{im}.Udiff = compUdiff2;
      DWN{im}.Sdiff = 0;
    end
  end
end

end