% D. A. Dunavant, 
% High degree efficient symmetrical Gaussian quadrature rules for the triangle, 
% Int. J. Num. Meth. Engng, 21(1985):1129-1148.

 % n = 1
para.cubature ...
   = [0.33333333333333 0.33333333333333 1.00000000000000];

para.gaussian.ngau  = size(para.cubature,1);