function para = getConnectivityStruct(para)
% Hacer struct de conectividades y geometr?a inicial
% Se devuelve cont1() con los elementos por cada contorno:
%     .m   :  El medio al que pertenece el contorno
%     .mv  :  El medio con el que hay continuidad de desp. y trac.
%     .vec :  Estructura con los siguientes elementos:
%         .xc  .yc  .zc   : componentes centro de elemento
%         .vnx .vny .vnz  : componentes normal orientada hacia -z
%                           Las normales se reorientan pero el orden de los
%                           v?rtices no se actualiza (aguas con crossprod!)
%         .cv             : [1 ? 2] si es contorno de arriba o abajo
% (no)    .r              : logitud rectificada acumulada (2D)
%         .radio          : c?rculo estimado del ?rea calculada
%         .minVel         : la menor de las velocidades a cada lado del punto
n = 0;
for m = 1:para.nmed  % 1 ?
  n = n + para.cont(m,1).NumPieces;
end
cont1 = struct('m', zeros(1,n),...
  'mv', zeros(1,n),...
  'vec', [],...
  'radio', zeros(1,n),...
  'minVel', zeros(1,n));
i = 1;
hacerlo = false;
for m = 1:para.nmed
  for p = 1:para.cont(m,1).NumPieces
    if (size(para.cont(m,1).piece{p}.fileName,2)>1) % se carg? algo v?lido
      if size(para.cont(m,1).piece{p}.geoFileData,2)>0 % y hay datos
        kind = para.cont(m,1).piece{p}.kind;
        if kind ~= 3 % si no es una frontera auxiliar
          hacerlo = true;
          cont1(i).m = m; %medio al que pertenece
          cont1(i).vec.mv = para.cont(m,1).piece{p}.continuosTo;
          % por cada cara (tri?ngulo)
          cont1(i).vec.xc = squeeze(para.cont(m,1).piece{p}.geoFileData.centers(1,:));
          cont1(i).vec.yc = squeeze(para.cont(m,1).piece{p}.geoFileData.centers(2,:));
          cont1(i).vec.zc = squeeze(para.cont(m,1).piece{p}.geoFileData.centers(3,:));
          cont1(i).vec.vnx = squeeze(para.cont(m,1).piece{p}.geoFileData.N(:,1));
          cont1(i).vec.vny = squeeze(para.cont(m,1).piece{p}.geoFileData.N(:,2));
          cont1(i).vec.vnz = squeeze(para.cont(m,1).piece{p}.geoFileData.N(:,3));
          range = cont1(i).vec.vnz > 0;
          cont1(i).vec.cv = ones(size(cont1(i).vec.xc)); % es contorno de arriba
          
          % si es una frontera de continuidad
%           if kind == 2
%           % por convenci?n en multi-ibem, las normales apuntan a -z
%           % invertir la normal
          cont1(i).vec.cv(range) = 2; % es contorno de abajo
          cont1(i).vec.vnx(range) = -cont1(i).vec.vnx(range);
          cont1(i).vec.vny(range) = -cont1(i).vec.vny(range);
          cont1(i).vec.vnz(range) = -cont1(i).vec.vnz(range);
%           end
          
          cont1(i).radio = sqrt(para.cont(m,1).piece{p}.geoFileData.areas(:,1)/pi);
          cantTriangs = length(cont1(i).radio);
          disp(['[m' num2str(m) ' p' num2str(p) '] ' num2str(cantTriangs) ' triangulos'])
          % la velocidad m?s baja de cada lado del contorno en el punto
          indm    = [cont1(i).m;cont1(i).vec.mv]; % los dos medios involucrados
          indm(indm==0)   = [];
          indm    = squeeze(indm);
          
          % La menor velocidad de propagaciOn a cada lado de cada triAngulo
          % de la pieza
          minVel = zeros(cantTriangs,length(indm));
          v       = 0*indm; %% La menor velocidad a cada lado del punto de colocaciOn:
          for k=1:length(indm)
            if floor(para.tipoMed(indm(k))) == 1  % medio homogeneo
              if para.reg(indm(k)).rho==0 % si esta hueco
                v(k) = 0;
              else
                v(k) = para.reg(indm(k)).bet;
              end
              minVel(:,k)= v(k);
            else  % estratificado
              para.reg(indm(k)).bet=12345;
              para.reg(indm(k)).rho=12345;
              % asignar estrato a cada triangulo
              
              % hipotesis: todos estAn en el semiespacio
              layer = zeros(cantTriangs,1);
              layer(1:cantTriangs) = para.reg(indm(k)).nsubmed; 
              z = 0; % preguntar si estAn en cada estrato
              for il = 1:para.reg(indm(k)).nsubmed-1
                h = para.reg(indm(k)).sub(il).h;
                % si esta entre z y z+h
                thisL = ((z <= cont1(i).vec.zc) & (cont1(i).vec.zc < (z+h)));
                layer(thisL) = il;
              end
              for it = 1:cantTriangs
                Vel = para.reg(indm(k)).sub(layer(it)).bet;
                if para.reg(indm(k)).sub(layer(it)).rho == 0
                  Vel = 0;
                else
                  if Vel == 0 % si es un medio ac?stico
                    Vel = para.reg(indm(k)).sub(layer(it)).alpha;
                  end
                end
              minVel(it,k) = Vel;
              end
            end   
          end
          
%             if para.reg(indm(k)).rho==0 % si est? hueco
%               v(k) = 0;
%             else
%               if para.tipoMed(indm(k)) == 1
%                 v(k) = para.reg(indm(k)).bet; % medio homog?neo
%               else
%                 minbeta = 100000000000000000000000;
%                 for im = 1:para.reg(indm(k)).nsubmed
%                   minbeta = min(minbeta,para.reg(indm(k)).sub(im).bet);
%                 end
%                 v(k) = minbeta;clear minbeta % estratificado
%               end
%               if v(k)==0 % si es un medio ac?stico
%                 if para.tipoMed(indm(k)) == 1
%                   v(k) = para.reg(indm(k)).alpha; % medio homog?neo
%                 else
%                   minalfa = 100000000000000000000000;
%                 for im = 1:para.reg(indm(k)).nsubmed
%                   minalfa = min(minalfa,para.reg(indm(k)).sub(im).alpha);
%                 end
%                   v(k) = minalfa; clear minalfa % estratificado
%                 end
%               end
%             end
%           end
%           v(v==0) = [];
%           v       = squeeze(v);
%           if isempty(v)
%             error('v isempty')
%           end
%           cont1(i).minVel = min(v);
          cont1(i).minVel = min(minVel,[],2);
          
          % En la estructura de datos original
          D = struct('centers',para.cont(m,1).piece{p}.geoFileData.centers,...
            'triangles',para.cont(m,1).piece{p}.geoFileData.triangles,...
            'areas',para.cont(m,1).piece{p}.geoFileData.areas,...
            'N',para.cont(m,1).piece{p}.geoFileData.N,...
            'cv',[],...
            'radios',[],...
            'radioObjetivo',[],...
            'minVel',[]);
          % se preservan los campos: .centers .triangles .areas .N
          
          D.cv = ones(size(cont1(i).vec.xc)); % es contorno de arriba
          
          % si es una frontera de continuidad
%           if kind == 2
          % por convenci?n en multi-ibem, las normales apuntan a -z
          % invertir la normal
          D.cv(range) = 2; % es contorno de abajo
          D.N(range,1) = -D.N(range,1);
          D.N(range,2) = -D.N(range,2);
          D.N(range,3) = -D.N(range,3);
%           end
          D.radios = sqrt(D.areas(:,1)/pi);
          D.radioObjetivo = D.areas(:,1).*0;
          D.minVel = min(minVel,[],2);
%           if isfield(para.cont(m,1).piece{p},'subdibData')
%             para.cont(m,1).piece{p} = rmfield(para.cont(m,1).piece{p},'subdibData');
%           end
          para.cont(m,1).piece{p}.subdibData = D;
          i = i + 1;
        end
      else
        disp('una pieza no tiene datos');
      end % hay datos
    end % se carg?
  end % cada pieza
end % cada medio
if  hacerlo
  para.cont1 = cont1;
  
  % de cont0 se toma la geometr?a para conocer la regi?n de los receptores
  para.cont0  = cont1;
  para.nmed0	= 1 + length(para.cont0);
  
  %cuando hay contornos que se dividen, hay que reencontrar el medio exterior
  %con la nueva numerotacion
  nmed1  = length(cont1);
  
  tam = 0;
  for m1=1:nmed1 % las inclusiones
    if ~isempty(m1)
      tam = m1+1;
    end
  end
  subm1=zeros(1,tam);
  subm1(1) = 1; % el medio de fondo
  for m1=1:nmed1 % las inclusiones
    if ~isempty(m1)
      subm1(m1+1)=cont1(m1).m;
    end
  end
  
  subm=struct('m',zeros(1,para.nmed));
  for m=1:para.nmed
    subm(m).m=find(subm1==m);
  end
  para.subm1=subm1;
  para.subm =subm;
  para.nmedf = para.nmed;
end %hacerlo
end