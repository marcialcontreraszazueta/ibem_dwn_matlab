function [out,flag] = previewSTL(h,cont)
% Adaptado de CAD2MATDEMO de Don Riley (c) 2003
out = []; flag = 0; 

if cont.isalist
  v = [fileparts(pwd),'/ins/',cont.stage(end).fileName];
  if iscell(v)
    nombreCompleto = v{1};
  else
    nombreCompleto = v;
  end
else
  nombreCompleto = cont.fileName;
end

% disp(['loading: ' nombreCompleto]);
[F,V,N,C,T,A,name,errmsg] = rndread(nombreCompleto);

% algunos STL estan bien chafas y tienen caras ficticias con normal (0,0,0)
aN = N(1,:)+N(2,:)+N(3,:);
rmvMe = find(abs(aN)<0.01);
C(:,rmvMe) = [];
T(:,:,rmvMe) = [];
A(rmvMe) = [];
N(:,rmvMe) = [];
% F
% V

% disp(['cant de puntos de colocación: ' num2str(length(C))])
if ~strcmp(errmsg,''); return; end
out.fileName = name;
out.centers = C;
out.triangles = T;
out.areas = A;
out.F = F;
out.V = V;
out.N = N';
if (h ~= 0)
axes(h); cla;
hold on
plotSTL(h,out,cont.ColorIndex,0.5);
axes(h);light;daspect([1 1 1]) 
disp([num2str(length(A))  ' segmentos '])
end
flag = 1;
end
