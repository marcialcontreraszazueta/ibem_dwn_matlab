function z = SemiEsfera(a,x0,y0,x,y)
z = sqrt(a^2 - (x-x0)^2 - (y-y0)^2);
if z <= 0
  z = 0;
end
end