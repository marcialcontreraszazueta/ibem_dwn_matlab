function GNIJ=Greenex_3D(CAKA,R,BEALF,VN,mu,g,EPS)
% GREENEX integraciOn analItica en elemento circular de Gij(x,xi) | x=xi
% [(u v w) x (fx fy fz)]

% CAKA = omega/beta
% CAQA = omega/alpha (no se usa)
% R = el radio del disco
% BEALF = beta/alpha
% VN = normal (3x1)

% if RNM (distancia fuente a receptor es) > 0.1 R (el radio del disco)
  % g(1)=XNM/RNM
  % g(2)=YNM/RNM
  % g(3)=ZNM/RNM
  % EPS = RNM/R
% else
  % g(:) no se usa
  % EPS = 0
% end

GNIJ = complex(zeros(3,3,1));
PG = zeros(3,1);

BEA2=BEALF*BEALF;
BEA3=BEA2*BEALF;
BEA4=BEA2*BEA2;

%     GREEN FUNCTION (WITH ANALYTIC INTEGRATION)

% CAQR=CAQA*R;
CAKR=CAKA*R;
% AQR2=CAQR*CAQR;
AKR2=CAKR*CAKR;
% AQR3=AQR2*CAQR;
AKR3=AKR2*CAKR;

if (EPS == 0)
  F1=1-1i*(2+BEA3)*CAKR/6-1*(1+0.5*BEA4)*AKR2/9+1i*BEA2*BEA3*AKR3/24;
  F1=F1/(pi*R);
  F2=1*(1+BEA2)/2-1i*(2+BEA3)*CAKR/6-1*(2+BEA4)*AKR2/18+1i*AKR3/24;
  F2=F2/(pi*R);

  for I=1:3
    for J=1:3
      AUX=(F2-F1)*VN(I)*VN(J);
      if(I == J)
        AUX=AUX+F1+F2;
      end
      GNIJ(I,J,1)=AUX/4.0/mu;
    end
  end
else
  PG(1)=VN(2)*g(3)-VN(3)*g(2);
  PG(2)=VN(3)*g(1)-VN(1)*g(3);
  PG(3)=VN(1)*g(2)-VN(2)*g(1);
  EPS2=EPS*EPS;
  EPS4=EPS2*EPS2;
  EPS6=EPS4*EPS2;
  C1=1.0-EPS2*0.25-EPS4*3./64.-EPS6*5./192.;
  C2=1.0;
  C3=1.0+EPS2*0.75-EPS4*3./64.-EPS6/256.;
  C4=1.0+EPS2*2.;
  F2C=1*(1.+BEA2)/2.*C1-1i*(2.+BEA3)*CAKR/6.*C2 ...
    -1*(2.+BEA4)*AKR2/18.*C3+1i*AKR3/24.*C4;
  F2C=F2C/(pi*R);
  D1=1.0-EPS2*0.125-EPS4/64.-EPS6*5./1024.;
  D2=1.0+EPS2*0.5;
  D3=1.0+EPS2*21./8.-EPS4*21./64.-EPS6*43./1024.;
  D4=1.0+EPS2*4.;
  E1=1.0-EPS2*0.375-EPS4*5./64.-EPS6*35./1024.;
  E2=1.0-EPS2*0.5;
  E3=1.0+EPS2*1.875-EPS4*57./64.-EPS6*55./1024.;
  E4=1.0;
  F1D=1*D1-1i*(2.+BEA3)*CAKR/6.*D2 ...
    -1*(1.+.5*BEA4)*AKR2/9.*D3+1i*BEA2*BEA3*AKR3/24.*D4;
  F1D=F1D/(pi*R);
  F2D=1*(1.+BEA2)/2.*D1-1i*(2.+BEA3)*CAKR/6.*D2 ...
    -1*(2.+BEA4)*AKR2/18.*D3+1i*AKR3/24.*D4;
  F2D=F2D/(pi*R);

  F1E=1*E1-1i*(2.+BEA3)*CAKR/6.*E2 ...
    -1*(1.+.5*BEA4)*AKR2/9.*E3+1i*BEA2*BEA3*AKR3/24.*E4;
  F1E=F1E/(pi*R);
  F2E=1*(1.+BEA2)/2.*E1-1i*(2.+BEA3)*CAKR/6.*E2 ...
    -1*(2.+BEA4)*AKR2/18.*E3+1i*AKR3/24.*E4;
  F2E=F2E/(pi*R);

  for I=1:3
    for J=1:3
      AUX=(F1D-F2D)*g(I)*g(J)+(F1E-F2E)*PG(I)*PG(J);
      if (I==J)
        AUX=AUX+F2C*2.0;
      end
      GNIJ(I,J,1)=AUX/4.0/mu;
    end
  end
end
end