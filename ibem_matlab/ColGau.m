function z = ColGau(h,x0,y0,sx,sy,x,y)
z = h * exp(-((x-x0)^2)/(2*sx^2)) * exp(-((y-y0)^2)/(2*sy^2));
end