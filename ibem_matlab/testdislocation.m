% test dislocation

%% geometria
% fuente en para.xs, .ys, .zs
A = 1.0; %Area de ruptura

% receptores
para=pos_rec(para);
x0(:,3) = para.rec.zr;
x0(:,2) = para.rec.yr;
x0(:,1) = para.rec.xr;
nrec    = para.rec.nrec;

figure(4322);clf; hold on; plot3(para.xs,para.ys,para.zs,'r*')
 plot3(x0(:,1),x0(:,2),x0(:,3),'k.'); view(3)
zlim([1 2.5])
%% funciOn de ruptura
% para.pulso.a : raise time
% para.pulso.b : desfase
% para.pulso.tipo = 5; % gaussiano raise time
[Fq,utc,tps,~] = showPulso( para , false); % <- true grafica funciOn
Fq(1) = 0.01*Fq(2);
om = 2*pi*Fq;

%% desplazamiento en el dominio de la frecuencia
para = normalizacion(para);
nf   = para.nf;
nfN  = nf/2+1;
df   = para.fmax/nfN;
% escala del sismograma:
  dt_nopad = 1/df/(nf-1); % dt sin zeropading
  sca = dt_nopad*para.zeropad/nf/nf;
  
u0   = zeros(3,nfN,nrec);
u0t  = zeros(para.zeropad,nrec,1,3); %consistente con utc
for iX = 1:nrec
  u0(:,:,iX) = dislocation(para,x0(iX,:),utc,om,A);
  
  %se�al en el tiempo para cada canal
  for k=1:3
    % crepa:
    vec = zeros(1,para.zeropad);
    vec(1:nfN) = u0(k,1:nfN,iX);
    vec(end-nfN+3:end)=conj(u0(k,nfN-1:-1:2,iX));
    
    u0taux= real(sca*ifft(vec)); % inversa
    u0t(:,iX,1,k) = u0taux;
  end
end

%% graficar algunos resultados
figure(44343); hold on
iX = 140;
clc;disp([x0(iX,1) x0(iX,2) x0(iX,3)])
plot(tps,u0t(:,iX,1,1),'r-');
plot(tps,u0t(:,iX,1,2),'g-');
plot(tps,u0t(:,iX,1,3),'b-');

%% graficar campo de desplazamiento
figure(4322); hold on
h = gca;

film = para.film; %definido en la interfaz grAfica
iinc0 = 1; % Numero de la incidencia
filmoscopio3D(para,u0t,0,film,iinc0,h);
