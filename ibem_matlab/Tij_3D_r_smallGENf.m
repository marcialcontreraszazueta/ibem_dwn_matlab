function [Tij]=Tij_3D_r_smallGENf(xr,vn,... %receptor coord y vec normal
        CubPts,CubWts,... % coordenadas y pesos de la cubatura (fuente)
        ks,kp,~)

% Una fuente un receptor. 
% El receptor muy cerca de la fuente.
% La fuente distribuida en un segmento triangular.

  % cosenos directores entre el receptor y las fuentes
  ngau = length(CubWts);
  xij = xr(1)-CubPts(1,1:ngau);
  yij = xr(2)-CubPts(2,1:ngau);
  zij = xr(3)-CubPts(3,1:ngau);
  rij = sqrt(xij.^2+yij.^2+zij.^2);
  g = zeros(3,ngau);
  g(1,1:ngau)  = xij./rij;
  g(2,1:ngau)  = yij./rij;
  g(3,1:ngau)  = zij./rij;
  
  %% Funcion de Green fuente puntual:
  Trij = complex(zeros(3,3,ngau,1)); % <-- ngau
  for iQ = 1:ngau
    Trij(:,:,iQ,1) = Tij_3Done(ks,kp,rij(iQ),g(1:3,iQ),vn(1:3));
  end
  % De antemano sabemos que rij ~= 0 en todos lados.
  % ya serIa muy mala suerte que el receptor este en uno de los puntos de
  % cubatura.
  Tij  = complex(zeros(3,3,1,1));
  for i=1:3
    for j=1:3
      Tij(i,j,1,1)	= sum(CubWts(1:ngau).*squeeze(Trij(i,j,1:ngau,1)));
    end
  end
end
