function fac = verticalFactors(icr,ncapas,z,h,SolutionPSV,SolutionSH,...
  nu,gamma,kr,tipo,nkr)
% disp('solucion con la fase vertical')
% disp([icr,z,h])
if tipo == 1
fac = complex(zeros(5,nkr,1));  
else
fac = complex(zeros(10,nkr,1));
end
exp1    = exp(-1i.*nu.* z);
exp2    = exp(-1i.*gamma.* z);
exp3    = exp( 1i.*nu.*(z-h));
exp4    = exp( 1i.*gamma.*(z-h));

% Coeficientes de los desplazamientos en funcion de los potenciales:
if tipo == 1 % Desplazamientos
  % fx %
  %%%%%%
  Uz = complex(zeros(3,nkr));
  Uz(1,:) =-1i*gamma;    %     fx   fz              Amplitude Phi sens z +
  Uz(2,:) = kr.^2;       %     fx   fz              Amplitude Psi z +
  Uz(3,:) = 0;           %     fx                   Amplitude Chi z +
  
  Ur = complex(zeros(3,nkr));
  Ur(1,:) = 1;           %     fx   fz              Amplitude Phi z +
  Ur(2,:) =-1i*nu;       %     fx   fz              Amplitude Psi z +
  Ur(3,:) = 1;           %     fx                   Amplitude Chi z +
  %Ut=Ur;
  
  %   coeficientes de los desplazamientos en funcion de los potenciales
  if icr<ncapas
    fac(1,:,1)=...fx: facUPSV
      +Ur(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Ur(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1 ...
      +Ur(1,:).*SolutionPSV(3+(icr-1)*4,:,1).*exp4 ...
      -Ur(2,:).*SolutionPSV(4+(icr-1)*4,:,1).*exp3;
    fac(2,:,1)=...fx:facUSH
      +Ur(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1 ...
      +Ur(3,:).*SolutionSH(2+(icr-1)*2,:).*exp3;
    fac(3,:,1)=...fx:facUz
      +Uz(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Uz(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1 ...
      -Uz(1,:).*SolutionPSV(3+(icr-1)*4,:,1).*exp4 ...
      +Uz(2,:).*SolutionPSV(4+(icr-1)*4,:,1).*exp3;
    fac(4,:,1)=...fz:farUrz
      +Ur(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Ur(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1 ...
      +Ur(1,:).*SolutionPSV(3+(icr-1)*4,:,2).*exp4 ...
      -Ur(2,:).*SolutionPSV(4+(icr-1)*4,:,2).*exp3;
    fac(5,:,1)=...fz:facUzz
      +Uz(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Uz(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1 ...
      -Uz(1,:).*SolutionPSV(3+(icr-1)*4,:,2).*exp4 ...
      +Uz(2,:).*SolutionPSV(4+(icr-1)*4,:,2).*exp3;
  else
    fac(1,:,1)=...fx:facUPSV
      +Ur(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Ur(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1;
    fac(2,:,1)=...fx:facUSH
      +Ur(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1;
    fac(3,:,1)=...fx=facUz
      +Uz(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Uz(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1;
    fac(4,:,1)=...fz:farUrz
      +Ur(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Ur(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1;
    fac(5,:,1)=...fz:facUzz
      +Uz(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Uz(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1;
  end
  
end
if tipo == -1 % Esfuerzos
  % fx %
  %%%%%%
  xi = nu.^2-kr.^2;
  
  Szz = complex(zeros(3,nkr));
  Szz(1,:)=-xi;          %     fx   fz              Amplitude Phi sens z +
  Szz(2,:)=-2i*nu.*kr.^2;%     fx   fz              Amplitude Psi z +
  Szz(3,:)= 0;           %     fx                   Amplitude Chi z +
  
  Szr = complex(zeros(3,nkr));
  Szr(1,:)=-2i*gamma;    %     fx   fz              Amplitude Phi z +
  Szr(2,:)=-xi;          %     fx   fz              Amplitude Psi z +
  Szr(3,:)=-1i*nu;       %     fx                 	Amplitude Chi z +
  
  Srr = complex(zeros(3,nkr));
  Srr(1,:)= 1;           %     fx   fz              Amplitude Phi sens z +
  Srr(2,:)=-1i*nu;       %     fx   fz              Amplitude Psi z +
  Srr(3,:)= 1;           %     fx                   Amplitude Chi z +
  
  %Szt = Szr;
  %Srt = Srr;
  %Stt = Srr;
  
  if icr<ncapas
    %fx:
    fac(1,:,1)=...fx:facSzrPSV
      +Szr(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Szr(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1 ...
      -Szr(1,:).*SolutionPSV(3+(icr-1)*4,:,1).*exp4 ...
      +Szr(2,:).*SolutionPSV(4+(icr-1)*4,:,1).*exp3;
    fac(2,:,1)=...fx:facSzrSH
      +Szr(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1 ...
      -Szr(3,:).*SolutionSH(2+(icr-1)*2,:).*exp3;
    fac(3,:,1)=...fx:facSzz
      +Szz(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Szz(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1 ...
      +Szz(1,:).*SolutionPSV(3+(icr-1)*4,:,1).*exp4 ...
      -Szz(2,:).*SolutionPSV(4+(icr-1)*4,:,1).*exp3;
    fac(4,:,1)=...fx:facSrrP
      +Srr(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Srr(1,:).*SolutionPSV(3+(icr-1)*4,:,1).*exp4;
    fac(5,:,1)=...fx:facSrrSV
      +Srr(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1 ...
      -Srr(2,:).*SolutionPSV(4+(icr-1)*4,:,1).*exp3;
    fac(6,:,1)=...fx:facSrrSH
      +Srr(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1 ...
      +Srr(3,:).*SolutionSH(2+(icr-1)*2,:).*exp3;
    %fz:
    fac(7,:,1)=...fz:facSzr
      +Szr(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Szr(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1 ...
      -Szr(1,:).*SolutionPSV(3+(icr-1)*4,:,2).*exp4 ...
      +Szr(2,:).*SolutionPSV(4+(icr-1)*4,:,2).*exp3;
    fac(8,:,1)=...fz:facSzz
      +Szz(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Szz(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1 ...
      +Szz(1,:).*SolutionPSV(3+(icr-1)*4,:,2).*exp4 ...
      -Szz(2,:).*SolutionPSV(4+(icr-1)*4,:,2).*exp3;
    fac(9,:,1)=...fz:facSrrP
      +Srr(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Srr(1,:).*SolutionPSV(3+(icr-1)*4,:,2).*exp4;
    fac(10,:,1)=...fz:facSrrSV
      +Srr(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1 ...
      -Srr(2,:).*SolutionPSV(4+(icr-1)*4,:,2).*exp3;
    
  else
    %fx:
    fac(1,:,1)=...fx:facSzrPSV
      +Szr(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Szr(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1;
    fac(2,:,1)=...fx:facSzrSH
      +Szr(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1;
    fac(3,:,1)=...fx:facSzz
      +Szz(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2 ...
      +Szz(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1;
    fac(4,:,1)=...fx:facSrrP
      +Srr(1,:).*SolutionPSV(1+(icr-1)*4,:,1).*exp2;
    fac(5,:,1)=...fx:facSrrSV
      +Srr(2,:).*SolutionPSV(2+(icr-1)*4,:,1).*exp1;
    fac(6,:,1)=...fx:facSrrSH
      +Srr(3,:).*SolutionSH(1+(icr-1)*2,:).*exp1;
    
    %fz:
    fac(7,:,1)=...fz:facSzr
      +Szr(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Szr(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1;
    fac(8,:,1)=...fz:facSzz
      +Szz(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2 ...
      +Szz(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1;
    fac(9,:,1)=...fz:facSrrP
      +Srr(1,:).*SolutionPSV(1+(icr-1)*4,:,2).*exp2;
    fac(10,:,1)=...fz:facSrrSV
      +Srr(2,:).*SolutionPSV(2+(icr-1)*4,:,2).*exp1;
  end
end
end