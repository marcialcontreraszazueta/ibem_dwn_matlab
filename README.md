# README #

The Indirect Boundary Element Method (IBEM) is a collocation method aimed to the modeling of elastic wave diffraction in bounded and unbounded domains. The current implementation supports 2D and 3D arbitrary boundaries and multiple domains. Some of the domains may be half-spaces with flat horizontal layers. These domains fundamental solutions are obtained through a simple discrete wave number representation.

The current repository is to be regarded as a tested and working copy of the algorithm up to late 2016. This is a solidly academic version. Some portions of the code are orphan, legacy or unit-test like so this is not to be considered a production ready code. 

Result oriented tests are described in reference (4) and may be reproduced with scripts in the 'benchmarks' directory. 

### Current and future versions ###

Code is in the Matlab language but the most intensive parts may be compiled readily. A fully C++ version is on the works.

Each function is thoroughly commented but prior knowledge of the method is required. Unfortunately comments are in mixed Spanish, French and English. Comments will be translated to English over time.

A comprehensive set of tutorials intended for a graduate course is under development.

### References ###

This is a non user friendly, academic software. Required readings on the topic are:

1- Sánchez-Sesma, F. J. y Campillo, M. (1991). Diffraction of P, SV, and Rayleigh waves by topographic features: A boundary integral formulation. Bulletin of the Seismological Society of America, 81(6):2234–2253.

2- Sánchez-Sesma, F. J. y Luzón, F. (1995). Seismic response of alluvial valleys for incident P, SV and Rayleigh waves. Bull. Seism. Soc. of America, 85(1):269–284.

3- Perton, M., Contreras-Zazueta, M. A., y Sánchez-Sesma, F. J. (2016). Indirect boun- dary element method to simulate elastic wave propagation in piecewise irregular and flat regions. Geophysical Journal International, 205(3):1832–1842.

4- Contreras-Zazueta, M. A. (2017) "Optimación del método indirecto de elementos de contorno para aplicaciones en geofísica y geotecnia" doctoral thesis in Spanish at UNAM, Mexico.

### How do I get set up? ###

Clone the repository to your local machine or remote cluster. Code will be in the 'ibem_matlab' directory. 'benchmarks' and an 'ins' directory with additional geometry files will also be installed. 

Wile inside 'ibem_matlab' a benchmark test may be executed with:
run ../benchmarks/005/batchGenerarEjercicio005.m

The program may be called  in several ways:
- through the visual user interface: main.m
- from a fixed Matlab setup script called by BatchScript.m
- from a set of text input files called by the script batch_runFromFiles.m

Results are saved to the 'out' directory and variables become available to memory during the current session. Output during the calculation is sent to the terminal. The Script input methods are recommended for remote sessions or large batch analyses.

### Contact, Copyright and Citation ###

Contributions, collaboration inquires, error reports, and feature requests are greatly appreciated. 
Please contact:

Marcial Contreras Zazueta 
marcialcz@gmail.com
mcontrerasz@iingen.unam.mx

or 

Francisco José Sánchez Sesma
sesma@unam.mx

Copyright:
This software was developed under sponsorship of the National Science and Technology Council (CONACYT) at the Institute of Engineering (IINGEN) and belongs to the National Autonomous University of Mexico (UNAM). Consult the license at http://www.iingen.unam.mx

Citation:
Please refer the date and website from were you downloaded. 
This version is:  IBEM-DWN (2016) by  Institute of Engineering UNAM.